﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers.Bases;
using Environment = AuthorizeNet.Environment;
using EM.Factory;

namespace EM.Helpers
{
    public class PaymentSession : IDisposable
    {
        private bool _disposed;
        public String ApiId { get; set; }
        public String ApiTsKey { get; set; }
        public Boolean Status { get; set; }
        public String Message { get; set; }
        public AuthorizeNet.Environment AEnv { get; set; }
        public PaymentSession()
        {
            _disposed = false;
            this.ApiId = AnInfo.GetAnApiId();
            this.ApiTsKey = AnInfo.GetAnApiTsKey();
            var configMode = System.Configuration.ConfigurationManager.AppSettings["AnProductionMode"];
            AEnv = AuthorizeNet.Environment.SANDBOX;
            if (configMode == "1")
            {
                AEnv = Environment.PRODUCTION;
            }
        }
        public AnProfile CreateCustomerProfile(AnProfile profile)
        {
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            customerProfileType customerProfile = new customerProfileType();
            customerProfile.email = profile.User.Email;
            //customerProfile.description = profile.User.CompanyName;

            var request = new createCustomerProfileRequest { profile = customerProfile, validationMode = validationModeEnum.none };

            var controller = new createCustomerProfileController(request);          // instantiate the contoller that will call the service
            controller.Execute();

            createCustomerProfileResponse response = controller.GetApiResponse();   // get the response from the service (errors contained if any)

            //validate

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok && response.messages.message != null)
            {
                profile.AnProfileId = response.customerProfileId;
                this.Status = true;
                return profile;
            }
            else
            {
                if (response != null && response.messages != null && response.messages.message.Any())
                { this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text; }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Creating Profile.No response from Auth.Net API."; }
                this.Status = false;
                return null;
            }
        }

        public AnProfile UpdateCustomerProfile(AnProfile profile)
        {
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTsKey,
            };

            var customerProfile = new customerProfileExType
            {
                //merchantCustomerId = profile.User.Email,
                description = profile.User.UserName,
                email = profile.User.Email,
                customerProfileId = profile.AnProfileId

            };


            var request = new updateCustomerProfileRequest();
            request.profile = customerProfile;

            // instantiate the controller that will call the service
            var controller = new updateCustomerProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                this.Status = true;
                this.Message = response.messages.message[0].text;
                return profile;
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                { this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text; }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Update Profile.No response from Auth.Net API."; }
                return null;
            }
        }

        public AnProfile GetPaymentProfile(AnProfile profile)
        {
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            var request = new getCustomerPaymentProfileRequest();
            request.customerProfileId = profile.AnProfileId;
            request.customerPaymentProfileId = profile.PayProfile.AnPaymentProfileId;

            // Set this optional property to true to return an unmasked expiration date
            //request.unmaskExpirationDateSpecified = true;
            //request.unmaskExpirationDate = true;


            // instantiate the controller that will call the service
            var controller = new getCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                this.Message = response.messages.message[0].text;
                //Console.WriteLine("Customer Payment Profile Id: " + response.paymentProfile.customerPaymentProfileId);
                if (response.paymentProfile.payment.Item is creditCardMaskedType)
                {
                    profile.CardNumber = (response.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
                    profile.ExpirationDate = (response.paymentProfile.payment.Item as creditCardMaskedType).expirationDate;
                    profile.CardType = (response.paymentProfile.payment.Item as creditCardMaskedType).cardType;

                }
                this.Status = true;
                return profile;
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                { this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text; }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Get Payment Profile.No response from Auth.Net API."; }

                return null;
            }
        }

        public bool ValidatePaymentProfile(AnProfile profile, UserPaymentProfile payProfile)
        {
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            var request = new validateCustomerPaymentProfileRequest();
            request.customerProfileId = profile.AnProfileId;
            request.customerPaymentProfileId = payProfile.AnPaymentProfileId;
            request.validationMode = validationModeEnum.liveMode;


            // instantiate the controller that will call the service
            var controller = new validateCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                Message = response.messages.message[0].text;
                this.Status = true;
                return true;
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                { this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text; }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Validate Payment Profile.No response from Auth.Net API."; }
                return false;
            }
        }

        public AnProfile UpdatePaymenProfile(AnProfile profile)
        {
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };
            var creditCard = new creditCardType
            {
                cardNumber = profile.CardNumber.Trim(),
                expirationDate = profile.ExpirationDate,
                cardCode = profile.CCV
            };
            paymentType cc = new paymentType { Item = creditCard };

            customerPaymentProfileExType ccPaymentProfile = new customerPaymentProfileExType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = new customerAddressType()
            {
                address = profile.PayProfile.Address1 + " " + profile.PayProfile.Address2,
                firstName = profile.PayProfile.FirstName,
                lastName = profile.PayProfile.LastName,
                city = profile.PayProfile.City,
                company = profile.User.Email,
                country = profile.PayProfile.Country,
                email = profile.PayProfile.Email,
                state = profile.PayProfile.State,
                zip = profile.PayProfile.Zip
            };
            ccPaymentProfile.customerPaymentProfileId = profile.PayProfile.AnPaymentProfileId;

            var request = new updateCustomerPaymentProfileRequest();
            request.customerProfileId = profile.AnProfileId;
            request.paymentProfile = ccPaymentProfile;
            request.validationMode = validationModeEnum.none;


            // instantiate the controller that will call the service
            var controller = new updateCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                this.Status = true;
                this.Message = response.messages.message[0].text;
                AnProfile profileNew = GetPaymentProfile(profile);
                if (profileNew != null)
                {
                    this.Status = true;
                    profile.CardType = profileNew.CardType;
                    return profile;
                }
                else
                {

                    return profile;
                }
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                { this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text; }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Update Payment Profile.No response from Auth.Net API."; }
                return null;
            }
        }
        public AnProfile CreatePaymenProfile(AnProfile profile)
        {
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };
            var creditCard = new creditCardType
            {
                cardNumber = profile.CardNumber.Replace("-", ""),
                expirationDate = profile.ExpirationDate,
                cardCode = profile.CCV
            };
            paymentType cc = new paymentType { Item = creditCard };

            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = new customerAddressType()
            {
                address = profile.PayProfile.Address1 + " " + profile.PayProfile.Address2,
                firstName = profile.PayProfile.FirstName,
                lastName = profile.PayProfile.LastName,
                city = profile.PayProfile.City,
                company = profile.User.Email,
                country = profile.PayProfile.Country,
                email = profile.PayProfile.Email,
                state = profile.PayProfile.State,
                zip = profile.PayProfile.Zip
            };

            var request = new createCustomerPaymentProfileRequest();
            request.customerProfileId = profile.AnProfileId;
            request.paymentProfile = ccPaymentProfile;
            request.validationMode = validationModeEnum.none;


            // instantiate the controller that will call the service
            var controller = new createCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {

                profile.PayProfile.AnPaymentProfileId = response.customerPaymentProfileId;
                this.Message = response.messages.message[0].text;

                AnProfile profileNew = GetPaymentProfile(profile);
                if (profileNew != null)
                {
                    this.Status = true;
                    profile.CardType = profileNew.CardType;
                    return profile;
                }
                else
                {
                    this.Status = false;
                }
                return null;
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                { this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text; }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Create Payment Profile.No response from Auth.Net API."; }
                return null;
            }

        }
        public AnTranResponse ChargeCreditCard(CCProfile ccProfile, decimal amount)
        {

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };
            var creditCard = new creditCardType
            {
                cardNumber = ccProfile.CardNumber,
                expirationDate = ccProfile.ExpirationDate,
                cardCode = ccProfile.CCV
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            var billTo = new customerAddressType()
            {
                address = ccProfile.Address,
                firstName = ccProfile.FirstName,
                lastName = ccProfile.LastName,
                city = ccProfile.City,
                country = ccProfile.Country,
                state = ccProfile.State,
                zip = ccProfile.Zip,
                phoneNumber = ccProfile.ContactNo

            };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // charge the card
                amount = amount,
                payment = paymentType,
                billTo = billTo,
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transactionResponse != null)
                {
                    this.Status = true;
                    return new AnTranResponse()
                    {
                        ApprovalCode = response.transactionResponse.authCode,
                        TransId = response.transactionResponse.transId,
                        ResponseCode = response.transactionResponse.responseCode
                    };
                    //  Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                }
                else
                {
                    this.Status = true;
                    return new AnTranResponse();
                }
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                {
                    if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                    {
                        this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                    }
                    else
                    {
                        this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                    }
                }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Charge Credit Card.No response from Auth.Net API."; }

                return null;
            }
        }


        public AnTranResponse PreAuthorizeCreditCard(decimal amount, string cardNumber, string expirationDate)
        {

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            var creditCard = new creditCardType
            {
                cardNumber = cardNumber,
                expirationDate = expirationDate

            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authOnlyTransaction.ToString(),    // authorize only
                amount = amount,
                payment = paymentType,
            };


            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {

                    if (response.transactionResponse != null)
                    {
                        this.Status = true;
                        return new AnTranResponse()
                        {
                            ApprovalCode = response.transactionResponse.authCode,
                            TransId = response.transactionResponse.transId,
                            ResponseCode = response.transactionResponse.responseCode
                        };
                        //  Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                    }
                    else
                    {
                        this.Status = true;
                        return new AnTranResponse();
                    }
                }
                else
                {
                    this.Status = false;
                    if (response != null && response.messages != null)
                    {
                        if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                        {
                            this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                        }
                        else
                        {
                            this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                        }
                    }
                    else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                    { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                    else
                    { this.Message = "Error in Charge Customer Profile.No response from Auth.Net API."; }

                    return null;
                }
            }
            else
            {
                if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                this.Status = false;
                return new AnTranResponse();
            }

        }


        public AnTranResponse PreAuthorizeCreditCard(AnProfile profile, decimal amount)
        {
            if (string.IsNullOrEmpty(profile?.AnProfileId) || string.IsNullOrEmpty(profile?.PayProfile?.AnPaymentProfileId))
            {
                throw new Exception("Invalid credit card profile.");
            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = profile.AnProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = profile.PayProfile.AnPaymentProfileId };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authOnlyTransaction.ToString(),
                amount = amount,
                profile = profileToCharge,
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {

                    if (response.transactionResponse != null)
                    {
                        this.Status = true;
                        return new AnTranResponse()
                        {
                            ApprovalCode = response.transactionResponse.authCode,
                            TransId = response.transactionResponse.transId,
                            ResponseCode = response.transactionResponse.responseCode
                        };
                        //  Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                    }
                    else
                    {
                        this.Status = true;
                        return new AnTranResponse();
                    }
                }
                else
                {
                    this.Status = false;
                    if (response != null && response.messages != null)
                    {
                        if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                        {
                            this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                        }
                        else
                        {
                            this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                        }
                    }
                    else
                    { this.Message = "Error in Charge Customer Profile.No response from Auth.Net API."; }

                    return null;
                }
            }
            else
            {
                if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                this.Status = false;
                return new AnTranResponse();
            }

        }


        public AnTranResponse PreAuthChargeCustomerProfile(AnProfile profile, decimal amount, String poNumber, string transactionId)
        {
            if (string.IsNullOrEmpty(profile?.AnProfileId) || string.IsNullOrEmpty(profile?.PayProfile?.AnPaymentProfileId))
            {
                throw new Exception("Invalid credit card profile.");
            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };


            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = profile.AnProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = profile.PayProfile.AnPaymentProfileId };

            var transactionRequest = new transactionRequestType
            {
                //transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // refund type
                //amount = amount,
                //profile = profileToCharge,

                //order = new orderType()
                //{
                //    invoiceNumber = poNumber
                //}
                transactionType = transactionTypeEnum.priorAuthCaptureTransaction.ToString(),    // capture prior only
                amount = amount,
                refTransId = transactionId,
                poNumber = poNumber,
                order = new orderType()
                {
                    invoiceNumber = poNumber
                }
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the collector that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transactionResponse != null)
                {
                    this.Status = true;
                    return new AnTranResponse()
                    {
                        ApprovalCode = response.transactionResponse.authCode,
                        TransId = response.transactionResponse.transId,
                        ResponseCode = response.transactionResponse.responseCode
                    };
                    //  Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                }
                else
                {
                    this.Status = true;
                    return new AnTranResponse();
                }
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                {
                    if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                    {
                        this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                    }
                    else
                    {
                        this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                    }
                }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Charge Customer Profile.No response from Auth.Net API."; }

                return null;
            }
        }



        public AnTranResponse ChargeCustomerProfile(string AnPaymentProfileId, string AnProfileId, decimal amount, String poNumber, String description = null)
        {
            if (string.IsNullOrEmpty(AnPaymentProfileId) || string.IsNullOrEmpty(AnProfileId))
            {
                throw new Exception("Invalid credit card profile.");
            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };
            //create a customer payment profile
            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = AnProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = AnPaymentProfileId };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // refund type
                amount = amount,
                profile = profileToCharge,
                poNumber = poNumber,
                order = new orderType()
                {
                    invoiceNumber = poNumber,
                    description = description
                }
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the collector that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate

            if (response != null && response.messages != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transactionResponse != null)
                {
                    this.Status = true;
                    return new AnTranResponse()
                    {
                        ApprovalCode = response.transactionResponse.authCode,
                        TransId = response.transactionResponse.transId,
                        ResponseCode = response.transactionResponse.responseCode
                    };
                    //  Console.WriteLine("Success, Auth Code : " + response.transactionResponse.authCode);
                }
                else
                {
                    this.Status = true;
                    return new AnTranResponse();
                }
            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                {
                    //Fix for object reference null error for invoice and surcharge
                    if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                    {
                        this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                    }
                    else
                    {
                        this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                    }
                }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else
                { this.Message = "Error in Charge Customer Profile.No response from Auth.Net API."; }

                return null;
            }
        }

        public ANetApiResponse RefundTransaction(decimal TransactionAmount, string TransactionID, AnProfile profile)
        {
            this.Status = false;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = profile.AnProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = profile.PayProfile.AnPaymentProfileId };

            // //standard api call to retrieve response
            // var paymentType = new paymentType { Item = creditCard };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.refundTransaction.ToString(),    // refund type
                profile = profileToCharge,
                amount = TransactionAmount,
                refTransId = TransactionID
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            if (response == null)
            {
                this.Status = false;
                if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else { this.Message = "Error in processing refund."; }
                return null;
            }
            //validate
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                this.Status = true;
                if (response.transactionResponse != null)
                {
                    this.Message = "Success, Auth Code : " + response.transactionResponse.authCode;
                }
            }
            else if (response != null)
            {
                this.Status = false;
                this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                {
                    this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                }
            }

            return response;
        }
        public List<string> GetUnsettledTransactions()
        {
            this.Status = false;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };


            var request = new getUnsettledTransactionListRequest();

            // instantiate the controller that will call the service
            var controller = new getUnsettledTransactionListController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            if (response == null)
            {
                this.Status = false;
                if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else { this.Message = "Error in processing request."; }
                return null;
            }

            if (response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transactions == null)
                    return null;
                this.Status = true;
                var lst = new List<string>();
                foreach (var item in response.transactions)
                {
                    lst.Add(item.transId);
                }
                return lst;
            }
            else
            {
                this.Status = false;
                this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;

            }

            return new List<string>();
        }
        public AnTranStatusReponse GetTransactionStatus(string transactionId)
        {
            this.Status = false;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            var request = new getTransactionDetailsRequest();
            request.transId = transactionId;

            // instantiate the controller that will call the service
            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            if (response == null)
            {
                this.Status = false;
                if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else { this.Message = "Error in processing request."; }
                return null;
            }

            if (response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response.transaction == null)
                    return null;
                this.Status = true;
                return new AnTranStatusReponse()
                {
                    AuthAmount = response.transaction.authAmount,
                    SettleAmount = response.transaction.settleAmount,
                    TransactionType = response.transaction.transactionType,
                    TransId = response.transaction.transId,
                    TransactionStatus = response.transaction.transactionStatus
                };
            }
            else
            {
                this.Status = false;
                this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
            }

            return null;
        }
        public ANetApiResponse VoidTransaction(string TransactionID, AnProfile profile)
        {
            this.Status = false;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = this.AEnv;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = profile.AnProfileId;
            profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = profile.PayProfile.AnPaymentProfileId };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.voidTransaction.ToString(),    // refund type
                profile = profileToCharge,
                refTransId = TransactionID
            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //validate
            if (response == null)
            {
                this.Status = false;
                if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                { this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]); }
                else { this.Message = "Error in processing request."; }
                return null;
            }
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                this.Status = true;
                if (response.transactionResponse != null)
                {
                    this.Message = "Success, Auth Code : " + response.transactionResponse.authCode;
                }
            }
            else if (response != null)
            {
                this.Status = false;
                this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                if (response.transactionResponse != null && response.transactionResponse.errors != null
                        && response.transactionResponse.errors[0] != null)
                {
                    this.Message = "Transaction Error : " + response.transactionResponse.errors[0].errorCode + " " + response.transactionResponse.errors[0].errorText;
                }
            }


            return response;
        }



        public AnProfile CreateCutomerandPaymentProfile(AnProfile profile)
        {
            //Console.WriteLine("Create Customer Profile Sample");
            string AnProfileId = string.Empty;

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = this.ApiId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = this.ApiTsKey,
            };

            customerProfileType customerProfile = new customerProfileType();
            customerProfile.email = profile.User.Email;

            var request = new createCustomerProfileRequest { profile = customerProfile, validationMode = validationModeEnum.none };

            var controller = new createCustomerProfileController(request);          // instantiate the contoller that will call the service
            controller.Execute();

            createCustomerProfileResponse response = controller.GetApiResponse();   // get the response from the service (errors contained if any)

            //validate

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok && response.messages.message != null)
            {
                profile.AnProfileId = response.customerProfileId;

            }
            else
            {
                this.Status = false;
                if (response != null && response.messages != null)
                {
                    this.Message = "Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text;
                }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                {
                    this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]);
                }
                else
                {
                    this.Message = "Error in Get Payment Profile.No response from Auth.Net API.";
                }
                return null;
            }

            var creditCard = new creditCardType
            {
                cardNumber = profile.CardNumber,
                expirationDate = "0" + profile.ExpirationDate,
                cardCode = profile.CCV
            };
            creditCard.expirationDate = profile.ExpirationDate;
            paymentType cc = new paymentType { Item = creditCard };


            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = new customerAddressType()
            {
                address = profile.PayProfile.Address1,
                firstName = profile.PayProfile.FirstName,
                lastName = profile.PayProfile.LastName,
                city = profile.PayProfile.City,
                country = profile.PayProfile.Country,
                email = profile.User.Email,
                state = profile.PayProfile.State,
                zip = profile.PayProfile.Zip
            };

            var request2 = new createCustomerPaymentProfileRequest();
            request2.customerProfileId = profile.AnProfileId;
            request2.paymentProfile = ccPaymentProfile;
            request2.validationMode = validationModeEnum.none;


            // instantiate the controller that will call the service
            var controller2 = new createCustomerPaymentProfileController(request2);
            controller2.Execute();

            // get the response from the service (errors contained if any)
            var response2 = controller2.GetApiResponse();

            if (response2 != null && response2.messages != null && response2.messages.resultCode == messageTypeEnum.Ok)
            {
                profile.PayProfile.AnPaymentProfileId = response2.customerPaymentProfileId;
                this.Message = response.messages.message[0].text;
                AnProfile profileNew = GetPaymentProfile(profile);
                if (profileNew != null)
                {
                    this.Status = true;
                    profile.CardType = profileNew.CardType;
                    return profile;
                }
                else
                {
                    this.Status = false;
                }


            }
            else
            {
                if (response2 != null && response2.messages != null)
                {
                    this.Message = "Error: " + response2.messages.message[0].code + "  " + response2.messages.message[0].text;
                    CreateLog.ActionLog(response2.messages.message[0].text);
                }
                else if (controller.GetResultCode() == messageTypeEnum.Error && controller.GetResults()?.Count > 0)
                {
                    this.Message = "Error: " + Convert.ToString(controller.GetResults()[0]);
                    CreateLog.ActionLog(controller.GetResults()[0]);
                }
                else
                {
                    this.Message = "Error in Get Payment Profile.No response from Auth.Net API.";
                    CreateLog.ActionLog("Error in Get Payment Profile.No response from Auth.Net API.");
                }

            }

            return profile;
        }

        #region IDisposable Implementation

        //______________________________________________________________________________
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        //______________________________________________________________________________
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            //
            if (!this._disposed)
            {
                if (disposing)
                {
                    //
                }
                _disposed = true;
            }
        }

        #endregion




    }
}