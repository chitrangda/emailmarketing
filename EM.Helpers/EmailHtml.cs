﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace EM.Helpers
{
    public class EmailHtml
    {
        public static string getAdminHtml(string _email, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong> Admin</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _email + " has requested access.<br/><br/> To activate this account please click on the following link. </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + path + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team CheapesteMail</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }

        public static string getRegisterHtml(string _name, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi " + _name + ",</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thank you for signing up!</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Please click on the link below to create your password for your Cheapest Email account.</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><a href='" + path + "' >Create Password</a></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Best,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getResetPasswordHtml(string _name, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Please reset your password by clicking <a href='" + path + "' >Reset Password</a></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }
        public static string getAccountActivatedHtml(string _name, string _email, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _email + " account has been activated by admin, to login please click on the following link: </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + path + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team CheapesteMail </p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();

        }

        public static string GetApplicationPath()
        {
            //return "https://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath;
            return ConfigurationManager.AppSettings["AppUrl"].ToString();
        }

        public static string getAccountStatusHtml(string _name, string _email, string path, string status)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Cheapest Email'/>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _email + " account has been   " + status + " by admin");
            if (status == "activated")
            {
                sbAdmin.Append("<p>To login please click on the following link: </p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + path + "</p>");
            }
            else
            {
                sbAdmin.Append(".");
            }
            sbAdmin.Append(
                        "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                        + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team CheapesteMail </p>"
                        + "</tr>"
                        + "</tbody>"
                        + "</table>"
                        + "</td>"
                        + "</tr>"
                        //<!-- content end-->
                        + "</table>");
            return sbAdmin.ToString();

        }



        public static string getContactHtml(string _name, string _email, string _phone, string _comment)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Please see the mail and please reach out customer as soon as possible</p>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Name: <strong>" + _name + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Email: <strong> " + _email + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            //+ "<tr>"
                            //+ "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Company Email: <strong> " + _cmpEmail + "</strong>,</p>" + "</td>"
                            //+ "</tr>"
                            //+ "</tr>" 
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Phone Number: <strong> " + _phone + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Comment: <strong> " + _comment + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team"
                            + "</td>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getQuoteHtml(string _name, string _email, string _subject, string _message)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Here is support mail.<br/>Plaese see the mail and please reach out customer as soon as possible</p>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Name: <strong>" + _name + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Email: <strong> " + _email + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            //+ "<tr>"
                            //+ "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Company Email: <strong> " + _cmpEmail + "</strong>,</p>" + "</td>"
                            //+ "</tr>"
                            //+ "</tr>" 
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Subject: <strong> " + _subject + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Message: <strong> " + _message + "</strong>,</p>" + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team"
                            + "</td>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getUserDetails(string _name, string _password, string _email, string _repName, string _salesRepContact)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0'cellspacing='0' border='0' width='600' style='margin:0px auto; border: 10px solid #009fc7; font-family:Arial, Helvetica, sans-serif; background:#fff;'>"
                            + " <tr><td valign = 'top' style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin: 0px auto'>"
                            + "<tbody><tr>"
                            + " <td style='height: 15px;'>&nbsp;</td>"
                              + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Your login details are as follows:<br/>"
                            + "</p>"
                            + "<p style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Name: <strong>" + _name + "</strong>,</p>" + "<br/>"
                            + "</p>"
                            + "<p style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>Email: <strong> " + _email + "</strong>,</p>" + "<br/>"
                            + "</p>"
                            + "<p style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>If you need help in setting up your account, please contact your Account Manger : <strong>" + _repName + "</strong>," +
                            "at <strong> " + _salesRepContact + " or you can email him / her at <strong> " + _email + " </p>" + "<br/>"
                            + "</p>"
                            + "<p style = 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333; width: 131px;'><p>  Kind regards,</p>" + "<br/>"
                            + "</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getConfirmHtml(string _name, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thank you for registration, Please confirm your mail by clicking <a href='" + path + "' >link</a></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'> Customer Service Team CheapesteMail</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getCNameDetails(string _name, string _cName)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>To verify your email domain, please insert below CName details in your DNS server</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CName:<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + _cName + "</strong></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>For any kind of assistance, please contact your Network Administrator.</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getConfirmHtmlCampaign(string _name, string path)
        {
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Hi <strong>" + _name + "</strong>,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Please verify your mail by clicking <a href='" + path + "' >link</a></p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>CheapesteMail Customer Support Team</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getInvoiceSend(string _accountname, string _date, string _type, string _amount, string _nameoncard, string _address, string _city, string _state, string _zip, string _cardtype, string _cardNo, string _Phone , string _invoiceNo , string _descMain , string _descSec , string _descThird)
        {
            StringBuilder sbAdmin = new StringBuilder();
            var Date = String.Format("MM/dd/yyyy", _date);


            sbAdmin.Append("<table align='left' cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:60px;' align='left' style='border-top:#333 2px solid; font-size:12px; color:#FFF;'>To ensure you receive future emails, please add billing@cheapestEmail.com to your Safe Sender list.</td>"
                            + "</tr>"
                             + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:15px auto'>");
            sbAdmin.Append("<tr>" 
                            + "<td align='left'>"
                            + "<img style='height:34px;' src='"+GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "<td align='right' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333;'><strong>" + _date + "</strong>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td align='left' style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Dear " + _accountname + ",</p>"
                            + "<p align='left' style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _type + " $" + _amount + "</p>&nbsp;"
                            + "<p align='left' style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>" + _nameoncard.Trim() + "<br/> " + (!string.IsNullOrEmpty(_address) ? _address.Trim() : String.Empty) + "<br/> "+ (!string.IsNullOrEmpty(_city) ? _city.Trim() : String.Empty) + "," + (!string.IsNullOrEmpty(_state) ? _state.Trim() : String.Empty) + "," + (!string.IsNullOrEmpty(_zip) ? _zip.Trim() : String.Empty) + " <br/> " + _cardtype + " " + _cardNo + " <br/>Phone No: " + _Phone + "</p>"
                           + "<p align='left' style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><strong>Invoice Details</strong></p>"
                           + "<p align='left' style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><strong>Invoice Number: </strong> " + _invoiceNo + "</p>"
                           +"</td>"
                           + "</tr>"
                           + "<tr>"
                           + "<td align='left' style='border-bottom:#333 1px solid; font-size:12px; color: #FFF;'></td>"
                           +"</tr>"
                           +"<tr>"
                           + "<td align='left'><table width='100%' border='0' cellpadding='0' cellspacing='0'>"
                           +"<tr>"
                           + "<td width='60%' height='25' align='left' valign='middle' style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333;'>Description</td><td width='40%' align='right' style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333;'>Amount</td>"
                           + "</tr>"
                           +"<tr>"
                           + "<td align='left' style='border-bottom:#333 2px solid; font-size:12px; color:#FFF;'></td>"
                           + "<td align='right' style='border-bottom:#333 2px solid; font-size:12px; color:#FFF;'></td>"
                           + "</tr>"
                           +"<tr>"
                           + "<td align='left' style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333;'>" + _descMain + " <br/> " + (!string.IsNullOrEmpty(_descSec) ? _descSec.Trim() : String.Empty) + " <br/> " + (!string.IsNullOrEmpty(_descThird) ? _descThird.Trim() : String.Empty) + " <br/></td>"
                           + "<td align='right' style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; white-space:nowrap;'>$" + _amount + " USD</td>"
                           +"</tr>"
                           +"<tr>"
                           + "<td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:14px; color:#333;'>&nbsp;</td>"
                           + "<td align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:14px; color:#333;'>&nbsp;</td>"
                           +"</tr>"
                           + "<tr><td colspan='2'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='74%' align='right' style='font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333;'><strong>Total:</strong></td><td width = '26%' align = 'right' style = 'font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333;' >$ " + _amount + " USD </td></tr></table></td></tr>"
                           + "</table></td></tr>"
                           +"<tr>"
                           + "<td align='center' style='font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#333;'>It may take a few moments for this transaction to appear in your account.</td>"
                           + "</tr>"
                           + "<tr>"
                           + "<td align='left' style='border-bottom:#333 2px solid; font-size:12px; color:#FFF;'></td>"
                           + "</tr>"
                           + "<tr>"
                           + "<td height='25' align='left' valign='middle' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#333;'><strong>Have issues with this Transaction ?</strong></td>"
                           + "</tr>"
                           +"<tr>"
                           + "<td align='left' style='font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#333;'><p>You can get in touch with Cheapest Email via email at &nbsp;<a href='mailto:billing@cheapestEmail.com'>billing@cheapestEmail.com</a></p>"
                           + "</td>"
                           +"</tr>"
                           + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getSingleSubscriberWelcomeEmail(string _name , string _subscriberName)
        {
            var path = "https://www.app.cheapestemail.com/";
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            +"<a>"
                            + "<img  src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Welcome <strong> " + (!string.IsNullOrEmpty(_subscriberName) ? _subscriberName.Trim() : String.Empty) + "</strong>!</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>You have been successfully added by <strong>" + _name + "</strong>.</p>"
                            + "<a href='" + path + "'>"
                            + "<img  style='width:180px; height:30px' src ='" + GetApplicationPath() + "/Content/imagesHome/button_take-a-look-around.jpg' alt=''/>"
                            + "</a></td>"
                            + "</tr><br/><br/>"
                            +"<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Customer Service Team CheapesteMail</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }

        public static string getMultipleSubscriberWelcomeEmail(string _name)
        {
            var path = "https://www.app.cheapestemail.com/";
            StringBuilder sbAdmin = new StringBuilder();


            sbAdmin.Append("<table cellpadding='0' cellspacing='0' border='0' width='600' style='margin:0px auto;'>"
                            + "<tr><td valign='top' style= 'font-size:14px; font-family:Arial, Helvetica, sans-serif; color: #333333'>"
                            + "<table width='570' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>"
                            + "<tbody><tr>"
                            + "<td style='height:25px;'>&nbsp;</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td>"
                            //<!-- header start--->
                            + "<table width='520' border='0' cellspacing='0' cellpadding='0' style='margin:0px auto'>");
            sbAdmin.Append("<tr>"
                            + "<td style='text-align:center'>"
                            + "<img style='height:34px;' src='" + GetApplicationPath() + "/Content/imagesHome/logo.png' alt='Email Marketing'/>"
                            + "</td>"
                            + "</tr>"
                            //<!-- header end--->
                            + "<tbody><tr><td style='height:30px;'>&nbsp;</td></tr>"
                            //<!-- starts content --->
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p>Welcome <strong>{**FIRSTNAME**}</strong>!</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>You have been successfully added by <strong>" + _name + "</strong>.</p>"
                            + "<a href='" + path + "'>"
                            + "<img  style='width:180px; height:30px' src ='" + GetApplicationPath() + "/Content/imagesHome/button_take-a-look-around.jpg' alt ='Take a look around'/>"
                            + "</a></td>"
                            + "</tr><br/><br/>"
                            + "<tr>"
                            + "<td style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'><p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Thanks,</p>"
                            + "<p style='font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#333;'>Customer Service Team CheapesteMail</p>"
                            + "</tr>"
                            + "</tbody>"
                            + "</table>"
                            + "</td>"
                            + "</tr>"
                            //<!-- content end-->
                            + "</table>");
            return sbAdmin.ToString();
        }


    }
}
