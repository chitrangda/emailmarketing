﻿using SendGrid;
using System;
using System.Threading.Tasks;

namespace EM.Helpers
{
    public class SendGridAPIHelper
    {
        SendGridClient client = null;
        public SendGridAPIHelper()
        {
            var apiKey = System.Configuration.ConfigurationManager.AppSettings["SendGridKey"];
            client = new SendGridClient(apiKey);

        }

        public string geStats(string queryParams)
        {
            queryParams = "{" + queryParams + "}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "stats", queryParams: queryParams);
            response.Wait();
            if (response.IsCompleted)
            {
                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }
        }

        public string getStatsByCategory(string queryParams)
        {
            queryParams = "{" + queryParams + "}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "categories/stats", queryParams: queryParams);
            response.Wait();
            if (response.IsCompleted)
            {
                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }
        }

        public string getStatsSumByCategory(string queryParams)
        {
            queryParams = "{" + queryParams + "}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "categories/stats/sums", queryParams: queryParams);
            response.Wait();
            if (response.IsCompleted)
            {
                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }
        }

        public bool setIpPoolName(string name)
        {
            string data = "{\"name\":\"" + name + "\"}";
            var response = client.RequestAsync(method: SendGridClient.Method.POST, urlPath: "ips/pools", requestBody: data);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    return true;
                }
                else
                {
                    CreateLog.Log(response.Result.StatusCode.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool updateIpPoolName(string newName, string oldName)
        {
            string data = "{\"name\":\"" + newName + "\"}";
            string urls = "ips/pools/" + oldName;
            var response = client.RequestAsync(method: SendGridClient.Method.PUT, urlPath: urls, requestBody: data);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool removeIpPoolName(string name)
        {
            string urls = "ips/pools/" + name;
            var response = client.RequestAsync(method: SendGridClient.Method.DELETE, urlPath: urls);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool setIpAddress(string ipPoolName, string ipAddress)
        {
            string data = "{\"ip\":\"" + ipAddress + "\"}";
            string url = string.Format("ips/pools/{0}/ips", ipPoolName);
            var response = client.RequestAsync(method: SendGridClient.Method.POST, urlPath: url, requestBody: data);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    client.RequestAsync(method: SendGridClient.Method.POST, urlPath: "ips/warmup", requestBody: data);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool removeIp(string ipPoolName, string ipAddress)
        {
            string data = "{\"ip\":\"" + ipAddress + "\"}";
            string url = string.Format("ips/pools/{0}/ips/{1}", ipPoolName, ipAddress);
            var response = client.RequestAsync(method: SendGridClient.Method.DELETE, urlPath: url);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool getIP(string pool_name)
        {
            try
            {
                string url = "ips/pools/" + pool_name;
                var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: url);
                response.Wait();
                if (response.IsCompleted)
                {
                    if (response.Result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var data = Newtonsoft.Json.JsonConvert.DeserializeObject<SendGridIPViewModel>(response.Result.Body.ReadAsStringAsync().Result);
                        if (data.ips.Length > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false; ;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string postDomainAuthenticate(string domain)
        {

            string data = "{\"domain\":\"" + domain + "\"}";
            var response = client.RequestAsync(method: SendGridClient.Method.POST, urlPath: "whitelabel/domains", requestBody: data);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    return response.Result.Body.ReadAsStringAsync().Result;

                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }



        }

        public string getValidatedDomain(string domain)
        {
            string url = "whitelabel/domains";
            string data = "{\"domain\":\"" + domain + "\"}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: url, queryParams: data);
            response.Wait();
            if (response.IsCompleted)
            {

                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }

        }

        public string postValidateDomain(int Id)
        {
            string urls = "whitelabel/domains/" + Id + "/validate";
            var response = client.RequestAsync(method: SendGridClient.Method.POST, urlPath: urls);
            response.Wait();
            if (response.IsCompleted)
            {
                if (response.Result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return response.Result.Body.ReadAsStringAsync().Result;

                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }


        public string getStatsByEmail(string queryParams)
        {
            queryParams = "{" + queryParams + "}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "mailbox_providers/stats", queryParams: queryParams);
            response.Wait();
            if (response.IsCompleted)
            {
                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }
        }
        public async Task<string> getEmailActivityByCampaign(string CampaignId, string toEmail, int limit = 10)
        {
            string filter = "(Contains(categories,\"" + CampaignId + "\")) AND to_email=\"" + toEmail + "\"";
            string queryParams = @"{'query': '" + filter + "','limit':" + limit + "}";
            var response = await client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "messages", queryParams: queryParams);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return "";
            }
            //if (response.IsCompleted)
            //{
            //     return response.Result.Body.ReadAsStringAsync().Result;
            //}
            //else
            //{
            //    return null;
            //}
        }
        public async Task<string> getEmailActivityByCampaign(string CampaignId, string[] toEmail, int limit = 10)
        {
            string sFilterCriteria = "";
            foreach (string email in toEmail)
            {
                sFilterCriteria += "to_email=\"" + email + "\" OR ";
            }
            sFilterCriteria = sFilterCriteria.Remove((sFilterCriteria.Length - 3), 3);
            string filter = "(Contains(categories,\"" + CampaignId + "\")) AND (" + sFilterCriteria + ")";
            string queryParams = @"{'query': '" + filter + "','limit':" + limit + "}";
            var response = await client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "messages", queryParams: queryParams);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return "";
            }
            //if (response.IsCompleted)
            //{
            //     return response.Result.Body.ReadAsStringAsync().Result;
            //}
            //else
            //{
            //    return null;
            //}
        }

        public string getEmailActivityBySubject(string subject, int limit = 10)
        {
            string filter = "subject=\"" + subject + "\"";
            string queryParams = @"{'query': '" + filter + "','limit':" + limit + "}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "messages", queryParams: queryParams);
            if (response.IsCompleted)
            {
                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }
        }
        public string getEmailActivityByFromEmail(string subject, string fromEmail, int limit = 10)
        {
            string filter = "subject=\"" + subject + "\" AND from_email=\"" + fromEmail + "\"";
            string queryParams = @"{'query': '" + filter + "','limit':" + limit + "}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "messages", queryParams: queryParams);
            if (response.IsCompleted)
            {
                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }
        }
        public async Task<string> getEmailActivityByToEmail(int CampaignId, string fromEmail, string toEmail, int limit = 10)
        {
            string filter = "(Contains(categories,\"" + CampaignId + "\")) AND from_email=\"" + fromEmail + "\" AND to_email=\"" + toEmail + "\"";
            string queryParams = @"{'query': '" + System.Web.HttpUtility.UrlEncode(filter) + "','limit':" + limit + "}";
            var response = await client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "messages", queryParams: queryParams);
            //return response.Body.ReadAsStringAsync().Result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return "";
            }
        }

        public async Task<string> getEmailActivityByCampaignBounce(string CampaignId, int limit = 10)
        {
            string sFilterCriteria = "status=\"" + "bounced" + "\" OR status =\"" + "not_delivered" + "\" ";
            string filter = "(Contains(categories,\"" + CampaignId + "\")) AND (" + sFilterCriteria + ")";
            string queryParams = @"{'query': '" + filter + "','limit':" + limit + "}";
            var response = await client.RequestAsync(method: SendGridClient.Method.GET, urlPath: "messages", queryParams: queryParams);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return "";
            }
            //return response.Body.ReadAsStringAsync().Result;
            //if (response.IsCompleted)
            //{
            //     return response.Result.Body.ReadAsStringAsync().Result;
            //}
            //else
            //{
            //    return null;
            //}
        }

        public async Task<string> GetSubUserList(string userName)
        {
            string url = "subusers";
            string data = "{\"username\":\"" + userName + "\"}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: url, queryParams: data);
            response.Wait();
            if (response.IsCompleted)
            {

                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }

        }

        public async Task<bool> createSubUser(string username, string email, string password, string[] defaultIp)
        {
           
            string data = "{\"username\":\"" + username + "\",\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"ips\":[\"" + string.Join(",",defaultIp) + "\"]}";
            var response = await client.RequestAsync(method: SendGridClient.Method.POST, urlPath: "subusers", requestBody: data);

            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                return true;
            }
            else
            {
                var r = response.Body.ReadAsStringAsync().Result;

                return false;
            }

        }

        public string getIPAddress()
        {
            string url = "ips";
            string data = "{\"limit\":\"" + 100 + "\"}";
            var response = client.RequestAsync(method: SendGridClient.Method.GET, urlPath: url, queryParams: data);
            response.Wait();
            if (response.IsCompleted)
            {

                return response.Result.Body.ReadAsStringAsync().Result;
            }
            else
            {
                return null;
            }

        }


    }
}