﻿using System;

namespace EM.Helpers
{
    public class AnInfo
    {
        public static String GetAnApiId()
        {
            return System.Configuration.ConfigurationManager.AppSettings["AnApiId"];
        }
        public static String GetAnApiTsKey()
        {
            return System.Configuration.ConfigurationManager.AppSettings["AnApiTsKey"];
        }
 
    }
}