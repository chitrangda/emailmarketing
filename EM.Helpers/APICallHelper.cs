﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;

namespace EM.Helpers
{
    public class APICallHelper
    {
        public HttpResponseMessage callWebApi(string url, string stringData)
        {
            using (var client = new HttpClient())
            {
                var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync(url, contentData).Result;
                return response;

            }
        }
        public HttpResponseMessage Get(string url,string id)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(url+"/"+id).Result;
                return response;

            }
        }
        public HttpResponseMessage Get(string url)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(url).Result;
                return response;

            }
        }


        public HttpResponseMessage Put(string url,string data)
        {
            try
            {        
                using (var client = new HttpClient())
                {
                    try
                    {
                        var contentData = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PutAsync(url, contentData).Result;
                        return response;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                

                }
            }
            catch (Exception) 
            {
                throw;
            }
        }

        public HttpResponseMessage Put(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        HttpResponseMessage response = client.PutAsync(url,null).Result;
                        return response;
                    }
                    catch (Exception)
                    {
                        throw;
                    }


                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public HttpResponseMessage Post(string url, string data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        var contentData = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage response = client.PostAsync(url, contentData).Result;
                        return response;
                    }
                    catch (Exception)
                    {
                        throw;
                    }


                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public HttpResponseMessage Delete(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        HttpResponseMessage response = client.DeleteAsync(url).Result;
                        return response;
                    }
                    catch (Exception)
                    {
                        throw;
                    }


                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public HttpResponseMessage Delete(string url,int id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        HttpResponseMessage response = client.DeleteAsync(url+"/"+id).Result;
                        return response;
                    }
                    catch (Exception)
                    {
                        throw;
                    }


                }
            }
            catch (Exception)
            {
                throw;
            }
        }



    }
}