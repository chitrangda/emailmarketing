using System;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Net.Configuration;
using System.Threading.Tasks;

namespace EM.Helpers
{
    public class Mail
    {
        /// <summary>
        /// This method is responsible for sending mail.
        /// </summary>
        /// <param name="toEmail"></param>
        /// <param name="queryString"></param>
        /// <returns></returns>
        public async Task SendMail(string toEmail, string subject, string mailbody, string fromName, string SMTPEmailSettings, string toName, string bcc = null)
        {


            var smtpSection = (SmtpSection)ConfigurationManager.GetSection(SMTPEmailSettings);

            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage();
            message.Subject = subject;
            if (toEmail.Contains(","))
            {
                string[] recipents = toEmail.Split(',');
                if (recipents.Length > 0)
                {
                    foreach (string rec in recipents)
                    {
                        message.To.Add(new MailAddress(rec));

                    }
                }
            }
            else
            {
                message.To.Add(new MailAddress(toEmail, toName));
            }
            if (bcc != null)
            {
                message.Bcc.Add(new MailAddress(bcc));
            }
            message.From = new MailAddress(smtpSection.From, fromName);
            message.Body = mailbody;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;


            client.Host = smtpSection.Network.Host; //---- client Host Details. 
            client.EnableSsl = smtpSection.Network.EnableSsl; //---- Specify whether host accepts SSL Connections or not.
            NetworkCredential NetworkCred = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
            client.Credentials = NetworkCred;
            client.Port = smtpSection.Network.Port; //---- client Server port number. This varies from host to host. 
            client.EnableSsl = true;
            try
            {
                await client.SendMailAsync(message);
                //return true;

            }
            catch (Exception ex)
            {
                string m = ex.Message;
                //return false;
            }


        }

        public static bool SendMailCC(string toEmail, string ccEmail, string subject, string mailbody)
        {

            try
            {
                var smtpSection = (SmtpSection)ConfigurationManager.GetSection(Utilities.SMTPEmailSettings);

                SmtpClient client = new SmtpClient();
                MailMessage message = new MailMessage();
                message.Subject = subject;
                if (toEmail.Contains(","))
                {
                    string[] recipents = toEmail.Split(',');
                    if (recipents.Length > 0)
                    {
                        foreach (string rec in recipents)
                        {
                            message.To.Add(new MailAddress(rec));

                        }
                    }
                }
                else
                {
                    message.To.Add(new MailAddress(toEmail));
                }

                message.CC.Add(new MailAddress(ccEmail));

                message.From = new MailAddress(smtpSection.From);
                message.Body = mailbody;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                client.Host = smtpSection.Network.Host; //---- client Host Details. 
                client.EnableSsl = smtpSection.Network.EnableSsl; //---- Specify whether host accepts SSL Connections or not.
                NetworkCredential NetworkCred = new NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                client.Credentials = NetworkCred;
                client.Port = smtpSection.Network.Port; //---- client Server port number. This varies from host to host. 
                client.EnableSsl = true;
                client.Send(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
