﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace EM.Helpers
{
    public class Utilities
    {
        bool invalid = false;

        public static string SMTPEmailSettings = "system.net/mailSettings/smtp";
        public bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
        public static bool IsValidSqlDate(string date)
        {
            bool valid = false;
            DateTime testDate = DateTime.MinValue;
            System.Data.SqlTypes.SqlDateTime sdt;
            if (DateTime.TryParse(date, out testDate))
            {
                try
                {
                    sdt = new System.Data.SqlTypes.SqlDateTime(testDate);
                    valid = true;
                }
                catch (System.Data.SqlTypes.SqlTypeException)
                {
                    valid = false;
                }
            }

            return valid;
        }
        public static String StripText(Object text, Int32 numOfCharacters)
        {
            if (text is String)
            {
                return StripText(text.ToString(), numOfCharacters);
            }
            return String.Empty;
        }
        public static Int32 GetRandomNumber()
        {
            Random r = new Random();
            return r.Next(10000, 99999);
        }
        public static bool IsValidPassword(string pass)
        {
            if (string.IsNullOrEmpty(pass))
                return false;
            var r = new Regex(@"^(?=.*\d)(?=.*[A-Z]).*$");
            return r.IsMatch(pass);

        }
        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
                return false;
            //var r = new Regex(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}");
            //^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$
            var r = new Regex(@"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$");
            return r.IsMatch(phoneNumber);

        }
        public static double RoundToTwoDecimalPlaces(string value)
        {
            double val = Convert.ToDouble(value);
            return Math.Round(val, 2);
        }

        public static Guid CreateNewGuid()
        {
            Guid myNewID = Guid.NewGuid();

            return myNewID;
        }

        public static Double GetRoundedValue(Double value)
        {
            return Math.Round(value, 2);
        }

        public static String ToTitleCase(String textToConvert)
        {
            if (String.IsNullOrEmpty(textToConvert))
                return String.Empty;

            //Converting it to lowercase first otherwise it won't work with upppercase characters.
            return CultureInfo.GetCultureInfo("en-us").TextInfo.ToTitleCase(textToConvert.ToLower());
        }
        public static String ToUpperCase(String textToConvert)
        {
            if (String.IsNullOrEmpty(textToConvert))
                return String.Empty;

            //Converting it to lowercase first otherwise it won't work with upppercase characters.
            return CultureInfo.GetCultureInfo("en-us").TextInfo.ToUpper(textToConvert.ToLower());
        }
        public static String ToLowerCase(String textToConvert)
        {
            if (String.IsNullOrEmpty(textToConvert))
                return String.Empty;

            //Converting it to lowercase first otherwise it won't work with upppercase characters.
            return CultureInfo.GetCultureInfo("en-us").TextInfo.ToLower(textToConvert.ToLower());
        }
        public static String GetQueryString(String PageUrl, String StrToExtract)
        {
            String strQValue = String.Empty;
            String[] arrVals;

            if (PageUrl.IndexOf(StrToExtract) == -1)
            {
                strQValue = String.Empty;
            }
            else
            {
                arrVals = PageUrl.Split('&');
                foreach (String QueryStringVal in arrVals)
                {
                    if (QueryStringVal.IndexOf(StrToExtract) != -1)
                    {
                        Int32 LastIndex = QueryStringVal.LastIndexOf('=');
                        strQValue = QueryStringVal.Substring(LastIndex + 1, (QueryStringVal.Length - LastIndex - 1));
                    }
                }
            }

            return strQValue;
        }

        public static List<CCEXpiryYear> GetCreditCardExpiryYears()
        {
            List<CCEXpiryYear> lstExpirYears = new List<CCEXpiryYear>();
            int currentYear = DateTime.Now.Year;
            int upto = currentYear + 15;
            for (int i = currentYear; i <= upto; i++)
            {
                lstExpirYears.Add(new CCEXpiryYear() { Year = i, TwoDigitYear = Utilities.GetInt32(i.ToString().Substring(2, 2)) });
            }
            return lstExpirYears;

        }

        public static Int32 GetInt32(Object value)
        {
            Int32 intValue = 0;

            if (value != null && value != DBNull.Value)
            {
                intValue = Int32.Parse(value.ToString());
            }

            return intValue;
        }


        public static DataTable GetTableFromHTML(string htmlCode)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlCode);
            var headers = doc.DocumentNode.SelectNodes("//tr/th");
            DataTable table = new DataTable();
            foreach (HtmlNode header in headers)
                table.Columns.Add(header.InnerText.Trim().Replace("&nbsp;", "").Replace("&amp;", "&"));  // create columns from th
                                                                                                         // select rows with td elements 
            foreach (var row in doc.DocumentNode.SelectNodes("//tr[td]"))
                table.Rows.Add(row.SelectNodes("td").Select(td => td.InnerText.Trim().Replace("&nbsp;", "").Replace("&amp;", "&")).ToArray());
            return table;
        }

        public static string ConvertToDollarString(string value)
        {
            value = string.IsNullOrWhiteSpace(value) ? "0" : value;
            return "$ " + value;
        }

        public static string MaskCreditCardNo(string cardNumber)
        {
            //take first 6 characters
            string firstPart = cardNumber.Substring(0, 6);

            //take last 4 characters
            int len = cardNumber.Length;
            string lastPart = cardNumber.Substring(len - 4, 4);

            //take the middle part (XXXXXXXXX)
            int middlePartLenght = cardNumber.Substring(6, 5).Count();
            string middlePart = new String('X', 5);

            return firstPart + middlePart + lastPart;
        }

        public static string getCardType(string cardNumber)
        {

            if (Regex.Match(cardNumber, @"^4[0-9]{12}(?:[0-9]{3})?$").Success)
            {
                return CardType.Visa.ToString();
            }

            else if (Regex.Match(cardNumber, @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$").Success)
            {
                return CardType.MasterCard.ToString();
            }

            else if (Regex.Match(cardNumber, @"^3[47][0-9]{13}$").Success)
            {
                return CardType.AmericanExpress.ToString();
            }

            else if (Regex.Match(cardNumber, @"^6(?:011|5[0-9]{2})[0-9]{12}$").Success)
            {
                return CardType.Discover.ToString();
            }

            else if (Regex.Match(cardNumber, @"^(?:2131|1800|35\d{3})\d{11}$").Success)
            {
                return CardType.JCB.ToString();
            }
            else if (Regex.Match(cardNumber, @"^3(?:0[0-5]|[68][0-9])[0-9]{11}$").Success)
            {
                return CardType.DinersClub.ToString();
            }
            else
            {
                return null;
            }

        }

    }
    public enum CardType
    {
        MasterCard, Visa, AmericanExpress, Discover, JCB, DinersClub


    };
}