﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace EM.Helpers
{
    public static class CreateLog
    {
        public static void Log(string messages, Exception ex = null, string res = null)
        {
            if(!Directory.Exists(HttpContext.Current.Server.MapPath("~")+"/Logs/"))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "/Logs/");
            }
            var ServerPath = HttpContext.Current.Server.MapPath("~") + "/Logs/LogFile_" + DateTime.Now.ToString("ddMMyy") + ".txt";
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));

            message += Environment.NewLine;
            message += string.Format("Custom Message: {0}", messages);
            message += Environment.NewLine;
            if (ex != null)
            {
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Message: {0}", ex.Message);
                message += Environment.NewLine;
                message += string.Format("StackTrace: {0}", ex.StackTrace);
                message += Environment.NewLine;
                message += string.Format("Source: {0}", ex.Source);
                message += Environment.NewLine;
                message += string.Format("Source: {0}", ex.InnerException);
                message += Environment.NewLine;
            }
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            if (res != null)
            {
                message += string.Format("Response: {0}", res);
            }
            message += Environment.NewLine;
            using (StreamWriter writer = new StreamWriter(ServerPath, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

        public static void ActionLog(string messages)
        {
            var ServerPath = HttpContext.Current.Server.MapPath("~");
            var LogPath = "/UserUpload/logs/";
            if (!Directory.Exists(ServerPath + "/UserUpload"))
            {
                Directory.CreateDirectory(ServerPath + "/UserUpload");
            }
            if (!Directory.Exists(ServerPath + LogPath))
            {
                Directory.CreateDirectory(ServerPath + LogPath);
            }
            var Date = DateTime.Now.ToString("dd/MM/yyyy hh").Replace("/","").Replace(" ","");
            var fileName = string.Format("LogFile{0}.txt", Date);
            var fileNameWithServerPath = ServerPath + LogPath + fileName;
            WriteFile(fileNameWithServerPath, messages);
        }
        public static void WriteFile(string path,string content)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(content);
                    writer.Close();
                }
            }
            catch(Exception exception)
            {
                CreateLog.Log(exception.Message, exception);

            }
        }

        public static void SendGridResponseLog(string messages, string CampaignId,string page)
        {
            var ServerPath = HttpContext.Current.Server.MapPath("~");
            var LogPath = "/UserUpload/SendGridResponse/";
            if (!Directory.Exists(ServerPath + "/UserUpload"))
            {
                Directory.CreateDirectory(ServerPath + "/UserUpload");
            }
            if (!Directory.Exists(ServerPath + LogPath))
            {
                Directory.CreateDirectory(ServerPath + LogPath);
            }
            var fileName = string.Format("CampaignStat_{0}.txt", CampaignId+"_"+page);
            var fileNameWithServerPath = ServerPath + LogPath + fileName;
            WriteFile(fileNameWithServerPath, messages);
        }

        public static void ActionLogRL(string messages)
        {
            var ServerPath = HttpContext.Current.Server.MapPath("~");
            var LogPath = "/UserUpload/RegistrationLink/";
            if (!Directory.Exists(ServerPath + "/UserUpload"))
            {
                Directory.CreateDirectory(ServerPath + "/UserUpload");
            }
            if (!Directory.Exists(ServerPath + LogPath))
            {
                Directory.CreateDirectory(ServerPath + LogPath);
            }
            var Date = DateTime.Now.ToString("dd/MM/yyyy hh").Replace("/", "").Replace(" ", "");
            var fileName = string.Format("registrationlink{0}.txt", Date);
            var fileNameWithServerPath = ServerPath + LogPath + fileName;
            WriteFile(fileNameWithServerPath, messages);
        }

    }
}