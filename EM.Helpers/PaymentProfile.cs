﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;

namespace EM.Helpers
{
    public class PaymentProfile
    {
    }

    public class AnProfile
    {
        public AspNetUser User { get; set; }
        public UserPaymentProfile PayProfile { get; set; }
        public string CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string CCV { get; set; }
        public string AnProfileId { get; set; }
        public string CardType { get; set; }
        public string NameAsPerAccount { get; set; }
        public string NameOfBank { get; set; }
        public string TypeOfAccount { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
    }

    public class CCProfile
    {
        public String CardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string CCV { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
    }

    public class AnTranResponse
    {
        public string TransId { get; set; }
        public string ApprovalCode { get; set; }
        public string ResponseCode { get; set; }

    }

    public class AnTranStatusReponse
    {
        public string TransId { get; set; }
        public string TransactionType { get; set; }
        public string TransactionStatus { get; set; }
        public decimal AuthAmount { get; set; }
        public decimal SettleAmount { get; set; }
    }

    public class CCEXpiryYear
    {
        public int TwoDigitYear { get; set; }
        public int Year { get; set; }
    }
}