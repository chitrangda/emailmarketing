﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EM.WebAPI.Interfaces;
using Moq;
using EM.Factory;
using EM.Factory.ViewModels;
namespace CheapestEmail.Tests.WebApi
{
    [TestClass]
    public class UserTest
    {       
        [TestMethod]
        public void CheckDepartmentExistWithMoq()
        {
            //return object
            var vm = new ListViewModel();

            vm.ListId = 7;
            vm.ListName = "Testing";
            vm.FromEmail = "wendy@yopmail.com";
            vm.FromName = "Taylor John";
            vm.Remainder = null;
            vm.Company = "TRO";
            vm.Address1 = "Georgia";
            vm.Address2 = null;
            vm.City = "Atlanta";
            vm.Zip = "1548";
            vm.Country = "Australia";
            vm.Phone = "(346) 778-8999";
            vm.Optin = null;
            vm.GDPR = null;
            vm.Subscriber_Unsubscriber = null;
            vm.Subscriber = null;
            vm.Unsubscriber = null;

            //Create Fake Object
            var fakeObject = new Mock<IUserListService>();
            fakeObject.Setup(x => x.GetListById(It.IsAny<int>())).Returns(vm);

            //Call the method with hard code test.
            var Res = fakeObject.Object.GetListById(7);

            Assert.AreEqual(vm.FromEmail, Res.FromEmail);
        }
    }
}
