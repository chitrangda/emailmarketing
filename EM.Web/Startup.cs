﻿using EM.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System.Configuration;
using EM.Helpers;

[assembly: OwinStartupAttribute(typeof(EM.Web.Startup))]
namespace EM.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        private void createRolesandUsers()
        {
            // In Startup iam creating first Admin Role and creating a default Admin User    
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // first we create Admin rool
            if (!roleManager.RoleExists("Admin"))
            {                   
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
           }
            // creating Creating Manager role     
            if (!roleManager.RoleExists("User"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "User";
                roleManager.Create(role);
            }
            // creating Creating SalesRep role     
            if (!roleManager.RoleExists("SalesRep"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "SalesRep";
                roleManager.Create(role);
            }

            ////Here we create a Admin super user who will maintain the website                   

            var admin = new ApplicationUser();
            admin.UserName = ConfigurationManager.AppSettings["AdminEmail"];
            admin.Email = ConfigurationManager.AppSettings["AdminEmail"];

            string userPWD = Encryption64.decryptString(ConfigurationManager.AppSettings["AdminPassword"]);

            if (UserManager.FindByEmail(admin.Email) == null)
            {

                var chkUser = UserManager.Create(admin, userPWD);

                //Add default User to Role Admin
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(admin.Id, "Admin");

                }
            }

        }
    }
}
