﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Models
{
    public class ManageBillingModel
    {
        public usp_getManageBillingDetail_Result ManageBilling { get; set; }

        public SelectList MemberTypeList { get; set; }
        public SelectList SubscriberList { get; set; }
    }
}