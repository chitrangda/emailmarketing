﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Models
{
    public class AdminUpcomingBillingViewModel
    {
        public usp_getUpComingBillingDetails_Result BillingDetails { get; set; }

        public SelectList MemberTypeList { get; set; }

        public SelectList SubscriberList { get; set; }
    }
}