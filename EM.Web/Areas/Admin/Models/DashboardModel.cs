﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Areas.Admin.Models
{
    public class DashboardModel
    {
        public UserInfoViewModel currentUser { get; set; }

        public List<UserInfoViewModel> allUsers { get; set; }
    }
}