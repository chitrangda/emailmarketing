﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.Admin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UpcomingBillingController : Controller
    {
        // GET: Admin/UpcomingBilling
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public ActionResult getUpcomingBilling(DateTime? dateFrom, DateTime? dateTo)
        {

            String APIURL = string.Format("{0}/{1}?DateFrom={2}&DateTo={3}&pageIndex=0&pageSize=", ConfigurationManager.AppSettings["ApiURL"], "AdminUpcomingBilling", dateFrom, dateTo, 0);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = Json(JsonConvert.DeserializeObject<List<usp_getUpComingBillingDetails_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
                response.MaxJsonLength = Int32.MaxValue;
                return response;
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]
        public ActionResult GetUpcomingBillingDetails(string id)
        {
            AdminUpcomingBillingViewModel model = new AdminUpcomingBillingViewModel();
            String APIURL = string.Format("{0}/{1}?DateFrom={2}&DateTo={3}&pageIndex={4}&pageSize={5}&Id={6}", ConfigurationManager.AppSettings["ApiURL"], "AdminUpcomingBilling/GetDetails", null, null, null, 0, id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<usp_getUpComingBillingDetails_Result>(result.ToString());
            model.BillingDetails = response;
            model.MemberTypeList = DataHelper.GetPlanSelectList(response.membertype);
            model.SubscriberList = DataHelper.GetSubscriberList(model.BillingDetails.Subscriber, model.BillingDetails.PlanId);
            if (model.BillingDetails.monthlycredit == 0.ToString())
            {
                model.BillingDetails.monthlycredit = "Unlimited Emails";
            }
            return PartialView("EditUpcomingBilling", model);

        }

        [HttpPost]
        public ActionResult EditUpcomingBilling(AdminUpcomingBillingViewModel model)
        {
            String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "AdminUpcomingBilling");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.BillingDetails)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.Success,
                    msg = "Updated successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
