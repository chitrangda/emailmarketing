﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Web.Mvc;
using EM.Factory;
using EM.Helpers;
using System.Collections.Generic;
using System.Linq;
using EM.Factory.ViewModels;
using EM.Web.Areas.Admin.Models;
using System.Globalization;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class DashboardController : Controller
    {
        // GET: Admin/Data
        public ActionResult Index()
        {
            usp_getAdminTodays_Result model = new usp_getAdminTodays_Result();
            AdminTodayViewModel data = new AdminTodayViewModel();
            String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model = JsonConvert.DeserializeObject<usp_getAdminTodays_Result>(result.ToString());
                data.BillDeclined = model.BillDeclined;
                data.ClientBilling = model.ClientBilling;
                data.TotalClient = model.TotalClient;
                data.BillDue = model.BillDue;
                data.PacakagePurchase = model.PacakagePurchase;
                data.PackageDeclined = model.PackageDeclined;
                data.AllClients = model.AllClients;
                data.ClientTypeList = DataHelper.GetClientTypeSelectList(data.ClientType.ToString());
                data.CampaignList = DataHelper.GetCampaignSelectList(data.CampaignType.ToString());
                data.MemberTypeList = DataHelper.GetPlanSelectList(data.MemberType.ToString());

            }
            return View(data);
        }

        [HttpPost]
        public JsonResult getAdminData(string key, string value, string clientType)
        {
            try
            {
                System.Text.StringBuilder APIURL = new System.Text.StringBuilder();
                switch (key)
                {
                    case "Name":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", value, null, null, null, null, null, null, null, null, null, clientType));
                        break;
                    case "Email":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, value, null, null, null, null, null, null, null, null, clientType));
                        break;
                    case "MobileNumber":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, value, null, null, null, null, null, null, null, clientType));
                        break;
                    case "NameOnCard":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, value, null, null, null, null, null, null, clientType));
                        break;
                    case "PackageType":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, value, null, null, null, null, null, clientType));
                        break;
                    case "ReferralCode":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, null, value, null, null, null, null, clientType));
                        break;
                    case "City":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, null, null, value, null, null, null, clientType));
                        break;
                    case "State":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, null, null, null, value, null, null, clientType));
                        break;
                    case "SalesRep":
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, null, null, null, null, value, null, clientType));
                        break;
                    case "MemberType":
                        value = value == "Select" ? null : value;
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, null, null, null, null, null, value, clientType));
                        break;
                    default:
                        APIURL.Append(string.Format("{0}/{1}?Name={2}&Email={3}&MobileNumber={4}&NameOnCard={5}&MumberType={6}&ReferralCode={7}&City={8}&State={9}&SalesRep={10}&MemberType={11}&ClientType={12}", ConfigurationManager.AppSettings["ApiURL"], "AdminDashboard/GetGridData", null, null, null, null, null, null, null, null, null, null, null));
                        break;
                }
                var result = (new APICallHelper()).Get(APIURL.ToString()).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getAdminDashboardData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult GetEditClientView(string id)
        {
           
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "SalesRepData/GetClientData", id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<usp_getUserDetails_Result>(result.ToString());
            AdminDashboardModel model = new AdminDashboardModel();
            model.ClientDetails = data;
            model.ClientTypeList = DataHelper.GetClientTypeSelectList(data.ClientType);
            model.CampaignList = DataHelper.GetCampaignSelectList(data.CampaignTypeId.ToString());
            model.SalesRepList = DataHelper.GetSalesRep(data.SalesRepId!=null?data.SalesRepId:null);
            model.PlanList = DataHelper.GetPlanSelectList(data.PlanId.ToString());
            model.TimeZone = DataHelper.GetTimeZone(model.ClientDetails.TimeZoneName);
            model.IPPoolList = DataHelper.GetIPPoolList(data.IPPoolId.ToString());
            model.SubscriberList = DataHelper.GetSubscriberList(model.ClientDetails.NoOfSubscribersMax, model.ClientDetails.PlanId);
            if(model.ClientDetails.MonthlyCredit == 0.ToString())
            {
                model.ClientDetails.MonthlyCredit = "Unlimited Emails";
            }
            return PartialView("EditClientView", model);

        }
        [HttpPost]
        public JsonResult UpdateUserProfile(AdminDashboardModel userAdminDataViewModel)
        {
            try
            {
                var context = new EmailMarketingDbEntities();
                var userPro = context.UserProfiles.Where(x => x.Id == userAdminDataViewModel.ClientDetails.Id).FirstOrDefault();
                var userPlan = context.UserPlans.Where(x => x.UserId == userAdminDataViewModel.ClientDetails.Id && x.IsActive == true).FirstOrDefault();
                //var notes = context.SalesPortalRepNotes.Where(x => x.UserId == userAdminDataViewModel.UserId).OrderByDescending(r => r.LastModDate).ThenByDescending(r => r.CreatedDate).FirstOrDefault();
                if (userPro != null)
                {
                    if (userAdminDataViewModel.ClientDetails.SalesRepId != null && (userAdminDataViewModel.ClientDetails.SalesRepId != userPro.SalesRepId || userPro.SalesRepId == null))
                    {
                        string oldSalesRepId = userPro.SalesRepId;
                        var oldSlaesRepDetails = context.SalesRepProfiles.Find(oldSalesRepId);
                        if (oldSlaesRepDetails != null)
                        {
                            oldSlaesRepDetails.NoOfClients = oldSlaesRepDetails.NoOfClients - 1;
                        }
                        var newSlaesRepDetails = context.SalesRepProfiles.Find(userAdminDataViewModel.ClientDetails.SalesRepId);
                        if (newSlaesRepDetails != null)
                        {
                            newSlaesRepDetails.NoOfClients = newSlaesRepDetails.NoOfClients + 1;
                        }

                        userPro.SalesRepId = userAdminDataViewModel.ClientDetails.SalesRepId;

                    }
                    userPro.IsActive = userAdminDataViewModel.ClientDetails.IsActive;
                    userPro.ClientType = userAdminDataViewModel.ClientDetails.ClientType;
                   // userPro.CampaignTypeId = userAdminDataViewModel.ClientDetails.CampaignTypeId;
                    userPro.IsCSVActive = userAdminDataViewModel.ClientDetails.IsCSVActive;
                    userPro.EmailSending = userAdminDataViewModel.ClientDetails.EmailSending;

                    if (userAdminDataViewModel.ClientDetails.IPPoolId != null)
                    {
                        userPro.IPPoolId = Convert.ToInt32(userAdminDataViewModel.ClientDetails.IPPoolId);
                    }
                    else
                    {
                        userPro.IPPoolId = null;
                    }
                    userPro.PromotionCode = userAdminDataViewModel.ClientDetails.PromotionCode;
                    if (userAdminDataViewModel.ClientDetails.FollowUpDate != null)
                    {
                        DateTime t;
                        DateTime.TryParse(userAdminDataViewModel.ClientDetails.FollowUpDate, out t);
                        userPro.FollowupDate = t;
                        userPro.FollowupTime = t;

                    }
                    userPro.TimeZoneId = Convert.ToInt32(userAdminDataViewModel.ClientDetails.TimeZoneID);


                }
                if (userPlan != null)
                {
                    if (userAdminDataViewModel.ClientDetails.PackageId != userPlan.PackageId)
                    {
                        //var packageDetails = context.Packages.Where(r => r.PlanId == userAdminDataViewModel.ClientDetails.PlanId).FirstOrDefault();

                        DataHelper.CreateUserPlan(userAdminDataViewModel.ClientDetails.PackageId, userAdminDataViewModel.ClientDetails.Id);

                    }
                    userPlan = context.UserPlans.Where(x => x.UserId == userAdminDataViewModel.ClientDetails.Id && x.IsActive == true).FirstOrDefault();
                    if (userAdminDataViewModel.ClientDetails.NextPackageUpdateDate != null)
                    {
                        userPlan.NextPackageUpdateDate = userAdminDataViewModel.ClientDetails.NextPackageUpdateDate;
                    } 
                    if(userAdminDataViewModel.ClientDetails.MonthlyCredit == "Unlimited Emails")
                    {
                        var credits = 0;
                        userPlan.MonthlyCredit = credits.ToString();
                        userPlan.TotalEmails = credits;
                        userPlan.totalRemainingCredits = credits.ToString();
                    }
                    else
                    {
                        userPlan.MonthlyCredit = userAdminDataViewModel.ClientDetails.MonthlyCredit;
                        userPlan.TotalEmails = Convert.ToInt32(userAdminDataViewModel.ClientDetails.MonthlyCredit);
                        userPlan.totalRemainingCredits = Convert.ToString(userAdminDataViewModel.ClientDetails.MonthlyCredit);
                    }
                  
                    userPlan.BillingAmount = userAdminDataViewModel.ClientDetails.BillingAmount;
                    
                    
                    if (userAdminDataViewModel.ClientDetails.PackageUpdateDate != null)
                    {
                        userPlan.PackageUpdateDate = Convert.ToDateTime(userAdminDataViewModel.ClientDetails.PackageUpdateDate);

                    }
                }
                context.SaveChanges();

                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Updated successfully.",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                CreateLog.Log(exception.Message, exception);
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //    if (userPro is null && userPlan is null && notes is null)
        //{
        //    return this.Json(new
        //    {
        //        status = Constants.ErrorTitle,
        //        msg = ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
        //    }, JsonRequestBehavior.AllowGet);
        //}
        //else
        //{
        //    userPro.FirstName = userAdminDataViewModel.FirstName;
        //    userPro.LastName = userAdminDataViewModel.LastName;
        //    userPro.IsActive = userAdminDataViewModel.IsActive;
        //    userPro.SalesRepId = userAdminDataViewModel.SalesRepId;
        //    userPro.ClientType = userAdminDataViewModel.ClientType;
        //    userPro.CampaignTypeId = userAdminDataViewModel.CampaignTypeId;
        //    context.SaveChanges();
        //    userPlan.MonthlyCredit = userAdminDataViewModel.MonthlyCredit;
        //    userPlan.CustomizableTemplates = userAdminDataViewModel.CustomizableTemplates;
        //    userPlan.NextPackageUpdateDate = userAdminDataViewModel.NextPackageUpdateDate is null ? DateTime.UtcNow : userAdminDataViewModel.NextPackageUpdateDate;
        //    context.SaveChanges();
        //    var salesRepProfile = context.SalesRepProfiles.Where(x => x.Id == userAdminDataViewModel.SalesRepId).FirstOrDefault();
        //    var oldSalesRepProfile = context.SalesRepProfiles.Where(x => x.Id == userAdminDataViewModel.OldSalesRepId).FirstOrDefault();
        //    if (salesRepProfile !=null && userAdminDataViewModel.OldSalesRepId != userAdminDataViewModel.SalesRepId)
        //    {
        //        salesRepProfile.NoOfClients = salesRepProfile.NoOfClients is null ? 1 : Convert.ToInt32(salesRepProfile.NoOfClients) + 1;
        //        context.SaveChanges();
        //        if (oldSalesRepProfile !=null)
        //        {
        //            oldSalesRepProfile.NoOfClients = oldSalesRepProfile.NoOfClients is null ? 0 : Convert.ToInt32(oldSalesRepProfile.NoOfClients) - 1;
        //            context.SaveChanges();
        //        }
        //    }
        //    var userPackage = context.Packages.Where(x => x.PlanId == userAdminDataViewModel.PlanTypeId && x.TotalMonth == userAdminDataViewModel.PlanDuration).FirstOrDefault();
        //    if (userPackage !=null)
        //    {
        //        userPlan.PackageId = userPackage.Id;
        //        context.SaveChanges();
        //    }
        //    return this.Json(new
        //    {
        //        status = Constants.SuccessTitle,
        //        msg = "Saved successfully.",
        //    }, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult CampaignReport(string id, string startDate, string endDate)
        {
            var response = (new User.Controllers.ReportController()).GetCampaignList(Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), 1);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAdminUserLists()
        {
            AdminUserListsViewModel adminUserListsViewModel = new AdminUserListsViewModel();
            string APIURLSalesRep = string.Format("{0}/{1}?FirstName={2}&LastName={3}&NoOfClients={4}&SpentTotal={5}&ViewRepActivity={6}&CanExport{7}&ExportLead={8}&Manager={9}&IsActive={10}&pageIndex=0&pageSize=", ConfigurationManager.AppSettings["ApiURL"], "ManageSalesRep", null, null, null, null, null, null, null, null, null, 0);
            var salesRepresult = (new APICallHelper()).Get(APIURLSalesRep).Content.ReadAsStringAsync().Result;

            if (salesRepresult != null)
            {
                adminUserListsViewModel.SalesRepList = JsonConvert.DeserializeObject<List<usp_getAllSalesRep_Result>>(salesRepresult.ToString());

            }
            adminUserListsViewModel.CampaignList = DataHelper.GetCampaignList();
            adminUserListsViewModel.ClientTypesList = DataHelper.GetClientTypeList();
            adminUserListsViewModel.PlanList = DataHelper.GetPlanList();
            adminUserListsViewModel.TimeZoneList = DataHelper.GetTimeZoneList();
            adminUserListsViewModel.IPPoolList = DataHelper.GetPoolList();


            return Json(adminUserListsViewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetNoteView(string userid)
        {
            NoteViewModel model = new NoteViewModel();
            model.newNote = new SalesPortalRepNote() { UserId = userid };
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Notes", userid);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.noteList = JsonConvert.DeserializeObject<List<usp_getUserNoteList_Result>>(result.ToString());

            }
            model.TimeZoneList = DataHelper.GetTimeZone(model.newNote.TimeZone);
            return PartialView("AddEditNotes", model);
        }
        [HttpPost]
        public ActionResult AddNote(NoteViewModel model)
        {

            var UserId = User.Identity.GetUserId();
            model.newNote.LastModById = UserId;
            model.newNote.CreatedById = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Notes");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.newNote)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.newNote.NoteId == 0 ? "Note has been successfully created" : "Note has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNote(int noteId)
        {
            string APIURL = string.Format("{0}/{1}?id={2}&userid={3}", Constants.ApiURL, "Notes", noteId, User.Identity.GetUserId());
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Note has been successfully deleted",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
    public class AdminUserListsViewModel
    {
        public List<usp_getAllSalesRep_Result> SalesRepList { get; set; }
        public List<ClientType> ClientTypesList { get; set; }
        public List<Plan> PlanList { get; set; }//as Member 
        public List<CampaignList> CampaignList { get; set; }

        public List<Factory.TimeZone> TimeZoneList { get; set; }
        
        public List<IP_Pool> IPPoolList { get; set; }

    }

}