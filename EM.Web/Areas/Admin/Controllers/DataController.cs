﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Web.Mvc;
using EM.Factory;
using EM.Helpers;
using System.Collections.Generic;
using System.Linq;
using EM.Factory.ViewModels;
using EM.Web.Areas.Admin.Models;
using EM.Web.Utilities;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class DataController : Controller
    {
        // GET: Admin/Data
        public ActionResult Index()
        {
            ViewData["ddlStates"] = DataHelper.GetStateList(null);
            ViewData["ddlPackage"] = DataHelper.GetPlanSelectList(null);
            ViewData["ddlTimeZone"] = DataHelper.GetTimeZone(null);
            ViewData["ddlMonths"] = DataHelper.GetMonths(1);
            ViewData["ddlYears"] = DataHelper.GetCreditCardYear(1);
            ViewBag.CampaignList = DataHelper.GetCampaignSelectList(null);
            return View();
        }

        [HttpPost]
        public JsonResult GetData()
        {
            try
            {
                String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "AdminData");
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getAdminData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult SearchData(DateTime? FromDate, DateTime? ToDate, int? Year, string Quarter, int? Month, string City, string State, string Zip, string TimeZone, string Find, int? MemberType, bool? Status, int? CampaignType)
        {
            try
            {
                string APIURL = string.Format("{0}/{1}?&FromDate={2}&ToDate={3}&Year={4}&Quarter={5}&Month={6}&City={7}&State={8}&Zip={9}&TimeZone={10}&Find={11}&MemberType={12}&Status={13}&CampaignType={14}", Constants.ApiURL, "AdminData", FromDate, ToDate, Year, Quarter, Month, City, State, Zip, TimeZone, Find, MemberType, Status, CampaignType);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getAdminData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);

            }
        }
    }
}