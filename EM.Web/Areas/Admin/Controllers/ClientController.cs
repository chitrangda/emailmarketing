﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.Admin.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ClientController : Controller
    {
        // GET: Admin/Client
        public ActionResult Index(ManageMessage? message)
        {
            DashboardModel model = new DashboardModel();
            ViewBag.StatusMessage =
               message == ManageMessage.ChangePasswordSuccess ? "Password changed successfully."
               : message == ManageMessage.Error ? "An error has occurred."
               : "";
            String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "UserInfo");
            var result = (new APICallHelper()).Get(APIURL, User.Identity.GetUserId()).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<UserInfoViewModel>(result.ToString());
                model.currentUser = data;
                Session["AdminDetails"] = data;
                //ViewBag.Name = data.FirstName + " " + data.LastName;

            }
            return View(model);
        }

        [HttpPost]
        public JsonResult getAllUsers(string FirstName, string LastName, string MobilePhone, string City, string State, string Country, string Zip, bool? isActive)
        {
            try
            {
                String APIURL = string.Format("{0}/{1}?FirstName={2}&LastName={3}&MobilePhone={4}&City={5}&State={6}&Country={7}&Zip={8}&isActive{9}&StartIndex={10}&PageSize=", ConfigurationManager.AppSettings["ApiURL"], "UserInfo", FirstName, LastName, MobilePhone, City, State, Country, Zip, isActive, 0);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var response = Json(JsonConvert.DeserializeObject<List<UserInfoViewModel>>(result.ToString()), JsonRequestBehavior.AllowGet);
                response.MaxJsonLength = Int32.MaxValue;
                return response;
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }

        [HttpPost]
        public JsonResult changeAccountStatus(string id, bool status)
        {
            try
            {
                String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "UserInfo?id=" + id + "&status=" + status);
                var result = (new APICallHelper()).Put(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = "updated",
                        msg = "updated"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.NotFoundTitle,
                        msg = "not found"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult getCountyList()
        {
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "Country/GetCountry");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var countryViewModel = JsonConvert.DeserializeObject<List<CountryViewModel>>(count.ToString());
                return Json(countryViewModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);

            }

        }
        [HttpGet]
        public JsonResult GetSaleRepList()
        {
            string APIURLSalesRep = string.Format("{0}/{1}?FirstName={2}&LastName={3}&NoOfClients={4}&SpentTotal={5}&ViewRepActivity={6}&CanExport{7}&ExportLead={8}&Manager={9}&IsActive={10}&pageIndex=0&pageSize=", ConfigurationManager.AppSettings["ApiURL"], "ManageSalesRep", null, null, null, null, null, null, null, null, null, 0);
            var salesRepresult = (new APICallHelper()).Get(APIURLSalesRep).Content.ReadAsStringAsync().Result;

            if (salesRepresult != null)
            {
                var result = JsonConvert.DeserializeObject<List<usp_getAllSalesRep_Result>>(salesRepresult.ToString());
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpGet]
        [ActionName("UpdateUserDetails")]
        public PartialViewResult UpdateUserDetailsGet(UserInfoViewModel userInfoViewModel)
        {
            TempData["Country"] = DataHelper.GetCountryList(userInfoViewModel.Country);
            TempData.Keep("Country");
            return PartialView("EditClient", userInfoViewModel);
        }
        [HttpPost]
        public JsonResult UpdateUserDetails(UserInfoViewModel userInfoViewModel)
        {
            //userInfoViewModel.FirstName = FirstName;
            //userInfoViewModel.LastName = LastName;
            //userInfoViewModel.MobilePhone = MobilePhone;
            //userInfoViewModel.City = City;
            //userInfoViewModel.State = State;
            //userInfoViewModel.Country = Country;
            //userInfoViewModel.Zip = Zip;
            //userInfoViewModel.isActive = isActive;
            //userInfoViewModel.Id = id;
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserInfo");
            var result = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(userInfoViewModel)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = "",
                    msg = "User details updated successfully.",
                    data = userInfoViewModel
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage

                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ExportList()
        {
            String APIURL = string.Format("{0}/{1}?FirstName=&LastName=&MobilePhone=&City=&State=&Country=&Zip=&isActive=&StartIndex={2}&PageSize=", ConfigurationManager.AppSettings["ApiURL"], "UserInfo", 0);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var users = JsonConvert.DeserializeObject<List<UserInfoViewModel>>(result.ToString()).Select(r => new { r.FirstName, r.LastName, r.MobilePhone, r.City, r.State, r.Country, r.Zip, r.isActive });

            DataTable dt = users.ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Users");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Users.xlsx");
                }
            }
        }

        public ActionResult createNewAccount(int? Package)
        {
            UserInfoViewModel model = new UserInfoViewModel();
            if (Package == null)
            {
                Package = Convert.ToInt32(ConfigurationManager.AppSettings["FreePackageId"]);
            }
            String APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", Package);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var planDetail = JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(result.ToString());
                model.PackageBillingAmount = planDetail.BillingAmount;
                model.SelectedPackakgeId = Package;
            }

            TempData["Country"] = DataHelper.GetCountryList(Constants.DefaultState);
            TempData.Keep("Country");
            TempData["States"] = DataHelper.GetStateList(null);
            TempData.Keep("States");
            var selectedIndustry = model.IndustryRefId == null ? "" : model.IndustryRefId.ToString();
            TempData["IndustryList"] = DataHelper.GetIndustryList(selectedIndustry);
            TempData.Keep("IndustryList");

            return PartialView("CreateNewAccount", model);
        }

        [HttpPost]
        public async Task<ActionResult> createNewAccount(UserInfoViewModel model)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var user = new ApplicationUser();
            user.UserName = model.Email;
            user.Email = model.Email;

            string userPWD = model.Password;

            if (UserManager.FindByEmail(user.Email) == null)
            {

                var chkUser = UserManager.Create(user, userPWD);


                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "User");

                    using (var db = new EmailMarketingDbEntities())
                    {
                        UserProfile oUser = new UserProfile();
                        oUser.Id = user.Id;
                        oUser.FirstName = model.FirstName;
                        oUser.LastName = model.LastName;
                        oUser.ContactEmail = user.Email;
                        oUser.City = model.City;
                        oUser.State = model.State;
                        oUser.Country = model.Country;
                        oUser.Zip = model.Zip;
                        oUser.MobilePhone = model.MobilePhone;
                        oUser.RegisteredDate = DateTime.Now;
                        oUser.IndustryRefId = model.IndustryRefId;
                        oUser.CompanyName = model.CompanyName;
                        oUser.Address1 = model.Address1;
                        oUser.IsActive = true;
                        oUser.IsCSVActive = true;
                        oUser.TimeZoneId = 3;
                        /*If user's email domain is in whitelabel then sendingfromemail will be same Email and if its different then it will add root domain to that email doamin*/
                        //var domain = user.Email.Split('@')[1];
                        //string emailDomain = "";
                        //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                        //{
                        //    model.CompanyName = await MailgunHelper.ReplaceSpecialCharWithBlank(model.CompanyName);
                        //    oUser.SendingFromEmail = await MailgunHelper.ReturnDomainforWhitelabelDomain(model.FirstName.ToLower(), model.CompanyName.ToLower());
                        //    emailDomain = oUser.SendingFromEmail.ToLower();
                        //}
                        //else
                        //{
                        //    oUser.SendingFromEmail = await MailgunHelper.ReturnMailWithDomain(model.Email);
                        //    emailDomain = oUser.SendingFromEmail.ToLower();
                        //}
                       // model.CompanyName = await MailgunHelper.ReplaceSpecialCharWithBlank(model.CompanyName);

                        //create user sending from emal as per mailgun domain
                        oUser.SendingFromEmail = await MailgunHelper.AddSubDomaininEmailWithLower(model.Email, model.CompanyName);
                        db.UserProfiles.Add(oUser);
                        db.SaveChanges();


                        String APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", model.SelectedPackakgeId);
                        var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                        if (apiResult != null)
                        {
                            var planDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(apiResult.ToString());
                            DataHelper.CreateUserPlan(model.SelectedPackakgeId, oUser.Id);
                        }
                        EmailConfirmation oEmail = db.EmailConfirmations.SingleOrDefault(r => r.UserId == user.Id && r.FromEmailAddress == user.Email);
                        if (oEmail == null)
                        {
                            oEmail = new EmailConfirmation();
                            oEmail.UserId = user.Id;
                            oEmail.FromEmailAddress = user.Email;
                            oEmail.Status = true;
                            oEmail.SendingFromEmail = oUser.SendingFromEmail.ToLower();
                            db.EmailConfirmations.Add(oEmail);
                            db.SaveChanges();
                        }
                        //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                        //{
                        //    return this.Json(new
                        //    {
                        //        status = "",
                        //        msg = "Account Created Successfully!"
                        //    }, JsonRequestBehavior.AllowGet);
                        //}
                        //else
                        //{
                            //var response = await verfiyDomainMailGun(emailDomain);
                            //if (response == true)
                            //{
                            //    /*To create route so that email should forwarded on original user email*/
                            //    //var res = await MailgunHelper.CreateRoute(emailDomain);
                            //    var webhook = await MailgunHelper.InsertWebHookLinks(emailDomain);
                            //var domainVerify = emailDomain.Split('@')[1];
                            //var domainSetting = await MailgunHelper.verifyDomainSettings(domainVerify);
                                return this.Json(new
                                {
                                    status = "",
                                    msg = "Account Created Successfully!"
                                }, JsonRequestBehavior.AllowGet);
                            }
                            //else
                            //{
                            //    return this.Json(new
                            //    {
                            //        status = Constants.ErrorTitle,
                            //        msg = Constants.ErrorMessage
                            //    }, JsonRequestBehavior.AllowGet);
                            //}
                    }
              //  }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Email Id already exists!"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<bool> verfiyDomainMailGun(string email)
        {
            /*To check whether domain is verified or not*/
            var check = await MailgunHelper.IsVerifiedDomainMailGun(email);
            if (check == 0)
            {
                /*To create domain if its not created*/
                var response = await MailgunHelper.createDomainMailGun(email);

            }
            return true;
        }


    }
}

