﻿using EM.Factory;
using EM.Helpers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PromotionCodeController : Controller
    {
        // GET: Admin/PromotionCode
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEditPromotion(PromotionCode model)
        {

            var UserId = User.Identity.GetUserId();
            model.CreatedById = UserId;
            model.LastModById = UserId;
            var promotion = DataHelper.CheckDuplicatePromotion(model.PromotionCodeName);
            if (promotion == true)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = " Promotion Code  Already Exists",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "PromotionCode");
                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = model.PromotionCodeId == 0 ? "Promotion Code has been successfully created" : "Promotion Code has been successfully updated",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }

            }
        }
        public ActionResult getPromotionList()
        {

            String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "PromotionCode");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = Json(JsonConvert.DeserializeObject<List<PromotionCode>>(result.ToString()), JsonRequestBehavior.AllowGet);
                return response;
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]

        public ActionResult EditPromotion(int PromotionId)
        {
            PromotionCode model = new PromotionCode();
            String APIURL = string.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "PromotionCode", PromotionId);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<PromotionCode>(result.ToString());
                model = response;

            }
            return PartialView("EditPromotionCode", model);

        }

        [HttpPost]
        public ActionResult DeletePromotion(int PromotionId)
        {
            var APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "PromotionCode", PromotionId);
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Promotion Code has been deleted successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}