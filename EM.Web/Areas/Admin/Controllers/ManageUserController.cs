﻿using EM.Factory.ViewModels;
using EM.Helpers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using EM.Factory;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using EM.Web.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using EM.Web.Areas.Admin.Models;
using System.Configuration;
using EM.Web.Utilities;
using EM.Web.Extentions;
using System.Data.SqlClient;
using NMIPayment;
using System.Threading.Tasks;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManageUserController : Controller
    {
        MailgunHelper _mailGun = new MailgunHelper();
        Mail oMail = new Mail();
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        // GET: Admin/ManageUser
        public ActionResult Index(string id)
        {
            TempData["Id"] = id;
            TempData.Keep("Id");
            return View();
        }

        public ActionResult CreateInvoice(string id)
        {
            InvoiceViewModel model = null;
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Invoice/GetNewInvoice", id);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                model = JsonConvert.DeserializeObject<InvoiceViewModel>(apiResult.ToString());
                model.UserId = id;
            }
            model.YearList = DataHelper.GetCreditCardYear(Convert.ToInt32(model.ExpiresYear));
            model.MonthList = DataHelper.GetMonths(Convert.ToInt32(model.ExpiresMonth));
            model.PaymnetTypeList = DataHelper.GetPaymentTypeList(model.PaymentTypeId.ToString());
            return PartialView("CreateInvoice", model);

        }

        [HttpPost]
        public JsonResult CreateInvoice(InvoiceViewModel model)
        {
            if (ModelState.IsValid)
            {
                //string transCode = EM.Helpers.Utilities.GetRandomNumber().ToString();
                string token = null;
                if (model.CardType == null)
                {
                    model.CardType = EM.Helpers.Utilities.getCardType(model.CardNumber);
                }
                using (Payment pp = new Payment())
                {
                    decimal amount = model.AmountCharged != null ? Convert.ToDecimal(model.AmountCharged) : 0;
                    PaymentInfo profile = new PaymentInfo();
                    profile.NavigateUrl = DataHelper.ApplicationUrl() + "/Admin/ManageUser/AdminConfirmPayment";
                    profile.CCNumber = model.CardNumber;
                    var ExpYear = model.ExpiresYear.ToString().Length == 2 ? model.ExpiresYear.ToString() : model.ExpiresYear.ToString().Substring(2, 2);
                    profile.CCEXp = Convert.ToString(model.ExpiresMonth + ExpYear);
                    if (profile.CCEXp.Length == 3)
                    {
                        profile.CCEXp = "0" + profile.CCEXp;
                    }
                    profile.CCV = model.VerificationNo;
                    profile.amount = amount.ToString();
                    profile.billingInfo = new BillingInfo();
                    profile.billingInfo.FirstName = model.NameOnCard.Split(' ')[0];
                    profile.billingInfo.LastName = model.NameOnCard.Split(' ')[1];
                    profile.billingInfo.Address1 = model.Address;
                    profile.billingInfo.City = model.City;
                    profile.billingInfo.State = model.State;
                    profile.billingInfo.Zip = model.Zip;
                    profile.billingInfo.Phone = model.Phone;
                    profile.billingInfo.Fax = model.Phone;
                    profile.billingInfo.Email = model.Email;
                    profile.orderInfo = new OrderInfo();
                    profile.orderInfo.OrderId = model.OrderID;
                    profile.orderInfo.OrderDesc = model.InvoiceType;
                    profile.orderInfo.TaxAmount = 0.00;
                    profile.orderInfo.Amount = Convert.ToDouble(amount);
                    profile.orderInfo.PoNumber = model.UserPaymentProfileId.ToString();
                    profile.orderInfo.CustomField2 = model.CardNumber;
                    profile.orderInfo.CustomField = model.UserId;
                    profile.orderInfo.CustomField3 = model.CardType;

                    //profile.TransCode = transCode;
                    profile.shippingInfo = new ShippingInfo();
                    profile.shippingInfo.FirstName = model.NameOnCard.Split(' ')[0];
                    profile.shippingInfo.LastName = model.NameOnCard.Split(' ')[1];

                    profile.shippingInfo.Address1 = model.Address;
                    profile.shippingInfo.City = model.City;
                    profile.shippingInfo.State = model.State;
                    profile.shippingInfo.Zip = model.Zip;
                    profile.shippingInfo.Phone = model.Phone;
                    profile.shippingInfo.Fax = model.Phone;
                    if (model.AnPaymentProfileId != null)
                    {
                        profile.CustomerVaultId = model.AnPaymentProfileId;
                        token = pp.Transaction(profile, Constants.PaymentProductionMode, false, "Admin Invoice");

                    }
                    else
                    {
                        profile.CustomerVaultId = model.UserId;
                        token = pp.Transaction(profile, Constants.PaymentProductionMode, true, "Admin Invoice");

                    }
                }
                if (token != null && DataHelper.GetTransactionStatus(token) != null && DataHelper.GetTransactionStatus(token) == "100")
                {
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Invoice has been successfully created",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.Error,
                        msg = "Payment Declined!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> AdminConfirmPayment()
        {
            ViewBag.TokenId = Request.QueryString["token-id"];
            PaymentStatus paymentStatus = new PaymentStatus();
            InvoiceViewModel model = new InvoiceViewModel();
            paymentStatus = Payment.getPaymentstatus(ViewBag.TokenId, Constants.PaymentProductionMode);
            if (paymentStatus != null)
            {
                paymentStatus.TransCreatedBy = "Admin Invoice";
                DataHelper.updateTransactionDetails(paymentStatus, ViewBag.TokenId);
                if (paymentStatus.result == "1")
                {
                    model.NameOnCard = paymentStatus.FirstName + " " + paymentStatus.LastName;
                    model.City = paymentStatus.City;
                    model.State = paymentStatus.State;
                    model.Zip = paymentStatus.Zip;
                    model.Phone = paymentStatus.Phone;
                    model.CardNumber = paymentStatus.CustomField2;
                    model.ExpiresMonth = Convert.ToInt32(paymentStatus.CCEXp.Substring(0, 2));
                    model.ExpiresYear = Convert.ToInt32(DateTime.Now.Year.ToString().Substring(0, 2) + Convert.ToInt32(paymentStatus.CCEXp.Substring(2, 2))); model.OrderID = paymentStatus.OrderId;
                    model.CreatedById = paymentStatus.CustomField;
                    model.UserId = paymentStatus.CustomField;
                    model.InvoiceType = paymentStatus.OrderDesc;
                    model.UserPaymentProfileId = Convert.ToInt32(paymentStatus.PoNumber);
                    model.AnPaymentProfileId = paymentStatus.customervaultid;
                    model.Email = paymentStatus.Email;
                    model.Address = paymentStatus.Address1;
                    //save user payment profile
                    if (Convert.ToInt32(paymentStatus.PoNumber) == 0)
                    {

                        String URL = string.Format("{0}/{1}", Constants.ApiURL, "Invoice");
                        var response = (new APICallHelper()).Post(URL, JsonConvert.SerializeObject(model)).Content.ReadAsStringAsync().Result;

                    }
                    //save  invoice
                    var invoiceDetails = DataHelper.SaveInvoiceDetails(paymentStatus);
                    //send invoice
                    //SendGridEmails sendGrid = new SendGridEmails();
                    string _creditCardNo = "XXXX-XXXX-XXXX-" + paymentStatus.CCNumber.Substring(paymentStatus.CCNumber.Length - 4);
                    //await _mailGun.sendSingleEmailInvoice(paymentStatus.CustomField,
                    //    paymentStatus.OrderDesc + " Invoice", EmailHtml.getInvoiceSend(paymentStatus.FirstName + " " + paymentStatus.LastName,
                    //    DateTime.Now.ToString("MM/dd/yyyy"), "The following Credit Card has been charged for " + paymentStatus.OrderDesc + " of amount ", paymentStatus.amount, paymentStatus.FirstName + " " + paymentStatus.LastName,
                    //    paymentStatus.Address1, paymentStatus.City, paymentStatus.State, paymentStatus.Zip, invoiceDetails.CardType, _creditCardNo,
                    //    paymentStatus.Phone, paymentStatus.transactionid, paymentStatus.OrderDesc, "", ""), Constants.SupportEmail, Constants.SupportName, paymentStatus.Email,
                    //    paymentStatus.FirstName + "" + paymentStatus.LastName);


                    await oMail.SendMail(paymentStatus.Email, paymentStatus.OrderDesc + " Invoice", EmailHtml.getInvoiceSend(paymentStatus.FirstName + " " + paymentStatus.LastName,
                        DateTime.Now.ToString("MM/dd/yyyy"), "The following Credit Card has been charged for " + paymentStatus.OrderDesc + " of amount ", paymentStatus.amount, paymentStatus.FirstName + " " + paymentStatus.LastName,
                        paymentStatus.Address1, paymentStatus.City, paymentStatus.State, paymentStatus.Zip, invoiceDetails.CardType, _creditCardNo,
                        paymentStatus.Phone, paymentStatus.transactionid, paymentStatus.OrderDesc, "", ""), Constants.BillingName, "mailSettings/Billing", (paymentStatus.FirstName + "" + paymentStatus.LastName), System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);



                }
                else
                {

                    var oTrans = DataHelper.GetTransactionInfo(ViewBag.TokenId);
                    // SendGridEmails sendGrid = new SendGridEmails();
                    string _creditCardNo = "XXXX-XXXX-XXXX-" + oTrans.CreditCardNo.Substring(oTrans.CreditCardNo.Length - 4);
                    //await _mailGun.sendSingleEmailInvoice(oTrans.ContactEmail,
                    //    oTrans.Message + " Invoice", EmailHtml.getInvoiceSend(oTrans.FirstName + " " + oTrans.LastName,
                    //    DateTime.Now.ToString("MM/dd/yyyy"), "The following Credit Card has not been charged for " + oTrans.Message + " of amount ", oTrans.Amount, oTrans.FirstName + " " + oTrans.LastName,
                    //    oTrans.Address1, oTrans.City, oTrans.State, oTrans.Zip, oTrans.CardType, _creditCardNo,
                    //    oTrans.MobilePhone, oTrans.AnTransactionId, oTrans.Message, "", ""), Constants.SupportEmail, Constants.SupportName, oTrans.ContactEmail,
                    //    oTrans.FirstName + "" + oTrans.LastName);

                    await oMail.SendMail(oTrans.ContactEmail, oTrans.Message + " Invoice", EmailHtml.getInvoiceSend(oTrans.FirstName + " " + oTrans.LastName,
                      DateTime.Now.ToString("MM/dd/yyyy"), "The following Credit Card has not been charged for " + oTrans.Message + " of amount ", oTrans.Amount, oTrans.FirstName + " " + oTrans.LastName,
                      oTrans.Address1, oTrans.City, oTrans.State, oTrans.Zip, oTrans.CardType, _creditCardNo,
                      oTrans.MobilePhone, oTrans.AnTransactionId, oTrans.Message, "", ""), Constants.BillingName, "mailSettings/Billing", (oTrans.FirstName + "" + oTrans.LastName), System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);


                }

            }
            return View();

        }


        [HttpPost]
        public ActionResult GetInvoices(string userid)
        {
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Invoice/GetUserInvoice", userid);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                return Json(JsonConvert.DeserializeObject<List<usp_getInvoice_Result>>(apiResult.ToString()), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetUserActivity(string Userid, DateTime? fromDate, DateTime? ToDate)
        {
            string APIURL = string.Format("{0}/{1}?Userid={2}&fromDate={3}&ToDate{4}", Constants.ApiURL, "ManageUser/GetUserActivityLog", Userid, fromDate, ToDate);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                return Json(JsonConvert.DeserializeObject<List<USP_UserActivityLog_Result>>(apiResult.ToString()), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "",
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AccountDetails(string id)
        {
            CurrentUser.createCurUserSession(UserRole.User.ToString(), id);
            AccountDetailsViewModel model = null;
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "ManageUser/GetAccountDetails", id);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                model = JsonConvert.DeserializeObject<AccountDetailsViewModel>(apiResult.ToString());
                model.UserId = id;
            }
            model.YearList = DataHelper.GetCreditCardYear(model.ExpYear);
            model.MonthList = DataHelper.GetMonths(model.ExpMonth);
            model.IndustryList = DataHelper.GetIndustryList(model.IndustryRefId.ToString());
            model.CountryList = DataHelper.GetCountryList(model.Country);
            if (model.Country == null)
            {
                model.Country = Constants.DefaultState;

            }
            TempData["States"] = DataHelper.GetStateList(null);
            TempData.Keep("States");
            model.Password = Encryption64.Decrypt(model.Password);
            return PartialView("AccountDetails", model);
        }

        [HttpPost]
        public ActionResult AccountDetails(AccountDetailsViewModel model)
        {
            model.CreatedById = User.Identity.GetUserId();
            model.LastModById = User.Identity.GetUserId();
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser();
            var provider = new DpapiDataProtectionProvider("ASP.NET Identity");
            UserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("ASP.NET Identity"));
            user.Email = model.Email;
            if (UserManager.FindByEmail(user.Email) != null && model.VerifyPassword != null)
            {
                var token = UserManager.GeneratePasswordResetToken(model.UserId);
                var Result = UserManager.ResetPassword(model.UserId, token, model.VerifyPassword);
                if (Result.Succeeded)
                {
                    string res = updateProfile(model);
                    //String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageUser");
                    //var res = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(model)).Content.ReadAsStringAsync().Result;
                    if (res == "\"success\"")
                    {
                        return this.Json(new
                        {
                            status = Constants.SuccessTitle,
                            msg = "Profile has been successfully updated",
                        }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.ErrorTitle,
                            msg = res,
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                string res = updateProfile(model);
                if (res == "\"success\"")
                {
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = "Profile has been successfully updated",
                    }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = res,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public string updateProfile(AccountDetailsViewModel model)
        {
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageUser");
            var res = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(model)).Content.ReadAsStringAsync().Result;
            return res;
        }

        public ActionResult ExportInvoicePdf(string id)
        {
            //MemoryStream m = new MemoryStream();
            string FileName = "Invoice_" + id + ".pdf";
            //StringReader sr = new StringReader(getInvoiceDetailsSection(id));
            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, m);
            //pdfDoc.Open();
            //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            //return File(m, "application/pdf", FileName);

            String htmlText = getInvoiceDetailsSection(id);
            Document document = new Document();
            string filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);
            if (System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception)
                {

                }
                filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);

            }
            if (!Directory.Exists(Server.MapPath("~/UserUpload/Pdf")))
            {
                Directory.CreateDirectory(Server.MapPath("~/UserUpload/Pdf/"));
                filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);
            }
            try
            {
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, new StringReader(htmlText));
                document.Close();
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
            }

            return File(filePath, "application/pdf");

        }

        public string getInvoiceDetailsSection(string invoiceId)
        {
            String htmlStr = string.Empty;
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Invoice/GetInvoiceDetails", invoiceId);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                var result = JsonConvert.DeserializeObject<usp_getInvoiceDetails_Result>(apiResult.ToString());
                htmlStr = "<table width='650' border='0' cellpadding='0' cellspacing='0'><tbody><tr><td align='left'><table width='100%' border='0' cellpadding='0' " +
                    "cellspacing='0'><tr><td align='left'><img src='" + Constants.AppUrl + "/Content/imagesHome/logo.png' height='34 px' alt='' /></td><td align='right' valign='top' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'><strong>" + result.TransactionDate + "</strong></td></tr></table></td></tr><tr><td height='25' align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>Dear " + result.FirstName + " " + result.LastName + " </td></tr>" +
                    "<tr><td align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333; line-height:20px;'>Your " + (!string.IsNullOrEmpty(result.InvoiceType) ? result.InvoiceType.Trim() : String.Empty) + " of $ " + (!string.IsNullOrEmpty(result.AmountCharged) ? result.AmountCharged.Trim() : String.Empty) + " to the following Credit Card:</td></tr><tr><td align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333; line-height:20px;'>" + (!string.IsNullOrEmpty(result.NameOnCard) ? result.NameOnCard.Trim() : String.Empty) + " <br />" +
                    "" + (!string.IsNullOrEmpty(result.Address) ? result.Address.Trim() : String.Empty) + " <br/>" + (!string.IsNullOrEmpty(result.City) ? result.City.Trim() : String.Empty) + ", " + result.State.Trim() + " " + result.Zip.Trim() + "<br />" + (!string.IsNullOrEmpty(result.CardTypeInvoice) ? result.CardTypeInvoice.Trim() : String.Empty) + " " + result.CardNumber.Trim() + "<br/>" + "Phone: " + result.Phone + "<br/></td></tr><tr><td align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:16px; color:#333;'><strong>Invoice Details</strong></td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; " +
                    "font-size:13px; color:#333;'><strong>Invoice Number:</strong> " + result.AnTransactionId + "</td></tr><tr><td align='left' style='border-bottom:#333 1px solid; font-size:12px; color:#FFF;'>.</td></tr><tr><td align='left'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='27%' height='25' align='left' valign='middle' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>Description</td><td width='73%' align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>" +
                    "Amount</td></tr><tr><td align='left' style='border-top:#333 2px solid; font-size:12px; color:#FFF;'>.</td><td align='left' style='border-top:#333 2px solid; font-size:12px; color:#FFF;'>.</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>" + result.InvoiceType + "</td><td align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>$" + result.AmountCharged + " USD</td></tr>" +
                    "<tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>&nbsp;</td><td align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>&nbsp;</td></tr><tr><td align='left'>&nbsp;</td><td align='right'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='74%' align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'><strong>Total:</strong></td>" +
                    "<td width='26%' align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>$ " + result.AmountCharged + " USD</td></tr></table></td></tr></table></td></tr><tr><td align='left'>&nbsp;</td></tr><tr><td align='center' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:10px; color:#333;'>It may take a few moments for this transaction to appear in your account.</td></tr><tr><td align='left' style='border-bottom:#333 1px solid; font-size:10px; color:#FFF;'>.</td></tr>" +
                    "<tr><td height='25' align='left' valign='middle' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'><strong>Have issues with this Transaction?</strong></td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>You can get in touch with Cheapest Email via email at &nbsp; <a href='mailto:Invoices@CheapestEmail.com'>Invoices@CheapestEmail.com</a></td></tr></tbody></table>";
                //htmlStr = EmailHtml.getInvoiceSend(result.FirstName, result.TransactionDate.ToString(), result.InvoiceType, result.AmountCharged, result.FirstName, result.Address1, result.City, result.State, result.Zip, result.CardTypeInvoice, result.CardNumber, result.Phone, result.AnTransactionId);
            }
            return htmlStr.ToString();
        }

        //    htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;\">");
        //    htmlStr.Append("<tr><td style=\" text-align: center;\"><img src=\"http://18.233.130.90:81/Content/imagesHome/logo.png\" /></td></tr>");
        //    htmlStr.Append("</table>");

        //    #region  Bill Address Table

        //    htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:20px;\">");
        //    htmlStr.Append("<tr>");
        //    htmlStr.Append("<td style=\"width:380; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Bill From :</strong></td>");
        //    htmlStr.Append("<td style=\"width:380; padding-left:10px;padding-top:10px;height: 34px;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Bill To :</strong></td>");
        //    htmlStr.Append("</tr>");
        //    htmlStr.Append("<tr>");
        //    htmlStr.Append("<td style=\"width:380;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">Cheapest Email</td>");
        //    htmlStr.Append("<td><table>");
        //    htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;padding-top:10px;font-size: 14px; color: #000000\">" + result.ContactEmail + "</td></tr>");
        //    htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + result.CompanyName + "</td></tr>");
        //    htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;font-size: 14px; color: #000000\">" + result.Address1 + " </td></tr>");
        //    htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + result.City + "," + result.Zip + "</td></tr>");
        //    if (result.Phone == null)
        //    {
        //        htmlStr.Append("<tr><td style=\"width:380;height: 20px; padding-left:10px;font-size: 14px;\">Phone #:</td></tr>");
        //    }

        //    else
        //    {
        //        htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;font-size: 14px;\">Phone #:" + Regex.Replace(result.Phone, @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") + "</td></tr>");
        //    }
        //    htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\"></td></tr>");
        //    htmlStr.Append("</table></td></tr>");
        //    htmlStr.Append("</table>");
        //    #endregion

        //    #region  Invoice Table

        //    htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
        //    htmlStr.Append("<tr>");
        //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Invoice ID</td>");
        //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Invoice Date</td>");
        //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Amount</td>");
        //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Invoice Type</td>");
        //    htmlStr.Append("</tr>");
        //    htmlStr.Append("<tr>");
        //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
        //    htmlStr.Append(result.InvoiceID);
        //    htmlStr.Append("</td>");
        //    htmlStr.Append("<td  style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
        //    htmlStr.Append(result.TransactionDate);
        //    htmlStr.Append("</td>");
        //    htmlStr.Append("<td  style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
        //    htmlStr.Append(result.AmountCharged);
        //    htmlStr.Append("</td>");
        //    htmlStr.Append("<td  style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
        //    htmlStr.Append(result.InvoiceType);
        //    htmlStr.Append("</td>");
        //    htmlStr.Append("</tr>");
        //    htmlStr.Append("</table>");
        //}

        [HttpPost]

        public ActionResult ManageBillingDetails(ManageBillingModel manageBillingDetails)
        {
            if (manageBillingDetails != null)
            {

                using (var db = new EmailMarketingDbEntities())
                {
                    var user = db.UserProfiles.Where(x => x.Id == manageBillingDetails.ManageBilling.Id).FirstOrDefault();
                    user.IsActive = manageBillingDetails.ManageBilling.IsActive;
                    db.SaveChanges();

                    var _billinguser = db.UserPlans.Where(x => x.UserId == manageBillingDetails.ManageBilling.Id && x.IsActive == true).FirstOrDefault();
                    if (_billinguser != null)
                    {

                        if (manageBillingDetails.ManageBilling.PackageId != _billinguser.PackageId)
                        {
                            DataHelper.CreateUserPlan(manageBillingDetails.ManageBilling.PackageId, manageBillingDetails.ManageBilling.Id);
                        }
                        _billinguser = db.UserPlans.Where(x => x.UserId == manageBillingDetails.ManageBilling.Id && x.IsActive == true).FirstOrDefault();

                        if (manageBillingDetails.ManageBilling.NextBillingDate != null)
                        {
                            _billinguser.NextPackageUpdateDate = Convert.ToDateTime(manageBillingDetails.ManageBilling.NextBillingDate);
                        }
                        _billinguser.BillingAmount = manageBillingDetails.ManageBilling.BillingAmount;
                        if (manageBillingDetails.ManageBilling.MonthlyCredit == "Unlimited Emails")
                        {
                            var credits = 0;
                            _billinguser.MonthlyCredit = credits.ToString();
                            _billinguser.TotalEmails = credits;
                            _billinguser.totalRemainingCredits = credits.ToString();
                        }
                        else
                        {
                            _billinguser.MonthlyCredit = manageBillingDetails.ManageBilling.MonthlyCredit;
                            _billinguser.TotalEmails = Convert.ToInt32(manageBillingDetails.ManageBilling.MonthlyCredit);
                            _billinguser.totalRemainingCredits = Convert.ToString(manageBillingDetails.ManageBilling.MonthlyCredit);
                        }
                    }
                    db.SaveChanges();
                }
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Successfully Updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ManageBillingDetails(string id)
        {
            ManageBillingModel data = new ManageBillingModel();
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "ManageUser/GetBillingDetails", id);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                var result = JsonConvert.DeserializeObject<usp_getManageBillingDetail_Result>(apiResult.ToString());
                data.ManageBilling = result;
                data.MemberTypeList = DataHelper.GetPlanSelectList(data.ManageBilling.AccountType);
                data.SubscriberList = DataHelper.GetSubscriberList(data.ManageBilling.Subscriber, data.ManageBilling.PlanId);
                if (data.ManageBilling.MonthlyCredit == 0.ToString())
                {
                    data.ManageBilling.MonthlyCredit = "Unlimited Emails";
                }
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "",
                }, JsonRequestBehavior.AllowGet);
            }
            return PartialView("ManageBillingDetails", data);
        }


        [HttpPost]
        public ActionResult GetNoteView(string userid)
        {
            NoteViewModel model = new NoteViewModel();
            model.newNote = new SalesPortalRepNote() { UserId = userid };
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Notes", userid);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.noteList = JsonConvert.DeserializeObject<List<usp_getUserNoteList_Result>>(result.ToString());

            }
            model.TimeZoneList = DataHelper.GetTimeZone(model.newNote.TimeZone);
            return PartialView("AddEditNotes", model);
        }
        [HttpPost]
        public ActionResult AddNote(NoteViewModel model)
        {

            var UserId = User.Identity.GetUserId();
            model.newNote.LastModById = UserId;
            model.newNote.CreatedById = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Notes");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.newNote)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.newNote.NoteId == 0 ? "Note has been successfully created" : "Note has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNote(int noteId)
        {
            string APIURL = string.Format("{0}/{1}?id={2}&userid={3}", Constants.ApiURL, "Notes", noteId, User.Identity.GetUserId());
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Note has been successfully deleted",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //-----------ImportContact-------------------//

        [HttpPost]
        public JsonResult getList(string UserId)
        {

            String Api = string.Format("{0}/{1}?Id={2}", ConfigurationManager.AppSettings["ApiURL"], "ImportListContact", UserId);
            var lst = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (lst != null)
            {
                var listModel = JsonConvert.DeserializeObject<List<EM.Factory.ViewModels.ListViewModel>>(lst.ToString());
                return Json(listModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        [ActionName("ImportSubscriberList")]
        public ActionResult ImportSheetSubscriberList(int? id)
        {

            if (Request.Files.Count > 0)
            {
                try
                {
                    var UserId = CurrentUser.getCurUserId();
                    var subscriberList = Request.Files[0];
                    var ExcelFilePath = subscriberList.SaveExcelFile();
                    var ServerPath = Server.MapPath("~");
                    var FilePath = ServerPath + ExcelFilePath;
                    var listSubscriber = DataHelper.CheckSubscriberListFromExcel(FilePath, UserId, id);
                    string header = listSubscriber.FirstOrDefault().validHeaders;
                    if (listSubscriber.FirstOrDefault().countInvalidheader == true)
                    {
                       //var header = string.Join(", ", from item in listSubscriber select item.invalidHeaders);
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = $"The file that you are uploading has invalid headers.<p>Please upload excel with correct headers!</p><p><a href='/UserUpload/SubscriberListSample.xlsx'>Download Excel Format For Subscriber</a></p>",
                            URL = ""
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (listSubscriber.FirstOrDefault().invalidEmail == true)
                    {
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = $"The file that you are uploading has invalid email header.<p><a href='/UserUpload/SubscriberListSample.xlsx'>Download Excel Format For Subscriber</a></p>",
                            URL = ""
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (listSubscriber.Count > 0)
                    {
                        if (listSubscriber.Count <= Convert.ToInt32(Constants.SubscriberLimit))
                        {
                            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/SubscriberListPost1");
                            try
                            {
                                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listSubscriber));
                                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    // var subscribers = subscriberViewModels.Select(x => new Contacts { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).ToList();
                                    //SendGridEmails sendGrid = new SendGridEmails();
                                    //sendGrid.SendGridMultipleSubscriberEmailSync("Welcome Email", EmailHtml.getMultipleSubscriberWelcomeEmail(CurrentUser.getCurUserDetails().FirstName + CurrentUser.getCurUserDetails().LastName), Constants.SupportEmail, Constants.SupportName , subscribers);
                                    CurrentUser.SetAuthoriztionInfo();
                                    //DataHelper.CleanExcel(FilePath);
                                    var response = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                                    if (response.FirstOrDefault().result == 0)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Error,
                                            msg = "Please upgrade your plan to add more subscriber!",
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    if (response.FirstOrDefault().result == 1)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Success,
                                            msg = $"{header} data imported Successfully.",
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    if (response.FirstOrDefault().result == 3)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Success,
                                            msg = "The susbcribers you are uploading have already exists in your list!"
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    if (response.FirstOrDefault().result == 2)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Error,
                                            msg = Constants.ErrorMessage
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Success,
                                            msg = "Import Successful",
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                return Json(new
                                {
                                    status = Constants.Error,
                                    msg = Constants.ErrorMessage,
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = Constants.Error,
                                msg = "Please upload records in bunches of " + Constants.SubscriberLimit,
                                URL = FilePath
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = "The file that you are uploading has no records!",
                        }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = Constants.Error,
                        msg = ex.Message,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(new
            {
                status = Constants.Error,
                msg = "The file that you are uploading has no records!",
                URL = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveImportSheetSubscribers(string param1, string param2)
        {
            ViewBag.ListId = !string.IsNullOrEmpty(param2) ? Convert.ToInt32(param2) : 0;
            ViewBag.ExcelFilePath = param1;
            var UserId = User.Identity.GetUserId();// CurrentUser.getCurUserId();
            var listSubscriber = DataHelper.GetSubscriberListFromExcel(param1, UserId, 0);
            return PartialView("SaveImportSheetSubscribers", listSubscriber);

        }

        [HttpPost]
        public ActionResult SaveImportSheetSubscribers(string param1, int? param2, string userid)
        {
            if (param1 != null && param2 != null && userid != null)
            {
                SubscriberImportResponseViewModel subscriberErrorResponseViewModel = new SubscriberImportResponseViewModel();
                List<SubscriberViewModel> subscriberViewModels = new List<SubscriberViewModel>();
                List<SubscriberViewModel> inValidSubscriberViewModels = new List<SubscriberViewModel>();
                var listSubscriber = DataHelper.GetSubscriberListFromExcel(param1, User.Identity.GetUserId(), param2);
                foreach (var item in listSubscriber)
                {
                    var isValid = DataHelper.ValidateSubscriber(item);
                    if (isValid)
                    {

                        item.CreatedById = userid;
                        if (item.Email.Length < 250)
                        {
                            subscriberViewModels.Add(item);
                        }
                    }
                    else
                    {
                        inValidSubscriberViewModels.Add(item);
                    }
                }
                subscriberErrorResponseViewModel.InvalidSubscribers = inValidSubscriberViewModels;
                if (subscriberViewModels.Count > 0)
                {
                    //string APIURL1 = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/ExistSubscriber");
                    //try
                    //{
                    //    var result = (new APICallHelper()).Post(APIURL1, JsonConvert.SerializeObject(subscriberViewModels));
                    //    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    //    {
                    //        var ExistSubscribers = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                    //        //subscriberViewModels.RemoveAll(x => ExistSubscribers.Any(y => y.Email == x.Email));
                    //        //subscriberErrorResponseViewModel.SuccessSubscribers = subscriberViewModels;
                    //        subscriberErrorResponseViewModel.DuplicateSubscribers = ExistSubscribers;
                    //        //return Json(new
                    //        //{
                    //        //    status = Constants.Success,
                    //        //    msg = "Import Successful",
                    //        //    Content = subscriberErrorResponseViewModel
                    //        //}, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                    //catch (Exception ex)
                    //{

                    //    return Json(new
                    //    {
                    //        status = Constants.Error,
                    //        msg = ex.Message,
                    //        Content = subscriberErrorResponseViewModel
                    //    }, JsonRequestBehavior.AllowGet);
                    //}
                    string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/SubscriberListPost1");
                    try
                    {
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(subscriberViewModels));
                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            DataHelper.CleanExcel(param1);
                            //var duplicateSubscribers = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                            //subscriberViewModels.RemoveAll(x => duplicateSubscribers.Any(y => y.Email == x.Email));
                            //subscriberErrorResponseViewModel.SuccessSubscribers = subscriberViewModels;
                            //subscriberErrorResponseViewModel.DuplicateSubscribers = duplicateSubscribers;
                            return Json(new
                            {
                                status = Constants.Success,
                                msg = "Import Successful",
                                Content = subscriberErrorResponseViewModel
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {

                        return Json(new
                        {
                            status = Constants.Error,
                            msg = ex.Message,
                            Content = subscriberErrorResponseViewModel
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = Constants.Error,
                    msg = "The file that you are uploading have no valid emails! ",
                    Content = subscriberErrorResponseViewModel,
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage,
                    Content = "",
                }, JsonRequestBehavior.AllowGet);
            }
        }


        // Add List 
        [HttpPost]
        public ActionResult AddList(string ListName, string UserId)
        {
            ListViewModel listViewModel = new ListViewModel();
            if (ModelState.IsValid)
            {

                try
                {
                    //var UserId = User.Identity.GetUserId();// CurrentUser.getCurUserId();
                    listViewModel.CreatedById = UserId;
                    //listViewModel.LastModById = UserId;
                    listViewModel.ListName = ListName;
                    if (listViewModel.ListId == 0)
                    {
                        String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserList");
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listViewModel)).StatusCode;
                        if (result == System.Net.HttpStatusCode.OK)
                        {
                            //------------User Activity Log-------------//
                            var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                            UserActivityLog userActivtyObj = new UserActivityLog();
                            // userActivtyObj.UserId = getCurrentUserDetails.Id;
                            userActivtyObj.Createdby = listViewModel.CreatedById;
                            userActivtyObj.Description = listViewModel.ListName + " List Added ";// For " + getCurrentUserDetails.FirstName + " " + getCurrentUserDetails.LastName + " by " + User.Identity.GetUserName();
                            userActivtyObj.DateTime = DateTime.Now;
                            var res = DataHelper.InsertUserActivity(userActivtyObj);
                            //------------------------------------------//
                            return this.Json(new
                            {
                                status = Constants.SuccessTitle,
                                msg = "List has been successfully created",
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }


                }
                catch (Exception ex)
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }

            }


            return this.Json(new
            {
                status = Constants.ErrorTitle,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);


        }

        public ActionResult BillClient(string id)
        {
            InvoiceViewModel model = null;
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Invoice/GetNewInvoice", id);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                model = JsonConvert.DeserializeObject<InvoiceViewModel>(apiResult.ToString());
                model.UserId = id;
            }
            else
            {
                model = new InvoiceViewModel();
                model.UserId = id;
            }
            model.YearList = DataHelper.GetCreditCardYear(Convert.ToInt32(model.ExpiresYear));
            model.MonthList = DataHelper.GetMonths(Convert.ToInt32(model.ExpiresMonth));
            model.PaymnetTypeList = DataHelper.GetPaymentTypeList(model.PaymentTypeId.ToString());
            return PartialView("BillClient", model);

        }

        [HttpPost]
        public ActionResult GetVerifiedEmail(string userid)
        {
           // var oemail = DataHelper.GetConfirmEmailAddress(userid);
            return Json( db.EmailConfirmations.Where(x => x.Status == true && x.UserId == userid).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReActivateCampaign(string userid)
        {
            var reactivate = db.usp_getReActivateCampaign(userid).FirstOrDefault();
            if (reactivate.Result == 1)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Campaign suspended functionality deactivated successfully.",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
           
        }



    }
}























