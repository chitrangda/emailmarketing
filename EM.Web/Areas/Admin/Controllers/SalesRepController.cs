﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EM.Helpers;
using EM.Factory.ViewModels;
using System.Configuration;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity.EntityFramework;
using EM.Web.Models;
using Microsoft.AspNet.Identity;
using EM.Factory;
using System.IO;
using ClosedXML.Excel;
using System.Data;
using MoreLinq;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;
using EM.Web.Utilities;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SalesRepController : Controller
    {
        // GET: Admin/SalesRep
        public ActionResult Index(string id = null, string curView = null)
        {
            ViewBag.salesRepId = id;
            ViewBag.curView = curView;
            ViewBag.Title = "Sales Rep";
            return View();
        }

        public ActionResult SalesRepView()
        {
            return PartialView("SalesRepView");
        }

        [HttpPost]
        //public ActionResult getAllSalesRep(DataTableAjaxPostModel model)
        //{
        //    int index = 0;
        //    if (model.start != 0)
        //    {
        //        model.start += 1;
        //        index = model.start / model.length;
        //    }
        //    try
        //    {
        //        String APIURL = string.Format("{0}/{1}?FirstName=&LastName=&NoOfClients=&SpentTotal=&ViewRepActivity=&CanExport&ExportLead=&Manager=&IsActive=&pageIndex={2}&pageSize={3}", ConfigurationManager.AppSettings["ApiURL"], "ManageSalesRep", index, model.length);
        //        var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
        //        DTResult res = new DTResult();
        //        res.data = JsonConvert.DeserializeObject<List<usp_getAllSalesRep_Result>>(result.ToString());
        //        res.draw = res.draw;
        //        res.recordsTotal = res.data.FirstOrDefault() != null ? res.data.FirstOrDefault().totalrecords.Value : 10;
        //        res.recordsFiltered = res.recordsTotal;
        //        return Json(res, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(ex.Message);
        //    }
        //}

        public ActionResult getAllSalesRep(string FirstName, string LastName, int? NoOfClients, double? SpentTotal, bool? ViewRepActivity, bool? ExportLead, bool? Manager, bool? CanExport, bool? IsActive, int? page)
        {
            int index = 0;
            if (page != null)
            {
                index = page.Value;
            }
            try
            {
                String APIURL = string.Format("{0}/{1}?FirstName={2}&LastName={3}&NoOfClients={4}&SpentTotal={5}&ViewRepActivity={6}&CanExport{7}&ExportLead={8}&Manager={9}&IsActive={10}&pageIndex={11}&pageSize=", ConfigurationManager.AppSettings["ApiURL"], "ManageSalesRep", FirstName, LastName, NoOfClients, SpentTotal, ViewRepActivity, CanExport, ExportLead, Manager, IsActive, index);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getAllSalesRep_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }


        public ActionResult AddEditSalesRep(string id)
        {
            SalesRepInfoViewModel model = null;
            if (id != null)
            {
                String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ManageSalesRep?id=" + id);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<SalesRepInfoViewModel>(result.ToString());
            }
            return PartialView("AddEditSalesRep", model);
        }

        [HttpPost]
        public JsonResult AddEditSalesRep(SalesRepInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var provider = new DpapiDataProtectionProvider("ASP.NET Identity");
                UserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("ASP.NET Identity"));

                if (model.Id == null)
                {
                    if (UserManager.FindByEmail(model.Email) == null)
                    {
                        var user = new ApplicationUser();
                        user.UserName = model.Email;
                        user.Email = model.Email;
                        string userPWD = model.Password;
                        var chkUser = UserManager.Create(user, userPWD);

                        if (chkUser.Succeeded)
                        {
                            var result1 = UserManager.AddToRole(user.Id, "SalesRep");
                            var profile = new SalesRepProfile()
                            {
                                Id = user.Id,
                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                ContactNo = model.ContactNo,
                                NoOfClients = 0,
                                SpentTotal = 0,
                                ViewRepActivity = false,
                                CanExport = false,
                                ExportLead = false,
                                Manager = false,
                                CreatedBy = User.Identity.GetUserId(),
                                CreatedDate = DateTime.Now,
                                IsActive = true

                            };
                            using (var db = new EmailMarketingDbEntities())
                            {
                                db.SalesRepProfiles.Add(profile);
                                db.SaveChanges();
                            }

                        }
                        return this.Json(new
                        {
                            status = Constants.SuccessTitle,
                            msg = "Sales Representative added successfully.",
                        }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = "Error",
                            msg = "Email id already exist!"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                else
                {
                    using (var db = new EmailMarketingDbEntities())
                    {
                        if (model.Password != null)
                        {
                            var token = UserManager.GeneratePasswordResetToken(model.Id);
                            var Result = UserManager.ResetPassword(model.Id, token, model.Password);
                        }
                        var profile = db.SalesRepProfiles.Find(model.Id);
                        if (profile != null)
                        {
                            profile.FirstName = model.FirstName;
                            profile.LastName = model.LastName;
                            profile.ContactNo = model.ContactNo;
                            profile.ModifedDate = DateTime.Now;
                            profile.ModifiedBy = User.Identity.GetUserId();
                        }

                        db.SaveChanges();
                        return this.Json(new
                        {
                            status = Constants.SuccessTitle,
                            msg = "Update successfully.",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public JsonResult updateSalesRep(string Id, string FirstName, string LastName, int? NoOfClients, double? SpentTotal, bool? ViewRepActivity, bool? ExportLead, bool? Manager, bool? CanExport, bool? IsActive)
        {
            try
            {
                var salesRepProfile = new SalesRepProfile();
                salesRepProfile.Id = Id;
                salesRepProfile.FirstName = FirstName;
                salesRepProfile.LastName = LastName;
                salesRepProfile.NoOfClients = NoOfClients;
                salesRepProfile.SpentTotal = SpentTotal;
                salesRepProfile.ViewRepActivity = ViewRepActivity;
                salesRepProfile.ExportLead = ExportLead;
                salesRepProfile.Manager = Manager;
                salesRepProfile.CanExport = CanExport;
                salesRepProfile.IsActive = IsActive;
                String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageSalesRep");
                var result = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(salesRepProfile)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = "Update Sucessfully.",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ExportList()
        {
            String APIURL = string.Format("{0}/{1}?FirstName=&LastName=&NoOfClients=&SpentTotal=&ViewRepActivity=&CanExport&ExportLead=&Manager=&IsActive=&pageIndex=0&pageSize=", ConfigurationManager.AppSettings["ApiURL"], "ManageSalesRep");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var users = JsonConvert.DeserializeObject<List<usp_getAllSalesRep_Result>>(result.ToString());
            DataTable dt = users.Select(r => new { r.Email, r.FirstName, r.LastName, r.NoOfClients, r.SpentTotal, r.ViewRepActivity, r.CanExport, r.ExportLead, r.Manager, r.IsActive }).ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "SalesRep");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SalesRep.xlsx");
                }
            }
        }

        [HttpPost]
        public JsonResult changeAccountStatus(string id, bool status)
        {
            try
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    var profile = db.SalesRepProfiles.Find(id);
                    if (profile != null)
                    {
                        profile.IsActive = status;
                        db.SaveChanges();
                        return this.Json(new
                        {
                            status = "updated",
                            msg = "updated"
                        }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.NotFoundTitle,
                            msg = "not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]

        public JsonResult changeStatus(string id, bool value, string field)
        {
            try
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    var profile = db.SalesRepProfiles.Find(id);
                    if (profile != null)
                    {
                        if (field == "ViewRepActivity")
                        {


                            profile.ViewRepActivity = value;
                        }
                        if (field == "CanExport")
                        {
                            profile.CanExport = value;
                        }
                        if (field == "ExportLead")
                        {
                            profile.ExportLead = value;
                        }
                        if (field == "Manager")
                        {
                            profile.Manager = value;
                        }
                        db.SaveChanges();
                        return this.Json(new
                        {
                            status = Constants.SuccessTitle,
                            msg = "Updated Successfully!"
                        }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.NotFoundTitle,
                            msg = "not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class DTResult
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<usp_getAllSalesRep_Result> data { get; set; }
    }

    //Start - JSon class sent from Datatables


    public class DataTableAjaxPostModel
    {
        // properties are not capital due to json mapping
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }
    }

    public class Column
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public Search search { get; set; }
    }

    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }

    public class Order
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
    /// End- JSon class sent from Datatables
}