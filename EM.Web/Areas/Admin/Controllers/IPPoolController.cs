﻿using EM.Factory;
using EM.Helpers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class IPPoolController : Controller
    {
        // GET: Admin/IPPool
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PoolData()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PoolData(IP_Pool model)
        {
            var UserId = User.Identity.GetUserId();
            model.CreatedBy = UserId;
            model.LastModBy = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageIPPool");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.Id == 0 ? "IP Pool has been successfully created" : "IP Pool has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetIPPoolList()
        {
            string APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPPool");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = Json(JsonConvert.DeserializeObject<List<IP_Pool>>(result.ToString()), JsonRequestBehavior.AllowGet);
                return response;
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    message = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditIPPool(int id)
        {
            IP_Pool model = new IP_Pool();
            String APIURL = String.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPPool", id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<IP_Pool>(result.ToString());
                model = response;
            }
            return PartialView("EditIPPool", model);
        }

        [HttpPost]
        public ActionResult DeleteIPPool(int id)
        {
            var APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "ManageIPPool", id);
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "IP Pool has been deleted successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}