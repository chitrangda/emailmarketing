﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.Admin.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    public class IPAddressController : Controller
    {
        // GET: Admin/IPAddress
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult IPAddressData()
        {
            IPAddressViewModel model = new IPAddressViewModel();
            TempData["IPPool"] = DataHelper.GetIPPoolList("");
            TempData.Keep("IPPool");
            TempData["IPAddress"] = DataHelper.GetIPAddress("");
            TempData.Keep("IPAddress");
            //model.IPAddressList = DataHelper.GetIPAddress("");
            return View();
        }

        [HttpPost]
        public ActionResult IPAddressData(IPAddressViewModel iP_Address)
        {
            var UserId = User.Identity.GetUserId();
            iP_Address.CreatedBy = UserId;
            iP_Address.LastModBy = UserId;
            TempData["Ip_pool_Id"] = iP_Address.IPPool_Id;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageIPAddress");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(iP_Address)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = iP_Address.Id == 0 ? "IP Address has been successfully saved" : "IP Address has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else if (result == System.Net.HttpStatusCode.NotFound)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "IP address not found!"
                }, JsonRequestBehavior.AllowGet);
            }
            else if (result == System.Net.HttpStatusCode.Conflict)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "IP address already exist!"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetIPAddressList()
        {
            string APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPAddress");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = Json(JsonConvert.DeserializeObject<List<IP_Address>>(result.ToString()), JsonRequestBehavior.AllowGet);
                return response;
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    message = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetIPAddressListBYIPPool(int id)
        {
            try
            {
                //var Ip_pool_Id= TempData["Ip_pool_Id"];
                String APIURL = String.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPAddress/GetByPoolId", id);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    var oIpList = JsonConvert.DeserializeObject<List<IP_Address>>(result.ToString()).Select(r => r.IP).ToArray();
                    var response = Json(oIpList, JsonRequestBehavior.AllowGet);
                    return response;
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        message = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message,ex);
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    message = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditIPAddress(int id)
        {
            IP_Address model = new IP_Address();
            String APIURL = String.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPAddress", id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<IP_Address>(result.ToString());
                model = response;
            }
            return PartialView("EditIPAddress", model);
        }

        [HttpPost]
        public ActionResult DeleteIPAddress(int id)
        {
            var APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "ManageIPAddress", id);
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "IP Address has been deleted successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public PartialViewResult AddIPPool()
        {
            return PartialView("AddIPPool");
        }

        [HttpPost]
        public ActionResult AddIP(IPAddressViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.CreatedBy = UserId;
            model.LastModBy = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageIPAddress/PostIPPool");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.Id == 0 ? "IP Pool has been successfully created" : "IP Pool has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}