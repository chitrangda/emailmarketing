﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class RepActivityController : Controller
    {
        // GET: Admin/RepActivity
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetSaleRepActivityData(string RepId,DateTime? From, DateTime? To)
        {
            try
            {
               String APIURL = string.Format("{0}/{1}?RepId={2}&From={3}&To={4}", Constants.ApiURL, "RepActivity/GetGridData", RepId, From, To);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepActivity_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        [HttpPost]
        public JsonResult GetSaleRepprofile(string RepId, DateTime? From, DateTime? To)
        {
            try
            {
                var salerepProfile  = DataHelper.GetSalesRep(null);
                return Json(salerepProfile.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult ExportSaleRepActivity(string RepId,DateTime? From, DateTime? To)
        {
            try
            {
                //var salesRepData = CurrentUser.getSalesRep();
                String APIURL = string.Format("{0}/{1}?RepId={2}&From={3}&To={4}", Constants.ApiURL, "RepActivity/GetGridData", RepId, From, To);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                List<usp_getSalesRepActivity_Result> data = JsonConvert.DeserializeObject<List<usp_getSalesRepActivity_Result>>(result.ToString());
                DataTable dt = ActivityDetails(data);
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt, "List");
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SaleRepActivity.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        DataTable ActivityDetails(List<usp_getSalesRepActivity_Result> data)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Username");
            dt.Columns.Add("RepName");
            dt.Columns.Add("Category");
            dt.Columns.Add("Description");
            dt.Columns.Add("Date");
            dt.Columns.Add("AddComments");
            foreach (var item in data)
            {
                DataRow dr = dt.NewRow();
                dr["Username"] = item.Username;
                dr["RepName"] = item.FirstName + " " + item.LastName;
                dr["Category"] = item.Category;
                dr["Description"] = item.Description;
                dr["Date"] = item.CreatedDate;
                dr["AddComments"] = DBNull.Value;

                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}