﻿
using EM.Factory;
using EM.Helpers;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class AdminControlController : Controller
    {
        // GET: Admin/AdminControl
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult MasterData()
        {

            return View();

        }
        [HttpPost]
        public ActionResult MasterData(SalesPortal_Providers model)
        {

            var UserId = User.Identity.GetUserId();
            model.CreatedBy = UserId;
            model.LastModBy = UserId;
            var provider = DataHelper.CheckDuplicateProviders(model.ProviderName);
            if (provider == true)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Provider Name Already Exists",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Providers");
                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = model.ProviderId == 0 ? "Provider has been successfully created" : "Provider has been successfully updated",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }

            }
        }
        public ActionResult getProviderList()
        {

            String APIURL = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "Providers");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = Json(JsonConvert.DeserializeObject<List<SalesPortal_Providers>>(result.ToString()), JsonRequestBehavior.AllowGet);
                return response;
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);

            }
        }
        [HttpPost]

        public ActionResult EditProvider(int ProviderId)
        {
            SalesPortal_Providers model = new SalesPortal_Providers();
            String APIURL = string.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "Providers", ProviderId);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<SalesPortal_Providers>(result.ToString());
                model = response;

            }
            return PartialView("EditProvider", model);

        }

        [HttpPost]
        public ActionResult DeleteProvider(int ProviderId)
        {
            var APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Providers", ProviderId);
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Provider has been deleted successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}

