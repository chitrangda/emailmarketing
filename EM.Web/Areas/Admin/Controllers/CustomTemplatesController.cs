﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Extentions;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CustomTemplatesController : Controller
    {
        // GET: Admin/AdminTemplate
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult TemplateList(int? page, string search)
        {
            if (page == null || page == 0 || !string.IsNullOrEmpty(search))
            {
                page = 1;
            }
            ViewBag.PageNumber = page;
            List<usp_getTemplateList_Result> templateViewModel = new List<usp_getTemplateList_Result>();
            string APIURLUser = string.Format("{0}/{1}?id={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy", Constants.ApiURL, "Template", User.Identity.GetUserId(), page, Constants.PageSize, search);
            var countUser = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (countUser != null)
            {
                var templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getTemplateList_Result>>(countUser.ToString());
                ViewBag.TotalRecord =templates!=null && templates.Count > 0 ? templates.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                templateViewModel = templates;
            }

            return PartialView("TemplateList", templateViewModel);
        }
        [HttpPost]
        public ActionResult GetTemplateListByPaging(int? page, string search, string orderBy)
        {
            if (page == null || page == 0)
            {
                page = 1;
            }
            ViewBag.PageNumber = page;
            List<usp_getTemplateList_Result> templateViewModel = new List<usp_getTemplateList_Result>();
            string APIURLUser = string.Format("{0}/{1}?id={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy={6}", Constants.ApiURL, "Template", User.Identity.GetUserId(), page, Constants.PageSize, search, orderBy);
            var countUser = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (countUser != null)
            {
                var templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getTemplateList_Result>>(countUser.ToString());
                ViewBag.TotalRecord = templates.Count > 0 ? templates.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                templateViewModel = templates;
            }
            return PartialView("TemplateListGrid", templateViewModel);
        }

        public ActionResult BeeTemplate(int? id)
        {
            ViewBag.TemplateId = id;
            ViewBag.UserId = User.Identity.GetUserId(); 
            return PartialView("BeeTemplate");
        }

        public ActionResult GetTemplateById(int? id, string type = "")
        {
            TemplateViewModel templateViewModel = new TemplateViewModel();
            try
            {
                string APIURLUser = string.Format("{0}/{1}/{2}?id={3}&type={4}", Constants.ApiURL, "Template", "GetTemplateById", id, type);
                var countUser = (new APICallHelper()).Get(APIURLUser);
                if (countUser.StatusCode == HttpStatusCode.OK)
                {
                    var templates = Newtonsoft.Json.JsonConvert.DeserializeObject<TemplateViewModel>(countUser.Content.ReadAsStringAsync().Result.ToString());
                    templateViewModel = templates;
                    if (type == "layout")
                    {
                        string FilePath = (Server.MapPath("~/UserUpload/BeeTemplateJson/" + templateViewModel.TemplateName + ".json"));
                        using (StreamReader r = new StreamReader(FilePath))
                        {
                            templateViewModel.TemplateJSON = r.ReadToEnd();
                        }
                    }
                    return Json(templates, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(false);
            }
            return Json(false);
        }

        [HttpGet]
        public ActionResult AuthorizeBEE()
        {
            var httpRequst = HttpContext.Request;

            // TODO: configuration to get the Bee Authorizator Server  end-point
            String BeeEndPointAuthorizatorServer = ConfigurationManager.AppSettings["appAuth"];

            // Create request to get the Authorization from Server Bee
            HttpWebRequest request = CreateWebRequestToBeeAuthorizatorServer(BeeEndPointAuthorizatorServer);

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                // If response is not 200... throw new App Exception
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }
                // grab the response  
                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        // read the response from AuthorizatorServer 
                        string respBeeAuthorizatorServer = reader.ReadToEnd();
                        // Return the response
                        var result = JObject.Parse(respBeeAuthorizatorServer).ToString();
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
            }

        }

        // Bee Template Save Content
        [HttpPost]
        public ActionResult SaveBeeTemplateContent(TemplateViewModel templateViewModel)
        {
            if (ModelState.IsValid)
            {
                var UserName = User.Identity.GetUserName();
                var UserId = User.Identity.GetUserId();
                var template = DataHelper.SaveTemplateContent(templateViewModel, UserName , UserId);
                return Json(template, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #region Bee Private method
        /// <summary>
        ///     Method used to prepare requesto to Bee Authorizator Server
        /// </summary>
        /// <param name="endPoint">The end-point to call</param>
        /// <returns>The request to send to Bee Authorizator Server</returns>
        private HttpWebRequest CreateWebRequestToBeeAuthorizatorServer(string endPoint)
        {
            String BeePluginClientId = ConfigurationManager.AppSettings["BeePluginClientId"];
            String BeePluginClientSecret = ConfigurationManager.AppSettings["BeePluginClientSecret"];
            byte[] data = Encoding.UTF8.GetBytes(String.Format("grant_type=password&client_id={0}&client_secret={1}", BeePluginClientId, BeePluginClientSecret));

            // Create request
            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            // parametrization of request
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            // put body for currrent request to POST
            using (Stream s = request.GetRequestStream())
            {
                s.Write(data, 0, data.Length);
                s.Close();
            }

            return request;
        }

        public ActionResult DeleteTemplate(string id)
        {

            try
            {
                var APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "Template", id);
                var result = (new APICallHelper()).Delete(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                   
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Template has been deleted successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult CheckDuplicacy(string templateName , int TemplateId)
        {
            if (templateName != null)
            {
                var CheckDuplicateTemplate = DataHelper.CheckDuplicateAdminTemplate(templateName, TemplateId);
                if (CheckDuplicateTemplate == true)
                {
                    return this.Json(new
                    {
                        status = Constants.Success,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.Error,
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}