﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Helpers;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class SpendController : Controller
    {
   
        public ActionResult Index()
        {
          
            ViewData["ddlMonths"] = DataHelper.GetMonths(1);
            ViewData["ddlYears"] = DataHelper.GetCreditCardYear(1);
            ViewData["ddlDays"] = DataHelper.GetDays(1);
            return View();
        }
        [HttpPost]
        public JsonResult getSpendData(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText)
        {

            try
            {
                string APIURL = string.Format("{0}/{1}?&startDate={2}&endDate={3}&startMonth={4}&endMonth={5}&startYear={6}&endYear={7}&SearchText={8}", Constants.ApiURL, "AdminSpend", startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<USP_GetSpendData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);

            }
        }
        public ActionResult Export(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText)
        {

            string APIURL = string.Format("{0}/{1}?&startDate={2}&endDate={3}&startMonth={4}&endMonth={5}&startYear={6}&endYear={7}&SearchText={8}", Constants.ApiURL, "AdminSpend", startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<List<USP_GetSpendData_Result>>(result.ToString());
            DataTable dt = data.Select(r => new { r.SaleRepName, r.AccountName, r.ClientType, r.MemberType, r.PromotionCode, r.SpentTotal, r.IsActive, r.RegisteredDate, r.ContactEmail, r.MobilePhone ,r.January,
                                                  r.February,r.March,r.April,r.May,r.June,r.July,r.August,r.September,r.October,r.November,r.December}).ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "AdminSpend");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AdminSpend.xlsx");
                }
            }
        }

        public JsonResult AutoFillUserName(string prefix)
        {
            try
            {
                var context = new EmailMarketingDbEntities();
                var Name = (from N in context.UserProfiles
                                where N.FirstName.StartsWith(prefix)
                                select new { N.FirstName }).ToList();
                return Json(Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);

            }

        }
    }
}