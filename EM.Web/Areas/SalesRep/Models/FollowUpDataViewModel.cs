﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Models
{
    public class FollowUpDataViewModel
    {
        public usp_getUserDetails_Result ClientDetails { get; set; }
        public SelectList ClientTypeList { get; set; }

        public SelectList SalesRepList { get; set; }

        public SelectList PlanList { get; set; }

        public string ReferralCode { get; set; }
    }
}