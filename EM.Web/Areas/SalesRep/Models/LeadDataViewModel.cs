﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Models
{
	public class LeadDataViewModel
	{
		public SalesPortal_Leads salesPortal_Leads { get; set; }

		public SalesPortal_Providers providers { get; set; }
        public IEnumerable<SelectListItem> IndustryList { get; set; }
        public IEnumerable<SelectListItem> LeadsQualityList { get; set; }
        public IEnumerable<SelectListItem> ProvidersList { get; set; }

        public string[] SelectedProviders { get; set; }
    }
}