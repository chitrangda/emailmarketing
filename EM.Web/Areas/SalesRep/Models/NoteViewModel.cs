﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Models
{
    public class NoteViewModel
    {
        public SalesPortalRepNote newNote { get; set; }

        public List<usp_getUserNoteList_Result> noteList { get; set; }
        public IEnumerable<SelectListItem> TimeZoneList { get; set; }
    }
}