﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Models
{
    public class DashboardViewModel
    {
        public usp_getSalesRepTodays_Result todaysData { get; set; }

        public SelectList ClientTypeList { get; set; }

        public SelectList CampaignList { get; set; }

        public SelectList SalesRepList { get; set; }

        public bool canExport { get; set; }

        public bool repActivity { get; set; }
        public SelectList MemberTypeList { get; set; }





    }
}