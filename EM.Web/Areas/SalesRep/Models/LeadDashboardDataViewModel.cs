﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Models
{
    public class LeadDashboardDataViewModel
    {
        public USP_GetLeadDashBoard_Result LeadDashboardData {get; set;}

        public SalesPortal_Leads salesPortal_Leads { get; set; }

        public List<USP_GetLeadEmail_Result> emailData { get; set; }

        public List<USP_GetLeads_MobileNumList_Result> mobileData { get; set; }

        public SalesPortal_Providers providers { get; set; }

        public SalesPortal_Leads_EmailIdList idList { get; set; }

        public SalesPortal_Leads_MobileNumList numList { get; set; }
        public IEnumerable<SelectListItem> LeadQuality { get; set; }

        public IEnumerable<SelectListItem> LeadColor { get; set; }
        public IEnumerable<SelectListItem> State { get; set; }
        public IEnumerable<SelectListItem> Industry { get; set; }

        public IEnumerable<SelectListItem> SalesRep { get; set; }
        public IEnumerable<SelectListItem> ProvidersList { get; set; }

        public string[] SelectedProviders { get; set; }

    }
}