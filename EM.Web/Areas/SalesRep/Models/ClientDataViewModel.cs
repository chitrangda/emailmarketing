﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Models
{
    public class ClientDataViewModel
    {
        public usp_getUserDetails_Result ClientDetails { get; set; }
        public SelectList ClientTypeList { get; set; }

        public SelectList CampaignList { get; set; }

        public SelectList SalesRepList { get; set; }

        public SelectList PlanList { get; set; }

        public string ReferralCode { get; set; }

        public bool CSV_Upload { get; set; }

        public IEnumerable<SelectListItem> TimeZone { get; set; }

        public SelectList IPPoolList { get; set; }

        public SelectList SubscriberList { get; set; }
    }
}