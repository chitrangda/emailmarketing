﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class FollowUpController : Controller
    {
        // GET: SalesRep/FollowUp
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FollowUp(string id = null)
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetFollowUp(DateTime? dateFrom, DateTime? dateTo,bool? IsCurrent)
        {

            var salesepDetails = CurrentUser.getSalesRep();
            String APIURL = string.Format("{0}/{1}?id={2}&DateFrom={3}&DateTo={4}&IsCurrent={5}", Constants.ApiURL, "FollowUp/GetGridData", salesepDetails.Id,dateFrom,dateTo,IsCurrent);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            return Json(JsonConvert.DeserializeObject<List<getSalesRepFollowUp_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult EditFollowUp(string id)
        {
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "FollowUp/GetClientData", id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<usp_getUserDetails_Result>(result.ToString());
            ClientDataViewModel model = new ClientDataViewModel();
            model.ClientDetails = data;
            model.ClientTypeList = DataHelper.GetClientTypeSelectList(data.ClientType);
            model.SalesRepList = DataHelper.GetSalesRep(data.SalesRepId.ToString());
            model.PlanList = DataHelper.GetPlanSelectList(data.PlanId.ToString());
            model.SubscriberList = DataHelper.GetSubscriberList(model.ClientDetails.NoOfSubscribersMax, model.ClientDetails.PlanId);
            return PartialView("EditFollowUpView", model);

        }

        [HttpPost]
        public JsonResult UpdateFollowUp(ClientDataViewModel model)
        {
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "FollowUp");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.ClientDetails)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Updated successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetNoteView(string userid)
        {
            NoteViewModel model = new NoteViewModel();
            model.newNote = new SalesPortalRepNote() { UserId = userid };
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Notes", userid);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.noteList = JsonConvert.DeserializeObject<List<usp_getUserNoteList_Result>>(result.ToString());

            }
            model.TimeZoneList = DataHelper.GetTimeZone(model.newNote.TimeZone);
            return PartialView("AddEditNotes", model);
        }
        [HttpPost]
        public ActionResult AddNote(NoteViewModel model)
        {

            var UserId = User.Identity.GetUserId();
            model.newNote.LastModById = UserId;
            model.newNote.CreatedById = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Notes");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.newNote)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {

                var salesRepDetails = CurrentUser.getSalesRep();
                Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                salesport_Activity.RepId = salesRepDetails.Id;
                salesport_Activity.Category = "Create Note";
                salesport_Activity.Description = model.newNote.NoteId ==0 ?salesRepDetails.FirstName + " Note Added Successfully to " + model.newNote.NoteDescription : salesRepDetails.FirstName + " Note Updated Successfully to " + model.newNote.NoteDescription;
                salesport_Activity.CreatedDate = DateTime.UtcNow;
                salesport_Activity.CreatedBy = salesRepDetails.Id;
                salesport_Activity.NoteId = model.newNote.NoteId;
                DataHelper.InsertRepActivity(salesport_Activity);

                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.newNote.NoteId == 0 ? "Note has been successfully created" : "Note has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNote(int noteId)
        {
            string APIURL = string.Format("{0}/{1}?id={2}&userid={3}", Constants.ApiURL, "Notes", noteId, User.Identity.GetUserId());
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Note has been successfully deleted",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]

        public JsonResult IsComplete(string id)
        {
            try
            {
                var context = new EmailMarketingDbEntities();
                var userProfile = context.UserProfiles.Where(x => x.Id == id).FirstOrDefault();
                if (userProfile != null)
                {
                    userProfile.FollowupDate = null;
                    userProfile.FollowupTime = null;
                    context.SaveChanges();
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = "Updated successfully.",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception )
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult ExportList(DateTime? DateFrom , DateTime? DateTo , bool? IsCurrent)
        {
            var salesepDetails = CurrentUser.getSalesRep();
            String APIURL = string.Format("{0}/{1}?id={2}&DateFrom={3}&DateTo={4}&IsCurrent={5}", Constants.ApiURL, "FollowUp/GetGridData", salesepDetails.Id, DateFrom, DateTo, IsCurrent);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<List<getSalesRepFollowUp_Result>>(result.ToString());
            DataTable dt = data.Select(r => new { r.FollowUpDate, r.FollowUpTime, r.RepName,r.AccountName,r.ClientType,r.PlanName,r.PromotionCode,r.totalSpendCredits,r.AccountStatus,r.RegisteredDate}).ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "FollowUp");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SalesRepFollowUp.xlsx");
                }
            }
        }

    }
}



  