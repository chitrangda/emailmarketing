﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class LeadsController : Controller
    {

        // GET: SalesRep/Leads
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateNewLead()
        {
            LeadDataViewModel model = new LeadDataViewModel();
            model.IndustryList = DataHelper.GetIndustryList("");
            model.LeadsQualityList = DataHelper.GetLeadsQuality("");
            model.ProvidersList = DataHelper.GetProviders("");
            return View("CreateNewLead", model);
        }
        [HttpPost]
        public ActionResult CreateNewLead(LeadDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.salesPortal_Leads.UpdatedBy = UserId;
            model.salesPortal_Leads.CreatedBy = UserId;
            model.salesPortal_Leads.SalesRepID = CurrentUser.getSalesRep().Id;
            if (model.SelectedProviders != null)
            {
                model.salesPortal_Leads.ProviderId = string.Join(",", model.SelectedProviders);
            }
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Leads");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.salesPortal_Leads)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                var salesRepDetails = CurrentUser.getSalesRep();
                Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                salesport_Activity.RepId = salesRepDetails.Id;
                salesport_Activity.Category = "Create Lead";
                salesport_Activity.Description = salesRepDetails.FirstName + "Created lead for" + "" + model.salesPortal_Leads.FirstName + " " + model.salesPortal_Leads.LastName + ".";
                salesport_Activity.CreatedDate = DateTime.UtcNow;
                salesport_Activity.CreatedBy = salesRepDetails.Id;
                DataHelper.InsertRepActivity(salesport_Activity);
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Lead Created Successfully!"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult AddProviders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEditProviders(LeadDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.providers.CreatedBy = UserId;
            model.providers.LastModBy = UserId;
            var provider = DataHelper.CheckDuplicateProviders(model.providers.ProviderName);
            if (provider == true)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Provider Name Already Exists",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Providers");
                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.providers)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    var salesRepDetails = CurrentUser.getSalesRep();
                    Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                    salesport_Activity.RepId = salesRepDetails.Id;
                    salesport_Activity.Category = "Add Provider";
                    salesport_Activity.Description = salesRepDetails.FirstName + " Added Provider" + "" + model.providers.ProviderName;
                    salesport_Activity.CreatedDate = DateTime.UtcNow;
                    salesport_Activity.CreatedBy = salesRepDetails.Id;
                    DataHelper.InsertRepActivity(salesport_Activity);
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = "Provider Created Successfully!"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }

            }
        }
    }
}