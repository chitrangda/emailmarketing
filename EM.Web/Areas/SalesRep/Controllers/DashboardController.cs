﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EM.Web.Extentions;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class DashboardController : Controller
    {
        // GET: SalesRep/Dashboard
        public ActionResult Index(string id = null)
        {
            usp_getSalesRepTodays_Result model = new usp_getSalesRepTodays_Result();
            DashboardViewModel data = new DashboardViewModel();

            string salesRepId = User.Identity.GetUserId();
            if (id != null)
            {
                salesRepId = id;
                CurrentUser.createCurUserSession(UserRole.SalesRep.ToString(), id);
            }
            var salesepDetails = CurrentUser.getSalesRep();
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "SalesRepData", salesRepId);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model = JsonConvert.DeserializeObject<usp_getSalesRepTodays_Result>(result.ToString());
                data.todaysData = model;
                //data.todaysData.BillDeclined = model.BillDeclined;
                //data.todaysData.ClientBilling = model.ClientBilling;
                //data.todaysData.TotalClient = model.TotalClient;
                //data.todaysData.BillDue = model.BillDue;
                //data.todaysData.PacakagePurchase = model.PacakagePurchase;
                //data.todaysData.PackageDeclined = model.PackageDeclined;
                //data.todaysData.AllClients = model.AllClients;
                data.ClientTypeList = DataHelper.GetClientTypeSelectList("");
                data.CampaignList = DataHelper.GetCampaignSelectList("");
                data.SalesRepList = DataHelper.GetSalesRep("");
                data.MemberTypeList = DataHelper.GetPlanSelectList("");
                data.canExport = salesepDetails.CanExport != null ? salesepDetails.CanExport.Value : false;
                data.repActivity = salesepDetails.ViewRepActivity != null ? salesepDetails.ViewRepActivity.Value : false;

            }
            return View(data);
        }


        [HttpPost]
        public JsonResult GetSalesRepData()
        {
            try
            {
                var salesRepData = Session["SalesRepDetails"] as SalesRepInfoViewModel;
                var userId = string.Empty;
                if (salesRepData is null || salesRepData.Id is null)
                {
                    userId = CurrentUser.getCurSalesRepId();
                }
                else
                {
                    userId = salesRepData.Id;
                }
                String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "SalesRepData/GetGridData", userId);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepDashboardData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult FilterSalesRepData(string key, string value, string clientType, string memberType)
        {
            try
            {
                value = value == "" ? null : value;
                clientType = clientType == "" ? null : clientType;
                memberType = memberType == "Select" ? null : memberType;
                var salesRepData = Session["SalesRepDetails"] as SalesRepInfoViewModel;
                var userId = string.Empty;
                if (salesRepData is null || salesRepData.Id is null)
                {
                    userId = CurrentUser.getCurSalesRepId();
                }
                else
                {
                    userId = salesRepData.Id;
                }
                String APIURL = string.Format("{0}/{1}?id={2}&key={3}&value={4}&clientType={5}&MemberType={6}", Constants.ApiURL, "SalesRepData/FilterSalesRepData", userId, key, value, clientType, memberType);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepDashboardData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult GetNoteView(string userid)
        {
            NoteViewModel model = new NoteViewModel();
            model.newNote = new SalesPortalRepNote() { UserId = userid };
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Notes", userid);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.noteList = JsonConvert.DeserializeObject<List<usp_getUserNoteList_Result>>(result.ToString());

            }
            model.TimeZoneList = DataHelper.GetTimeZone(model.newNote.TimeZone);
            return PartialView("AddEditNotes", model);
        }
        [HttpPost]
        public ActionResult AddNote(NoteViewModel model)
        {

            var UserId = User.Identity.GetUserId();
            model.newNote.LastModById = UserId;
            model.newNote.CreatedById = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Notes");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.newNote)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {

                // var salesRepDetails = CurrentUser.getSalesRep();
                // Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                // //salesport_Activity.Username = ;
                //// salesport_Activity.UserId = model.newNote.UserId;
                // salesport_Activity.RepId = salesRepDetails.Id;
                // salesport_Activity.Category = "Create Note";
                // salesport_Activity.Description = salesRepDetails.Email.ToString() + " Note Added Successfully to " + model.newNote.UserProfile.FirstName + " " + model.newNote.UserProfile.LastName + ".";
                // salesport_Activity.CreatedDate = DateTime.UtcNow;
                // salesport_Activity.CreatedBy = salesRepDetails.Id;
                // salesport_Activity.NoteId = model.newNote.NoteId;
                // DataHelper.InsertRepActivity(salesport_Activity);

                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.newNote.NoteId == 0 ? "Note has been successfully created" : "Note has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNote(int noteId)
        {
            string APIURL = string.Format("{0}/{1}?id={2}&userid={3}", Constants.ApiURL, "Notes", noteId, User.Identity.GetUserId());
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Note has been successfully deleted",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult GetEditClientView(string id)
        {
            var salesRepData = Session["SalesRepDetails"] as SalesRepInfoViewModel;
            var curSalesRepId = string.Empty;
            if (salesRepData is null || salesRepData.Id is null)
            {
                curSalesRepId = CurrentUser.getCurSalesRepId();
            }
            else
            {
                curSalesRepId = salesRepData.Id;
            }
            String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "SalesRepData/GetClientData", id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<usp_getUserDetails_Result>(result.ToString());
            ClientDataViewModel model = new ClientDataViewModel();
            model.ClientDetails = data;
            model.ClientTypeList = DataHelper.GetClientTypeSelectList(data.ClientType);
            model.CampaignList = DataHelper.GetCampaignSelectList(data.CampaignTypeId.ToString());
            model.SalesRepList = DataHelper.GetSalesRep(data.SalesRepId.ToString());
            model.PlanList = DataHelper.GetPlanSelectList(data.PlanId.ToString());
            model.TimeZone = DataHelper.GetTimeZone(data.TimeZoneName);
            model.IPPoolList = DataHelper.GetIPPoolList(data.IPPoolId.ToString());
            model.SubscriberList = DataHelper.GetSubscriberList(data.NoOfSubscribersMax, data.PlanId);
            if (model.ClientDetails.MonthlyCredit == 0.ToString())
            {
                model.ClientDetails.MonthlyCredit = "Unlimited Emails";
            }
            return PartialView("EditClientView", model);

        }

        [HttpPost]
        public JsonResult UpdateClientData(ClientDataViewModel model)
        {
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "SalesRepData");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.ClientDetails)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.Success,
                    msg = "Save successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult AccountDetails(string id)
        {
            CurrentUser.createCurUserSession(UserRole.User.ToString(), id);
            AccountDetailsViewModel model = new AccountDetailsViewModel();



            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "ManageUser/GetAccountDetails", id);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                model = JsonConvert.DeserializeObject<AccountDetailsViewModel>(apiResult.ToString());
                model.UserId = id;
            }
            model.YearList = DataHelper.GetCreditCardYear(model.ExpYear);
            model.MonthList = DataHelper.GetMonths(model.ExpMonth);
            model.IndustryList = DataHelper.GetIndustryList(model.IndustryRefId.ToString());
            model.CountryList = DataHelper.GetCountryList(model.Country);
            if (model.Country == null)
            {
                model.Country = Constants.DefaultState;
            }
            model.Password = Encryption64.Decrypt(model.Password);
            model.StateList = DataHelper.GetStateList(model.State);
            model.listmodel = new ListViewModel();
            //return PartialView("AccountDetails", model);
            return View("AccountDetails", model);
        }

        [HttpPost]
        public ActionResult AccountDetails(AccountDetailsViewModel model)
        {
            model.CreatedById = User.Identity.GetUserId();
            model.LastModById = User.Identity.GetUserId();

            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser();
            var provider = new DpapiDataProtectionProvider("ASP.NET Identity");
            UserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("ASP.NET Identity"));
            user.Email = model.Email;
            if (UserManager.FindByEmail(user.Email) != null && model.VerifyPassword != null)
            {
                var token = UserManager.GeneratePasswordResetToken(model.UserId);
                var Result = UserManager.ResetPassword(model.UserId, token, model.VerifyPassword);
                if (Result.Succeeded)
                {
                    string res = updateProfile(model);
                    //String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageUser");
                    //var res = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(model)).Content.ReadAsStringAsync().Result;
                    if (res == "\"success\"")
                    {
                        ViewBag.msg = "Profile has been successfully updated";

                    }
                    else
                    {
                        ViewBag.msg = Constants.ErrorMessage;


                    }
                }
                else
                {
                    ViewBag.msg = Constants.ErrorMessage;

                }

            }
            else
            {
                string res = updateProfile(model);
                if (res == "\"success\"")
                {
                    ViewBag.msg = "Profile has been successfully updated";
                }
                else
                {
                    ViewBag.msg = Constants.ErrorMessage;
                }
            }
            model.YearList = DataHelper.GetCreditCardYear(model.ExpYear);
            model.MonthList = DataHelper.GetMonths(model.ExpMonth);
            model.IndustryList = DataHelper.GetIndustryList(model.IndustryRefId.ToString());
            model.CountryList = DataHelper.GetCountryList(model.Country);
            model.StateList = DataHelper.GetStateList(model.State);
            model.Password = Encryption64.Decrypt(model.Password);
            model.listmodel = new ListViewModel();
            //ListViewModel listmodel = new ListViewModel();
            //ViewBag.ListModel = listmodel;
            return View("AccountDetails", model);

        }

        public string updateProfile(AccountDetailsViewModel model)
        {
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "ManageUser");
            var res = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(model)).Content.ReadAsStringAsync().Result;
            return res;
        }


        public ActionResult ExportClient(string id)
        {
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "SalesRepData/GetClientData", id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;

            var data = JsonConvert.DeserializeObject<usp_getUserDetails_Result>(result.ToString());
            Salesportal_RepActivity(data);
            DataTable dt = ClientDetails(data);
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "List");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "UserAccount.xlsx");
                }
            }


        }
        private void Salesportal_RepActivity(usp_getUserDetails_Result data)
        {
            var salesRepDetails = CurrentUser.getSalesRep();

            Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();

            String RepID = salesRepDetails.Id;
            salesport_Activity.Username = data.FirstName + " " + data.LastName;
            salesport_Activity.UserId = data.Id;
            salesport_Activity.RepId = salesRepDetails.Id;
            salesport_Activity.Category = "Export Contact";
            salesport_Activity.Description = salesRepDetails.FirstName + " exported contacts of " + data.FirstName + " " + data.LastName + ".";
            salesport_Activity.CreatedDate = DateTime.UtcNow;
            salesport_Activity.CreatedBy = User.Identity.GetUserId();
            salesport_Activity.NoteId = null;
            DataHelper.InsertRepActivity(salesport_Activity);

        }

        DataTable ClientDetails(usp_getUserDetails_Result data)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("Email");
            dt.Columns.Add("Contact");
            dt.Columns.Add("City");
            dt.Columns.Add("State");
            dt.Columns.Add("Country");
            dt.Columns.Add("Zip");
            DataRow dr = dt.NewRow();
            dr["FirstName"] = data.FirstName;
            dr["LastName"] = data.LastName;
            dr["Email"] = data.Email;
            dr["Contact"] = data.MobilePhone;
            dr["City"] = data.City;
            dr["State"] = data.State; ;
            dr["Country"] = data.Country;
            dr["Zip"] = data.Zip;
            dt.Rows.Add(dr);
            return dt;
        }


        public ActionResult ViewUserActivity(string id = null)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]

        public ActionResult GetUserActivity(string Userid, DateTime? fromDate, DateTime? ToDate)
        {
            string APIURL = string.Format("{0}/{1}?Userid={2}&fromDate={3}&ToDate{4}", Constants.ApiURL, "ManageUser/GetUserActivityLog", Userid, fromDate, ToDate);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                return Json(JsonConvert.DeserializeObject<List<USP_UserActivityLog_Result>>(apiResult.ToString()), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "",
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //-----------ImportContact-------------------//


        [HttpPost]
        public JsonResult getList(string UserId)
        {
            String Api = string.Format("{0}/{1}?Id={2}", Constants.ApiURL, "ImportListContact", UserId);
            var lst = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (lst != null)
            {
                var listModel = JsonConvert.DeserializeObject<List<EM.Factory.ViewModels.ListViewModel>>(lst.ToString());
                return Json(listModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        [ActionName("ImportSubscriberList")]
        public ActionResult ImportSheetSubscriberList(int? id)
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    var UserId = CurrentUser.getCurUserId();
                    var subscriberList = Request.Files[0];
                    var ExcelFilePath = subscriberList.SaveExcelFile();
                    var ServerPath = Server.MapPath("~");
                    var FilePath = ServerPath + ExcelFilePath;
                    var listSubscriber = DataHelper.CheckSubscriberListFromExcel(FilePath, UserId, id);
                    string header = listSubscriber.FirstOrDefault().validHeaders;
                    if (listSubscriber.FirstOrDefault().countInvalidheader == true)
                    {
                        //var header = string.Join(", ", from item in listSubscriber select item.invalidHeaders);
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = $"The file that you are uploading has invalid headers.<p>Please upload excel with correct headers!</p><p><a href='/UserUpload/SubscriberListSample.xlsx'>Download Excel Format For Subscriber</a></p>",
                            URL = ""
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (listSubscriber.FirstOrDefault().invalidEmail == true)
                    {
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = $"The file that you are uploading has invalid email header.<p><a href='/UserUpload/SubscriberListSample.xlsx'>Download Excel Format For Subscriber</a></p>",
                            URL = ""
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (listSubscriber.Count > 0)
                    {
                        if (listSubscriber.Count <= Convert.ToInt32(Constants.SubscriberLimit))
                        {
                            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/SubscriberListPost1");
                            try
                            {
                                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listSubscriber));
                                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    // var subscribers = subscriberViewModels.Select(x => new Contacts { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).ToList();
                                    //SendGridEmails sendGrid = new SendGridEmails();
                                    //sendGrid.SendGridMultipleSubscriberEmailSync("Welcome Email", EmailHtml.getMultipleSubscriberWelcomeEmail(CurrentUser.getCurUserDetails().FirstName + CurrentUser.getCurUserDetails().LastName), Constants.SupportEmail, Constants.SupportName , subscribers);
                                    CurrentUser.SetAuthoriztionInfo();
                                    //DataHelper.CleanExcel(FilePath);
                                    var response = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                                    if (response.FirstOrDefault().result == 0)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Error,
                                            msg = "Please upgrade your plan to add more subscriber!"
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    if (response.FirstOrDefault().result == 1)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Success,
                                            msg = $"{header} data imported Successfully."
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    if (response.FirstOrDefault().result == 3)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Success,
                                            msg = "The subscribers you are uploading have already exists in your list!"
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    if (response.FirstOrDefault().result == 2)
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Error,
                                            msg = Constants.ErrorMessage
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        return Json(new
                                        {
                                            status = Constants.Success,
                                            msg = "Import Successful",
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                return Json(new
                                {
                                    status = Constants.Error,
                                    msg = Constants.ErrorMessage,
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = Constants.Error,
                                msg = "Please upload records in bunches of " + Constants.SubscriberLimit,
                                URL = FilePath
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = "The file that you are uploading has no records!",
                        }, JsonRequestBehavior.AllowGet);
                    }

                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = Constants.Error,
                        msg = ex.Message,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(new
            {
                status = Constants.Error,
                msg = "The file that you are uploading has no records!",
                URL = ""
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SaveImportSheetSubscribers(string param1, string param2, string _userId)
        {
            ViewBag.ListId = !string.IsNullOrEmpty(param2) ? Convert.ToInt32(param2) : 0;
            ViewBag.ExcelFilePath = param1;
            ViewBag.userId = _userId;
            var UserId = User.Identity.GetUserId();// CurrentUser.getCurUserId();
            var listSubscriber = DataHelper.GetSubscriberListFromExcel(param1, UserId, 0);
            return View("SaveImportSaleRepSheetSubscribers", listSubscriber);
        }
        [HttpPost]
        public ActionResult SaveImportSheetSubscribers(string param1, int? param2, string _userId)
        {
            if (param1 != null && param2 != null && _userId != null)
            {
                SubscriberImportResponseViewModel subscriberErrorResponseViewModel = new SubscriberImportResponseViewModel();
                List<SubscriberViewModel> subscriberViewModels = new List<SubscriberViewModel>();
                List<SubscriberViewModel> inValidSubscriberViewModels = new List<SubscriberViewModel>();
                var UserId = User.Identity.GetUserId();// CurrentUser.getCurUserId();
                var listSubscriber = DataHelper.GetSubscriberListFromExcel(param1, UserId, param2);
                foreach (var item in listSubscriber)
                {
                    var isValid = DataHelper.ValidateSubscriber(item);
                    if (isValid)
                    {

                        item.CreatedById = _userId.ToString();
                        if (item.Email.Length < 250)
                        {
                            subscriberViewModels.Add(item);
                        }
                    }
                    else
                    {
                        inValidSubscriberViewModels.Add(item);
                    }
                }
                subscriberErrorResponseViewModel.InvalidSubscribers = inValidSubscriberViewModels;
                if (subscriberViewModels.Count > 0)
                {
                    //string APIURL1 = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/ExistSubscriber");
                    //try
                    //{
                    //    var result = (new APICallHelper()).Post(APIURL1, JsonConvert.SerializeObject(subscriberViewModels));
                    //    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    //    {
                    //        var ExistSubscribers = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                    //        //subscriberViewModels.RemoveAll(x => ExistSubscribers.Any(y => y.Email == x.Email));
                    //        //subscriberErrorResponseViewModel.SuccessSubscribers = subscriberViewModels;
                    //        subscriberErrorResponseViewModel.DuplicateSubscribers = ExistSubscribers;
                    //        //return Json(new
                    //        //{
                    //        //    status = Constants.Success,
                    //        //    msg = "Import Successful",
                    //        //    Content = subscriberErrorResponseViewModel
                    //        //}, JsonRequestBehavior.AllowGet);
                    //    }
                    //}
                    //catch (Exception ex)
                    //{

                    //    return Json(new
                    //    {
                    //        status = Constants.Error,
                    //        msg = ex.Message,
                    //        Content = subscriberErrorResponseViewModel
                    //    }, JsonRequestBehavior.AllowGet);
                    //}
                    string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/SubscriberListPost1");
                    try
                    {
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(subscriberViewModels));
                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            DataHelper.CleanExcel(param1);
                            //var duplicateSubscribers = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                            //subscriberViewModels.RemoveAll(x => duplicateSubscribers.Any(y => y.Email == x.Email));
                            //subscriberErrorResponseViewModel.SuccessSubscribers = subscriberViewModels;
                            //subscriberErrorResponseViewModel.DuplicateSubscribers = duplicateSubscribers;
                            return Json(new
                            {
                                status = Constants.Success,
                                msg = "Import Successful",
                                Content = subscriberErrorResponseViewModel
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {

                        return Json(new
                        {
                            status = Constants.Error,
                            msg = ex.Message,
                            Content = subscriberErrorResponseViewModel
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = Constants.Error,
                    msg = "The file that you are uploading have no valid emails! ",
                    Content = subscriberErrorResponseViewModel,
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage,
                    Content = "",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        // Add List 
        [HttpPost]
        public ActionResult AddList(string ListName, string UserId)
        {
            ListViewModel listViewModel = new ListViewModel();
            if (ModelState.IsValid)
            {

                try
                {
                    //var UserId = User.Identity.GetUserId();// CurrentUser.getCurUserId();
                    listViewModel.CreatedById = UserId;
                    //listViewModel.LastModById = User.Identity.GetUserId();
                    listViewModel.ListName = ListName;
                    if (listViewModel.ListId == 0)
                    {
                        String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserList");
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listViewModel)).StatusCode;
                        if (result == System.Net.HttpStatusCode.OK)
                        {
                            //------------User Activity Log-------------//
                            var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                            UserActivityLog userActivtyObj = new UserActivityLog();
                            // userActivtyObj.UserId = getCurrentUserDetails.Id;
                            userActivtyObj.Createdby = listViewModel.CreatedById;
                            userActivtyObj.Description = listViewModel.ListName + " List Added ";// For " + getCurrentUserDetails.FirstName + " " + getCurrentUserDetails.LastName + " by " + User.Identity.GetUserName();
                            userActivtyObj.DateTime = DateTime.Now;
                            var res = DataHelper.InsertUserActivity(userActivtyObj);
                            //------------------------------------------//
                            return this.Json(new
                            {
                                status = Constants.SuccessTitle,
                                msg = "List has been successfully created",
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }


                }
                catch (Exception ex)
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }

            }


            return this.Json(new
            {
                status = Constants.ErrorTitle,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);


        }

    }
}
