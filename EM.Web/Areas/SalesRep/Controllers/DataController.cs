﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class DataController : Controller
    {
        // GET: SalesRep/Data
        public ActionResult Index()
        {
            ViewData["ddlStates"] = DataHelper.GetStateList(null);
            ViewData["ddlPackage"] = DataHelper.GetPlanSelectList(null);
            ViewData["ddlTimeZone"] = DataHelper.GetTimeZone(null);
            ViewData["ddlMonths"] = DataHelper.GetMonths(1);
            ViewData["ddlYears"] = DataHelper.GetCreditCardYear(1);
            ViewBag.CampaignList = DataHelper.GetCampaignSelectList(null);
            return View();
        }

        [HttpPost]
        public JsonResult GetData()
        {
            try
            {
                var salesRepData = CurrentUser.getSalesRep();
                String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "SalesRepData/GetData", salesRepData.Id);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult SearchData(DateTime? FromDate, DateTime? ToDate, int? Year, string Quarter, int? Month, string City, string State, string Zip, string TimeZone, string Find, int? MemberType, bool? Status, int? CampaignType)
        {
            try
            {
                var salesRepData = CurrentUser.getSalesRep(); String APIURL = string.Format("{0}/{1}?id={2}&FromDate={3}&ToDate={4}&Year={5}&Quarter={6}&Month={7}&City={8}&State={9}&Zip={10}&TimeZone={11}&Find={12}&MemberType={13}&Status={14}&CampaignType={15}", Constants.ApiURL, "SalesRepData/GetData", salesRepData.Id, FromDate, ToDate, Year, Quarter, Month, City, State, Zip, TimeZone, Find, MemberType, Status, CampaignType);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }



    }
}