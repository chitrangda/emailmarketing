﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.Admin.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class ClientController : Controller
    {
        // GET: Admin/Client
        public ActionResult Index()

        { 
            return View();
        }



        public ActionResult ExportList()
        {
            String APIURL = string.Format("{0}/{1}?FirstName=&LastName=&MobilePhone=&City=&State=&Country=&Zip=&isActive=&StartIndex={2}&PageSize=", ConfigurationManager.AppSettings["ApiURL"], "UserInfo", 0);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var users = JsonConvert.DeserializeObject<List<UserInfoViewModel>>(result.ToString()).Select(r => new { r.FirstName, r.LastName, r.MobilePhone, r.City, r.State, r.Country, r.Zip, r.isActive });

            DataTable dt = users.ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "Users");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Users.xlsx");
                }
            }
        }

        public ActionResult createNewAccount(string id = null)
        {
            UserInfoViewModel model = new UserInfoViewModel();
            if(id != null)
            {
                var salesRepData = Session["SalesRepDetails"] as SalesRepInfoViewModel;
                var curSalesRepId = string.Empty;
                if (salesRepData is null || salesRepData.Id is null)
                {
                    curSalesRepId = CurrentUser.getCurSalesRepId();
                }
                else
                {
                    curSalesRepId = salesRepData.Id;
                }
                String APIURL = string.Format("{0}/{1}?RepId={2}&leadId={3}", Constants.ApiURL, "LeadDashBoard/EditLeadDetails", curSalesRepId, id);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<USP_GetLeadDashBoard_Result>(result.ToString());
                model.FirstName = data.FirstName;
                model.LastName = data.Lastname;
                model.CompanyName = data.Company;
                model.Email = data.Email;
                model.MobilePhone = data.MobileNo;
            }
            model.PlanList = DataHelper.GetPlanSelectList(model.planId.ToString());
            model.SalesRepList = DataHelper.GetSalesRepList(model.PromotionCode);
            model.RepName = CurrentUser.getSalesRep().FirstName;
            model.SalesRepId = CurrentUser.getSalesRep().Id;
            return PartialView("CreateNewAccount", model);
        }

        [HttpPost]
        public async Task<ActionResult> createNewAccount(UserInfoViewModel model)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var user = new ApplicationUser();
            user.UserName = model.Email;
            user.Email = model.Email;

            string userPWD = DataHelper.RandomString(8);

            if (UserManager.FindByEmail(user.Email) == null)
            {

                var chkUser = UserManager.Create(user, userPWD);


                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "User");

                    using (var db = new EmailMarketingDbEntities())
                    {
                        UserProfile oUser = new UserProfile();
                        oUser.Id = user.Id;
                        oUser.FirstName = model.FirstName;
                        oUser.LastName = model.LastName;
                        oUser.ContactEmail = user.Email;
                        oUser.City = model.City;
                        oUser.State = model.State;
                        oUser.Country = model.Country;
                        oUser.Zip = model.Zip;
                        oUser.MobilePhone = model.MobilePhone;
                        oUser.RegisteredDate = DateTime.Now;
                        oUser.IndustryRefId = model.IndustryRefId;
                        oUser.CompanyName = model.CompanyName;
                        oUser.IsActive = true;
                        oUser.IsCSVActive = true;
                        oUser.PromotionCode = model.PromotionCode;
                        oUser.SalesRepId = model.SalesRepId;
                        oUser.TimeZoneId = 3;
                        /*If user's email domain is in whitelabel then sendingfromemail will be same Email and if its different then it will add root domain to that email doamin*/
                        //var domain = user.Email.Split('@')[1];
                        //string emailDomain = "";
                        //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                        //{
                        //    model.CompanyName = await MailgunHelper.ReplaceSpecialCharWithBlank(model.CompanyName);
                        //    oUser.SendingFromEmail = await MailgunHelper.ReturnDomainforWhitelabelDomain(model.FirstName.ToLower(), model.CompanyName.ToLower());
                        //    emailDomain = oUser.SendingFromEmail.ToLower();
                        //}
                        //else
                        //{
                        //    oUser.SendingFromEmail = await MailgunHelper.ReturnMailWithDomain(model.Email);
                        //    emailDomain = oUser.SendingFromEmail.ToLower();
                        //}
                        //model.CompanyName = await MailgunHelper.ReplaceSpecialCharWithBlank(model.CompanyName);

                        //create user sending from emal as per mailgun domain
                        oUser.SendingFromEmail = await MailgunHelper.AddSubDomaininEmailWithLower(model.Email, model.CompanyName);
                        db.UserProfiles.Add(oUser);
                        db.SaveChanges();


                        string APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", model.SelectedPackakgeId);
                        var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                        if (apiResult != null)
                        {
                            var planDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(apiResult.ToString());

                            UserPlan oPlan = new UserPlan();
                            oPlan.UserId = user.Id;
                            oPlan.TotalEmails = planDetail.NoOfCredits;
                            oPlan.TotalDays = 30;
                            oPlan.PackageId = model.SelectedPackakgeId.Value;
                            oPlan.MonthlyCredit = planDetail.NoOfCredits.ToString();
                            oPlan.NoOfSubscribersMin = 0;
                            oPlan.NoOfSubscribersMax = planDetail.NoOfSubscribersMax;
                            oPlan.PackageUpdateDate = DateTime.Now;
                            oPlan.BillingAmount = planDetail.BillingAmount != null ? planDetail.BillingAmount.Value : 0;
                            oPlan.totalRemainingCredits = planDetail.NoOfCredits.ToString();
                            oPlan.ContactManagement = planDetail.ContactManagement;
                            oPlan.UnlimitedEmails = planDetail.UnlimitedEmails;
                            oPlan.EmailScheduling = planDetail.EmailScheduling;
                            oPlan.EducationalResources = planDetail.EducationalResources;
                            oPlan.LiveSupport = planDetail.LiveSupport;
                            oPlan.CustomizableTemplates = planDetail.CustomizableTemplates;
                            oPlan.IsActive = true;
                            oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                            oPlan.BillingStatus = 1;
                            oPlan.totalSpendCredits = "0";
                            db.UserPlans.Add(oPlan);
                            db.SaveChanges();
                            var SalesRepContact = db.SalesRepProfiles.Where(x => x.Id == model.SalesRepId).FirstOrDefault();
                            model.SalesRepContact = SalesRepContact.ContactNo;
                            model.Password = userPWD;
                            //MailgunHelper _mailGun = new MailgunHelper();
                            //await _mailGun.sendSingleEmail("", "Your Account Details", EmailHtml.getUserDetails(model.FirstName + " " + model.LastName, model.Password, model.Email, model.RepName, model.SalesRepContact), Constants.SupportEmail, Constants.SupportName, model.Email, model.FirstName + "" + model.LastName);

                            Mail oMail = new Mail();
                            await oMail.SendMail(model.Email, "Your Account Details", EmailHtml.getUserDetails(model.FirstName + " " + model.LastName, model.Password, model.Email, model.RepName, model.SalesRepContact), Constants.SupportName, "mailSettings/Support", (model.FirstName + "" + model.LastName));


                        }
                        var salesRepDetails = CurrentUser.getSalesRep();
                        Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                        salesport_Activity.Username = model.FirstName + " " + model.LastName;
                        salesport_Activity.UserId = user.Id;
                        salesport_Activity.RepId = salesRepDetails.Id;
                        salesport_Activity.Category = "Create Account - Package Allocation";
                        salesport_Activity.Description = salesRepDetails.FirstName + " Package Allocate Successfully to " + model.FirstName + " " + model.LastName + ".";
                        salesport_Activity.CreatedDate = DateTime.UtcNow;
                        salesport_Activity.CreatedBy = salesRepDetails.Id;
                        salesport_Activity.NoteId = null;
                        DataHelper.InsertRepActivity(salesport_Activity);

                        EmailConfirmation oEmail = db.EmailConfirmations.SingleOrDefault(r => r.UserId == user.Id && r.FromEmailAddress == user.Email);
                        if (oEmail == null)
                        {
                            oEmail = new EmailConfirmation();
                            oEmail.UserId = user.Id;
                            oEmail.FromEmailAddress = user.Email;
                            oEmail.Status = true;
                            oEmail.SendingFromEmail = oUser.SendingFromEmail.ToLower();
                            db.EmailConfirmations.Add(oEmail);
                            db.SaveChanges();
                        }
                            //var response = await verfiyDomainMailGun(emailDomain);
                            //if (response == true)
                            //{
                            //    /*To create route so that email should forwarded on original user email*/
                            //   // var res = await MailgunHelper.CreateRoute(emailDomain);
                            //    var webhook = await MailgunHelper.InsertWebHookLinks(emailDomain);
                            //var domainVerify = emailDomain.Split('@')[1];
                            //var domainSetting = await MailgunHelper.verifyDomainSettings(domainVerify);
                                return this.Json(new
                                {
                                    status = "",
                                    msg = "Account Created Successfully and Check your mail for your account details!"
                                }, JsonRequestBehavior.AllowGet);
                            //}
                            //else
                            //{
                            //    return this.Json(new
                            //    {
                            //        status = Constants.ErrorTitle,
                            //        msg = Constants.ErrorMessage
                            //    }, JsonRequestBehavior.AllowGet);
                            //}
                    }
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Email Id already exists!"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<bool> verfiyDomainMailGun(string email)
        {
            /*To check whether domain is verified or not*/
            var check = await MailgunHelper.IsVerifiedDomainMailGun(email);
            if (check == 0)
            {
                /*To create domain if its not created*/
                var response = await MailgunHelper.createDomainMailGun(email);

            }
            return true;
        }


    }
}

