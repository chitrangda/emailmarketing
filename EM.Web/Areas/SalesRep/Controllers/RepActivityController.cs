﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class RepActivityController : Controller
    {
        // GET: SalesRep/RepActivity
        public ActionResult Index(string id = null)
        {
            List<usp_getSalesRepActivity_Result> model = new List<usp_getSalesRepActivity_Result>();
            string salesRepId = User.Identity.GetUserId();
            if (id != null)
            {
                salesRepId = id;
                CurrentUser.createCurUserSession(UserRole.SalesRep.ToString(), id);
            }
            var salesepDetails = CurrentUser.getSalesRep();
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "RepActivity");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result!=null)
            {
                model = JsonConvert.DeserializeObject<List<usp_getSalesRepActivity_Result>>(result.ToString());
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult GetSaleRepActivity()
        {
            try
            {
                var salesRepData = CurrentUser.getSalesRep();
                String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "RepActivity");
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepActivity_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult GetSaleRepActivityData(DateTime? From, DateTime? To)
        {
            try
            {
                //api/RepActivity/GetGridData?RepId={RepId}&From={From}&To={To}
                var salesRepData = CurrentUser.getSalesRep();
                String APIURL = string.Format("{0}/{1}?RepId={2}&From={3}&To={4}", Constants.ApiURL, "RepActivity/GetGridData", salesRepData.Id, From, To);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<usp_getSalesRepActivity_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

    }
}
