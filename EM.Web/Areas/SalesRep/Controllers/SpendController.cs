﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Helpers;
using EM.Web.Utilities;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class SpendController : Controller
    {
        // GET: SalesRep/Spend
        public ActionResult Index()
        {
            ViewData["ddlMonths"] = DataHelper.GetMonths(1);
            ViewData["ddlYears"] = DataHelper.GetCreditCardYear(1);
            ViewData["ddlDays"] = DataHelper.GetDays(1);
            return View();
        }
        [HttpPost]
        public JsonResult getSalesRepSpendData(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText)
        {

            try
            {
                string APIURL = string.Format("{0}/{1}?&startDate={2}&endDate={3}&startMonth={4}&endMonth={5}&startYear={6}&endYear={7}&SearchText={8}&SalesRepId={9}", Constants.ApiURL, "SalesRepSpend", startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText,CurrentUser.getSalesRep().Id);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<USP_GetSpendData_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);

            }
        }
        public ActionResult ExportSalesRep(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText)
        {

            string APIURL = string.Format("{0}/{1}?&startDate={2}&endDate={3}&startMonth={4}&endMonth={5}&startYear={6}&endYear={7}&SearchText={8}&SalesRepId={9}", Constants.ApiURL, "SalesRepSpend", startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText, CurrentUser.getSalesRep().Id);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<List<USP_GetSpendData_Result>>(result.ToString());
            DataTable dt = data.Select(r => new {
                r.SaleRepName,
                r.AccountName,
                r.ClientType,
                r.MemberType,
                r.PromotionCode,
                r.SpentTotal,
                r.IsActive,
                r.RegisteredDate,
                r.ContactEmail,
                r.MobilePhone,
                r.January,
                r.February,
                r.March,
                r.April,
                r.May,
                r.June,
                r.July,
                r.August,
                r.September,
                r.October,
                r.November,
                r.December
            }).ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "SalesRepSpend");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SalesRepSpend.xlsx");
                }
            }
        }
        public JsonResult AutoFillUserName(string prefix)
        {
            try
            {
                var context = new EmailMarketingDbEntities();
                var SalesRepId = CurrentUser.getSalesRep().Id;
                var Name = (from N in context.UserProfiles
                            where N.FirstName.StartsWith(prefix) && N.SalesRepId == SalesRepId
                            select new { N.FirstName }).ToList();
                return Json(Name, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);

            }

        }

    }
}