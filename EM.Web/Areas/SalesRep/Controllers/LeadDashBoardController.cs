﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.SalesRep.Models;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace EM.Web.Areas.SalesRep.Controllers
{
    [Authorize(Roles = "Admin,SalesRep")]
    public class LeadDashBoardController : Controller
    {
        // GET: SalesRep/LeadDashBoard
        public ActionResult Index()
        {

            ViewBag.QualityOfLeades = DataHelper.GetQualityofLeadsList(null);
            ViewBag.IndustryList = DataHelper.GetIndustryList(null);
            return View();
        }

        [HttpPost]
        public JsonResult GetLeadDashBoardData(int? LeadQualityID, int? TypeOfLead, int? IndustryId, int? pageIndex, int? pageSize)
        {
            try
            {
                String APIURL = string.Format("{0}/{1}?RepId={2}&LeadQualityID={3}&TypeOfLead={4}&IndustryId={5}&pageIndex={6}&pageSize={7}",
                    Constants.ApiURL, "LeadDashBoard/LeadDashBoardData", CurrentUser.getSalesRep().Id, LeadQualityID, TypeOfLead, IndustryId, pageIndex, pageSize);

                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<USP_GetLeadDashBoard_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult ExportLeadDashBordData(int? LeadQualityID, int? TypeOfLead, int? IndustryId, int? pageIndex, int? pageSize)
        {
            try
            {
                //var salesRepData = CurrentUser.getSalesRep();

                String APIURL = string.Format("{0}/{1}?RepId={2}&LeadQualityID={3}&TypeOfLead={4}&IndustryId={5}&pageIndex={6}&pageSize={7}",
                    Constants.ApiURL, "LeadDashBoard/LeadDashBoardData", CurrentUser.getSalesRep().Id, LeadQualityID, TypeOfLead, IndustryId, pageIndex, pageSize);

                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;

                List<USP_GetLeadDashBoard_Result> data = JsonConvert.DeserializeObject<List<USP_GetLeadDashBoard_Result>>(result.ToString());
                DataTable dt = LeadDashBoardDetails(data);
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt, "List");
                    using (MemoryStream stream = new MemoryStream())
                    {
                        wb.SaveAs(stream);
                        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "LeadDashBoard.xlsx");
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        private DataTable LeadDashBoardDetails(List<USP_GetLeadDashBoard_Result> lst)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SalesRep");
            dt.Columns.Add("FollowUp");
            dt.Columns.Add("FollowUpTime");
            dt.Columns.Add("Name");
            dt.Columns.Add("DateAdded");
            dt.Columns.Add("Days");
            dt.Columns.Add("JobTitle");
            dt.Columns.Add("Company");
            dt.Columns.Add("Industry");
            dt.Columns.Add("Mobile");
            dt.Columns.Add("Phone");
            dt.Columns.Add("Email");
            dt.Columns.Add("Website");
            dt.Columns.Add("LinkedIn");
            dt.Columns.Add("Twitter");
            dt.Columns.Add("Instagram");
            dt.Columns.Add("Facebook");
            dt.Columns.Add("ProvidersName");
            dt.Columns.Add("HowHot?");
            dt.Columns.Add("OpportunityAmount");
            dt.Columns.Add("State");
            //dt.Columns.Add("Create Account");

            foreach (var item in lst)
            {
                DataRow dr = dt.NewRow();
                dr["SalesRep"] = item.SaleRepName;
                dr["FollowUp"] = item.FollowUpDate;
                dr["FollowUpTime"] = item.FollowUpTime;
                dr["Name"] = item.LeadName;
                dr["DateAdded"] = item.DateAdded;
                dr["Days"] = item.days;
                dr["JobTitle"] = item.JobTitle;
                dr["Company"] = item.Company;
                dr["Industry"] = item.IndustryName;
                dr["Mobile"] = item.MobileNo;
                dr["Phone"] = item.PhoneNo;
                dr["Email"] = item.Email;
                dr["Website"] = item.Website;
                dr["LinkedIn"] = item.LinkedIn;
                dr["Twitter"] = item.Twitter;
                dr["Instagram"] = item.Instagram;
                dr["Facebook"] = item.Facebook;
                dr["ProvidersName"] = item.ProviderName;
                dr["HowHot?"] = item.HotName;
                dr["OpportunityAmount"] = item.OpportunityAmount;
                dr["State"] = item.State;
                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }



            return dt;

        }

        public JsonResult IsComplete(int id)
        {
            try
            {
                var context = new EmailMarketingDbEntities();
                var leads = context.SalesPortal_Leads.Where(x => x.LeadId == id).FirstOrDefault();
                if (leads != null)
                {
                    leads.FollowUpDate = null;
                    leads.FollowupTime = null;
                    context.SaveChanges();
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = "Updated successfully.",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult GetEditLeadView(int leadId)
        {
            LeadDashboardDataViewModel model = new LeadDashboardDataViewModel();
            var salesRepData = Session["SalesRepDetails"] as SalesRepInfoViewModel;
            var curSalesRepId = string.Empty;
            if (salesRepData is null || salesRepData.Id is null)
            {
                curSalesRepId = CurrentUser.getCurSalesRepId();
            }
            else
            {
                curSalesRepId = salesRepData.Id;
            }
            String APIURL = string.Format("{0}/{1}?RepId={2}&leadId={3}", Constants.ApiURL, "LeadDashBoard/EditLeadDetails", curSalesRepId, leadId);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<USP_GetLeadDashBoard_Result>(result.ToString());
            model.LeadDashboardData = data;
            model.LeadQuality = DataHelper.GetQualityofLeadsList(model.LeadDashboardData.HotId.ToString());
            model.State = DataHelper.GetStateList(model.LeadDashboardData.State);
            model.Industry = DataHelper.GetIndustryList(model.LeadDashboardData.IndustryId.ToString());
            if (model.SelectedProviders != null)
            {
                model.SelectedProviders = model.LeadDashboardData.ProviderId.Split(',');
            }
            model.ProvidersList = DataHelper.GetProviders(null);
            model.SalesRep = DataHelper.GetSalesRep(model.LeadDashboardData.SalesRepID);
            model.LeadColor = DataHelper.GetLeadColor(model.LeadDashboardData.ColorId.ToString());
            return PartialView("EditLead", model);
        }

        [HttpPost]
        public ActionResult UpdateLeadData(LeadDashboardDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.LeadDashboardData.UpdatedBy = UserId;
            if (model.SelectedProviders != null)
            {
                model.LeadDashboardData.ProviderId = string.Join(",", model.SelectedProviders);
            }
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Leads");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.LeadDashboardData)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                var salesRepDetails = CurrentUser.getSalesRep();
                Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                salesport_Activity.RepId = salesRepDetails.Id;
                salesport_Activity.Category = "Update Lead";
                salesport_Activity.Description = salesRepDetails.FirstName + "Updated lead for" + "" + model.LeadDashboardData.FirstName + " " + model.LeadDashboardData.Lastname + ".";
                salesport_Activity.CreatedDate = DateTime.UtcNow;
                salesport_Activity.CreatedBy = salesRepDetails.Id;
                DataHelper.InsertRepActivity(salesport_Activity);
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Lead Updated Successfully!"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult GetNoteView(int leadId)
        {
            LeadNoteViewModel model = new LeadNoteViewModel();
            model.newNote = new SalesPortal_LeadNote() { LeadId = leadId };
            String APIURL = string.Format("{0}/{1}?LeadId={2}", Constants.ApiURL, "LeadNotes", leadId);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.noteList = JsonConvert.DeserializeObject<List<usp_getLeadNoteList_Result>>(result.ToString());
            }
            model.TimeZoneList = DataHelper.GetTimeZone(model.newNote.TimeZone);
            return PartialView("AddEditLeadsNotes", model);
        }
        [HttpPost]
        public ActionResult AddNote(LeadNoteViewModel model)
        {

            var UserId = User.Identity.GetUserId();
            model.newNote.LastModById = UserId;
            model.newNote.CreatedById = UserId;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "LeadNotes");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.newNote)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {

                var salesRepDetails = CurrentUser.getSalesRep();
                Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                salesport_Activity.RepId = salesRepDetails.Id;
                salesport_Activity.Category = "Create Lead Note";
                salesport_Activity.Description = (model.newNote.NoteId == 0) ? salesRepDetails.FirstName + " Lead Note Added Successfully to " + model.newNote.NoteDescription + "." : salesRepDetails.FirstName + " Lead Note Added Successfully to " + model.newNote.NoteDescription + ".";
                salesport_Activity.CreatedDate = DateTime.UtcNow;
                salesport_Activity.CreatedBy = salesRepDetails.Id;
                salesport_Activity.NoteId = model.newNote.NoteId;
                DataHelper.InsertRepActivity(salesport_Activity);

                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = model.newNote.NoteId == 0 ? "Note has been successfully created" : "Note has been successfully updated",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNote(int noteId)
        {
            string APIURL = string.Format("{0}/{1}?id={2}&userid={3}", Constants.ApiURL, "LeadNotes", noteId, User.Identity.GetUserId());
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Note has been successfully deleted",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }


       
        public ActionResult AddProviders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEditProviders(LeadDashboardDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.providers.CreatedBy = UserId;
            var provider = DataHelper.CheckDuplicateProviders(model.providers.ProviderName);
            if (provider == true)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Provider Name Already Exists",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Providers");
                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.providers)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    var salesRepDetails = CurrentUser.getSalesRep();
                    Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                    salesport_Activity.RepId = salesRepDetails.Id;
                    salesport_Activity.Category = "Add Provider";
                    salesport_Activity.Description = salesRepDetails.FirstName + " Added Provider" + "" + model.providers.ProviderName;
                    salesport_Activity.CreatedDate = DateTime.UtcNow;
                    salesport_Activity.CreatedBy = salesRepDetails.Id;
                    DataHelper.InsertRepActivity(salesport_Activity);
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = "Provider Created Successfully!"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }

            }
        }
    
        [HttpPost]
        public ActionResult GetLeadEmailView(int leadId , int? EmailRowId)
        {
            LeadDashboardDataViewModel model = new LeadDashboardDataViewModel();
            model.idList = new SalesPortal_Leads_EmailIdList() { LeadId = leadId };
            if (EmailRowId != null)
            {
                String APIURL = string.Format("{0}/{1}?leadId={2}&EmailRowId={3}", Constants.ApiURL, "SalesLeadEmail", leadId, EmailRowId);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<SalesPortal_Leads_EmailIdList>(result.ToString());
                model.idList = data;
                return PartialView("AddEditLeadEmail", model);
            }
            else
            {
                return PartialView("AddEditLeadEmail", model);
            }
           
        }
        [HttpPost]
        public JsonResult LeadEmailList(int leadId)
        {
            LeadDashboardDataViewModel model = new LeadDashboardDataViewModel();
            model.idList = new SalesPortal_Leads_EmailIdList() { LeadId = leadId };
            try
            {
                String APIURL = string.Format("{0}/{1}?leadId={2}", Constants.ApiURL, "SalesLeadEmail", leadId);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<USP_GetLeadEmail_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult AddEditLeadEmail(LeadDashboardDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.idList.CreatedBy = UserId;
            model.idList.UpdatedBy = UserId;
            var email = DataHelper.CheckDuplicateEmail(model.idList.Email);
            if (email == true)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Email Id Already Exists",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "SalesLeadEmail");
                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.idList)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = model.idList.EmailRowId == 0 ? "Added Successfully!" : "Updated Successfully",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }

        }

        [HttpPost]
        public JsonResult DeleteLeadEmail(int EmailId)
        {
            var APIURL = string.Format("{0}/{1}?leadId={2}", Constants.ApiURL, "SalesLeadEmail", EmailId);
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Deleted successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult setPrimaryEmail(int LeadId ,int EmailRowId)
        {
            var APIURL = string.Format("{0}/{1}?LeadId={2}&EmailRowId={3}", Constants.ApiURL, "SalesLeadEmail", LeadId,EmailRowId);
            var result = (new APICallHelper()).Put(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Updated successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }




        [HttpPost]
        public ActionResult GetLeadMobileView(int leadId, int? MobileRowId)
        {
            LeadDashboardDataViewModel model = new LeadDashboardDataViewModel();
            model.numList = new SalesPortal_Leads_MobileNumList() { LeadId = leadId };
            if (MobileRowId != null)
            {
                String APIURL = string.Format("{0}/{1}?leadId={2}&MobileRowId={3}", Constants.ApiURL, "SalesLeadMobile", leadId, MobileRowId);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var data = JsonConvert.DeserializeObject<SalesPortal_Leads_MobileNumList>(result.ToString());
                model.numList = data;
                return PartialView("AddEditLeadMobile", model);
            }
            else
            {
                return PartialView("AddEditLeadMobile", model);
            }

        }
        [HttpPost]
        public JsonResult LeadMobileList(int leadId)
        {
            LeadDashboardDataViewModel model = new LeadDashboardDataViewModel();
            model.numList = new SalesPortal_Leads_MobileNumList() { LeadId = leadId };
            try
            {
                String APIURL = string.Format("{0}/{1}?leadId={2}", Constants.ApiURL, "SalesLeadMobile", leadId);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<USP_GetLeads_MobileNumList_Result>>(result.ToString()), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult AddEditLeadMobile(LeadDashboardDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.numList.CreatedBy = UserId;
            model.numList.UpdatedBy = UserId;
            var mobile = DataHelper.CheckDuplicateMobile(model.numList.MobileNumber);
            if (mobile == true)
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = "Mobile Number Already Exists",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "SalesLeadMobile");
                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.numList)).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.SuccessTitle,
                        msg = model.numList.MobileRowId == 0 ? "Added Successfully!" : "Updated Successfully",
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = Constants.ErrorMessage
                    }, JsonRequestBehavior.AllowGet);

                }
            }

        }

        [HttpPost]
        public JsonResult DeleteMobile(int MobileId)
        {
            var APIURL = string.Format("{0}/{1}?leadId={2}", Constants.ApiURL, "SalesLeadMobile", MobileId);
            var result = (new APICallHelper()).Delete(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Deleted successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult setPrimaryMobile(int LeadId, int MobileRowId)
        {
            var APIURL = string.Format("{0}/{1}?LeadId={2}&MobileRowId={3}", Constants.ApiURL, "SalesLeadMobile", LeadId, MobileRowId);
            var result = (new APICallHelper()).Put(APIURL).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Updated successfully!",
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult EditLead(LeadDataViewModel model)
        {
            var UserId = User.Identity.GetUserId();
            model.salesPortal_Leads.UpdatedBy = UserId;
            model.salesPortal_Leads.CreatedBy = UserId;
            model.salesPortal_Leads.SalesRepID = CurrentUser.getSalesRep().Id;
            model.salesPortal_Leads.ProviderId = string.Join(",", model.SelectedProviders);
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Leads");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(model.salesPortal_Leads)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                var salesRepDetails = CurrentUser.getSalesRep();
                Salesportal_AdminRepActivity salesport_Activity = new Salesportal_AdminRepActivity();
                salesport_Activity.RepId = salesRepDetails.Id;
                salesport_Activity.Category = "Update Lead";
                salesport_Activity.Description = salesRepDetails.FirstName + "Updated lead for" + "" + model.salesPortal_Leads.FirstName + " " + model.salesPortal_Leads.LastName + ".";
                salesport_Activity.CreatedDate = DateTime.UtcNow;
                salesport_Activity.CreatedBy = salesRepDetails.Id;
                DataHelper.InsertRepActivity(salesport_Activity);
                return this.Json(new
                {
                    status = Constants.SuccessTitle,
                    msg = "Lead Updated Successfully!"
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = Constants.ErrorMessage
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}