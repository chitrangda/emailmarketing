﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Areas.User.Models
{
    public class OrganizationViewModel
    {
        public List<Organizations> organizations { get; set; }

        public PaginationOrg pagination { get; set; }
    }
    public class Organizations
    {
        public string type { get; set; }
        public string name { get; set; }
        public string vertical { get; set; }

        [JsonProperty(PropertyName = "parent_id")]
        public string parentid { get; set; }
        public string locale { get; set; }

        [JsonProperty(PropertyName = "image_id")]
        public string imageid { get; set; }

        public string id { get; set; }
    }

    public class PaginationOrg
    {
        [JsonProperty(PropertyName = "object_count")]
        public int objectcount { get; set; }

        [JsonProperty(PropertyName = "page_number")]
        public int pagenumber { get; set; }

        [JsonProperty(PropertyName = "page_size")]
        public int pagesize { get; set; }

        [JsonProperty(PropertyName = "page_count")]
        public int pagecount { get; set; }

        [JsonProperty(PropertyName = "has_more_items")]
        public bool hasmoreitems { get; set; }

        public string continuation { get; set; }


    }
}