﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Areas.User.Models
{
    public class SubscriberMenuViewmodel
    {
        public int? ListId { get; set; }

        public string ListName { get; set; }
    }
}