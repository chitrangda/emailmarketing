﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EM.Web.Areas.User.Models
{
    public class CampaignSummaryReportViewModel
    {
        public int CampaignId { get; set; }
        public string CampaignName { get; set; }

        public string UserId { get; set; }
        public int Subscribers { get; set; }
        public double Opens { get; set; }
        public double Clicks { get; set; }
        public double Delivered { get; set; }
        public int Sents { get; set; }
        public DateTime SentDate { get; set; }

        public int TotalRecords { get; set; }

        public string ListName { get; set; }
        public Nullable<int> Receipents { get; set; }

        public int Bounce { get; set; }
        public string Subject { get; set; }

        public int Unique_clicks { get; set; }
        public int Unique_opens { get; set; }

        public int Unsubscribes { get; set; }
        public int Spam_reports { get; set; }

        public double ClickPercentage { get; set; }

        public double OpenPercentage { get; set; }
        public double DeliveredPercentage { get; set; }

        public string LastClicked { get; set; }

        public string LastOpened { get; set; }
        public string HtmlContent { get; set; }

        public string FromName { get; set; }
        public string FromEmail { get; set; }

        public string PreviewText { get; set; }

        public List<usp_getCampaignRecipient_Result> emails { get; set; }


        public string EmailId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public double ClickPerUniqueOpenPercentage { get; set; }

        public string BounceList { get; set; }

        public int Dropped { get; set; }

        public int UniqueClickPer { get; set; }

        public string URL { get; set; }

        public int Stat { get; set; }




    }
}