﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Areas.User.Models
{
    public class AttendeesViewModel
    {
        public PaginationAttendees pagination { get; set; }

        public List<Attendees> attendees { get; set; }
    }

    public class PaginationAttendees
    {
        [JsonProperty(PropertyName = "object_count")]
        public int objectcount { get; set; }

        [JsonProperty(PropertyName = "page_number")]
        public int pagenumber { get; set; }

        [JsonProperty(PropertyName = "page_size")]
        public int pagesize { get; set; }

        [JsonProperty(PropertyName = "page_count")]
        public int pagecount { get; set; }

        [JsonProperty(PropertyName = "has_more_items")]
        public bool hasmoreitems { get; set; }

    }

    public class Attendees
    {
        public string team { get; set; }

        public Costs costs { get; set; }

        public string resource_uri { get; set; }

        public string id { get; set; }

        public DateTime changed { get; set; }
        public DateTime created { get; set; }

        public int quantity { get; set; }

        [JsonProperty(PropertyName = "variant_id")]
        public string variantid { get; set; }

        public Profile profile { get; set; }

        public List<Barcodes> barcodes { get; set; }

        public List<string> answers { get; set; }

        [JsonProperty(PropertyName = "checked_in")]
        public bool checkedin { get; set; }
        public bool cancelled { get; set; }
        public bool refunded { get; set; }

        public string affiliate { get; set; }

        [JsonProperty(PropertyName = "guestlist_id")]
        public string guestlistid { get; set; }
        [JsonProperty(PropertyName = "invited_by")]
        public string invitedby { get; set; }

        public string status { get; set; }
        [JsonProperty(PropertyName = "ticket_class_name")]
        public string ticketclassname { get; set; }
        [JsonProperty(PropertyName = "delivery_method")]
        public string deliverymethod { get; set; }
        [JsonProperty(PropertyName = "event_id")]
        public string eventid { get; set; }
        [JsonProperty(PropertyName = "order_id")]
        public string orderid { get; set; }
        [JsonProperty(PropertyName = "ticket_class_id")]
        public string ticketclassid { get; set; }
    }

    public class Costs
    {
        [JsonProperty(PropertyName = "base_price")]
        public BasePrice baseprice { get; set; }

        [JsonProperty(PropertyName = "eventbrite_fee")]
        public Eventbritefee eventbritefee { get; set; }

        public Gross gross { get; set; }

        [JsonProperty(PropertyName = "payment_fee")]
        public Paymentfee paymentfee { get; set; }

        public Tax tax { get; set; }


    }

    public class BasePrice
    {
        public string display { get; set; }

        public string currency { get; set; }

        public int value { get; set; }

        [JsonProperty(PropertyName = "major_value")]
        public string majorvalue { get; set; }
    }
    public class Eventbritefee
    {
        public string display { get; set; }

        public string currency { get; set; }

        public int value { get; set; }

        [JsonProperty(PropertyName = "major_value")]
        public string majorvalue { get; set; }
    }
    public class Gross
    {
        public string display { get; set; }

        public string currency { get; set; }

        public int value { get; set; }

        [JsonProperty(PropertyName = "major_value")]
        public string majorvalue { get; set; }
    }
    public class Paymentfee
    {
        public string display { get; set; }

        public string currency { get; set; }

        public int value { get; set; }

        [JsonProperty(PropertyName = "major_value")]
        public string majorvalue { get; set; }
    }
    public class Tax
    {
        public string display { get; set; }

        public string currency { get; set; }

        public int value { get; set; }

        [JsonProperty(PropertyName = "major_value")]
        public string majorvalue { get; set; }
    }

    public class Profile
    {
        [JsonProperty(PropertyName = "first_name")]
        public string firstname { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string lastname { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public Address addresses { get; set; }
    }

    public class Barcodes
    {
        public string status { get; set; }

        public string barcode { get; set; }

        public DateTime created { get; set; }

        public DateTime changed { get; set; }

        [JsonProperty(PropertyName = "checkin_type")]
        public int checkintype { get; set; }

        [JsonProperty(PropertyName = "is_printed")]
        public bool isprinted { get; set; }
    }

    public class Address
    {

    }

}