﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Extentions;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using MoreLinq;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class SubscriberController : Controller
    {
        private  EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        // GET: User/Subscriber
        public ActionResult Index(int? id, string name, string method)
        {

            if (id == null || id < 1)
            {
                return RedirectToAction("Index", "List", new { area = "User" });
            }
            ViewBag.ListId = id;
            ViewBag.ListName = name;
            ViewBag.ListName1 = name;
            ViewBag.Action = method;
            return View();
        }

        public ActionResult AddEditSubscriber(int? listId, int? subscriberId, string listName)
        {
            SubscriberViewModel subscriberViewModel = new SubscriberViewModel();

            ViewBag.ListName = listName;
            try
            {
                if (subscriberId != null)
                {
                    string APIURLUser = string.Format("{0}/{1}?subscriberId={2}", Constants.ApiURL, "Subscriber", subscriberId);
                    var countUser = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
                    if (countUser != null)
                    {
                        var subscriber = Newtonsoft.Json.JsonConvert.DeserializeObject<SubscriberViewModel>(countUser.ToString());
                        subscriberViewModel = subscriber;
                    }
                }
                TempData["Country"] = DataHelper.GetCountryList(subscriberViewModel.Country);
                TempData.Keep("Country");
                TempData["States"] = DataHelper.GetStateList(null);
                TempData.Keep("States");
                if (subscriberViewModel.Country == null || subscriberViewModel.Country == "")
                {
                    subscriberViewModel.Country = Constants.DefaultState;
                    subscriberViewModel.State = "California";

                }
                ViewBag.ListId = listId == null ? 0 : Convert.ToInt32(listId);
                subscriberViewModel.ListId = listId == null ? 0 : Convert.ToInt16(listId);
                subscriberViewModel.SubscriberId = subscriberId == null ? 0 : Convert.ToInt32(subscriberId);
                return PartialView("AddEditSubscriber", subscriberViewModel);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public async Task<ActionResult> AddEditSubscriber(SubscriberViewModel subscriberViewModel)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                    subscriberViewModel.CreatedById = getCurrentUserDetails.Id;
                    subscriberViewModel.EmailPermission = true;

                    if (subscriberViewModel.SubscriberId == 0)
                    {
                        //Add Members to particular mailList of Mailgun 
                       // var List= db.Lists.Where(X => X.ListId == subscriberViewModel.ListId).FirstOrDefault();
                        //subscriberViewModel.ListName = List.ListNameDomainWise.ToString();
                       // var AddSubscriberMailgunList =  await MailgunHelper.AddListMember(subscriberViewModel);
                        String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber");
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(subscriberViewModel));
                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            int duplicate = Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
                            CurrentUser.SetAuthoriztionInfo();
                            //------------Add Subscriber Activity Log-------------//
                            UserActivityLog userActivtyObj = new UserActivityLog();
                            userActivtyObj.UserId = getCurrentUserDetails.Id;
                            userActivtyObj.Createdby = subscriberViewModel.CreatedById;
                            userActivtyObj.Description = subscriberViewModel.FirstName + " subscriber added by " + User.Identity.GetUserName();
                            userActivtyObj.DateTime = DateTime.Now;
                            var res = DataHelper.InsertUserActivity(userActivtyObj);
                            //------------------------------------------//
                            if (duplicate == 2)
                            {
                                return this.Json(new
                                {
                                    status = Constants.SuccessTitle,
                                    msg = "Subscriber already exists! However, other details updated successfully ",
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else if (duplicate == 3)
                            {
                                return this.Json(new
                                {
                                    status = Constants.SuccessTitle,
                                    msg = "Subscriber already blocked! ",
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else if (duplicate == 4)
                            {
                                return this.Json(new
                                {
                                    status = Constants.SuccessTitle,
                                    msg = "Subscriber already exists but in unsubscribed state! So, you cannot add them back",
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                //SendGridEmails sendGrid = new SendGridEmails();
                                //sendGrid.SendGridSubscriberEmailSync("", "Welcome Email" , EmailHtml.getSingleSubscriberWelcomeEmail(getCurrentUserDetails.FirstName + getCurrentUserDetails.LastName , subscriberViewModel.FirstName), Constants.SupportEmail, Constants.SupportName, subscriberViewModel.Email, subscriberViewModel.Email);
                                return this.Json(new
                                {
                                    status = Constants.SuccessTitle,
                                    msg = "Subscriber successfully added.",
                                }, JsonRequestBehavior.AllowGet);
                            }

                        }
                    }
                    else
                    {
                        String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber");
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(subscriberViewModel)).StatusCode;
                        if (result == System.Net.HttpStatusCode.OK)
                        {
                            //------------Edit Subscriber Activity Log-------------//
                            UserActivityLog userActivtyObj = new UserActivityLog();
                            userActivtyObj.UserId = getCurrentUserDetails.Id;
                            userActivtyObj.Createdby = subscriberViewModel.CreatedById;
                            userActivtyObj.Description = subscriberViewModel.FirstName + " subscriber edited by " + User.Identity.GetUserName();
                            userActivtyObj.DateTime = DateTime.Now;
                            var res = DataHelper.InsertUserActivity(userActivtyObj);
                            //------------------------------------------//

                            return this.Json(new
                            {
                                status = Constants.SuccessTitle,
                                msg = "Subscriber details updated successfully.",
                            }, JsonRequestBehavior.AllowGet);
                        }

                    }
                }
                catch (Exception ex)
                {

                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return this.Json(new
            {
                status = Constants.ErrorTitle,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewSubscriber(int? listId, int? Page, string listname, string Search)
        {
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.ListName = listname;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            if (Search == "undefined")
            {
                Search = null;
            }
            ViewBag.PageNumber = Page;
            List<usp_getSubscriberList_Result> subscriberList = new List<usp_getSubscriberList_Result>();
            try
            {
                var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy", Constants.ApiURL, "Subscriber", listId, Page, Constants.PageSize, Search);
                string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    subscriberList = JsonConvert.DeserializeObject<List<usp_getSubscriberList_Result>>(result);
                }
                ViewBag.ListId = listId;
                ViewBag.TotalRecord = subscriberList.Count > 0 ? subscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                return PartialView("ViewSubscriber", subscriberList);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getSubscriberListGridView(int? listId, int? Page, string order, string Search)
        {
            Search = Search == "" ? null : Search;
            List<usp_getSubscriberList_Result> subscriberList = new List<usp_getSubscriberList_Result>();
            var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy={6}", Constants.ApiURL, "Subscriber", listId, Page, Constants.PageSize, Search, order);
            string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getSubscriberList_Result>>(result.ToString());
                subscriberList = data;
                ViewBag.PageNumber = Page;
                ViewBag.TotalRecord = subscriberList.Count > 0 ? subscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
            }

            return PartialView("SubscriberListGrid", subscriberList);
        }


        public ActionResult DeleteSubscriber(string id, int ListId)
        {
            try
            {
                var APIURL = string.Format("{0}/{1}?id={2}&ListId={3}", Constants.ApiURL, "Subscriber", id, ListId);
                var result = (new APICallHelper()).Delete(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    CurrentUser.SetAuthoriztionInfo();
                    //------------Subscriber Delete Activity Log-------------//
                    var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                    UserActivityLog userActivtyObj = new UserActivityLog();
                    userActivtyObj.UserId = getCurrentUserDetails.Id;
                    userActivtyObj.Createdby = User.Identity.GetUserId();
                    userActivtyObj.Description = getCurrentUserDetails.FirstName + " subscriber List Id " + id + " Delete by " + User.Identity.GetUserName();
                    userActivtyObj.DateTime = DateTime.Now;
                    var res = DataHelper.InsertUserActivity(userActivtyObj);
                    //------------------------------------------//

                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Subscriber successfully deleted.",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ImportSubscriberList(int? id, string Listname)
        {
            ViewBag.ListId = id;
            ViewBag.ListName = Listname;

            return PartialView("ImportSubscriberList");
        }

        [HttpPost]
        [ActionName("ImportSubscriberList")]
        public ActionResult ImportSheetSubscriberList(int? id)
        {
            CurrentUser.SetAuthoriztionInfo();
            var userdetails = System.Web.HttpContext.Current.Session["UserAuthorizationInfo"] as usp_getAuthorization_Result;
            if (userdetails.IsCSVActive == true)
            {
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        var UserId = CurrentUser.getCurUserId();
                        var subscriberList = Request.Files[0];
                        var ExcelFilePath = subscriberList.SaveExcelFile();
                        var ServerPath = Server.MapPath("~");
                        var FilePath = ServerPath + ExcelFilePath;
                        var listSubscriber = DataHelper.CheckSubscriberListFromExcel(FilePath, UserId, id);
                        string header = listSubscriber.FirstOrDefault().validHeaders;
                        if (listSubscriber.FirstOrDefault().countInvalidheader == true)
                        {
                            //header = string.Join(", ", from item in listSubscriber select item.invalidHeaders);
                            return Json(new
                            {
                                status = Constants.Error,
                                msg = $"The file that you are uploading has invalid headers.<p>Please upload excel with correct headers!</p><p><a href='/UserUpload/SubscriberListSample.xlsx'>Download Excel Format For Subscriber</a></p>",      
                                URL = ""
                            }, JsonRequestBehavior.AllowGet);
                        }
                        if(listSubscriber.FirstOrDefault().invalidEmail == true)
                        {
                            return Json(new
                            {
                                status = Constants.Error,
                                msg = $"The file that you are uploading has invalid email header.<p><a href='/UserUpload/SubscriberListSample.xlsx'>Download Excel Format For Subscriber</a></p>",
                                URL = ""
                            }, JsonRequestBehavior.AllowGet);
                        }
                        if (listSubscriber.Count > 0)
                        {
                            if (listSubscriber.Count <= Convert.ToInt32(Constants.SubscriberLimit))
                            {
                                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/SubscriberListPost1");
                                try
                                {
                                    var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listSubscriber));
                                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                                    {
                                        // var subscribers = subscriberViewModels.Select(x => new Contacts { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).ToList();
                                        //SendGridEmails sendGrid = new SendGridEmails();
                                        //sendGrid.SendGridMultipleSubscriberEmailSync("Welcome Email", EmailHtml.getMultipleSubscriberWelcomeEmail(CurrentUser.getCurUserDetails().FirstName + CurrentUser.getCurUserDetails().LastName), Constants.SupportEmail, Constants.SupportName , subscribers);
                                        CurrentUser.SetAuthoriztionInfo();
                                        //DataHelper.CleanExcel(FilePath);
                                        var response = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                                        if (response.FirstOrDefault().result == 0)
                                        {
                                            return Json(new
                                            {
                                                status = Constants.Error,
                                                msg = "Maximum subscriber limit of your plan does not allow you to add " + listSubscriber.Count + " subscribers.Want to add subscribers ?",
                                            }, JsonRequestBehavior.AllowGet);
                                        } 
                                        if(response.FirstOrDefault().result == 1)
                                        {
                                            return Json(new
                                            {
                                                status = Constants.Success,
                                                msg = $"{header} data imported Successfully.",
                                            }, JsonRequestBehavior.AllowGet);
                                        }
                                        if(response.FirstOrDefault().result == 3)
                                        {
                                            return Json(new
                                            {
                                                status = Constants.Success,
                                                msg = "The subscribers you are uploading have already exists in your list!"
                                            }, JsonRequestBehavior.AllowGet);
                                        }
                                        if (response.FirstOrDefault().result == 2)
                                        {
                                            return Json(new
                                            {
                                                status = Constants.Error,
                                                msg = Constants.ErrorMessage
                                            }, JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                        {
                                            return Json(new
                                            {
                                                status = Constants.Success,
                                                msg = "Import Successful",
                                            }, JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                }
                                catch (Exception)
                                {

                                    return Json(new
                                    {
                                        status = Constants.Error,
                                        msg = Constants.ErrorMessage,
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = Constants.Error,
                                    msg = "Please upload records in bunches of " + Constants.SubscriberLimit,
                                    URL = FilePath
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = Constants.Error,
                                msg = "The file that you are uploading has no records!",
                            }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    catch (Exception ex)
                    {
                        return Json(new
                        {
                            status = Constants.Error,
                            msg = ex.Message,
                        }, JsonRequestBehavior.AllowGet);
                    }

                }
                return Json(new
                {
                    status = Constants.Error,
                    msg = "The file that you are uploading has no records!",
                    URL = ""
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = Constants.Error,
                    msg = "Your are not allowed to upload the file. Please contact to admin.",
                    URL = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult SaveImportSheetSubscribers(string param1, string param2, string ListName)
        {
            ViewBag.ListId = !string.IsNullOrEmpty(param2) ? Convert.ToInt32(param2) : 0;
            ViewBag.ListName = ListName;
            ViewBag.ExcelFilePath = param1;
            var UserId = CurrentUser.getCurUserId();
            var listSubscriber = DataHelper.GetSubscriberListFromExcel(param1, UserId, 0);
            return PartialView("SaveImportSheetSubscribers", listSubscriber);
        }

        [HttpPost]
        public ActionResult SaveImportSheetSubscribers(string param1, int? param2)
        {
            if (param1 != null && param2 != null)
            {
                SubscriberImportResponseViewModel subscriberErrorResponseViewModel = new SubscriberImportResponseViewModel();
                List<SubscriberViewModel> subscriberViewModels = new List<SubscriberViewModel>();
                List<SubscriberViewModel> inValidSubscriberViewModels = new List<SubscriberViewModel>();
                var UserId = CurrentUser.getCurUserId();
                var listSubscriber = DataHelper.GetSubscriberListFromExcel(param1, UserId, param2);
                foreach (var item in listSubscriber)
                {
                    try
                    {
                        var isValid = DataHelper.ValidateSubscriber(item);
                        if (isValid)
                        {
                            // item.UserId = CurrentUser.getCurUserDetails().Id;
                            item.CreatedById = CurrentUser.getCurUserDetails().Id;
                            if (item.Email.Length < 250)
                            {
                                subscriberViewModels.Add(item);
                            }
                        }
                        else
                        {
                            inValidSubscriberViewModels.Add(item);
                        }
                    }
                    catch (Exception EX)
                    {
                        CreateLog.Log(EX.Message);
                    }
                }
                subscriberErrorResponseViewModel.InvalidSubscribers = inValidSubscriberViewModels;
                if (subscriberViewModels.Count > 0)
                {
                    string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/SubscriberListPost1");
                    try
                    {
                        var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(subscriberViewModels));
                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            // var subscribers = subscriberViewModels.Select(x => new Contacts { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).ToList();
                            //SendGridEmails sendGrid = new SendGridEmails();
                            //sendGrid.SendGridMultipleSubscriberEmailSync("Welcome Email", EmailHtml.getMultipleSubscriberWelcomeEmail(CurrentUser.getCurUserDetails().FirstName + CurrentUser.getCurUserDetails().LastName), Constants.SupportEmail, Constants.SupportName , subscribers);
                            CurrentUser.SetAuthoriztionInfo();
                            DataHelper.CleanExcel(param1);
                            var response = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                            if (response.FirstOrDefault().result != 1)
                            {
                                return Json(new
                                {
                                    status = Constants.Success,
                                    msg = "Import is in progress.Please check after sometime",
                                    Content = subscriberErrorResponseViewModel
                                }, JsonRequestBehavior.AllowGet);
                            }

                            else
                            {
                                return Json(new
                                {
                                    status = Constants.Success,
                                    msg = "Import Successful",
                                    Content = subscriberErrorResponseViewModel
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        return Json(new
                        {
                            status = Constants.Success,
                            msg = "Import Successful",
                            Content = subscriberErrorResponseViewModel
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = Constants.Error,
                    msg = "The file that you are uploading have no valid emails! ",
                    Content = subscriberErrorResponseViewModel,
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    status = Constants.Error,
                    msg = Constants.ErrorMessage,
                    Content = "",
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteAllSubscriber(int? listId)
        {
            String APIURL = string.Format("{0}/{1}?listId={2}", Constants.ApiURL, "Subscriber/DeleteAll", listId);
            try
            {
                var result = (new APICallHelper()).Delete(APIURL);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    CurrentUser.SetAuthoriztionInfo();
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Subscribers successfully deleted.",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(0);
        }

        [HttpPost]
        public ActionResult SetSubsribeStatus(int? SubscriberId, bool IsSubsribe, int ListId)
        {
            String APIURL = string.Format("{0}/{1}?Id={2}&isSubsribe={3}&ListId={4}", Constants.ApiURL, "Subscriber/SetSubsribeStatus", SubscriberId, IsSubsribe, ListId);
            try
            {
                var result = (new APICallHelper()).Put(APIURL);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (IsSubsribe == true)
                    {
                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = "Subscribed successfully!",
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = "Unsubscribed successfully!",
                        }, JsonRequestBehavior.AllowGet);

                    }
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(0);
        }

        public ActionResult ExportList(int listId)
        {
            var APIURL = string.Format("{0}/{1}?listId={2}&search=&pageNumber={3}&pageSize=&orderBy=", Constants.ApiURL, "Subscriber", listId, 1);
            var subscriber = JsonConvert.DeserializeObject<List<usp_getSubscriberList_Result>>((new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result);

            DataTable dt = subscriber.ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                //-----------ExportList Activity Log-------------//
                var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                UserActivityLog userActivtyObj = new UserActivityLog();
                userActivtyObj.UserId = getCurrentUserDetails.Id;
                userActivtyObj.Createdby = User.Identity.GetUserId();
                userActivtyObj.Description = User.Identity.GetUserName() + " Exported  Subscriber List, List id is " + listId;
                userActivtyObj.DateTime = DateTime.Now;
                var res = DataHelper.InsertUserActivity(userActivtyObj);
                //------------------------------------------//
                wb.Worksheets.Add(dt, "subscriber");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Subscriber.xlsx");
                }
            }
        }

        public ActionResult ViewUnSubscriber(int? listId, int? Page, string listname, string Search)
        {
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.ListName = listname;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            ViewBag.PageNumber = Page;
            List<usp_getUnSubscriberList_Result> unsubscriberList = new List<usp_getUnSubscriberList_Result>();
            try
            {
                var APIURL = string.Format("{0}/{1}/{2}?listId={3}&serach={6}&pageNumber={4}&pageSize={5}&orderBy=", Constants.ApiURL, "Subscriber", "UnsubscriberList", listId, Page, Constants.PageSize, Search);
                string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    unsubscriberList = JsonConvert.DeserializeObject<List<usp_getUnSubscriberList_Result>>(result);
                }
                ViewBag.ListId = listId;
                ViewBag.TotalRecord = unsubscriberList.Count > 0 ? unsubscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                return PartialView("ViewUnSubscriber", unsubscriberList);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getUnSubscriberListGridView(int? listId, int? Page, string order, string Search)
        {

            List<usp_getUnSubscriberList_Result> unsubscriberList = new List<usp_getUnSubscriberList_Result>();
            var APIURL = string.Format("{0}/{1}/{2}?listId={3}&serach={6}&pageNumber={4}&pageSize={5}&orderBy={7}", Constants.ApiURL, "Subscriber", "UnsubscriberList", listId, Page, Constants.PageSize, Search, order);
            string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var unsubscriberdata = JsonConvert.DeserializeObject<List<usp_getUnSubscriberList_Result>>(result.ToString());
                unsubscriberList = unsubscriberdata;
            }
            ViewBag.PageNumber = Page;
            ViewBag.TotalRecord = unsubscriberList.Count > 0 ? unsubscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
            return PartialView("UnSubscriberListGrid", unsubscriberList);
        }

        public ActionResult AllSubscriber(string id, int ListId)
        {
            String APIURL = string.Format("{0}/{1}?Id={2}&ListId={3}", Constants.ApiURL, "Subscriber/SubscriberList", id, ListId);

            try
            {

                var result = (new APICallHelper()).Put(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "UnSubscriber has been subscribed successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDeletedSubscriber(int? listId, int? Page, string listname, string Search)
        {
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.ListName = listname;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            if (Search == "undefined")
            {
                Search = null;
            }
            ViewBag.PageNumber = Page;
            List<usp_getDeletedSubscriberList_Result> subscriberList = new List<usp_getDeletedSubscriberList_Result>();
            try
            {
                var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy", Constants.ApiURL, "Subscriber/DeletedSubscriberList", listId, Page, Constants.PageSize, Search);
                string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    subscriberList = JsonConvert.DeserializeObject<List<usp_getDeletedSubscriberList_Result>>(result);
                }
                ViewBag.ListId = listId;
                ViewBag.TotalRecord = subscriberList.Count > 0 ? subscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                return PartialView("ViewDeletedSubscriber", subscriberList);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult RecoverDeleteSubscriber(string id, int ListId)
        {

            try
            {
                var APIURL = string.Format("{0}/{1}/{2}?id={3}&listId={4}", Constants.ApiURL, "Subscriber", "RecoverDeleteSubscriber", id, ListId);
                var result = (new APICallHelper()).Get(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    CurrentUser.SetAuthoriztionInfo();
                    //------------Subscriber Delete Activity Log-------------//
                    var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                    UserActivityLog userActivtyObj = new UserActivityLog();
                    userActivtyObj.UserId = getCurrentUserDetails.Id;
                    userActivtyObj.Createdby = User.Identity.GetUserId();
                    userActivtyObj.Description = getCurrentUserDetails.FirstName + " subscriber List Id " + id + " Recover by " + User.Identity.GetUserName();
                    userActivtyObj.DateTime = DateTime.Now;
                    var res = DataHelper.InsertUserActivity(userActivtyObj);
                    //------------------------------------------//

                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Subscriber has been Recover successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public ActionResult getDeletedSubscriberListGridView(int? listId, int? Page, string order, string Search)
        {
            Search = Search == "" ? null : Search;
            List<usp_getDeletedSubscriberList_Result> subscriberList = new List<usp_getDeletedSubscriberList_Result>();
            var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy={6}", Constants.ApiURL, "Subscriber/DeletedSubscriberList", listId, Page, Constants.PageSize, Search, order);
            string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                subscriberList = JsonConvert.DeserializeObject<List<usp_getDeletedSubscriberList_Result>>(result);

                ViewBag.PageNumber = Page;
                ViewBag.TotalRecord = subscriberList.Count > 0 ? subscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
            }


            return PartialView("DeletedSubscriberListGrid", subscriberList);

        }


        public ActionResult ViewSuppressed(int? listId, int? Page, string listname, string Search)
        {
            ViewBag.PageSize = Constants.PageSize;
            ViewBag.ListName = listname;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            if (Search == "undefined")
            {
                Search = null;
            }
            ViewBag.PageNumber = Page;
            List<usp_getSuppressedSubscribersByListId_Result> suppressedSubscriberList = new List<usp_getSuppressedSubscribersByListId_Result>();
            try
            {
                var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy", Constants.ApiURL, "Subscriber/SuppressedSubscriberList", listId, Page, Constants.PageSize, Search);
                string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    suppressedSubscriberList = JsonConvert.DeserializeObject<List<usp_getSuppressedSubscribersByListId_Result>>(result);
                }
                ViewBag.ListId = listId;
                ViewBag.TotalRecord = suppressedSubscriberList.Count > 0 ? suppressedSubscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                return PartialView("ViewSuppressedSubscribers", suppressedSubscriberList);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getSuppressedSubscriberListGridView(int? listId, int? Page, string order, string Search)
        {
            Search = Search == "" ? null : Search;
            List<usp_getSuppressedSubscribersByListId_Result> suppressedSubscriberList = new List<usp_getSuppressedSubscribersByListId_Result>();
            var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy={6}", Constants.ApiURL, "Subscriber/SuppressedSubscriberList", listId, Page, Constants.PageSize, Search, order);
            string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getSuppressedSubscribersByListId_Result>>(result.ToString());
                suppressedSubscriberList = data;
                ViewBag.PageNumber = Page;
                ViewBag.TotalRecord = suppressedSubscriberList.Count > 0 ? suppressedSubscriberList.Select(x => x.TotalRecord).FirstOrDefault() : 0;
            }

            return PartialView("ViewSuppressedSubscriberListGrid", suppressedSubscriberList);
        }

        public ActionResult ExportSuppressedSubscribers(int listId)
        {
            var APIURL = string.Format("{0}/{1}?listId={2}&search=&pageNumber={3}&pageSize=&orderBy=", Constants.ApiURL, "Subscriber/SuppressedSubscriberList", listId, 1);
            var subscriber = JsonConvert.DeserializeObject<List<usp_getSuppressedSubscribersByListId_Result>>((new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result);

            DataTable dt = subscriber.ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                //-----------ExportList Activity Log-------------//
                var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                UserActivityLog userActivtyObj = new UserActivityLog();
                userActivtyObj.UserId = getCurrentUserDetails.Id;
                userActivtyObj.Createdby = User.Identity.GetUserId();
                userActivtyObj.Description = User.Identity.GetUserName() + " Exported Suppressed Subscriber List, List id is " + listId;
                userActivtyObj.DateTime = DateTime.Now;
                var res = DataHelper.InsertUserActivity(userActivtyObj);
                //------------------------------------------//
                wb.Worksheets.Add(dt, "Suppressedsubscriber");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SuppressedSubscriber.xlsx");
                }
            }
        }


        [HttpPost]
        public async Task<JsonResult> validateEmailAddress(string email)
        {
            var response = await MailgunHelper.GetValidate(email);
            return this.Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}