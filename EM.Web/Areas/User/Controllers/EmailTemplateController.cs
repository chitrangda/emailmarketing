﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class EmailTemplateController : Controller
    {
        // GET: User/EmailTemplate
        public ActionResult Index()
        {
            return View();
        }

        
        [HttpGet]
        public ActionResult Post()
        {
            var httpRequst = HttpContext.Request;

            // TODO: configuration to get the Bee Authorizator Server  end-point

            String BeeEndPointAuthorizatorServer = Constants.ApiURL;

            // Create request to get the Authorization from Server Bee
            HttpWebRequest request = CreateWebRequestToBeeAuthorizatorServer(BeeEndPointAuthorizatorServer);

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                // If response is not 200... throw new App Exception
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response  
                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        // read the response from AuthorizatorServer 
                        string respBeeAuthorizatorServer = reader.ReadToEnd();

                        // Return the response

                        var result = JObject.Parse(respBeeAuthorizatorServer).ToString();
                        return Json(result, JsonRequestBehavior.AllowGet);

                    }
                }
            }

        }

        [HttpPost]
        public ActionResult SaveBeeTemplateContent(string fileName, string fileContent)
        {

            return Json(fileContent);
        }


        #region Private method
        /// <summary>
        ///     Method used to prepare requesto to Bee Authorizator Server
        /// </summary>
        /// <param name="endPoint">The end-point to call</param>
        /// <returns>The request to send to Bee Authorizator Server</returns>
        private HttpWebRequest CreateWebRequestToBeeAuthorizatorServer(string endPoint)
        {
            String BeePluginClientId = ConfigurationManager.AppSettings["BeePluginClientId"];
            String BeePluginClientSecret = ConfigurationManager.AppSettings["BeePluginClientSecret"];
            byte[] data = Encoding.UTF8.GetBytes(String.Format("grant_type=password&client_id={0}&client_secret={1}", BeePluginClientId, BeePluginClientSecret));

            // Create request
            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            // parametrization of request
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            // put body for currrent request to POST
            using (Stream s = request.GetRequestStream())
            {
                s.Write(data, 0, data.Length);
                s.Close();
            }

            return request;
        }
        #endregion
    }
}