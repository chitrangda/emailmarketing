﻿using EM.Factory;
using EM.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    public class EmailGroupController : Controller
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        APICallHelper helper = new APICallHelper();
        // GET: User/EmailGroup
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/EmailGroup/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/EmailGroup/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/EmailGroup/Create
        [HttpPost]
        public ActionResult Create(List group, int GroupId)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var result = Convert.ToBoolean(CheckAlreadyExistsGroupname(group.GroupName));
                    if (result == false)
                    {
                        return this.Json(new
                        {
                            EnableError = true,
                            //ErrorTitle = MasterConstants.ERRORTITLE,
                            //ErrorMsg = MasterConstants.ALREADYEXISTS
                        });
                    }
                    else
                    {

                        //using (var client = new HttpClient())
                        //{
                        //    //string url = ConfigurationManager.AppSettings["key"];
                        //    //client.BaseAddress = new Uri ("http://localhost:49558/api/Group");
                        //    //HTTP POST

                        //    string stringData = JsonConvert.SerializeObject(group);
                        //    var contentData = new StringContent(stringData, System.Text.Encoding.UTF8, "application/json");
                        //    HttpResponseMessage response = client.PostAsync ("http://localhost:49558/api/AddGroup", contentData).Result;
                        HttpResponseMessage response = helper.callWebApi("http://localhost:49558/api/AddGroup", JsonConvert.SerializeObject(group));
                        if (response.IsSuccessStatusCode)
                        {

                            return this.Json(new
                            {
                                EnableSuccess = true,
                                //SuccessTitle = MasterConstants.SUCCESSTITLE,
                                //SuccessMsg = MasterConstants.SUCCESSTITLE
                            });

                        }
                        else
                        {
                            return this.Json(new
                            {
                                EnableError = true,
                                //ErrorTitle = MasterConstants.ERRORTITLE,
                                //ErrorMsg = MasterConstants.ALREADYEXISTS
                            });
                        }
                    }

                }
                return this.Json(new
                {
                    EnableError = true,
                    //ErrorTitle = MasterConstants.ERRORTITLE,
                    //ErrorMsg = MasterConstants.ALREADYEXISTS
                });

            }



            catch
            {
                return View();
            }
        }

        // GET: User/EmailGroup/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/EmailGroup/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/EmailGroup/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/EmailGroup/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public JsonResult CheckAlreadyExistsGroupname(string groupName)
        {
            var Group = (from u in db.Lists
                         where u.GroupName == groupName
                         select new { groupName }).FirstOrDefault();

            bool status;
            if (Group != null)
            {
                //Already registered  
                status = false;
            }
            else
            {
                //Available to use  
                status = true;
            }
            return Json(status, JsonRequestBehavior.AllowGet);

        }
    }
}