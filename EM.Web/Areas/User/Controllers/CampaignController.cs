﻿using EM.Factory.ViewModels;
using EM.Helpers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EM.Factory;
using System.Net;
using EM.Web.Utilities;
using EM.Web.Extentions;
using System.Threading.Tasks;
using System.Text;
using System.IO;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class CampaignController : Controller
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        SendGridAPIHelper _sendGrid = new SendGridAPIHelper();
        // GET: User/Campaign
        public ActionResult Index(string page = "", int? templateId = 0, int? CampaignId = 0)
        {
            ViewBag.Page = page;
            ViewBag.TemplateId = templateId;
            ViewBag.CampaignId = CampaignId;
            return View();
        }

        public PartialViewResult CampaignNameView(int? templateId, int? campaignId, string actionName = "")
        {
            ViewBag.IsEdit = campaignId == 0 ? false : true;
            ViewBag.ActionName = actionName;
            CampaignViewModel campaignViewModel = new CampaignViewModel();
            campaignViewModel.TemplateId = templateId != null ? Convert.ToInt32(templateId) : 0;
            ViewBag.UserId = CurrentUser.getCurUserId();
            try
            {
                if (campaignId != null && campaignId > 0)
                {
                    string APIURLUser = string.Format("{0}/{1}/{2}?id={3}", Constants.ApiURL, "Campaign", "GetCampaignById", campaignId);
                    var countUser = (new APICallHelper()).Get(APIURLUser);
                    if (countUser.StatusCode == HttpStatusCode.OK)
                    {
                        var campaign = JsonConvert.DeserializeObject<CampaignViewModel>(countUser.Content.ReadAsStringAsync().Result.ToString());
                        if (campaign.Status.ToLower() != "sent" && !string.IsNullOrEmpty(campaign.JSONContent))
                        {
                            ViewBag.IsSessionRemove = false;
                            Session["TemplateCampaignViewModel"] = new TemplateViewModel { TemplateJSON = campaign.JSONContent };
                        }
                        campaignViewModel = campaign;
                        if (campaign.FromEmail == null && campaign.FromName == null)
                        {
                            var curUserInfo = CurrentUser.getCurUserDetails();
                            campaignViewModel.FromName = curUserInfo.FirstName + " " + curUserInfo.LastName;
                            campaignViewModel.FromEmail = curUserInfo.Email;
                            campaignViewModel.SendingFromEmail = curUserInfo.SendingFromEmail;
                        }
                    }
                }
                else
                {
                    var curUserInfo = CurrentUser.getCurUserDetails();
                    campaignViewModel.FromName = curUserInfo.FirstName + " " + curUserInfo.LastName;
                    campaignViewModel.FromEmail = curUserInfo.Email;
                    campaignViewModel.SendingFromEmail = curUserInfo.SendingFromEmail;
                }
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
                return PartialView("CampaignNameView", campaignViewModel);
            }
            return PartialView("CampaignNameView", campaignViewModel);
        }

        public ActionResult CreateCampaign(int? templateId, int? campaignId,string ListIds)
        {
            var UniqueSubscriber = DataHelper.Getsubscriberjoinids(ListIds);
            ViewBag.UniqueSubscriberIds = UniqueSubscriber.SubscriberIds != null ? UniqueSubscriber.SubscriberIds : null;
            ViewBag.UniqueSubscriberIdsCount = UniqueSubscriber.SubscriberIds != null?UniqueSubscriber.SubscriberCount:0;
            return PartialView("CreateCampaign");
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CreateCampaign(CampaignViewModel campaignViewModel)
        {
            var UserId = CurrentUser.getCurUserDetails().Id;
            using (var db = new EmailMarketingDbEntities())
            {

                var oEmail = db.EmailConfirmations.Where(x => x.FromEmailAddress == campaignViewModel.FromEmail && x.Status == true && x.UserId == UserId).FirstOrDefault();
                if (oEmail == null)
                {
                    var validEmailAddress = await MailgunHelper.GetValidate(campaignViewModel.FromEmail);
                    if (validEmailAddress == true)
                    {
                        //save campaign details
                        campaignViewModel.Status = "Draft";
                        campaignViewModel.SendingFromEmail = campaignViewModel.SendingFromEmail;
                        var campaignDetails = await PostCampaignDetails(campaignViewModel);
                        var Campaign = db.Campaigns.SingleOrDefault(x => x.CamapignName == campaignViewModel.CampaignName && x.UserId == UserId);

                        //send confirmation email to vefiy
                        var ConfirmEmailUrl = Url.Action("ConfirmEmailVerification", "Account", new { area = "", userId = UserId, email = campaignViewModel.FromEmail, name = campaignViewModel.FromName, campaignId = Campaign.CampaignId }, protocol: Request.Url.Scheme);
                        CreateLog.ActionLogRL(ConfirmEmailUrl);
                        //MailgunHelper _mailGun = new MailgunHelper();
                        //await _mailGun.sendSingleEmail("", "Email Verification ", EmailHtml.getConfirmHtmlCampaign(campaignViewModel.FromName, ConfirmEmailUrl), Constants.SupportEmail, Constants.SupportName, campaignViewModel.FromEmail, campaignViewModel.FromName);
                        Mail oMail = new Mail();
                        await oMail.SendMail(campaignViewModel.FromEmail, "Email Verification", EmailHtml.getConfirmHtmlCampaign(campaignViewModel.FromName, ConfirmEmailUrl), Constants.SupportName, "mailSettings/Support", campaignViewModel.FromName);


                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = "Please first verify your email address, we have sent you a mail, Campaign Successfully Draft !",
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Error,
                            msg = "Email address that you have entered is invalid, please enter valid email address",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
                    var campaignDetails = await PostCampaignDetails(campaignViewModel);
                    if (campaignDetails == true)
                    {

                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = $"Campaign {campaignViewModel.Status} successfully.",
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Error,
                            msg = Constants.ErrorMessage,
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            //if (campaignViewModel.Status != "Draft")
            //{
            //    using (var db = new EmailMarketingDbEntities())
            //    {
            //        var UserId = CurrentUser.getCurUserDetails().Id;
            //        /*To check whether from email address is verified or not*/
            //        var oEmail = db.EmailConfirmations.Where(x => x.FromEmailAddress == campaignViewModel.FromEmail && x.Status == true && x.UserId == UserId).FirstOrDefault();
            //        if (oEmail != null)
            //        {
            //            campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
            //            var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            if (campaignDetails == true)
            //            {

            //                return this.Json(new
            //                {
            //                    status = Constants.Success,
            //                    msg = $"Campaign {campaignViewModel.Status} successfully.",
            //                }, JsonRequestBehavior.AllowGet);
            //            }
            //            else
            //            {
            //                return this.Json(new
            //                {
            //                    status = Constants.Error,
            //                    msg = Constants.ErrorMessage,
            //                }, JsonRequestBehavior.AllowGet);
            //            }
            //            ///*To split campaign sending from email*/
            //            //var emailDomain = oEmail.SendingFromEmail.Split('@')[1];
            //            ///*To check whether sending domain is up or not. If sending domain is not up then this verify domain api is call*/
            //            //try
            //            //{
            //            //    var resp = await MailgunHelper.verifyDomainSettings(emailDomain);
            //            //}
            //            //catch (Exception exception)
            //            //{
            //            //    CreateLog.Log(exception.Message, exception);

            //            //}
            //            ///*To check whether sending domain is verified or not*/
            //            //var checkDomain = await MailgunHelper.IsVerifiedDomainMailGun(oEmail.SendingFromEmail);
            //            //{
            //            //    /*To create sending domain if not create*/
            //            //    if (checkDomain == 0)
            //            //    {
            //            //        var res = await MailgunHelper.createDomainMailGun(oEmail.SendingFromEmail);
            //            //        var webhook = await MailgunHelper.InsertWebHookLinks(oEmail.SendingFromEmail);
            //            //        var domainSetting = await MailgunHelper.verifyDomainSettings(emailDomain);
            //            //        if (domainSetting == true)
            //            //        {
            //            //            campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
            //            //            var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            //            if (campaignDetails == true)
            //            //            {

            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Success,
            //            //                    msg = $"Campaign {campaignViewModel.Status} successfully.",
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //            else
            //            //            {
            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Error,
            //            //                    msg = Constants.ErrorMessage,
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //        }
            //            //        else
            //            //        {
            //            //            campaignViewModel.Status = "Draft";
            //            //            campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
            //            //            var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            //            if (campaignDetails == true)
            //            //            {

            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Success,
            //            //                    msg = $"Campaign {campaignViewModel.Status} successfully but your domain is still not verified.",
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //            else
            //            //            {
            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Error,
            //            //                    msg = Constants.ErrorMessage,
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //        }

            //            //    }
            //            //    /*To send campaign code */
            //            //    if (checkDomain == 1)
            //            //    {
            //            //        campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
            //            //        var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            //        if (campaignDetails == true)
            //            //        {

            //            //            return this.Json(new
            //            //            {
            //            //                status = Constants.Success,
            //            //                msg = $"Campaign {campaignViewModel.Status} successfully.",
            //            //            }, JsonRequestBehavior.AllowGet);
            //            //        }
            //            //        else
            //            //        {
            //            //            return this.Json(new
            //            //            {
            //            //                status = Constants.Error,
            //            //                msg = Constants.ErrorMessage,
            //            //            }, JsonRequestBehavior.AllowGet);
            //            //        }
            //            //    }
            //            //    /*To check whether sending domain is up or not. If sending domain is not up then this verify domain api is call*/
            //            //    if (checkDomain == 2)
            //            //    {
            //            //        var response = await MailgunHelper.verifyDomainSettings(emailDomain);
            //            //        if (response == true)
            //            //        {
            //            //            campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
            //            //            var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            //            if (campaignDetails == true)
            //            //            {

            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Success,
            //            //                    msg = $"Campaign {campaignViewModel.Status} successfully.",
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //            else
            //            //            {
            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Error,
            //            //                    msg = Constants.ErrorMessage,
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //        }
            //            //        else
            //            //        {
            //            //            campaignViewModel.Status = "Draft";
            //            //            campaignViewModel.SendingFromEmail = oEmail.SendingFromEmail;
            //            //            var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            //            if (campaignDetails == true)
            //            //            {

            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Success,
            //            //                    msg = $"Campaign {campaignViewModel.Status} successfully but your domain is still not verified",
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //            else
            //            //            {
            //            //                return this.Json(new
            //            //                {
            //            //                    status = Constants.Error,
            //            //                    msg = Constants.ErrorMessage,
            //            //                }, JsonRequestBehavior.AllowGet);
            //            //            }
            //            //        }
            //            //    }
            //            //    else
            //            //    {
            //            //        return this.Json(new
            //            //        {
            //            //            status = Constants.Error,
            //            //            msg = Constants.ErrorMessage,
            //            //        }, JsonRequestBehavior.AllowGet);
            //            //    }


            //            //}
            //        }
            //        else if (campaignViewModel.CampaignId == 0)
            //        {
            //            /*To save campaign in draft mode and send email verification email to the from email address*/
            //            campaignViewModel.Status = "Draft";
            //            var CampaignName = campaignViewModel.CampaignName;
            //            var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //            var Campaign = db.Campaigns.SingleOrDefault(x => x.CamapignName == CampaignName && x.UserId == UserId);

            //            var ConfirmEmailUrl = Url.Action("ConfirmEmailVerification", "Account", new { area = "", userId = UserId, email = campaignViewModel.FromEmail, name = campaignViewModel.FromName, campaignId = Campaign.CampaignId }, protocol: Request.Url.Scheme);
            //            CreateLog.ActionLogRL(ConfirmEmailUrl);
            //            MailgunHelper _mailGun = new MailgunHelper();
            //            await _mailGun.sendSingleEmail("", "Email Verification ", EmailHtml.getConfirmHtmlCampaign(campaignViewModel.FromName, ConfirmEmailUrl), Constants.SupportEmail, Constants.SupportName, campaignViewModel.FromEmail, campaignViewModel.FromName);
            //            return this.Json(new
            //            {
            //                status = Constants.Success,
            //                msg = "Campaign Successfully Draft !" +
            //                       "We have  sent you a mail for email verification, please first verify your FromEmail address",
            //            }, JsonRequestBehavior.AllowGet);
            //        }
            //        else
            //        {
            //            /*To save campaign in draft mode and check whether from email address is same or changed.If changed then, send email verification email to the from email address*/
            //            var Campaign = db.Campaigns.SingleOrDefault(x => x.CampaignId == campaignViewModel.CampaignId && x.UserId == UserId);
            //            if (Campaign.FromEmail != campaignViewModel.FromEmail)
            //            {
            //                campaignViewModel.Status = "Draft";
            //                var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //                var ConfirmEmailUrl = Url.Action("ConfirmEmailVerification", "Account", new { area = "", userId = UserId, email = campaignViewModel.FromEmail, name = campaignViewModel.FromName, campaignId = campaignViewModel.CampaignId }, protocol: Request.Url.Scheme);
            //                CreateLog.ActionLogRL(ConfirmEmailUrl);
            //                MailgunHelper _mailGun = new MailgunHelper();
            //                await _mailGun.sendSingleEmail("", "Email Verification ", EmailHtml.getConfirmHtmlCampaign(campaignViewModel.FromName, ConfirmEmailUrl), Constants.SupportEmail, Constants.SupportName, campaignViewModel.FromEmail, campaignViewModel.FromName);

            //                return this.Json(new
            //                {
            //                    status = Constants.Success,
            //                    msg = "We have sent you a mail for email verification, please first verify your FromEmail address",
            //                }, JsonRequestBehavior.AllowGet);
            //            }
            //            else
            //            {
            //                /*To save campaign in draft mode and email verification mail has already sent to the from email address*/
            //                campaignViewModel.Status = "Draft";
            //                var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //                return this.Json(new
            //                {
            //                    status = Constants.Success,
            //                    msg = "We have already sent you a mail for email verification, please first verify your FromEmail address",
            //                }, JsonRequestBehavior.AllowGet);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    /*To save campaign in draft mode*/
            //    var campaignDetails = await PostCampaignDetails(campaignViewModel);
            //    if (campaignDetails == true)
            //    {

            //        return this.Json(new
            //        {
            //            status = Constants.Success,
            //            msg = $"Campaign successfully saved as {campaignViewModel.Status}!",
            //        }, JsonRequestBehavior.AllowGet);
            //    }
            //    else
            //    {
            //        return this.Json(new
            //        {
            //            status = Constants.Error,
            //            msg = Constants.ErrorMessage,
            //        }, JsonRequestBehavior.AllowGet);
            //    }

            //}
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> DraftCampaign(CampaignViewModel campaignViewModel)
        {

            campaignViewModel.CreatedById = User.Identity.GetUserId();
            campaignViewModel.UserId = CurrentUser.getCurUserId();
            // var serverPath = Server.MapPath("~");
            if (campaignViewModel.HtmlContent != null)
            {
                string path = "~/UserUpload/CampaignImages/" + campaignViewModel.UserId;
                //if (campaignViewModel != null && !string.IsNullOrEmpty(campaignViewModel.ContentImagePath) && System.IO.File.Exists(serverPath + campaignViewModel.ContentImagePath))
                //{
                //    System.IO.File.Delete(serverPath + campaignViewModel.ContentImagePath);
                //}
                if (!Directory.Exists(Server.MapPath(path)))
                {
                    Directory.CreateDirectory(Server.MapPath(path));
                }
                string campaignImagePath = path + "/" + EM.Helpers.Utilities.GetRandomNumber() + ".jpg";
                ImageProcessing.ConvertHtmlToImage(campaignViewModel.HtmlContent, Server.MapPath(campaignImagePath));
                campaignViewModel.ContentImagePath = Url.Content(campaignImagePath);
            }
            if (campaignViewModel.Status == "Draft" && !string.IsNullOrEmpty(campaignViewModel.JSONContent))
            {
                ViewBag.IsSessionRemove = false;
                Session["TemplateCampaignViewModel"] = new TemplateViewModel
                {
                    TemplateJSON = campaignViewModel.JSONContent,
                    TemplateHtml = campaignViewModel.HtmlContent,
                    TemplatePath = campaignViewModel.ContentImagePath
                };
            }
            if (Session["TemplateCampaignViewModel"] != null)
            {
                var templateItem = Session["TemplateCampaignViewModel"] as TemplateViewModel;
                campaignViewModel.HtmlContent = templateItem.TemplateHtml;
                campaignViewModel.JSONContent = templateItem.TemplateJSON;
                campaignViewModel.ContentImagePath = templateItem.TemplatePath;
            }
            Session.Remove("TemplateCampaignViewModel");
            if (ModelState.IsValid)
            {
                try
                {
                    String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Campaign");
                    var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(campaignViewModel));
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var campaign = Newtonsoft.Json.JsonConvert.DeserializeObject<CampaignViewModel>(result.Content.ReadAsStringAsync().Result.ToString());
                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = $"Campaign successfully saved as {campaignViewModel.Status}!",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {

                    return this.Json(new
                    {
                        status = Constants.Error,
                        msg = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage,
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> ResendCampaign(int CampaignId)
        {
            if (CampaignId > 0)
            {
                try
                {
                    var listCampaignEmailStatus = _context.CampaignEmailStatus.Where(x => x.CampaignId == CampaignId).ToList();
                    if (listCampaignEmailStatus != null && listCampaignEmailStatus.Count() > 0)
                    {
                        _context.CampaignEmailStatus.RemoveRange(listCampaignEmailStatus);
                        await _context.SaveChangesAsync();
                    }
                    var failedCampaign = _context.ScheduledCampaigns.Where(x => x.CampaignId == CampaignId).FirstOrDefault();
                    if (failedCampaign != null)
                    {
                        failedCampaign.ScheduledDateTime = DateTime.Now;
                        failedCampaign.Status = CampaignStatus.Queued;
                        await _context.SaveChangesAsync();
                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = $"Campaign has been Queued successfully !",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                catch (Exception ex)
                {

                    return this.Json(new
                    {
                        status = Constants.Error,
                        msg = ex.Message
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CampaignList(int? page, string search)
        {
            if (page == null || page == 0 || !string.IsNullOrEmpty(search))
            {
                page = 1;
            }
            ViewBag.PageNumber = page;
            List<usp_getCampaignList_Result> campaignModel = new List<usp_getCampaignList_Result>();
            string APIURLUser = string.Format("{0}/{1}?id={2}&pageNumber={3}&pageSize={4}&Search={5}&orderBy", Constants.ApiURL, "Campaign", CurrentUser.getCurUserId(), page, Constants.PageSize, search);
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var camapaigns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getCampaignList_Result>>(count.ToString());
                ViewBag.TotalRecord = camapaigns.Count > 0 ? camapaigns.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                campaignModel = camapaigns;
            }
            return PartialView("CampaignList", campaignModel);
        }

        [HttpPost]
        public ActionResult getCampaignListGrid(int? page, string order, string search)
        {
            if (page == null || page == 0)
            {
                page = 1;
            }
            ViewBag.PageNumber = page;
            List<usp_getCampaignList_Result> campaignModel = new List<usp_getCampaignList_Result>();
            string APIURLUser = string.Format("{0}/{1}?id={2}&pageNumber={3}&pageSize={4}&Search={5}&orderBy={6}", Constants.ApiURL, "Campaign", CurrentUser.getCurUserId(), page, Constants.PageSize, search, order);
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var camapigns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getCampaignList_Result>>(count.ToString());
                ViewBag.TotalRecord = camapigns.Count > 0 ? camapigns.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                campaignModel = camapigns;

            }
            return PartialView("CampaignListGrid", campaignModel);
        }

        public ActionResult CampaignEditorView(int? templateId, string type = "")
        {
            ViewBag.TemplateId = templateId;
            ViewBag.Type = type;
            if (Session["TemplateCampaignViewModel"] != null)
            {
                var templateViewModel = Session["TemplateCampaignViewModel"] as TemplateViewModel;
                ViewBag.TemplateContent = templateViewModel.TemplateJSON;
            }
            return PartialView("CampaignEditorView");
        }

        public ActionResult SaveBeeTemplateContent(TemplateViewModel templateViewModel)
        {
            Session.Remove("TemplateCampaignViewModel");
            string path = "~/UserUpload/CampaignImages/"+CurrentUser.getCurUserId();
            if (!Directory.Exists(Server.MapPath(path))){
                Directory.CreateDirectory(Server.MapPath(path));
            }
            string campaignImagePath = path + "/" + EM.Helpers.Utilities.GetRandomNumber() + ".jpg";
            ImageProcessing.ConvertHtmlToImage(templateViewModel.TemplateHtml, Server.MapPath(campaignImagePath));
            //var ServerPath = Server.MapPath("~/UserUpload/CampaignImages/") + "/campaign.png";
            //if (templateViewModel != null && !string.IsNullOrEmpty(templateViewModel.TemplatePath))
            //{
            //    var serverPath = Server.MapPath("~");
            //    if (System.IO.File.Exists(serverPath + templateViewModel.TemplatePath))
            //    {
            //        System.IO.File.Delete(serverPath + templateViewModel.TemplatePath);
            //    }
            //}
            //var images = templateViewModel.Base64Content.Base64ToImage().SaveImageTemplateFileInFolder();
            templateViewModel.TemplatePath = Url.Content(campaignImagePath);// "../../UserUpload/CampaignImages/" + CurrentUser.getCurUserId() + "/" + EM.Helpers.Utilities.GetRandomNumber() + ".png";
            Session["TemplateCampaignViewModel"] = templateViewModel;
            return Json(templateViewModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTemplateByContent()
        {
            if (Session["TemplateCampaignViewModel"] != null)
            {
                var templateViewModel = Session["TemplateCampaignViewModel"] as TemplateViewModel;
                return Json(templateViewModel, JsonRequestBehavior.AllowGet);
            }
            return Json(false);
        }

        public async Task SendEmail(string UserId, int CampaignId, string subject, string htmlContent, string fromEmail, string fromName, string listId, string SubscriberIds)
        {
            var subscriberListId = SubscriberIds.Split(',').Select(x => Convert.ToInt32(x));
            var subscribers = _context.Subscribers.Where(x => subscriberListId.Any(y => y == x.SubscriberId)).Select(x => new Contacts { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName, PhoneNo = x.PhoneNumber }).ToList();
            await (new SendGridEmails()).SendGridMultipleEmail(UserId, CampaignId, subject, htmlContent, fromEmail, fromName, subscribers);
        }


        public PartialViewResult CampaignReplicateNameView(int? templateId, int? campaignId, string actionName = "")
        {
            ViewBag.IsEdit = campaignId == 0 ? false : true;
            ViewBag.ActionName = actionName;
            CampaignViewModel campaignViewModel = new CampaignViewModel();
            campaignViewModel.TemplateId = templateId != null ? Convert.ToInt32(templateId) : 0;
            ViewBag.UserId = CurrentUser.getCurUserId();
            try
            {
                if (campaignId != null && campaignId > 0)
                {
                    string APIURLUser = string.Format("{0}/{1}/{2}?id={3}", Constants.ApiURL, "Campaign", "GetCampaignById", campaignId);
                    var countUser = (new APICallHelper()).Get(APIURLUser);
                    if (countUser.StatusCode == HttpStatusCode.OK)
                    {
                        var campaign = JsonConvert.DeserializeObject<CampaignViewModel>(countUser.Content.ReadAsStringAsync().Result.ToString());
                        if (campaign.Status.ToLower() == "sent" && !string.IsNullOrEmpty(campaign.JSONContent))
                        {
                            ViewBag.IsSessionRemove = false;
                            Session["TemplateCampaignViewModel"] = new TemplateViewModel { TemplateJSON = campaign.JSONContent };
                        }
                        campaignViewModel = campaign;
                    }
                }
            }
            catch (Exception)
            {
                return PartialView("CampaignReplecateNameView", campaignViewModel);
            }
            return PartialView("CampaignReplecateNameView", campaignViewModel);
        }
        [HttpPost]
        public PartialViewResult CampaignListSubscriberView()
        {
            var ListData = DataHelper.GetListWithSubscriberByListId(CurrentUser.getCurUserId());
            return PartialView("CampaignListSubscriberView", ListData);
        }
        public ActionResult GetSubscriberByListId(int? listId)
        {
            List<usp_getSubscriberList_Result> subscriberList = new List<usp_getSubscriberList_Result>();
            var APIURL = string.Format("{0}/{1}?listId={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy={6}", Constants.ApiURL, "Subscriber", listId, 1, 50, string.Empty, string.Empty);
            string result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getSubscriberList_Result>>(result.ToString());
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<usp_getSubscriberList_Result>(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCampignStats(string id, string date)
        {
            string APIURLUser = string.Format("{0}/{1}?UserId={2}&CampaignId={3}", Constants.ApiURL, "Campaign/GetSingleCampaignReport", CurrentUser.getCurUserId(), id);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var camapaigns = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getSingleCampaignReport_Result>(response.ToString());
                return this.Json(camapaigns, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }
        }

        public ActionResult LayoutView()
        {
            int page = 1;
            ViewBag.PageNumber = page;
            List<usp_getBeeTemplateList_Result> model = new List<usp_getBeeTemplateList_Result>();
            string APIURLUser = string.Format("{0}/{1}?pageNumber={2}&pageSize={3}&Search={4}&orderBy={5}", Constants.ApiURL, "Template/GetLayout", page, null, null, null);
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var layout = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getBeeTemplateList_Result>>(count.ToString());
                ViewBag.TotalRecord = layout.Count > 0 ? layout.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                model = layout;

            }
            return PartialView("LayoutList", model);
        }

        [HttpPost]
        public PartialViewResult ViewEmail(int campaignId)
        {
            CampaignViewModel model = new CampaignViewModel();
            if (campaignId > 0)
            {
                string url = string.Format("{0}/{1}/{2}?id={3}", Constants.ApiURL, "Campaign", "GetCampaignById", campaignId);
                var response = (new APICallHelper()).Get(url);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    model = Newtonsoft.Json.JsonConvert.DeserializeObject<CampaignViewModel>(response.Content.ReadAsStringAsync().Result.ToString());

                }
            }
            return PartialView("ViewEmail", model);
        }

        public ActionResult BeeTemplateView()
        {
            int page = 1;
            ViewBag.PageNumber = page;
            List<usp_getBeeTemplateList_Result> model = new List<usp_getBeeTemplateList_Result>();
            string APIURLUser = string.Format("{0}/{1}?pageNumber={2}&pageSize={3}&Search={4}&orderBy={5}&version={6}", Constants.ApiURL, "Template/GetLayout", page, null, null, null, "v3");
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var layout = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getBeeTemplateList_Result>>(count.ToString());

                ViewBag.TotalRecord = layout.Count > 0 ? layout.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                model = layout;

            }
            return PartialView("BeeTemplateList", model);
        }

        public ActionResult CheapestEmailTemplates()
        {
            int page = 1;
            ViewBag.PageNumber = page;
            var order = "NameAsc";
            List<TemplateViewModel> model = new List<TemplateViewModel>();

            string APIURLUser = string.Format("{0}/{1}?id={2}&pageNumber={3}&pageSize={4}&search={5}&orderBy={6}", Constants.ApiURL, "Template", Constants.AdminId, page, null, null,order);
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var layout = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TemplateViewModel>>(count.ToString());
                ViewBag.TotalRecord = layout.Count > 0 ? layout.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                model = layout;

            }
            return PartialView("CheapestEmailTemplates", model);
        }

        async Task<bool> PostCampaignDetails(CampaignViewModel campaignViewModel)
        {
            campaignViewModel.CreatedById = User.Identity.GetUserId();
            campaignViewModel.UserId = CurrentUser.getCurUserId();
            //var domain = campaignViewModel.FromEmail.Split('@')[1];
            //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
            //{
            //    campaignViewModel.SendingFromEmail = campaignViewModel.FromEmail;
            //}
            //else
            //{
            //    campaignViewModel.SendingFromEmail = campaignViewModel.FromEmail.Substring(0, campaignViewModel.FromEmail.LastIndexOf('.')) + Constants.RootDomain;
            //}

            //if (DateTimeOffset.Now.Offset.TotalMinutes != campaignViewModel.Offset)
            //{
            //    campaignViewModel.ScheduleDate = campaignViewModel.ScheduleDate.AddMinutes(campaignViewModel.Offset);
            //}
            var serverPath = Server.MapPath("~");
            //if (campaignViewModel != null && !string.IsNullOrEmpty(campaignViewModel.ContentImagePath) && System.IO.File.Exists(serverPath + campaignViewModel.ContentImagePath))
            //{
            //    System.IO.File.Delete(serverPath + campaignViewModel.ContentImagePath);
            //}
            if (Session["TemplateCampaignViewModel"] != null)
            {
                var templateItem = Session["TemplateCampaignViewModel"] as TemplateViewModel;
                campaignViewModel.HtmlContent = templateItem.TemplateHtml;
                campaignViewModel.JSONContent = templateItem.TemplateJSON;
                campaignViewModel.ContentImagePath = templateItem.TemplatePath;
            }
            Session.Remove("TemplateCampaignViewModel");
            if (ModelState.IsValid)
            {
                try
                {
                    String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Campaign");
                    var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(campaignViewModel));
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var campaign = Newtonsoft.Json.JsonConvert.DeserializeObject<CampaignViewModel>(result.Content.ReadAsStringAsync().Result.ToString());
                        //Commented below code as the campaign will be scheduled and sent through service
                        //if (campaignViewModel.Status.ToLower() == "send")
                        //{
                        //    await SendEmail(campaign.UserId, campaign.CampaignId, campaign.Subject, campaign.HtmlContent, campaign.FromEmail, campaign.FromName, campaign.ListId, campaign.SubscriberIds);
                        //}
                        //var content = string.Empty;
                        //switch (campaignViewModel.Status)
                        //{
                        //    case "Send":
                        //        content = "Sent";
                        //        break;
                        //    case "Draft":
                        //        content = "Drafted";
                        //        break;
                        //    case "Schedule":
                        //        content = "Scheduled";
                        //        break;
                        //}
                        //------------ Activity Log-------------//

                        //  LogService.WriteToLog(" ScheduleDate for Campaign Is " +campaignViewModel.CampaignName +"  " + campaignViewModel.ScheduleDate, campaignViewModel.ListId);
                        //------------------------------------------//
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    CreateLog.Log(ex.Message, ex);
                    return false;
                }

            }
            else
            {
                return false;
            }


        }

        public async Task<ActionResult> ConfirmEmailSendMail(CampaignViewModel model)
        {
            var UserId = CurrentUser.getCurUserDetails().Id;

            using (var db = new EmailMarketingDbEntities())
            {

                //var oEmail = db.EmailConfirmations.Where(x => x.FromEmailAddress == model.FromEmail && x.Status == true && x.UserId == UserId).FirstOrDefault();
                //if (oEmail == null)
                //{
                    var ConfirmEmailUrl = Url.Action("ConfirmEmailVerification", "Account", new { area = "", userId = UserId, email = model.FromEmail, name = model.FromName , campaignId = model.CampaignId }, protocol: Request.Url.Scheme);
                    //SendGridEmails sendGrid = new SendGridEmails();
                    //MailgunHelper _mailGun = new MailgunHelper();
                    //await _mailGun.sendSingleEmail("", "Email Verification ", EmailHtml.getConfirmHtmlCampaign(model.FromName, ConfirmEmailUrl), Constants.SupportEmail, Constants.SupportName, model.FromEmail, model.FromName);
                    //CreateLog.ActionLogRL(ConfirmEmailUrl);

                Mail oMail = new Mail();
                await oMail.SendMail(model.FromEmail, "Email Verification ", EmailHtml.getConfirmHtmlCampaign(model.FromName, ConfirmEmailUrl), Constants.SupportName, "mailSettings/Support", model.FromName);
                CreateLog.ActionLogRL(ConfirmEmailUrl);
                 
                return this.Json(new
                    {
                        status = Constants.Success,
                        //msg = "We have send you mail ,please verify your email first.",
                        msg = "We have send a email verification link to your email address, please check your inbox.",

                    }, JsonRequestBehavior.AllowGet);
                //}
            }
        }
        [HttpPost]
        public JsonResult CheckDuplicacyCampaign(string CampaignName , int CampaignId , int IsReplicateCase)
        {
            var CheckDuplicateTemplate = DataHelper.CheckDuplicateCampaignName(CampaignName, CampaignId, IsReplicateCase);
            if (CheckDuplicateTemplate == true)
            {
                return this.Json(new
                {
                    status = Constants.Success,
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> IsDomainVerified(string email)
        {
            var userDetails = CurrentUser.getCurUserDetails();
            string emailDomain = "";
            var domain = email.Split('@')[1];
            if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
            {
                emailDomain = await MailgunHelper.ReturnDomainforWhitelabelDomain(userDetails.FirstName, userDetails.CompanyName);
            }
            else
            {
                emailDomain = email;
            }
            var check = await MailgunHelper.IsVerifiedDomainMailGun(emailDomain);
            if (check == 1)
            {
                return this.Json(new
                {
                    status = Constants.Success,
                }, JsonRequestBehavior.AllowGet);

            }
            else if (check == 2)

            {
                var domainVerify = emailDomain.Split('@')[1];
                var response = await MailgunHelper.verifyDomainSettings(domainVerify);
                if (response == true)
                {
                    return this.Json(new
                    {
                        status = Constants.Success,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return this.Json(new
                    {
                        status = Constants.Error,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> GetConfirmedEmails()
        {
            var UserId = CurrentUser.getCurUserDetails().Id;
            var oemail = DataHelper.GetConfirmEmailAddress(UserId);
            return Json(oemail.ToList(), JsonRequestBehavior.AllowGet);
         
        }

        public async Task<JsonResult> EmailsByList(string ListId)
        {
         var emails=_context.usp_getEmailsbyListId(ListId).ToArray();
        var jsonResult = Json(emails, JsonRequestBehavior.AllowGet);
        jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}