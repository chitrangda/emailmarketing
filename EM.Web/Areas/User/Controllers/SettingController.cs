﻿using EM.Factory.ViewModels;
using EM.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class SettingController : Controller
    {
        // GET: User/Fields
        public ViewResult Index(int listId, string page)
        {
            ViewBag.Page = page;
            ViewBag.ListId = listId;
            return View();
        }
        public ActionResult ViewFields(int id)
        {
            ViewBag.ListId = id;
            string APIURL = string.Format("{0}/{1}?listId={2}", Constants.ApiURL, "Fields", id);
            var result = (new APICallHelper()).Get(APIURL);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var respone = JsonConvert.DeserializeObject<List<FieldsViewModel>>(result.Content.ReadAsStringAsync().Result);
                return PartialView("ViewFields", respone);
            }
            return PartialView("ViewFields");
        }

        [HttpGet]
        public ActionResult ActivateDeactiveFields(string fieldsId,int listId,string actionName)
        {
            try
            {
                var APIURL = string.Format("{0}/{1}?fields={2}&listId={3}&actionName={4}", Constants.ApiURL, "Fields/ActivateDeactivate", fieldsId, listId, actionName);
                var result = (new APICallHelper()).Get(APIURL);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    actionName = actionName == "Activate" ? "Added" : "Removed";
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = $"{actionName} successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }
    }
}