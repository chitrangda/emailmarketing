﻿using EM.Factory.ViewModels;
using EM.Factory;
using EM.Helpers;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Text.RegularExpressions;
using NMIPayment;

namespace EM.Web.Areas.User.Controllers
{
    // Created on 3:21 PM 1/3/2019 By Alok Pandey
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class BillingController : Controller
    {
        // GET: User/Billing
        public ActionResult Index(string page)
        {
            ViewBag.Page = page;
            return View();
        }

        public ActionResult BillingList()
        {
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Billing", CurrentUser.getCurUserId());
            var result = (new APICallHelper()).Get(APIURL);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var respone = JsonConvert.DeserializeObject<List<UserPaymentProfileViewModel>>(result.Content.ReadAsStringAsync().Result);
                return PartialView("BillingList", respone);
            }
            return PartialView("BillingList");
        }

        public ActionResult AddEditBilling(int? id)
        {
            var User = CurrentUser.getCurUserDetails();
            UserPaymentProfileViewModel userPaymentProfileViewModel = new UserPaymentProfileViewModel();
            if (id == null)
            {
                id = 0;
            }
            try
            {
                TempData["Country"] = DataHelper.GetCountryList("");
                TempData.Keep("Country");
                TempData["States"] = DataHelper.GetStateList(null);
                TempData.Keep("States");
                //ViewBag.CreditCardExpiryYear = DataHelper.GetCreditCardYear(0);
                //ViewBag.CreditCardExpiryMonths = DataHelper.GetMonths(0);


                userPaymentProfileViewModel.Id = Convert.ToInt32(id);
                string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Billing/GetPaymentPofileById", userPaymentProfileViewModel.Id);
                var result = (new APICallHelper()).Get(APIURL);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    userPaymentProfileViewModel = JsonConvert.DeserializeObject<UserPaymentProfileViewModel>(result.Content.ReadAsStringAsync().Result);
                }
                userPaymentProfileViewModel.YearList = DataHelper.GetCreditCardYear(userPaymentProfileViewModel.ExYear);
                userPaymentProfileViewModel.MonthList = DataHelper.GetMonths(userPaymentProfileViewModel.ExMonth);
                ViewBag.Country = User.Country;
                userPaymentProfileViewModel.Email = User.Email;

            }
            catch (Exception)
            {

                throw;
            }


            return PartialView("AddEditBilling", userPaymentProfileViewModel);
        }
        [HttpPost]
        public ActionResult AddEditBilling(UserPaymentProfileViewModel userPaymentProfileViewModel)
        {
            string message = "";
            bool retStatus = false;
            if (ModelState.IsValid)
            {

                if (userPaymentProfileViewModel.Id == 0)
                {
                    var creditcardNumber = DataHelper.CheckDuplicateCreditCard(userPaymentProfileViewModel.CreditCardNo, CurrentUser.getCurUserId());
                    if (creditcardNumber == false)
                    {
                        message = savePaymentProfile(userPaymentProfileViewModel);
                       
                        if (message == "\"success\"")
                        {
                            CurrentUser.createCurUserSession("user", userPaymentProfileViewModel.UserId);
                            retStatus = Constants.Success;
                            message = "Payment details save successfully.";
                        }

                    }
                    else
                    {
                        retStatus = Constants.Error;
                        message = "Credit Card Number already exist!";

                    }
                }
                else
                {
                    message = savePaymentProfile(userPaymentProfileViewModel);
                    if (message == "\"success\"")
                    {
                        retStatus = Constants.Success;
                        message = "Payment details update successfully.";
                    }
                    else
                    {
                        retStatus = Constants.Error;
                        message = "Credit Card Number already exist!";

                    }
                }
            }
            else
            {
                retStatus = Constants.Error;
                message = Constants.ErrorMessage;
            }
            return this.Json(new
            {
                status = retStatus,
                msg = message,
            }, JsonRequestBehavior.AllowGet);



        }

        string savePaymentProfile(UserPaymentProfileViewModel userPaymentProfileViewModel)
        {
            using (Payment pp = new Payment())
            {
                PaymentInfo profile = new PaymentInfo();
                //var path = DataHelper.ApplicationUrl();
                profile.amount = "0.0";
                profile.NavigateUrl = DataHelper.ApplicationUrl();
                profile.CCNumber = userPaymentProfileViewModel.CreditCardNo;
                profile.CCEXp = userPaymentProfileViewModel.ExMonth.ToString() + userPaymentProfileViewModel.ExYear.ToString();
                profile.CCV = userPaymentProfileViewModel.CVV;
                profile.billingInfo = new BillingInfo();
                profile.billingInfo.FirstName = userPaymentProfileViewModel.FirstName;
                profile.billingInfo.LastName = userPaymentProfileViewModel.LastName;
                profile.billingInfo.Address1 = userPaymentProfileViewModel.Address1;
                profile.billingInfo.Address2 = userPaymentProfileViewModel.Address2;
                profile.billingInfo.City = userPaymentProfileViewModel.City;
                profile.billingInfo.State = userPaymentProfileViewModel.State;
                profile.billingInfo.Zip = userPaymentProfileViewModel.Zip;
                profile.billingInfo.Country = userPaymentProfileViewModel.Country;
                profile.billingInfo.Phone = userPaymentProfileViewModel.Phone;
                profile.billingInfo.Fax = userPaymentProfileViewModel.Phone;
                profile.billingInfo.Email = userPaymentProfileViewModel.Email;
                profile.shippingInfo = new ShippingInfo();
                profile.shippingInfo.FirstName = userPaymentProfileViewModel.FirstName;
                profile.shippingInfo.LastName = userPaymentProfileViewModel.LastName;
                profile.shippingInfo.Address1 = userPaymentProfileViewModel.Address1;
                profile.shippingInfo.Address2 = userPaymentProfileViewModel.Address2;
                profile.shippingInfo.City = userPaymentProfileViewModel.City;
                profile.shippingInfo.State = userPaymentProfileViewModel.State;
                profile.shippingInfo.Zip = userPaymentProfileViewModel.Zip;
                profile.shippingInfo.Country = userPaymentProfileViewModel.Country;
                profile.shippingInfo.Phone = userPaymentProfileViewModel.Phone;
                profile.shippingInfo.Fax = userPaymentProfileViewModel.Phone;
                profile.CustomerVaultId = CurrentUser.getCurUserId().Substring(0, 4) + "_" + userPaymentProfileViewModel.CreditCardNo.Substring(userPaymentProfileViewModel.CreditCardNo.Length - 4);
                if (userPaymentProfileViewModel.Id == 0) // if already exist 
                {
                    pp.CreateUpdateCustomer(profile, Constants.PaymentProductionMode, true);
                }
                else
                {
                    pp.CreateUpdateCustomer(profile, Constants.PaymentProductionMode, false);

                }
            }
            userPaymentProfileViewModel.AnPaymentProfileId = CurrentUser.getCurUserId().Substring(0,4) + "_" + userPaymentProfileViewModel.CreditCardNo.Substring(userPaymentProfileViewModel.CreditCardNo.Length - 4);
            userPaymentProfileViewModel.CreatedById = User.Identity.GetUserId();
            userPaymentProfileViewModel.UserId = CurrentUser.getCurUserId();
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Billing");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(userPaymentProfileViewModel)).Content.ReadAsStringAsync().Result;
            return result;
        }

        public ActionResult DeletePayment(string id)
        {

            try
            {
                var APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "Billing", id);
                var result = (new APICallHelper()).Delete(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Deleted successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult DeletePayment(int? id)
        {

            try
            {
                var APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "Billing/DeleteSingle", id);
                var result = (new APICallHelper()).Delete(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Deleted successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult setPrimaryCard(int id)
        {
            try
            {
                var APIURL = string.Format("{0}/{1}?id={2}&UserId={3}", Constants.ApiURL, "Billing", id, CurrentUser.getCurUserId());
                var result = (new APICallHelper()).Put(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    CurrentUser.getCurUserDetails();
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "Update successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TranscationList()
        {
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Invoice/GetUserInvoice", CurrentUser.getCurUserId());
            var result = (new APICallHelper()).Get(APIURL);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var respone = JsonConvert.DeserializeObject<List<usp_getInvoice_Result>>(result.Content.ReadAsStringAsync().Result);
                return PartialView("TranscationList", respone);
            }
            return PartialView("TranscationList");
        }


        public ActionResult ExportInvoicePdf(string id)
        {
            //MemoryStream m = new MemoryStream();
            string FileName = "Invoice_" + id + ".pdf";
            //StringReader sr = new StringReader(getInvoiceDetailsSection(id));
            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, m);
            //pdfDoc.Open();
            //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
            //return File(m, "application/pdf", FileName);

            String htmlText = getInvoiceDetailsSection(id);
            Document document = new Document();
            string filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);
            if (System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception)
                {

                }
                FileName = "Invoice_" + id + DateTime.Now.Ticks.ToString() + ".pdf";
                filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);

            }
            if (!Directory.Exists(Server.MapPath("~/UserUpload/Pdf")))
            {
                Directory.CreateDirectory(Server.MapPath("~/UserUpload/Pdf/"));
                filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);
            }
            try
            {
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, new StringReader(htmlText));
                document.Close();
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
            }

            return File(filePath, "application/pdf");

        }

        public string getInvoiceDetailsSection(string invoiceId)
        {
            String htmlStr = string.Empty;
            string APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "Invoice/GetInvoiceDetails", invoiceId);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                var result = JsonConvert.DeserializeObject<usp_getInvoiceDetails_Result>(apiResult.ToString());
                htmlStr = "<table width='650' border='0' cellpadding='0' cellspacing='0'><tbody><tr><td align='left'><table width='100%' border='0' cellpadding='0' " +
                    "cellspacing='0'><tr><td align='left'><img src='" + Constants.AppUrl + "/Content/imagesHome/logo.png' height='34 px' alt='' /></td><td align='right' valign='top' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'><strong>" + result.TransactionDate + "</strong></td></tr></table></td></tr><tr><td height='25' align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>Dear " + result.FirstName + " " + result.LastName + " </td></tr>" +
                    "<tr><td align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333; line-height:20px;'>Your " + (!string.IsNullOrEmpty(result.InvoiceType) ? result.InvoiceType.Trim() : String.Empty) + " of $ " + (!string.IsNullOrEmpty(result.AmountCharged) ? result.AmountCharged.Trim() : String.Empty) + " to the following Credit Card:</td></tr><tr><td align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333; line-height:20px;'>" + (!string.IsNullOrEmpty(result.NameOnCard) ? result.NameOnCard.Trim() : String.Empty) + " <br />" +
                    "" + (!string.IsNullOrEmpty(result.Address) ? result.Address.Trim() : String.Empty) + " <br/>" + (!string.IsNullOrEmpty(result.City) ? result.City.Trim() : String.Empty) + ", " + result.State.Trim() + " " + result.Zip.Trim() + "<br />" + (!string.IsNullOrEmpty(result.CardTypeInvoice) ? result.CardTypeInvoice.Trim() : String.Empty) + " " + result.CardNumber.Trim() + "<br/>" + "Phone: " + result.Phone + "<br/></td></tr><tr><td align='left'>&nbsp;</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:16px; color:#333;'><strong>Invoice Details</strong></td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; " +
                    "font-size:13px; color:#333;'><strong>Invoice Number:</strong> " + result.AnTransactionId + "</td></tr><tr><td align='left' style='border-bottom:#333 1px solid; font-size:12px; color:#FFF;'>.</td></tr><tr><td align='left'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='27%' height='25' align='left' valign='middle' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>Description</td><td width='73%' align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>" +
                    "Amount</td></tr><tr><td align='left' style='border-top:#333 2px solid; font-size:12px; color:#FFF;'>.</td><td align='left' style='border-top:#333 2px solid; font-size:12px; color:#FFF;'>.</td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>" + result.InvoiceType + "</td><td align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>$" + result.AmountCharged + " USD</td></tr>" +
                    "<tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>&nbsp;</td><td align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>&nbsp;</td></tr><tr><td align='left'>&nbsp;</td><td align='right'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td width='74%' align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'><strong>Total:</strong></td>" +
                    "<td width='26%' align='right' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>$ " + result.AmountCharged + " USD</td></tr></table></td></tr></table></td></tr><tr><td align='left'>&nbsp;</td></tr><tr><td align='center' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:10px; color:#333;'>It may take a few moments for this transaction to appear in your account.</td></tr><tr><td align='left' style='border-bottom:#333 1px solid; font-size:10px; color:#FFF;'>.</td></tr>" +
                    "<tr><td height='25' align='left' valign='middle' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'><strong>Have issues with this Transaction?</strong></td></tr><tr><td align='left' style='font-family:Calibri, Arial, Tahoma, Verdana; font-size:13px; color:#333;'>You can get in touch with Cheapest Email via email at &nbsp; <a href='mailto:Invoices@CheapestEmail.com'>Invoices@CheapestEmail.com</a></td></tr></tbody></table>";
                //htmlStr = EmailHtml.getInvoiceSend(result.FirstName, result.TransactionDate.ToString(), result.InvoiceType, result.AmountCharged, result.FirstName, result.Address1, result.City, result.State, result.Zip, result.CardTypeInvoice, result.CardNumber, result.Phone, result.AnTransactionId);
            }
            return htmlStr.ToString();

            //htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;\">");
            //    htmlStr.Append("<tr><td style=\" text-align: center;\"><img src=\"http://18.233.130.90:81/Content/imagesHome/logo.png\" /></td></tr>");
            //    htmlStr.Append("</table>");

            //    #region  Bill Address Table

            //    htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:20px;\">");
            //    htmlStr.Append("<tr>");
            //    htmlStr.Append("<td style=\"width:380; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Bill From :</strong></td>");
            //    htmlStr.Append("<td style=\"width:380; padding-left:10px;padding-top:10px;height: 34px;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Bill To :</strong></td>");
            //    htmlStr.Append("</tr>");
            //    htmlStr.Append("<tr>");
            //    htmlStr.Append("<td style=\"width:380;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">Cheapest Email</td>");
            //    htmlStr.Append("<td><table>");
            //    htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;padding-top:10px;font-size: 14px; color: #000000\">" + result.ContactEmail + "</td></tr>");
            //    htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + result.CompanyName + "</td></tr>");
            //    htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;font-size: 14px; color: #000000\">" + result.Address1 + " </td></tr>");
            //    htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + result.City + "," + result.Zip + "</td></tr>");
            //    if (result.Phone == null)
            //    {
            //        htmlStr.Append("<tr><td style=\"width:380;height: 20px; padding-left:10px;font-size: 14px;\">Phone #:</td></tr>");
            //    }

            //    else
            //    {
            //        htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;font-size: 14px;\">Phone #:" + Regex.Replace(result.Phone, @"(\d{3})(\d{3})(\d{4})", "$1-$2-$3") + "</td></tr>");
            //    }
            //    htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\"></td></tr>");
            //    htmlStr.Append("</table></td></tr>");
            //    htmlStr.Append("</table>");
            //    #endregion

            //    #region  Invoice Table

            //    htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
            //    htmlStr.Append("<tr>");
            //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Invoice ID</td>");
            //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Invoice Date</td>");
            //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Amount</td>");
            //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 14px;color: #2f68b1;font-weight: normal;background: #eff8ff;\">Invoice Type</td>");
            //    htmlStr.Append("</tr>");
            //    htmlStr.Append("<tr>");
            //    htmlStr.Append("<td style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
            //    htmlStr.Append(result.InvoiceID);
            //    htmlStr.Append("</td>");
            //    htmlStr.Append("<td  style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
            //    htmlStr.Append(result.TransactionDate);
            //    htmlStr.Append("</td>");
            //    htmlStr.Append("<td  style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
            //    htmlStr.Append(result.AmountCharged);
            //    htmlStr.Append("</td>");
            //    htmlStr.Append("<td  style=\"vertical-align: top;font-family: 'Open Sans';font-size: 12px;color: #2f68b1;font-weight: normal;\">");
            //    htmlStr.Append(result.InvoiceType);
            //    htmlStr.Append("</td>");
            //    htmlStr.Append("</tr>");
            //    htmlStr.Append("</table>");

            //    #endregion

        }


    }
}