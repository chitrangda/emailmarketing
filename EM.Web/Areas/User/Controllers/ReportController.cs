﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EM.Web.Areas.User.Models;
using System.Net;
using System.Data;
using System.IO;
using MoreLinq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using ClosedXML.Excel;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class ReportController : Controller
    {
        private readonly EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        private readonly SendGridAPIHelper sendGridAPIHelper = new SendGridAPIHelper();
        // GET: User/Report
        public ActionResult Index()
        {
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserDashboard");
            var result = (new APICallHelper()).Get(APIURL, CurrentUser.getCurUserId()).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getUserDashboardData_Result>>(result.ToString());
                if (data != null)
                {
                    Session["PlanName"] = data.FirstOrDefault().PlanName;
                    Session["TotalEmails"] = data.FirstOrDefault().TotalEmails;
                    Session["DaysLeft"] = data.FirstOrDefault().DaysLeft;
                }
                return View();

            }
            else
            {
                return RedirectToAction("Login", "Account", new { area = "" });
            }
        }

        public ActionResult SummaryReport(DateTime? startDate, DateTime? endDate, int? pageNumber)
        {

            if (pageNumber == null || pageNumber == 0)
            {
                pageNumber = 1;
            }
            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = Constants.CampaignPageSize;
            var campaignList = GetCampaignList(startDate, endDate, pageNumber);
            ViewBag.TotalRecord = campaignList.Count > 0 ? campaignList.Select(x => x.TotalRecords).FirstOrDefault() : 0;
            return PartialView("SummaryReport", campaignList);
        }

        public ActionResult CampaignList(DateTime? startDate, DateTime? endDate, int? pageNumber)
        {

            if (pageNumber == null || pageNumber == 0)
            {
                pageNumber = 1;
            }
            ViewBag.PageNumber = pageNumber;
            var campaignList = GetCampaignList(startDate, endDate, pageNumber);
            ViewBag.TotalRecord = campaignList.Count > 0 ? campaignList.Select(x => x.TotalRecords).FirstOrDefault() : 0;
            return PartialView("CampaignList", campaignList);
        }

        [HttpPost]

        public List<CampaignSummaryReportViewModel> GetCampaignList(DateTime? startDate, DateTime? endDate, int? pageNumber)
        {
            try
            {
                string APIURLUser = string.Format("{0}/{1}?userid={2}&pageNumber={3}&pageSize={4}&fromDate={5}&toDate={6}", Constants.ApiURL, "Campaign/GetCampaigns", CurrentUser.getCurUserId(), pageNumber, Constants.CampaignPageSize, startDate, endDate);
                var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
                if (count != null)
                {
                    var campaigns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getCampaignReport_Result>>(count.ToString());
                    //var userDetails = CurrentUser.getCurUserDetails();
                    //string queryParams = string.Format(@"
                    //                'aggregated_by': 'day',
                    //                'start_date':'{0}','categories':[{1}],
                    //                ", userDetails.RegisterDate.Value.ToString("yyyy-MM-dd"), string.Join(",", camapaigns.Select(x => x.CampaignId).ToArray()));
                    //var sendGridResponseString = (new SendGridAPIHelper()).getStatsByCategory(queryParams);
                    //var sendGridResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SendGridReportViewModel>>(sendGridResponseString);
                    //if (sendGridResponse.Count < 1)
                    //{
                    //    return new List<CampaignSummaryReportViewModel>();
                    //}
                    var response = (from camps in campaigns
                                    select new CampaignSummaryReportViewModel
                                    {
                                        CampaignId = camps.CampaignId.Value,
                                        CampaignName = camps.CamapignName,
                                        SentDate = camps.SentDate.Value,
                                        Opens = /*sendGridResponse.Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.unique_opens))*/camps.Opens.Value,
                                        Clicks = /*sendGridResponse.Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.unique_clicks))*/camps.Clicks.Value,
                                        Delivered = /*sendGridResponse.Where(res => res.date == camps.SentDate.Value.ToString("yyyy-MM-dd")).Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.delivered))*/camps.Delivered.Value,
                                        Sents = /*sendGridResponse.Where(res => res.date == camps.SentDate.Value.ToString("yyyy-MM-dd")).Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.delivered))*/camps.Sents.Value,
                                        Subscribers = /*sendGridResponse.Where(res => res.date == camps.SentDate.Value.ToString("yyyy-MM-dd")).Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.requests))*/camps.Subscribers.Value,
                                        Bounce = /*sendGridResponse.Where(res => res.date == camps.SentDate.Value.ToString("yyyy-MM-dd")).Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.bounce_drops)) + sendGridResponse.Where(res => res.date == camps.SentDate.Value.ToString("yyyy-MM-dd")).Sum(x => x.stats.Where(r => r.name == camps.CampaignId.Value.ToString()).Sum(r => r.metrics.bounces))*/camps.Bounce.Value,
                                        TotalRecords = camps.TotalRecord.Value,
                                        Subject = camps.Subject,
                                        FromEmail = camps.FromEmail,
                                        Dropped=camps.Dropped.Value,
                                    }).ToList();
                    return response;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exception)
            {
                CreateLog.Log(exception.Message, exception);
                return new List<CampaignSummaryReportViewModel>();
                throw;
            }
        }

        public string FormatDate(DateTime? sentDate)
        {
            var CreatedDate = sentDate == null ? DateTime.UtcNow : Convert.ToDateTime(sentDate);
            var FormattedDate = string.Format("{0} {1}, {2} {3} {4}", CreatedDate.ToString("MMM"), CreatedDate.Day, CreatedDate.Year, CreatedDate.ToString("HH:mm"), CreatedDate.ToString("tt"));
            return FormattedDate;
        }

        public ActionResult CampaignDetailedReport(int? CampaignId, DateTime? SentDate, string Text, string Subject, int Recipient, string FromEmail, string CampaignName, int? IsCampaignFlag)
        {
            ViewBag.CampaignId = CampaignId;
            ViewBag.SentDate = SentDate;
            ViewBag.Subject = Subject;
            ViewBag.Subscriber = Recipient;
            ViewBag.FromEmail = FromEmail;
            ViewBag.IsCampaignFlag = IsCampaignFlag;
            ViewBag.CampaignName = CampaignName;
            ViewBag.Text = Text;
            return View();
        }
        public ActionResult OverViewCamapign(int? CampaignId)
        {
            try
            {
                var response = CampaignDetails(CampaignId);
                return PartialView("OverViewCamapignStats", response);
            }

            catch (Exception exception)
            {
                CreateLog.Log(exception.Message, exception);
                throw;
            }
        }

        public JsonResult GetCamapignGraph(int CampaignId, DateTime SentDate)
        {

            //string APIURLUser = string.Format("{0}/{1}?userid={2}&CampaignId={3}", Constants.ApiURL, "Campaign/GetSendCampaignDetail", CurrentUser.getCurUserId(), CampaignId);
            //string userid = CurrentUser.getCurUserId();
            string aggregated_by = "day";
            if ((DateTime.Now - SentDate).Days > 14)
            {
                aggregated_by = "week";
            }
            var startdate = SentDate.ToString("yyyy-MM-dd");
            var enddate = DateTime.Now.ToString("yyyy-MM-dd");
            string APIURL = string.Format("{0}/{1}?dateFrom={2}&dateTo={3}&aggregated_by={4}&CampaignId={5}",Constants.ApiURL, "Campaign/getReportChartData", startdate, enddate, aggregated_by, CampaignId);
            //string queryParams = string.Format(@"
            //                        'aggregated_by': '{0}', 
            //                        'start_date': '{1}',
            //                        'end_date':'{2}',
            //                        'categories': '{3}'
            //                        ", aggregated_by, SentDate.ToString("yyyy-MM-dd"), DateTime.Now.Date.ToString("yyyy-MM-dd"), CampaignId);
            //string queryParams = string.Format(@"
            //                        'aggregated_by': 'week', 
            //                        'start_date': '2019-07-15',
            //                        'end_date':'2019-07-29',
            //                        'categories': '{0}'
            //                        ", CampaignId);
            //var response = (new SendGridAPIHelper()).getStatsByCategory(queryParams);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<List<usp_getReportChartData_Result>>(result.ToString());
                return this.Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }

        }


        public string getCompaignDetailsReport(int? CampaignId)
        {
            StringBuilder htmlStr = new StringBuilder();
            var response = CampaignDetails(CampaignId);
            if (response != null)
            {
                htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;\">");
                htmlStr.Append("<tr><td style=\" text-align: center;\"><img src=\"http://18.233.130.90:81/Content/imagesHome/logo.png\" /></td></tr>");
                htmlStr.Append("</table>");

                #region  

                htmlStr.Append("<table width=\"660\" style=\"border-collapse: collapse;margin-top:20px;\">");
                htmlStr.Append("<tr>");
                htmlStr.Append("<td style=\"width:380; padding-left:10px;height: 20px;padding-top:10px;font-size: 14px; color: #000000\">" + response.Subscribers + "<span>Recipients</span></td>");
                htmlStr.Append("<td style=\"width:380; padding-left:10px;height: 20px;padding-top:10px;font-size: 14px; color: #000000\"></td>");
                htmlStr.Append("</tr>");
                htmlStr.Append("<tr>");
                htmlStr.Append("<td style=\"width:380; padding-left:10px;height: 20px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\"><span>List:</span>" + response.ListName + "</td>");
                htmlStr.Append("<td><table>");
                htmlStr.Append("<tr><td style=\"width:380; padding-left:10px;height: 20px;padding-top:10px;font-size: 14px; color: #000000\"><span>Delivered:</span>" + response.SentDate + "</td></tr>");
                htmlStr.Append("</table></td></tr>");
                htmlStr.Append("<tr><td style=\"width:380; height: 20px;padding-left:10px;font-size: 14px; color: #000000\"><span>Subject:</span>" + response.Subject + " </td>");
                htmlStr.Append("<td style=\"width:380; padding-left:10px;height: 20px;padding-top:10px;font-size: 14px; color: #000000\"></td>");
                htmlStr.Append("</tr>");

                #endregion

                #region  

                htmlStr.Append("<table width=\"700\" style=\"border-collapse: collapse;margin-top:10px;\">");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Orders</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">0</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Average order revenue</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">$0.00</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Total revenue</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">$0.00</td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Open Rate</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.OpenPercentage + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>List average</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">50.0%</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Industry average</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\"></td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Click Rate</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.ClickPercentage + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>List average</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">0.0%</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Industry average</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\"></td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Opened</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Opens + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Clicked</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Clicks + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Bounced</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Bounce + "</td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Unsubscribed</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Unsubscribes + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Successful deliveries</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Delivered + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Total opens</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Opens + "</td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Last opened</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.LastOpened + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Forwarded</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">0</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Clicks per unique opens</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Unique_opens + "</td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("<tr>");
                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-top:10px;\">");
                htmlStr.Append("<tr><td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Total clicks</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Clicks + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Last clicked</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.LastClicked + "</td></tr></table>");
                htmlStr.Append("</td>");

                htmlStr.Append("<td><table width=\"300\" style=\"border-collapse: collapse;border: 1px solid #e4e5e7;margin-left:10px;margin-top:10px;\"><tr>");
                htmlStr.Append("<td style=\"width:320; padding-left:10px;padding-top:10px;height: 34px;border-right: 1px solid #e4e5e7;font-size: 16px; color: #2f68b1;border-bottom: 1px solid #d3ebfd; background: #eff8ff;\"><strong>Abuse reports</strong></td></tr>");
                htmlStr.Append("<tr><td style=\"width:320;padding-left:10px;height: 20px;padding-top:10px;border-right: 1px solid #e4e5e7;font-size: 14px; color: #000000\">" + response.Spam_reports + "</td></tr></table>");
                htmlStr.Append("</td>");
                htmlStr.Append("</tr>");

                htmlStr.Append("</table>");


                #endregion

            }

            return htmlStr.ToString();
        }


        [HttpPost]
        public ActionResult ViewEmail(int campaignId)
        {
            CampaignSummaryReportViewModel model = new CampaignSummaryReportViewModel();
            if (campaignId > 0)
            {
                string url = string.Format("{0}/{1}/{2}?id={3}", Constants.ApiURL, "Campaign", "GetCampaignById", campaignId);
                var response = (new APICallHelper()).Get(url);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    model = Newtonsoft.Json.JsonConvert.DeserializeObject<CampaignSummaryReportViewModel>(response.Content.ReadAsStringAsync().Result.ToString());


                }
            }
            return PartialView("ViewEmail", model);
        }

        public ActionResult PrintCampaign(int? CampaignId)
        {
            string FileName = "CampaignStats_" + CampaignId + ".pdf";
            String htmlText = getCompaignDetailsReport(CampaignId);
            Document document = new Document();
            string filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);
            if (System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception)
                {

                }
                FileName = "CampaignStats_" + CampaignId + DateTime.Now.Ticks.ToString() + ".pdf";
                filePath = Server.MapPath("~/UserUpload/Pdf/" + FileName);

            }
            try
            {
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.Create));

                document.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, new StringReader(htmlText));
                document.Close();
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
            }

            return File(filePath, "application/pdf");
        }

        public ActionResult ExportCampaignDetail(int? CampaignId, DateTime SentDate)
        {
            var response = CampaignDetails(CampaignId);
            response.SentDate = SentDate;
            DataTable dt = CmpDetails(response);
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "CampaignStats");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "CampaignStats.xlsx");
                }
            }

        }

        DataTable CmpDetails(CampaignSummaryReportViewModel data)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add();
            dt.Columns.Add();
            dt.Rows.Add("Email Campaign Report", "");
            dt.Rows.Add("List Name:", data.ListName);
            dt.Rows.Add("Subject Line:", data.Subject);
            dt.Rows.Add("Delivery Date/Time:", data.SentDate.ToString("dddd MMM dd yyyy hh:mm tt"));
            dt.Rows.Add("Overall Stats", "");
            dt.Rows.Add("Total Recipients:", data.Receipents);
            dt.Rows.Add("Successful Deliveries:", data.Delivered);
            dt.Rows.Add("Bounces:", data.Bounce);
            dt.Rows.Add("Recipients Who Opened:", data.Opens + " " + (data.OpenPercentage));
            dt.Rows.Add("Total Opens:", data.Opens);
            dt.Rows.Add("Last Open Date:", data.LastOpened);
            dt.Rows.Add("Recipients Who Clicked:", data.Clicks + " " + (data.ClickPercentage));
            dt.Rows.Add("Total Clicks:", data.Clicks);
            dt.Rows.Add("Last Click Date:", data.LastClicked);
            dt.Rows.Add("Total Unsubs:", data.Unsubscribes);
            dt.Rows.Add("Total Abuse Complaints:", data.Spam_reports);
            return dt;

        }

        public ActionResult Details(int? CampaignId)
        {
            try
            {
                var response = CampaignDetails(CampaignId);
                return PartialView("Details", response);
            }

            catch (Exception exception)
            {
                CreateLog.Log(exception.Message, exception);
                throw;
            }
        }

        public CampaignSummaryReportViewModel CampaignDetails(int? CampaignId)
        {

            string APIURLUser = string.Format("{0}/{1}?UserId={2}&CampaignId={3}", Constants.ApiURL, "Campaign/GetSingleCampaignReport", CurrentUser.getCurUserId(), CampaignId);

            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var camapaigns = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getSingleCampaignReport_Result>(count.ToString());
                var Clicks = Convert.ToDouble(camapaigns.uniqueclick.Value);
                var Opens = Convert.ToDouble(camapaigns.uniqueopen.Value);
                var Delivered = Convert.ToDouble(camapaigns.delivered.Value);
                var Requests = camapaigns.sents.Value;
                var response = (new CampaignSummaryReportViewModel
                {
                    CampaignId = Convert.ToInt32(camapaigns.campaignid),
                    CampaignName = camapaigns.camapignname,
                    SentDate = camapaigns.sentdate.Value,
                    Opens = camapaigns.opens.Value,
                    Clicks = camapaigns.clicks.Value,
                    Delivered = camapaigns.delivered.Value,
                    Sents = camapaigns.sents.Value,
                    Subscribers = camapaigns.sents.Value,
                    Bounce = camapaigns.bounce.Value,
                    Unique_clicks = camapaigns.uniqueclick.Value,
                    Unique_opens = camapaigns.uniqueopen.Value,
                    Unsubscribes = camapaigns.Unsubscriber.Value,
                    ClickPercentage = Clicks == 0 ? 0 : Math.Round((Clicks / Requests) * 100, 2),
                    DeliveredPercentage = Delivered == 0 ? 0 : Math.Round((Delivered / Requests) * 100, 2),
                    OpenPercentage = Opens == 0 ? 0 : Math.Round((Opens / Requests) * 100, 2),
                    ClickPerUniqueOpenPercentage = Clicks == 0 ? 0 : Math.Round((Clicks / Opens) * 100, 2),
                    ListName = camapaigns.ListName,
                    Subject = camapaigns.subject,
                    Receipents = camapaigns.subscribers,
                    FromName = camapaigns.FromName,
                    FromEmail = camapaigns.fromemail,
                    LastClicked = camapaigns.LastClicked.ToString(),
                    LastOpened = camapaigns.LastOpened.ToString(),
                });

                return response;
            }
            else
            {
                return null;
            }
        }

        public async Task<ActionResult> ReportActivity(int? CampaignId, DateTime? SentDate, string Text, string Subject, int? Recipient, int? Page, string FromEmail, string CampaignName, string Search)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            int PageSize = Constants.ReportPageSize;
            switch (Text)
            {
                case "Recipient":

                    model = await SubscriberEmails(CampaignId, SentDate, Recipient, Page, PageSize, Text, Search);
                    ViewBag.Text = Text;
                    break;
                case "Delivered":

                    model = await DeliveredEmails(CampaignId, SentDate, Recipient, Page, PageSize, Text, Search);
                    ViewBag.Text = Text;
                    break;
                case "Open":
                    model = await ReportDetails(CampaignId, SentDate, Recipient, Page, PageSize, Text, Search);
                    ViewBag.Text = Text;
                    break;
                case "Click":
                    model = await ReportDetails(CampaignId, SentDate, Recipient, Page, PageSize, Text, Search);
                    ViewBag.Text = Text;
                    break;
                case "UnSubscribe":
                    model = await UnSubscribeDetails(CampaignId, Page, PageSize,Search);
                    ViewBag.Text = Text;
                    break;
                case "Bounce":
                    model = await BounceDetails(CampaignId, SentDate, Recipient, Page, PageSize, Text, Search);
                    ViewBag.Text = Text;
                    break;
                case "DidntOpen":
                    model = await DidntOpenEmail(CampaignId, SentDate, Recipient, Page, PageSize, "",Search);
                    ViewBag.Text = Text;
                    break;
                case "Suppressed":
                    model = await SuppressedEmails(CampaignId, SentDate, Recipient, Page, PageSize,"", Search);
                    ViewBag.Text = Text;
                    break;
            }
            ViewBag.CampaignId = CampaignId;
            ViewBag.SentDate = SentDate;
            ViewBag.Subject = Subject;
            ViewBag.Recipient = Recipient;
            ViewBag.PageNumber = Page;
            ViewBag.FromEmail = FromEmail;
            ViewBag.CampaignName = CampaignName;
            ViewBag.TotalRecord = model.Count > 0 ? model.Select(x => x.TotalRecords).FirstOrDefault() : 0;
            return PartialView("ReportActivity", model);
        }

        public async Task<ActionResult> DetailsReportSendTo(string Email, int? CampaignId, string FromEmail)
        {
            CampaignSummaryReportViewModel model = new CampaignSummaryReportViewModel();
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&Email={3}", Constants.ApiURL, "Campaign/GetStat", CampaignId, Email);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var camps = JsonConvert.DeserializeObject<usp_getActivityStat_Result>(response.ToString());
                if (camps != null)
                {
                    model = (new CampaignSummaryReportViewModel
                    {
                        Opens = Convert.ToDouble(camps.Open),
                        Clicks = Convert.ToDouble(camps.Click),
                    });
                }
                else
                {
                    model = (new CampaignSummaryReportViewModel
                    {
                        Opens = 0,
                        Clicks = 0,
                    });
                }

            }
            else
            {
                model = (new CampaignSummaryReportViewModel
                {
                    Opens = 0,
                    Clicks = 0,
                });
            }
            return View("DetailsReportSendTo", model);
        }

        public async Task<List<CampaignSummaryReportViewModel>> ReportDetails(int? CampaignId, DateTime? SentDate, int? Recipient, int? Page, int? PageSize, string Text, string Search)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&Stat={3}&PageNumber={4}&PageSize={5}&Email={6}", Constants.ApiURL, "Campaign/GetCampaignStat", CampaignId, Text, Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var oReceipientData = JsonConvert.DeserializeObject<List<usp_getCampaignActivityStat_Result>>(response.ToString());
                var result = (from e in oReceipientData
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = e.SentDate.Value,
                                  ListName = e.ListName,
                                  Stat = e.Stat.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();

                return result;
            }
            else
            {
                var email = JsonConvert.DeserializeObject<List<usp_getCampaignActivityStat_Result>>(response.ToString());
                var result = (from e in email
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  ListName = e.ListName,
                                  Stat = 0,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();
                return result;
            }
        }
        public async Task<List<CampaignSummaryReportViewModel>> UnSubscribeDetails(int? CampaignId, int? Page, int? PageSize, string Search)
        {
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&UserId={3}&PageNumber={4}&PageSize={5}&EmailSearch={6}", Constants.ApiURL, "Campaign/GetUnSubscribeEmails", CampaignId, CurrentUser.getCurUserId(), Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var email = JsonConvert.DeserializeObject<List<usp_CampaignUnsubscriber_Result>>(response.ToString());
                var result = (from e in email
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  ListName = e.ListName,
                                  TotalRecords = e.TotalRecord.Value
                              }).ToList();
                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<ActionResult> ExportReportDetails(int? CampaignId, int? offset ,DateTime? SentDate, string Text, int Recipient, int? Page, string Search)
        {
            DataTable dt = new DataTable();
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            //ViewBag.PageSize = Constants.ReportPageSize;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            ViewBag.PageNumber = Page;
            string fileName = "ReportDetails.xlsx";
            double Addoffset = Convert.ToDouble(offset);
            switch (Text)
            {
                case "Recipient":

                    model = await SubscriberEmails(CampaignId, SentDate, Recipient, Page, null, Text, Search);
                    dt = model.Select(r => new { Email =  r.EmailId, r.FirstName, r.LastName }).ToDataTable();
                    fileName = "ReportDetails_RecipientDetails.xlsx";
                    break;
                case "Delivered":

                    model = await DeliveredEmails(CampaignId, SentDate, Recipient, Page, null,Text, Search);
                    dt = model.Select(r => new { Email =  r.EmailId, r.FirstName, r.LastName }).ToDataTable();
                    fileName = "ReportDetails_SentTo.xlsx";
                    break;
                case "Open":
                    model = await ReportDetails(CampaignId, SentDate, Recipient, Page,null, Text, Search);
                    dt = model.Select(r => new { Email = r.EmailId, r.Stat, r.FirstName, r.LastName, OpenedDate = r.SentDate.AddMinutes(Addoffset).ToString("MM-dd-yyyy hh:mm:ss tt") }).ToDataTable();
                    fileName = "ReportDetails_Opened.xlsx";
                    break;
                case "Click":
                    model = await ReportDetails(CampaignId, SentDate, Recipient, Page,null, Text, Search);
                    dt = model.Select(r => new { Email = r.EmailId, r.Stat, r.FirstName, r.LastName, ClickedDate = r.SentDate.AddMinutes(Addoffset).ToString("MM-dd-yyyy hh:mm:ss tt") }).ToDataTable();
                    fileName = "ReportDetails_Clicked.xlsx";
                    break;
                case "UnSubscribe":
                    model = await UnSubscribeDetails(CampaignId, Page,null, Search);
                    dt = model.Select(r => new { Email = r.EmailId, r.FirstName, r.LastName }).ToDataTable();
                    fileName = "ReportDetails_UnSubscribed.xlsx";
                    break;
                case "Bounce":
                    model = await BounceDetails(CampaignId, SentDate, Recipient, Page,null, Text, Search);
                    dt = model.Select(r => new { Email = r.EmailId, r.FirstName, r.LastName }).ToDataTable();
                    fileName = "ReportDetails_Bounced.xlsx";
                    break;
                case "DidntOpen":
                    model = await DidntOpenEmail(CampaignId, SentDate, Recipient, Page,null, "", Search);
                    dt = model.Select(r => new { Email = r.EmailId, r.FirstName, r.LastName }).ToDataTable();
                    fileName = "ReportDetails_DidntOpen.xlsx";
                    break;
                case "Suppressed":
                    model = await SuppressedEmails(CampaignId, SentDate, Recipient, Page,null, "", Search);
                    dt = model.Select(r => new { Email = r.EmailId, r.FirstName, r.LastName }).ToDataTable();
                    fileName = "ReportDetails_Suppressed.xlsx";
                    break;
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "ReportDetails");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
                }
            }
        }

        public ActionResult Links(int? CampaignId, string CampaignName, DateTime? SentDate, string Subject, int? Receipents, string FromEmail)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}", Constants.ApiURL, "Campaign/GetCampaignLinkStat", CampaignId);
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var links = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getCampaignLinkStat_Result>>(count.ToString());
                model = (from e in links
                         select new CampaignSummaryReportViewModel
                         {
                             URL = e.URL,
                             Clicks = Convert.ToDouble(e.ClickCount),
                             Unique_clicks = e.UniqueClick.Value,
                             ClickPercentage = Convert.ToDouble(e.ClickPer),
                             UniqueClickPer = e.UniqueClickPer.Value,
                             CampaignId = CampaignId.Value,
                             SentDate = SentDate.Value,
                             Subject = Subject,
                             Receipents = Receipents,
                             FromEmail = FromEmail,
                             CampaignName = CampaignName
                         }).ToList();



            }
            return PartialView("Links", model);
        }

        public async Task<List<CampaignSummaryReportViewModel>> BounceDetails(int? CampaignId, DateTime? SentDate, int? Recipient, int? Page, int? PageSize, string Text, string Search)
        {

            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&PageNumber={3}&PageSize={4}&EmailSearch={5}", Constants.ApiURL, "Campaign/GetSuppressedEmails", CampaignId, Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var email = JsonConvert.DeserializeObject<List<usp_getSuppressedSubscribers_Result>>(response.ToString());
                var result = (from e in email
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();

                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<CampaignSummaryReportViewModel>> DidntOpenEmail(int? CampaignId, DateTime? SentDate, int? Recipient, int? Page, int? PageSize, string Text, string Search)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&UserId={3}&PageNumber={4}&PageSize={5}&EmailSearch={6}", Constants.ApiURL, "Campaign/GetCampaignDidntOpen", CampaignId,CurrentUser.getCurUserId(),Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var oReceipientData = JsonConvert.DeserializeObject<List<usp_getDidNotOpenEmails_Result>>(response.ToString());
                var result = (from e in oReceipientData
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();

                return result;
            }
            else
            {
                var email = JsonConvert.DeserializeObject<List<usp_getDidNotOpenEmails_Result>>(response.ToString());
                var result = (from e in email
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();
                return result;
            }
        }

        public async Task<List<CampaignSummaryReportViewModel>> SuppressedEmails(int? CampaignId, DateTime? SentDate, int? Recipient, int? Page, int? PageSize, string Text, string Search)
        {
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&PageNumber={3}&PageSize={4}&EmailSearch={5}", Constants.ApiURL, "Campaign/GetSuppressedEmails", CampaignId, Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var email = JsonConvert.DeserializeObject<List<usp_getSuppressedSubscribers_Result>>(response.ToString());
                var result = (from e in email
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();

                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<CampaignSummaryReportViewModel>> DeliveredEmails(int? CampaignId, DateTime? SentDate, int? Recipient, int? Page, int? PageSize, string Text, string Search)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&UserId={3}&PageNumber={4}&PageSize={5}&EmailSearch={6}", Constants.ApiURL, "Campaign/GetCampaignDeliveredEmails", CampaignId, CurrentUser.getCurUserId(), Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var oReceipientData = JsonConvert.DeserializeObject<List<usp_getDeliveredEmails_Result>>(response.ToString());
                var result = (from e in oReceipientData
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();

                return result;
            }
            else
            {
                var email = JsonConvert.DeserializeObject<List<usp_getDeliveredEmails_Result>>(response.ToString());
                var result = (from e in email
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  SentDate = SentDate.Value,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();
                return result;
            }
        }

        public async Task<List<CampaignSummaryReportViewModel>> SubscriberEmails(int? CampaignId, DateTime? SentDate, int? Recipient, int? Page, int? PageSize, string Text, string Search)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            string APIURLUser = string.Format("{0}/{1}?CampaignId={2}&UserId={3}&PageNumber={4}&PageSize={5}&EmailSearch={6}", Constants.ApiURL, "Campaign/GetCampaignRecipientEmails", CampaignId, CurrentUser.getCurUserId(), Page, PageSize, Search);
            var response = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (response != null)
            {
                var oReceipientData = JsonConvert.DeserializeObject<List<usp_getRecipientData_Result>>(response.ToString());
                var result = (from e in oReceipientData
                              select new CampaignSummaryReportViewModel
                              {
                                  EmailId = e.Email,
                                  FirstName = e.FirstName,
                                  LastName = e.LastName,
                                  TotalRecords = e.TotalRecords.Value,
                              }).ToList();

                return result;
            }
            else
            {
                return null;
            }
        }



    }
}
 
           







