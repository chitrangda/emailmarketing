﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.User.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class EventbriteIntegrationController : Controller
    {
        // GET: User/Integration
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public ActionResult Index()
        {
            EmailMarketingDbEntities db = new EmailMarketingDbEntities();
            var Id = CurrentUser.getCurUserId();
            var oEventbriteUser = db.Eventbrite_User_AccessCode.FirstOrDefault(x => x.UserId == Id);
            if (oEventbriteUser != null)
            {
                var userInfo = EventBriteAPIHelper.getUserInfo(oEventbriteUser.AccessCode);
                ViewBag.AccessToken = oEventbriteUser.AccessCode;
                ViewBag.OrgId = oEventbriteUser.OrgID;
                return View(userInfo);
            }
            else
            {
                ViewBag.AccessToken = "";
                ViewBag.OrgId = "";
                return View();

            }

        }
        //[HttpGet]
        //public JsonResult CheckAccessCode()
        //{
        //    EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        //    var Id = CurrentUser.getCurUserId();
        //    var Eventbrite = db.Eventbrite_User_AccessCode.FirstOrDefault(x => x.UserId == Id);
        //    if (Eventbrite != null)
        //    {
        //        if (Eventbrite.AccessCode != null)
        //        {
        //            return this.Json(new
        //            {
        //                status = Constants.Success,
        //                msg = Eventbrite.AccessCode + "," + Eventbrite.OrgID,
        //            }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            return this.Json(new
        //            {
        //                status = Constants.Error,
        //                msg = "",
        //            }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    else
        //    {
        //        return this.Json(new
        //        {
        //            status = Constants.Error,
        //            msg = "",
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        public ActionResult Save_EventbriteUserAccessToken(string code)
        {
            var Id = CurrentUser.getCurUserId();
            string redirectUri = System.Configuration.ConfigurationManager.AppSettings["RedirectionURLEventBrite"];
            string sAccessCode = EventBriteAPIHelper.getUserAccessToken(redirectUri, code);
            if (sAccessCode != null)
            {
                string sOrgId = EventBriteAPIHelper.getOrgnization(sAccessCode, null);
                var sEventbrite_UserId= EventBriteAPIHelper.getUserInfo(sAccessCode);
                EventBriteAPIHelper.Insert_EventbriteWebhookLink(sAccessCode, sOrgId);
                using (var db = new EmailMarketingDbEntities())
                {
                    var oEventbriteUser = db.Eventbrite_User_AccessCode.SingleOrDefault(x => x.UserId == Id && x.AccessCode == sAccessCode);
                    if (oEventbriteUser == null)
                    {

                        Eventbrite_User_AccessCode oEventBrite_AccessCode = new Eventbrite_User_AccessCode();
                        oEventBrite_AccessCode.UserId = CurrentUser.getCurUserId();
                        oEventBrite_AccessCode.AccessCode = sAccessCode;
                        oEventBrite_AccessCode.OrgID = sOrgId;
                        oEventBrite_AccessCode.EventBrite_User_Id = sEventbrite_UserId.id;
                        db.Eventbrite_User_AccessCode.Add(oEventBrite_AccessCode);
                        db.SaveChanges();
                    }
                    else
                    {
                        oEventbriteUser.AccessCode = sAccessCode;
                        oEventbriteUser.OrgID = sOrgId;
                        db.SaveChanges();
                    }
                }
                return RedirectToActionPermanent("GetEventBriteEventslist", new { OrgId = sOrgId, accesscode = sAccessCode });
            }
            else
            {
                return Redirect("https://www.eventbrite.com/oauth/authorize?response_type=code&client_id="+ EventBriteAPIHelper.APIkey + "&redirect_uri=" + redirectUri);
            }
        }


        public ActionResult GetEventBriteEventslist(string accesscode, string OrgId)
        {
            //var Id = CurrentUser.getCurUserId();
            //if (accesscode == null)
            //{
            //    string redirectUri = System.Configuration.ConfigurationManager.AppSettings["RedirectionURLEventBrite"];
            //    accesscode = EventBriteAPIHelper.getUserAccessToken(redirectUri, code);
            //}
            //ViewBag.AccessCode = accesscode;
            //if (OrgId == null || OrgId=="")
            //{
            //    OrgId = EventBriteAPIHelper.getOrgnization(accesscode, null);
            //}
            //using (var db = new EmailMarketingDbEntities())
            //{
            //    var EventbriteCode = db.Eventbrite_User_AccessCode.SingleOrDefault(x => x.UserId == Id && x.AccessCode == accesscode);
            //    if (EventbriteCode == null)
            //    {
            //        if (accesscode != null)
            //        {
            //            Eventbrite_User_AccessCode oEventBrite_AccessCode = new Eventbrite_User_AccessCode();
            //            oEventBrite_AccessCode.UserId = Id;
            //            oEventBrite_AccessCode.AccessCode = accesscode;
            //            oEventBrite_AccessCode.OrgID = OrgId;
            //            db.Eventbrite_User_AccessCode.Add(oEventBrite_AccessCode);
            //            db.SaveChanges();
            //        }

            //    }
            //    else
            //    {
            //        EventbriteCode.AccessCode = accesscode;
            //        EventbriteCode.OrgID = OrgId;
            //        db.SaveChanges();
            //    }
            //}
            var response = EventBriteAPIHelper.getEventsWithAttendeeCount(OrgId, accesscode);

            if (response != null)
            {
                return View(response);
            }
            else
            {
                return View();
            }
        }


        [HttpGet]
        public ActionResult ListData()
        {
            List<usp_getUserList_Result> ListData = DataHelper.GetListWithSubscriberByListId(CurrentUser.getCurUserId());
            return PartialView("EventListGrid", ListData);
        }

        public JsonResult SyncData(List<EventBriteModel> eventModelList)
        {
            var Userdetails = CurrentUser.getCurUserDetails();
            int ListId = 0;
            foreach (var item in eventModelList)
            {
                var list = db.Lists.Where(x => x.ListName.ToLower() == item.EventName.ToLower() && x.CreatedById == Userdetails.Id && x.IsDeleted == false && x.Eventbrite_EventId == item.EventId).FirstOrDefault();
                if (list == null)
                {
                    var ListDetails = db.usp_CreateList(item.EventName, Userdetails.Id, User.Identity.GetUserId(), User.Identity.GetUserName(),item.EventId).FirstOrDefault();
                    ListId = Convert.ToInt32(ListDetails.ListId);
                }
                else
                {
                    ListId = Convert.ToInt32(list.ListId);
                }

                CurrentUser.SetAuthoriztionInfo();
                var UserAuthDetails = System.Web.HttpContext.Current.Session["UserAuthorizationInfo"] as usp_getAuthorization_Result;
                var Attendess = EventBriteAPIHelper.getEventAttendes(item.EventId);
                if (Attendess != null)
                {

                    if (Attendess.attendees.Count() + UserAuthDetails.TotalSubscriberCreated <= UserAuthDetails.MaxSubscriber)
                    {

                        List<SubscriberViewModel> listSubscriber = new List<SubscriberViewModel>();
                        listSubscriber = Attendess.attendees.Where(x => !string.IsNullOrEmpty(x.profile.email)).Select(x =>
                                new SubscriberViewModel
                                {
                                    SubscriberId = 0,
                                    ListId = ListId,
                                    FirstName = x.profile.firstname,
                                    LastName = x.profile.lastname,
                                    Email = x.profile.email.Contains("\n") ? x.profile.email.Replace("\n", "") : x.profile.email,
                                    Address1 = string.Empty,
                                    Address2 = string.Empty,
                                    City = string.Empty,
                                    State = string.Empty,
                                    Country = string.Empty,
                                    PhoneNumber = string.Empty,
                                    DOB = DateTime.UtcNow,
                                    EmailPermission = true,
                                    ZipCode = string.Empty,
                                    CreatedById = Userdetails.Id,
                                    Eventbrite_AttendeeId = x.id,
                                }).ToList();
                        List<SubscriberViewModel> Subscriber = listSubscriber.GroupBy(p => p.Email.ToLower()).Select(g => g.First()).ToList();
                        if (Subscriber.Count > 0)
                        {
                            if (Subscriber.Count <= Convert.ToInt32(Constants.SubscriberLimit))
                            {
                                string URL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/EventbriteSyncingAttendees");
                                var response = (new APICallHelper()).Post(URL, JsonConvert.SerializeObject(Subscriber));
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    var res1 = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(response.Content.ReadAsStringAsync().Result.ToString());

                                }
                            }
                        }
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Error,
                            msg = "Maximum subscriber limit of your plan does not allow you to add subscribers.",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return this.Json(new
            {
                status = Constants.Success,
                msg = "Successfully Synced",
            }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult SyncSingleData(int? id, string ListName)
        {
            var Userdetails = CurrentUser.getCurUserDetails();
            int ListId = 0;
            var orgid = EventBriteAPIHelper.getOrgnization(null, null);
            var getEventsList = EventBriteAPIHelper.getEvents(orgid, null);
            if (id == 0)
            {
                var Eventbrite_EventId = string.Join(",", getEventsList.events.Select(x => x.id).ToArray());
                var ListDetails = db.usp_CreateList(ListName, Userdetails.Id, User.Identity.GetUserId(), User.Identity.GetUserName(), Eventbrite_EventId).FirstOrDefault();
                ListId = Convert.ToInt32(ListDetails.ListId);
            }
            else
            {
                ListId = Convert.ToInt32(id);
            }
            foreach (var item in getEventsList.events)
            {
                var Attendess = EventBriteAPIHelper.getEventAttendes(item.id);
                CurrentUser.SetAuthoriztionInfo();
                var UserAuthDetails = System.Web.HttpContext.Current.Session["UserAuthorizationInfo"] as usp_getAuthorization_Result;
                if (Attendess != null)
                {
                    if (Attendess.attendees.Count() + UserAuthDetails.TotalSubscriberCreated <= UserAuthDetails.MaxSubscriber)
                    {
                        List<SubscriberViewModel> listSubscriber = new List<SubscriberViewModel>();
                        listSubscriber = Attendess.attendees.Where(x => !string.IsNullOrEmpty(x.profile.email)).Select(x =>
                        new SubscriberViewModel
                        {
                            SubscriberId = 0,
                            ListId = ListId,
                            FirstName = x.profile.firstname,
                            LastName = x.profile.lastname,
                            Email = x.profile.email.Contains("\n") ? x.profile.email.Replace("\n", "") : x.profile.email,
                            Address1 = string.Empty,
                            Address2 = string.Empty,
                            City = string.Empty,
                            State = string.Empty,
                            Country = string.Empty,
                            PhoneNumber = string.Empty,
                            DOB = DateTime.UtcNow,
                            EmailPermission = true,
                            ZipCode = string.Empty,
                            CreatedById = Userdetails.Id,
                            Eventbrite_AttendeeId = x.id,
                        }).ToList();
                        List<SubscriberViewModel> Subscriber = listSubscriber.GroupBy(p => p.Email.ToLower()).Select(g => g.First()).ToList();
                        if (Subscriber.Count > 0)
                        {
                            if (Subscriber.Count <= Convert.ToInt32(Constants.SubscriberLimit))
                            {
                                string URL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/EventbriteSyncingAttendees");
                                var response = (new APICallHelper()).Post(URL, JsonConvert.SerializeObject(Subscriber));
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    var res1 = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(response.Content.ReadAsStringAsync().Result.ToString());
                                }
                            }
                        }
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Error,
                            msg = "Maximum subscriber limit of your plan does not allow you to add subscribers.",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return this.Json(new
            {
                status = Constants.Success,
                msg = "Successfully Synced",
            }, JsonRequestBehavior.AllowGet);
        }

        public bool CheckDuplicateListName(string ListName)
        {
            var Userdetails = CurrentUser.getCurUserDetails();
            var list = db.Lists.Where(x => x.ListName.ToLower() == ListName.ToLower() && x.CreatedById == Userdetails.Id && x.IsDeleted == false).FirstOrDefault();
            if (list == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public JsonResult SyncDataInMasterList(List<EventBriteModel> eventModelList, string ListName)
        {
            var Userdetails = CurrentUser.getCurUserDetails();
            int ListId = 0;
            var list = db.Lists.Where(x => x.ListName.ToLower() == ListName.ToLower() && x.CreatedById == Userdetails.Id && x.IsDeleted == false).FirstOrDefault();
            if (list == null)
            {
                var Eventbrite_EventId = string.Join(",", eventModelList.Select(r => r.EventId).ToArray());
                var ListDetails = db.usp_CreateList(ListName, Userdetails.Id, User.Identity.GetUserId(), User.Identity.GetUserName(), Eventbrite_EventId).FirstOrDefault();
                ListId = Convert.ToInt32(ListDetails.ListId);
            }
            foreach (var item in eventModelList)
            {
                CurrentUser.SetAuthoriztionInfo();
                var UserAuthDetails = System.Web.HttpContext.Current.Session["UserAuthorizationInfo"] as usp_getAuthorization_Result;
                var Attendess = EventBriteAPIHelper.getEventAttendes(item.EventId);
                if (Attendess != null)
                {
                    if (Attendess.attendees.Count() + UserAuthDetails.TotalSubscriberCreated <= UserAuthDetails.MaxSubscriber)
                    {

                        List<SubscriberViewModel> listSubscriber = new List<SubscriberViewModel>();
                        listSubscriber = Attendess.attendees.Where(x => !string.IsNullOrEmpty(x.profile.email)).Select(x =>
                                new SubscriberViewModel
                                {
                                    SubscriberId = 0,
                                    ListId = ListId,
                                    FirstName = x.profile.firstname,
                                    LastName = x.profile.lastname,
                                    Email = x.profile.email.Contains("\n") ? x.profile.email.Replace("\n", "") : x.profile.email,
                                    Address1 = string.Empty,
                                    Address2 = string.Empty,
                                    City = string.Empty,
                                    State = string.Empty,
                                    Country = string.Empty,
                                    PhoneNumber = string.Empty,
                                    DOB = DateTime.UtcNow,
                                    EmailPermission = true,
                                    ZipCode = string.Empty,
                                    CreatedById = Userdetails.Id,
                                    Eventbrite_AttendeeId = x.id,
                                }).ToList();
                        List<SubscriberViewModel> Subscriber = listSubscriber.GroupBy(p => p.Email.ToLower()).Select(g => g.First()).ToList();
                        if (Subscriber.Count > 0)
                        {
                            if (Subscriber.Count <= Convert.ToInt32(Constants.SubscriberLimit))
                            {
                                string URL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/EventbriteSyncingAttendees");
                                var response = (new APICallHelper()).Post(URL, JsonConvert.SerializeObject(Subscriber));
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    var res1 = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(response.Content.ReadAsStringAsync().Result.ToString());

                                }
                            }
                        }
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Error,
                            msg = "Maximum subscriber limit of your plan does not allow you to add subscribers.",
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return this.Json(new
            {
                status = Constants.Success,
                msg = "Successfully Synced",
            }, JsonRequestBehavior.AllowGet);
        }
    }
}