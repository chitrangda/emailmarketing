﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Areas.User.Models;
using EM.Web.Extentions;
using EM.Web.Models;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using NMIPayment;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class DashboardController : Controller
    {
        SendGridAPIHelper _sendGrid = new SendGridAPIHelper();
        MailgunHelper _mailGun = new MailgunHelper();
        Mail oMail = new Mail();
        // GET: User/Home
        public ActionResult Index(ManageMessage? message, string id = null)
        {
            ViewBag.StatusMessage =
            message == ManageMessage.ChangePasswordSuccess ? "Password changed successfully."
            : message == ManageMessage.Error ? "An error has occurred."
            : "";
            if (id != null)
            {
                CurrentUser.createCurUserSession(UserRole.User.ToString(), id);
                CurrentUser.SetAuthoriztionInfo();

            }
            else
            {
                CurrentUser.SetAuthoriztionInfo();

            }
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserDashboard");
            var result = (new APICallHelper()).Get(APIURL, CurrentUser.getCurUserId()).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getUserDashboardData_Result>>(result.ToString());
                return View(data);
            }
            else
            {
                return RedirectToAction("Login", "Account", new { area = "" });
            }

        }


        public ActionResult Campaign()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            UserInfoViewModel model = new UserInfoViewModel();

            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserInfo");
            var result = (new APICallHelper()).Get(APIURL, CurrentUser.getCurUserId()).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<UserInfoViewModel>(result.ToString());
                model = data;

            }
            var StateList = DataHelper.GetStateList(null);
            TempData["State"] = StateList;
            TempData.Keep("State");
            TempData["Country"] = DataHelper.GetCountryList(model.Country);
            TempData.Keep("Country");
            var selectedIndustry = model.IndustryRefId == null ? "" : model.IndustryRefId.ToString();
            TempData["IndustryList"] = DataHelper.GetIndustryList(selectedIndustry);
            TempData.Keep("IndustryList");
            TempData["TimezoneList"] = DataHelper.GetTimeZone(model.TimeZoneID.ToString());
            TempData.Keep("TimezoneList");
            return View(model);
        }

        [HttpPost]
        public ActionResult UserProfile(UserInfoViewModel userInfoViewModel, HttpPostedFileBase UserProfilePhoto)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var UserProfileImageURL = UserProfilePhoto.SaveFile(userInfoViewModel.ProfileImage);
                    String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserInfo");
                    userInfoViewModel.ProfileImage = string.IsNullOrEmpty(UserProfileImageURL) ? userInfoViewModel.ProfileImage : UserProfileImageURL;
                    var result = (new APICallHelper()).Put(APIURL, JsonConvert.SerializeObject(userInfoViewModel)).StatusCode;
                    if (result == System.Net.HttpStatusCode.OK)
                    {
                        ViewBag.UpdateStatus = "true";
                        CurrentUser.createCurUserSession(UserRole.User.ToString(), userInfoViewModel.Id);
                    }
                    else
                    {
                        ViewBag.UpdateStatus = "false";
                    }
                }
                catch (Exception)
                {
                    return View(userInfoViewModel);
                }
            }
            TempData.Keep("State");
            TempData.Keep("Country");
            TempData.Keep("IndustryList");
            TempData.Keep("TimezoneList");
            return View(userInfoViewModel);
        }

        public ActionResult ViewSubsribers()
        {
            String APIURL = string.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "UserList/GetRecent", CurrentUser.getCurUserId());
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<ListViewModel>(result.ToString());
                return RedirectToAction("Index", "Subscriber", new { area = "User", id = data.ListId, name = data.ListName, method = "view" });

            }
            else
            {
                return RedirectToAction("Index", "List", new { area = "User" });
            }

        }

        public ActionResult AddSubsriber()
        {
            String APIURL = string.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "UserList/GetRecent", CurrentUser.getCurUserId());
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<ListViewModel>(result.ToString());
                return RedirectToAction("Index", "Subscriber", new { area = "User", id = data.ListId, name = data.ListName, method = "add" });

            }
            else
            {
                return RedirectToAction("Index", "List", new { area = "User" });
            }


        }


        public JsonResult GetEmailStatusChart(DateTime? startdate, DateTime? endDate)
        {
            //String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Campaign/GetCampaignByUserId");
            string userid = CurrentUser.getCurUserId();
            //  string queryParams = null;
            string APIURL = null;
            string StartDate = null;
            string Enddate = null;
            if (startdate != null)
            {

                StartDate = startdate.Value.ToString("yyyy-MM-dd");
                Enddate = endDate != null ? endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
                //queryParams = string.Format(@"
                //                    'aggregated_by': 'day', 
                //                    'start_date': '{0}',
                //                    'end_date' : '{1}',
                //                    'categories': '{2}'
                //                    ", sStartDate, sEndDate, userid);
                APIURL = string.Format("{0}/{1}?id={2}&dateFrom={3}&dateTo={4}", Constants.ApiURL, "UserDashboard/GetEmailActivity", userid, StartDate, Enddate);
            }
            else
            {
                StartDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                Enddate = DateTime.Now.ToString("yyyy-MM-dd");
                //queryParams = string.Format(@"
                //                    'aggregated_by': 'day', 
                //                    'start_date': '{0}',
                //                    'categories': '{1}'
                //                    ", startDate, userid);
                APIURL = string.Format("{0}/{1}?id={2}&dateFrom={3}&dateTo={4}", Constants.ApiURL, "UserDashboard/GetEmailActivity", CurrentUser.getCurUserId(), StartDate, Enddate);

            }
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;

            //var response = JsonConvert.DeserializeObject(_sendGrid.geStats(queryParams));
            // var response = _sendGrid.getStatsByCategory(queryParams);
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<List<usp_getEmailActivity_Result>>(result.ToString());
                return this.Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }


        }

        public JsonResult GetFilterEmailStatusChart(DateTime? startdate, DateTime? endDate)
        {
            string userid = CurrentUser.getCurUserId();
            var queryParams = string.Empty;
            string sStartDate = startdate.Value.ToString("yyyy-MM-dd");
            string sEndDate = endDate != null ? endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
            queryParams = string.Format(@"
                                    'aggregated_by': 'day', 
                                    'start_date': '{0}',
                                    'end_date' : '{1}',
                                    'categories': '{2}'
                                    ", sStartDate, sEndDate, userid);
            var response = _sendGrid.getStatsByCategory(queryParams);
            if (response != null)
            {
                return this.Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }

        }

        public ActionResult GetAllEmailStatusChart()
        {
            var userDetails = CurrentUser.getCurUserDetails();
            if (userDetails != null)
            {
                string userid = CurrentUser.getCurUserId();
                string startDate = userDetails.RegisterDate != null ? userDetails.RegisterDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
                string Enddate = DateTime.Now.ToString("yyyy-MM-dd");
                var APIURL = string.Format("{0}/{1}?id={2}&dateFrom={3}&dateTo={4}", Constants.ApiURL, "UserDashboard/GetAllEmailActivity", CurrentUser.getCurUserId(), startDate, Enddate);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    var response = JsonConvert.DeserializeObject<List<usp_getAllEmailActivity_Result>>(result.ToString());
                    return this.Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(Constants.ErrorTitle);
                }
            }
            else
            {
                return RedirectToAction("LogOff", "Account", new { area = "" });
            }
        }

        public ActionResult getFilterAllEmailStatusChart(DateTime? startdate, DateTime? endDate)
        {
            var userDetails = CurrentUser.getCurUserDetails();
            string APIURL = null;
            string StartDate = null;
            string Enddate = null;
            if (startdate != null)
            {
                StartDate = startdate.Value.ToString("yyyy-MM-dd");
                Enddate = endDate != null ? endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
                APIURL = string.Format("{0}/{1}?id={2}&dateFrom={3}&dateTo={4}", Constants.ApiURL, "UserDashboard/GetAllEmailActivity", CurrentUser.getCurUserId(), StartDate, Enddate);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    var response = JsonConvert.DeserializeObject<List<usp_getAllEmailActivity_Result>>(result.ToString());
                    return this.Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(Constants.ErrorTitle);
                }
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }

        }

        //String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Campaign/GetCampaignByUserId");
        //string userid = CurrentUser.getCurUserId();
        //string sStartDate = startdate != null ? startdate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
        //string sEndDate = endDate != null ? endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
        //string queryParams = string.Format(@"
        //                    'aggregated_by': 'day', 
        //                    'start_date': '{0}',
        //                    'end_date' : '{1}',
        //                    'categories': '{2}'
        //                    ", sStartDate, sEndDate, userid);
        //var response = _sendGrid.getStatsByCategory(queryParams);
        //if (response != null)
        //{
        //    return this.Json(response, JsonRequestBehavior.AllowGet);
        //}
        //else
        //{
        //    return Json(Constants.ErrorTitle);
        //}
        //}
        //    else
        //    {
        //        return RedirectToAction("LogOff", "Account", new { area = "" });
        //    }

        public JsonResult getCampignStats(string campaignIds, string startDate)
        {
            string queryParams = string.Format(@"
                                'aggregated_by': 'day', 
                                'start_date': '{0}',
                                'categories': [{1}]
                                ", startDate, campaignIds);
            var response = _sendGrid.getStatsByCategory(queryParams);
            if (response != null)
            {
                return this.Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }
        }
        public JsonResult getCampignStatsByFilter(string campaignIds, DateTime? startDate, DateTime? endDate)
        {
            string sStartDate = startDate != null ? startDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
            string sEndDate = endDate != null ? endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
            string queryParams = string.Format(@"
                                'aggregated_by': 'day', 
                                'start_date': '{0}',
                                'end_date' : '{1}',
                                'categories': [{2}]
                                ", sStartDate, sEndDate, campaignIds);
            var response = _sendGrid.getStatsByCategory(queryParams);
            if (response != null)
            {
                return this.Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }
        }

        public ActionResult GetLatestCampaignByFilter(DateTime? startdate, DateTime? endDate)
        {
            string userid = CurrentUser.getCurUserId();
            List<usp_getUserDashboardData_Result> model = new List<usp_getUserDashboardData_Result>();
            string APIURL = string.Format("{0}/{1}?id={2}&dateFrom={3}&dateTo={4}", Constants.ApiURL, "UserDashboard/GetLatestCampaignByFilter", CurrentUser.getCurUserId(), startdate, endDate);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getUserDashboardData_Result>>(result.ToString());
                model = data;
                return PartialView("LatestCampaign", model);
                //return this.Json(data, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index");
                //return View("index", model);
            }
            else
            {
                return Json(Constants.ErrorTitle);
            }

            //}   
        }

        //[HttpPost]
        //public ActionResult UploadPic()
        //{
        //    // Checking no of files injected in Request object  
        //    string picpath = "";
        //    if (Request.Files.Count > 0)
        //    {
        //        try
        //        {
        //            //  Get all files from Request object  
        //            HttpFileCollectionBase files = Request.Files;
        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                HttpPostedFileBase file = files[i];
        //                picpath = file.Save("");

        //            }
        //            // Returns message that successfully uploaded  
        //            //return Json(fname);
        //            return Json(new
        //            {
        //                path = picpath,
        //            });
        //        }
        //        catch (Exception)
        //        {
        //            return Json("Error");
        //        }
        //    }
        //    else
        //    {
        //        return Json("No file selected");
        //    }
        //}

        public ActionResult Upgrade()
        {
            PacakagesViewModel model = new PacakagesViewModel();
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "EmailPlans");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.packagesList = JsonConvert.DeserializeObject<List<usp_geAllPlans_Result>>(result.ToString());
            }
            APIURL = string.Format("{0}/{1}", Constants.ApiURL, "EmailPlans/GetPlan");
            result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.planList = JsonConvert.DeserializeObject<List<Plan>>(result.ToString());
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult UpgradePlan(int packageId)
        {
            UserInfoViewModel curUser = CurrentUser.getCurUserDetails();
            String APIURL = "", token = null;
            if (curUser != null)
            {
                if (curUser.PaymentProfileId != null)
                {
                    APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", packageId);
                    var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                    if (apiResult != null)
                    {
                        var planDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(apiResult.ToString());

                        using (Payment pp = new Payment())
                        {
                            decimal amount = curUser.Country == "Canada" ? planDetail.BillingAmount_Canada != null ? (decimal)planDetail.BillingAmount_Canada.Value : 0 : planDetail.BillingAmount != null ? (decimal)planDetail.BillingAmount.Value : 0;
                            PaymentInfo profile = new PaymentInfo();
                            //var path = DataHelper.ApplicationUrl();
                            profile.NavigateUrl = DataHelper.ApplicationUrl() + "/User/Dashboard/ConfirmPayment";
                            profile.amount = amount.ToString();
                            profile.orderInfo = new OrderInfo();
                            profile.orderInfo.OrderId = planDetail.PlanId.ToString();
                            profile.orderInfo.OrderDesc = planDetail.PlanName;
                            profile.orderInfo.TaxAmount = 0.00;
                            profile.orderInfo.Amount = Convert.ToDouble(amount);
                            profile.orderInfo.CustomField = curUser.Id;
                            profile.orderInfo.CustomField2 = packageId.ToString();
                            profile.CustomerVaultId = curUser.PaymentProfileId;
                            profile.orderInfo.CustomField3 = curUser.CardType;
                            token = pp.Transaction(profile, Constants.PaymentProductionMode, false, "Upgrade Plan");

                        }
                        if (token != null && DataHelper.GetTransactionStatus(token) != null && DataHelper.GetTransactionStatus(token) == "100")
                        {
                            return this.Json(new
                            {
                                status = Constants.Success,
                                msg = "Current plan has been upgraded successfully.",
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return this.Json(new
                            {
                                status = Constants.Error,
                                msg = "Payment Declined!",
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return this.Json(new
                        {
                            status = Constants.Error,
                            msg = "Selected package not found",
                        }, JsonRequestBehavior.AllowGet);

                    }
                }

                else
                {
                    return this.Json(new
                    {
                        status = Constants.Error,
                        msg = "Please add payment profile",
                    }, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                return this.Json(new
                {
                    status = Constants.Error,
                    msg = "Selected package not found",
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmPayment()
        {
            int MaxSubscriber = 0;
            ViewBag.TokenId = Request.QueryString["token-id"];
            PaymentStatus paymentStatus = new PaymentStatus();
            InvoiceViewModel model = new InvoiceViewModel();
            paymentStatus = Payment.getPaymentstatus(ViewBag.TokenId, Constants.PaymentProductionMode);
            if (paymentStatus != null)
            {
                paymentStatus.TransCreatedBy = "Upgrade Plan";
                DataHelper.updateTransactionDetails(paymentStatus, ViewBag.TokenId);
            }
            if (paymentStatus.result == "1")
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    var planUpgradeDetails = DataHelper.CreateUserPlan(Convert.ToInt32(paymentStatus.CustomField2), paymentStatus.CustomField);
                    MaxSubscriber = Convert.ToInt32(planUpgradeDetails.NoOfSubscribersMax);
                }
                var invoiceDetails = DataHelper.SaveInvoiceDetails(paymentStatus);
               // SendGridEmails sendGrid = new SendGridEmails();
                string _creditCardNo = "XXXX-XXXX-XXXX-" + paymentStatus.CCNumber.Substring(paymentStatus.CCNumber.Length - 4);
                //await _mailGun.sendSingleEmailInvoice(paymentStatus.CustomField, "Upgrade Plan " + paymentStatus.OrderDesc + " Invoice",
                //    EmailHtml.getInvoiceSend(paymentStatus.FirstName + " " + paymentStatus.LastName, DateTime.Now.ToString("MM/dd/yyyy"),
                //    "Thank you for upgrading your Plan through CheapestEmail. The following Credit Card has been charged in the amount of",
                //    paymentStatus.amount, paymentStatus.FirstName + " " + paymentStatus.LastName, paymentStatus.Address1, paymentStatus.City,
                //    paymentStatus.State, paymentStatus.Zip, invoiceDetails.CardType, _creditCardNo, paymentStatus.Phone, paymentStatus.transactionid, "Upgraded Plan", paymentStatus.OrderDesc, MaxSubscriber + " Subscribers"),
                //    Constants.BillingEmail, Constants.BillingName, paymentStatus.Email, paymentStatus.FirstName + "" + paymentStatus.LastName);

                await oMail.SendMail(paymentStatus.Email, "Upgrade Plan " + paymentStatus.OrderDesc + " Invoice", EmailHtml.getInvoiceSend(paymentStatus.FirstName + " " + paymentStatus.LastName, DateTime.Now.ToString("MM/dd/yyyy"),
                    "Thank you for upgrading your Plan through CheapestEmail. The following Credit Card has been charged in the amount of",
                    paymentStatus.amount, paymentStatus.FirstName + " " + paymentStatus.LastName, paymentStatus.Address1, paymentStatus.City,
                    paymentStatus.State, paymentStatus.Zip, invoiceDetails.CardType, _creditCardNo, paymentStatus.Phone, paymentStatus.transactionid, "Upgraded Plan", paymentStatus.OrderDesc, MaxSubscriber + " Subscribers"), Constants.BillingName, "mailSettings/Billing", (paymentStatus.FirstName + "" + paymentStatus.LastName), System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);


                UserActivityLog userActivtyObj = new UserActivityLog();
                userActivtyObj.UserId = paymentStatus.CustomField;
                userActivtyObj.Createdby = User.Identity.GetUserId();
                userActivtyObj.Description = User.Identity.GetUserName() + " has upgraded plan into " + paymentStatus.OrderDesc + " for  " + paymentStatus.FirstName + " " + paymentStatus.LastName;
                userActivtyObj.DateTime = DateTime.Now;
                var res = DataHelper.InsertUserActivity(userActivtyObj);
            }
            else
            {

                var oTrans = DataHelper.GetTransactionInfo(ViewBag.TokenId);
               // SendGridEmails sendGrid = new SendGridEmails();
                string _creditCardNo = "XXXX-XXXX-XXXX-" + oTrans.CreditCardNo.Substring(oTrans.CreditCardNo.Length - 4);
                //await _mailGun.sendSingleEmailInvoice(oTrans.ContactEmail,
                //    oTrans.Message + " Invoice", EmailHtml.getInvoiceSend(oTrans.FirstName + " " + oTrans.LastName,
                //    DateTime.Now.ToString("MM/dd/yyyy"), "The following Credit Card has not been charged for upgrade plan -" + oTrans.Message + " of amount ", oTrans.Amount, oTrans.FirstName + " " + oTrans.LastName,
                //    oTrans.Address1, oTrans.City, oTrans.State, oTrans.Zip, oTrans.CardType, _creditCardNo,
                //    oTrans.MobilePhone, oTrans.AnTransactionId, oTrans.Message, "", ""), Constants.SupportEmail, Constants.SupportName, oTrans.ContactEmail,
                //    oTrans.FirstName + "" + oTrans.LastName);

                await oMail.SendMail(oTrans.ContactEmail, oTrans.Message + " Invoice", EmailHtml.getInvoiceSend(oTrans.FirstName + " " + oTrans.LastName,
  DateTime.Now.ToString("MM/dd/yyyy"), "The following Credit Card has not been charged for " + oTrans.Message + " of amount ", oTrans.Amount, oTrans.FirstName + " " + oTrans.LastName,
  oTrans.Address1, oTrans.City, oTrans.State, oTrans.Zip, oTrans.CardType, _creditCardNo,
  oTrans.MobilePhone, oTrans.AnTransactionId, oTrans.Message, "", ""), Constants.BillingName, "mailSettings/Billing", (oTrans.FirstName + "" + oTrans.LastName), System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);




            }
            return View();
        }

        public ActionResult SubscriberDetails(int? PageNumber)
        {
            ViewBag.PageSize = Constants.PageSize;
            if (PageNumber == null || PageNumber == 0)
            {
                PageNumber = 1;
            }
            ViewBag.PageNumber = PageNumber;
            List<UserInfoViewModel> model = new List<UserInfoViewModel>();
            String APIURL = string.Format("{0}/{1}?id={2}&PageNumber={3}&PageSize={4}", Constants.ApiURL, "UserDashboard", CurrentUser.getCurUserId(), PageNumber, Constants.PageSize);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<UserInfoViewModel>>(result.ToString());
                model = data;
                ViewBag.TotalRecord = model.Count > 0 ? model.Select(x => x.TotalRecord).FirstOrDefault() : 0;

            }
            return PartialView("SubscriberDetail", model);
        }

        public ActionResult OpenClickDetails(String Text, int? Pagenumber)
        {
            List<CampaignSummaryReportViewModel> model = new List<CampaignSummaryReportViewModel>();
            if (Pagenumber == null || Pagenumber == 0)
            {
                Pagenumber = 1;
            }
            switch (Text)
            {
                case "Click":
                    model = CampDetails(Pagenumber);
                    ViewBag.Text = Text;
                    break;
                case "Open":
                    model = CampDetails(Pagenumber);
                    ViewBag.Text = Text;
                    break;

            }
            ViewBag.PageNumber = Pagenumber;
            ViewBag.TotalRecord = model.Count > 0 ? model.Select(x => x.TotalRecords).FirstOrDefault() : 0;
            return PartialView("OpenClickDetails", model);
        }

        public List<CampaignSummaryReportViewModel> CampDetails(int? Pagenumber)
        {
            string APIURLUser = string.Format("{0}/{1}?id={2}&PageNumber={3}&PageSize={4}", Constants.ApiURL, "UserDashboard/GetDashboardReport", CurrentUser.getCurUserId(), Pagenumber, Constants.CampaignPageSize);
            var count = (new APICallHelper()).Get(APIURLUser).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var camapaigns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getDashboardReport_Result>>(count.ToString());
                var response = (from camps in camapaigns
                                select new CampaignSummaryReportViewModel
                                {
                                    CampaignId = camps.CampaignId.Value,
                                    CampaignName = camps.CamapignName,
                                    Opens = camps.Opens.Value,
                                    Clicks = camps.Clicks.Value,
                                    TotalRecords = camps.TotalRecord.Value,
                                }).ToList();
                return response;
            }
            else
            {
                return null;
            }
        }

        public ActionResult UnSubscriberDetails(int? PageNumber)
        {
            ViewBag.PageSize = Constants.PageSize;
            if (PageNumber == null || PageNumber == 0)
            {
                PageNumber = 1;
            }
            ViewBag.PageNumber = PageNumber;
            List<UserInfoViewModel> model = new List<UserInfoViewModel>();
            String APIURL = string.Format("{0}/{1}?id={2}&PageNumber={3}&PageSize={4}", Constants.ApiURL, "UserDashboard/GetUnSubscriber", CurrentUser.getCurUserId(), PageNumber, Constants.CampaignPageSize);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<UserInfoViewModel>>(result.ToString());
                model = data;
                ViewBag.TotalRecord = model.Count > 0 ? model.Select(x => x.TotalRecord).FirstOrDefault() : 0;

            }
            return PartialView("UnSubscriberDetail", model);
        }


    }
}