﻿using ClosedXML.Excel;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Extentions;
using EM.Web.Utilities;
using Microsoft.AspNet.Identity;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EM.Web.Areas.User.Controllers
{
    [Authorize(Roles = "Admin,SalesRep,User")]
    public class ListController : Controller
    {
        // GET: User/List
        public ActionResult Index(string page = "")
        {
            ViewBag.Page = page;
            return View();
        }
        public ActionResult UserList(int? Page , string listName)
        {
            ViewBag.PageSize = Constants.PageSize;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            ViewBag.PageNumber = Page;
            List<usp_getUserList_Result> subscriberListViewModel = new List<usp_getUserList_Result>();
            try
            {
                string APIURL = string.Format("{0}/{1}?id={2}&pageNumber={3}&listname={4}&pageSize={5}&orderBy=", Constants.ApiURL, "UserList", CurrentUser.getCurUserId(), Page, listName, Constants.PageSize);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    var data = JsonConvert.DeserializeObject<List<usp_getUserList_Result>>(result.ToString());
                    subscriberListViewModel = data;
                }
                ViewBag.TotalRecord = subscriberListViewModel.Count > 0 ? subscriberListViewModel.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                return PartialView("UserList", subscriberListViewModel);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getUserListGridView(int? Page, string order , string listName)
        {

            List<usp_getUserList_Result> subscriberListViewModel = new List<usp_getUserList_Result>();
            string APIURL = string.Format("{0}/{1}?id={2}&listName={3}&pageNumber={4}&pageSize={5}&orderBy={6}", Constants.ApiURL, "UserList", CurrentUser.getCurUserId(), listName, Page, Constants.PageSize,order);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getUserList_Result>>(result.ToString());
                subscriberListViewModel = data;
                ViewBag.PageNumber = Page;
                ViewBag.TotalRecord = subscriberListViewModel.Count > 0 ? subscriberListViewModel.Select(x => x.TotalRecord).FirstOrDefault() : 0;
            }
            return PartialView("UserListGrid", subscriberListViewModel);
        }
        public ActionResult AddUserList(int? id)
        {
            ListViewModel lst = new ListViewModel();

            if (id == null)
            {

                var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                if (getCurrentUserDetails != null)
                {
                    lst.FromName = getCurrentUserDetails.FirstName;
                    lst.FromEmail = getCurrentUserDetails.Email;
                    lst.Company = getCurrentUserDetails.CompanyName;
                    lst.Address1 = getCurrentUserDetails.Address1;
                    lst.City = getCurrentUserDetails.City;
                    lst.Zip = getCurrentUserDetails.Zip;
                    lst.Country = getCurrentUserDetails.Country;
                    lst.State = getCurrentUserDetails.State;
                    string selectedCountry = getCurrentUserDetails.Country == null ? Constants.DefaultState : getCurrentUserDetails.Country;
                    //string selectedState = getCurrentUserDetails.State == null ? "California" : getCurrentUserDetails.State;
                    TempData["Country"] = DataHelper.GetCountryList(selectedCountry);
                    TempData.Keep("Country");
                    TempData["State"] = DataHelper.GetStateList(null);
                    TempData.Keep("State");
                    lst.Phone = getCurrentUserDetails.MobilePhone;
                }
                return PartialView("AddUserList", lst);

            }
            else
            {

                String APIURL = string.Format("{0}/{1}?id={2}", Constants.ApiURL, "UserList", id);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    var data = JsonConvert.DeserializeObject<ListViewModel>(result.ToString());
                    lst = data;
                }

                TempData["Country"] = DataHelper.GetCountryList(lst.Country);
                TempData["State"] = DataHelper.GetStateList(lst.State);
                return PartialView("AddUserList", lst);

            }

        }


        [HttpPost]
        public async Task<ActionResult> AddUserList(ListViewModel listViewModel)
        {
            if (ModelState.IsValid)
            {
                var CheckDuplicateList = DataHelper.CheckDuplicateList(listViewModel.ListName, listViewModel.ListId);
                if(CheckDuplicateList == true)
                {
                    return this.Json(new
                    {
                        status = Constants.ErrorTitle,
                        msg = "List name already exists! Please try another."
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    try
                    {
                        //var UserId = CurrentUser.getCurUserId();
                        var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                        listViewModel.CreatedById = getCurrentUserDetails.Id;
                        listViewModel.LastModById = getCurrentUserDetails.Id;
                        var domain = listViewModel.FromEmail.Split('@')[1];
                        if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                        {
                            listViewModel.ListNameDomainWise = listViewModel.ListName + "@" + Constants.MailingListDomain;
                        }
                        else
                        {
                            listViewModel.ListNameDomainWise = listViewModel.ListName + "@" + domain.Split('.')[0] + Constants.RootDomain;
                        }

                        CurrentUser.SetAuthoriztionInfo();
                        var userdetails = System.Web.HttpContext.Current.Session["UserAuthorizationInfo"] as usp_getAuthorization_Result;
                        if (listViewModel.ListId == 0)
                        {
                            var susbcriberCount=DataHelper.GetsubscriberCountByListId(listViewModel.ListName, listViewModel.CreatedById);
                            if (susbcriberCount <= userdetails.MaxSubscriber)
                            {
                                //create mailList Using Mailgun Api 
                               // var createmailinglist = await MailgunHelper.CreateMailingList(listViewModel.ListNameDomainWise);
                                String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserList");
                                var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listViewModel)).StatusCode;
                                if (result == System.Net.HttpStatusCode.OK)
                                {
                                    //------------User Activity Log-------------//

                                    UserActivityLog userActivtyObj = new UserActivityLog();
                                    userActivtyObj.UserId = getCurrentUserDetails.Id;
                                    userActivtyObj.Createdby = listViewModel.CreatedById;
                                    userActivtyObj.Description = listViewModel.ListName + " List Added For " + getCurrentUserDetails.FirstName + " " + getCurrentUserDetails.LastName + " by " + User.Identity.GetUserName();
                                    userActivtyObj.DateTime = DateTime.Now;
                                    var res = DataHelper.InsertUserActivity(userActivtyObj);
                                    //------------------------------------------//
                                    return this.Json(new
                                    {
                                        status = Constants.SuccessTitle,
                                        msg = "List successfully added.",
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return this.Json(new
                                {
                                    status = Constants.ErrorTitle,
                                    msg = "List already exists! But Your Max Subscriber Limit Does Not Allow you to Restore this list.",
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {

                            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserList");
                            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listViewModel)).StatusCode;
                            if (result == System.Net.HttpStatusCode.OK)
                            {
                                return this.Json(new
                                {
                                    status = Constants.SuccessTitle,
                                    msg = "List successfully updated.",
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                        return this.Json(new
                        {
                            status = Constants.ErrorTitle,
                            msg = ex.Message
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

            }


            return this.Json(new
            {
                status = Constants.ErrorTitle,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult AddSubscriber()
        {
            try
            {
                return PartialView("AddSubscriber");
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteList(string id)
        {

            try
            {
                var APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "UserList", id);
                var result = (new APICallHelper()).Delete(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    CurrentUser.SetAuthoriztionInfo();
                    //------------User Delete List Activity Log-------------//
                    var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                    UserActivityLog userActivtyObj = new UserActivityLog();
                    userActivtyObj.UserId = getCurrentUserDetails.Id;
                    userActivtyObj.Createdby = User.Identity.GetUserId();
                    userActivtyObj.Description ="User List for  " + getCurrentUserDetails.FirstName + " " + getCurrentUserDetails.LastName + ", List id is " + id.ToString() + " Deleted by "+User.Identity.GetUserName() +" ";
                    userActivtyObj.DateTime = DateTime.Now;
                    var res = DataHelper.InsertUserActivity(userActivtyObj);
                    //------------------------------------------//

                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "List successfully deleted.",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult RestoreList(string id)
        {

            try
            {
                var APIURL = string.Format("{0}/{1}/{2}?id={3}", Constants.ApiURL, "UserList","RestoreDelete",id);
                var result = (new APICallHelper()).Get(APIURL).StatusCode;
                if (result == System.Net.HttpStatusCode.OK)
                {
                    CurrentUser.SetAuthoriztionInfo();
                    //------------User Delete List Activity Log-------------//
                    var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                    UserActivityLog userActivtyObj = new UserActivityLog();
                    userActivtyObj.UserId = getCurrentUserDetails.Id;
                    userActivtyObj.Createdby = User.Identity.GetUserId();
                    userActivtyObj.Description = "User List for  " + getCurrentUserDetails.FirstName + " " + getCurrentUserDetails.LastName + ", List id is " + id.ToString() + " Recover by " + User.Identity.GetUserName() + " ";
                    userActivtyObj.DateTime = DateTime.Now;
                    var res = DataHelper.InsertUserActivity(userActivtyObj);
                    //------------------------------------------//

                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = "List has been Recover successfully!",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.Error,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ImportList()
        {
            return PartialView("ImportList");
        }

        [HttpPost]
        [ActionName("ImportList")]
        public ActionResult ImportListPost()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    var listFile = Request.Files[0];
                    var ExcelFilePath = listFile.SaveExcelFile();
                    var ServerPath = Server.MapPath("~");
                    var FilePath = ServerPath + ExcelFilePath;
                    return this.Json(new
                    {
                        status = Constants.Success,
                        msg = Constants.ErrorMessage,
                        URL = FilePath
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return this.Json(new
                    {
                        status = Constants.Error,
                        msg = ex.Message,
                        URL = ""
                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return this.Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage,
                URL = ""
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SaveImportSheetList(string param1)
        {
            ViewBag.ExcelFilePath = param1;
            var userId = CurrentUser.getCurUserId();
            var listData = DataHelper.GetListFromExcel(param1, userId);
            return PartialView("SaveImportSheetList", listData);
        }

        [HttpPost]
        [ActionName("SaveImportSheetList")]
        public ActionResult SaveImportSheetListPost(string param1)
        {
            ListImportResponseViewModel listImportResponseViewModel = new ListImportResponseViewModel();
            List<ListViewModel> listViewModels = new List<ListViewModel>();
            List<ListViewModel> inValidListViewModels = new List<ListViewModel>();
            var userId = CurrentUser.getCurUserId();
            var listData = DataHelper.GetListFromExcel(param1, userId);
            foreach (var item in listData)
            {
                var isValid = DataHelper.ValidateList(item);
                if (isValid)
                {
                    listViewModels.Add(item);
                }
                else
                {
                    inValidListViewModels.Add(item);
                }
            }
            listImportResponseViewModel.InvalidList = inValidListViewModels;
            if (listViewModels.Count > 0)
            {
                string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "UserList/ListPost");
                try
                {
                    var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(listViewModels));
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var duplicateSubscribers = JsonConvert.DeserializeObject<List<ListViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                        listViewModels.RemoveAll(x => duplicateSubscribers.Any(y => y.FromEmail == x.FromEmail));
                        listImportResponseViewModel.SuccessList = listViewModels;
                        listImportResponseViewModel.DuplicateList = duplicateSubscribers;
                        return this.Json(new
                        {
                            status = Constants.Success,
                            msg = "Import Successful",
                            Content = listImportResponseViewModel
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {

                    return this.Json(new
                    {
                        status = Constants.Error,
                        msg = ex.Message,
                        Content = listImportResponseViewModel
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                status = Constants.Error,
                msg = Constants.ErrorMessage,
                Content = listImportResponseViewModel,
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportList()
        {
            string APIURL = string.Format("{0}/{1}?id={2}&listName=&pageNumber={3}&pageSize=&orderby=", Constants.ApiURL, "UserList", CurrentUser.getCurUserId(), 1);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;

            var data = JsonConvert.DeserializeObject<List<usp_getUserList_Result>>(result.ToString());
            //------------User Export List Activity Log-------------//
            var getCurrentUserDetails = CurrentUser.getCurUserDetails();
            UserActivityLog userActivtyObj = new UserActivityLog();
            userActivtyObj.UserId = getCurrentUserDetails.Id;
            userActivtyObj.Createdby = User.Identity.GetUserId();
            userActivtyObj.Description = getCurrentUserDetails.FirstName + " " + getCurrentUserDetails.LastName + " User List Exported By "+ User.Identity.GetUserName();
            userActivtyObj.DateTime = DateTime.Now;
            var res = DataHelper.InsertUserActivity(userActivtyObj);
            //------------------------------------------//
            DataTable dt = data.ToDataTable();
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "List");
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "List.xlsx");
                }
            }
        }
      

        public ActionResult UserRestoreList(int? Page, string listName)
        {
            ViewBag.PageSize = Constants.PageSize;
            if (Page == null || Page == 0)
            {
                Page = 1;
            }
            ViewBag.PageNumber = Page;
            List<usp_getDeletedUserList_Result> subscriberListViewModel = new List<usp_getDeletedUserList_Result>();
            try
            {
                string APIURL = string.Format("{0}/{1}?id={2}&pageNumber={3}&listname={4}&pageSize={5}&orderBy=", Constants.ApiURL, "UserList/DeletedList", CurrentUser.getCurUserId(), Page, listName, Constants.PageSize);
                var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                if (result != null)
                {
                    var data = JsonConvert.DeserializeObject<List<usp_getDeletedUserList_Result>>(result.ToString());
                    subscriberListViewModel = data;
                }
                ViewBag.TotalRecord = subscriberListViewModel.Count > 0 ? subscriberListViewModel.Select(x => x.TotalRecord).FirstOrDefault() : 0;
                return PartialView("UserRestoreList", subscriberListViewModel);
            }
            catch (Exception ex)
            {

                return this.Json(new
                {
                    status = Constants.ErrorTitle,
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult getDeltedUserListGridView(int? Page, string order, string listName)
        {

            List<usp_getDeletedUserList_Result> subscriberListViewModel = new List<usp_getDeletedUserList_Result>();
           // string APIURL = string.Format("{0}/{1}?id={2}&listName={3}&pageNumber={4}&pageSize={5}&orderBy={6}", Constants.ApiURL, "UserList", CurrentUser.getCurUserId(), listName, Page, Constants.PageSize, order);
            string APIURL = string.Format("{0}/{1}?id={2}&pageNumber={3}&listname={4}&pageSize={5}&orderBy={6}", Constants.ApiURL, "UserList/DeletedList", CurrentUser.getCurUserId(), Page, listName, Constants.PageSize,order);

            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var data = JsonConvert.DeserializeObject<List<usp_getDeletedUserList_Result>>(result.ToString());
                subscriberListViewModel = data;
                ViewBag.PageNumber = Page;
                ViewBag.TotalRecord = subscriberListViewModel.Count > 0 ? subscriberListViewModel.Select(x => x.TotalRecord).FirstOrDefault() : 0;
            }
            return PartialView("DeletedUserListGrid", subscriberListViewModel);
        }
    }
}
