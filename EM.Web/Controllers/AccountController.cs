﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using EM.Web.Models;
using EM.Factory;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Configuration;
using EM.Helpers;
using EM.Factory.ViewModels;
using System.Collections.Generic;
using Newtonsoft.Json;
using EM.Web.Attributes;
using EM.Web.Utilities;
using NMIPayment;
using System.Net.Http;
using System.Text;
using System.Net;
using RestSharp;
using RestSharp.Authenticators;

namespace EM.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        SendGridAPIHelper _sendGrid = new SendGridAPIHelper();
        MailgunHelper _mailgun = new MailgunHelper();
        Mail oMail = new Mail();


        public AccountController()
        {


        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {


            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("GetDataAndRedirectToDashboard");
            }
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var db = new EmailMarketingDbEntities())
            {
                var um = UserManager.FindByEmail(model.Email);
                if (um != null)
                {
                    var loginProfile = db.UserProfiles.Where(x => x.Id == um.Id).FirstOrDefault();
                    var salesProfile = db.SalesRepProfiles.Where(x => x.Id == um.Id).FirstOrDefault();
                    var userPlanDetails = db.UserPlans.Where(x => x.UserId == um.Id && x.IsActive == true).FirstOrDefault();
                    if ((loginProfile != null && Convert.ToBoolean(loginProfile.IsActive) == false) ||
                        (salesProfile != null && Convert.ToBoolean(salesProfile.IsActive) == false))
                    {
                        ViewBag.Message = "Your account is currently deactivated. Please contact to Admin";
                        return View(model);
                    }
                    if (userPlanDetails != null && (userPlanDetails.NextPackageUpdateDate < DateTime.Now.Date))
                    {
                        ViewBag.Message = "Your payment is due. Please contact to Admin";
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Message = "User not found!";
                    return View(model);
                }
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        return RedirectToAction("GetDataAndRedirectToDashboard");
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                    ViewBag.Message = "Login details are incorrect";
                    return View();
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        public async Task<ActionResult> GetDataAndRedirectToDashboard()
        {

            if (User.IsInRole(UserRole.Admin.ToString()))
            {
                CurrentUser.createCurUserSession(UserRole.Admin.ToString());
                return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                //return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
            }
            else if (User.IsInRole(UserRole.User.ToString()))
            {
                CurrentUser.createCurUserSession(UserRole.User.ToString());

                UserActivityLog userActivtyObj = new UserActivityLog();
                userActivtyObj.UserId = User.Identity.GetUserId();
                userActivtyObj.Createdby = User.Identity.GetUserId();
                userActivtyObj.Description = "Successfully Login by " + CurrentUser.getCurUserDetails().FirstName + " " + CurrentUser.getCurUserDetails().LastName + "";
                userActivtyObj.DateTime = DateTime.Now;

                var res = DataHelper.InsertUserActivity(userActivtyObj);
                using (var db = new EmailMarketingDbEntities())
                {

                    var userProfile = db.UserProfiles.AsEnumerable().Where(x => x.Id == User.Identity.GetUserId()).FirstOrDefault();
                    userProfile.LastLogin = DateTime.UtcNow;
                    db.SaveChanges();
                    var domain = userProfile.SendingFromEmail.Split('@')[1];
                    if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) == -1)
                    {
                        /*To check whether domain should active or not by calling verifyDomainSetting*/
                        var response = await MailgunHelper.verifyDomainSettings(domain);
                    }
                    if (userProfile.FirstName == null || userProfile.LastName == null || userProfile.ContactEmail == null || userProfile.MobilePhone == null || userProfile.City == null || userProfile.State == null || userProfile.Zip == null || userProfile.Country == null)
                    {
                        return RedirectToAction("UserProfile", "Dashboard", new { area = "User" });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Dashboard", new { area = "User" });

                    }
                }
            }
            else if (User.IsInRole(UserRole.SalesRep.ToString()))
            {
                CurrentUser.createCurUserSession(UserRole.SalesRep.ToString());
                return RedirectToAction("Index", "Dashboard", new { area = "SalesRep" });
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(int? Package, string loc, string f, string l, string e)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            if (Package == null || Package < 1)
            {
                return RedirectToAction("PricingPlan", "Home");
            }
            RegisterViewModel model = new RegisterViewModel();
            model.FirstName = f;
            model.LastName = l;
            model.Email = e;
            String APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", Package);
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                var planDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(result.ToString());
                model.MaxSubscribers = planDetail.NoOfSubscribersMax;
                if (loc != null)
                {
                    if (loc.ToLower() == "us")
                    {
                        model.PackageBillingAmount = planDetail.BillingAmount;
                    }
                    else
                    {
                        model.PackageBillingAmount = planDetail.BillingAmount_Canada;
                    }
                }
                else
                {
                    model.PackageBillingAmount = 0;
                }
                model.SelectedPackakgeId = Package;
                model.TotalEmails = planDetail.NoOfCredits;
                model.UnlimtedEmail = planDetail.UnlimitedEmails;
                model.MinSubscribers = 0;
                model.MaxSubscribers = planDetail.NoOfSubscribersMax;
                model.PlanName = planDetail.PlanName;
                model.location = loc;
            }
            ViewBag.loc = loc;
            var selectedCountry = loc == "CA" ? "Canada" : Constants.DefaultState;

            model.CountryList = DataHelper.GetCountryList(selectedCountry);
            model.StateList = DataHelper.GetStateList(null);
            model.YearList = DataHelper.GetCreditCardYear(Convert.ToInt32(model.ExpYear));
            model.MonthList = DataHelper.GetMonths(Convert.ToInt32(model.ExpMonth));
            model.TimeZoneList = DataHelper.GetTimeZone(model.TimeZoneID.ToString());
            return View(model);
        }


        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                if (!MailgunHelper.isValidEmail(model.Email))
                {
                    ViewBag.Message = "Invalid Email Address!";

                }
                else
                {
                    if (UserManager.FindByEmail(model.Email) == null)
                    {
                        if (model.SelectedPackakgeId != null && model.SelectedPackakgeId != 1)
                        {
                            var ExpYear = model.ExpYear.ToString().Length == 2 ? model.ExpYear.ToString() : model.ExpYear.ToString().Substring(2, 2);
                            PaymentStatus response = Payment.DirectPost(new NMIPayment.NMIdirectPostModel()
                            {
                                firstname = model.FirstName,
                                lastname = model.LastName,
                                city = model.City,
                                state = model.State,
                                zip = model.Zip,
                                amount = model.PackageBillingAmount.Value,
                                ccnumber = model.CreditCardNo,
                                ccexp = Convert.ToString(model.ExpMonth + ExpYear).Length == 3 ? Convert.ToString("0" + model.ExpMonth + ExpYear) : Convert.ToString(model.ExpMonth + ExpYear),
                                cvv = model.cvv
                            },
                                Constants.PaymentProductionMode);
                            if (response != null && response.result == "1")
                            {
                                response.TransCreatedBy = "Register";
                                //create user
                                var userRes = await createUser(model);
                                if (userRes != null)
                                {
                                    response.CustomField = userRes;
                                    response.CCNumber = EM.Helpers.Utilities.MaskCreditCardNo(model.CreditCardNo);
                                    response.CCEXp = Convert.ToString(model.ExpMonth + ExpYear).Length == 3 ? Convert.ToString("0" + model.ExpMonth + ExpYear) : Convert.ToString(model.ExpMonth + ExpYear);
                                    response.FirstName = model.FirstName;
                                    response.LastName = model.LastName;
                                    response.Phone = model.MobilePhone;
                                    response.Address1 = model.Address1;
                                    response.City = model.City;
                                    response.State = model.State;
                                    response.Zip = model.Zip;
                                    response.OrderId = model.SelectedPackakgeId.ToString();
                                    response.amount = model.PackageBillingAmount.ToString();
                                    response.CustomField3 = EM.Helpers.Utilities.getCardType(model.CreditCardNo);
                                    response.customervaultid = userRes;
                                    response.OrderDesc = "Register";
                                    // create invoice
                                    var invoiceDetails = DataHelper.SaveInvoiceDetails(response);
                                    var result = DataHelper.SaveTransactionDetails(response, response.TransCode);

                                    // SendGridEmails sendGrid = new SendGridEmails();
                                    string _creditCardNo = "XXXX-XXXX-XXXX-" + model.CreditCardNo.Substring(model.CreditCardNo.Length - 4);
                                    //await _mailgun.sendSingleEmailInvoice(userRes, "Monthly Billing Invoice", EmailHtml.getInvoiceSend(model.FirstName + " " + model.LastName,
                                    //    DateTime.Now.ToString("MM/dd/yyyy"), "Thank you for signing up for the " + model.PlanName + " Plan. Your card has been charged in the amount of",
                                    //    model.PackageBillingAmount.Value.ToString(), (model.FirstName + "  " + model.LastName),
                                    //    model.Address1, model.City, model.State, model.Zip, invoiceDetails.CardType, _creditCardNo,
                                    //    invoiceDetails.Phone, response.transactionid, model.PlanName + " Plan", model.MaxSubscribers.ToString() + " Subscribers", ""),
                                    //    Constants.BillingEmail, Constants.BillingName, model.Email, model.FirstName + "" + model.LastName);

                                    await oMail.SendMail(model.Email, "Monthly Billing Invoice", EmailHtml.getInvoiceSend(model.FirstName + " " + model.LastName,
                                       DateTime.Now.ToString("MM/dd/yyyy"), "Thank you for signing up for the " + model.PlanName + " Plan. Your card has been charged in the amount of",
                                       model.PackageBillingAmount.Value.ToString(), (model.FirstName + "  " + model.LastName),
                                       model.Address1, model.City, model.State, model.Zip, invoiceDetails.CardType, _creditCardNo,
                                       invoiceDetails.Phone, response.transactionid, model.PlanName + " Plan", model.MaxSubscribers.ToString() + " Subscribers", ""), Constants.BillingName, "mailSettings/Billing", (model.FirstName + "" + model.LastName), System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);


                                    //create NMI customer vault
                                    using (Payment pp = new Payment())
                                    {
                                        PaymentInfo profile = new PaymentInfo();
                                        //var path = DataHelper.ApplicationUrl();
                                        profile.amount = "0.0";
                                        profile.NavigateUrl = DataHelper.ApplicationUrl() + "/Account/ConfirmCustomerAdded";
                                        profile.CCNumber = model.CreditCardNo;
                                        profile.CCEXp = Convert.ToString(model.ExpMonth + ExpYear).Length == 3 ? Convert.ToString("0" + model.ExpMonth + ExpYear) : Convert.ToString(model.ExpMonth + ExpYear);
                                        profile.CCV = model.cvv;
                                        profile.billingInfo = new BillingInfo();
                                        profile.billingInfo.FirstName = model.FirstName;
                                        profile.billingInfo.LastName = model.LastName;
                                        profile.billingInfo.Address1 = model.Address1;
                                        profile.billingInfo.City = model.City;
                                        profile.billingInfo.State = model.State;
                                        profile.billingInfo.Zip = model.Zip;
                                        profile.billingInfo.Country = model.Country;
                                        profile.billingInfo.Phone = model.MobilePhone;
                                        profile.billingInfo.Fax = model.MobilePhone;
                                        profile.billingInfo.Email = model.Email;
                                        profile.shippingInfo = new ShippingInfo();
                                        profile.shippingInfo.FirstName = model.FirstName;
                                        profile.shippingInfo.LastName = model.LastName;
                                        profile.shippingInfo.Address1 = model.Address1;
                                        profile.shippingInfo.Address2 = model.Address2;
                                        profile.shippingInfo.City = model.City;
                                        profile.shippingInfo.State = model.State;
                                        profile.shippingInfo.Zip = model.Zip;
                                        profile.shippingInfo.Country = model.Country;
                                        profile.shippingInfo.Phone = model.MobilePhone;
                                        profile.shippingInfo.Fax = model.MobilePhone;
                                        profile.shippingInfo.Email = model.Email;
                                        profile.CustomerVaultId = userRes;

                                        pp.CreateUpdateCustomer(profile, Constants.PaymentProductionMode, true);
                                    }
                                    ViewBag.Message = "<p>Thank you for signing up with CheapesteMail.</p><p>We sent you an email with a link to create your password.</p><p>If you do not see our email in your Inbox, please check your SPAM folder.</p><p>To ensure you receive future emails, please add <a href='mailto:support@CheapestEmail.com'>Support@CheapestEmail.com</a> and <a href='mailto:invoices@CheapestEmail.com'>Invoices@CheapestEmail.com</a> to your Safe Sender list.</p>";
                                }

                            }
                            else
                            {
                                //SendGridEmails sendGrid = new SendGridEmails();
                                string _creditCardNo = "XXXX-XXXX-XXXX-" + model.CreditCardNo.Substring(model.CreditCardNo.Length - 4);
                                string _cardType = EM.Helpers.Utilities.getCardType(model.CreditCardNo);
                                //await _mailgun.sendSingleEmailInvoice("", "Monthly Billing Invoice", EmailHtml.getInvoiceSend(model.FirstName + " " + model.LastName,
                                //    DateTime.Now.ToString("MM/dd/yyyy"), "Thank you for signing up for the " + model.PlanName + " Plan. The following Credit Card has not been charged in the amount of",
                                //    model.PackageBillingAmount.Value.ToString(), (model.FirstName + "  " + model.LastName),
                                //    model.Address1, model.City, model.State, model.Zip, _cardType, _creditCardNo,
                                //    model.MobilePhone, "Failed Transaction" + "Error: " + response.resulttext, model.PlanName + " Plan", model.MaxSubscribers.ToString() + " Subcsribers", ""),
                                //    Constants.BillingEmail, Constants.BillingName, model.Email, model.FirstName + "" + model.LastName);

                                await oMail.SendMail(model.Email, "Monthly Billing Invoice", EmailHtml.getInvoiceSend(model.FirstName + " " + model.LastName,
                                  DateTime.Now.ToString("MM/dd/yyyy"), "Thank you for signing up for the " + model.PlanName + " Plan. The following Credit Card has not been charged in the amount of",
                                  model.PackageBillingAmount.Value.ToString(), (model.FirstName + "  " + model.LastName),
                                  model.Address1, model.City, model.State, model.Zip, _cardType, _creditCardNo,
                                  model.MobilePhone, "Failed Transaction" + "Error: " + response.resulttext, model.PlanName + " Plan", model.MaxSubscribers.ToString() + " Subcsribers", ""), Constants.BillingName, "mailSettings/Billing", (model.FirstName + "" + model.LastName), System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);


                                ViewBag.Message = "Payment Declined - " + response.resulttext;

                            }
                        }
                        else
                        {
                            var userRes = await createUser(model);
                            if (userRes != null)
                            {
                                ViewBag.Message = "<p>Thank you for signing up with CheapesteMail.</p>" +
                                    "<p>We sent you an email with a link to create your password.</p>" +
                                    "<p>If you do not see our email in your Inbox, please check your SPAM folder.</p>" +
                                    "<p>To ensure you receive future emails, please add <a href='mailto:support@CheapestEmail.com'>Support@CheapestEmail.com</a> and <a href='mailto:invoices@CheapestEmail.com'>Invoices@CheapestEmail.com</a> to your Safe Sender list.</p>";
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Email Id already exist!";

                    }
                }
            }
            else
            {
                ViewBag.Message = "Error!";

            }

            model.CountryList = DataHelper.GetCountryList(model.location);
            model.StateList = DataHelper.GetStateList(null);
            model.YearList = DataHelper.GetCreditCardYear(Convert.ToInt32(model.ExpYear));
            model.MonthList = DataHelper.GetMonths(Convert.ToInt32(model.ExpMonth));
            model.TimeZoneList = DataHelper.GetTimeZone(model.TimeZoneID.ToString());
            return View(model);
        }

        async Task<string> createUser(RegisterViewModel model)
        {

            try
            {
                /*create user and insert user information in User Profile Table */
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "User");
                }
                string ouserId = user.Id;
                using (var db = new EmailMarketingDbEntities())
                {

                    UserProfile oUser = new UserProfile();
                    oUser.Id = ouserId;
                    oUser.FirstName = model.FirstName;
                    oUser.LastName = model.LastName;
                    oUser.ContactEmail = model.Email;
                    oUser.City = model.City;
                    oUser.State = model.State;
                    oUser.Country = model.Country;
                    oUser.Zip = model.Zip;
                    oUser.MobilePhone = model.MobilePhone;
                    oUser.RegisteredDate = DateTime.Now;
                    oUser.IsActive = true;
                    oUser.IsCSVActive = true;
                    oUser.TimeZoneId = Convert.ToInt32(model.TimeZoneID);
                    oUser.CompanyName = model.Company;
                    /*If user's email domain is in whitelabel then sendingfromemail will be same Email and if its different then it will add root domain to that email doamin*/
                    //var domain = model.Email.Split('@')[1];
                    //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                    //{
                    //    model.Company = await MailgunHelper.ReplaceSpecialCharWithBlank(model.Company);
                    //    oUser.SendingFromEmail = await MailgunHelper.ReturnDomainforWhitelabelDomain(model.FirstName.ToLower(), model.Company.ToLower());
                    //}
                    //else
                    //{

                    //    oUser.SendingFromEmail = await MailgunHelper.ReturnMailWithDomain(model.Email);
                    //}
                    //model.Company = await MailgunHelper.ReplaceSpecialCharWithBlank(model.Company);

                    //create user sending from emal as per mailgun domain
                    oUser.SendingFromEmail = await MailgunHelper.AddSubDomaininEmailWithLower(model.Email,model.Company);
                    oUser.EmailSending = false;
                    db.UserProfiles.Add(oUser);
                    db.SaveChanges();
                    DataHelper.CreateUserPlan(model.SelectedPackakgeId, ouserId);

                    //save credir card details in authroize.net
                    if (model.SelectedPackakgeId != 1)
                    {
                        savePaymentProfile(model, ouserId);
                    }
                    /* create a link of create password and send it to user's email*/
                    var callbackUrl = Url.Action("CreatePassword", "Account", new { userId = ouserId, email = model.Email, name = model.FirstName + " " + model.LastName, company = model.Company, id = "create" }, protocol: Request.Url.Scheme);
                    CreateLog.ActionLogRL(callbackUrl);
                    Mail oMail = new Mail();
                    await oMail.SendMail(model.Email, "Create Password", EmailHtml.getRegisterHtml(model.FirstName + " " + model.LastName, callbackUrl), Constants.SupportName, "mailSettings/Support", (model.FirstName + "" + model.LastName));
                   //await _mailgun.sendSingleEmail(ouserId, "Create Password", EmailHtml.getRegisterHtml(model.FirstName + " " + model.LastName, callbackUrl), Constants.SupportEmail, Constants.SupportName, model.Email, model.FirstName + " " + model.LastName);
                    return ouserId;
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message,ex);
                return null;
            }
        }

        string savePaymentProfile(RegisterViewModel model, string userid)
        {
            UserPaymentProfileViewModel paymentModel = new UserPaymentProfileViewModel();
            paymentModel.AnPaymentProfileId = userid;
            paymentModel.UserId = userid;
            paymentModel.FirstName = model.FirstName;
            paymentModel.LastName = model.LastName;
            paymentModel.Address1 = model.Address1;
            paymentModel.City = model.City;
            paymentModel.State = model.State;
            paymentModel.Country = model.Country;
            paymentModel.Zip = model.Zip;
            paymentModel.CreditCardNo = model.CreditCardNo;
            paymentModel.CVV = model.cvv;
            paymentModel.Email = model.Email;
            paymentModel.ExMonth = Convert.ToInt32(model.ExpMonth);
            paymentModel.ExYear = Convert.ToInt32(model.ExpYear);
            paymentModel.Phone = model.MobilePhone;
            paymentModel.IsPrimary = true;
            paymentModel.CreatedById = userid;
            string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Billing");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(paymentModel)).Content.ReadAsStringAsync().Result;
            return result;
        }

        [AllowAnonymous]
        public ActionResult ConfirmCustomerAdded()
        {
            ViewBag.TokenId = Request.QueryString["token-id"];
            PaymentStatus paymentStatus = Payment.getPaymentstatus(ViewBag.TokenId, Constants.PaymentProductionMode);
            return View();
        }


        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string email, string name)
        {

            if (userId == null)
            {
                return View("Error");
            }
            string confirmCode = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
            var result = await UserManager.ConfirmEmailAsync(userId, confirmCode);
            using (var db = new EmailMarketingDbEntities())
            {
                EmailConfirmation oEmail = db.EmailConfirmations.SingleOrDefault(r => r.UserId == userId && r.FromEmailAddress == email);
                if (oEmail == null)
                {
                    oEmail = new EmailConfirmation();
                    oEmail.UserId = userId;
                    oEmail.FromEmailAddress = email;
                    oEmail.Status = true;
                    db.EmailConfirmations.Add(oEmail);
                    db.SaveChanges();
                }
            }

            ViewBag.id = userId;
            string resetPassCode = await UserManager.GeneratePasswordResetTokenAsync(userId);
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            model.Code = resetPassCode;
            model.Email = email;
            model.userid = userId;
            model.name = name;
            model.isNewUser = true;
            ViewBag.create = "create";
            return View("ResetPassword", model);
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]

        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByNameAsync(model.Email);
                    if (user == null)/* || !(await UserManager.IsEmailConfirmedAsync(user.Id)))*/
                    {
                        // Don't reveal that the user does not exist or is not confirmed
                        //return View("ForgotPasswordConfirmation");
                        ViewBag.Message = "Email id does not exist.";
                        return View(model);

                    }
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                   // await UserManager.SendEmailAsync(user.Id, "Reset Password", EmailHtml.getResetPasswordHtml(model.Email, callbackUrl));
                    await oMail.SendMail(model.Email, "Reset Password", EmailHtml.getResetPasswordHtml(model.Email, callbackUrl), Constants.SupportName, "mailSettings/Support", model.Email);
                    CreateLog.ActionLogRL(callbackUrl);

                    ViewBag.Message = "Reset Password link has been sent you registered email id. Please check your inbox.";
                    ModelState.Clear();
                    return View(new ForgotPasswordViewModel());
                    //return RedirectToAction("Login", "Account");


                }

                // If we got this far, something failed, redisplay form
                return View(model);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return View(new ForgotPasswordViewModel());

            }
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string userId, string code)
        {
            ViewBag.id = userId;
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            model.isNewUser = false;
            return code == null ? View("Error") : View("ResetPassword", model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> CreatePassword(string userId, string email, string name, string company)
        {
            if (userId == null)
            {
                return View("Error");
            }
            /*It will confirm user email and store data in EmailConfirmations Table*/
            string confirmCode = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
            var result = await UserManager.ConfirmEmailAsync(userId, confirmCode);
            //var domain = email.Split('@')[1];
            //string emailDomain = "";
            //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
            //{
            //    company = await MailgunHelper.ReplaceSpecialCharWithBlank(company);
            //    emailDomain = await MailgunHelper.ReturnDomainforWhitelabelDomain(name.ToLower(), company.ToLower());
            //}
            //else
            //{
            //    emailDomain = await MailgunHelper.ReturnMailWithDomain(email);
            //}
            //company = await MailgunHelper.ReplaceSpecialCharWithBlank(company);
            using (var db = new EmailMarketingDbEntities())
            {
                EmailConfirmation oEmail = db.EmailConfirmations.SingleOrDefault(r => r.UserId == userId && r.FromEmailAddress == email);
                if (oEmail == null)
                {
                    oEmail = new EmailConfirmation();
                    oEmail.UserId = userId;
                    oEmail.FromEmailAddress = email;
                    oEmail.Status = true;
                    oEmail.SendingFromEmail = await MailgunHelper.AddSubDomaininEmailWithLower(email, company);
                    db.EmailConfirmations.Add(oEmail);
                    db.SaveChanges();
                }
            }
            ViewBag.id = userId;
            string resetPassCode = await UserManager.GeneratePasswordResetTokenAsync(userId);
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            model.Code = resetPassCode;
            model.Email = email;
            model.userid = userId;
            model.name = name;
            model.CompanyName = company;
            model.isNewUser = true;
            ViewBag.create = "create";
            //var response = await verfiyDomainMailGun(emailDomain);
            //if (response == true)
            //{
            //    return View("ResetPassword", model);
            // }
            return View("ResetPassword", model);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {

                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                ViewBag.Message = "User does not exists";
                // Don't reveal that the user does not exist
                //return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            else
            {
                ViewBag.UserId = user.Id;
                var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);

                if (result.Succeeded)
                {
                    if (model.isNewUser == true)
                    {
                        //var domain = model.Email.Split('@')[1];
                        //string emailDomain = "";
                        //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                        //{

                        //    model.CompanyName = await MailgunHelper.ReplaceSpecialCharWithBlank(model.CompanyName);
                        //    emailDomain = await MailgunHelper.ReturnDomainforWhitelabelDomain(model.name.ToLower(), model.CompanyName.ToLower());
                        //    //ViewBag.Message = "Password successfully created.";
                        //}
                        //else
                        //{
                        //    emailDomain = await MailgunHelper.ReturnMailWithDomain(model.Email);
                        //}
                        //var webhooksettings = await MailgunHelper.InsertWebHookLinks(emailDomain);
                        ///*To check whether domain should active or not by calling verifyDomainSetting mail gun API*/
                        //var domainVerify = emailDomain.Split('@')[1];
                        //var response = await MailgunHelper.verifyDomainSettings(domainVerify);
                        //if (response == true)
                        //{

                        //    ViewBag.Message = "Password successfully created.";
                        //}
                        //else
                        //{
                        //    ViewBag.Message = "Password successfully created.";
                        //}


                        ViewBag.Message = "Password successfully created.";
                    }
                    else
                    {
                        ViewBag.Message = "Your password has been reset successfully!";

                    }

                    //return RedirectToAction("ResetPasswordConfirmation", "Account");
                }

                else
                {
                    ViewBag.Message = "You have already created your password";
                }

                AddErrors(result);
            }
            ModelState.Clear();
            return View(new ResetPasswordViewModel());

        }


        [AllowAnonymous]
        public async Task<bool> verfiyDomainMailGun(string email)
        {
            /*To check whether domain is verified or not*/
            var check = await MailgunHelper.IsVerifiedDomainMailGun(email);
            if (check == 0)
            {
                /*To create domain if its not created*/
                var response = await MailgunHelper.createDomainMailGun(email);

            }
            return true;
        }
        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [NoCache]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        //public string CardPayment(RegisterViewModel model)
        //{
        //    AnTranResponse tranres = null;
        //    using (PaymentSession paySession = new PaymentSession())
        //    {
        //        decimal amount = model.PackageBillingAmount != null ? (decimal)model.PackageBillingAmount.Value : 0;
        //        CCProfile profile = new CCProfile();
        //        profile.FirstName = model.FirstName;
        //        profile.LastName = model.LastName;
        //        profile.Email = model.Email;
        //        profile.Address = model.Address1;
        //        profile.City = model.City;
        //        profile.State = model.State;
        //        profile.Country = model.Country;
        //        profile.Zip = model.Zip;
        //        profile.ContactNo = model.MobilePhone;
        //        profile.CardNumber = model.CreditCardNo;
        //        profile.CCV = model.cvv;
        //        profile.ExpirationDate = model.ExpiryDate.Value.ToString("MM") + model.ExpiryDate.Value.ToString("yy");
        //        tranres = paySession.ChargeCreditCard(profile, amount);
        //        if (paySession.Status && tranres.ResponseCode == "1")
        //        {

        //            AnTransactionLog transLog = new AnTransactionLog();
        //            transLog.AnTransactionId = tranres.TransId;
        //            transLog.TransactionDate = DateTime.Now;
        //            transLog.CreatedBy = "Register";
        //            transLog.Message = "approval Code: " + tranres.ApprovalCode;
        //            transLog.TransactionType = "Credit Card";
        //            transLog.Amount = amount.ToString();
        //            transLog.TransactionDate = DateTime.Now;
        //            transLog.CreatedDate = DateTime.Now;
        //            transLog.PaymentProfileId = profile.CardNumber;
        //            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "TransactionLog");
        //            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(transLog)).StatusCode;

        //            return tranres.TransId;
        //        }
        //        else
        //        {
        //            return paySession.Message;
        //        }

        //    }
        //}




        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }


        }

        async Task<bool> domainVerification(string userId, string email, string name)
        {
            var host = email.Split('@')[1];
            var response = _sendGrid.postDomainAuthenticate(host);
            if (response != null)
            {

                var domainResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<DomainAuthenticationViewModel>(response);
                using (var db = new EmailMarketingDbEntities())
                {
                    VerifiedDomain model = new VerifiedDomain();
                    model.Domain = domainResponse.Domain;
                    model.CNAME = domainResponse.dns.Mail_Cname.host;
                    // model.Status = domainResponse.Valid;
                    db.VerifiedDomains.Add(model);
                    db.SaveChanges();
                    await UserManager.SendEmailAsync(userId, "CName Details", EmailHtml.getCNameDetails(name, model.CNAME));
                    return true;
                }
            }
            else
            {
                return false;
            }

        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmailVerification(string userId, string email, string name, int CampaignId)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                var CampSendDomain = "";
                var userDetails = db.UserProfiles.SingleOrDefault(x => x.Id == userId);
                var campaign = db.Campaigns.SingleOrDefault(x => x.CampaignId == CampaignId);
                CampSendDomain = await MailgunHelper.AddSubDomaininEmailWithLower(email, userDetails.CompanyName);
                campaign.SendingFromEmail = CampSendDomain;
                db.SaveChanges();
                //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
                //{
                //    //var userDetails = db.UserProfiles.SingleOrDefault(x => x.Id == userId);
                //    var campaign = db.Campaigns.SingleOrDefault(x => x.CampaignId == CampaignId);
                //    if (campaign != null)
                //    {
                //        /*To change sending email address as per current from email address*/
                //        //userDetails.CompanyName = await MailgunHelper.ReplaceSpaces(userDetails.CompanyName);
                //        var emailFirstPart = email.Split('@')[0];
                //        var emailSecondPart = domain;
                //        //if (emailFirstPart.Contains('.'))
                //        //{
                //        //    var emailFirstFPart = emailFirstPart.Split('.')[0];

                //        //    CampSendDomain = emailFirstFPart + "@" + Constants.RootDomain;
                //        //}
                //        //else
                //        //{
                //            CampSendDomain = emailFirstPart + "@"  + Constants.RootDomain;
                //       // }
                //        campaign.SendingFromEmail = CampSendDomain.ToLower();
                //        emailDomain = CampSendDomain.ToLower();
                //        db.SaveChanges();
                //    }

                //}
                //else
                //{
                //    /*To change sending email address as per current from email address*/
                //    var campaign = db.Campaigns.SingleOrDefault(x => x.CampaignId == CampaignId);
                //    if (campaign != null)
                //    {
                //        CampSendDomain = await MailgunHelper.ReturnMailWithDomain(email);
                //        campaign.SendingFromEmail = CampSendDomain.ToLower();
                //        emailDomain = CampSendDomain.ToLower();
                //        db.SaveChanges();
                //    }
                //}
                /*To insert email in Email confirmation as user clicks on email verification link*/
                var EmailDetails = db.EmailConfirmations.SingleOrDefault(x => x.UserId == userId && x.FromEmailAddress == email);
                if (EmailDetails == null)
                {
                    EmailConfirmation oEmail = new EmailConfirmation();
                    oEmail.UserId = userId;
                    oEmail.FromEmailAddress = email;
                    oEmail.Status = true;
                    oEmail.SendingFromEmail = CampSendDomain.ToLower();
                    db.EmailConfirmations.Add(oEmail);
                    db.SaveChanges();
                }
                /*Domain creation process and insert records in go daddy, insert webhook link and call verify domain api*/
                //var check = await MailgunHelper.IsVerifiedDomainMailGun(emailDomain);
                //if (check == 0)
                //{
                //    var response = await MailgunHelper.createDomainMailGun(emailDomain);
                //    if (response == true)
                //    {
                //        var webhooksettings = await MailgunHelper.InsertWebHookLinks(emailDomain);
                //        var domainVerify = emailDomain.Split('@')[1];
                //        var domainSetting = await MailgunHelper.verifyDomainSettings(domainVerify);
                //        if (domainSetting == true)
                //        {
                //            ViewBag.Message = "Thank you for confirming your email address.";
                //        }
                //        else
                //        {
                //            ViewBag.Message = "Thank you for confirming your email address.";
                //        }
                //        return View("ConfirmEmailVerification");

                //    }
                //    else
                //    {
                //        return View("Error");
                //    }
                //}
                //else
                //{
                    ViewBag.Message = "Thank you for confirming your email address.";
                    return View("ConfirmEmailVerification");
                //}
            }
        }


        [AllowAnonymous]
        public async Task<ActionResult> ResetPasswordLogin(string userid)
        {
            var user = await UserManager.FindByIdAsync(userid);
            if (user != null)
            {
                await SignInManager.SignInAsync(UserManager.FindById(userid), true, true);
                //switch (result)
                //{
                //    case SignInStatus.Success:
                //        {
                //            return RedirectToAction("GetDataAndRedirectToDashboard");
                //        }
                //    case SignInStatus.LockedOut:
                //        return View("Lockout");
                //    case SignInStatus.RequiresVerification:
                //        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                //    case SignInStatus.Failure:
                //        ViewBag.Message = "Login details are incorrect";
                //        return View();
                //    default:
                //        ModelState.AddModelError("", "Invalid login attempt.");
                //        return View(model);
                //}
                return RedirectToAction("GetDataAndRedirectToDashboard");
            }

            else
            {
                return RedirectToAction("Login");

            }
        }



        #endregion
    }
}