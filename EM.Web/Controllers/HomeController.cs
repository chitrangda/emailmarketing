﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web;
using EM.Web.Models;
using EM.Web.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace EmailMarketing.Web.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            SendGridAPIHelper sendGridapi = new SendGridAPIHelper();
            //sendGridapi.getIP("CE Marketing");
            return View();
        }

        public ActionResult TestFileUpload()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ContactUs(ContactUsViewModel model)
        {
            //SendGridEmails sendGrid = new SendGridEmails();
            Mail oMail = new Mail();
            await oMail.SendMail(Constants.SupportEmail, "Support Mail", EmailHtml.getContactHtml(model.Name, model.Email, model.PhoneNumber, model.Comment), Constants.SupportName, "mailSettings/Support", model.Name);
            ViewBag.Message = "Thank you for contacting us, we will get back to you soon.";
            ModelState.Clear();
            return View(new ContactUsViewModel());
        }
        public ActionResult GetAQuote()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetAQuote(GetAQuoteViewModel model)
        {
            //SendGridEmails sendGrid = new SendGridEmails();
            //await _mailGun.sendSingleEmail("", "Get a Quote Request - " + model.Subject, EmailHtml.getQuoteHtml(model.Name, model.Email, model.Subject, model.Message), Constants.SupportEmail, Constants.SupportName, model.Email, model.Name);
            Mail oMail = new Mail();
            await oMail.SendMail(Constants.SupportEmail, "Get a Quote Request - " + model.Subject, EmailHtml.getQuoteHtml(model.Name, model.Email, model.Subject, model.Message), Constants.SupportName, "mailSettings/Support", model.Name);
            ViewBag.Message = "Thank you for contacting us, we will get back to you soon.";
            ModelState.Clear();
            return View(new GetAQuoteViewModel());
        }

        public ActionResult Error()
        {
            return View();
        }
        public ActionResult Unsubscribe(int id, string e, string uid)
        {
            String APIURL = string.Format("{0}/{1}?id={2}&email={3}&uid={4}", Constants.ApiURL, "Campaign/UnsubscribedfromCampaign",id,e,uid);
            var result = (new APICallHelper()).Put(APIURL);
            if (result != null)
            {
                var response = JsonConvert.DeserializeObject<usp_UnSubscribefromCampaign_Result>(result.Content.ReadAsStringAsync().Result);
                if (response.Result != 0)
                {
                    return View();
                }
            }
                //var context = new EmailMarketingDbEntities();
                //var subscriber = context.Subscribers.Where(r => r.Email == e && r.CreatedById == uid).FirstOrDefault();
                //var campaign = context.Campaigns.Find(id);
                //if (campaign != null)
                //{
                //    string[] arrListId = campaign.ListId != null ? campaign.ListId.Split(',') : null;
                //    if (arrListId != null)
                //    {
                //        foreach (string listid in arrListId)
                //        {
                //            int listId = Convert.ToInt32(listid);
                //            var subscriberlistInfo = context.SubscriberLists.Where(r => r.ListId == listId && r.SubscriberId == subscriber.SubscriberId).FirstOrDefault();
                //            if (subscriberlistInfo != null)
                //            {
                //                subscriberlistInfo.IsSubscribe = false;
                //                context.SaveChanges();
                //            }
                //        }
                //    }

                //}
                return View();
        }

        public ActionResult PricingPlan()
        {
            PacakagesViewModel model = new PacakagesViewModel();
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "EmailPlans");
            var result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.packagesList = JsonConvert.DeserializeObject<List<usp_geAllPlans_Result>>(result.ToString());
            }
            APIURL = string.Format("{0}/{1}", Constants.ApiURL, "EmailPlans/GetPlan");
            result = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                model.planList = JsonConvert.DeserializeObject<List<Plan>>(result.ToString());
            }
            return View(model);
        }

        public ActionResult GetPaymentStatus()
        {
            if (Request.QueryString["token-id"] != null)
            {
                string APIURL = string.Format("{0}/{1}?TokenId={2}", Constants.ApiURL, "BillingPaymentService", Request.QueryString["token-id"]);
                var result = (new APICallHelper()).Get(APIURL);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    CreateLog.Log(Request.QueryString["token-id"]);
                }
            }
            return View("Index");
        }

    }


}

