﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Web.Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace EM.Web.Utilities
{
    public class MailgunHelper
    {
        static string apiKey = Constants.MailgunKey;
        static string url = "https://api.mailgun.net/v3";
        static string senderDomain = "cheapestemail.com";

        /*Send mail to single recipient*/
        public async Task sendSingleEmail(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
           
            string fromEmailaddress = System.Configuration.ConfigurationManager.AppSettings["SupportEmailhost"];
            string fromDomain = fromEmailaddress.Split('@')[1];
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(url);
            client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", fromDomain, ParameterType.UrlSegment);
            request.Resource = fromDomain + "/messages";
            request.AddParameter("from", senderName + " <" + fromEmailaddress + ">");// Excited User <mailgun@sandbox5c97fa2b49464f5093726913d09d8c77.mailgun.org>");
            request.AddParameter("to", recipientEmail);
            request.AddParameter("subject", subject);
            request.AddParameter("html", contentHtml);
            request.AddParameter("h:Reply-To", senderEmail);
            request.Method = Method.POST;
            //var response = client.Execute(request);
            var response = await client.ExecutePostTaskAsync(request);
            LogService.WriteToLog(response.StatusCode.ToString(), Id);

        }

        /*Send mail to multiple recipients*/
        public async Task sendMultipleEmail(string Id, string subject, string contentHtml, string senderEmail, string senderName, List<MailgunRecipientModel> recipientEmails, string recipientName)
        {
            Dictionary<string, MailgunRecipientModel> d = new Dictionary<string, MailgunRecipientModel>();

            RestClient client = new RestClient();
            client.BaseUrl = new Uri(url);
            client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", senderDomain, ParameterType.UrlSegment);
            request.Resource = senderDomain + "/messages";
            request.AddParameter("from", senderName + " <" + senderEmail + ">");// Excited User <mailgun@sandbox5c97fa2b49464f5093726913d09d8c77.mailgun.org>");
            foreach (MailgunRecipientModel oRecipient in recipientEmails)
            {
                request.AddParameter("to", oRecipient.Email);
                d[oRecipient.Email] = oRecipient;

            }
            request.AddParameter("subject", subject);
            request.AddParameter("html", contentHtml);
            request.AddParameter("recipient-variables", new JavaScriptSerializer().Serialize(d));
            request.Method = Method.POST;
            //var response = client.Execute(request);
            var response = await client.ExecutePostTaskAsync(request);
            LogService.WriteToLog(response.StatusCode.ToString(), Id);

        }

        /*Send mail to single recipient for email invoice*/
        public async Task sendSingleEmailInvoice(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            string fromEmailaddress= System.Configuration.ConfigurationManager.AppSettings["BillingEmailhost"];
            string fromDomain = fromEmailaddress.Split('@')[1];

            RestClient client = new RestClient();
            client.BaseUrl = new Uri(url);
            client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", fromDomain, ParameterType.UrlSegment);
            request.Resource = fromDomain + "/messages";
            request.AddParameter("from", senderName + " <" + fromEmailaddress + ">");// Excited User <mailgun@sandbox5c97fa2b49464f5093726913d09d8c77.mailgun.org>");
            request.AddParameter("to", recipientEmail);
            request.AddParameter("bcc", System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);
            request.AddParameter("subject", subject);
            request.AddParameter("html", contentHtml);
            request.AddParameter("h:Reply-To", senderEmail);
            request.Method = Method.POST;
            //var response = client.Execute(request);
            var response = await client.ExecutePostTaskAsync(request);
            LogService.WriteToLog(response.StatusCode.ToString(), Id);

        }

        /*To check whether domain is created or not in mail gun.*/
        public static async Task<int> IsVerifiedDomainMailGun(string email)
        {
            var domain = email.Split('@')[1];
            if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
            {
                return 1;
            }
            if (domain.Contains('.'))
            {
                domain = domain.Split('.')[0];
            }
            var subDomainName = domain + Constants.RootDomain;
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(Constants.MailgunAPI);
            client.Authenticator =
            new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", subDomainName, ParameterType.UrlSegment);
            request.Resource = "/domains/{domain}";
            var response = client.Execute(request);
            if (response != null)
            {
                var domainResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<MailGunDomainAuthenticationViewModel>(response.Content.ToString());
                if (domainResponse.message == "Domain not found")
                {
                    return 0;
                }
                else
                if (domainResponse != null && domainResponse.domain.state == "active")
                {
                    using (var db = new EmailMarketingDbEntities())
                    {
                        var vdomain = db.VerifiedDomains.Where(x => x.DomainID == domainResponse.domain.id).FirstOrDefault();
                        if (vdomain != null)
                        {
                            vdomain.Status = "active";
                            await db.SaveChangesAsync();

                        }
                    }
                    return 1;
                }
                //else
                //{
                //    var result = await AuthenticateDNS(domainResponse);
                //    return result;
                //}
            }
            return 2;
        }

        /*To create domain in mail gun.*/
        public static async Task<bool> createDomainMailGun(string email)
        {
            MailGunDomainAuthenticationViewModel domainResponse = new MailGunDomainAuthenticationViewModel();
            var subDomainName = await MailgunHelper.ReturnDomain(email);
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(Constants.MailgunAPI + "/");
            client.Authenticator =
            new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "domains";
            request.AddParameter("name", subDomainName);
            request.AddParameter("force_dkim_authority", true);
            //request.AddParameter("smtp_password", "supersecretpassword");
            request.Method = Method.POST;
            var response = client.Execute(request);
            if (response != null)
            {
                domainResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<MailGunDomainAuthenticationViewModel>(response.Content.ToString());
                if (domainResponse != null)
                {
                    using (var db = new EmailMarketingDbEntities())
                    {
                        VerifiedDomain verifiedmodel = new VerifiedDomain();
                        verifiedmodel.Domain = domainResponse.domain.name;
                        verifiedmodel.Status = domainResponse.domain.state;
                        verifiedmodel.DomainID = domainResponse.domain.id;
                        db.VerifiedDomains.Add(verifiedmodel);
                        db.SaveChanges();
                    }
                }
                /*To add DNS records in GoDaddy*/
                int result = await AuthenticateDNS(domainResponse);
                if (result == 1)
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /*To enter dns records in go daddy.*/
        public static async Task<int> AuthenticateDNS(MailGunDomainAuthenticationViewModel model)
        {
            string subdomain = model.domain.name;
            string rootdomain = Constants.RootDomain;
            if (model != null)
            {
                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("sso-key", Constants.GodaddyKey);
                    var requestURl = Constants.GodaddyAPI;
                    List<DomainRecordsViewModel> records = new List<DomainRecordsViewModel>();
                    foreach (var item in model.sending_dns_records)
                    {
                        DomainRecordsViewModel oDNS = new DomainRecordsViewModel();
                        oDNS.data = item.value;
                        oDNS.type = item.record_type;
                        oDNS.ttl = 600;
                        if (item.record_type == "TXT")
                        {
                            if (item.value.IndexOf("spf1") >= 0)
                            {
                                oDNS.name = item.name.Substring(0, (subdomain.Length - rootdomain.Length));
                            }
                            else
                            {
                                oDNS.name = item.name.Substring(0, (item.name.Length - rootdomain.Length));

                            }
                        }
                        else
                        {
                            oDNS.name = item.name.Substring(0, (item.name.Length - rootdomain.Length));
                        }
                        records.Add(oDNS);

                    }
                    foreach (var item in model.receiving_dns_records)
                    {

                        records.Add(new DomainRecordsViewModel
                        {
                            data = item.value,
                            name = subdomain.Substring(0, (subdomain.Length - rootdomain.Length)),
                            type = item.record_type,
                            ttl = 600,

                        });

                    }
                    var body = JsonConvert.SerializeObject(records);
                    var stringContent = new StringContent(body, Encoding.UTF8, "application/json");
                    var method = new HttpMethod("PATCH");
                    var request = new HttpRequestMessage(method, requestURl)
                    {
                        Content = stringContent
                    };
                    HttpResponseMessage responseMessage = client.SendAsync(request).Result;
                    if (responseMessage.StatusCode == HttpStatusCode.OK)
                    {
                        /*To open and click tracking*/
                        var result = await MailgunHelper.UpdateOpenClickTrackingSetting(model.domain.name);
                        //using (var db = new EmailMarketingDbEntities())
                        //{
                        //    var domain = db.VerifiedDomains.Where(x => x.DomainID == model.domain.id).FirstOrDefault();
                        //    if (domain != null)
                        //    {
                        //        domain.Status = "active";
                        //        await db.SaveChangesAsync();

                        //    }


                        //}

                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
            }
            return 2;
        }

        /*To create route in mail gun.*/
        public static async Task<bool> CreateRoute(string email)
        {
            var subDomainName = await MailgunHelper.ReturnDomain(email);
            var recipient = ".*" + email.Split('@')[0] + "@" + subDomainName;
            RouteViewModel routeResponse = new RouteViewModel();
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(Constants.MailgunAPI);
            client.Authenticator = new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "routes";
            request.AddParameter("priority", 0);
            request.AddParameter("expression", "match_recipient('" + recipient + "')");
            request.AddParameter("action", "forward('" + email + "')");
            request.AddParameter("action", "stop()");
            request.Method = Method.POST;
            var response = client.Execute(request);
            if (response != null)
            {
                routeResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<RouteViewModel>(response.Content.ToString());
                if (routeResponse.message == "Route has been created")
                {
                    return true;
                }
            }
            return false;
        }

        /*To verify domain setting through mail gun api.*/
        public static async Task<bool> verifyDomainSettings(string domain)
        {
            // var subDomainName = MailgunHelper.ReturnDomain(email);
            //var subDomainName = string.Empty;
            //if (Array.IndexOf(Constants.whilelabelDomainsArr, domain) != -1)
            //{
            //    subDomainName = domain + Constants.RootDomain;
            //}
            //else
            //{
            var subDomainName = domain;
            //}
            MailGunDomainAuthenticationViewModel res = new MailGunDomainAuthenticationViewModel();
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(Constants.MailgunAPI);
            client.Authenticator = new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "domains/" + subDomainName + "/verify";
            request.Method = Method.PUT;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                res = Newtonsoft.Json.JsonConvert.DeserializeObject<MailGunDomainAuthenticationViewModel>(response.Content.ToString());
                if (res != null && res.domain.state == "active")
                {
                    using (var db = new EmailMarketingDbEntities())
                    {
                        var domainDetails = db.VerifiedDomains.Where(x => x.Domain == subDomainName).FirstOrDefault();
                        if (domainDetails != null)
                        {
                            domainDetails.Status = "active";
                            await db.SaveChangesAsync();

                        }
                        return true;
                    }
                }
            }
            return false;
        }


        /*To on open and click tracking of a particular domain through mail gun api.*/
        public static async Task<bool> UpdateOpenClickTrackingSetting(string subDomainName)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(Constants.MailgunAPI);
            client.Authenticator =
            new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "domains/" + subDomainName + "/tracking/open";
            request.AddParameter("active", true);
            request.Method = Method.PUT;
            var Openresponse = client.Execute(request);
            RestRequest request1 = new RestRequest();
            request1.Resource = "domains/" + subDomainName + "/tracking/click";
            request1.AddParameter("active", true);
            request1.Method = Method.PUT;
            var clickresponse = client.Execute(request1);
            if (Openresponse.StatusCode == HttpStatusCode.OK && clickresponse.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        /*To split domain and add main domain in it.*/
        public static async Task<string> ReturnDomain(string email)
        {
            var domain = email.Split('@')[1];
            if (domain.Contains('.'))
            {
                domain = domain.Split('.')[0];
            }
            var subDomainName = domain + Constants.RootDomain;
            return subDomainName;
        }

        /*To insert webhook links for a particular domain in mail gun*/
        public static async Task<bool> InsertWebHookLinks(string email)
        {
            var subDomainName = await MailgunHelper.ReturnDomain(email);
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(Constants.MailgunAPI);
            client.Authenticator =
            new HttpBasicAuthenticator("api", Constants.MailgunKey);

            RestRequest request = new RestRequest();
            request.Resource = "domains/" + subDomainName + "/webhooks";
            request.Method = Method.POST;
            request.AddParameter("id", "clicked");
            request.AddParameter("url", "https://webhookapi.cheapestemail.com/api/MailGunClickWebhook");
            var webhookLinkClicked = client.Execute(request);

            RestRequest request1 = new RestRequest();
            request1.Resource = "domains/" + subDomainName + "/webhooks";
            request1.Method = Method.POST;
            request1.AddParameter("id", "delivered");
            request1.AddParameter("url", "https://webhookapi.cheapestemail.com/api/MailGunDeliveredWebhook");
            var webhookLinkDelivered = client.Execute(request1);

            RestRequest request2 = new RestRequest();
            request2.Resource = "domains/" + subDomainName + "/webhooks";
            request2.Method = Method.POST;
            request2.AddParameter("id", "opened");
            request2.AddParameter("url", "https://webhookapi.cheapestemail.com/api/MailGunOpenWebhook");
            var webhookLinkOpened = client.Execute(request2);

            RestRequest request3 = new RestRequest();
            request3.Resource = "domains/" + subDomainName + "/webhooks";
            request3.Method = Method.POST;
            request3.AddParameter("id", "permanent_fail");
            request3.AddParameter("url", "https://webhookapi.cheapestemail.com/api/MailGunPermanentFailedWebhook");
            var webhookLinkPerFail = client.Execute(request3);

            RestRequest request4 = new RestRequest();
            request4.Resource = "domains/" + subDomainName + "/webhooks";
            request4.Method = Method.POST;
            request4.AddParameter("id", "temporary_fail");
            request4.AddParameter("url", "https://webhookapi.cheapestemail.com/api/MailGunTempFailedWebhook");
            var webhookLinkTempFail = client.Execute(request4);

            RestRequest request5 = new RestRequest();
            request5.Resource = "domains/" + subDomainName + "/webhooks";
            request5.Method = Method.POST;
            request5.AddParameter("id", "complained");
            request5.AddParameter("url", "https://webhookapi.cheapestemail.com/api/MailGunComplainedWebhook");
            var webhookComplained = client.Execute(request5);


            if (webhookLinkClicked.StatusCode == HttpStatusCode.OK && webhookLinkDelivered.StatusCode == HttpStatusCode.OK && webhookLinkOpened.StatusCode == HttpStatusCode.OK
                && webhookLinkTempFail.StatusCode == HttpStatusCode.OK && webhookLinkPerFail.StatusCode == HttpStatusCode.OK && webhookComplained.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public static async Task<bool> CreateMailingList(string listName)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "lists";
            request.AddParameter("address", listName);
            request.Method = Method.POST;
            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        public static async Task<bool> AddListMember(SubscriberViewModel model)
        {
            model.LastName = model.LastName != null ? model.LastName : "null";
            model.Address1 = model.Address1 != null ? model.Address1 : "null";
            model.Address2 = model.Address2 != null ? model.Address2 : "null";
            model.City = model.City != null ? model.City : "null";
            model.State = model.State != null ? model.State : "null";
            model.ZipCode = model.ZipCode != null ? model.ZipCode : "null";
            model.Country = model.Country != null ? model.Country : "null";
            model.PhoneNumber = model.PhoneNumber != null ? model.PhoneNumber : "null";

            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "lists/{list}/members";
            request.AddParameter("list", model.ListName, ParameterType.UrlSegment);
            request.AddParameter("address", model.Email);
            request.AddParameter("subscribed", true);
            request.AddParameter("name", model.FirstName != null ? model.FirstName : null);
            request.AddParameter("vars", "{\"LastName\": \"" + model.LastName + "\",\"Address1\": \"" + model.Address1 + "\",\"Address2\": \"" + model.Address2 + "\" ,\"City\": \"" + model.City + "\",\"State\": \"" + model.State + "\",\"ZipCode\": \"" + model.ZipCode +
                "\",\"Country\": \"" + model.Country + "\",\"PhoneNumber\": \"" + model.PhoneNumber + "\"}");
            request.Method = Method.POST;
            var response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }

        /*To validate email address through mail gun.*/
        public static async Task<bool> GetValidate(string email)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v4");
            client.Authenticator = new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "/address/validate";
            request.AddParameter("address", email);
            var response = client.Execute(request);
            if (response != null)
            {
                var oResult = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailValidationResultModel>(response.Content.ToString());
                if (oResult.result == "deliverable")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool isValidEmail(string email)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v4");
            client.Authenticator = new HttpBasicAuthenticator("api", Constants.MailgunKey);
            RestRequest request = new RestRequest();
            request.Resource = "/address/validate";
            request.AddParameter("address", email);
            var response = client.Execute(request);
            if (response != null)
            {
                var oResult = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailValidationResultModel>(response.Content.ToString());
                if (oResult.result == "deliverable")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /*To return subdomain on the basis of white label domain*/
        public static async Task<string> ReturnDomainforWhitelabelDomain(string name, string company)
        {
            if (name.Contains(' '))
            {
                name = name.Split(' ')[0];
            }
           // var subDomainName = name + "@" + company + Constants.RootDomain;
            var subDomainName = name + "." + company + "@"  + Constants.RootDomain;
            return subDomainName;
        }

        /*To return subdomain on the basis of if email is not in white label list*/
        public static async Task<string> ReturnMailWithDomain(string email)
        {
            
            var emailFirstPart = email.Split('@')[0];
            var emailSecondPart = email.Split('@')[1];
            //string emailSNew = Regex.Replace(emailSecondPart,"[!@#$%^&*(),?:{}|<>]", "");
            string emailDomain = emailFirstPart + "." + emailSecondPart.Substring(0, emailSecondPart.LastIndexOf('.')) + "@" + Constants.RootDomain;
            //if (emailFirstPart.Contains('.'))
            //{
            //    var emailFirstFPart = emailFirstPart.Split('.')[0];

            //    emailDomain = emailFirstFPart + "@" + emailSecondPart.Substring(0, emailSecondPart.LastIndexOf('.')) + Constants.RootDomain;
            //}
            //else
            //{
            //    emailDomain = emailFirstPart + "@" + emailSecondPart.Substring(0, emailSecondPart.LastIndexOf('.')) + Constants.RootDomain;
            //}
            return emailDomain.ToLower();
        }

        /*To replace special character from company name*/
        public static async Task<string> ReplaceSpecialCharWithBlank(string CompanyName)
        {
            string newCompanyName = CompanyName.Replace(" ", "");
            //string CompName = Regex.Replace(newCompanyName, "[!@#$%^&*(),.?:{}|<>]", "");
            return newCompanyName.ToLower();
        }

        public static async Task<string> AddSubDomaininEmailWithLower(string email, string CompanyName)
        {
            string newCompanyName = CompanyName.Replace(" ", "");
            string sendingDomain = (email.Split('@')[0] + "." + newCompanyName + "@" + Constants.subDomainName.First() + "." + Constants.RootDomain).ToLower();
            return sendingDomain;
        }

    }
}
