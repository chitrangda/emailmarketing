﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EM.Web.Extentions
{
    public static class Extentions
    {
        public static string SaveFile(this HttpPostedFileBase file, string OldURL = "")
        {
            if (file != null)
            {
                var ServerPath = HttpContext.Current.Server.MapPath("~");
                var ImagePath = "/UserUpload/photos/";
                if (!Directory.Exists(ServerPath + "/UserUpload"))
                {
                    Directory.CreateDirectory(ServerPath + "/UserUpload");
                    if (!Directory.Exists(ServerPath + ImagePath))
                    {
                        Directory.CreateDirectory(ServerPath + ImagePath);
                    }

                }
                var dateFileName = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "").Replace(" ", "").Replace(":", "").ToString();
                var ImageNameWithPath = string.Format("{0}{1}{2}", ImagePath, dateFileName, Path.GetExtension(file.FileName));
                file.SaveAs(ServerPath + ImageNameWithPath);
                if (OldURL != "")
                {
                    DeleteImage(OldURL);
                }
                return ImageNameWithPath;
            }
            return "";
        }

        public static string SaveFile(this HttpPostedFileBase file)
        {
            if (file != null)
            {
                var ServerPath = HttpContext.Current.Server.MapPath("~");
                var ImagePath = "/UserUpload/photos/";
                if (!Directory.Exists(ServerPath + "/UserUpload"))
                {
                    Directory.CreateDirectory(ServerPath + "/UserUpload");
                    if (!Directory.Exists(ServerPath + ImagePath))
                    {
                        Directory.CreateDirectory(ServerPath + ImagePath);
                    }

                }
                var dateFileName = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "").Replace(" ", "").Replace(":", "").ToString();
                var ImageNameWithPath = string.Format("{0}{1}{2}", ImagePath, dateFileName, Path.GetExtension(file.FileName));
                file.SaveAs(ServerPath + ImageNameWithPath);
                return ImageNameWithPath;
            }
            return "";
        }

        public static string SaveImageTemplateFileInFolder(this System.Drawing.Image file)
        {
            if (file != null)
            {
                var ServerPath = HttpContext.Current.Server.MapPath("~");
                var ImagePath = "/UserUpload/templateImages/";
                if (!Directory.Exists(ServerPath + "/UserUpload"))
                {
                    Directory.CreateDirectory(ServerPath + "/UserUpload");
                }
                if (!Directory.Exists(ServerPath + ImagePath))
                {
                    Directory.CreateDirectory(ServerPath + ImagePath);
                }
                var dateFileName = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "").Replace(" ", "").Replace(":", "").ToString();
                var ImageNameWithPath = string.Format("{0}{1}", ImagePath, dateFileName);
                file.Save(ServerPath + ImageNameWithPath+".png", System.Drawing.Imaging.ImageFormat.Png);
                return ImageNameWithPath + ".png";
            }
            return "";
        }

        public static string SaveExcelFile(this HttpPostedFileBase file)
        {
            if (file != null)
            {
                var ServerPath = HttpContext.Current.Server.MapPath("~");
                var ExcelPath = "/UserUpload/excels/";
                if (!Directory.Exists(ServerPath + "/UserUpload"))
                {
                    Directory.CreateDirectory(ServerPath + "/UserUpload");
                }
                if (!Directory.Exists(ServerPath + ExcelPath))
                {
                    Directory.CreateDirectory(ServerPath + ExcelPath);
                }
                var dateFileName = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "").Replace(" ", "").Replace(":", "").ToString();
                var ExcelNameWithPath = string.Format("{0}{1}{2}", ExcelPath, dateFileName, Path.GetExtension(file.FileName));
                file.SaveAs(ServerPath + ExcelNameWithPath);
                return ExcelNameWithPath;
            }
            return "";
        }

        public static string CheckNullOrEmpty(this object value)
        {
            return value != null ? value.ToString() != "" ? value.ToString() : "" : "";
        }

        public static bool DeleteImage(this string URL)
        {
            try
            {
                var ImageServerPath = HttpContext.Current.Server.MapPath("~" + URL);
                if (System.IO.File.Exists(ImageServerPath))
                {
                    System.IO.File.Delete(ImageServerPath);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return false;
        }

        public static int ConvertIntoInt32(this int? value)
        {
            if (value is null)
            {
                return 0;
            }
            return Convert.ToInt32(value);
        }
        public static System.Drawing.Image Base64ToImage(this string base64String)
        {
            try
            {
                byte[] imageBytes = Convert.FromBase64String(base64String);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                return image;
            }
            catch (Exception)
            {
                return null;
            }
           
        }
    }
}