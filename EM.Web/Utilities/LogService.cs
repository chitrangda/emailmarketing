﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;


namespace EM.Web.Utilities
{
    public class LogService
    {
        public static int CreateLog(string userid, string Description, string CreatedBY)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                try
                {
                    EmailServiceLog obj = new EmailServiceLog();
                    obj.UserId = userid;
                    obj.Description = Description;
                    obj.LogDateTime = DateTime.Now;
                    obj.Createdby = CreatedBY;

                    db.EmailServiceLogs.Add(obj);
                    db.SaveChanges();
                    return 1;
                }
                catch (Exception) 
                {
                    return -1;
                }

            }
        }

        public static void WriteToLog(string msg,string id)
        {
            //get current directory and then prepare to create a file name Logger.txt
            var ServerPath = System.Web.HttpContext.Current.Server.MapPath("~");
            var TemplateLogs = "/UserUpload/TemplateLogs/";
            if (!Directory.Exists(ServerPath + "/UserUpload"))
            {
                Directory.CreateDirectory(ServerPath + "/UserUpload");
            }
            if (!Directory.Exists(ServerPath + TemplateLogs))
            {
                Directory.CreateDirectory(ServerPath + TemplateLogs);
            }
            var filepath = ServerPath + TemplateLogs;
            string filename = "Logger_" + id + ".txt";
            System.IO.StreamWriter sw = System.IO.File.AppendText(filepath + "\\"+filename);
            try
            {
                //include the time of error
                string logLine = System.String.Format("{0:G}: {1}.", System.DateTime.Now, msg);
                sw.WriteLine(logLine); //write the string to file
                sw.Flush();
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
