﻿using HiQPdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace EM.Web.Utilities
{
    public class ImageProcessing
    {

        public static void ConvertHtmlToImage(string htmlContent, string imagePath)
        {
            HtmlToImage htmlToImageConverter = new HtmlToImage();
            htmlToImageConverter.TriggerMode = ConversionTriggerMode.Auto;
            // convert to image
            System.Drawing.Image imageObject = null;
            imageObject = htmlToImageConverter.ConvertHtmlToImage(htmlContent, "")[0];
            //imagePath = imagePath.Replace(".png", ".jpg");
            MakeImageWithoutArea(imageObject, imagePath);
            //Imager.PerformImageResizeAndPutOnCanvas(imagePath, 500, 230, imagePath);
            imageObject.Dispose();
        }

        private static Bitmap MakeImageWithoutArea(Image source_bm, string imagePath)
        {
            // Copy the image.
            Bitmap bm = new Bitmap(source_bm);
            Color pixelColor = bm.GetPixel(480, 5);
            Rectangle rectangle = new Rectangle(480, 5, 250, 30);
            // Clear the selected area.
            using (Graphics gr = Graphics.FromImage(bm))
            {
                GraphicsPath path = new GraphicsPath();
                path.AddRectangle(rectangle);
                gr.SetClip(path);
                gr.Clear(pixelColor);
                gr.ResetClip();
            }
            bm.Save(imagePath, ImageFormat.Jpeg);
            return bm;
        }
    }
}