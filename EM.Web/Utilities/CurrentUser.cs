﻿using Microsoft.AspNet.Identity;
using System.Web;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Factory;

namespace EM.Web.Utilities
{
    public class CurrentUser
    {
        public static string getCurUserId()
        {

            string userid = HttpContext.Current.User.Identity.GetUserId();
            if (HttpContext.Current.User.IsInRole("Admin") || HttpContext.Current.User.IsInRole("SalesRep"))
            {
                if (HttpContext.Current.Session["UserDetails"] != null)
                {
                    var userInfo = HttpContext.Current.Session["UserDetails"] as UserInfoViewModel;
                    userid = userInfo.Id;
                }
                else
                {
                    RedirectToLogin();
                }
            }
            return userid;
        }

        public static void RedirectToLogin()
        {
            HttpContext.Current.Response.Redirect("~/Account/Login");
        }

        public static string getCurSalesRepId()
        {

            string userid = HttpContext.Current.User.Identity.GetUserId();
            if (HttpContext.Current.User.IsInRole("Admin"))
            {
                if (HttpContext.Current.Session["SalesRepDetails"] != null)
                {
                    var userInfo = HttpContext.Current.Session["SalesRepDetails"] as UserInfoViewModel;
                    userid = userInfo.Id;
                }
                else
                {
                    RedirectToLogin();
                }
            }
            return userid;
        }

        public static void createCurUserSession(string role, string id = null)
        {
            if (id == null)
            {
                id = HttpContext.Current.User.Identity.GetUserId();
            }
            if (role == UserRole.Admin.ToString())
            {
                string APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "UserInfo", id);
                var userResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var curUserInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoViewModel>(userResult.ToString());
                    HttpContext.Current.Session["AdminDetails"] = curUserInfo;
               
            }
            else if (role == UserRole.SalesRep.ToString())
            {
                string APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "ManageSalesRep", id);
                var userResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var curUserInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<SalesRepInfoViewModel>(userResult.ToString());
               
                    HttpContext.Current.Session["SalesRepDetails"] = curUserInfo;
              

            }
            else
            {
                string APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "UserInfo", id);
                var userResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                var curUserInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UserInfoViewModel>(userResult.ToString());

                    HttpContext.Current.Session["UserDetails"] = curUserInfo;
               

            }
        }
        public static UserInfoViewModel getCurUserDetails()
        {
            if (HttpContext.Current.Session["UserDetails"] != null)
            {
                return HttpContext.Current.Session["UserDetails"] as UserInfoViewModel;
            }
            else
            {
                RedirectToLogin();
                return null;
            }


        }
        public static void SetAuthoriztionInfo()
        {
            var userId = getCurUserId();
            string APIURL = string.Format("{0}/{1}/{2}", Constants.ApiURL, "UserInfo/GetAuthorizationInfo", userId);
            var userResult = (new APICallHelper()).Get(APIURL);
            if (userResult.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var authorizationInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getAuthorization_Result>(userResult.Content.ReadAsStringAsync().Result.ToString());

                    HttpContext.Current.Session["UserAuthorizationInfo"] = authorizationInfo;

                
            }
        }

        public static SalesRepInfoViewModel getSalesRep()
        {
            if (HttpContext.Current.Session["SalesRepDetails"] != null)
            {
                return HttpContext.Current.Session["SalesRepDetails"] as SalesRepInfoViewModel;
            }
            else
            {
                createCurUserSession("SalesRep");
                return HttpContext.Current.Session["SalesRepDetails"] as SalesRepInfoViewModel;

            }
        }

        public static UserInfoViewModel getAdminDetails()
        {
            if (HttpContext.Current.Session["AdminDetails"] != null)
            {
                return HttpContext.Current.Session["AdminDetails"] as UserInfoViewModel;
            }
            else
            {
                createCurUserSession("Admin");
                return HttpContext.Current.Session["AdminDetails"] as UserInfoViewModel;

            }
        }
    }
}
