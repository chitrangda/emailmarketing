﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace EM.Web
{
    public static class Constants
    {
        public static string ApiURL { private set; get; } = ConfigurationManager.AppSettings["ApiURL"];

        public static string staticAppUrl { private set; get; } = ConfigurationManager.AppSettings["StaticAppUrl"];

        public static string AppUrl = ConfigurationManager.AppSettings["AppUrl"];

        public static string BeePluginClientId { private set; get; } = ConfigurationManager.AppSettings["BeePluginClientId"];

        public static string BeePluginClientSecret { private set; get; } = ConfigurationManager.AppSettings["BeePluginClientSecret"];

        public static string BeeAppAuth { private set; get; } = ConfigurationManager.AppSettings["appAuth"];

        public static string ErrorTitle { private set; get; } = "Error";

        public static bool Error { private set; get; } = false;

        public static string ErrorMessage { private set; get; } = "Something went wrong!";

        public static string SuccessTitle { private set; get; } = "";

        public static bool Success { private set; get; } = true;

        public static int PageSize { get; private set; } = 25;

        public static string NotFoundTitle { private set; get; } = "Not Found!";

        public static string SubscriberListSampleURL { private set; get; } = "/UserUpload/SubscriberListSample.xlsx";

        public static string ListSampleURL { private set; get; } = "/UserUpload/ListSample.xlsx";

        public static string DefaultState = "United States" ;

        public static string SupportEmail = ConfigurationManager.AppSettings["SupportEmail"];

        public static string SupportName = ConfigurationManager.AppSettings["supportname"];

        public static string AdminId { private set; get; } = ConfigurationManager.AppSettings["AdminId"];

        public static string SubscriberLimit { private set; get; } = ConfigurationManager.AppSettings["SubscriberLimit"];

        public static int ReportPageSize { get; private set; } = Convert.ToInt32(ConfigurationManager.AppSettings["SendGridPageSize"]);

        public static int CampaignPageSize { get; private set; } = 10;

        public static string[] whilelabelDomainsArr = { "gmail.com", "yahoo.com", "hotmail.com", "rediffmail.com", "outlook.com","yopmail.com", "sandbox5c97fa2b49464f5093726913d09d8c77.mailgun.org", "cheapestemail.com" , "srmtechsol.com","aol.com","live.com"};

        public static string Password { private set; get; } = ConfigurationManager.AppSettings["Password"];
        public static string[]  DefaultIP { private set; get; } = { "149.72.132.147" };

        public static bool PaymentProductionMode { private set; get; }= bool.Parse(ConfigurationManager.AppSettings["PaymentProductionMode"]);

        public static string BillingEmail = ConfigurationManager.AppSettings["BillingEmail"];

        public static string BillingName = ConfigurationManager.AppSettings["BillingName"];
 
       public static string RootDomain { private set; get; } = ConfigurationManager.AppSettings["RootDomain"].ToString();

        public static string MailgunKey = ConfigurationManager.AppSettings["MailgunKey"].ToString();

        public static string MailgunAPI = ConfigurationManager.AppSettings["MailgunAPI"].ToString();

        public static string GodaddyKey = ConfigurationManager.AppSettings["GodaddyKey"].ToString();

        public static string GodaddyAPI = ConfigurationManager.AppSettings["GodaddyAPI"].ToString();
        public static string MailingListDomain { private set; get; } = "cheapestemail.com";

        public static string[] validHeaders = { "Email","FirstName", "LastName", "Address1", "Address2", "City", "State", "ZipCode", "Country", "PhoneNumber", "First Name","Last Name", "Address 1", "Address 2", "Zip Code", "Phone Number" };

        public static decimal BounceRateLimit { get; private set; } = 5;

        public static string[] subDomainName = { "ce01","ce02","ce03","ce04","ce05" };

        public static string[] disposableEmailAddress = { "yopmail.com", "finxmail.com", "wemel.top", "maildrop.cc" };


    }
    public enum ManageMessage
    {
        ProfileUpdatedSuccessfully,
        AddPhoneSuccess,
        ChangePasswordSuccess,
        Error
    }

    public enum CampaginStatus
    {
        Draft,
        Sent,
        Schedule,
        Failed,
        Queued
    }

    public enum UserRole
    {
        Admin,
        SalesRep,
        User
    }
}