﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.Web.Extentions;
using EM.Web.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using NMIPayment;
using System.Net;
using RestSharp;
using RestSharp.Authenticators;
using System.Net.Http;
using System.Text;

namespace EM.Web
{

    public class DataHelper
    {
        private static EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        private static SendGridAPIHelper _sendGrid = new SendGridAPIHelper();

        public static SelectList GetCountryList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "United States" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "Country/GetCountry");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var countryViewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CountryViewModel>>(count.ToString());
                SelectList Country = new SelectList(countryViewModel, "CountryName", "CountryName", selectedItem);
                return Country;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static SelectList GetStateList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "California" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "Country/GetStates");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var states = Newtonsoft.Json.JsonConvert.DeserializeObject<List<State>>(count.ToString());
                SelectList stateList = new SelectList(states, "StateName", "StateName", selectedItem);
                return stateList;
            }
            else
            {
                return new SelectList(null);
            }
        }
        public static SelectList GetIndustryList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "Industry");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var countryViewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IndustryViewModel>>(count.ToString());
                SelectList Country = new SelectList(countryViewModel, "IndustryID", "IndustryName", selectedItem);
                return Country;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static SelectList GetQualityofLeadsList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "LeadsQuality");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var countryViewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesPortal_LeadQuality>>(count.ToString());
                SelectList Qualityofleads = new SelectList(countryViewModel, "LeadQualityID", "LeadQualityName", selectedItem);
                return Qualityofleads;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static List<usp_getUserList_Result> GetListWithSubscriberByListId(string id)
        {

            String Api = string.Format("{0}/{1}?id={2}&pageNumber=1&listname=&pageSize=&orderBy=", ConfigurationManager.AppSettings["ApiURL"], "UserList", id);
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var countryViewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<usp_getUserList_Result>>(count.ToString());
                return countryViewModel;
            }
            else
            {
                return new List<usp_getUserList_Result>();
            }
        }
        public static DataTable ReadExcelAsDataTable(string path)
        {
            var hasHeader = true;
            int extindx = path.LastIndexOf('.');
            string ext = path.Substring(extindx + 1, path.Length - extindx - 1);
            if (ext.ToUpper() == "CSV")
            {
                DataTable tbl = ReadCsvFile(path);
                return tbl;
            }
            if (ext.ToUpper() == "XLS")
            {
                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
                DataTable tbl = ConvertXSLXtoDataTable(path, connString);
                return tbl;
            }
            else
            {
                using (var pck = new OfficeOpenXml.ExcelPackage())
                {

                    using (var stream = System.IO.File.OpenRead(path))
                    {
                        pck.Load(stream);
                    }
                    var ws = pck.Workbook.Worksheets.First();
                    DataTable tbl = new DataTable();
                    foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                    {
                        tbl.Columns.Add(hasHeader ? firstRowCell.Text.Trim() : string.Format("Column {0}", firstRowCell.Start.Column));
                    }
                    var startRow = hasHeader ? 2 : 1;
                    for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                    {
                        //var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                        var wsRow = ws.Cells[rowNum, 1, rowNum, tbl.Columns.Count];
                        DataRow row = tbl.Rows.Add();
                        foreach (var cell in wsRow)
                        {
                            row[cell.Start.Column - 1] = cell.Text;
                        }
                    }

                    return tbl;
                }
            }

        }

        #region GET DATA TABLE FROM CSV FILE
        public static DataTable ReadCsvFile(string path)
        {

            DataTable dtCsv = new DataTable();
            string Fulltext;

            // FileUpload1.SaveAs(FileSaveWithPath);
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\r'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = Regex.Split(rows[i], ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                        //string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j].Trim()); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = rowValues[k].ToString();
                                }
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }

            }
            return dtCsv;
        }

        #endregion

        #region Get data from xls file 
        public static DataTable ConvertXSLXtoDataTable(string strFilePath, string connString)
        {
            OleDbConnection oledbConn = new OleDbConnection(connString);
            DataTable dt = new DataTable();
            try
            {

                oledbConn.Open();
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oledbConn))
                {
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    oleda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    oleda.Fill(ds);

                    dt = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
            }
            finally
            {

                oledbConn.Close();
            }

            return dt;

        }
        #endregion

        public static bool ValidateSubscriber(SubscriberViewModel subscriberViewModel)
        {
            // string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            //if (string.IsNullOrEmpty(subscriberViewModel.Email) || !System.Text.RegularExpressions.Regex.IsMatch(subscriberViewModel.Email, pattern))
            //{
            //    return false;
            //}
            try
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,11})+)$");
                if (string.IsNullOrEmpty(subscriberViewModel.Email) || !regex.IsMatch(subscriberViewModel.Email))
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message);
            }
            //if (string.IsNullOrEmpty(subscriberViewModel.FirstName))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(subscriberViewModel.LastName))
            //{
            //    return false;
            //}

            //if (string.IsNullOrEmpty(subscriberViewModel.Address1))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(subscriberViewModel.City))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(subscriberViewModel.State))
            //{
            //    return false;
            //}

            //if (string.IsNullOrEmpty(subscriberViewModel.Country))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(subscriberViewModel.ZipCode))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(subscriberViewModel.PhoneNumber))
            //{
            //    return false;
            //}
            //if (subscriberViewModel.Day == 0 || subscriberViewModel.Day > 31 || subscriberViewModel.Day.ToString().Length > 2)
            //{
            //    return false;
            //}
            //if (subscriberViewModel.Month == 0 || subscriberViewModel.Month > 12 || subscriberViewModel.Month.ToString().Length > 2)
            //{
            //    return false;
            //}
            return true;
        }

        public static bool ValidateList(ListViewModel lViewModel)
        {
            if (string.IsNullOrEmpty(lViewModel.ListName))
            {
                return false;
            }
            if (string.IsNullOrEmpty(lViewModel.FromEmail))
            {
                return false;
            }
            if (string.IsNullOrEmpty(lViewModel.FromName))
            {
                return false;
            }
            //if (string.IsNullOrEmpty(lViewModel.Company))
            //{
            //    return false;
            //}
            string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            if (string.IsNullOrEmpty(lViewModel.FromEmail) || !System.Text.RegularExpressions.Regex.IsMatch(lViewModel.FromEmail, pattern))
            {
                return false;
            }
            //if (string.IsNullOrEmpty(lViewModel.Address1))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(lViewModel.City))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(lViewModel.Country))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(lViewModel.Zip))
            //{
            //    return false;
            //}
            //if (string.IsNullOrEmpty(lViewModel.Phone))
            //{
            //    return false;
            //}
            return true;
        }

        public static List<SubscriberViewModel> GetSubscriberListFromExcel(string filePath, string userId, int? listId)
        {
            try
            {
                var SubsList = ReadExcelAsDataTable(filePath);
                if (!SubsList.Columns.Contains("FirstName"))
                {
                    SubsList.Columns.Add("FirstName");
                }
                if (!SubsList.Columns.Contains("LastName"))
                {
                    SubsList.Columns.Add("LastName");
                }
                if (!SubsList.Columns.Contains("Address1"))
                {
                    SubsList.Columns.Add("Address1");
                }
                if (!SubsList.Columns.Contains("Address2"))
                {
                    SubsList.Columns.Add("Address2");
                }
                if (!SubsList.Columns.Contains("City"))
                {
                    SubsList.Columns.Add("City");
                }
                if (!SubsList.Columns.Contains("State"))
                {
                    SubsList.Columns.Add("State");
                }
                if (!SubsList.Columns.Contains("ZipCode"))
                {
                    SubsList.Columns.Add("ZipCode");
                }
                if (!SubsList.Columns.Contains("Country"))
                {
                    SubsList.Columns.Add("Country");
                }
                if (!SubsList.Columns.Contains("PhoneNumber"))
                {
                    SubsList.Columns.Add("PhoneNumber");
                }
                var ListId = Convert.ToInt32(listId);
                var listSubscriber = (from ls in SubsList.AsEnumerable()
                                      where ls["Email"] != null  //&& ls["FirstName"] != null && ls["LastName"] != null
                                      select new SubscriberViewModel
                                      {
                                          SubscriberId = 0,
                                          ListId = ListId,
                                          FirstName = ls["FirstName"].CheckNullOrEmpty(),
                                          LastName = ls["LastName"].CheckNullOrEmpty(),
                                          Email = /*ls["Email"].CheckNullOrEmpty()*/ls["Email"].ToString().Contains("\n") ? ls["Email"].ToString().Replace("\n", "") : ls["Email"].CheckNullOrEmpty(),
                                          Address1 = ls["Address1"].CheckNullOrEmpty(),
                                          Address2 = ls["Address2"].CheckNullOrEmpty(),
                                          City = ls["City"].CheckNullOrEmpty(),
                                          State = ls["State"].CheckNullOrEmpty(),
                                          Country = ls["Country"].CheckNullOrEmpty(),
                                          PhoneNumber = ls["PhoneNumber"].CheckNullOrEmpty(),
                                          DOB = DateTime.UtcNow,
                                          EmailPermission = true,// ls["EmailPermission"].ToString().ToLower() == "1" ? true : false,
                                          ZipCode = ls["ZipCode"].CheckNullOrEmpty(),
                                          CreatedById = userId
                                      }).Where(x => !string.IsNullOrEmpty(x.Email)).ToList();
                List<SubscriberViewModel> Subscriber = listSubscriber.GroupBy(p => p.Email.ToLower()).Select(g => g.First()).ToList();
                //.Where(x => !string.IsNullOrEmpty(x.FirstName) && !string.IsNullOrEmpty(x.LastName) && !string.IsNullOrEmpty(x.Email) && !string.IsNullOrEmpty(x.City)).ToList();
                return Subscriber;
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
                return new List<SubscriberViewModel>();
            }
        }

        public static List<ListViewModel> GetListFromExcel(string filePath, string userId)
        {
            try
            {
                var listData = ReadExcelAsDataTable(filePath);
                var userList = (from ls in listData.AsEnumerable()
                                select new ListViewModel
                                {
                                    ListId = 0,
                                    ListName = ls["ListName"].CheckNullOrEmpty(),
                                    FromName = ls["FromName"].CheckNullOrEmpty(),
                                    FromEmail = ls["FromEmail"].CheckNullOrEmpty(),
                                    Company = ls["Company"].CheckNullOrEmpty(),
                                    Address1 = ls["Address1"].CheckNullOrEmpty(),
                                    Address2 = ls["Address2"].CheckNullOrEmpty(),
                                    City = ls["City"].CheckNullOrEmpty(),
                                    Phone = ls["Phone"].CheckNullOrEmpty(),
                                    Country = ls["Country"].CheckNullOrEmpty(),
                                    Zip = ls["Zip"].CheckNullOrEmpty(),
                                    CreatedById = userId
                                }).Where(x => !string.IsNullOrEmpty(x.FromName) && !string.IsNullOrEmpty(x.ListName) && !string.IsNullOrEmpty(x.FromEmail)/* && !string.IsNullOrEmpty(x.City)*/).ToList();
                List<ListViewModel> list = userList.GroupBy(p => p.ListName).Select(g => g.First()).ToList();
                return list;
            }
            catch (Exception)
            {
                return new List<ListViewModel>();
            }

        }

        public static double GetDateDiff(DateTime fromDate, DateTime todayDate)
        {
            return (todayDate.Date - fromDate.Date).TotalDays;
        }

        public static bool CheckDuplicateCreditCard(string cardNumber, string userId)
        {
            cardNumber = EM.Helpers.Utilities.MaskCreditCardNo(cardNumber.Trim()).ToString();
            var creditcard = db.UserPaymentProfiles.Where(x => x.CreditCardNo == cardNumber && x.UserId == userId).FirstOrDefault();

            if (creditcard != null)
            {
                return true;
            }

            return false;
        }

        public static IEnumerable<SelectListItem> GetCreditCardYear(int selectedYear)
        {
            List<SelectListItem> yearData = new List<SelectListItem>();
            for (int i = 0; i < 20; i++)
            {
                var data = DateTime.UtcNow.AddYears(i);
                if (data.Year == selectedYear)
                {
                    var item = new SelectListItem { Text = data.Year.ToString(), Value = data.Year.ToString(), Selected = true };
                    yearData.Add(item);

                }
                else
                {
                    var item = new SelectListItem { Text = data.Year.ToString(), Value = data.Year.ToString() };
                    yearData.Add(item);
                }
            }
            return yearData;
        }
        public static IEnumerable<SelectListItem> GetMonths(int selectedMonth)
        {
            return Enumerable.Range(1, 12).Select(x => new SelectListItem
            {
                Value = x.ToString(),
                Text = DateTimeFormatInfo.CurrentInfo.GetMonthName(x),
                Selected = x == selectedMonth ? true : false

            });
        }

        public static SelectList GetPaymentTypeList(string selectedItem)
        {

            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "PaymentType");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var payment = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentTypeViewModel>>(count.ToString());
                SelectList Payment = new SelectList(payment, "PaymentTypeId", "PaymentTypeName", selectedItem);
                return Payment;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static List<ClientType> GetClientTypeList()
        {
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ClientType");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var clientType = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClientType>>(count.ToString());
                return clientType;
            }
            else
            {
                return null;
            }
        }

        public static SelectList GetClientTypeSelectList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ClientType");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var clientType = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClientType>>(count.ToString());
                SelectList Client = new SelectList(clientType, "ClientType1", "ClientType1", selectedItem);
                return Client;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static List<CampaignList> GetCampaignList()
        {
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "CampaignList");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var campaignList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CampaignList>>(count.ToString());
                return campaignList;
            }
            else
            {
                return null;
            }
        }

        public static SelectList GetCampaignSelectList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "CampaignList");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var campaignList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CampaignList>>(count.ToString());
                SelectList Campaign = new SelectList(campaignList, "CampaignTypeId", "CampaignName", selectedItem);
                return Campaign;
            }
            else
            {
                return new SelectList(null);
            }
        }


        public static SelectList GetSalesRep(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "SalesRepData/GetSalesRep");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var salesViewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesRepProfile>>(count.ToString());
                SelectList SalesRep = new SelectList(salesViewModel, "Id", "FirstName", selectedItem);
                return SalesRep;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static List<Plan> GetPlanList()
        {

            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlan");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var planModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Plan>>(count.ToString());
                return planModel;
            }
            else
            {
                return null;
            }
        }

        public static SelectList GetPlanSelectList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlan");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var planModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Plan>>(count.ToString());
                SelectList Plan = new SelectList(planModel, "PlanId", "PlanName", selectedItem);
                return Plan;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static SelectList GetTimeZone(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "TimeZone");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var time = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Factory.TimeZone>>(count.ToString());
                SelectList Time = new SelectList(time, "TimeZoneID", "TimeZoneName", selectedItem);
                return Time;
            }
            else
            {
                return new SelectList(null);
            }
        }


        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static SelectList GetSalesRepList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "SalesRepData/GetSalesRep");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var salesViewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesRepProfile>>(count.ToString());
                SelectList SalesRep = new SelectList(salesViewModel, "FirstName", "FirstName", selectedItem);
                return SalesRep;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static List<Factory.TimeZone> GetTimeZoneList()
        {

            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "TimeZone");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var time = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Factory.TimeZone>>(count.ToString());

                return time;
            }
            else
            {
                return null;
            }
        }

        public static SelectList GetLeadsQuality(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "LeadsQuality");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var qualityModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesPortal_LeadQuality>>(count.ToString());
                SelectList LeadQuality = new SelectList(qualityModel, "LeadQualityID", "LeadQualityName", selectedItem);
                return LeadQuality;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static SelectList GetProviders(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "Providers");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var providersModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesPortal_Providers>>(count.ToString());
                SelectList Providers = new SelectList(providersModel, "ProviderId", "ProviderName", selectedItem);
                return Providers;
            }
            else
            {
                return new SelectList(null);
            }
        }
        public static int InsertRepActivity(Salesportal_AdminRepActivity activityobj)
        {
            var ActivityLog = db.Set<Salesportal_AdminRepActivity>();
            ActivityLog.Add(activityobj);
            int i = db.SaveChanges();
            return i;
        }

        public static int InsertUserActivity(UserActivityLog userActivityObj)
        {
            var ActivityLog = db.Set<UserActivityLog>();
            ActivityLog.Add(userActivityObj);
            int i = db.SaveChanges();
            return i;
        }

        public static IEnumerable<SelectListItem> GetDays(int selectedDays)
        {
            List<SelectListItem> dayData = new List<SelectListItem>();
            for (int i = 1; i < 32; i++)
            {
                var item = new SelectListItem { Text = i.ToString(), Value = i.ToString() };
                dayData.Add(item);

            }
            return dayData;
        }

        public static SelectList GetLeadColor(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "LeadColor");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var colorModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesPortal_lead_Color>>(count.ToString());
                SelectList LeadColor = new SelectList(colorModel, "ColorID", "Color", selectedItem);
                return LeadColor;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static bool CheckDuplicateEmail(string email)
        {
            var Email = db.SalesPortal_Leads_EmailIdList.Where(x => x.Email == email).FirstOrDefault();

            if (Email != null)
            {

                return true;
            }

            return false;
        }

        public static bool CheckDuplicateMobile(string mobile)
        {
            var Mobile = db.SalesPortal_Leads_MobileNumList.Where(x => x.MobileNumber == mobile).FirstOrDefault();

            if (Mobile != null)
            {
                return true;
            }

            return false;
        }

        public static SelectList GetIPPoolList(string selectedItem)
        {
            selectedItem = selectedItem == null ? "Select IP Pool " : selectedItem;
            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPPool");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var IPPoolModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IP_Pool>>(count.ToString());
                SelectList IPPool = new SelectList(IPPoolModel, "Id", "Name", selectedItem);
                return IPPool;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static bool CheckDuplicateProviders(string providerName)
        {
            var provider = db.SalesPortal_Providers.Where(x => x.ProviderName == providerName && x.IsDelete == false).FirstOrDefault();

            if (provider != null)
            {

                return true;
            }

            return false;
        }

        public static List<IP_Pool> GetPoolList()
        {

            String Api = string.Format("{0}/{1}", ConfigurationManager.AppSettings["ApiURL"], "ManageIPPool");
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var IPPool = Newtonsoft.Json.JsonConvert.DeserializeObject<List<IP_Pool>>(count.ToString());
                return IPPool;
            }
            else
            {
                return null;
            }
        }
        public static bool CheckDuplicatePromotion(string promotionCode)
        {
            var promotion = db.PromotionCodes.Where(x => x.PromotionCodeName == promotionCode && x.IsDelete == false).FirstOrDefault();

            if (promotion != null)
            {

                return true;
            }

            return false;
        }

        public static string GetDaySuffix(int day)
        {
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return "st";
                case 2:
                case 22:
                    return "nd";
                case 3:
                case 23:
                    return "rd";
                default:
                    return "th";
            }
        }


        public static usp_getPackageDetail_Result CreateUserPlan(int? SelectedPackakgeId, string userId)
        {
            var _curPlan = db.UserPlans.Where(r => r.UserId == userId && r.IsActive == true).FirstOrDefault();
            if (_curPlan != null)
            {
                _curPlan.IsActive = false;

            }
            string APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", SelectedPackakgeId);
            var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
            if (apiResult != null)
            {
                var planDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(apiResult.ToString());

                UserPlan oPlan = new UserPlan();
                oPlan.UserId = userId;
                oPlan.TotalEmails = planDetail.NoOfCredits;
                oPlan.TotalDays = 30;
                oPlan.PackageId = SelectedPackakgeId.Value;
                oPlan.MonthlyCredit = planDetail.NoOfCredits.ToString();
                oPlan.NoOfSubscribersMin = 0;
                oPlan.NoOfSubscribersMax = planDetail.NoOfSubscribersMax;
                oPlan.PackageUpdateDate = DateTime.Now;
                oPlan.BillingAmount = planDetail.BillingAmount != null ? planDetail.BillingAmount.Value : 0;
                oPlan.totalRemainingCredits = planDetail.NoOfCredits.ToString();
                oPlan.ContactManagement = planDetail.ContactManagement;
                oPlan.UnlimitedEmails = planDetail.UnlimitedEmails;
                oPlan.EmailScheduling = planDetail.EmailScheduling;
                oPlan.EducationalResources = planDetail.EducationalResources;
                oPlan.LiveSupport = planDetail.LiveSupport;
                oPlan.CustomizableTemplates = planDetail.CustomizableTemplates;
                oPlan.IsActive = true;
                oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                oPlan.BillingStatus = 1;
                oPlan.totalSpendCredits = "0";
                db.UserPlans.Add(oPlan);
                db.SaveChanges();
                return planDetail;
            }
            else
            {
                return null;

            }
        }

        public static SelectList GetSubscriberList(int? selectedItem, int? PlanId)
        {
            //selectedItem = selectedItem == null ? "" : selectedItem;
            String Api = string.Format("{0}/{1}?id={2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanPricing", PlanId);
            var count = (new APICallHelper()).Get(Api).Content.ReadAsStringAsync().Result;
            if (count != null)
            {
                var planModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PlanPricingsViewModel>>(count.ToString());
                SelectList Plan = new SelectList(planModel, "Id", "NoOfSubscribersMax", selectedItem);
                return Plan;
            }
            else
            {
                return new SelectList(null);
            }
        }

        //public static async Task<bool> IsVerifiedDomain(string email)
        //{
        //    var host = email.Split('@')[1];
        //    //if (host == "gmail.com" || host == "yahoo.com" || host == "hotmail.com" || host == "rediffmail.com" || host == "outlook.com" || host == "yopmail.com")
        //    if (Array.IndexOf(Constants.whilelabelDomainsArr, host) != -1)
        //    {
        //        return true;
        //    }
        //    var response = _sendGrid.getValidatedDomain(host);
        //    if (response != null)
        //    {
        //        var domainResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DomainAuthenticationViewModel>>(response);
        //        if (domainResponse != null && domainResponse.Count() > 0 && domainResponse.FirstOrDefault().Valid == true)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            if (domainResponse.Count() > 0)
        //            {
        //                var validate = await DataHelper.ValidatedDomain(domainResponse.FirstOrDefault().Id);

        //                return validate;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        //return true;
        //    }
        //    return false;
        //}

        //public static async Task<bool> ValidatedDomain(int Id)
        //{
        //    var response = _sendGrid.postValidateDomain(Id);
        //    if (response != null)
        //    {
        //        var domainResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<DomainValidationViewModel>(response);
        //        if (domainResponse.Valid == true)
        //        {
        //            using (var db = new EmailMarketingDbEntities())
        //            {
        //                var domain = db.VerifiedDomains.Where(x => x.DomainID == domainResponse.Id).FirstOrDefault();
        //                if (domain != null)
        //                {
        //                    //domain.Status = true;
        //                    await db.SaveChangesAsync();
        //                    return true;
        //                }
        //                else
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    return false;
        //}
        public static async Task<bool> domainVerification(string userId, string email, string name)
        {
            var host = email.Split('@')[1];
            string CName = "";
            using (var db = new EmailMarketingDbEntities())
            {
                var domain = db.VerifiedDomains.Where(x => x.Domain == host).FirstOrDefault();
                if (domain != null)
                {
                    CName = domain.CNAME;
                }
                else
                {
                    var response = _sendGrid.postDomainAuthenticate(host);
                    if (response != null)
                    {

                        var domainResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<DomainAuthenticationViewModel>(response);
                        VerifiedDomain model = new VerifiedDomain();
                        // model.DomainID = domainResponse.Id;
                        model.Domain = domainResponse.Domain;
                        model.CNAME = domainResponse.dns.Mail_Cname.host;
                        //model.Status = domainResponse.Valid;
                        CName = domainResponse.dns.Mail_Cname.host;
                        db.VerifiedDomains.Add(model);
                        db.SaveChanges();

                    }
                    else
                    {
                        return false;
                    }
                }

                SendGridEmails sendGrid = new SendGridEmails();
                await sendGrid.SendGridSingleEmail(userId, "CName Details", EmailHtml.getCNameDetails(name, CName), Constants.SupportEmail, Constants.SupportName, email, name);
                return true;
            }

        }

        public static async Task<string> getCNameDetails(string email)
        {
            var host = email.Split('@')[1];
            if (Array.IndexOf(Constants.whilelabelDomainsArr, host) != -1)
            {
                return "";
            }
            using (var db = new EmailMarketingDbEntities())
            {
                var response = db.VerifiedDomains.Where(x => x.Domain == host).FirstOrDefault();
                if (response != null)
                {
                    string CName = response.CNAME;
                    return CName;
                }
                else
                {
                    return "";
                }
            }
        }

        public static async Task<bool> CheckDuplicateSubUser(string username, string email, string password, string[] defaultIP)
        {
            var response = await _sendGrid.GetSubUserList(username);
            if (response != null)
            {
                var subUserResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SubUserViewModel>>(response);
                if (subUserResponse != null && subUserResponse.Count() > 0)
                {
                    return true;
                }
                else
                {
                    var createSubUser = await _sendGrid.createSubUser(email, email, password, defaultIP);
                    if (createSubUser == true)
                    {
                        return true;
                    }
                }

            }
            return false;
        }

        public static SelectList GetIPAddress(string selectedItem)
        {
            selectedItem = selectedItem == null ? "" : selectedItem;
            var response = _sendGrid.getIPAddress();
            if (response != null)
            {

                var ipaddressmodel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SendGridIPAddressViewModel>>(response.ToString());
                SelectList Ip = new SelectList(ipaddressmodel, "ip", "ip", selectedItem);
                return Ip;
            }
            else
            {
                return new SelectList(null);
            }
        }

        public static bool CheckDuplicateTemplate(string templateName, int TemplateId)
        {
            var userId = CurrentUser.getCurUserId();
            if (TemplateId != 0)
            {
                var template = db.Templates.Where(x => x.TemplateName.ToLower() == templateName.ToLower() && x.UserId == userId && x.TemplateId != TemplateId && x.IsDeleted == false).FirstOrDefault();

                if (template != null)
                {
                    return true;
                }

                return false;
            }
            else
            {
                var template = db.Templates.Where(x => x.TemplateName.ToLower() == templateName.ToLower() && x.UserId == userId && x.IsDeleted == false).FirstOrDefault();

                if (template != null)
                {
                    return true;
                }

                return false;
            }
        }

        public static bool CheckDuplicateAdminTemplate(string templateName, int TemplateId)
        {
            var userId = Constants.AdminId;
            if (TemplateId != 0)
            {
                var template = db.Templates.Where(x => x.TemplateName.ToLower() == templateName.ToLower() && x.UserId == userId && x.TemplateId != TemplateId && x.IsDeleted == false).FirstOrDefault();

                if (template != null)
                {
                    return true;
                }

                return false;
            }
            else
            {
                var template = db.Templates.Where(x => x.TemplateName.ToLower() == templateName.ToLower() && x.UserId == userId && x.IsDeleted == false).FirstOrDefault();

                if (template != null)
                {
                    return true;
                }

                return false;
            }
        }

        public static bool CheckDuplicateCampaignName(string CampaignName, int CampaignId, int IsReplicateCase)
        {
            var userId = CurrentUser.getCurUserId();
            if (CampaignId != 0)
            {
                if (IsReplicateCase == 0)
                {
                    var campaign = db.Campaigns.Where(x => x.CamapignName.ToLower() == CampaignName.ToLower() && x.UserId == userId && x.CampaignId != CampaignId).FirstOrDefault();

                    if (campaign != null)
                    {
                        return true;
                    }

                    return false;
                }
                else
                {
                    var campaign = db.Campaigns.Where(x => x.CamapignName.ToLower() == CampaignName.ToLower() && x.UserId == userId && (x.CampaignId == CampaignId || x.CampaignId != CampaignId)).FirstOrDefault();

                    if (campaign != null)
                    {
                        return true;
                    }

                    return false;
                }
            }
            else
            {
                var campaign = db.Campaigns.Where(x => x.CamapignName.ToLower() == CampaignName.ToLower() && x.UserId == userId).FirstOrDefault();

                if (campaign != null)
                {
                    return true;
                }

                return false;
            }
        }

        public static bool CheckDuplicateList(string listName, int listId)
        {
            var userId = CurrentUser.getCurUserId();
            if (listId != 0)
            {
                var list = db.Lists.Where(x => x.ListName.ToLower() == listName.ToLower() && x.CreatedById == userId && x.ListId != listId && x.IsDeleted == false).FirstOrDefault();
                if (list != null)
                {
                    return true;
                }

                return false;
            }
            else
            {
                var list = db.Lists.Where(x => x.ListName.ToLower() == listName.ToLower() && x.CreatedById == userId && x.IsDeleted == false).FirstOrDefault();

                if (list != null)
                {
                    return true;
                }

                return false;
            }
        }

        public static TemplateViewModel SaveTemplateContent(TemplateViewModel templateViewModel, string UserName, string UserId)
        {
            templateViewModel.UserId = UserId;
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            string path = "~/UserUpload/templateImages/" + templateViewModel.UserId;
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(path)))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
            }
            string templateImagePath = path + "/" + EM.Helpers.Utilities.GetRandomNumber() + ".jpg";
            ImageProcessing.ConvertHtmlToImage(templateViewModel.TemplateHtml, HttpContext.Current.Server.MapPath(templateImagePath));
            templateViewModel.TemplatePath = urlHelper.Content(templateImagePath);

            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Template");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(templateViewModel)).StatusCode;
            if (result == System.Net.HttpStatusCode.OK)
            {
                //------------SaveBeeTemplateContent Activity Log-------------//
                var getCurrentUserDetails = CurrentUser.getCurUserDetails();
                UserActivityLog userActivtyObj = new UserActivityLog();
                userActivtyObj.UserId = templateViewModel.UserId;
                userActivtyObj.Createdby = templateViewModel.CreatedById;
                userActivtyObj.Description = templateViewModel.TemplateName + " Bee Template created by " + UserName;
                userActivtyObj.DateTime = DateTime.Now;
                var res = DataHelper.InsertUserActivity(userActivtyObj);
                //------------------------------------------//
                return templateViewModel;
            }
            else
            {
                return templateViewModel;
            }
        }

        public static string ApplicationUrl()
        {
            var appPath = string.Empty;
            appPath = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.Url.Port == 80 ? string.Empty : ":" + HttpContext.Current.Request.Url.Port);
            return appPath;
        }

        public static bool getPaymentStatus(string code)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                var oTrans = db.AnTransactionLogs.Where(r => r.TransCode == code).SingleOrDefault();
                if (oTrans != null && oTrans.StatusCode.Trim() == "100")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static HttpStatusCode SaveTransactionDetails(PaymentStatus paymentStatus, string tokenId)
        {
            AnTransactionLog transLog = new AnTransactionLog();
            transLog.AnTransactionId = paymentStatus.transactionid;
            transLog.TransactionDate = DateTime.Now;
            transLog.CreatedBy = paymentStatus.TransCreatedBy;
            transLog.Message = "approval Code: " + tokenId;
            transLog.TransactionType = "Credit Card";
            transLog.Amount = paymentStatus.amount;
            transLog.CreatedDate = DateTime.Now;
            transLog.UserId = paymentStatus.CustomField;
            transLog.StatusCode = paymentStatus.resultcode;
            transLog.StatusText = paymentStatus.resulttext;
            transLog.TransCode = paymentStatus.TransCode;
            transLog.PaymentProfileId = paymentStatus.customervaultid;
            String APIURL = string.Format("{0}/{1}", Constants.ApiURL, "TransactionLog");
            var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(transLog)).StatusCode;
            return result;
        }
        public static void updateTransactionDetails(PaymentStatus paymentStatus, string tokenId)
        {
            AnTransactionLog transLog = db.AnTransactionLogs.Where(r => r.TransCode == tokenId).FirstOrDefault();
            if (transLog != null)
            {
                transLog.AnTransactionId = paymentStatus.transactionid;
                transLog.TransactionDate = DateTime.Now;
                transLog.StatusCode = paymentStatus.resultcode;
                transLog.StatusText = paymentStatus.resulttext;
                db.SaveChanges();
            }
        }
        public static usp_getTransactionInfo_Result GetTransactionInfo(string token)
        {
            var _transaction = db.usp_getTransactionInfo(token).FirstOrDefault();
            return _transaction;
        }
        public static string GetTransactionStatus(string token)
        {
            var _transaction = db.AnTransactionLogs.FirstOrDefault(r => r.TransCode == token);
            if (_transaction != null)
            {
                return _transaction.StatusCode.ToString();
            }
            else
            {
                return null;
            }
        }
        public static Invoice SaveInvoiceDetails(PaymentStatus paymentStatus)
        {
            Invoice invoiceModel = new Invoice();
            invoiceModel.InvoiceID = invoiceModel.InvoiceID;
            invoiceModel.UserId = paymentStatus.CustomField;
            invoiceModel.AmountCharged = paymentStatus.amount;
            invoiceModel.OrderID = paymentStatus.OrderId;
            invoiceModel.CardNumber = paymentStatus.CCNumber;
            invoiceModel.CardType = paymentStatus.CustomField3;
            invoiceModel.ExpiresMonth = paymentStatus.CCEXp.Substring(0, 2);
            invoiceModel.ExpiresYear = DateTime.Now.Year.ToString().Substring(0, 2) + paymentStatus.CCEXp.Substring(2, 2);
            invoiceModel.NameOnCard = paymentStatus.FirstName + paymentStatus.LastName;
            invoiceModel.Phone = paymentStatus.Phone;
            invoiceModel.Address = paymentStatus.Address1;
            invoiceModel.City = paymentStatus.City;
            invoiceModel.State = paymentStatus.State;
            invoiceModel.Zip = paymentStatus.Zip;
            invoiceModel.CreatedById = paymentStatus.CustomField;
            invoiceModel.CreatedDate = DateTime.Now;
            invoiceModel.AnTransactionId = paymentStatus.transactionid;
            invoiceModel.InvoiceType = paymentStatus.OrderDesc;
            db.Invoices.Add(invoiceModel);
            db.SaveChanges();
            return invoiceModel;
        }




        public static usp_getJoinSubscriberIds_Result Getsubscriberjoinids(string ListId)
        {
            using (var context = new EmailMarketingDbEntities())
            {
                context.Database.CommandTimeout = 180;
                var model = context.usp_getJoinSubscriberIds(ListId).SingleOrDefault();
                return model;
            }
        }

        public static List<SubscriberViewModel> GetUsersSuppressedSubcriber(List<SubscriberViewModel> subscriberViewModels)
        {
            string APIURL2 = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/CheckSuppressedSubscriber");
            var result1 = (new APICallHelper()).Post(APIURL2, JsonConvert.SerializeObject(subscriberViewModels));
            var SuppressedSubscribers = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result1.Content.ReadAsStringAsync().Result.ToString());
            return SuppressedSubscribers;
        }


        public static string TimeZoneDetails()
        {
            using (var context = new EmailMarketingDbEntities())
            {
                var userId = CurrentUser.getCurUserId();
                var user_Details = context.UserProfiles.Where(x => x.Id == userId).FirstOrDefault();
                var timeZone_Details = context.TimeZones.Where(x => x.TimeZoneID == user_Details.TimeZoneId).FirstOrDefault();
                if (timeZone_Details != null)
                {
                    return timeZone_Details.TimeZoneAbbr;
                }
                else
                {
                    return null;
                }
            }
        }

        public static void CleanExcel(string path)
        {
            try
            {
                if (System.IO.File.Exists(path))
                {

                    System.IO.File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
            }
        }

        public static List<SubscriberViewModel> CheckSubscriberListFromExcel(string filePath, string userId, int? listId)
        {
            try
            {
                var SubsList = ReadExcelAsDataTable(filePath);
                string[] headersValue = SubsList.Columns.Cast<DataColumn>()
                               .Select(x => x.ColumnName)
                               .ToArray();
                List<SubscriberViewModel> model = new List<SubscriberViewModel>();
                SubscriberViewModel subscriberListmodel = new SubscriberViewModel();

                if (!headersValue.Contains("Email"))
                {
                    subscriberListmodel.invalidEmail = true;
                    model.Add(subscriberListmodel);
                    return model;
                }
                foreach (var item in headersValue)
                {
                    if (Array.IndexOf(Constants.validHeaders, item) == -1)
                    {

                        
                        subscriberListmodel.countInvalidheader = true;
                        SubsList.Columns.Remove(item);
                        //subscriberListmodel.invalidHeaders = item;
                        model.Add(subscriberListmodel);
                    }
                }

                
                if (SubsList.Columns.Count == 0)
                {
                    return model;
                }
                //string[] headersValue = SubsList.Columns.Cast<DataColumn>()
                //                 .Select(x => x.ColumnName)
                //                 .ToArray();
                //foreach (var item in headersValue)
                //{
                //    if (Array.IndexOf(Constants.validHeaders, item) == -1)
                //    {
                //        List<SubscriberViewModel> model = new List<SubscriberViewModel>();
                //        SubscriberViewModel subscriberListmodel = new SubscriberViewModel();
                //        subscriberListmodel.countInvalidheader = true;
                //        model.Add(subscriberListmodel);
                //        return model;
                //    }
                //}
                var validHeaders = SubsList.Columns.Cast<DataColumn>()
                              .Select(x => x.ColumnName)
                              .ToArray();
                var header = string.Join(", ", from item in validHeaders select item);
                if (!SubsList.Columns.Contains("FirstName") && !SubsList.Columns.Contains("First Name"))
                {
                    SubsList.Columns.Add("FirstName");
                }
                if (!SubsList.Columns.Contains("FirstName") && SubsList.Columns.Contains("First Name"))
                {
                    SubsList.Columns["First Name"].ColumnName = "FirstName";
                }
                if (!SubsList.Columns.Contains("LastName") && !SubsList.Columns.Contains("Last Name"))
                {
                    SubsList.Columns.Add("LastName");
                }
                if (!SubsList.Columns.Contains("LastName") && SubsList.Columns.Contains("Last Name"))
                {
                    SubsList.Columns["Last Name"].ColumnName = "LastName";
                }
                if (!SubsList.Columns.Contains("Address1") && !SubsList.Columns.Contains("Address 1"))
                {
                    SubsList.Columns.Add("Address1");
                }
                if (!SubsList.Columns.Contains("Address1") && SubsList.Columns.Contains("Address 1"))
                {
                    SubsList.Columns["Address 1"].ColumnName = "Address1";
                }
                if (!SubsList.Columns.Contains("Address2") && !SubsList.Columns.Contains("Address 2"))
                {
                    SubsList.Columns.Add("Address2");
                }
                if (!SubsList.Columns.Contains("Address2") && SubsList.Columns.Contains("Address 2"))
                {
                    SubsList.Columns["Address 2"].ColumnName = "Address2";
                }
                if (!SubsList.Columns.Contains("City"))
                {
                    SubsList.Columns.Add("City");
                }
                if (!SubsList.Columns.Contains("State"))
                {
                    SubsList.Columns.Add("State");
                }
                if (!SubsList.Columns.Contains("ZipCode") && !SubsList.Columns.Contains("Zip Code"))
                {
                    SubsList.Columns.Add("ZipCode");
                }
                if (!SubsList.Columns.Contains("ZipCode") && SubsList.Columns.Contains("Zip Code"))
                {
                    SubsList.Columns["Zip Code"].ColumnName = "ZipCode";
                }

                if (!SubsList.Columns.Contains("Country"))
                {
                    SubsList.Columns.Add("Country");
                }
                if (!SubsList.Columns.Contains("PhoneNumber") && !SubsList.Columns.Contains("Phone Number"))
                {
                    SubsList.Columns.Add("PhoneNumber");
                }
                if (!SubsList.Columns.Contains("PhoneNumber") && SubsList.Columns.Contains("Phone Number"))
                {
                    SubsList.Columns["Phone Number"].ColumnName = "PhoneNumber";
                }

                var ListId = Convert.ToInt32(listId);
                var listSubscriber = (from ls in SubsList.AsEnumerable()
                                      where ls["Email"] != null
                                      select new SubscriberViewModel
                                      {
                                          SubscriberId = 0,
                                          ListId = ListId,
                                          FirstName = ls["FirstName"].CheckNullOrEmpty(),
                                          LastName =  ls["LastName"].CheckNullOrEmpty(),
                                          Email = ls["Email"].ToString().Contains("\n") ? ls["Email"].ToString().Replace("\n", "") : ls["Email"].CheckNullOrEmpty(),
                                          Address1 = ls["Address1"].CheckNullOrEmpty(),
                                          Address2 = ls["Address2"].CheckNullOrEmpty(),
                                          City = ls["City"].CheckNullOrEmpty(),
                                          State = ls["State"].CheckNullOrEmpty(),
                                          Country = ls["Country"].CheckNullOrEmpty(),
                                          PhoneNumber =ls["PhoneNumber"].CheckNullOrEmpty(),
                                          DOB = DateTime.UtcNow,
                                          EmailPermission = true,
                                          ZipCode = ls["ZipCode"].CheckNullOrEmpty(),
                                          CreatedById = userId
                                      }).Where(x => !string.IsNullOrEmpty(x.Email)).ToList();
                List<SubscriberViewModel> Subscriber = listSubscriber.GroupBy(p => p.Email.ToLower()).Select(g => g.First()).ToList();
                SubscriberImportResponseViewModel subscriberErrorResponseViewModel = new SubscriberImportResponseViewModel();
                List<SubscriberViewModel> subscriberViewModels = new List<SubscriberViewModel>();
                List<SubscriberViewModel> inValidSubscriberViewModels = new List<SubscriberViewModel>();
                foreach (var item in Subscriber)
                {
                    try
                    {
                        var isValid = DataHelper.ValidateSubscriber(item);
                        //var isValid = MailgunHelper.GetValidateSubscriber(item.Email);
                        if (isValid)
                        {
                            item.CreatedById = CurrentUser.getCurUserDetails().Id;
                            if (item.Email.Length < 250)
                            {
                                subscriberViewModels.Add(item);
                            }
                        }
                        else
                        {
                            inValidSubscriberViewModels.Add(item);
                        }
                    }
                    catch (Exception EX)
                    {
                        CreateLog.Log(EX.Message);
                    }
                }
                subscriberErrorResponseViewModel.InvalidSubscribers = inValidSubscriberViewModels;
                subscriberViewModels.FirstOrDefault().validHeaders = header;
                if (subscriberViewModels.Count > 0)
                {
                    //string APIURL = string.Format("{0}/{1}", Constants.ApiURL, "Subscriber/NotExistSubscriber");
                    //var result = (new APICallHelper()).Post(APIURL, JsonConvert.SerializeObject(Subscriber));
                    //var response = JsonConvert.DeserializeObject<List<SubscriberViewModel>>(result.Content.ReadAsStringAsync().Result.ToString());
                    return subscriberViewModels;
                }
                else
                {
                    return new List<SubscriberViewModel>();
                }
            }



            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
                return new List<SubscriberViewModel>();
            }
        }

        public static Nullable<int> GetsubscriberCountByListId(string listname, string createdbyId)
        {
            List listM = null;
            List<int> activelist = null;

            activelist = db.Lists.Where(x => x.CreatedById == createdbyId && x.IsDeleted == false).Select(x => x.ListId).ToList();
            var activelists = string.Join(",", activelist);
            //  var activelistSubscriberCount = Getsubscriberjoinids(activelists).SubscriberCount;


            Nullable<int> deletedactiveListSubscriberCount = null;
            using (var context = new EmailMarketingDbEntities())
            {
                listM = db.Lists.Where(x => x.ListName == listname && x.CreatedById == createdbyId && x.IsDeleted == true).FirstOrDefault();
                if (listM != null)
                {
                    var UniqueSubscriber = Getsubscriberjoinids(listM.ListId.ToString() + "," + activelists);
                    deletedactiveListSubscriberCount = UniqueSubscriber.SubscriberCount != null ? UniqueSubscriber.SubscriberCount : 0;

                }
                else
                {
                    deletedactiveListSubscriberCount = 0;
                }
                return deletedactiveListSubscriberCount;
            }
        }

        public static List<string> GetConfirmEmailAddress(string UserId)
        {
            using (var context = new EmailMarketingDbEntities())
            {
                var oEmail = db.EmailConfirmations.Where(x => x.Status == true && x.UserId == UserId).Select(x=>x.FromEmailAddress).ToList();
                return oEmail;
            }
        }

    }
}





