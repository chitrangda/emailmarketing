﻿using EM.Factory;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using EM.Helpers;

namespace EM.Web.Utilities
{
    public class SendGridEmails
    {
        SendGridClient sendGridClient;
        SendGridAPIHelper sendGridapi = new SendGridAPIHelper();

        public SendGridEmails()
        {
            var apiKey = ServiceConstants.SendGridKey;
            sendGridClient = new SendGridClient(apiKey);
        }

        public async Task SendGridMultipleEmail(string UserId, int? CampaignId, string campaignSubject, string templateContentHtml, string fromCampaign, string fromName, List<Contacts> subscribers)
        {
            var oEmail = await CreateMessage(UserId, CampaignId.Value.ToString(), fromCampaign, fromName, subscribers, campaignSubject, templateContentHtml);
            if (oEmail != null)
            {
                try
                {
                    var response = await sendGridClient.SendEmailAsync(oEmail);
                    LogService.WriteToLog(response.StatusCode.ToString(), "Campaign_" + CampaignId);
                    if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                    {
                        await UpdateSentStatus(CampaignId);
                        await UpdateCredits(UserId, subscribers.Count());
                    }
                    else
                    {
                        LogService.WriteToLog(response.Body.ReadAsStringAsync().Result, "Campaign_" + CampaignId);
                    }
                }
                catch (Exception)
                {
                }


            }
            else
            {
                LogService.WriteToLog("Email Message is null", "Campaign_" + CampaignId);
            }
        }

        public async Task SendGridSingleEmail(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            var from = new EmailAddress(senderEmail, senderName);
            var to = new EmailAddress(recipientEmail, recipientName);
            var oEmail = MailHelper.CreateSingleEmail(from, to, subject, null, contentHtml);
            var response = await sendGridClient.SendEmailAsync(oEmail);
            //LogService.WriteToLog(response.StatusCode.ToString(), Id);
        }

        public async Task<SendGridMessage> CreateMessage(string UserId, string CampaignId, string fromEmail, string fromName, List<Contacts> subscribers, string subject, string htmlBody)
        {
            string ipPoolName = null;
            var context = new EmailMarketingDbEntities();
            var userProfile = context.UserProfiles.Find(UserId);
            if (userProfile != null)
            {
                ipPoolName = userProfile.IP_Pool != null ? userProfile.IP_Pool.Name : null;
            }
            string address = userProfile.Address1 + " " + userProfile.City + "<br/>" + userProfile.State + " " + userProfile.Country + ", " + userProfile.Zip;
            var msg = new SendGridMessage();
            msg = AddSubstitutions(msg, subscribers, htmlBody, userProfile.CompanyName, address, CampaignId, UserId);
            msg.From = new EmailAddress(fromEmail, fromName);
            msg.Subject = subject;
            //msg.SetSpamCheck(true);
            if (ipPoolName != null && sendGridapi.getIP(ipPoolName) != false)
            {
                msg.SetIpPoolName(ipPoolName);
            }
            msg.SetClickTracking(true, false);
            msg.HtmlContent = htmlBody;
            msg.AddCategories(new List<string>() { UserId, CampaignId });
            return await Task.FromResult<SendGridMessage>(msg);

        }

        public SendGridMessage AddSubstitutions(SendGridMessage eMessage, List<Contacts> subscribers, string content, string companyName = "", string address = "", string CampaignId = "", string UserId = "")
        {
            //string footerContent = "<div style='background-color: #e6e6e6;line-height:25px'><p style='font-size: 12px; text-align: center;'>To unsubscribe please <a href='" + url + "'>click here </a></p><p style='font-size: 14px; text-align: center;'>" + companyName + "</p><p style='font-size: 14px; text-align: center;'>" + address + "</p></div>";
            //eMessage.SetFooterSetting(true, footerContent, "");
            eMessage.Personalizations = new List<Personalization>();
            foreach (var contact in subscribers)
            {
                try
                {
                    string url = string.Format("{0}Home/Unsubscribe?id={1}&e={2}&uid={3}", Constants.AppUrl, CampaignId, contact.Email, UserId);
                    var name = contact.FirstName + " " + contact.LastName;
                    var personalization = new Personalization();
                    personalization.Tos = new List<EmailAddress>();
                    personalization.Tos.Add(new EmailAddress() { Name = name, Email = contact.Email });
                    personalization.Substitutions = new Dictionary<string, string>();
                    if (content.IndexOf(ServiceConstants.FirstName) != -1)
                    {
                        personalization.Substitutions.Add(ServiceConstants.FirstName, contact.FirstName);
                    }
                    if (content.IndexOf(ServiceConstants.LastName) != -1)
                    {
                        personalization.Substitutions.Add(ServiceConstants.LastName, contact.LastName);
                    }
                    if (content.IndexOf(ServiceConstants.Email) != -1)
                    {
                        personalization.Substitutions.Add(ServiceConstants.Email, contact.Email);
                    }
                    if (content.IndexOf(ServiceConstants.PhoneNo) != -1)
                    {
                        personalization.Substitutions.Add(ServiceConstants.PhoneNo, contact.PhoneNo);
                    }
                    //personalization.Substitutions.Add("%tag%", contact.Email);
                    personalization.Substitutions.Add("%tag2%", url);
                    eMessage.Personalizations.Add(personalization);
                }
                catch (Exception)
                {

                }
            }
            return eMessage;
        }

        public async Task UpdateSentStatus(int? campaginId)
        {
            using (var context = new EmailMarketingDbEntities())
            {
                var campagin = await context.Campaigns.FindAsync(campaginId);
                if (campagin != null)
                {
                    campagin.Status = CampaginStatus.Sent.ToString();
                    campagin.SentDate = DateTime.Now;
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task UpdateCredits(string UserId, int totalSendEmails)
        {
            using (var context = new EmailMarketingDbEntities())
            {
                var userPlan = context.UserPlans.Where(r => r.UserId == UserId && r.IsActive==true).FirstOrDefault();
                if (userPlan != null)
                {
                    userPlan.totalSpendCredits = Convert.ToString(Convert.ToInt32(userPlan.totalSpendCredits) + totalSendEmails);
                    userPlan.totalRemainingCredits = Convert.ToString(userPlan.TotalEmails.Value - totalSendEmails);
                    await context.SaveChangesAsync();
                }
            }
        }

        public async Task SendGridSingleEmailInvoice(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            var from = new EmailAddress(senderEmail, senderName);
            var to = new EmailAddress(recipientEmail, recipientName);
            var oEmail = MailHelper.CreateSingleEmail(from, to, subject, null, contentHtml);
            oEmail.AddBcc(System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"], System.Configuration.ConfigurationManager.AppSettings["InvoiceName"]);
            var response = await sendGridClient.SendEmailAsync(oEmail);
            //LogService.WriteToLog(response.StatusCode.ToString(), Id);
        }

        public void SendGridSubscriberEmailSync(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            try
            {
                var from = new EmailAddress(senderEmail, senderName);
                var to = new EmailAddress(recipientEmail, recipientName);
                var oEmail = MailHelper.CreateSingleEmail(from, to, subject, null, contentHtml);
                sendGridClient.SendEmailAsync(oEmail).Wait();
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
            }

            //LogService.WriteToLog(response.StatusCode.ToString(), Id);
        }

        public void SendGridMultipleSubscriberEmailSync(string subject, string contentHtml, string senderEmail, string senderName, List<Contacts> subscribers)
        {
            var oEmail = CreateSubscriberMessage(senderEmail, senderName, subscribers, subject, contentHtml);
            if (oEmail != null)
            {

                try
                {
                   sendGridClient.SendEmailAsync(oEmail).Wait();
                }
                catch (Exception ex)
                {
                    CreateLog.ActionLog(ex.Message);
                }
            }
            else
            {
            }

        }

        public  SendGridMessage CreateSubscriberMessage(string fromEmail, string fromName, List<Contacts> subscribers, string subject, string htmlBody)
        {
            var msg = new SendGridMessage();
            msg = AddSubscriberSubstitutions(msg, subscribers, htmlBody);
            msg.From = new EmailAddress(fromEmail, fromName);
            msg.Subject = subject;
            msg.HtmlContent = htmlBody;
            return msg;

        }
        public SendGridMessage AddSubscriberSubstitutions(SendGridMessage eMessage, List<Contacts> subscribers, string content)
        {
            eMessage.Personalizations = new List<Personalization>();
            foreach (var contact in subscribers)
            {
                try
                {
                    if (contact.FirstName != null)
                    {
                        var name = contact.FirstName + " " + contact.LastName;
                    }
                    var personalization = new Personalization();
                    personalization.Tos = new List<EmailAddress>();
                    personalization.Tos.Add(new EmailAddress() { Name = contact.Email, Email = contact.Email });
                    personalization.Substitutions = new Dictionary<string, string>();
                    if (content.IndexOf(ServiceConstants.FirstName) != -1)
                    {
                        personalization.Substitutions.Add(ServiceConstants.FirstName, contact.FirstName);
                    }
                    eMessage.Personalizations.Add(personalization);
                }
                catch (Exception)
                {

                }
            }
            return eMessage;
        }



    }

    public class Contacts
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }



    }

   
}