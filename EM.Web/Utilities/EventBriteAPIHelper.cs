﻿using EM.Factory;
using EM.Web.Areas.User.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Configuration;
using EM.Helpers;

namespace EM.Web.Utilities
{
    public class EventBriteAPIHelper
    {
        public static string APIkey = ConfigurationManager.AppSettings["EventBriteAPIkey"];
        public static string ClientSecret = ConfigurationManager.AppSettings["EventBriteClientSecret"];
        public static string PrivateToken = ConfigurationManager.AppSettings["EventBritePrivateToken"];
        public static string PublicToken = ConfigurationManager.AppSettings["EventBritePublicToken"];

        public static string UserAccessToken { get; set; }

        public EventBriteAPIHelper()
        {

        }

        public static string getUserAccessToken(string redirectUri, string accessCode)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbrite.com/oauth/token");
            RestRequest request = new RestRequest();
            request.AddParameter("grant_type", "authorization_code");
            request.AddParameter("client_id", APIkey);
            request.AddParameter("client_secret", ClientSecret);
            request.AddParameter("code", accessCode);
            request.AddParameter("redirect_uri", redirectUri);

            request.Method = Method.POST;
            //var response = client.Execute(request);
            var response = JsonConvert.DeserializeObject<EventBriteTokenViewModel>(client.Execute(request).Content);
            UserAccessToken = response.access_token;
            return response.access_token;


        }

        public static string getOrgnization(string UserAccessToken, string OrgId)
        {
            var Id = CurrentUser.getCurUserId();
            EmailMarketingDbEntities db = new EmailMarketingDbEntities();
            var EventbriteCode = db.Eventbrite_User_AccessCode.FirstOrDefault(x => x.UserId == Id);
            if (EventbriteCode != null && EventbriteCode.OrgID != null)
            {
                return EventbriteCode.OrgID;

            }
            else
            {
                RestClient client = new RestClient();
                client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/users/me/organizations?token=" + UserAccessToken);
                RestRequest request = new RestRequest();
                request.Method = Method.GET;
                var response = client.Execute(request);
                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    var oOrganization = JsonConvert.DeserializeObject<OrganizationViewModel>(response.Content);
                    if (oOrganization != null && oOrganization.organizations.Count() > 0)
                    {

                        return oOrganization.organizations.FirstOrDefault().id;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }

        }

        public static EventViewModel getEvents(string orgId, string UserAccessToken)
        {
            if (UserAccessToken == null)
            {
                var Id = CurrentUser.getCurUserId();
                using (var db = new EmailMarketingDbEntities())
                {
                    var EventbriteCode = db.Eventbrite_User_AccessCode.FirstOrDefault(x => x.UserId == Id);
                    if (EventbriteCode != null)
                    {
                        UserAccessToken = EventbriteCode.AccessCode;
                    }
                }
            }
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/organizations/" + orgId + "/events/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.Method = Method.GET;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                var oEvent = JsonConvert.DeserializeObject<EventViewModel>(response.Content);
                if (oEvent.events != null && oEvent.events.Count() > 0)
                {
                    if (oEvent.pagination.hasmoreitems == true)
                    {
                        var pageCount = oEvent.pagination.pagecount;
                        var pageNumber = oEvent.pagination.pagenumber + 1;
                        while (pageCount >= pageNumber)
                        {
                            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/organizations/" + orgId + "/events/");
                            RestRequest request1 = new RestRequest();
                            request1.AddHeader("Authorization", "Bearer " + UserAccessToken);
                            request1.AddQueryParameter("page", pageNumber.ToString());
                            request1.Method = Method.GET;
                            var res = client.Execute(request1);
                            if (res != null && res.StatusCode == HttpStatusCode.OK)
                            {
                                var oEventList = JsonConvert.DeserializeObject<EventViewModel>(res.Content);
                                if (oEventList != null && oEventList.events.Count() > 0)
                                {
                                    oEvent.events.AddRange(oEventList.events);
                                }
                            }
                            pageNumber++;
                        }

                        return oEvent;
                    }
                    else
                    {
                        return oEvent;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static AttendeesViewModel getEventAttendes(string eventId)
        {

            if (UserAccessToken == null)
            {
                var Id = CurrentUser.getCurUserId();
                using (var db = new EmailMarketingDbEntities())
                {
                    var EventbriteCode = db.Eventbrite_User_AccessCode.FirstOrDefault(x => x.UserId == Id);
                    if (EventbriteCode != null)
                    {
                        UserAccessToken = EventbriteCode.AccessCode;
                    }
                }
            }
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(" https://www.eventbriteapi.com/v3/events/" + eventId + "/attendees/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.Method = Method.GET;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                var oAttendees = JsonConvert.DeserializeObject<AttendeesViewModel>(response.Content);
                if (oAttendees != null && oAttendees.attendees.Count() > 0)
                {
                    if (oAttendees.pagination.hasmoreitems == true)
                    {
                        var pageCount = oAttendees.pagination.pagecount;
                        var pageNumber = oAttendees.pagination.pagenumber + 1;
                        while (pageCount >= pageNumber)
                        {
                            client.BaseUrl = new Uri(" https://www.eventbriteapi.com/v3/events/" + eventId + "/attendees/");
                            RestRequest request1 = new RestRequest();
                            request1.AddHeader("Authorization", "Bearer " + UserAccessToken);
                            request1.AddQueryParameter("page", pageNumber.ToString());
                            request1.Method = Method.GET;
                            var res = client.Execute(request1);
                            if (res != null && res.StatusCode == HttpStatusCode.OK)
                            {
                                var oAttendeesList = JsonConvert.DeserializeObject<AttendeesViewModel>(res.Content);
                                if (oAttendeesList != null && oAttendeesList.attendees.Count() > 0)
                                {
                                    oAttendees.attendees.AddRange(oAttendeesList.attendees);
                                }
                            }
                            pageNumber++;
                        }
                        return oAttendees;
                    }
                    else
                    {
                        return oAttendees;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static EventbrieUserInfoViewModel getUserInfo(string UserAccessToken)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/users/me/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.Method = Method.GET;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                var oUserInfo = JsonConvert.DeserializeObject<EventbrieUserInfoViewModel>(response.Content);
                if (oUserInfo != null)
                {
                    return oUserInfo;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static EventViewModel getEventsWithAttendeeCount(string orgId, string UserAccessToken)
        {
            if (UserAccessToken == null || orgId==null)
            {
                var Id = CurrentUser.getCurUserId();
                using (var db = new EmailMarketingDbEntities())
                {
                    var EventbriteCode = db.Eventbrite_User_AccessCode.FirstOrDefault(x => x.UserId == Id);
                    if (EventbriteCode != null)
                    {
                        UserAccessToken = EventbriteCode.AccessCode;
                        orgId = EventbriteCode.OrgID;
                    }
                }
            }
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/organizations/" + orgId + "/events/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.Method = Method.GET;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                var oEvent = JsonConvert.DeserializeObject<EventViewModel>(response.Content);
                if (oEvent.events != null && oEvent.events.Count() > 0)
                {
                    if (oEvent.pagination.hasmoreitems == true)
                    {
                        var pageCount = oEvent.pagination.pagecount;
                        var pageNumber = oEvent.pagination.pagenumber + 1;
                        while (pageCount >= pageNumber)
                        {
                            client.BaseUrl = new Uri(" https://www.eventbriteapi.com/v3/organizations/" + orgId + "/events/");
                            RestRequest request1 = new RestRequest();
                            request1.AddHeader("Authorization", "Bearer " + UserAccessToken);
                            request1.AddQueryParameter("page", pageNumber.ToString());
                            request1.Method = Method.GET;
                            var res = client.Execute(request1);
                            if (res != null && res.StatusCode==HttpStatusCode.OK)
                            {
                                var oEventList = JsonConvert.DeserializeObject<EventViewModel>(res.Content);
                                if (oEventList != null && oEventList.events.Count() > 0)
                                {
                                    oEvent.events.AddRange(oEventList.events);
                                }
                            }
                            pageNumber++;
                        }

                    }
                    foreach (var item in oEvent.events)
                    {
                        client.BaseUrl = new Uri(" https://www.eventbriteapi.com/v3/events/" + item.id + "/attendees/");
                        RestRequest request2 = new RestRequest();
                        request2.AddHeader("Authorization", "Bearer " + UserAccessToken);
                        request2.Method = Method.GET;
                        var resp = client.Execute(request2);
                        if (resp != null && resp.StatusCode == HttpStatusCode.OK)
                        {
                            var oAttendees = JsonConvert.DeserializeObject<AttendeesViewModel>(resp.Content);
                            if (oAttendees != null)
                            {
                                item.capacity = oAttendees.pagination.objectcount;

                            }
                        }
                    }
                    return oEvent;

                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static void Insert_EventbriteWebhookLink(string UserAccessToken, string OrgId)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/organizations/" + OrgId + "/webhooks/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.AddParameter("endpoint_url", "https://www.api.cheapestemail.com/api/EventbriteEventCreated");
            request.AddParameter("actions", "event.created,event.updated");
            //request.AddParameter("event_id", "");

            request.Method = Method.POST;
            var Response = client.Execute(request);
            if (Response.StatusCode != HttpStatusCode.OK)
            {
                CreateLog.Log(Response.StatusDescription + " " + Response.Content, null);

            }

            request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.AddParameter("endpoint_url", "https://www.api.cheapestemail.com/api/EventbriteAttendeeUpdated");
            request.AddParameter("actions", "attendee.updated");
            //request.AddParameter("event_id", "");
            request.Method = Method.POST;

            Response = client.Execute(request);
            if (Response.StatusCode != HttpStatusCode.OK)
            {
                CreateLog.Log(Response.StatusDescription + " " + Response.Content, null);

            }
        }


    }

    public class EventBriteTokenViewModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}
