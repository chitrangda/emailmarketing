﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Models
{
    public class MailgunRecipientModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNo { get; set; }

        public string Email { get; set; }
    }
}