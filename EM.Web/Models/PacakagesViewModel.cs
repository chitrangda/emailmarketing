﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Models
{
    public class PacakagesViewModel
    {
        public List<usp_geAllPlans_Result> packagesList = new List<usp_geAllPlans_Result>();
        public List<Plan> planList = new List<Plan>();
    }
}