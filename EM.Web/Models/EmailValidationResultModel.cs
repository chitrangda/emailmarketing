﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Web.Models
{
    public class EmailValidationResultModel
    {
        public string address { get; set; }

        public bool is_disposable_address { get; set; }

        public bool is_role_address { get; set; }

        public string[] reason { get; set; }

        public string result { get; set; }

        public string risk { get; set; }

    }
}