﻿var dColumnsDef = [];
var columns =
    [

        {
            data: "AccountName", "title": "Account Name", "autoWidth": true, render: function (data, type, row) {
                return "<a class='waves-effect waves-light' href='/SalesRep/Dashboard/AccountDetails/" + row.Id + "'>" + row.AccountName + "(" + row.Email + ")" + "</a><br/><a href='/User/Dashboard/Index/" + row.Id + "'>Login</a>";
            }
        },
        { "data": "MemberType", "title": "Member Type", "name": "MemberType", "autoWidth": true },
        { "data": "ReferralCode", "title": "Referral Code", "name": "ReferralCode", "autoWidth": true },
        { "data": "CampaignType", "title": "Campaign Type", "name": "CampaignType", "autoWidth": true },
        { "data": "totalSpendCredits", "title": "Spent Total", "name": "totalSpendCredits", "autoWidth": true },
        {
            data: "IsActive", "title": "Account Status", render: function (data, type, row) {
                if (row.IsActive == 1) {
                    return "Active"
                }
                else {
                    return "InActive";
                }
            }
        },
        { "data": "RegisteredDate", "title": "Date Register", "name": "RegisteredDate", "autoWidth": true },
        { "data": "FollowUpDate", "title": "Follow Up", "name": "FollowUpDate", "autoWidth": true },
        { "data": "SentToday", "title": "Sent Today", "name": "SentToday", "autoWidth": true },
        { "data": "Contacts", "title": "Contacts", "name": "Contacts", "autoWidth": true },
        { "data": "MonthlyCredit", "title": "Monthly credits", "name": "MonthlyCredit", "autoWidth": true },
        { "data": "BillingAmount", "title": "MONTH Bill", "name": "BillingAmount", "autoWidth": true },
        { "data": "NextPackageUpdateDate", "title": "Next Bill", "name": "NextPackageUpdateDate", "autoWidth": true },
        {
            data: "BillingStatus", "title": "Billing Status", render: function (data, type, row) {
                if (row.BillingStatus == 1) {
                    return "Active"
                }
                else {
                    return "InActive";
                }
            }
        }

    ];

$(document).ready(function (e) {
    $("#chkDates").change(function () {
        enableDatesControls();
    });
    $("#chkLocation").change(function () {
        enableLocationControls();
    });
    $('#dataTab').addClass("active");
    bindSalesGrid();
});

function bindSalesGrid() {
    InitDataTable($("#DataTable"), "/SalesRep/Data/GetData", null, columns, dColumnsDef, [[0, "desc"]], null, true, true);
}

function searchData() {
    // 
    var zip = $("#txtZip").val();
    if (!$("#chkLocation").attr('checked')) {
        if (!emptyStringValidator($("#txtZip")) && !zipcodeValidator($("#txtZip"))) {
            alert("Invalid zip code.");
            $("#txtZip").focus();
            return false;
        }
    }

    if (chkDates() != false) {
        showLoader();
        var dtFromDate = null, dtToDate = null, year, quarter, month, states, timeZone, city, zip, spackage, accountStatus, accountType, campaignType;
        if (!$("#chkDates").is(":checked")) {
            dtFromDate = $("#txtFrom").val().trim() == "" ? null : $("#txtFrom").val().trim();
            dtToDate = $("#txtTo").val().trim() == "" ? null : $("#txtTo").val().trim();

        }
        year = $("#ddlYears").val().trim() == "" ? null : $("#ddlYears").val();
        quarter = $("#ddlQuarter").val().trim() == "" ? null : $("#ddlQuarter").val();
        month = $("#ddlMonths").val().trim() == "" ? null : $("#ddlMonths").val();
        states = $("#ddlStates").val().trim() == "" ? null : $("#ddlStates").val();
        timeZone = $("#ddlTimeZone").val().trim() == "" ? null : $("#ddlTimeZone").val().trim();
        city = $("#txtCity").val().trim() == "" ? null : $("#txtCity").val().trim();
        zip = $("#txtZip").val() == "" ? null : $("#").val().trim();
        spackage = $("#ddlPackage").val().trim() == "" ? null : $("#ddlPackage").val().trim();
        accountStatus = $("#ddlAcType").val().trim() == "" ? null : $("#ddlAcType").val().trim();
        accountType = $("#ddlAccount").val().trim() == "" ? null : $("#ddlAccount").val().trim();
        campaignType = $("#ddlCampaignType").val().trim() == "" ? null : $("#ddlCampaignType").val().trim();
        var param = JSON.stringify({ "FromDate": dtFromDate, "ToDate": dtToDate, "Year": year, "Quarter": quarter, "Month": month, "City": city, "State": states, "Zip": zip, "TimeZone": timeZone, "Find": accountType, "MemberType": spackage, "Status": accountStatus, "CampaignType": campaignType });
        InitDataTable($("#DataTable"), "/SalesRep/Data/SearchData", param, columns, dColumnsDef, [[0, "desc"]], 0, true, true);
    }
}

function chkDates() {
    var isValid = true;
    if (!$("#chkDates").is(":checked")) {
        if ($("#rbDate").is(":checked")) {
            if ($("#txtFrom").val().trim() == '') {
                alert('Please select start date.');
                isValid = false;
            }
        }
        if ($("#rbYears").is(":checked")) {
            if ($("#ddlYears").val() == "") {
                alert('Please select year.');
                $("#ddlYears").focus();
                isValid = false;
            }
            
        }
    }
    else {
        isValid = true;
    }
    return isValid;
}

function enableDatesControls() {
    if ($("#chkDates").is(":checked")) {
        $("#rbDate").prop("checked", false);
        $("#txtFrom").prop("disabled", true);
        $("#txtTo").prop("disabled", true);
        $("#ddlYears").prop("disabled", true);
        $("#ddlQuarter").prop("disabled", true);
        $("#ddlMonths").prop("disabled", true);

    }
    else {
        $("#rbDate").prop("checked", true);
        $("#txtFrom").prop("disabled", false);
        $("#txtTo").prop("disabled", false);
        $("#ddlYears").prop("disabled", false);
        $("#ddlQuarter").prop("disabled", false);
        $("#ddlMonths").prop("disabled", false);

    }
}
function enableLocationControls() {
    if ($("#chkLocation").is(":checked")) {
        $("#ddlStates").prop("disabled", true);
        $("#txtCity").prop("disabled", true);
        $("#txtZip").prop("disabled", true);
        $("#ddlTimeZone").prop("disabled", true);
        $("#ddlAccount").prop("disabled", true);

    }
    else {
        $("#ddlStates").prop("disabled", false);
        $("#txtCity").prop("disabled", false);
        $("#txtZip").prop("disabled", false);
        $("#ddlTimeZone").prop("disabled", false);
        $("#ddlAccount").prop("disabled", false);


    }
}