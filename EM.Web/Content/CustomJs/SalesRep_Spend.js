﻿$(document).ready(function (e) {
    $('#spendActive').addClass("active");

    bindSalesRepSpend();
    $("#NewSearchCheck").click(function () {
        if (this.checked) {
            $("#searchenable").removeAttr("disabled");
            $("#searchenable").focus();
        }
        else {
            $("#searchenable").attr("disabled", "disabled");
            $("#searchenable").val("");
        }
    }


    );
    $("#searchenable").attr("disabled", "disabled");
    $("#searchenable").val("");

});


function bindSalesRepSpend(startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText) {
    var dColumnsDef =
        [];
    var columns =
        [
            { "data": "SaleRepName", "title": "Account Rep", "name": "SaleRepName", "autoWidth": true },
            {

                data: null, "title": "Account Name", render: function (data, type, row) {
                    return "<a class='waves-effect waves-light' href='/Admin/ManageUser/Index/" + row.UserID + "'>" + row.AccountName + "(" + row.ContactEmail + ")" + "</a><br/><a href='/User/Dashboard/Index/" + row.UserID + "'>Login</a>";
                }
            },
            { "data": "ClientType", "title": "Client Type", "name": "ClientType", "autoWidth": true },
            { "data": "MemberType", "title": "Member Type", "name": "MemberType", "autoWidth": true },
            { "data": "PromotionCode", "title": "Promotion Code", "name": "PromotionCode", "autoWidth": true },
            { "data": "SpentTotal", "title": "Spent Total", "name": "SpentTotal", "autoWidth": true },
            {

                data: null, "title": "Account Status", render: function (data, type, row) {
                    if (row.IsActive == 1) {
                        return "Active";
                    }
                    else {
                        return "InActive";
                    }
                }
            },
            { "data": "RegisteredDate", "title": "Date Registered", "name": "RegisteredDate", "autoWidth": true },
            { "data": "ContactEmail", "title": "Email", "name": "ContactEmail", "autoWidth": true },
            { "data": "MobilePhone", "title": "Mobile#", "name": "MobilePhone", "autoWidth": true },
            { "data": "January", "title": "January", "name": "January", "autoWidth": true },
            { "data": "February", "title": "February", "name": "February", "autoWidth": true },
            { "data": "March", "title": "March", "name": "March", "autoWidth": true },
            { "data": "April", "title": "April", "name": "April", "autoWidth": true },
            { "data": "May", "title": "May", "name": "May", "autoWidth": true },
            { "data": "June", "title": "June", "name": "June", "autoWidth": true },
            { "data": "July", "title": "July", "name": "July", "autoWidth": true },
            { "data": "August", "title": "August", "name": "August", "autoWidth": true },
            { "data": "September", "title": "September", "name": "September", "autoWidth": true },
            { "data": "October", "title": "October", "name": "October", "autoWidth": true },
            { "data": "November", "title": "November", "name": "November", "autoWidth": true },
            { "data": "December", "title": "December", "name": "December", "autoWidth": true },

        ];
    startDate = $("#startDay").val().trim() == "" ? null : $("#startDay").val();
    endDate = $("#endDay").val().trim() == "" ? null : $("#endDay").val();
    startMonth = $("#startMonth").val().trim() == "" ? null : $("#startMonth").val();
    endMonth = $("#endMonth").val().trim() == "" ? null : $("#endMonth").val();
    startYear = $("#startYear").val().trim() == "" ? null : $("#startYear").val().trim();
    endYear = $("#endYear").val().trim() == "" ? null : $("#endYear").val().trim();
    SearchText = $("#searchenable").val() == "" ? null : $("#searchenable").val().trim();
    var param = JSON.stringify({
        "startDate": startDate, "endDate": endDate, "startMonth": startMonth, "endMonth": endMonth, "startYear": startYear, "endYear": endYear, "SearchText": SearchText
    })
    InitDataTable($("#SalesRepSpendTable"), "/SalesRep/Spend/getSalesRepSpendData/", param, columns, dColumnsDef, [[0, "desc"]], 0, false, true);
    BindSpendSalesRepTotal("/SalesRep/Spend/getSalesRepSpendData/", param);

}
function BindSpendSalesRepTotal(url, urlParam) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        datatype: "json",
        data: urlParam,
        success: function (response) {
            if (response.length > 0) {
                $("#spendSalesRepExport").removeAttr('disabled');
                $("#spendSalesRepExport").css('opacity', 'initial');
            }
        },
        failure: function (failureData) {


        },
        error: function (errorData) {

            //alert(errorData);
        }
    });
}


function validateSearchSpend() {
    var result = true;
    var startDate = $("#startDay").val();
    var endDate = $("#endDay").val();
    var startMonth = $("#startMonth").val();
    var endMonth = $("#endMonth").val();
    var startYear = $("#startYear").val();
    var endYear = $("#endYear").val();
    var SearchText = $("#searchenable").val();
    var YearDiff = (endYear - startYear);

    if (NewSearchCheck.checked) {
        if (SearchText == '') {
            $('#searchenable').focus();
            $('#searchError').html('Enter Client Name');
            result = false;
        }
        else {
            $('#searchError').empty();
        }
    }
    if (startMonth == '') {
        $('#startMonth').focus();
        $('#startMonthError').html('Please select Start Month');
        result = false;
    }
    else {
        $('#startMonthError').empty();

    }
    if (endMonth == '') {
        $('#endMonth').focus();
        $('#endMonthError').html('Please select End Month');

        result = false;
    }
    else if (YearDiff == 0 && parseInt(startMonth) > parseInt(endMonth)) {
        $('#endMonth').focus();
        $('#endMonthError').html('Please Select Months in Increasing Order');
        result = false;
    }
    else {
        $('#endMonthError').empty();
    }

    if (startYear == '') {
        $('#startYear').focus();
        $('#startYearError').html('Please select Start Year');
        result = false;
    }
    else {
        $('#startYearError').empty();
    }

    if (endYear == '') {
        $('#endYear').focus();
        $('#endYearError').html('Please select End Year');
        result = false;
    }

    else if (parseInt(endYear) != parseInt(startYear)) {
        $('#endYear').focus();
        $('#endYearError').html('Please Select Same Start and End Year');
        result = false;
    }
    else if (parseInt(startYear) > parseInt(endYear)) {
        $('#endYear').focus();
        $('#endYearError').html('End Year should be greater than Start Year');
        result = false;
    }
    else if (YearDiff > 1 || YearDiff == 1 && parseInt(endMonth) >= parseInt(startMonth)) {
        $('#endYear').focus();
        $('#endYearError').html('Choose range for 1 Year  Only');

        result = false;
    }
    else {
        $('#endYearError').empty();
    }

    if (startDate == '') {
        $('#startDay').focus();
        $('#startDayError').html('Please select Start Day');

        result = false;
    }
    else {
        $('#startDayError').empty();

    }
    if (endDate == '') {
        $('#endDay').focus();
        $('#endDayError').html('Please select End Day');
        result = false;
    }

    else
        if (parseInt(startDate) > parseInt(endDate)) {
            $('#endDay').focus();
            $('#endDayError').html('End Day should be greater than Start Day');
            result = false;
        }
        else {
            $('#endDayError').empty();
        }
    if (result == true) {
        return true;
    }
    return result;
}

function SearchSalesRepSpend() {
    if (validateSearchSpend() == true) {
        var startDate = $("#startDay").val();
        var endDate = $("#endDay").val();
        var startMonth = $("#startMonth").val();
        var endMonth = $("#endMonth").val();
        var startYear = $("#startYear").val();
        var endYear = $("#endYear").val();
        var SearchText = $("#searchenable").val();
        bindSalesRepSpend(startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText);
    }

}

function ExportSalesRepSpend() {
    var startDate = $("#startDay").val();
    var endDate = $("#endDay").val();
    var startMonth = $("#startMonth").val();
    var endMonth = $("#endMonth").val();
    var startYear = $("#startYear").val();
    var endYear = $("#endYear").val();
    var SearchText = $("#searchenable").val();
    location.href = '/SalesRep/Spend/ExportSalesRep?startDate=' + startDate + '&endDate=' + endDate + '&startMonth=' + startMonth + '&endMonth=' + endMonth + '&startYear=' + startYear + '&endYear=' + endYear + '&SearchText=' + SearchText;
}

$("#searchenable").autocomplete({
    minLength: 2,
    source: function (request, response) {
        var SearchText = $("#searchenable").val();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/SalesRep/Spend/AutoFillUserName",
            data: '{"prefix": "' + SearchText + '"}',
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    response($.map(data, function (item) {
                        return {
                            label: item.FirstName,
                            val: item.FirstName
                        }
                    }));
                }
                else {
                    response([{ label: 'No Clients Found', val: -1 }]);
                    $('#searchenable').val('');
                }
            },
            error: function (result) {
                alert("Error");
            }
        });
    },
    //select: function (event, ui) {
    //    if (ui.item.val == -1) {
    //        return false;
    //    }
    //}
});


