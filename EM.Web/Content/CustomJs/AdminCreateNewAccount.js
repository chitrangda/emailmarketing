﻿function onAddUserBegin() {
    var result = true;

    if (!emptyStringValidator($('#txtUserEmail'))) {
        $('#txtUserEmail').focus();
        $("#eError").html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtUserEmail'))) {
            $('#txtUserEmail').focus();
            $("#eError").html("Invalid Email");
            result = false;
        }
        else {
            $("#eError").empty();
        }
    }


    if (!emptyStringValidator($('#txtUserPassword'))) {
        $('#txtUserPassword').focus();
        $('#pError').html("Password is required");
        result = false;
    }
    else {

        if (!PasswordValidator($('#txtUserPassword'))) {
            $('#txtUserPassword').focus();
            $('#pError').html("Your password must contain at least 6 chars, one uppercase, 1 numeric and 1 special character.");
            result = false;
        }
        else
        {
            $('#pError').empty();
        }
    }
    if (!emptyStringValidator($('#FirstName')))
    {
        $('#FirstName').focus();
        $("#fError").html("First Name is required");
        result = false;
    }
    else
    {
        $("#fError").empty();
    }
    if (!emptyStringValidator($('#CompanyName'))) {
        $('#CompanyName').focus();
        $("#cmError").html("Company Name is required");
        result = false;
    }
    else {
        $("#cmError").empty();
    }
    if (result == true)
    {
        showLoader();
    }
    return result;
}

function onAddUserSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", redirectToDashboard);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddUserFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

$(document).ready(function (e) {
    BindCountry();
    $('#newAccountActive').addClass("active");
    $('#MobilePhone').usPhoneFormat({
        format: '(xxx) xxx-xxxx',
    })
})

function bindState(CountryName) {
    if (CountryName != "United States") {
        $("#State").hide();
        $("#txtState").show();
        $("#txtState").attr('name', 'State')
    }
    else {
        $("#State").show();
        $("#txtState").attr('name', 'notState')
        $("#txtState").hide();
    }
}

function redirectToDashboard() {
    window.location.reload(true);
    window.location.href = "/Admin/Dashboard";
}

