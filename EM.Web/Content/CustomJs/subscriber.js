﻿var selectedList = [];

//Validation for List Create
var isValidEmailAddress = true;
function onAddSubscriberBegin() {
    var result = true;
    if (!emptyStringValidator($('#Email'))) {
        $('#Email').focus();
        $("#EmailError").html("Email is required.");
        result = false;
    }

    else if (!EmailValidator($('#Email'))) {
        $('#Email').focus();
        $("#EmailError").html("Invalid Email");
        result = false;
    }

    else if (!emailValidate($('#Email'))) {
        $('#Email').focus();
        $("#EmailError").html("Please don't use srmtechsol or stplinc domain");
        result = false;
    }
    //else if (!isValidEmailAddress) {
    //    $('#Email').focus();
    //    $("#EmailError").html("Invalid Email");
    //    result = false;

    //}
    else {
        $("#EmailError").empty();
    }

    if (result == true) {
        showLoader();
    }
    return result;
}

function onAddSubscriberSucess(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "success", false);
    LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));

}

function onAddSubscriberFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function SelectAllCheckBox(input) {
    //$(".SelectedCheckBox").click();
    selectedList = [];
    if ($(input).is(":checked") == true) {
        $(".SelectedCheckBox").each(function () {
            selectedList.push($(this).val());
            $(this).prop("checked", true);
        });
    }
    else {
        $(".SelectedCheckBox").each(function () {
            $(this).prop("checked", false);
            selectedList = [];
        });
    }
}


function DeleteSubscriber() {
    var control = $(".SelectedCheckBox");
    var subscriberId = [];
    $.each(control, function (index, value) {
        if (value.checked) {
            subscriberId.push(value.value);
        }
    });

    if (subscriberId.length > 0) {
        bootbox.confirm({
            message: "Are you sure want to delete this subscriber?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                HideLoader();
                if (result) {
                    AJAXCaller("User", "Subscriber", "DeleteSubscriber", subscriberId, $("#ListId").val());
                } else {
                    bootbox.hideAll();
                    //  LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));

                }
            }
        });

    }
    else {
        GenerateAlert("Please select to delete the subscriber!", "", "info", false);
    }

}

function AJAXCaller(AreaName, ControllerName, ActionName, data, listid) {
    showLoader();
    var ListId = listid;
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data, "ListId": ListId },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            if (result.status) {
                if (ActionName == 'RecoverDeleteSubscriber') {
                    HideLoader();
                    GenerateAlert(result.msg, "", "success", false);
                    LoadSubscriberHTML("User", "Subscriber", "ViewDeletedSubscriber", ListId, null, decodeURI(getUrlVars()["name"]));

                }
                else {
                    HideLoader();
                    GenerateAlert(result.msg, "", "success", false);
                    LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", ListId, null, decodeURI(getUrlVars()["name"]));

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadSubscriberHTML(AreaName, ControllerName, ActionName, listId, subscriberid, listName) {
    showLoader();

    var url = "";
    if (ActionName == "ViewSubscriber") {
        var PageNumber = $("#PageNumber").val();
        url = `/${AreaName}/${ControllerName}/${ActionName}?listId=${listId}&Page=${PageNumber}&listName=${listName}`;
    }
    else {
        //if (ActionName == "AddEditSubscriber") {
        url = `/${AreaName}/${ControllerName}/${ActionName}?listId=${listId}&subscriberId=${subscriberid}&listName=${listName}`;
    }

    $('#Month').val("");
    $('#Day').val("");
    $(".RequiredField").hide();
    $.ajax({
        url: url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SubscriberPage(PageControl, page, TotalRecord) {
    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }



    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function PaggingSubscriber(direction) {
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    SubscriberPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#SortSubscriber').find('option:selected').val();
    getUserSubsvriberGridView($("#PageNumber").val(), Order);
}

function SortSubscriber(input) {
    showLoader();
    var PageNumber = $("#PageNumber").val().trim();
    var sortOrder = $(input).find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    getUserSubsvriberGridView(PageNumber, sortOrder);
}

function getUserSubsvriberGridView(PageNumber, sortOrder, search) {
    var ListId = $("#ListId").val();
    $.ajax({
        url: '/User/Subscriber/getSubscriberListGridView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ listId: ListId, Page: PageNumber, order: sortOrder, Search: search }),
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#gridSubscriberList").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function redirectToUpgradePlan() {
    showLoader();
    window.location.href = "/User/Dashboard/Upgrade";
}

function ImportSubscriberList() {
    showLoader();
    var data = new FormData();
    var files = $("#SubscriberSheet").get(0).files;

    if (files.length > 0) { data.append("subscriberList", files[0]); }
    else {
        GenerateAlert('Please select the file to upload.', '', 'warning', HideLoader);
        return false;
    }
    var extension = $("#SubscriberSheet").val().split('.').pop().toUpperCase();
    if (extension != "XLS" && extension != "XLSX" && extension != "CSV") {
        GenerateAlert('Invalid file format, please upload the excel file', '', 'warning', HideLoader);
        return false;
    }
    $.ajax({
        url: '/User/Subscriber/ImportSubscriberList?id=' + $("#ListId").val(),
        type: "POST",
        processData: false,
        async: true,
        data: data,
        dataType: 'json',
        contentType: false,
        success: function (response) {
            $("#SubscriberSheet").val('');
            if (response.status) {

                GenerateAlertWithHTML(response.msg, "", "success");
                LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));
            }
            else {
                GenerateAlert(response.msg, "", "error", false);
                if (response.msg != "" && (response.msg.indexOf("Maximum") != -1)) {
                    GenerateAlertWithHTMLCustom("", response.msg, "error", "Upgrade Now", redirectToUpgradePlan);
                }
                else {
                    GenerateAlertWithHTML("", response.msg, "error", false);
                    LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));
                }

                HideLoader();
            }
        },
        error: function (er) {
            HideLoader();
        }

    });
}

function getImportedsubsciberView(FileURL, listId, listName) {
    $.ajax({
        url: '/User/Subscriber/SaveImportSheetSubscribers/',
        type: "GET",
        data: { "param1": FileURL, "param2": listId, "ListName": listName },
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function (response) {
            if (response) {
                $("#ContainerDiv").empty().html(response);
                HideLoader();
            }
            else {
                HideLoader();
            }

        },
        error: function (er) {
            HideLoader();

        }

    });
}

function SaveImportSubscriberList(url, listid) {
    showLoader();
    //var ExcelFilePath = $("#ExcelFilePath").val();
    $.ajax({
        url: '/User/Subscriber/SaveImportSheetSubscribers',
        type: "POST",
        data: JSON.stringify({ "param1": url, "param2": listid }),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response.status) {

                GenerateAlertWithHTML(response.msg, UploadResponseContent(response), "success");
                //GenerateAlertWithHTML("Import Successful", "", "success");
                LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", listid, null, decodeURI(getUrlVars()["name"]));
            } else {
                //GenerateAlert("", "error");
                //GenerateAlertWithHTML("", UploadResponseContent(response), "error");
                GenerateAlert(response.msg, "", "error", false);
                LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", listid, null, decodeURI(getUrlVars()["name"]));
            }

        },
        error: function (er) {
            HideLoader();
        }

    });
}

function UploadResponseContent(response) {
    var excelRecord = response.Content;
    var responseTable = "";
    if (excelRecord.DuplicateSubscribers != null && excelRecord.DuplicateSubscribers.length > 0) {
        responseTable += "<table class='' border='1' style='min-width:435px;'><thead><tr><th>Email</th><th>Name</th><th>Status</th></tr></thead><tbody>";
        for (var i = 0; i < excelRecord.DuplicateSubscribers.length; i++) {
            responseTable += `<tr><td>${excelRecord.DuplicateSubscribers[i].Email}</td><td>${excelRecord.DuplicateSubscribers[i].FirstName} ${excelRecord.DuplicateSubscribers[i].LastName}</td><td style='color:green'>Already exists!</td></tr>`;
        }
        responseTable += "</tbody></html>";
    }
    return responseTable;
}

function DeleteAll(ListId) {
    bootbox.confirm({
        message: "Are you sure want to delete all subscribers?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {

                $.ajax({
                    url: '/User/Subscriber/DeleteAllSubscriber/',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    data: JSON.stringify({ listId: ListId }),
                    async: true,
                    success: function (result) {
                        var data = $.parseJSON(result);
                        if (data.status) {
                            GenerateAlert(data.msg, "", "success", false);
                            LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            }
            else {
                LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));
            }
        }
    });
}

function setSubsribeStatus(SubscriberId, IsSubsribe, page, ListId) {

    var _msg = '';
    if (IsSubsribe) {
        _msg = "Are you sure, you want to subscribe this?"
    }
    else {
        _msg = "Are you sure, you want to unsubscribe this?"
    }
    bootbox.confirm({
        message: _msg,
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: '/User/Subscriber/SetSubsribeStatus/',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "html",
                    data: JSON.stringify({ SubscriberId: SubscriberId, IsSubsribe: IsSubsribe, ListId: ListId }),
                    async: true,
                    success: function (result) {
                        HideLoader();
                        var data = $.parseJSON(result);
                        if (data.status) {
                            GenerateAlert(data.msg, "", "success", false);
                            if (page == "subscriber") {
                                LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));
                            }
                            else {
                                LoadUnSubscriberHTML("User", "Subscriber", "ViewUnSubscriber", $("#ListId").val(), decodeURI(getUrlVars()["name"]))
                            }
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        HideLoader();
                    }
                });
            }
            else {
                return;

            }
        }
    });
}

function SearchSubscriber(oControl) {
    showLoader();
    var Page = $("#PageNumber").val().trim();
    var search = $(oControl).val().trim();
    getUserSubsvriberGridView(Page, "NameAsc", search)
}

function RecoverDeleteSubscriber(a) {

    var control = $(".SelectedCheckBox");
    var subscriberId = [];
    if (a == undefined) {
        $.each(control, function (index, value) {
            if (value.checked) {
                subscriberId.push(value.value);
            }
        });
    }
    else {
        subscriberId.push(a);
    }
    if (subscriberId.length > 0) {
        var APIURL = $("#APIURL").val();
        var UserId = $("#UserId").val();
        var URL = APIURL + `UserInfo/GetAuthorizationInfo?UserId=${UserId}`;
        $.ajax({
            url: URL,
            type: "Get",
            dataType: 'json',
            success: function (response) {
                var totalRecoveredRecords = response.TotalSubscriberCreated + subscriberId.length;
                if (response.MaxSubscriber >= totalRecoveredRecords) {
                    bootbox.confirm({
                        message: "Are you sure, you want to restore this subscriber?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            HideLoader();
                            if (result) {
                                AJAXCaller("User", "Subscriber", "RecoverDeleteSubscriber", subscriberId, $("#ListId").val());
                            } else {
                                // GenerateAlert("Your subscriber is safe", "", "success", false);
                                LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), null, decodeURI(getUrlVars()["name"]));

                            }
                        }
                    });
                }
                else {
                    HideLoader();
                    GenerateAlert("You have exhausted your limit. To recover records,please upgarde your plan", "", "info", false);
                }

            }
        });
    }
    else {
        GenerateAlert("Please select to restore the subscriber!", "", "info", false);
    }

}

function SortDeletedSubscriber(input) {
    showLoader();
    var PageNumber = $("#PageNumber").val().trim();
    var sortOrder = $(input).find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    getDeletedUserSubscriberGridView(PageNumber, sortOrder);
}

function SearchDeletedSubscriber(oControl) {
    showLoader();
    var Page = $("#PageNumber").val().trim();
    var search = $(oControl).val().trim();
    getDeletedUserSubscriberGridView(Page, "NameAsc", search)
}

function getDeletedUserSubscriberGridView(PageNumber, sortOrder, search) {

    var ListId = $("#ListId").val();

    $.ajax({
        url: '/User/Subscriber/getDeletedSubscriberListGridView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ listId: ListId, Page: PageNumber, order: sortOrder, Search: search }),
        async: true,
        success: function (result) {

            if (result) {
                HideLoader();
                $("#gridSubscriberList1").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function PaggingDeletedSubscriber(direction) {
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    SubscriberPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#SortSubscriber').find('option:selected').val();
    getDeletedUserSubscriberGridView($("#PageNumber").val(), Order);

}

function bindState(CountryName) {
    if (CountryName != "United States") {
        $("#State").hide();
        $("#txtState").show();
        $("#txtState").attr('name', 'State')
    }
    else {
        $("#State").show();
        $("#txtState").attr('name', 'notState')
        $("#txtState").hide();
    }
}

function validateEmailAddress() {
    showLoader();
    $.ajax({
        url: '/User/Subscriber/validateEmailAddress/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ email: $('#Email').val().trim() }),
        async: true,
        success: function (result) {
            HideLoader();
            if (result == "false") {
                isValidEmailAddress = false;
                return false;
            }
            else {
                isValidEmailAddress = true;
                return true;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function selectchk() {
    selectedList = [];
    $(".SelectedCheckBox").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
        }
    });
    if ($(".SelectedCheckBox").length == selectedList.length) {
        $("#MasterCheckbox").prop("checked", true);
    }
    else {
        $("#MasterCheckbox").prop("checked", false);
    }
}

