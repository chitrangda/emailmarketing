﻿
$(document).ready(function (e) {
    $('#billingActive').addClass("active");
    bindUserGrid();
});

function bindUserGrid(fromDate, toDate) {
    var dColumnsDef =
        [
            {

                "targets": [0],
                "visible": true,
                "searchable": false,
                "orderable": false,
                "className": 'text-center'
            }
        ];
    var columns =
        [
            {
                data: null, "title": "Action", render: function (data, type, row) {

                    var clickFuncton = 'getEditUpcomingBillingModal("' + data.id + '")';

                    return '<a href="#!" onclick=\'' + clickFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a>';


                }
            },

            {

                data: null, "title": "Account Name", render: function (data, type, row) {
                    return "<a class='waves-effect waves-light' href='/Admin/ManageUser/Index/" + row.id + "'>" + row.accountname +"(" + row.email + ")" + "</a><br/><a href='/User/Dashboard/Index/" + row.id + "'>Login</a>";
                }
            },
            { "data": "membertype", "title": "Member Type", "name": "membertype", "autoWidth": true },
            { "data": "referralcode", "title": "Referral Code", "name": "referralcode", "autoWidth": true },
            { "data": "spenttotal", "title": "Spent Total", "name": "spenttotal", "autoWidth": true },
            {

                data: null, "title": "Account Status", render: function (data, type, row) {
                    if (row.isactive == 1) {
                        return "Active";
                    }
                    else {
                        return "InActive";
                    }
                }
            },
            //{ "data": "isactive", "title": "Account Status", "name": "isactive", "autoWidth": true },
            { "data": "registereddate", "title": "Registered Date", "name": "registereddate", "autoWidth": true },
            { "data": "followup", "title": "Follow Up", "name": "followup", "autoWidth": true },
            { "data": "senttoday", "title": "Sent Today", "name": "senttoday", "autoWidth": true },
            { "data": "contacts", "title": "Contacts", "name": "contacts", "autoWidth": true },
            { "data": "monthlycredit", "title": "Monthly credits", "name": "monthlycredit", "autoWidth": true },
            { "data": "monthlybilling", "title": "MNTH Bill", "name": "monthlybilling", "autoWidth": true },
            //{ "data": "reload", "title": "Reload", "name": "reload", "autoWidth": true },
            { "data": "nextbillingdate", "title": "Next Bill", "name": "nextbillingdate", "autoWidth": true },
            { "data": "chargeddate", "title": "CHRG DATE", "name": "chargeddate", "autoWidth": true },
            //{ "data": "billingstatus", "title": "Billing Status", "name": "billingstatus", "autoWidth": true }
            {

                data: null, "title": "Billing Status", render: function (data, type, row) {
                    if (row.billingstatus == 1) {
                        return "Active";
                    }
                    else {
                        return "InActive";
                    }
                }
            },
        ];

    var param = JSON.stringify({ "dateFrom": fromDate, "dateTo": toDate })
    InitDataTable($("#billingTable"), "/Admin/UpcomingBilling/getUpcomingBilling/", param, columns, dColumnsDef, [[0, "desc"]],0, false, false);
}

function SearchNB(ocontrol1, ocontrol2) {
    var DateFrom = $(ocontrol1).val();
    var DateTo = $(ocontrol2).val();
    bindUserGrid(DateFrom, DateTo);

}

function getEditUpcomingBillingModal(id) {
    showLoader();
    $.ajax({
        url: "/Admin/UpcomingBilling/GetUpcomingBillingDetails",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "id": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#EditUpcomingBillingModal").html(response);         
            $('#editUpcomingModal').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onBegin() {
    showLoader();

}

function onSuccess(data) {
    HideLoader();
    $('#editUpcomingModal').modal('hide');
    if (data.status != "Error") {
        GenerateAlert("", data.msg, "success", bindUserGrid);
    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onFailure(data) {

    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}


function GetSubscriberBillingList()
{
    showLoader();
    var id = $("#BillingDetails_PlanId").val();
    var APIURL = $("#APIURL").val();
    var URL = APIURL + 'EmailPlans/GetPlanPricing?id=' + id;
    $.ajax({
        url: URL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $("#BillingDetails_PackageId").empty();
                $.each(response, function (index, value) {
                    $("#BillingDetails_PackageId").append($("<option></option>").val(value.Id).html(value.NoOfSubscribersMax));
                });
                GetPackageDetailBilling();
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function GetPackageDetailBilling ()
{
    showLoader();
    var url = $("#APIURL").val();
    var id = $('#BillingDetails_PackageId').val();
    var UserId = $('#BillingDetails_id').val();
    var APIURL = url + 'EmailPlans/GetPlanByID?id=' + id + '&UserId=' + UserId;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": APIURL,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        if (response != null && response != "") {
            HideLoader();
            if (response.Country == "Canada") {
                $("#BillingDetails_monthlybilling").val(response.BillingAmount_Canada);
                if (response.NoOfCredits == 0) {
                    $("#BillingDetails_monthlycredit").val("Unlimited Emails");
                }
                else {
                    $("#BillingDetails_monthlycredit").val(response.NoOfCredits);
                }
            }
            else {
                $("#BillingDetails_monthlybilling").val(response.BillingAmount);
                if (response.NoOfCredits == 0) {
                    $("#BillingDetails_monthlycredit").val("Unlimited Emails");
                }
                else {
                    $("#BillingDetails_monthlycredit").val(response.NoOfCredits);
                }
            }
        }
    });
}