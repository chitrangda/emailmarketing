﻿var arrCampaignId = [];
function UserDashboardSweetAlert() {
    if (StatusMessage != null && StatusMessage != "") {
        GenerateAlert(StatusMessage, "", "success");
        var uri = window.location.toString();
        if (uri.indexOf("?") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("?"));
            window.history.replaceState({}, document.title, clean_uri);
        }
    }
}

$(document).ready(function (e) {
    $.each($("#tblCampaign tr"), function () {
        arrCampaignId.push($(this).find("td:first").find($("input")).val());
    })
    getEmailStatusChart();
    //getCampignStats();
    getAllEmailStatusChart();    
})

function getCampignStats() {

    if (arrCampaignId.length != 0) {
        $.ajax({
            type: "POST",
            url: "/User/Dashboard/getCampignStats/",
            data: JSON.stringify({
                "campaignIds": arrCampaignId.join(), "startDate": startDate
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result && result != "Error") {
                    var campignStatsJson = $.parseJSON(result);
                    if (campignStatsJson) {
                        $.each(campignStatsJson, function (item, value) {
                            $.each(value.stats, function (innerItem, campignStatsValue) {
                                var ctl_subscriber = $("#subscriber_" + campignStatsValue.name);
                                if (ctl_subscriber) {
                                    var sum = parseInt(ctl_subscriber.html()) + parseInt(campignStatsValue.metrics.requests);
                                    ctl_subscriber.html(sum);
                                }
                                var ctl_sent = $("#sent_" + campignStatsValue.name);
                                if (ctl_sent) {
                                    var sum = parseInt(ctl_sent.html()) + parseInt(campignStatsValue.metrics.delivered);
                                    ctl_sent.html(sum);
                                }
                                var ctl_delivered = $("#delivered_" + campignStatsValue.name);
                                if (ctl_delivered) {
                                    var sum = parseInt(ctl_delivered.html()) + parseInt(campignStatsValue.metrics.delivered);
                                    ctl_delivered.html(sum);
                                }
                                var ctl_openCount = $("#opentCount_" + campignStatsValue.name);
                                if (ctl_openCount) {
                                    var sum = parseInt(ctl_openCount.html()) + parseInt(campignStatsValue.metrics.unique_opens);
                                    ctl_openCount.html(sum);
                                }
                                var ctl_clckCount = $("#clickCount_" + campignStatsValue.name);
                                if (ctl_clckCount) {
                                    var sum = parseInt(ctl_clckCount.html()) + parseInt(campignStatsValue.metrics.unique_clicks);
                                    ctl_clckCount.html(sum);
                                }
                                var ctl_bounce_ = $("#bounce_" + campignStatsValue.name);
                                if (ctl_bounce_) {
                                    var sum = parseInt(ctl_bounce_.html()) + parseInt(campignStatsValue.metrics.bounces);
                                    ctl_bounce_.html(sum);
                                }
                                var ctl_dropped_ = $("#dropped_" + campignStatsValue.name);
                                if (ctl_dropped_) {
                                    var sum = parseInt(ctl_dropped_.html()) + parseInt(campignStatsValue.metrics.bounce_drops);
                                    ctl_dropped_.html(sum);
                                }
                            })
                        })
                    }
                }
            }
        })
    }

}

function SubscriberDetail(PageControl, page, TotalRecord) {
    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    }
    else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }
    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    }
    else {
        $("#Next").removeAttr('disabled');

    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    }
    else {
        $("#Back").removeAttr('disabled');

    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function SubPagging(direction, Text) {
    var PageNumber = $("#PageNumber").val().trim();
    if (PageNumber != 'undefined' && PageNumber != "") {
        if (direction == "Next") {
            var Page = parseInt(PageNumber) + 1;
        } else {
            var Page = parseInt(PageNumber) - 1;
        }
    }
    $("#PageNumber").val(Page);
    SubscriberDetail($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val());
    if (Text == "Subscriber") {
        getSubscriberDetails($("#PageNumber").val());
    }
    else if (Text == "UnSubscribe") {
        getUnSubscriberDetails($("#PageNumber").val());
    }
    else {
        getOpenClickDetails(Text, $("#PageNumber").val());
    }
}

function getSubscriberDetails(PageNumber) {
    showLoader();
    $.ajax({
        url: '/User/Dashboard/SubscriberDetails/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ PageNumber: PageNumber }),
        async: true,
        success: function (result) {

            HideLoader();

            $("#divSubscriberModal").html(result);
            $('#details').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function getOpenClickDetails(Text, PageNumber) {
    showLoader();
    $.ajax({
        url: '/User/Dashboard/OpenClickDetails/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ "Text": Text, PageNumber: PageNumber }),
        async: true,
        success: function (result) {
            HideLoader();
            $("#divOpenClickModal").html(result);
            $('#openclickDetails').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function getUnSubscriberDetails(PageNumber) {
    showLoader();
    $.ajax({
        url: '/User/Dashboard/UnSubscriberDetails/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ PageNumber: PageNumber }),
        async: true,
        success: function (result) {
            HideLoader();
            $("#divUnSubscriberModal").html(result);
            $('#details').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function Close() {
   // window.location.href = "/User/Dashboard";
    $(".modal-fade").modal("hide");
    $(".modal-backdrop").remove();
}

function search()
{
    var startDate = $("#fromDate").val();
    var endDate = $("#toDate").val();
    $("#statuschartHeading").html("Campaign's Email Status");
    $("#Updateheading").html("Campaign's Updates");
    $("#campaignheading").html("Campaigns");
    getFilterEmailStatusChart(startDate, endDate);
    getFilterAllEmailStatusChart(startDate, endDate);
    getLatestCampaignByFilter(startDate, endDate);
    $("#btnClear").prop("disabled", false);
}

function clearFields()
{
    $("#fromDate").val('');
    $("#toDate").val('');
    $("#statuschartHeading").html("Last 7 days Campaign's Email Status");
    $("#Updateheading").html("All Time Campaign's Updates");
    $("#campaignheading").html("Latest Campaigns");
    getEmailStatusChart();
    getAllEmailStatusChart();
    getLatestCampaignByFilter();
    $("#btnClear").prop("disabled", true);
    //LoadHTMLPage("User", "Dashboard", "Index", [], [], "Get", $("#ContainerDiv"));
}

function getLatestCampaignByFilter(startDate, endDate) {
    //showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Dashboard/GetLatestCampaignByFilter/",
       data: JSON.stringify({
             "startDate": startDate, "endDate": endDate
        }),
        contentType: "application/json; charset=utf-8",
       dataType: "html",
        cache: false,
        processdata: true,
        success: function (result) {
            $("#tblLatestCampaign").empty().html(result);
            //getCampaignStatsByFilter(startDate, endDate);

        },
       error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

$("#toDate").click(function () {
   $("#applyReportFilter").removeAttr("disabled");
})
