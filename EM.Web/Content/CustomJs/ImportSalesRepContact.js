﻿ $(document).ready(function () {
        function alignModal() {
            var modalDialog = $(this).find(".modal-dialog");
            /* Applying the top margin on modal dialog to align it vertically center */
            modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
        }
        // Align modal when it is displayed
        $(".modal").on("shown.bs.modal", alignModal);

    // Align modal when user resize the window
        $(window).on("resize", function () {
        $(".modal:visible").each(alignModal);
     });
     BindddlListData();
   
     $('#btnImportContacts').click(function () {
         ImportSubscriberList();
         $("#ImportView").modal('hide');
     });
     //$('#ddllistName').click(function () {
     //    BindddlListData();
     //})
});

function BindddlListData()
{
    
    var myDropDownList = $('#ddllistName');
    var UserId = $('#UserId').val();

    myDropDownList.empty();
    myDropDownList.append($("<option></option>").val('0').html('Select'));
    $.ajax({
        type: "POST",
        url: "/SalesRep/Dashboard/getList?UserId=" + UserId,
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
     
            $.each(data, function () {
              
                myDropDownList.append($("<option></option>").val(this['ListId']).html(this['ListName']));
            });
        },
        failure: function (response) {
          
            alert(response.d);
        }
    });
}

function ImportSubscriberList()
{
    showLoader();
    var data = new FormData();
    var list = $('#ddllistName').val();
    var files = $("#uploadContacts").get(0).files;

    if (list == 0 || list == null) {
        GenerateAlert('Please select the list or create a new list.', '', 'info', false)
        return false;
    }

        if (files.length > 0) { data.append("subscriberList", files[0]); }
        else {
            GenerateAlert('Please select the file to upload.', '', 'warning', false);
            return false;
        }

        var extension = $("#uploadContacts").val().split('.').pop().toUpperCase();
        if (extension != "XLS" && extension != "XLSX" && extension != "CSV") {
            GenerateAlert('Invalid file format, please upload the excel file', '', 'warning', false);
            return false;
        
    }
    $.ajax({
        url: '../../DashBoard/ImportSubscriberList?id=' + $("#ddllistName").val()  ,
        type: "POST", processData: false,
        data: data, dataType: 'json',
        contentType: false,
        success: function (response) {
            $("#uploadContacts").val('');

            if (response.status) {
                HideLoader();
                    GenerateAlertWithHTML(response.msg, "", "success", refershWindow);
                    //GenerateAlertWithHTML("Import Successful", "", "success", refershWindow);
            } else {
                HideLoader();
                GenerateAlertWithHTML(response.msg,"", "error", refershWindow);
                    //GenerateAlertWithHTML("", UploadResponseContent(response), "error", refershWindow);
                    //GenerateAlert(response.msg, "", "error", false);
                }
        },
        error: function (er) {
            $("#divLoader").hide();
        }

    });
    return false;
}

function ImportLoadHTMLWithParam2(AreaName, ControllerName, ActionName, param1, param2, methodType) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}`,
        type: methodType,
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: { "param1": param1, "param2": param2 },
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
               // $("#containerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SaveImportSubscriberList(url, listid,UserId)
{
    showLoader();
    var ExcelFilePath = $("#ExcelFilePath").val();
    $.ajax({
        url: '/SalesRep/DashBoard/SaveImportSheetSubscribers',
        type: "POST",
        data: JSON.stringify({ "param1": url, "param2": listid, "_userid": UserId}),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status) {
                GenerateAlertWithHTML(response.msg, UploadResponseContent(response), "success", refershWindow);
                //GenerateAlertWithHTML("Import Successful", "", "success", refershWindow);
            } else {
                GenerateAlert("", "error", refershWindow);
                //GenerateAlertWithHTML("", UploadResponseContent(response), "error", refershWindow);
                //GenerateAlert(response.msg, "", "error", false);
            }

        },
        error: function (er) {
            HideLoader();
            Console.log(er);
            //alert(er);
        }

    });
} 

function UploadResponseContent(response) {
    var excelRecord = response.Content;
    var responseTable = "";
    if (excelRecord.DuplicateSubscribers != null && excelRecord.DuplicateSubscribers.length > 0) {
        responseTable += "<table class='' border='1' style='min-width:435px;'><thead><tr><th>Email</th><th>Name</th><th>Status</th></tr></thead><tbody>";
        for (var i = 0; i < excelRecord.DuplicateSubscribers.length; i++) {
            responseTable += `<tr><td>${excelRecord.DuplicateSubscribers[i].Email}</td><td>${excelRecord.DuplicateSubscribers[i].FirstName} ${excelRecord.DuplicateSubscribers[i].LastName}</td><td style='color:green'>Already exists!</td></tr>`;
        }
        responseTable += "</tbody></html>";
    }
    return responseTable;
}

function refershWindow(userId) {
    window.location = "/SalesRep/Dashboard";

}

function LoadSubscriberHTML(AreaName, ControllerName, ActionName, listId, subscriberid, listName) {
    showLoader();

    var url = "";
    if (ActionName == "ViewSubscriber") {
        var PageNumber = $("#PageNumber").val();
        url = `/${AreaName}/${ControllerName}/${ActionName}?listId=${listId}&Page=${PageNumber}&listName=${listName}`;
    }
    else {
        //if (ActionName == "AddEditSubscriber") {
        url = `/${AreaName}/${ControllerName}/${ActionName}?listId=${listId}&subscriberId=${subscriberid}&listName=${listName}`;
    }
    $('#Month').val("");
    $('#Day').val("");
    $(".RequiredField").hide();
    $.ajax({
        url: url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

//function UploadResponseContent(response) {
//    // console.log(response.Content);
//    var excelRecord = response.Content;
//    var responseTable = "";
//    console.log(excelRecord);
//    console.log(excelRecord.SuccessSubscribers);
//    if (excelRecord.DuplicateSubscribers.length != 0) {
//        responseTable += "<table class='' border='1' style='width:100%'><thead><tr><th>Email</th><th>Name</th><th>Status</th></tr></thead><tbody>";
//    }
    
//    //if (excelRecord.SuccessSubscribers != null) {
//    //    for (var i = 0; i < excelRecord.SuccessSubscribers.length; i++) {
//    //        responseTable += `<tr><td>${excelRecord.SuccessSubscribers[i].Email}</td><td>${excelRecord.SuccessSubscribers[i].FirstName} ${excelRecord.SuccessSubscribers[i].LastName}</td><td  style='color:green'>Success</td></tr>`;
//    //    }
//    //}

//    //if (excelRecord.InvalidSubscribers != null) {
//    //    for (var i = 0; i < excelRecord.InvalidSubscribers.length; i++) {
//    //        responseTable += `<tr><td>${excelRecord.InvalidSubscribers[i].Email}</td><td>${excelRecord.InvalidSubscribers[i].FirstName} ${excelRecord.InvalidSubscribers[i].LastName}</td><td style='color:red'>Invalid</td></tr>`;
//    //    }
//    //}

//    if (excelRecord.DuplicateSubscribers != null) {
//        for (var i = 0; i < excelRecord.DuplicateSubscribers.length; i++) {
//            responseTable += `<tr><td>${excelRecord.DuplicateSubscribers[i].Email}</td><td>${excelRecord.DuplicateSubscribers[i].FirstName} ${excelRecord.DuplicateSubscribers[i].LastName}</td><td style='color:green'>Already exists!</td></tr>`;
//        }
//    }
//    responseTable += "</tbody></html>";
//    return responseTable;
//    //console.log(responseTable);
//}

function CreateList() {
    
   // $("#ImportView").modal('hide');
    $('#txtListName').val('');
    $("#myModalCreateListPopUp").modal('show');
}
// Add List

function AddList()
 {
    var result = true;
    if (!emptyStringValidator($('#txtListName')))
    {
        $('#txtListName').focus();
        $("#lblListName").html("List Name is required");
        result = false;
    }
    else
    {
        $("#lblListName").empty();

    }
    if (result == true) {
        showLoader();
        var listName = $('#txtListName').val();
        var UserId = $('#UserId').val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ 'ListName': listName, 'UserId': UserId}),
            url: "/SalesRep/DashBoard/AddList",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                onAddList_Success(result);
                $("#myModalCreateListPopUp").modal('hide');
                BindddlListData();
                //alert(result.msg);
            },
            Error: function (result) {
                onAddList_Failure(result);
            }
        });
    }
}

function ResetList() {
    $('#txtListName').val('');
    $("#lblListName").html('');

}

function ClearContactUploadFile() {
    $("#uploadContacts").val(''); 
    $('#ddllistName').val('0');

}

function onAddList_Success(data) {
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success");


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddList_Failure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

$("#listmodalclose").click(function () {
    ClearContactUploadFile();
});

