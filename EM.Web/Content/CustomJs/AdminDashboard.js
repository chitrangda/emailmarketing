﻿$(document).ready(function (e) {
    $('#todayActive').addClass("active");
    bindUsersGrid();
    setTimeout(function () {
        GetAdminUserLists();
    }, 500)
    $("#MemberTypeDDL").attr("onchange", "FilterTodays('MemberType', $('#MemberTypeDDL').find('option:selected').text(),'Active')")
});
var dColumnsDef = [
    {
        "targets": [0],
        "visible": true,
        "searchable": false,
        "orderable": false,
        "className": 'text-center'
    },
    {
        "targets": [23],
        "visible": false,
        "searchable": false,
        "orderable": false,
        "className": 'text-center'
    },
    {
        "targets": [24],
        "visible": false,
        "searchable": false,
        "orderable": false,
        "className": 'text-center'
    }
];
var columns = [
    {
        data: null, render: function (data, type, row) {
            var clickFuncton = 'getEditDashboardModal("' + data.Id + '")';

            return '<a href="#!" onclick=\'' + clickFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a>';
 }, "title": "Action", "autoWidth": true
    },
    {
        data: null, "title": "Account Name", "autoWidth": true, render: function (data, type, row) {
            //return `<a  class="waves-effect waves-light" href="#!" onclick=getAccountDetails("${row.Id}")>${row.AccountName}</a><br/><a href='/User/Dashboard/Index/${row.Id}'>Login</a>`;
            return "<a href='/Admin/ManageUser/Index/" + row.Id + "'>" + row.AccountName + "(" + row.Email + ")" + "</a > <br /> <a href='/User/Dashboard/Index/" + row.Id + "'>Login</a>";
        }
    },
    //{ "data": "SalesRepName", "title": "Sales Rep", "name": "SalesRepName", "autoWidth": true },
    {
        data: null, "title": "Sales Rep", render: function (data, type, row) {
            if (row.SalesRepName != null) {
                return `<span oldSalesRepId="${row.SalesRepId}">${row.SalesRepName}</span>`;
            }
            else {
                return `<span oldSalesRepId="-1"></span>`;

            }
        },
    },
  
    {
        data: "Id", "title": "Notes", render: function (data, type, row) {
            return '<a href="#!" onclick="getNoteModalPopup(\'' + row.Id + '\')"><img src="/Content/imagesHome/note-ic.png" style="width:20px;" alt=""/></a>';
        }
    },
    //{ "data": "CampaignType", "title": "Campaign Type", "name": "PlanName", "autoWidth": true },
    { "data": "ClientType", "title": "Client Type", "name": "ClientType", "autoWidth": true },
    { "data": "MemberType", "title": "Member Type", "name": "MemberType", "autoWidth": true },
    //{
    //    data: null, "title": "Member Type", "name": "MemberType", "autoWidth": true, render: function (data, type, row) {
    //        return `<span planDurationAttr="${row.MemberType.split(',')[1]}">${row.MemberType.split(',')[0]}</span>`
    //    }
    //},
    { "data": "ReferralCode", "title": "Referral Code", "name": "ReferralCode", "autoWidth": true },
    {
        data: null, "title": "Spent Total", "autoWidth": true, render: function (data, type, row) {
            return row.totalSpendCredits == 0  ? "0" : "$" + row.totalSpendCredits;
        }
    },
    //{ "data": "totalSpendCredits", "title": "Spent Total", "name": "totalSpendCredits", "autoWidth": true },
    //{ "data": "IsActive", "title": "Account Status", "name": "IsActive", "autoWidth": true },
    {
        data: null, "title": "Account Status", "autoWidth": true, render: function (data, type, row) {
            return row.IsActive == true ? "Active" : "InActive";
        }
    },
    {
        data: null, render: function (data, type, row) {
            return (moment(row.RegisteredDate).format('YYYY-MM-DD hh:mm:ss A'));
        }, "title": "Registered Date", "autoWidth": true
    },
    //{ "data": "RegisteredDate", "title": "Date Registered", "name": "RegisteredDate", "autoWidth": true },
    { "data": "Days", "title": "Days", "name": "Days", "autoWidth": true },
    { "data": "FollowUpDate", "title": "Follow Up Date", "name": "FollowUpDate", "autoWidth": true },
    { "data": "FollowUpTime", "title": "Follow Up Time", "name": "FollowUpTime", "autoWidth": true },
    { "data": "TimeZone", "title": "Time Zone", "name": "TimeZone", "autoWidth": true },
    { "data": "SentToday", "title": "Sent Today", "name": "SentToday", "autoWidth": true },
    { "data": "Contacts", "title": "Contacts", "name": "Contacts", "autoWidth": true },
    { "data": "MonthlyCredit", "title": "Monthly Credits", "name": "MonthlyCredit", "autoWidth": true },
    { "data": "BillingAmount", "title": "MNTH Bill", "name": "BillingAmount", "autoWidth": true },
    { "data": "NextPackageUpdateDate", "title": "Next Bill", "name": "NextPackageUpdateDate", "autoWidth": true },
    { "data": "ChargedDate", "title": "CHRG Date", "name": "ChargedDate", "autoWidth": true },
    {
        data: null, "title": "Billing Status", "autoWidth": true, render: function (data, type, row) {
            return row.BillingStatus == 1 ? "Active" : "InActive";
        }
    },
    {
        data: null, "title": "CSV Upload", "autoWidth": true, render: function (data, type, row) {
            return row.CSVUpload == true ? "Active" : "InActive";
        }
    },
    {
        data: null, "title": "Sample Message", "name": "CustomizableTemplates", "autoWidth": true, render: function (data, type, row) {
            if (row.CustomizableTemplates == null || row.CustomizableTemplates == false) {
                return "OFF";
            } else {
                return "ON";
            }
        }
    },
    {
        "data": null, render: function (data, type, row) {
            if (data.FollowUpDateTime != null) {
                return "<input type='hidden' value='" + moment(data.FollowUpDateTime).format("DD-MM-YYYY hh:mm A")+"'>";
            }
            else {
                return "";
            }
        }
    },
      {
        data: null, "title": "IP Pool", render: function (data, type, row) {
            if (row.PoolName != null) {
                return `<span olPoolId="${row.IPPoolId}">${row.PoolName}</span>`;
            }
            else {
                return `<span olPoolId="-1"></span>`;

            }
        },
    },
    {
        data: null, "title": "Bounce Rate", render: function (data, type, row) {
            if (row.SubCount > 1000)
            {
                /*decimal BounceRate = (row.BounceCount * 100) / row.SubCount;*/
                return BounceRate = ((row.BounceCount * 100) / row.SubCount).toFixed(0)+ "%";
            }
            else {
                return 0 + "%";

            }
        },
    },
    
];
function bindUsersGrid() {
    InitDataTable($("#dataTable"), "/Admin/Dashboard/getAdminData", null, columns, dColumnsDef, [[10, 'desc']], 6, true, true);

}

function getEditDashboardModal(id) {
    showLoader();
    $.ajax({
        url: "/Admin/Dashboard/GetEditClientView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "id": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divEditClientModal").html(response);
            $('#updateDetails').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function GetSubscriberDashboardList()
{
    showLoader();
    var id = $("#ClientDetails_PlanId").val();
    var APIURL = $("#APIURL").val();
    var URL = APIURL + 'EmailPlans/GetPlanPricing?id=' + id;
    $.ajax({
        url: URL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $("#ClientDetails_PackageId").empty();
                $.each(response, function (index, value) {
                    $("#ClientDetails_PackageId").append($("<option></option>").val(value.Id).html(value.NoOfSubscribersMax));
                });
                GetPackageDetail();
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function GetPackageDetail() {
    showLoader();
    var url = $("#APIURL").val();
    var id = $('#ClientDetails_PackageId').val();
    var UserId = $('#ClientDetails_Id').val();
    var APIURL = url + 'EmailPlans/GetPlanByID?id=' + id + '&UserId=' + UserId;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": APIURL,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        if (response != null && response != "")
        {
            HideLoader();
            if (response.Country == "Canada")
            {
                $("#ClientDetails_BillingAmount").val(response.BillingAmount_Canada);
                if (response.NoOfCredits == 0) {
                    $("#ClientDetails_MonthlyCredit").val("Unlimited Emails");
                }
                else {
                    $("#ClientDetails_MonthlyCredit").val(response.NoOfCredits);
                }
               
               
            }
            else
            {
                $("#ClientDetails_BillingAmount").val(response.BillingAmount);

                if (response.NoOfCredits == 0) {
                    $("#ClientDetails_MonthlyCredit").val("Unlimited Emails");
                }
                else {
                    $("#ClientDetails_MonthlyCredit").val(response.NoOfCredits);
                }
            }
        }
    });
}






function UpdateUserData(id)
{
    $("#SalesRep").empty();
    $("#CampaignType").empty();
    $("#ClientType").empty();
    $("#MemberType").empty();
    $("#TimeZone").empty();
    $("#Ip_Pool").empty();

    $.each(SalesRepList, function (index, value) {
        $("#SalesRep").append(`<option value="${value.FirstName}" salesIdAttr="${value.id}">${value.FirstName} ${value.LastName}</option>`);
    });
    $.each(CampaignList, function (index, value) {
        $("#CampaignType").append(`<option value="${value.CampaignName}" campIdAttr="${value.CampaignTypeId}">${value.CampaignName}</option>`);
    });
    $.each(ClientTypesList, function (index, value) {
        $("#ClientType").append(`<option value="${value.ClientType1}" clientTypeIdAttr="${value.ClientTypeId}">${value.ClientType1}</option>`);
    });
    $.each(PlanList, function (index, value) {
        $("#MemberType").append(`<option value="${value.PlanId}">${value.PlanName}</option>`);
    });

    $.each(TimeZoneList, function (index, value) {
        $("#TimeZone").append(`<option value="${value.TimeZoneName}" timeZoneAttr="${value.TimeZoneID}">${value.TimeZoneName}</option>`);
    });
    $.each(IPPoolList, function (index, value) {
        $("#Ip_Pool").append(`<option value="${value.Name}" ipPoolAttr="${value.Id}">${value.Name}</option>`);
    });


    var editRow = $(`#${id}`).closest('tr');
    var accountName = editRow.find('td:eq(1)').text();
    var salesRepName = editRow.find('td:eq(2)').text();
    var campaignTypeName = editRow.find('td:eq(4)').text();
    var clientTypeName = editRow.find('td:eq(5)').text();
    var planDuration = editRow.find('td:eq(6)').find("span").attr("planDurationAttr");
    var accountStatus = editRow.find('td:eq(9)').text();
    var monthlyCredit = editRow.find('td:eq(17)').text();
    var nextBillingDate = editRow.find('td:eq(19)').text();
    var CustomizableTemplates = editRow.find('td:eq(21)').text();
    var followUp = editRow.find('td:eq(24)').find("input[type='hidden']").val();
    var TimeZone = editRow.find('td:eq(14)').text();
    var monthlybill = editRow.find('td:eq(18)').text();
    var chargedDate = editRow.find('td:eq(20)').text();
    var billingStatus = editRow.find('td:eq(21)').text();
    var CSVUpload = editRow.find('td:eq(22)').text();
    var userFullName = accountName.replace('Login', '').split(' ');
    var memberType = editRow.find("td:eq(6)").text();
    var ipPool = editRow.find('td:eq(25)').text();
    //console.log("salesRepName : ", salesRepName);
    //console.log("oldSalesRepId : ", editRow.find('td:eq(2)').find("span").attr("oldSalesRepId"));
    $("#oldSalesRepId").val(editRow.find('td:eq(2)').find("span").attr("oldSalesRepId"))
    $("#FirstName").val(userFullName[0])
    $("#LastName").val(userFullName[1])
    $("#IsActive").val(accountStatus)
    $("#clientId").val(id);
    $("#MonthlyCredit").val(monthlyCredit);
    $("#CustomizableTemplates").val(CustomizableTemplates)
    $("#PlanDuration").val(planDuration)
   
    var memval = $('#MemberType option').filter(function () { return $(this).html() == "" + memberType + ""; }).val();
    //console.log(memval);
    $("#SalesRep").val(salesRepName);
    $("#CampaignType").val(campaignTypeName);
    $("#ClientType").val(clientTypeName);
    $('#MonthlyBill').val(monthlybill);
    $('#BillingStatus').val(billingStatus);
    $('#CSVUpload').val(CSVUpload);
    $('#Ip_Pool').val(ipPool);
    $('#TimeZone').val(TimeZone);
    $("#MemberType").val(memval);
    $('#myModal').modal('toggle');
    $('#myModal').modal('show');
    initSignalDatePickerWithTime($('#NextBillingDate'));
    initSignalDatePickerWithTime($('#ChargedDate'));
    initSignalDatePicker($('#NextFollowup'));
    $('#NextBillingDate').val(nextBillingDate);
    if (nextBillingDate == '') {
        $('#NextBillingDate').val('');
    }
    $('#ChargedDate').val(chargedDate);
    if (chargedDate == '') {
        $('#ChargedDate').val('');
    }
    $('#NextFollowup').val(followUp);
    if (followUp == '') {
        $('#NextFollowup').val('');
    }

    //initDateTimeRangePicker($("#NextBillingDate"), {
    //    singleDatePicker: true,
    //    showDropdowns: true,
    //    drops: "up",
    //    timePicker: false,  // time format here
    //    startDate: nextBillDate,
    //    minDate: new Date(),
    //    locale: {
    //        format: 'MM-DD-YYYY'
    //    }
    //}, function (start, end, label) {
    //    containerEle.data('assignSD', start.format('MM-DD-YYYY'));
    //})
    // initSignalDatePickerWithTime($("#NextBillingDate"));
    //UpdateUserProfile(usp_GetAdminData_Result);
}

function onBegin() {
    showLoader();
    return true;
}

function onSuccess(data) {
    HideLoader();
    $('#updateDetails').modal('hide');
    if (data.status != "Error") {
        GenerateAlert("", "Update Successfully!", "success", bindUsersGrid);
    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onFailure()
{
    HideLoader();
}

function FilterTodaysAdmin(event,control, value, memberType)
{
    var key = event.keyCode || event.which;
    if (key == 13) {
        FilterTodays(control, value, memberType)
    }

    //$('#txtsearch').keydown(function (event) {
    //    if (event.keyCode == 13) {

    //        if (campaignType == "Active" && value != 'All') {
    //            var selectedFilter = control;
    //        } else {
    //            value = (value == '' || value == 'All') ? null : value;
    //            var selectedFilter = value == '' ? null : $('#ddlsearchoption').find("option:selected").val();
    //        }

    //        var clientType = $("#ClientTypeDDL").find("option:selected").text() == "All Client" ? null : $("#ClientTypeDDL").find("option:selected").text();
    //        var params = JSON.stringify({ "key": selectedFilter, "value": value, "clientType": clientType });
    //        var dataTable = InitDataTable($("#dataTable"), "/Admin/Dashboard/getAdminData/", params, columns, dColumnsDef, [[10, 'desc']], 6, true, true);
    //    }
    //});
}


function FilterTodays(control, value, memberType)
{
    
    if (memberType == "Active" && memberType != "Select") {
            var selectedFilter = control;
        } else {
        value = (value == '' || value == 'Select') ? null : value;
            var selectedFilter = value == '' ? null : $('#ddlsearchoption').find("option:selected").val();
        }

        var clientType = $("#ClientTypeDDL").find("option:selected").text() == "All Client" ? null : $("#ClientTypeDDL").find("option:selected").text();
        var params = JSON.stringify({ "key": selectedFilter, "value": value, "clientType": clientType });
        var dataTable = InitDataTable($("#dataTable"), "/Admin/Dashboard/getAdminData/", params, columns, dColumnsDef, [[10, 'desc']], 6, true, true);
}

function UpdateUserProfile()
{
    // var firstName = $("#FirstName").val()
    // var lastName = $("#LastName").val()
    var isActive = $("#IsActive").val()
    var clientId = $("#clientId").val();
    var monthlyCredit = $("#MonthlyCredit").val();
    var planDuration = $("#PlanDuration").val()
    var SalesRepId = $("#SalesRep").find("option:selected").attr("salesIdAttr");
    var customizableTemplates = $("#CustomizableTemplates").find("option:selected").val();
    var csvupload = $('#CSVUpload').find("option:selected").val();

    var ClientType = $("#ClientType").find("option:selected").text();
    var CampaignType = $("#CampaignType").find("option:selected").attr("campIdAttr");
    var PlanType = $("#MemberType").find("option:selected").val();
    var monthlybilling = $("#MonthlyBill").val();
    var billingstatus = $('#BillingStatus').val();
    
    var timeZone = $("#TimeZone").find("option:selected").attr("timeZoneAttr");
    var IpPool = $("#Ip_Pool").find("option:selected").attr("ipPoolAttr");
    var userAdminDataViewModel =
    {
        "UserId": clientId, "SalesRepId": SalesRepId, "IsActive": isActive == "Active" ? true : false, "MonthlyCredit": monthlyCredit, "CustomizableTemplates": customizableTemplates == "ON" ? true : false, "OldSalesRepId": $("#oldSalesRepId").val(),
        "ClientType": ClientType, "CampaignTypeId": CampaignType, "PlanTypeId": PlanType, "PlanDuration": planDuration, "NextPackageUpdateDate": $("#NextBillingDate").val(),
        "FollowUpDate": moment( $('#NextFollowup').val()), "BillingAmount": monthlybilling, "ChargedDate": $('#ChargedDate').val(),
        "BillingStatus": billingstatus == "Active" ? true : false, "CSVUpload": csvupload == "Active" ? true : false,
            "TimeZone": timeZone, "IpPool": IpPool
    };
    
    $.ajax({
        type: "POST",
        data: JSON.stringify(userAdminDataViewModel),
        url: "/Admin/Dashboard/UpdateUserProfile/",
        contentType: "application/json",
        async: true,
        success: function (data) {
            GenerateAlert(data.status, data.msg, "success", bindUsersGrid);
            $("#updateAdminUserDataModel").click()
            //bindUsersGrid();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //GenerateAlert(data.status, data.msg, "info", bindUsersGrid);
            $("#updateAdminUserDataModel").click()
            //bindUsersGrid();
        }

    });
}

function GetAdminUserLists() {
    $.ajax({
        type: "Get",
        url: "/Admin/Dashboard/GetAdminUserLists/",
        dataType: "json",
        async: true,
        success: function (result) {
            SalesRepList = result.SalesRepList;
            ClientTypesList = result.ClientTypesList;
            PlanList = result.PlanList;
            CampaignList = result.CampaignList;
            TimeZoneList = result.TimeZoneList;
            IPPoolList = result.IPPoolList
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            return false;
        }
    });
}

function bindReportView(id, startDate, endDate) {
    var fieldsReport = [

        { name: "CampaignName", type: "text", title: "Campaign Name", width: 120 },
        { name: "SentDateString", type: "text", title: "Sent Date", width: 120 },
        { name: "Opens", type: "number", title: "Opens", width: 100 },
        { name: "Clicks", type: "number", title: "Clicks", width: 100 },
        { name: "SubscriberIds", type: "text", title: "Total Subscribers", width: 100 }
    ];
    var controllerReport = {
        loadData: function () {
            return $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "/Admin/Dashboard/CampaignReport/" + id + `?startDate=${startDate}&endDate=${endDate}`,
                dataType: "json",
                async: true

            });
        },
        insertItem: $.noop,
        updateItem: $.noop,
        deleteItem: $.noop,
        updateItem: $.noop
    };
    $("#jsGrid").jsGrid("destroy");
    $("#jsGrid").empty();
    $("#todayData").hide();
    initGrid($("#jsGrid"), fieldsReport, controllerReport, true);
    if ($("#IsFilterActivated").length < 1) {
        var filter = `<div class="col-md-1"><label class="control-label">From : </label></div>`;
        filter += `<div class="col-md-3"><input type="text" name="toDate" id="fromDate" class="form-control" placeholder="From" /></div>`;
        filter += `<div class="col-md-1"><label class="control-label">To : </label></div>`;
        filter += `<div class="col-md-3"><input type="text" name="fromDate" id="toDate" class="form-control" placeholder="To" disabled="disabled" /></div>`;
        filter += `<div class="col-md-4"><button type="button" class="btn btn-info" id="applyReportFilter" disabled onclick="bindReportView('${id}',$('#fromDate').val(),$('#toDate').val())">Apply Filter</button></div><input type='hidden' id='IsFilterActivated' value='True'/>`;
        $("#campaignFilter").append(filter);
        $("#campaignFilter").after("<div class='row newRow'>&nbsp;</div>");
        initDatePickerForReport($("#fromDate"));
    }
}

function getAddEditSaleRepView(salesRepId) {
    window.location.href = "SalesRep/Index?id=" + salesRepId + "&curView=edit"
}

function getAccountDetails(Id) {
    $("#Id").val(Id);

}

// Note functions
function getNoteModalPopup(id) {
    if (typeof id === "undefined") {
        id = $("#newNote_UserId").val();
    }
    $.ajax({
        url: "/Admin/Dashboard/GetNoteView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "userid": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divNoteModal").html(response);
            initSignalDatePicker($('#followDT'));
            $('#repNote').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onAddNote() {
    var result = true;

    if (!emptyStringValidator($('#txtNotes'))) {
        $('#txtNotes').focus();
        $("#noteError").html("Note Description is required.");
        result = false;
    }
    else {
        $("#noteError").empty();
    }

    if (!emptyStringValidator($('#followDT'))) {
        $('#followDT').focus();
        $("#followError").html("FollowUp Date is required.");
        result = false;
    }
    else {
        $("#followError").empty();
    }

    if (!emptyStringValidator($('#notetype')) || $('#notetype').val() == "Select") {
        $('#notetype').focus();
        $("#noteTypeError").html("FollowUp Type is required.");
        result = false;
    }
    else {
        $("#noteTypeError").empty();
    }
    if (!emptyStringValidator($('#timeZone')) || $('#timeZone').val() == "Select") {
        $('#TimeZone').focus();
        $("#timeZoneError").html("Time Zone is required.");
        result = false;
    }
    else {
        $("#timeZoneError").empty();
    }
    if (result == true) {
        showLoader();

    }

    return result;
}

function onAddNoteSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "success", getNoteModalPopup);
    }
    else {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddNoteFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function Clear() {
    $("#newNote_NoteId").val(0);
    $("textarea[name='newNote.NoteDescription']").val('');
    $("input[name='newNote.FollowUpDate'").val('');
    $("select[name='newNote.NoteTypeId'").val('');
    $("select[name='newNote.TimeZone']").val('');
    $("#btnNoteSubmit").html("Update");

}

function editNote(oNote) {
    if (oNote) {
        $("#newNote_NoteId").val(oNote.NoteId);
        $("#newNote_UserId").val(oNote.UserId);
        $("textarea[name='newNote.NoteDescription']").val(oNote.NoteDescription);
        $("input[name='newNote.FollowUpDate'").val(moment(oNote.FollowUpDate).format('MM/DD/YYYY hh:mm A'));
        $("select[name='newNote.NoteTypeId'").val(oNote.NoteTypeId);
        $("select[name='newNote.TimeZone']").val(oNote.TimeZone);
        $("#btnNoteSubmit").html("Update");
    }
}

function deleteNoteConfirmation(noteId) {
    bootbox.confirm({
        message: "Are you sure want to delete this note?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                showLoader();
                deleteNote(noteId);
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function deleteNote(noteId) {
    $.ajax({
        url: "/Admin/Dashboard/DeleteNote",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "noteId": noteId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", getNoteModalPopup);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}