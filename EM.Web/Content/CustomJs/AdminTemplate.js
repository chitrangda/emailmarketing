﻿var selectedList = [];

function SortAdminTemplate(value) {
    showLoader();
    var serachTemplateValue = $("#serachTemplate").val().trim();
    GetTemplateListByPaging($("#PageNumber").val(), value, serachTemplateValue)
}

function TemplateAdminPagging(direction, page) {
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    TemplateAdminPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#SortTemplate').find('option:selected').val();
    GetTemplateListByPaging($("#PageNumber").val(), Order, "", page);
}

function TemplateAdminPage(PageControl, page, TotalRecord) {

    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }



    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function GetTemplateListByPaging(PageNumber, sortOrder, search, pageType) {
    $.ajax({
        url: '/Admin/CustomTemplates/GetTemplateListByPaging/',
        type: "Post",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ page: parseInt(PageNumber), orderBy: sortOrder, search: search, type: pageType }),
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                if (pageType == "SelectTemplateListGrid") {
                    $("#selectTemplateListGrid").empty().html(result);
                } else {
                    $("#templateListGrid").empty().html(result);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SearchAdminTemplate(type) {
    showLoader();
    var serachTemplateValue = $("#serachTemplate").val().trim();
    GetTemplateListByPaging($("#PageNumber").val(), "", serachTemplateValue, type)
}

function SelectAllCheckBoxAdminTemplate(input) {
    //$(".SelectedCheckBox").click();
    selectedList = [];
    if ($(input).is(":checked") == true) {
        $(".SelectedCheckBox").each(function () {
            selectedList.push($(this).val());
            $(this).prop("checked", true);
        });
    }
    else {
        $(".SelectedCheckBox").each(function () {

            $(this).prop("checked", false);
            selectedList = [];
        });
    }
}

function DeleteAdminTemplate() {
    var control = $(".SelectedCheckBox");

    var templateId = [];
    $.each(control, function (index, value) {
        if (value.checked) {
            templateId.push(value.value);
        }
    });

    if (templateId.length > 0) {

        bootbox.confirm({
            message: "Are you sure want to delete this template?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                HideLoader();
                if (result) {
                    AdminTemplateCaller("Admin", "CustomTemplates", "DeleteTemplate", templateId)
                }
            }
        });
    }
    else {
        GenerateAlert("Please select to delete the template!", "", "info", false);
    }

}
function AdminTemplateCaller(AreaName, ControllerName, ActionName, data) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result.status) {

                GenerateAlert(result.msg, "", "success", loadAdminTemplates);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}
function loadAdminTemplates() {
    //
    //LoadHTML("User", "Templates", "Index");
    window.location.reload();

}

function SaveBeeTemplateContent(htmlFile, jsonFile, name) {
    var titleName = '';
    if (name != '' && name != undefined && $("#TemplateId").val() != '') {
        titleName = 'Are you sure you want to update?';
    } else {
        titleName = 'Please enter the template name!';
    }
    var dialog = bootbox.dialog({
        title: titleName,
        message: "<input type='text' id='templateName' placeholder='Enter Template Name' class='form-control' value='" + name + "'/><br/><span id='errmsg' style='color:red;'></span>",
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function () {
                    HideLoader();
                    $('#templateName').modal('hide');
                }
            },
            success: {
                label: "Submit",
                callback: function ()
                {
                    $("#errmsg").empty();
                    CheckDuplicacyAdminTemplate(htmlFile, jsonFile, name, $("#TemplateId").val());
                    return false;
                }
            }
        }
    });
}

function CheckDuplicacyAdminTemplate(htmlFile, jsonFile, name, id)
{
    var templateName = $("#templateName").val().trim();
    if (templateName == null || templateName == "") {
        $("#errmsg").html("This field cannot be blank.");
        return false;
    }
    else {
        showLoader();
        $.ajax({
            url: '/Admin/CustomTemplates/CheckDuplicacy',
            type: 'POST',
            data: JSON.stringify({"templateName": templateName, "TemplateId" : id }),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async: true,
            success: function (result) {
                if (result.status == true) {
                    HideLoader();
                    $("#errmsg").html("Template name already exists! Please try another");
                    return false;
                }
                else {
                    var templateViewModels = {
                        "TemplateHtml": htmlFile,
                        "TemplateJSON": jsonFile,
                        "TemplateName": templateName,
                        "TemplateId": $("#TemplateId").val()
                    };

                    //var formData = new FormData();
                    //formData.append('templateViewModel', JSON.stringify(templateViewModel));
                    //$('#tempdata').html(htmlFile);
                    //$('#myModal').modal('toggle');
                    //$('#myModal').modal('show');
                    //var tempBase64Image = '';

                    //setTimeout(
                    //    function () {
                    //        getPreview(function (res) {
                    //            $('#closeModel').click()
                                $.ajax({
                                    type: "POST",
                                    url: '/Admin/CustomTemplates/SaveBeeTemplateContent',
                                    data: JSON.stringify({
                                        "templateViewModel": templateViewModels,
                                        //"base64String": res
                                    }),
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: "json",
                                    success: function (data) {
                                        HideLoader();
                                        if ($("#TemplateId").val() == 0) {
                                            GenerateAlert("Template created successfully", "", "success", loadAdminTemplates);

                                        } else {
                                            GenerateAlert("Template updated successfully", "", "success", loadAdminTemplates);
                                        }
                                        //LoadHTMLPage("User", "Templates", "TemplateList", [], [], "Get", $("#ContainerDiv"));
                                    },
                                    error: function (errordt) {
                                        //console.log("errordt  " + JSON.stringify(errordt));
                                    },
                                    failure: function (failuredt) {
                                        // console.log("failuredt  " + failuredt);
                                    }
                                });
                            //})


                       // }, 1000);

                }

            }
        });
    }

}

function selectchk() {
    selectedList = [];
    $(".SelectedCheckBox").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
        }
    });
    if ($(".SelectedCheckBox").length == selectedList.length) {
        $("#MasterCheckbox").prop("checked", true);
    }
    else {
        $("#MasterCheckbox").prop("checked", false);
    }
}
