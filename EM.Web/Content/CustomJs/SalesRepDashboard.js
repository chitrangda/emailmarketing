﻿var columns =
    [
        {
            data: null, "title": "Action", render: function (data, type, row) {
                //var clickFuncton = 'getEditSalesRepModal(' + JSON.stringify(data) + ')';
                var clickFuncton = 'getEditSalesRepModal("' + data.Id + '")';

                return '<a href="#!" onclick=\'' + clickFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a>';


            }, "autoWidth": true
        },
        { "data": "SalesRepName", "title": "Sales Rep", "name": "SalesRepName", "autoWidth": true },
        {
            data: "AccountName", "title": "Account Name", "autoWidth": true, render: function (data, type, row) {
                return "<a class='waves-effect waves-light' href='/SalesRep/Dashboard/AccountDetails/" + row.Id + "'>" + row.AccountName + "(" + row.Email + ")" + "</a><br/><a href='/User/Dashboard/Index/" + row.Id + "'>Login</a>";
            }
        },
        {
            data: "Id", "title": "Notes", render: function (data, type, row) {
                return '<a href="#!" onclick="getNoteModalPopup(\'' + row.Id + '\')"><img src="/Content/imagesHome/note-ic.png" style="width:20px;" alt=""/></a>';
            }
        },
        //{ "data": "CampaignName", "title": "Campaign Type", "name": "MemberType", "autoWidth": true },
        { "data": "ClientType", "title": "Client Type", "name": "ClientType", "autoWidth": true },
        { "data": "PlanName", "title": "Member Type", "name": "PlanName", "autoWidth": true },
        { "data": "ReferralCode", "title": "Referral Code", "name": "ReferralCode", "autoWidth": true },
        {
            data: null, "title": "Spent Total", "autoWidth": true, render: function (data, type, row) {
                return row.totalSpendCredits == 0 ? "0" : "$" + row.totalSpendCredits;
            }
        },
       // { "data": "totalSpendCredits", "title": "Spent Total", "name": "totalSpendCredits", "autoWidth": true },
        {
            data: "IsActive", "title": "Account Status", render: function (data, type, row) {
                if (row.IsActive == 1) {
                    return "Active"
                }
                else {
                    return "InActive";
                }
            }
        },
       // { "data": "RegisteredDate", "title": "Registered Date", "name": "RegisteredDate", "autoWidth": true },
        {
            data: null, render: function (data, type, row) {
                return (moment(row.RegisteredDate).format('YYYY-MM-DD hh:mm:ss A'));
            }, "title": "Registered Date", "autoWidth": true
        },
        { "data": "Days", "title": "Days", "name": "Days", "autoWidth": true },
        { "data": "FollowUpDate", "title": "Follow Up", "name": "FollowUpDate", "autoWidth": true },
        { "data": "FollowUpTime", "title": "Follow Up Time", "name": "FollowUpTime", "autoWidth": true },
        { "data": "TimeZone", "title": "Time Zone", "name": "TimeZone", "autoWidth": true },
        { "data": "SentToday", "title": "Sent Today", "name": "SentToday", "autoWidth": true },
        { "data": "Contacts", "title": "Contacts", "name": "Contacts", "autoWidth": true },
        { "data": "MonthlyCredit", "title": "Monthly credits", "name": "MonthlyCredit", "autoWidth": true },
        { "data": "BillingAmount", "title": "MONTH Bill", "name": "BillingAmount", "autoWidth": true },
        { "data": "NextPackageUpdateDate", "title": "Next Bill", "name": "NextPackageUpdateDate", "autoWidth": true },
        {
            data: "BillingStatus", "title": "Billing Status", render: function (data, type, row) {
                if (row.BillingStatus == 1) {
                    return "Active"
                }
                else {
                    return "InActive";
                }
            }
        },
        {
            data: "CSV_Upload", "title": "CSV Upload", render: function (data, type, row) {
                if (row.CSV_Upload == 1) {
                    return "Active"
                }
                else {
                    return "InActive";
                }
            }
        },
        {
            data: "CustomizableTemplates", "title": "Sample Message", render: function (data, type, row) {
                if (row.CustomizableTemplates == true) {
                    return "On"
                }
                else {
                    return "Off";
                }
            }
        },
        {
            data: null,
            title: "Export",
            render: function (data, type, row) {
                return '<a class="mb8" title="Export" href="/SalesRep/Dashboard/ExportClient?id=' + data.Id + '"> <i class="fa fa-file-excel-o"></i></a >'

            }
        },
        { "data": "PoolName", "title": "Pool Name", "name": "PoolName", "autoWidth": true },
    ];

$(document).ready(function (e) {
    $('#todayActive').addClass("active");
    bindSalesGrid();
    ddlsearchevent();
});

function bindSalesGrid() {
    InitDataTable($("#dashboardDataTable"), "/SalesRep/Dashboard/GetSalesRepData", null, columns, '', [[9, "desc"]], 6, true, true);
}

function getFilterData() {
    var key = $("#ddlsearchoption").val().trim();
    var value = $("#txtsearch").val().trim();
    var clientType = $("#ddlClientType").val();
    var memberType = $('#ddlMemberType').find("option:selected").text();
    var oParam = JSON.stringify({ "key": key, "value": value, "clientType": clientType, "MemberType": memberType });
    //$('#dashboardDataTable').DataTable().clear().destroy();
    InitDataTable($("#dashboardDataTable"), "/SalesRep/Dashboard/FilterSalesRepData", oParam, columns, dColumnsDef, [[9, "desc"]],6, true, true);

}

function getFilterDataonEnter(event) {
    var key = event.keyCode || event.which;
    if (key == 13) {
        getFilterData();

    }
}

function getEditSalesRepModal(id) {
    showLoader();
    $.ajax({
        url: "/SalesRep/Dashboard/GetEditClientView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "id": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divEditModal").html(response);
            $('#editSalesRepModal').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onBegin() {
    showLoader();
    return true;
}

function onSuccess(data) {
    HideLoader();
    $('#editSalesRepModal').modal('hide');
    if (data.status != "Error") {
        GenerateAlert("", "Update Successfully!", "success", bindSalesGrid);
    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onFailure() {

}

function getNoteModalPopup(id) {
    if (typeof id === "undefined") {
        id = $("#newNote_UserId").val();
    }
    $.ajax({
        url: "/SalesRep/Dashboard/GetNoteView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "userid": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divNoteModal").html(response);
            initSignalDatePicker($('#followDT'));
            $('#repNote').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onAddNote() {
    var result = true;

    if (!emptyStringValidator($('#txtNotes'))) {
        $('#txtNotes').focus();
        $("#noteError").html("Note Description is required.");
        result = false;
    }
    else {
        $("#noteError").empty();
    }

    if (!emptyStringValidator($('#followDT'))) {
        $('#followDT').focus();
        $("#followError").html("FollowUp Date is required.");
        result = false;
    }
    else {
        $("#followError").empty();
    }

    if (!emptyStringValidator($('#notetype')) || $('#notetype').val() == "Select") {
        $('#notetype').focus();
        $("#noteTypeError").html("FollowUp Type is required.");
        result = false;
    }
    else {
        $("#noteTypeError").empty();
    }
    if (!emptyStringValidator($('#timeZone')) || $('#timeZone').val() == "Select") {
        $('#TimeZone').focus();
        $("#timeZoneError").html("Time Zone is required.");
        result = false;
    }
    else {
        $("#timeZoneError").empty();
    }
    if (result == true) {
        showLoader();

    }

    return result;
}

function onAddNoteSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "success", getNoteModalPopup);
    }
    else {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddNoteFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function Clear() {
    $("#newNote_NoteId").val(0);
    $("textarea[name='newNote.NoteDescription']").val('');
    $("input[name='newNote.FollowUpDate'").val('');
    $("select[name='newNote.NoteTypeId'").val('');
    $("select[name='newNote.TimeZone']").val('');
    $("#btnNoteSubmit").html("Update");

}

function editNote(oNote) {
    if (oNote) {
        $("#newNote_NoteId").val(oNote.NoteId);
        $("#newNote_UserId").val(oNote.UserId);
        $("textarea[name='newNote.NoteDescription']").val(oNote.NoteDescription);
        $("input[name='newNote.FollowUpDate'").val(moment(oNote.FollowUpDate).format('MM/DD/YYYY hh:mm A'));
        $("select[name='newNote.NoteTypeId'").val(oNote.NoteTypeId);
        $("select[name='newNote.TimeZone']").val(oNote.TimeZone);
        $("#btnNoteSubmit").html("Update");
    }
}

function deleteNoteConfirmation(noteId) {
    bootbox.confirm({
        message: "Are you sure want to delete this note?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                showLoader();
                deleteNote(noteId);
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function deleteNote(noteId) {
    $.ajax({
        url: "/SalesRep/Dashboard/DeleteNote",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "noteId": noteId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", getNoteModalPopup);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}


function ddlsearchevent() {
    $("#ddlsearchoption").change(function () {
        var selval = $(this).val();
        $("#txtsearch").val('');
        if (selval == 'MobilePhone') {
            phoneFormat($("#txtsearch"));
        }
        else {
            $("#txtsearch").removeAttr('maxlength');
            $("#txtsearch").unbind("keypress");;
        }
    });
}

function bindUserActivity(id)
{
    var dColumnsDefs = [
        {
            "targets": [0],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }
    ];
    var columns = [

        { "data": "CreateDate", "title": "DateTime", "name": "CreateDate", "autoWidth": true },
        { "data": "Description", "title": "LOG SUMMARY", "name": "Description", "autoWidth": true },

    ]

    var param = JSON.stringify({ Userid: id });
    InitDataTable($("#UserActivityTable"), "/SalesRep/Dashboard/GetUserActivity", param, columns, dColumnsDefs, [[1, 'desc']], 0, false, false);
}

$(document).on("change", "#ddlplanlist", function ()
{
    GetSubscriber();
});

function GetSubscriber() {
    showLoader();
    var id = $("#ddlplanlist").val();
    var APIURL = $("#APIURL").val();
    var URL = APIURL + 'EmailPlans/GetPlanPricing?id=' + id;
    $.ajax({
        url: URL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $("#ddlSublist").empty();
                $.each(response, function (index, value) {
                    $("#ddlSublist").append($("<option></option>").val(value.Id).html(value.NoOfSubscribersMax));
                });
                GetPackageDetails();
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function GetPackageDetails() {
    showLoader();
    var url = $("#APIURL").val();
    var id = $('#ddlSublist').val();
    var UserId = $('#ClientDetails_Id').val();
    var APIURL = url + 'EmailPlans/GetPlanByID?id=' + id + '&UserId=' + UserId;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": APIURL,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        if (response != null && response != "") {
            HideLoader();
            if (response.Country == "Canada") {
                $("#ClientDetails_BillingAmount").val(response.BillingAmount_Canada);
                if (response.NoOfCredits == 0) {
                    $("#ClientDetails_MonthlyCredit").val("Unlimited Emails");
                }
                else {
                    $("#ClientDetails_MonthlyCredit").val(response.NoOfCredits);
                }
            }
            else {
                $("#ClientDetails_BillingAmount").val(response.BillingAmount);
                if (response.NoOfCredits == 0) {
                    $("#ClientDetails_MonthlyCredit").val("Unlimited Emails");
                }
                else {
                    $("#ClientDetails_MonthlyCredit").val(response.NoOfCredits);
                }

            }
        }
    });
}