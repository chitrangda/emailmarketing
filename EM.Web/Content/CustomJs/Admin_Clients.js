﻿//var countryList = [];
//var SaleRepList = [];
//var fields = [

//    {
//        type: "control", width: 150,
//        inserting: false,
//        deleteButton: false,
//        searchButton: false,
//        itemTemplate: function (value, item) {
//            var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
//            //var t = "InActive";
//            //if (!item.isActive) {
//            //    t = "Active"
//            //}
//            //var $customButton1 = $("<button>")
//            //    .addClass("glyphicon glyphicon-arrow-up ml-1 mr-1 btn btn-info")
//            //    .text(t)
//            //    .click(function (e) {
//            //        changeAccountStatus(item.Id, !item.isActive);
//            //        e.stopPropagation();
//            //    });
//            var $customButton = $("<a>")
//                .html("<i class='fa fa-eye'></i>")
//                .attr("title","view details")
//                .addClass("ml-1")
//                .attr("href", "#")
//                .click(function (e) {
//                    window.location.href = "/Admin/ManageUser/Index/" + item.Id;
//                    e.stopPropagation();
//                });
//            //var $customButton1 = $("<a>")
//            //    .addClass("ml-1")
//            //    .html("Campaign Report").attr("onClick", `bindReportView('${item.Id}','','')`).attr("href", "#");
//            return $result.add($customButton);
//        },
//        headerTemplate: function () {
//            //var $result = jsGrid.fields.control.prototype.headerTemplate.apply(this, arguments);
//            var $customHeader = $("<a>").attr("href", "/Admin/Client/ExportList").addClass("btn btn-jsGridHeader")
//                .html('<i class="fa fa-upload" aria-hidden="true"></i> Export');
//            //return $result.add($customHeader);
//            return $customHeader;
//        }
//    },
//    {
//        name: "FirstName", type: "text", title: "First Name", width: 100, validate: { validator: "required", message: "First Name is required." }
//    },
//    { name: "LastName", type: "text", title: "Last Name", width: 100 },
//    {
//        name: "MobilePhone", type: "text", title: "Contact", width: 100, validate: { validator: "required", message: "Contact is required." },
//        editTemplate: function (value) {
//            var mobileControl = $("<input>").val(value).attr("type", "text");
//            mobileControl.usPhoneFormat({
//                format: '(xxx) xxx-xxxx',
//            });
//            return this.editControl = mobileControl;
//        }
//    },
//    //{
//    //    name: "SalesRepId", type: "text", title: "SalesRep", validate: { validator: "required", message: "SalesRep is required." },
//    //    editTemplate: function (value) {
//    //        return this.editControl = $("<select>").html($.map(SaleRepList, function (i) {
//    //            return $('<option/>', { text: i.FirstName });
//    //        })).val(value);
//    //    }
//    //},
//    { name: "City", type: "text", validate: { validator: "required", message: "City is required." } },
//    { name: "State", type: "text", validate: { validator: "required", message: "State is required." } },
//    {
//        name: "Country", type: "text", validate: { validator: "required", message: "Country is required." },
//        editTemplate: function (value) {
//            return this.editControl = $("<select>").html($.map(countryList, function (i) {
//                return $('<option/>', { text: i.CountryName });
//            })).val(value);
//        }
//    },
//    {
//        name: "Zip", type: "text", width: 50, validate: {
//            validator: function (value, item) {
//                if (value.trim() == "") {
//                    alert('Zip code is required');
//                    return false;
//                }
//                else {
//                    if (/^\d{5}(-\d{4})?$/.test(value) == false) {
//                        alert("Invalid zip code.")
//                        return false;
//                    }
//                    else {
//                        return true;
//                    }
//                }
//            }
//        }
//    },
//    { name: "isActive", type: "checkbox", title: "Status", width: 50 }


//];
//var controller = {
//    loadData: function (filter) {
//        if (typeof filter.isActive === "undefined") {
//            filter.isActive = null;
//        }
//        filter.FirstName = filter.FirstName == "" ? null : filter.FirstName;
//        filter.LastName = filter.LastName == "" ? null : filter.LastName;
//        filter.MobilePhone = filter.MobilePhone == "" ? null : filter.MobilePhone;
//        filter.City = filter.City == "" ? null : filter.City;
//        filter.State = filter.State == "" ? null : filter.State;
//        filter.Country = filter.Country == "" ? null : filter.Country;
//        filter.Zip = filter.Zip == "" ? null : filter.Zip;
//        return $.ajax({
//            type: "POST",
//            data: JSON.stringify({ FirstName: filter.FirstName, LastName: filter.LastName, MobilePhone: filter.MobilePhone, City: filter.City, State: filter.State, Country: filter.Country, Zip: filter.Zip, isActive: filter.isActive }),
//            contentType: "application/json; charset=utf-8",
//            url: "/Admin/Client/getAllUsers/",
//            dataType: "json",
//            async: true

//        });
//    },
//    insertItem: $.noop,
//    updateItem: $.noop,
//    deleteItem: $.noop,
//    updateItem: $.noop
//};

$(document).ready(function (e) {
    $('#dashboardActive').addClass("active");
    bindUsersGrid();
    //getCountyList();
    //GetSaleRepList();
});
function bindUsersGrid() {
    //initGrid($("#jsGrid"), fields, controller, true, false, true, false, true, updateUserDetails);
    var dColumnsDef = [
        {
            "targets": [0],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }
    ];
    var columns = [
        {
            data: null, render: function (data, type, row) {
                return `<a onClick="LoadActiveModel('${row.Id}',${row.isActive})"><img src="/Content/admin/images/edit-ic.png"/></a>`;
            }, "title": "Action", "autoWidth": true
        },
        {
            data: null, render: function (data, type, row) {
                return `<a href="javascript:void(0)" id="${row.Id}" onclick="UpdateClient('${[row.FirstName, row.LastName, row.MobilePhone, row.City, row.State, row.Country, row.Zip, row.isActive].toString()}','${row.Id}')">${row.FirstName} ${row.LastName}</a></br><a class="mr-2" href="/User/Dashboard/Index/${row.Id}">Login</a>`;
            }, "title": "Name", "name": "Name", "autoWidth": true
        },
        { "data": "MobilePhone", "title": "MobilePhone", "name": "MobilePhone", "autoWidth": true },
        { "data": "City", "title": "City", "name": "City", "autoWidth": true },
        { "data": "State", "title": "State", "name": "State", "autoWidth": true },
        { "data": "Country", "title": "Country", "name": "Country", "autoWidth": true },
        { "data": "Zip", "title": "Zip", "name": "Zip", "autoWidth": true },
        //{ "data": "isActive", "title": "IsActive", "name": "isActive", "autoWidth": true }
        {
            data: null, render: function (data, type, row) {
                if (row.isActive) {
                    return "Active";
                } else {
                    return "InActive";
                }
            }, "title": "Active/Inactive", "autoWidth": true 
        }
    ];
    InitDataTable($("#userTable"), "/Admin/Client/getAllUsers", null, columns, dColumnsDef, [[2, 'desc']], 4,false,false);

}

function UpdateClient(item, id) {
    var values = item.split(',');
    var userInfoViewModel = { "Id": id, "FirstName": values[0], "LastName": values[1], "MobilePhone": values[2], "City": values[3], "State": values[4], "Country": values[5], "Zip": values[6], "isActive": values[7] }
    //console.log(userInfoViewModel);
    LoadHTMLPagePost("Admin", "Client", "UpdateUserDetails", userInfoViewModel, "Get", $("#ContainerDiv"), null);
    $("#userTable_wrapper").hide();
}
function getCountyList() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Client/getCountyList/",
        dataType: "json",
        async: true,
        success: function (result) {
            countryList = result;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            return false;
        }
    });
}
function GetSaleRepList() {
    $.ajax({
        type: "Get",
        url: "/Admin/Client/GetSaleRepList/",
        dataType: "json",
        async: true,
        success: function (result) {
            SaleRepList = result;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            return false;
        }
    });
}
function LoadActiveModel(id,status) {
    console.log(id);
    console.log(status);
    $("#clientId").val(id);
    $('#myModal').modal('toggle');
    $('#myModal').modal('show');
    if (status) {
        $('#IsActiveInActive').val("Active");
    } else {
        $('#IsActiveInActive').val("InActive");
    }
}
function SaveActive(control) {
    console.log($(control).find("option:selected").val());
    if ($(control).find("option:selected").val() == "Active") {
        changeAccountStatus($("#clientId").val(), true)
        GenerateAlert("", "Activated Successfully","success")
    } else {
        GenerateAlert("", "Deativated Successfully", "success")
        changeAccountStatus($("#clientId").val(), false)
    }
    $("#clientId").val("");
    $('#activeInActiveModelClose').click();
}
function changeAccountStatus(oId, oStatus) {
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/Client/changeAccountStatus/",
        dataType: "json",
        data: JSON.stringify({ "id": oId, "status": oStatus }),
        async: true,
        success: function (result) {
            $("#divLoader").hide();
            bindUsersGrid();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            return false;
        }
    });
}
function updateUserDetails(item) {
    $.ajax({
        type: "POST",
        data: JSON.stringify({ Id: item.Id, FirstName: item.FirstName, LastName: item.LastName, MobilePhone: item.MobilePhone, City: item.City, State: item.State, Country: item.Country, Zip: item.Zip, isActive: item.isActive }),
        url: "/Admin/Client/updateUserDetails/",
        contentType: "application/json",
        async: true,
        success: function (data) {
            if (data.status.toLowerCase() != "error") {
                GenerateAlert(data.status, data.msg, "success", bindUsersGrid);
            }
            else {
                GenerateAlert("", data.msg, "info", bindUsersGrid);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            GenerateAlert(data.status, data.msg, "info", bindUsersGrid);

        }

    });
}

function onClientEditBegin() {
    var result = true;
    if (!emptyStringValidator($('#FirstName'))) {
        $('#FirstName').focus();
        $("#FirstNameError").html("FirstName is required.");
        result = false;
    }
    else {
        $("#FirstNameError").empty();
    }
    if (!emptyStringValidator($('#LastName'))) {
        $('#LastName').focus();
        $("#LastNameError").html("LastName is required.");
        result = false;
    }
    else {
        $("#LastNameError").empty();
    }

    if (!emptyStringValidator($('#MobilePhone'))) {
        $('#MobilePhone').focus();
        $("#MobilePhoneError").html("Mobile Number is required.");
        result = false;
    }
    else {
        $("#MobilePhoneError").empty();
    }

  
    return result;
    //if (!emptyStringValidator($('#City'))) {
    //    $('#City').focus();
    //    $("#CityError").html("City is required.");
    //    result = false;
    //}
    //else {
    //    $("#CityError").empty();
    //}
}
function onClientEditSucess(data) {
    GenerateAlert("", "Updated Successfully","success");
    $('#ContainerDiv').empty(); $('#userTable_wrapper').show();
    var nameEditControl = $("<a>");
    nameEditControl.attr("id", data.data.Id)
    nameEditControl.text(data.data.FirstName + " " + data.data.LastName)
    nameEditControl.attr("onclick", `UpdateClient('${data.data.FirstName}, ${data.data.LastName}, ${data.data.MobilePhone}, ${data.data.City}, ${data.data.State}, ${data.data.Country}, ${data.data.Zip}, ${data.data.isActive}','${data.data.Id}')`)

    var trControl = $(`#${data.data.Id}`).closest('tr');
    trControl.find('td:eq(1)').html("");
    trControl.find('td:eq(1)').html(nameEditControl);
    trControl.find('td:eq(1)').append(`</br><a class="mr-2" href="/User/Dashboard/Index/${data.data.Id}">Login</a>`);
    //trControl.find('td:eq(2)').text(data.data.LastName);
    trControl.find('td:eq(2)').text(data.data.MobilePhone);
    trControl.find('td:eq(3)').text(data.data.City);
    trControl.find('td:eq(4)').text(data.data.State);
    trControl.find('td:eq(5)').text(data.data.Country);
    trControl.find('td:eq(6)').text(data.data.Zip);
    //$(`#${data.data.Id}`).removeAttr("onclick")
    //$(`#${data.data.Id}`).attr("onclick", `UpdateClient('${data.data.FirstName}, ${data.data.LastName}, ${data.data.MobilePhone}, ${data.data.City}, ${data.data.State}, ${data.data.Country}, ${data.data.Zip}, ${data.data.isActive}','${data.data.Id}')`)
}
function onClientEditFailure() {
    $('#ContainerDiv').empty(); $('#userTable_wrapper').show();
}