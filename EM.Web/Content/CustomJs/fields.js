﻿function SelectAllCheckBox(input) {
    $(".SelectedCheckBox").click();
}
function FieldAction(commandValue, selectedAction) {
    var fieldsId = [];
    var type = selectedAction == "Activate" ? "Add" : "Remove"; 
    if (selectedAction == "Activate" && commandValue.val() == "0") {
        GenerateAlert(`Please select the items to ${type}`, "", "info", false);
    } else {
        fieldsId.push(type == "Add" ? commandValue.val() : commandValue);
    }
    
   
    if (fieldsId.length > 0 && selectedAction != "") {
        if (selectedAction == "0") {
            GenerateAlert("Please select the correct action!", "", "info", false);
            return false;
        }
        bootbox.confirm({
            message: `Are you sure want to ${type} field(s)?`,
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $(commandValue).val("0");
                  ActivateDeactivate(fieldsId, selectedAction);
                } else {
                    //GenerateAlert("Your list is safe", "", "success", false);
                }
            }
        });
    }
    else {
        GenerateAlert("Please select the items to add/remove!", "", "info", false);
    }
}

function ActivateDeactivate(data, actionName) {
    var url = `/User/Setting/ActivateDeactiveFields?fieldsId=${data}&listId=${$("#ListId").val()}&actionName=${actionName}`;
    $.ajax({
        url: url,
        type: "Get",
        async: true,
        success: function (result) {
            if (result.status) {
                GenerateAlert(result.msg, "", "success", false);
                LoadHTMLPage("User", "Setting", "ViewFields", ['id'], [$("#ListId").val()], "Get", $("#ContainerDiv"));
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}