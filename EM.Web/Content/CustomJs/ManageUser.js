﻿/*jsGrid init fields start*/
//var fields = [
//    {
//        type: "control",
//        inserting: false,
//        deleteButton: false,
//        editButton: false,
//        updating: false,
//        itemTemplate: function (value, item) {
//            var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
//            var $customButton = $("<a>")
//                .html("Export")
//                .attr("href", "#!")
//                .click(function (e) {
//                    window.location.href = "/Admin/ManageUser/ExportInvoicePdf/" + item.InvoiceID;
//                    e.stopPropagation();
//                });
//            return $result.add($customButton);
//        },
//        headerTemplate: function (value, item) {
//            return $("<span>").html("Action")
//        }
//    },
//    { name: "InvoiceID", type: "text", title: "Invoice Id", width: 120, align: "center" },
//    { name: "OrderID", type: "text", title: "Order Id", width: 120, align: "center" },
//    { name: "AmountCharged", type: "text", title: "Amount Charged", width: 100, align: "center" },
//    { name: "InvoiceType", type: "text", title: "Invoice Type", width: 100, align: "center" },
//    { name: "FirstName", type: "text", title: "Name", width: 100, align: "center" },

//];

//var controller = {
//    loadData: function () {
//        return $.ajax({
//            type: "POST",
//            data: JSON.stringify({ userid: $("#hdnUserId").val() }),
//            contentType: "application/json; charset=utf-8",
//            url: "/Admin/ManageUser/GetInvoices/",
//            dataType: "json",
//            async: true

//        });
//    },
//    insertItem: $.noop,
//    updateItem: $.noop,
//    deleteItem: $.noop,
//    updateItem: $.noop
//};
/*jsGrid init fields end*/

$(document).ready(function (e) {
    $("#dashboardActive").addClass("active");
 
    getAccountDetailsView();
})


/* Invoice functions start*/
function onAddInvoiceBegin() {
    var result = true;
    if (!emptyStringValidator($('#txtAmount'))) {
        $('#txtAmount').focus();
        $("#amtError").html("Amount is required");
        result = false;
    }
    else {
        $("#amtError").empty();

    }
    if (!emptyStringValidator($('#OrderID')))
    {
            $('#OrderID').focus();
            $("#OrderError").html("Order is required");
            result = false;
     }
   else
        if (!NumberValidator($('#OrderID')))
        {
                $('#OrderID').focus();
                $("#OrderError").html("Enter only digits");
                result = false;
            }
        else
        {
                $("#OrderError").empty();
            }
        
       
       
    if (!emptyStringValidator($('#PaymentTypeId')) || $('#PaymentTypeId').val() == "0") {
        $('#PaymentTypeId').val("")
        $('#PaymentTypeId').focus();
        $("#PaymentTypeError").html("Charge Type is required.");
        result = false;
    }
    else {
        $("#PaymentTypeError").empty();
    }
    if (!$('#CardNumber').is('[readonly]')) 
    {
        if (!emptyStringValidator($('#CardNumber'))) {
            $('#CardNumber').focus();
            $("#CreditError").html("Card Number is required.");
            result = false;
        }
        else {
            var isCreditCardValid = validateCreditCardNumber($('#CardNumber').val().trim())

            if (isCreditCardValid) {
                $("#CreditError").empty();
            } else {
                $('#CardNumber').focus();
                $("#CreditError").html("Card Number is invalid.");
                result = false;
            }

        }
    }
    var current_Date = new Date().setFullYear($('#ExpYear').val(), $('#ExpMonth').val(), 1);
    var today = new Date();
    if (!emptyStringValidator($('#ExpMonth')) || $('#ExpMonth').val() == "0") {
        $('#ExpMonth').val("")
        $('#ExpMonth').focus();
        $("#ExpMonthError").html("Expiry Month is required.");
        result = false;
    }
    else if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
        $("#ExpMonthError").empty();
        result = false;
    } else if (current_Date < today) {
        $("#ExpMonthError").html("Month should be greater than today's month");
        result = false;
    }
    else {
        $("#ExpMonthError").empty();
    }

    if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
        $('#ExpYear').val("")
        $('#ExpYear').focus();
        $("#ExpYearError").html("Expiry Year is required.");
        result = false;
    }
    else {
        var current_year = new Date().getFullYear().toString().substr(-2);
        if (($('#ExpYear').val() < current_year) || ($('#ExpYear').val() > 2039)) {
            $("#ExpYearError").html(`Year should be in range ${current_year} to 2039 year`);
            return false;
        } else {
            $("#ExpYearError").empty();
        }
    }

    if (!emptyStringValidator($('#txtName'))) {
        $('#txtName').focus();
        $("#nameError").html("Name is required");
        result = false;
    }
    else {
        $("#nameError").empty();

    }

    if (!emptyStringValidator($('#txtPhone'))) {
        $('#txtPhone').focus();
        $("#PhoneErr").html("Phone Number is required");
        result = false;
    }
    else {
        if (!mobilenoValidator($('#txtPhone'))) {
            $('#txtPhone').focus();
            $("#PhoneErr").html("Invalid Mobile Number");
            result = false;
        }
        else {
            $("#PhoneErr").empty();

        }
    }

    if (!emptyStringValidator($('#txtAddress'))) {
        $('#txtAddress').focus();
        $("#addErr").html("Address is required");
        result = false;
    }
    else {
        $("#addErr").empty();

    }
    if (!emptyStringValidator($('#txtCity'))) {
        $('#txtCity').focus();
        $("#CityError").html("City is required");
        result = false;
    }
    else {
        $("#CityError").empty();

    }
    if (!emptyStringValidator($('#txtState'))) {
        $('#txtState').focus();
        $("#StateErr").html("State is required");
        result = false;
    }
    else {
        $("#StateErr").empty();

    }
    if (!emptyStringValidator($('#txtZip'))) {
        $('#txtZip').focus();
        $("#zipErr").html("Postal Code is required");
        result = false;
    }
    else {
        if (!zipcodeValidator($('#txtZip'))) {
            $('#zipErr').focus();
            $('#zipErr').html("Invalid Postal Code");
            result = false;
        }
        else {
            $('#zipErr').empty();
        }
    }

    if (result == true) {
        showLoader();
    }
    return result;
}

function onAddInvoiceSucess(data) {
    HideLoader();
    if (data.status != "false")
    {
        GenerateAlert("", data.msg, "success", bindInvoiceGrid);

    }
    else {
        GenerateAlert("", data.msg, "info");
    }

}

function onAddInvoiceFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function bindInvoiceGrid() {
    $('#managebillingActive,#createInvoiceActive,#accountActive,#noteNoteActive,#verifiedEmailActive,#re-activateCampActive').removeClass("btn-active");
    $("#invoiceActive").addClass("btn-active");
    var dColumnsDef = [
        {
            "targets": [0],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }
    ];
    var columns = [
        //{ "data": "TransactionDate", "title": "Date", "name": "TransactionDate", "autoWidth": true },
        {
            data: null, render: function (data, type, row)
            {
                return (moment(row.TransactionDate).format('YYYY-MM-DD hh:mm:ss A'));
            }, "title": "Date", "autoWidth": true
        },
        { "data": "AmountCharged", "title": "Amount", "name": "AmountCharged", "autoWidth": true },
        { "data": "AnTransactionId", "title": "Invoice#", "name": "AnTransactionId", "autoWidth": true },
        {
            data: null, render: function (data, type, row) {
                return '<a href="/Admin/ManageUser/ExportInvoicePdf/' + row.InvoiceID + '" target="_blank"><img src="../../../Content/imagesUser/export-ic.png" /></a>';
            }, "title": "Action", "autoWidth": true
        },
    ]
    $("#containerDiv").html('<table id="invoiceTable" class="display new_grid" style="width:100%"></table>');
    InitDataTable($("#invoiceTable"), "/Admin/ManageUser/GetInvoices", JSON.stringify({ userid: $("#hdnUserId").val() }), columns, dColumnsDef, [[0,'desc']], 0,false, false);
}

function bindUserActivityGrid(itm) {
    $('#managebillingActive,#createInvoiceActive,#accountActive').removeClass("btn-active");
    //$("#invoiceActive").addClass("btn-active");
    $(itm).addClass("btn-active");
    var dColumnsDef = [
        {
            "targets": [0],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }
    ];
    var columns = [
        
        { "data": "CreateDate", "title": "DateTime", "name": "CreateDate", "autoWidth": true },
        { "data": "Description", "title": "LOG SUMMARY", "name": "OrderID", "autoWidth": true },
        
    ]
    $("#containerDiv").html('<table id="UserActivityTable" class="display new_grid" style="width:100%"></table>');
    var param = JSON.stringify({ userid: $("#hdnUserId").val() });
    InitDataTable($("#UserActivityTable"), "/Admin/ManageUser/GetUserActivity", param, columns, dColumnsDef, [[1, 'desc']], 0, false, false);
}

/* Invoice functions end */

/* Account Details functions start*/

function getAccountDetailsView() {
    $('#managebillingActive,#createInvoiceActive,#invoiceActive,#noteNoteActive,#verifiedEmailActive,#re-activateCampActive').removeClass("btn-active");
    $("#accountActive").addClass("btn-active");
    showLoader();
    $.ajax({
        type: "GET",
        //data: JSON.stringify({ id: $("#hdnUserId").val() }),
        contentType: "application/json; charset=utf-8",
        url: "/Admin/ManageUser/AccountDetails/" + $("#hdnUserId").val(),
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                $("#containerDiv").empty().html(result);
                appyPhoneFormat();
                HideLoader();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function onUpdateAccountBegin() {
    var result = true;
    if (!emptyStringValidator($('#ChangePassword'))) {
        $('#chError').empty();
    }
    else {
        if (!PasswordValidator($('#ChangePassword'))) {
            $('#ChangePassword').focus();
            $('#chError').html("Your password must contain at least 6 chars, one uppercase, 1 numeric and 1 special character.");
            result = false;
        }
        else
            if (!emptyStringValidator($('#VerifyPassword'))) {

                $('#VerifyPassword').focus();
                $('#vError').html("Confirm Password is Required");
                result = false;
            }

            else {
                if (($('#ChangePassword').val().trim()) != ($('#VerifyPassword').val().trim())) {
                    $('#VerifyPassword').focus();
                    $('#vError').html("Confirm Password and Password is different");
                    result = false;
                }
                else {
                    $('#vError').empty();
                }
            }

    }

    if ($('#CardNumber').val().trim() != '') {
        var isCreditCardValid = validateCreditCardNumber($('#CardNumber').val().trim())

        if (isCreditCardValid) {

            $("#CardError").empty();



            if (!emptyStringValidator($('#ExpMonth')) || $('#ExpMonth').val() == "0") {
                $('#ExpMonth').val("")
                $('#ExpMonth').focus();
                $("#ExpMonthError").html("Expiry Month is required.");
                result = false;
            }
            else {
                $("#ExpMonthError").empty();
            }
            if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
                $("#ExpMonthError").empty();
                result = false;
            }
            else {
                var current_year = new Date().getFullYear();
                if (($('#ExpYear').val() < current_year) || ($('#ExpYear').val() > 2039)) {
                    $("#ExpYearError").html(`Year should be in range ${current_year} to 2039 year`);
                    result = false;
                } else {
                    $("#ExpYearError").empty();
                }
            }

        }
        else {
            $('#CardNumber').focus();
            $("#CardError").html("Card Number is invalid.");
            result = false;
        }


    }
    if (!emptyStringValidator($('#txtZip'))) {
        $('#ZipError').empty();
    }
    else {
        if (!zipcodeValidator($('#txtZip'))) {
            $('#txtZip').focus();
            $('#ZipError').html("Invalid Postal Code");
            result = false;
        }
        else {
            $('#ZipError').empty();
        }
    }
    if (!emptyStringValidator($('#BillingZip'))) {
        $('#Ziperror').empty();
    }
    else {
        if (!zipcodeValidator($('#BillingZip'))) {
            $('#BillingZip').focus();
            $('#Ziperror').html("Invalid Postal Code");
            result = false;
        }
        else {
            $('#Ziperror').empty();
        }
    }




    if (result == true) {
        showLoader();
    }
    return result;

}

function appyPhoneFormat() {
    phoneFormat($('#txtPhone'));
    phoneFormat($('#txtBillingPhone'));
    $("#jsGrid").empty();

}

function onUpdateAccountSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", RedirctToDashboard);

    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onUpdateAccountFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function onUpdateManageSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", RedirctToDashboard);

    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onUpdateManageFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

/* Account Details functions end*/

/*Note functions start */

function getNoteView() {
    showLoader();
    $('#accountActive,#managebillingActive,#createInvoiceActive,#invoiceActive,#verifiedEmailActive,#re-activateCampActive').removeClass("btn-active");
    $("#noteNoteActive").addClass("btn-active");
    id = $("#hdnUserId").val();
    $.ajax({
        url: "/Admin/ManageUser/GetNoteView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "userid": id }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            HideLoader();
            $("#containerDiv").empty().html(result);
            initSignalDatePicker($('#followDT'));
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onAddNote() {
    var result = true;

    if (!emptyStringValidator($('#txtNotes'))) {
        $('#txtNotes').focus();
        $("#noteError").html("Note Description is required.");
        result = false;
    }
    else {
        $("#noteError").empty();
    }

    if (!emptyStringValidator($('#followDT'))) {
        $('#followDT').focus();
        $("#followError").html("FollowUp Date is required.");
        result = false;
    }
    else {
        $("#followError").empty();
    }

    if (!emptyStringValidator($('#notetype')) || $('#notetype').val() == "Select") {
        $('#notetype').focus();
        $("#noteTypeError").html("FollowUp Type is required.");
        result = false;
    }
    else {
        $("#noteTypeError").empty();
    }
    if (!emptyStringValidator($('#timeZone')) || $('#timeZone').val() == "Select")
    {
        $('#TimeZone').focus();
        $("#timeZoneError").html("Time Zone is required.");
        result = false;
    }
    else
    {
        $("#timeZoneError").empty();
    }
    if (result == true) {
        showLoader();

    }

    return result;
}

function onAddNoteSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        Clear();
        GenerateAlert(data.status, data.msg, "success", getNoteView);
    }
    else {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddNoteFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function editNote(oNote) {
    if (oNote)
    {
        $("#newNote_NoteId").val(oNote.NoteId);
        $("#newNote_UserId").val(oNote.UserId);
        $("textarea[name='newNote.NoteDescription']").val(oNote.NoteDescription);
        $("input[name='newNote.FollowUpDate'").val(moment(oNote.FollowUpDate).format('MM/DD/YYYY hh:mm A'));
        $("select[name='newNote.NoteTypeId'").val(oNote.NoteTypeId);
        $("select[name='newNote.TimeZone']").val(oNote.TimeZone)
        $("#buttonLabel").empty().html('Update');
    }
}

function deleteNoteConfirmation(noteId) {
    bootbox.confirm({
        message: "Are you sure want to delete this note?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                showLoader();
                deleteNote(noteId);
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function deleteNote(noteId) {
    $.ajax({
        url: "/Admin/ManageUser/DeleteNote",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "noteId": noteId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", getNoteView);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}
function Clear() {
    $("#newNote_NoteId").val(0);
    $("textarea[name='newNote.NoteDescription']").val('');
    $("input[name='newNote.FollowUpDate'").val('');
    $("select[name='newNote.NoteTypeId'").val('');
    $("select[name='newNote.TimeZone']").val('');
    $("#btnNoteSubmit").html("Update");

}
/*Note functions end */

/*other utility functions start*/




function emptycontainerGridDiv() {
    $("#containerDiv").empty();
}

function RedirctToDashboard() {
    window.location.href = $("#hdnUserId").val();
}

function getManageBilling() {
    $('#accountActive,#createInvoiceActive,#invoiceActive,#re-activateCampActive,#noteNoteActive,#verifiedEmailActive').removeClass("btn-active");
    $("#managebillingActive").addClass("btn-active");
    showLoader();
    $.ajax({
        type: "GET",
        //data: JSON.stringify({ id: $("#hdnUserId").val() }),
        contentType: "application/json; charset=utf-8",
        url: "/Admin/ManageUser/ManageBillingDetails/" + $("#hdnUserId").val(),
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                $("#containerDiv").empty().html(result);
                appyPhoneFormat();
                HideLoader();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }

    });
}

function getInvoice() {
    $('#accountActive,#managebillingActive,#invoiceActive,#noteNoteActive,#verifiedEmailActive,#re-activateCampActive').removeClass("btn-active");
    $("#createInvoiceActive").addClass("btn-active");
    showLoader();
    $.ajax({
        type: "GET",
        //data: JSON.stringify({ id: $("#hdnUserId").val() }),
        contentType: "application/json; charset=utf-8",
        url: "/Admin/ManageUser/CreateInvoice/" + $("#hdnUserId").val(),
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                $("#containerDiv").empty().html(result);
                appyPhoneFormat();
                HideLoader();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }

    });

}

function getBillClient()
{
    $('#accountActive,#managebillingActive,#invoiceActive').removeClass("btn-active");
    $("#createInvoiceActive").addClass("btn-active");
    showLoader();
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/ManageUser/BillClient/" + $("#hdnUserId").val(),
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                $("#containerDiv").empty().html(result);
                appyPhoneFormat();
                HideLoader();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }

    });

}

/*other utility functions end*/

function onAddBillClientBegin()
{
    var result = true;
    if (!emptyStringValidator($('#txtAmount'))) {
        $('#txtAmount').focus();
        $("#amtError").html("Amount is required");
        result = false;
    }
    else {
        $("#amtError").empty();

    }

    if (!emptyStringValidator($('#PaymentTypeId')) || $('#PaymentTypeId').val() == "0") {
        $('#PaymentTypeId').val("")
        $('#PaymentTypeId').focus();
        $("#PaymentTypeError").html("Charge Type is required.");
        result = false;
    }
    else {
        $("#PaymentTypeError").empty();
    }

    if (!emptyStringValidator($('#CardNumber')))
    {
        $('#CardNumber').focus();
        $("#CreditError").html("Card Number is required.");
        result = false;
    }
    else {
        var isCreditCardValid = validateCreditCardNumber($('#CardNumber').val().trim())

        if (isCreditCardValid) {
            $("#CreditError").empty();
        } else {
            $('#CardNumber').focus();
            $("#CreditError").html("Card Number is invalid.");
            result = false;
        }

    }
    var current_Date = new Date().setFullYear($('#ExpYear').val(), $('#ExpMonth').val(), 1);
    var today = new Date();
    var current_year = new Date().getFullYear();
    if (!emptyStringValidator($('#ExpMonth')) || $('#ExpMonth').val() == "0") {
        $('#ExpMonth').val("")
        $('#ExpMonth').focus();
        $("#ExpMonthError").html("Expiry Month is required.");
        result = false;
    }
    else if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
        $("#ExpMonthError").empty();
        result = false;
    } else if (current_Date < today) {
        $("#ExpMonthError").html("Month should be greater than today's month");
        result = false;
    }
    else {
        $("#ExpMonthError").empty();
    }


    if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
        $('#ExpYear').val("")
        $('#ExpYear').focus();
        $("#ExpYearError").html("Expiry Year is required.");
        result = false;
    }
    else {
       
        if (($('#ExpYear').val() < current_year) || ($('#ExpYear').val() > 2039)) {
            $("#ExpYearError").html(`Year should be in range ${current_year} to 2039 year`);
            return false;
        } else {
            $("#ExpYearError").empty();
        }
    }

    if (!emptyStringValidator($('#txtName'))) {
        $('#txtName').focus();
        $("#nameError").html("Name is required");
        result = false;
    }
    else {
        $("#nameError").empty();

    }

    if (!emptyStringValidator($('#txtPhone'))) {
        $('#txtPhone').focus();
        $("#PhoneErr").html("Phone Number is required");
        result = false;
    }
    else {
        if (!mobilenoValidator($('#txtPhone'))) {
            $('#txtPhone').focus();
            $("#PhoneErr").html("Invalid Mobile Number");
            result = false;
        }
        else {
            $("#PhoneErr").empty();

        }
    }

    if (!emptyStringValidator($('#txtAddress'))) {
        $('#txtAddress').focus();
        $("#addErr").html("Address is required");
        result = false;
    }
    else {
        $("#addErr").empty();

    }
    if (!emptyStringValidator($('#txtCity'))) {
        $('#txtCity').focus();
        $("#CityError").html("City is required");
        result = false;
    }
    else {
        $("#CityError").empty();

    }
    if (!emptyStringValidator($('#txtState'))) {
        $('#txtState').focus();
        $("#StateErr").html("State is required");
        result = false;
    }
    else {
        $("#StateErr").empty();

    }
    if (!emptyStringValidator($('#txtZip'))) {
        $('#txtZip').focus();
        $("#zipErr").html("Postal Code is required");
        result = false;
    }
    else {
        if (!zipcodeValidator($('#txtZip'))) {
            $('#zipErr').focus();
            $('#zipErr').html("Invalid Postal Code");
            result = false;
        }
        else {
            $('#zipErr').empty();
        }
    }

    if (result == true) {
        showLoader();
    }
    return result;
}

function GetSubscriberList()
{
    showLoader();
    var id = $("#ManageBilling_PlanId").val();
    var APIURL = $("#APIURL").val();
    var URL = APIURL +'EmailPlans/GetPlanPricing?id=' + id;
    $.ajax({
        url: URL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "")
            {
                $("#ddlSub").empty();
                $.each(response, function (index, value) {
                    $("#ddlSub").append($("<option></option>").val(value.Id).html(value.NoOfSubscribersMax));
                });
                GetPackageDetails();
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function GetPackageDetails()
{
    showLoader();
    var url = $("#APIURL").val();
    var id = $('#ddlSub').val();
    var UserId = $('#ManageBilling_Id').val();
    var APIURL = url + 'EmailPlans/GetPlanByID?id=' + id + '&UserId=' + UserId;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": APIURL,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        if (response != null && response != "") {
            HideLoader();
            if (response.Country == "Canada")
            {
                $("#ManageBilling_PayGPrice").val(response.BillingAmount_Canada);
                $("#ManageBilling_BillingAmount").val(response.BillingAmount_Canada);
                if (response.NoOfCredits == 0) {
                    $("#ManageBilling_MonthlyCredit").val("Unlimited Emails");
                }
                else {
                    $("#ManageBilling_MonthlyCredit").val(response.NoOfCredits);
                }
            }
            else
            {
                $("#ManageBilling_PayGPrice").val(response.BillingAmount);
                $("#ManageBilling_BillingAmount").val(response.BillingAmount);
                if (response.NoOfCredits == 0) {
                    $("#ManageBilling_MonthlyCredit").val("Unlimited Emails");
                }
                else {
                    $("#ManageBilling_MonthlyCredit").val(response.NoOfCredits);
                }
            }
        }
    });
}

function bindState(CountryName) {
    if (CountryName != "United States") {
        $("#State").hide();
        $("#txtState").show();
        $("#txtState").attr('name', 'State')
    }
    else {
        $("#State").show();
        $("#txtState").attr('name', 'notState')
        $("#txtState").hide();
    }

}

/*Verified Email Bind*/

function bindVerifiedEmail() {
    $('#managebillingActive,#createInvoiceActive,#accountActive,#noteNoteActive,#invoiceActive,#re-activateCampActive').removeClass("btn-active");
    $("#verifiedEmailActive").addClass("btn-active");
    var dColumnsDef = [
    
    ];
    var columns = [
        { "data": "FromEmailAddress", "title": "Email", "name": "FromEmailAddress", "autoWidth": true },
        {
            data: null, "title": "Status", "autoWidth": true, render: function (data, type, row) {
                return row.Status == true ? "Active" : "InActive";
            }
        }
    ]
    $("#containerDiv").html('<table id="verifiedEmailsTable" class="display new_grid" style="width:100%"></table>');
    InitDataTable($("#verifiedEmailsTable"), "/Admin/ManageUser/GetVerifiedEmail", JSON.stringify({ userid: $("#hdnUserId").val() }), columns, dColumnsDef, [[0, 'desc']], 0, false, false);
}

/*Re-Activate Campaign*/
function reactivateCampaign()
{
    $('#accountActive,#managebillingActive,#invoiceActive,#noteNoteActive,#createInvoiceActive,#bindVerifiedEmail').removeClass("btn-active");
    $("#activateCampActive").addClass("btn-active");
    showLoader();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/ManageUser/GetReActivateCampaign/",
        dataType: 'json',
        data: JSON.stringify({ userid: $("#hdnUserId").val() }),
        success: function (result) {
            if (result.status != "Error") {
                HideLoader();
                GenerateAlert("", result.msg, "success", RedirctToDashboard);
            }
            else
            {
                HideLoader();
                GenerateAlert("", result.msg, "info", RedirctToDashboard);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }

    });

}