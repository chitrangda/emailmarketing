﻿var oConfig = [];
var xAxisData = [];
var yAxisData = [];

//$(document).ready(function ()
//{
//    showLoader();
//    getOverview(CampaignId,CampaignName);
    
//});
function getCampaignGraph(CampaignId, SentDate)
{
   
    $.ajax({
        type: "POST",
        url: "/User/Report/GetCamapignGraph?CampaignId=" + CampaignId +'&SentDate=' + SentDate ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result)
        {
            HideLoader();
            var json = $.parseJSON(result);
            xAxisData = [];
            yAxisData = [
                {
                    name: 'Open', data: [], color: '#17A2B9'
                },
                {
                    name: 'Clicks', data: [], color: '#FEC107'
                }];

            for (j = 0; j < json.length; j++) {
                var date = new Date(json[j].EventDate);
                var dd = date.getDate();
                dd = dd < 10 ? "0" + dd : dd;
                var MM = date.getMonth()+1;
                MM = MM < 10 ? "0" + MM : MM;
                var YYYY = date.getUTCFullYear();
                date = YYYY + '-' + MM + '-' + dd;
                xAxisData.push(date);
                yAxisData[0].data.push(
                    json[j].UniqueOpens
                );
                yAxisData[1].data.push(
                    json[j].UniqueClicks
                );
            }

            // create the chart
            var chart = new Highcharts.chart('Chartcontainer1', {
                chart: {
                    type: 'column'
                },
                lang: {
                    noData: 'No data to display'

                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: xAxisData,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
                    crosshair: true,
                },
                yAxis: {
                    tickInterval: 1,
                    title: {
                        text: 'Count',
                    },
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.9,
                        pointWidth: 20,
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    },
                    //series: {
                    //    borderRadiusTopLeft: '40%',
                    //    borderRadiusTopRight: '40%',
                    //    borderRadiusBottomLeft: '40%',
                    //    borderRadiusBottomRight: '40%'
                    //},


                },
                legend: {
                    align: 'left',
                    verticalAlign: 'top',
                   // y: -30,
                },
                series: yAxisData
            });
        }
    });
}

function viewEmail(campaignId, CampaignName) {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Report/ViewEmail",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ "campaignId": campaignId }),
        cache: false,
        success: function (result) {

            $('#ViewEmailDiv').append(result);
            $('#Email').modal('show');
            $("#lblCampName").empty().html('<i class="fa fa-check-square-o" aria-hidden="true"></i>' + " " + "Campaign- " + CampaignName);
            HideLoader();

        }
    });
}

function getOverview(campaignId,CampaignName)
{
    
    $("#lblReport").html('<i class="fa fa-check-square-o" aria-hidden="true"></i>' + " " + "Report- " + CampaignName);
    $.ajax({
        type: "POST",
        url: "/User/Report/OverViewCamapign",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ "CampaignId": campaignId }),
        cache: false,
        success: function (result)
        {

            $('#Container').empty().html(result);
            HideLoader();
            getCampaignGraph(CampaignId, SentDate);


        },
         error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function getDetails(campaignId , campaignName)
{
   
    showLoader();
    $("#lblReport").html('<i class="fa fa-check-square-o" aria-hidden="true"></i>' + " " + "Report- " + campaignName);
    $.ajax({
        type: "POST",
        url: "/User/Report/Details",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ "CampaignId": campaignId }),
        cache: false,
        success: function (result)
        {

            $('#Container').empty().html(result);
            HideLoader();

        },
         error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function getActivity(campaignId, SentDate, Text, Subject, Receipents, FromEmail,CampaignName ,PageNumber,name )
{
    
    showLoader();
    $("#lblReport").html('<i class="fa fa-check-square-o" aria-hidden="true"></i>' + " " + "Report- " + CampaignName);
    $.ajax({
        type: "POST",
        url: "/User/Report/ReportActivity",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({
            "CampaignId": campaignId, "SentDate": SentDate, "Text": Text, "Subject": Subject, "Recipient": Receipents, "Page": PageNumber, "FromEmail": FromEmail,
            "CampaignName": CampaignName , "Search" :name
        }),
        cache: false,
        success: function (result) {

            $('#Container').empty().html(result);
            HideLoader();
        },
         error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function getDetailsReportSendTo(email,CampaignId ,FromEmail)
{
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Report/DetailsReportSendTo",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ "Email": email, "CampaignId": CampaignId, "FromEmail": FromEmail }),
        cache: false,
        success: function (result)
        {
            HideLoader();
            $("#divDetailModal").html(result);
            $('#details').modal('show');
        },
         error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function getExport(CampaignId,SentDate,Text,Recipient,Page)
{
  
    var offSet = new Date().getTimezoneOffset();
    offSet = (offSet * (-1));
    sDate = moment(SentDate).add(offSet, "minutes").format('MM-DD-YYYY, h:mm:ss A');
    var name = $('#email').val().trim();
    location.href = '/User/Report/ExportReportDetails?CampaignId=' + CampaignId + '&offset=' + offSet +'&SentDate=' + sDate + '&Text=' + Text + '&Recipient=' + Recipient + '&Page=' + 1 + '&Search=' + name ;
}

function ReportPagging(direction,text)
{
    var PageNumberControl = $("#PageNumber").val().trim();
    var CamapignId = $("#CampaignId").val().trim();
    var Subject = $("#Subject").val().trim();
    var Recipients = $("#Recipients").val().trim();
    var SentDate = $("#SentDate").val().trim();
    var FromEmail = $("#FromEmail").val().trim();
    var CampaignName = $("#CampaignName").val().trim();

    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    ActivityPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    getActivity(CamapignId, SentDate, text, Subject, Recipients, FromEmail,CampaignName,$("#PageNumber").val());
}

function ActivityPage(PageControl, page, TotalRecord) {

    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }
    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function getExportCampaign(CampaignId, SentDate)
{
    var offSet = new Date(SentDate).getTimezoneOffset();
    offSet = (offSet * (-1));
    sDate = moment(SentDate).add(offSet, "minutes").format('MM-DD-YYYY, h:mm:ss A');
    location.href = '/User/Report/ExportCampaignDetail?CampaignId=' + CampaignId + ' &SentDate=' + sDate ;
}

function getLinks(CampaignId, SentDate, Subject, Receipents, FromEmail ,CampaignName)
{
    showLoader();
    $("#lblReport").html('<i class="fa fa-check-square-o" aria-hidden="true"></i>' + " " + "Report- " + CampaignName);
    $.ajax({
        type: "POST",
        url: "/User/Report/Links",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({
            "CampaignId": CampaignId, "CampaignName": CampaignName, "SentDate": SentDate, "Subject": Subject, "Receipents": Receipents, "FromEmail": FromEmail}),
        cache: false,
        success: function (result) {

            $('#Container').empty().html(result);
            HideLoader();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SearchActivity(oControl,text) {
    showLoader();
    var Page = 1;
    var name = $(oControl).val().trim();
    var CamapignId = $("#CampaignId").val().trim();
    var Subject = $("#Subject").val().trim();
    var Recipients = $("#Recipients").val().trim();
    var SentDate = $("#SentDate").val().trim();
    var FromEmail = $("#FromEmail").val().trim();
    var CampaignName = $("#CampaignName").val().trim();
    getActivity(CamapignId, SentDate, text, Subject, Recipients, FromEmail, CampaignName, Page, name);
}
