﻿var selectedList = [];

function UnSubscriberPage(PageControl, page, TotalRecord) {
    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }



    var FirstRec = (page - 1) * 10;
    var LastRec = (page * 10);
    var CheckNextDisabled = FirstRec + 10;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}
function Pagging(direction)
{
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    UnSubscriberPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#SortSubscriber').find('option:selected').val();
    getUnsubscriberGridView($("#PageNumber").val(), Order);
}
function SortUnsubscriber(input)
{
    showLoader();
    var PageNumber = $("#PageNumber").val().trim();
    var sortOrder = $(input).find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    getUnsubscriberGridView(PageNumber, sortOrder);
}
function getUnsubscriberGridView(PageNumber, sortOrder, search) {
    var ListId = $("#ListId").val();
    $.ajax({
        url: '/User/Subscriber/getUnSubscriberListGridView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ listId: ListId, Page: PageNumber, order: sortOrder, Search: search }),
        async: true,
        success: function (result) {
            if (result)
            {
                HideLoader();
                $("#gridSubscriberList").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function LoadUnSubscriberHTML(AreaName, ControllerName, ActionName, listId, listName)
{
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?listId=${listId}&Page=&listname=${listName}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
           }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SelectAllUnCheckBox(input)
{
    //$(".SelectedCheckBox").click();
    selectedList = [];
    if ($(input).is(":checked") == true) {
        $(".SelectedCheckBox").each(function () {
            selectedList.push($(this).val());
            $(this).prop("checked", true);
        });
    }
    else {
        $(".SelectedCheckBox").each(function () {
            $(this).prop("checked", false);
            selectedList = [];
        });
    }
}

function AllSubscriber()
{
    var control = $(".SelectedCheckBox");
    var subscriberId = [];
    $.each(control, function (index, value) {
        if (value.checked) {
            subscriberId.push(value.value);
        }
    });

    if (subscriberId.length > 0) {
        bootbox.confirm({
            message: "Are you sure want to subscribe this unsubscriber?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    AJAXUnSubscriberCaller("User", "Subscriber", "AllSubscriber", subscriberId, $("#ListId").val());
                } else {
                    GenerateAlert("Your subscriber is safe", "", "success", false);
                    LoadUnSubscriberHTML("User", "Subscriber", "ViewSubscriber", $("#ListId").val(), decodeURI(getUrlVars()["name"]));

                }
            }
        });

    }
    else {
        GenerateAlert("Please select to delete the subscriber!", "", "info", false);
    }

}


function AJAXUnSubscriberCaller(AreaName, ControllerName, ActionName, data, listid)
{
    showLoader();
    var ListId = listid;
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data, "ListId": listid },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result.status)
            {
                GenerateAlert(result.msg, "", "success", false);
                LoadUnSubscriberHTML("User", "Subscriber", "ViewUnSubscriber", ListId, decodeURI(getUrlVars()["name"]));
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}
function SearchUnSubscriber(oControl)
{
    showLoader();
    var Page = $("#PageNumber").val().trim();
    var search = $(oControl).val().trim();
    getUnsubscriberGridView(Page, "NameAsc", search)
}


function selectchk() {
    selectedList = [];
    $(".SelectedCheckBox").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
        }
    });
    if ($(".SelectedCheckBox").length == selectedList.length) {
        $("#MasterCheckbox").prop("checked", true);
    }
    else {
        $("#MasterCheckbox").prop("checked", false);
    }
}