﻿$(document).ready(function (e) {
    if (curView == 'edit') {
        getAddEditSaleRepView(salesRepId);
    }
    else if (curView == "add") {
        getAddEditSaleRepView();
    }
    else {
        getSalesRepView();
    }
});
function getSalesRepView() {
    $.ajax({
        url: '/Admin/SalesRep/SalesRepView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                $("#createRepActive").removeClass("active");
                $("#salesRepActive").addClass("active");
                $("#ContainerDiv").empty().html(result);
                bindSalesRepGrid();

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}


function bindSalesRepGrid() {
    var dColumnsDef = [
        {
            "targets": [0],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }
    ];

    var columns = [
        {
            data: null, title: "Action", render: function (data, type, row) {
                return '<a href="#!" onclick="getUpdateStatusModal(\'' + row.id + '\',\'' + row.IsActive + '\')"><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""></a>';
            }
        },
        {
            data: null, title: "Active/InActive", render: function (data, type, row) {
                if (row.IsActive) {
                    return "<span>Active</span>";

                }
                else {
                    return "<span>InActive</span>";

                }
            }
        },
        {
            data: null, title: "Rep Name", render: function (data, type, row) {
                return "<a class='waves-effect waves-light' href='#!' onclick='getAddEditSaleRepView(\"" + row.id.trim() + "\")'>" + row.FirstName + " " + row.LastName + "</a><br/><a href='/SalesRep/Dashboard/Index/" + row.id + "'>Login</a>";
            }
        },
        { "data": "SpentTotal", "title": "Spent Total", "name": "SpentTotal", "autoWidth": true },
        { "data": "NoOfClients", "title": "No Of Clients", "name": "NoOfClients", "autoWidth": true },
        {
            data: null, title: "View Rep Activity", render: function (data, type, row) {
                if (row.ViewRepActivity) {
                    return "<input type='checkbox' class='with-gap' id='chkViewRepActivity" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + false + "\",\"ViewRepActivity\")' checked><label class='' for='chkViewRepActivity" + row.id + "'></label>";
                }
                else {
                    return "<input type='checkbox' class='with-gap' id='chkViewRepActivity" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + true + "\" ,\"ViewRepActivity\")'><label class='' for='chkViewRepActivity" + row.id + "'></label>";
                }
            }
        },
        {
            data: null, title: "Can Export", render: function (data, type, row) {
                if (row.CanExport) {
                    return "<input type='checkbox' class='with-gap' id='chkCanExport" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + false + "\",\"CanExport\")' checked><label class='' for='chkCanExport" + row.id + "'></label>";
                }
                else {
                    return "<input type='checkbox' class='with-gap' id='chkCanExport" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + true + "\",\"CanExport\")'><label class='' for='chkCanExport" + row.id + "'></label>";
                }
            }
        },
        {
            data: null, title: "Export Lead", render: function (data, type, row) {
                if (row.ExportLead) {
                    return "<input type='checkbox' class='with-gap' id='chkExportLead" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + false + "\",\"ExportLead\")' checked><label class='' for='chkExportLead" + row.id + "'></label>";
                }
                else {
                    return "<input type='checkbox' class='with-gap' id='chkExportLead" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + true + "\",\"ExportLead\")'><label class='' for='chkExportLead" + row.id + "'></label>";
                }
            }
        },
        {
            data: null, title: "Manager", render: function (data, type, row) {
                if (row.Manager) {
                    return "<input type='checkbox' class='with-gap' id='chkManager" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + false + "\",\"Manager\")' checked><label class='' for='chkManager" + row.id + "'></label>";
                }
                else {
                    return "<input type='checkbox' class='with-gap' id='chkManager" + row.id + "' onchange='getUpdateStatus(\"" + row.id + "\" , \"" + true + "\",\"Manager\")'><label class='' for='chkManager" + row.id + "'></label>";
                }
            }
        }
    ];

    InitDataTable($("#userTable"), "/Admin/SalesRep/getAllSalesRep", null, columns, dColumnsDef, [[2, 'desc']], 0, false, false);
    //Init Data Table server side paging
    //$("#userTable").DataTable({
    //    "serverSide": true,
    //    "destroy": true,
    //    "processing": true, 
    //    "paging": true,
    //    "sScrollX": true,
    //    "sScrollY": "500px",
    //    "searching": false,
    //    "order": [[2, 'desc']],
    //    "lengthChange": false,
    //    "bScrollCollapse": true,
    //    fixedColumns: {
    //        leftColumns: 6,
    //    },
    //    "ajax": {
    //        url: "/Admin/SalesRep/getAllSalesRep",
    //        //data: JSON.stringify({ page: 0}),
    //        type: "POST",
    //    },
    //    "columns": columnsArr,
    //    "pageLength": 2,
    //    "PaginationType": "full_numbers",
    //    'Language': {
    //        'loadingRecords': '&nbsp;',
    //        'processing': 'Loading...'
    //    },
    //    buttons: [
    //        'copyHtml5',
    //        'excelHtml5',
    //        'csvHtml5',
    //        'pdfHtml5'
    //    ]

    //});
}

function getSalesRepData(page) {
    $.ajax({
        url: '/Admin/SalesRep/getAllSalesRep/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ Page: page }),
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
            }
        },
        error: function () {
            HideLoader();
        }
    })
}

function getAddEditSaleRepView(id) {
    if (id === undefined) {
        id = "";
    }
    $.ajax({
        url: '/Admin/SalesRep/AddEditSalesRep/' + id,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                $("#salesRepActive").removeClass("active");
                $("#createRepActive").addClass("active");
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function onAddBegin() {
    var result = true;
    if (!emptyStringValidator($('#txtFname'))) {
        $('#txtFname').focus();
        $("#fnError").html("First Name is required.");
        result = false;
    }
    else {
        $("#fnError").empty();
    }
    if ($('#txtEmail').length > 0) {
        if (!emptyStringValidator($('#txtEmail'))) {
            $('#txtEmail').focus();
            $("#emailError").html("Email is required.");
            result = false;
        }
        else {
            if (!EmailValidator($('#txtEmail'))) {
                $('#txtEmail').focus();
                $("#emailError").html("Invalid email address.");
                result = false;
            }
            else {
                $("#emailError").empty();
            }
        }
    }

    if ($("#Id").val() == '') {
        if (!emptyStringValidator($('#txtPswd'))) {
            $('#txtPswd').focus();
            $("#pswdError").html("Password is required.");
            result = false;
        }
        else {
            if (emptyStringValidator($('#txtPswd')) && !PasswordValidator($('#txtPswd'))) {
                $('#txtPswd').focus();
                $("#pswdError").html("Your password must contain at least 6 chars, one uppercase, 1 numeric and 1 special character.");
                result = false;
            }
            else {
                $("#pswdError").empty();
            }
        }

        if (!emptyStringValidator($("#confirmPassword"))) {
            $('#txtConfirmPswd').focus();
            $("#confrimPswdError").html("Please verify the password");
            result = false;
        }
        else {
            if (($("#confirmPassword").val().trim() != $("#txtPswd").val().trim())) {
                $('#txtConfirmPswd').focus();
                $("#confrimPswdError").html("Password and Confirm Password mismatch!");
                result = false;
            }
            else {
                $("#confrimPswdError").empty();
            }
        }

    }
    else {
        if (emptyStringValidator($('#txtPswd'))) {
            if (!PasswordValidator($('#txtPswd'))) {
                $('#txtPswd').focus();
                $("#pswdError").html("Your password must contain at least 6 chars, one uppercase, 1 numeric and 1 special character.");
                result = false;

            }
            else if (($("#confirmPassword").val().trim() != $("#txtPswd").val().trim())) {
                $('#txtConfirmPswd').focus();
                $("#confrimPswdError").html("Password and Confirm Password mismatch!");
                result = false;
            }
            else {
                $("#pswdError").empty();
                $("#confrimPswdError").empty();
            }
        }
    }
    if (result) {
        $("#divLoader").show();

    }
    return result;

}

function onAddSucess(data) {
    $("#divLoader").hide();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", getSalesRepView);
    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddFailure(data) {
    $("#divLoader").hide();
    GenerateAlert(data.status, data.msg, "info");

}

function redirctToDashboard() {
    window.location.href = "Dashbaord";
}

function cancelAdd() {
    window.location.href = "/Admin/SalesRep";
}

function updateSalesRep(item) {
    $.ajax({
        type: "POST",
        data: JSON.stringify({ Id: item.id, FirstName: item.FirstName, LastName: item.LastName, NoOfClients: item.NoOfClients, SpentTotal: item.SpentTotal, ViewRepActivity: item.ViewRepActivity, ExportLead: item.ExportLead, Manager: item.Manager, CanExport: item.CanExport, IsActive: item.IsActive }),
        url: "/Admin/SalesRep/updateSalesRep/",
        contentType: "application/json",
        async: true,
        success: function (data) {
            GenerateAlert(data.status, data.msg, "success", getSalesRepView);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            GenerateAlert(data.status, data.msg, "info", getSalesRepView);

        }

    });
}

function getUpdateStatusModal(oId, oStatus) {

    $("#ddlStatus").val(oStatus);
    $("#hdnSalesRepId").val(oId);
    //$("#btnConfirm").attr("onclick", "changeAccountStatus('" + oId + "','" + $("#ddlStatus").val() + "')");
    $('#EditStatusModal').modal('show')


}

function changeAccountStatus() {
    $("#divLoader").show();
    var oId = $("#hdnSalesRepId").val();
    var oStatus = $("#ddlStatus").val();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/SalesRep/changeAccountStatus/",
        dataType: "json",
        data: JSON.stringify({ "id": oId, "status": oStatus }),
        async: true,
        success: function (data) {
            $("#divLoader").hide();
            $('#EditStatusModal').modal('hide')

            if (data.status != "Error") {
                GenerateAlert(data.status, "Update Successfully!", "success", bindSalesRepGrid);
            }
            else {
                GenerateAlert(data.status, data.msg, "info");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            return false;
        }
    });
}

function getUpdateStatus(oId, oControl, field) {
    // console.log(oControl.val());
    $("#divLoader").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Admin/SalesRep/changeStatus/",
        dataType: "json",
        data: JSON.stringify({ "id": oId, "value": oControl, "field": field }),
        async: true,
        success: function (data) {
            $("#divLoader").hide();
            if (data.status != "Error") {
                GenerateAlert(data.status, data.msg, "success", bindSalesRepGrid);
            }
            else {
                GenerateAlert(data.status, data.msg, "info");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#divLoader").hide();
            return false;
        }
    });
}
