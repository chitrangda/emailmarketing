﻿$(document).ready(function () {
    LoadHTMLPage("User", "Report", "SummaryReport", [], [], "Get", $("#ContainerDiv"));

});
function CampaignPagging(direction) {
    showLoader();
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    CampaignPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#sortCampaign').find('option:selected').val();
    getCampaignSummaryReport($("#PageNumber").val())
}

function CampaignPage(PageControl, page, TotalRecord) {

    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }

    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < pageSize) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)

}
function getCampaignSummaryReport(pageNo) {
 
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/User/Report/CampaignList',
        datatype: "html",
        data: JSON.stringify({ "startDate": $("#fromDate").val(), "endDate": $("#toDate").val(), "pageNumber": pageNo }),
        async: true,
        success: function (data) {
            $("#reportContent").empty().html(data);
            HideLoader();
        }
    });
}

function searchReport() {
    showLoader();

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '/User/Report/SummaryReport',
        datatype: "html",
        data: JSON.stringify({ "startDate": $("#fromDate").val(), "endDate": $("#toDate").val()}),
        async: true,
        success: function (data) {
            $("#ContainerDiv").empty().html(data);
            $("#btnClear").prop("disabled", false);
            HideLoader();
        }
    });
}

function clearFilter() {
    $("#fromDate").val('');
    $("#toDate").val('');
    LoadHTMLPage("User", "Report", "SummaryReport", [], [], "Get", $("#ContainerDiv"));

}

