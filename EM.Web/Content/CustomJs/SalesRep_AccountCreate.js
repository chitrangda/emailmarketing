﻿function onAddUserBegin()
{
    var result = true;
    if (!emptyStringValidator($('#FirstName'))) {
        $('#FirstName').focus();
        $("#fError").html("First Name is required");
        result = false;
    }
    else {
        $("#fError").empty();
    }
    if (!emptyStringValidator($('#CompanyName'))) {
        $('#CompanyName').focus();
        $("#cmError").html("Company Name is required");
        result = false;
    }
    else {
        $("#cmError").empty();
    }
    if (!emptyStringValidator($('#txtUserEmail'))) {
        $('#txtUserEmail').focus();
        $("#eError").html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtUserEmail'))) {
            $('#txtUserEmail').focus();
            $("#eError").html("Invalid Email");
            result = false;
        }
        else {
            $("#eError").empty();
        }
    }
    

    if (!emptyStringValidator($('#ddlplanlist')) || $('#ddlplanlist').val() == "Select")
    {
        $('#ddlplanlist').focus();
        $("#planError").html("Choose Monthly Plan");
        result = false;
    }
    else {
        $("#planError").empty();
    }
    if (result == true) {
        showLoader();
    }
    return result;  

}

function onAddUserSucess(data)
{
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", redirecttoDashboard);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddUserFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}
function redirecttoDashboard() {
    window.location.href = "/SalesRep/Dashboard/Index";
}


$(document).ready(function (e) {
    $('#CreateAccount').addClass("active");
    $('#MobilePhone').usPhoneFormat({
        format: '(xxx) xxx-xxxx',
    })
})

$(document).on("change", "#ddlplanlist", function () {
    if ($("#ddlplanlist").val() != "") {
        GetSubscriber();
    }
    else {
        $("#ddlSublist").empty();
        $("#ddlSublist").append($("<option></option>").html("--Select--"));
    }
});
    
function GetSubscriber() {
    showLoader();
    var id = $("#ddlplanlist").val();
    var APIURL = $("#APIURL").val();
    var URL = APIURL + 'EmailPlans/GetPlanPricing?id=' + id;
    $.ajax({
        url: URL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $("#ddlSublist").empty();
                $.each(response, function (index, value) {
                    $("#ddlSublist").append($("<option></option>").val(value.Id).html(value.NoOfSubscribersMax));
                });
                GetPackageDetails();
            }
        },
        failure: function (failureData) {
            HideLoader();
        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function GetPackageDetails() {
    showLoader();
    var url = $("#APIURL").val();
    var id = $('#ddlSublist').val();
    var APIURL = url + 'EmailPlans/GetPlanByID?id=' + id;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": APIURL,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        if (response != null && response != "") {
            HideLoader();
            //$("#ManageBilling_PayGPrice").val(response.BillingAmount);
            //$("#ManageBilling_BillingAmount").val(response.BillingAmount);
            //$("#ManageBilling_MonthlyCredit").val(response.NoOfCredits);
        }
    });
}