﻿function onAddIP_Pool()
{
    var result = true;
    if (!emptyStringValidator($('#Name'))) {
        $('#Name').focus();
        $("#lblName").html("IP Pool Name is required");
        result = false;
    }
    else {
        $("#lblName").empty();

    }
    if (result == true) {
        showLoader();
    }

    return result;

}

function onAddIP_PoolSuccess(data) {
    HideLoader();
    if (data.status != "Error") {
        Clearcontrol();
        GenerateAlert(data.status, data.msg, "success", bindIPPoolData);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddIP_PoolFailure(data)
{
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function redirectToControl()
{
    window.location.href = "/Admin/IPPool/PoolData";
}

function bindIPPoolData() {
    var dColumnsDef =
        [{

            "targets": [1],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }];
    var columns =
        [
            { "data": "Name", "title": "IPPOOL", "name": "Name", "autoWidth": true },
            {
                data: null, "title": "Action", render: function (data, type, row) {

                    var clickEditFuncton = 'getEditIPPool(' + data.Id + ')';

                    var clickDeleteFuncton = 'getDeleteIPPool(' + data.Id + ')';

                    return '<a href="#!" onclick=\'' + clickEditFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a> <a href="#!" onclick=\'' + clickDeleteFuncton + '\'><img src="/Content/imagesHome/deletet-ic.png" style="width:20px;" alt=""/></a>';


                }
            },
        ];

    InitDataTable($("#pooldataTable"), "/Admin/IPPOOL/GetIPPoolList/", null, columns, dColumnsDef, [[0, "desc"]], 0, false, true);
}

$(document).ready(function (e) {
    $('#IPPoolActive').addClass("active");
    bindIPPoolData();
});

function getEditIPPool(Id)
{
    showLoader();
    $.ajax({
        url: "/Admin/IPPool/EditIPPool",
        type: 'Post',
        dataType: 'html',
        data: JSON.stringify({ Id: Id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#editContainer").html(response);
            $('#editIPPool').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onEditIPPool() {
    var result = true;
    if (!emptyStringValidator($('#txtIPPoolName'))) {
        $('#txtIPPoolName').focus();
        $("#IPPoolError").html("IP Pool Name is required");
        result = false;
    }
    else {
        $("#IPPoolError").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}

function onEditIP_PoolSuccess(data) {
    HideLoader();
    if (data.status != "Error") {
        $('#txtIPPoolName').val('');
        $("#editIPPool .close").click();
        GenerateAlert(data.status, data.msg, "success", bindIPPoolData);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function Clearcontrol()
{
    $('#Name').val('');

}

function getDeleteIPPool(Id) {

    bootbox.confirm({
        message: "Are you sure want to delete this IP Pool?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if (result) {

                DeleteIPPool(Id)
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function DeleteIPPool(Id) {
    showLoader();
    $.ajax({
        url: "/Admin/IPPool/DeleteIPPool",
        type: "Post",
        data: JSON.stringify({ Id: Id }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error") {
                    GenerateAlert(result.status, result.msg, "success", bindIPPoolData);


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}