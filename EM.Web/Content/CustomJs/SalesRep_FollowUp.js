﻿

$(document).ready(function (e)
{
    $('#followUpTab').addClass("active");
    $('#btnPast').addClass("btnpast");
    $('#btnCurrent').addClass("btnCurrent");
    bindFollowUpGrid();
    $("body").tooltip({
        selector: '[data-toggle="tooltip"]',
        title: 'Is followup complete?'
    });
});

function bindFollowUpGrid(DateFrom, DateTo,isCurrent) {
    var dColumnsDef = [];
    var columns =
        [
            {
                data: "FollowUpDate", "title": "FollowUp Date", "autoWidth": true, render: function (data, type, row) {

                    var clickFuncton = 'IsComplete("' + row.Id + '")';
                     
                    return row.FollowUpDate + '<a href="#!" onclick=\'' + clickFuncton + '\' data-toggle="tooltip"> <i class="fa fa-calendar-check-o"></i></a>';
                }
            },
            
            { "data": "FollowUpTime", "title": "FollowUp Time", "name": "FollowUpTime", "autoWidth": true },
            {
                data: "Action", "title": "Action", "autoWidth": true, render: function (data, type, row) {
                    var clickFuncton = 'getEditFollowUpModal("' + row.Id + '")';

                    return '<a href="#!" onclick=\'' + clickFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a>';
   }
            },
            { "data": "RepName", "title": "Sales Rep", "name": "RepName", "autoWidth": true },
            {
                data: "AccountName", "title": "Account Name", "autoWidth": true, render: function (data, type, row) {
                    return "<a class='waves-effect waves-light' href='/SalesRep/Dashboard/AccountDetails/" + row.Id + "'>" + row.AccountName + "(" + row.Email + ")"+"</a><br/><a href='/User/Dashboard/Index/" + row.Id + "'>Login</a>";
                }
            },
            {
                data: "null", "title": "Notes", render: function (data, type, row) {
                    return '<a href="#!" onclick="getFollowupNoteModalPopup(\'' + row.Id + '\')"><img src="/Content/imagesHome/note-ic.png" style="width:20px;" alt=""/></a>';
                }
            },
            { "data": "ClientType", "title": "Client Type", "name": "ClientType", "autoWidth": true },
            { "data": "PlanName", "title": "Member Type", "name": "PlanName", "autoWidth": true },
            { "data": "PromotionCode", "title": "Promotion Code", "name": "PromotionCode", "autoWidth": true },
            { "data": "totalSpendCredits", "title": "Spent Total", "name": "totalSpendCredits", "autoWidth": true },
            {
                data: "AccountStatus", "title": "Account Status", render: function (data, type, row) {
                    if (row.AccountStatus == 1) {
                        return "Active"
                    }
                    else {
                        return "InActive";
                    }
                }
            },
            { "data": "RegisteredDate", "title": "Date Registered", "name": "RegisteredDate", "autoWidth": true },
           

        ];

    var param = JSON.stringify({ "dateFrom": DateFrom, "dateTo": DateTo, "IsCurrent": isCurrent })
    InitDataTable($("#FollowTable"), "/SalesRep/FollowUp/GetFollowUp/", param, columns, dColumnsDef, [[0, "desc"]], 0, true, true);
    BindFollowUpTotal("/SalesRep/FollowUp/GetFollowUp/", param);

}
function BindFollowUpTotal(url, urlParam) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        datatype: "json",
        data: urlParam,
        success: function (response) {
            $("#DivRepActivityTotalRecords").text(response.length);
            if (response.length > 0) {
                $("#followExport").removeAttr('disabled');
                $("#followExport").css('opacity', 'initial');
            }
        },
        failure: function (failureData) {


        },
        error: function (errorData) {

            //alert(errorData);
        }
    });
}


function getEditFollowUpModal(id) {
    showLoader();
    $.ajax({
        url: "/SalesRep/FollowUp/EditFollowUp",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "id": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divEditModal").html(response);
            $('#editFollowUpModal').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onBegin()
{
    showLoader();
}

function onSuccess(data)
{
    HideLoader();
    $('#editFollowUpModal').modal('hide');
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", bindFollowUpGrid);
    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onFailure(data)
{
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function getFollowupNoteModalPopup(id)
{
    if (typeof id === "undefined") {
        id = $("#newNote_UserId").val();
    }
    $.ajax({
        url: "/SalesRep/FollowUp/GetNoteView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "userid": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divNoteModal").html(response);
          
            $('#followUpNote').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onAddNote() {
    var result = true;

    if (!emptyStringValidator($('#txtNotes'))) {
        $('#txtNotes').focus();
        $("#noteError").html("Note Description is required.");
        result = false;
    }
    else {
        $("#noteError").empty();
    }

    if (!emptyStringValidator($('#followDT'))) {
        $('#followDT').focus();
        $("#followError").html("FollowUp Date is required.");
        result = false;
    }
    else {
        $("#followError").empty();
    }

    if (!emptyStringValidator($('#notetype')) || $('#notetype').val() == "Select") {
        $('#notetype').focus();
        $("#noteTypeError").html("FollowUp Type is required.");
        result = false;
    }
    else {
        $("#noteTypeError").empty();
    }
    if (!emptyStringValidator($('#timeZone')) || $('#timeZone').val() == "Select") {
        $('#TimeZone').focus();
        $("#timeZoneError").html("Time Zone is required.");
        result = false;
    }
    else {
        $("#timeZoneError").empty();
    }
    if (result == true) {
        showLoader();

    }

    return result;
}

function onAddNoteSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        Clear();

        $("#followUpNote").click();
        GenerateAlert(data.status, data.msg, "success", getFollowupNoteModalPopup);
    }
    else {
        Clear();

        $("#followUpNote").click();
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddNoteFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function Clear() {
    $("#newNote_NoteId").val(0);
    $("textarea[name='newNote.NoteDescription']").val('');
    $("input[name='newNote.FollowUpDate'").val('');
    $("select[name='newNote.NoteTypeId'").val('');
    $("select[name='newNote.TimeZone']").val('');
    //$("#btnNoteSubmit").html("Update");

}

function editNote(oNote) {
    if (oNote) {
        $("#newNote_NoteId").val(oNote.NoteId);
        $("#newNote_UserId").val(oNote.UserId);
        $("textarea[name='newNote.NoteDescription']").val(oNote.NoteDescription);
        $("input[name='newNote.FollowUpDate'").val(moment(oNote.FollowUpDate).format('MM/DD/YYYY hh:mm A'));
        $("select[name='newNote.NoteTypeId'").val(oNote.NoteTypeId);
        $("select[name='newNote.TimeZone']").val(oNote.TimeZone);
        $("#btnNoteSubmit").html("Update");
    }
}

function deleteNoteConfirmation(noteId) {
    bootbox.confirm({
        message: "Are you sure want to delete this note?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                showLoader();
                deleteFollowNote(noteId);
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function deleteFollowNote(noteId) {
    $.ajax({
        url: "/SalesRep/FollowUp/DeleteNote",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "noteId": noteId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", getFollowupNoteModalPopup);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function IsComplete(id)
{
    $.ajax({
        url: "/SalesRep/FollowUp/IsComplete",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "id": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", bindFollowUpGrid);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

$("#btnFollowUp").click(function () {

    SearchFollowUp();

});

function SearchFollowUp()
{
    $('#btnPast').removeClass("btnpast btnCurrent");
    $('#btnCurrent').removeClass("btnCurrent btnpast");
    var DateFrom = $('#txtFollowUpDateRangeFrom').val();
    var DateTo = $('#txtFollowUpDateTo').val();
    bindFollowUpGrid(DateFrom, DateTo, null);

}


$("#btnPast").click(function ()
{
    $('#txtFollowUpDateRangeFrom').val('');
    $('#txtFollowUpDateTo').val('');
    $("#btnPast").addClass("btnCurrent");
    $("#btnCurrent").removeClass("btnCurrent");
    $("#btnCurrent").addClass("btnpast");
    $("#hdBtnType").val(false);
    bindFollowUpGrid(null, null, false);
});


$("#btnCurrent").click(function ()
{
    $('#txtFollowUpDateRangeFrom').val('');
    $('#txtFollowUpDateTo').val('');
    $("#btnPast").removeClass("btnCurrent");
    $("#btnPast").addClass("btnpast");
    $("#btnCurrent").addClass("btnCurrent");
    $("#hdBtnType").val(true);
    bindFollowUpGrid(null, null, true);
    
});

function ExportFollowUp()
{
    var DateFrom = $('#txtFollowUpDateRangeFrom').val();
    var DateTo = $('#txtFollowUpDateTo').val();
    var IsCurrent = $("#hdBtnType").val();
    if (IsCurrent == "undefined" || IsCurrent == null) {
        location.href = '/SalesRep/FollowUp/ExportList?DateFrom=' + DateFrom + '&DateTo=' + DateTo + '&IsCurrent=' + null;
    }
    else {
        location.href = '/SalesRep/FollowUp/ExportList?DateFrom=' + DateFrom + '&DateTo=' + DateTo + '&IsCurrent=' + IsCurrent;
    }
}

function ClearFollowUp()
{
    $('#txtFollowUpDateRangeFrom').val('');
   $('#txtFollowUpDateTo').val('');
}

function GetSubscriberFollowList()
{
    showLoader();
    var id = $("#ClientDetails_PlanId").val();
    var APIURL = $("#APIURL").val();
    var URL = APIURL + 'EmailPlans/GetPlanPricing?id=' + id;
    $.ajax({
        url: URL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $("#ClientDetails_PackageId").empty();
                $.each(response, function (index, value) {
                    $("#ClientDetails_PackageId").append($("<option></option>").val(value.Id).html(value.NoOfSubscribersMax));
                });
               // GetPackageDetailFollow();
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

//function GetPackageDetailFollow() {
//    showLoader();
//    var url = $("#APIURL").val();
//    var id = $('#ClientDetails_PackageId').val();
//    var APIURL = url + 'EmailPlans/GetPlanByID?id=' + id;
//    var settings = {
//        "async": true,
//        "crossDomain": true,
//        "url": APIURL,
//        "type": "Get",
//        "headers": {
//            "cache-control": "no-cache",
//        }
//    }
//    //$.ajax(settings).done(function (response) {
//    //    if (response != null && response != "") {
//    //        HideLoader();
//    //        $("#ClientDetails_monthlybilling").val(response.BillingAmount);
//    //        $("#ClientDetails_monthlycredit").val(response.NoOfCredits);
//    //    }
//    //});
//}

