﻿$(document).ready(function ()
{
    $('#LeadDashboardActive').addClass("active");
    bindLeadData(null, null, null, null, null);
    $("body").tooltip({
        selector: '[data-toggle="tooltip"]',
        title: 'Is followup complete?'
    });
});

function bindLeadData(LeadQualityID, TypeOfLead, IndustryId, pageIndex, pageSize) {
    var dColumnsDef =
        [
            {

                "targets": [0],
                "visible": true,
                "searchable": false,
                "orderable": false,
                "className": 'text-center'
            }
        ];
    var columns =
        [

            {
                data: null, "title": "Action", render: function (data, type, row) {

                    var clickFuncton = 'getEditLead("' + data.LeadId + '")';

                    return '<a href="#!" onclick=\'' + clickFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a>';


                }, "autoWidth": true
            },
            { "data": "SaleRepName", "title": "Sales Rep", "name": "SalesRep", "autoWidth": true },
            {
                data: "Id", "title": "Notes", render: function (data, type, row) {
                    return '<a href="#!" onclick="getLeadNoteModalPopup(\'' + row.LeadId + '\')"><img src="/Content/imagesHome/note-ic.png" style="width:20px;" alt=""/></a>';
                }
            },
            {
                data: "FollowUpDate", "title": "FollowUp Date", "autoWidth": true, render: function (data, type, row)
                {
                    if (row.FollowUpDate != null) {
                        var clickFuncton = 'IsComplete("' + row.LeadId + '")';

                        return row.FollowUpDate + '<a href="#!" onclick=\'' + clickFuncton + '\' data-toggle="tooltip"> <i class="fa fa-calendar-check-o"></i></a>';
                    }
                    else
                    {
                        return '<span></span>';
                    }
                    
                }
            },
            { "data": "FollowUpTime", "title": "Follow Up Time", "name": "FollowUpTime", "autoWidth": true },
            { "data": "LeadName", "title": "Name", "name": "LeadName", "autoWidth": true },
            { "data": "DateAdded", "title": "Date Added", "name": "DateAdded", "autoWidth": true },
            { "data": "days", "title": "DAYS", "name": "days", "autoWidth": true },
            { "data": "JobTitle", "title": "Job Title", "name": "JobTitle", "autoWidth": true },
            { "data": "Company", "title": "Company", "name": "Company", "autoWidth": true },
            { "data": "IndustryName", "title": "Industry", "name": "Industry", "autoWidth": true },
            { "data": "MobileNo", "title": "Mobile", "name": "Mobile", "autoWidth": true },
            { "data": "PhoneNo", "title": "Phone", "name": "Phone", "autoWidth": true },
            { "data": "Email", "title": "Email", "name": "Email", "autoWidth": true },
            { "data": "Website", "title": "Website", "name": "Website", "autoWidth": true },
            { "data": "LinkedIn", "title": "LinkedIn", "name": "LinkedIn", "autoWidth": true },
            { "data": "Twitter", "title": "Twitter", "name": "Twitter", "autoWidth": true },
            { "data": "Instagram", "title": "Instagram", "name": "Instagram", "autoWidth": true },
            { "data": "Facebook", "title": "Facebook", "name": "Facebook", "autoWidth": true },
            { "data": "ProviderName", "title": "Provider Name", "name": "ProviderName", "autoWidth": true },
            { "data": "HotName", "title": "How Hot?", "name": "HotName", "autoWidth": true },
            { "data": "OpportunityAmount", "title": "Opportunity Amount", "Opportunity Amount": "OpportunityAmount", "autoWidth": true },
            { "data": "State", "title": "State", "name": "State", "autoWidth": true },
            {
                data: null, title: "Create Account", render: function (data, type, row) {
                    return "<a href='/SalesRep/Client/CreateNewAccount/" + row.LeadId+ "' > <i class='fa fa - plus - square' aria-hidden='true'></i> Create Account</a > ";
                }
            },


        ];
    //
    var param = JSON.stringify({ "LeadQualityID": LeadQualityID, "TypeOfLead": TypeOfLead, "IndustryId": IndustryId, "pageIndex": pageIndex, "pageSize": pageSize });

    InitDataTable($("#tdLead"), "/SalesRep/LeadDashBoard/GetLeadDashBoardData", param, columns, dColumnsDef, [[0, "desc"]], 0, false, false);
    BindCountLead("LeadDashBoard/GetLeadDashBoardData", param);
}
function BindCountLead(url, urlParam) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        datatype: "json",
        data: urlParam,
        success: function (response) {
            $("#DivTotalRecords").text(response.length);
            if (response.length > 0) {
                $("#leadsDashboardExport").removeAttr('disabled');
                $("#leadsDashboardExport").css('opacity', 'initial');
            }
        },
        failure: function (failureData) {


        },
        error: function (errorData) {

            //alert(errorData);
        }
    });
}
$("#btnRepActivity").click(function () {
    SearchRepActivity();
});
function SearchRepActivity() {
    // 

    //var RepId = $('#ddlRepActivitySalesRep option:selected').val();

    var LeadQualityID = $('#ddlQualityLeads option:selected').val();
    var TypeOfLead = $('#ddlLeadType option:selected').val();
    var IndustryId = $('#ddlIndustryType option:selected').val();

    LeadQualityID = LeadQualityID == "0" || LeadQualityID == "" ? null : LeadQualityID;
    TypeOfLead = TypeOfLead == "0" || TypeOfLead == "" ? null : TypeOfLead;
    IndustryId = IndustryId == "0" || IndustryId == "" ? null : IndustryId;
    bindLeadData(LeadQualityID, TypeOfLead, IndustryId, null, null);

}


function ExportLead() {

    var LeadQualityID = $('#ddlQualityLeads option:selected').val();
    var TypeOfLead = $('#ddlLeadType option:selected').val();
    var IndustryId = $('#ddlIndustryType option:selected').val();

    LeadQualityID = LeadQualityID == "0" || LeadQualityID == "" ? null : LeadQualityID;
    TypeOfLead = TypeOfLead == "0" || TypeOfLead == "" ? null : TypeOfLead;
    IndustryId = IndustryId == "0" || IndustryId == "" ? null : IndustryId;
    //LeadQualityID, int? TypeOfLead, int? IndustryId, int? pageIndex, int? pageSize
    location.href = '/LeadDashBoard/ExportLeadDashBordData?LeadQualityID=' + LeadQualityID + '&TypeOfLead=' + TypeOfLead + '&IndustryId=' + IndustryId + '&pageIndex= &pageSize=';
}

function IsComplete(id) {
    $.ajax({
        url: "/SalesRep/LeadDashBoard/IsComplete",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "id": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", bindLeadData);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

//----------------------Lead Note---------------------------------//

function getLeadNoteModalPopup(id)
{
    if (typeof id === "undefined") {
        id = $("#newNote_LeadId").val();
    }
    $.ajax({
        url: "/SalesRep/LeadDashBoard/GetNoteView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ leadId: id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            //
            HideLoader();
            $("#divNoteModal").html(response);
            initSignalDatePicker($('#followDT'));
            $('#repNote').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onAddNote() {
    var result = true;

    if (!emptyStringValidator($('#txtNotes'))) {
        $('#txtNotes').focus();
        $("#noteError").html("Note Description is required.");
        result = false;
    }
    else {
        $("#noteError").empty();
    }

    if (!emptyStringValidator($('#followDT'))) {
        $('#followDT').focus();
        $("#followError").html("FollowUp Date is required.");
        result = false;
    }
    else {
        $("#followError").empty();
    }

    if (!emptyStringValidator($('#notetype')) || $('#notetype').val() == "Select") {
        $('#notetype').focus();
        $("#noteTypeError").html("FollowUp Type is required.");
        result = false;
    }
    else {
        $("#noteTypeError").empty();
    }
    if (!emptyStringValidator($('#timeZone')) || $('#timeZone').val() == "Select") {
        $('#TimeZone').focus();
        $("#timeZoneError").html("Time Zone is required.");
        result = false;
    }
    else {
        $("#timeZoneError").empty();
    }
    if (result == true) {
        showLoader();

    }

    return result;
}

function onAddNoteSucess(data) {

    HideLoader();
    if (data.status != "Error") {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "success", getLeadNoteModalPopup);
    }
    else {
        Clear();

        $("#salesRepAddNoteModel").click();
        GenerateAlert(data.status, data.msg, "info");
    }

}

function onAddNoteFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function Clear() {
    $("#newNote_NoteId").val(0);
    $("textarea[name='newNote.NoteDescription']").val('');
    $("input[name='newNote.FollowUpDate'").val('');
    $("select[name='newNote.NoteTypeId'").val('');
    $("select[name='newNote.TimeZone']").val('');
    $("#btnNoteSubmit").html("Update");

}

function editNote(oNote) {
    if (oNote) {
        $("#newNote_NoteId").val(oNote.NoteId);
        $("#newNote_UserId").val(oNote.UserId);
        $("textarea[name='newNote.NoteDescription']").val(oNote.NoteDescription);
        $("input[name='newNote.FollowUpDate'").val(moment(oNote.FollowUpDate).format('MM/DD/YYYY hh:mm A'));
        $("select[name='newNote.NoteTypeId'").val(oNote.NoteTypeId);
        $("select[name='newNote.TimeZone']").val(oNote.TimeZone);
        $("#btnNoteSubmit").html("Update");
    }
}

function deleteNoteConfirmation(noteId) {
    bootbox.confirm({
        message: "Are you sure want to delete this note?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                showLoader();
                deleteNote(noteId);
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function deleteNote(noteId) {
    $.ajax({
        url: "/SalesRep/LeadDashBoard/DeleteNote",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({ "noteId": noteId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            if (response.status != "Error") {
                GenerateAlert("", response.msg, "success", getLeadNoteModalPopup);
            }
            else {
                GenerateAlert(response.status, response.msg, "info");
            }
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

//----------------------Lead Edit---------------------------------//

function getEditLead(id) {
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/GetEditLeadView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "leadId": id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#divEditLeadModal").html(response);
            $('#editLeadModal').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onBegin()
{
    var result = true;
    if ($('#LeadDashboardData_OpportunityAmount').val() != "") {
        if (!NumberValidator($('#LeadDashboardData_OpportunityAmount'))) {
            $('#LeadDashboardData_OpportunityAmount').focus();
            $("#lblOppError").html("Enter only digits");
            result = false;
        }
        else {
            $("#lblOppError").empty();
        }
    }
    if (result == true)
    {
        showLoader();
    }

}

function onSuccess(data) {
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", RedirectToLeadDashboard);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

//----------------------Lead Add Provider---------------------------------//
function RedirectToLeadDashboard() {
    window.location.href = "/SalesRep/LeadDashBoard";
}

function AddProviderModalPopup() {

    $.ajax({
        url: "/SalesRep/LeadDashBoard/AddProviders/",
        type: 'Post',
        dataType: 'html',
        contentType: "application/json; charset=utf-8",
        success: function () {
            HideLoader();
            $("#AddProviderModal").html();
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}


function onAddProvider() {

    var result = true;
    if (!emptyStringValidator($('#providers_ProviderName'))) {
        $('#providers_ProviderName').focus();
        $("#lblProviderName").html("Provider Name is required");
        result = false;
    }
    else {
        $("#lblProviderName").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}
function onAddProviderSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        $('#providers_ProviderName').val('');
        GenerateAlert(data.status, data.msg, "success",false);


    }
    else {
        $('#providers_ProviderName').val('');
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddProviderFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

//----------------------Lead  Email---------------------------------//
function ClearEmail()
{
    $('#idList_Email').val('');
}



function AddEmailModalPopup(id,emailId)
{
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/GetLeadEmailView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "leadId": id, "EmailRowId": emailId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $("#LeadEmailModalContainer").html(response);
            bindLeadEmailData(id);
            $("#AddEmailModal").modal("show");
            HideLoader();
         
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });

}
function bindLeadEmailData(leadId) {
    var dColumnsDef =
        [
            {

                "targets": [2],
                "visible": true,
                "searchable": false,
                "orderable": false,
                "className": 'text-center'
            }
        ];
    var columns =
        [
            {
                data: null, title: "Primary Email", render: function (data, type, row) {
                    if (data.IsPrimaryEmail)
                    {
                        return "<input type='checkbox' class='with-gap' id='idList_IsPrimaryEmail" + row.EmailRowId + "' onchange='getPrimaryEmailStatus(\"" + row.LeadId + "\" , \"" + row.EmailRowId + "\")' checked><label class='' for='idList_IsPrimaryEmail" + row.EmailRowId + "'></label>";
                    }
                    else {
                        return "<input type='checkbox' class='with-gap' id='idList_IsPrimaryEmail" + row.EmailRowId + "' onchange='getPrimaryEmailStatus(\"" + row.LeadId + "\" , \"" + row.EmailRowId + "\" )'><label class='' for='idList_IsPrimaryEmail" + row.EmailRowId + "'></label>";
                    }
                }
            },
            { "data": "Email", "title": "Email", "name": "Email", "autoWidth": true },

            {
                data: null, "title": "Action", render: function (data, type, row)
                {
                    var clickEditFuncton = 'getEditLeadEmail(' + data.LeadId + ', ' + data.EmailRowId + ')';

                    var clickDeleteFuncton = 'getDeleteLeadEmail(' + data.EmailRowId + ')';

                    return '<a href="#!" onclick=\'' + clickEditFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a> <a href="#!" onclick=\'' + clickDeleteFuncton + '\'><img src="/Content/imagesHome/deletet-ic.png" style="width:20px;" alt=""/></a>';



                }, "autoWidth": true
            },
        ]

    var param = JSON.stringify({ "leadId": leadId });

    InitDataTable($("#leadEmailTable"), "/SalesRep/LeadDashBoard/LeadEmailList", param, columns, dColumnsDef, [[0, "desc"]], 0, false, false);
}

function onAddLeadEmail()
{
    var result = true;
    if (!emptyStringValidator($('#idList_Email'))) {
        $('#idList_Email').focus();
        $("#lblEmail").html("Email is required.");
        result = false;
    }

    else {
        if (!EmailValidator($('#idList_Email'))) {

            $('#idList_Email').focus();
            $("#lblEmail").html("Invalid Email");
            result = false;
        }

        else {
            $("#lblEmail").empty();
        }
    }
    if (result == true)
    {
        showLoader();
    }
    return result;
}

function onAddLeadEmailSucess(data) {
    HideLoader();
    if (data.status != "Error")
    {
        $('#idList_Email').val('');
        var id = $('#idList_LeadId').val();
        GenerateAlert(data.status, data.msg, "success", bindLeadEmailData(id));


    }
    else {
        $('#idList_Email').val('');
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddLeadEmailFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function getEditLeadEmail(leadId,EmailRowId)
{
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/GetLeadEmailView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "leadId": leadId, "EmailRowId": EmailRowId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $("#LeadEmailModalContainer").html(response);
            bindLeadEmailData(leadId);
            $('#btnSubmit').html('Update');
            $("#AddEmailModal").modal("show");
            HideLoader();

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function getDeleteLeadEmail(EmailId) {

    bootbox.confirm({
        message: "Are you sure want to delete this email?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if (result) {

                DeleteLeadEmail(EmailId)
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}
function DeleteLeadEmail(EmailId) {
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/DeleteLeadEmail",
        type: "Post",
        data: JSON.stringify({ EmailId: EmailId }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error")
                {
                    var id = $('#idList_LeadId').val();
                    GenerateAlert(result.status, result.msg, "success", bindLeadEmailData(id));


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function getPrimaryEmailStatus(LeadId,EmailRowId)
{
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/setPrimaryEmail",
        type: "Post",
        datatype: "json",
        data: JSON.stringify({"LeadId": LeadId, "EmailRowId": EmailRowId }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error")
                {
                    var id = $('#idList_LeadId').val();
                    GenerateAlert(result.status, result.msg, "success", bindLeadEmailData(id));


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

//----------------------Lead Mobile---------------------------------//

function ClearMobileField() {
    $('#numList_MobileNumber').val('');
}

function AddMobileModalPopup(id, mobileId) {
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/GetLeadMobileView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "leadId": id, "MobileRowId": mobileId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $("#LeadMobileModalContainer").html(response);
            bindLeadMobileData(id);
            $("#AddMobileModal").modal("show");
            HideLoader();

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });

}
function bindLeadMobileData(leadId) {
    var dColumnsDef =
        [
            {

                "targets": [2],
                "visible": true,
                "searchable": false,
                "orderable": false,
                "className": 'text-center'
            }
        ];
    var columns =
        [
            {
                data: null, title: "Primary Number", render: function (data, type, row) {
                    if (data.IsPrimaryNumber) {
                        return "<input type='checkbox' class='with-gap' id='numList_IsPrimaryNumber" + row.MobileRowId + "' onchange='getPrimaryMobileStatus(\"" + row.LeadId + "\" , \"" + row.MobileRowId + "\")' checked><label class='' for='numList_IsPrimaryNumber" + row.MobileRowId + "'></label>";
                    }
                    else {
                        return "<input type='checkbox' class='with-gap' id='numList_IsPrimaryNumber" + row.MobileRowId + "' onchange='getPrimaryMobileStatus(\"" + row.LeadId + "\" , \"" + row.MobileRowId + "\" )'><label class='' for='numList_IsPrimaryNumber" + row.MobileRowId + "'></label>";
                    }
                }
            },
            { "data": "MobileNumber", "title": "Mobile Number", "name": "MobileNumber", "autoWidth": true },

            {
                data: null, "title": "Action", render: function (data, type, row) {
                    var clickEditFuncton = 'getEditLeadMobile(' + data.LeadId + ', ' + data.MobileRowId + ')';

                    var clickDeleteFuncton = 'getDeleteLeadMobile(' + data.MobileRowId + ')';

                    return '<a href="#!" onclick=\'' + clickEditFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a> <a href="#!" onclick=\'' + clickDeleteFuncton + '\'><img src="/Content/imagesHome/deletet-ic.png" style="width:20px;" alt=""/></a>';



                }, "autoWidth": true
            },
        ]

    var param = JSON.stringify({ "leadId": leadId });

    InitDataTable($("#leadMobileTable"), "/SalesRep/LeadDashBoard/LeadMobileList", param, columns, dColumnsDef, [[0, "desc"]], 0, false, false);
}

function onAddLeadMobile()
{
    var result = true;
    if (!emptyStringValidator($('#numList_MobileNumber'))) {
        $('#numList_MobileNumber').focus();
        $("#lblMobile").html("Mobile Number is required.");
        result = false;
    }

    else {
        if ($('#numList_MobileNumber').val().length < 14)
        {

            $('#numList_MobileNumber').focus();
            $("#lblMobile").html("Invalid Mobile Number");
            result = false;
        }

        else {
            $("#lblMobile").empty();
        }
    }
    if (result == true) {
        showLoader();
    }
    return result;
}

function onAddLeadMobileSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        $('#numList_MobileNumber').val('');
        var id = $('#numList_LeadId').val();
        GenerateAlert(data.status, data.msg, "success", bindLeadMobileData(id));


    }
    else {
        $('#numList_MobileNumber').val('');
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddLeadMobileFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function getEditLeadMobile(leadId, MobileRowId) {
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/GetLeadMobileView",
        type: 'POST',
        dataType: 'html',
        data: JSON.stringify({ "leadId": leadId, "MobileRowId": MobileRowId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            $("#LeadMobileModalContainer").html(response);
            bindLeadMobileData(leadId);
            $('#btnSubmit').html('Update');
            $("#AddMobileModal").modal("show");
            HideLoader();

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function getDeleteLeadMobile(MobileId) {

    bootbox.confirm({
        message: "Are you sure want to delete this number?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if (result) {

                DeleteLeadNumber(MobileId)
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}
function DeleteLeadNumber(MobileId) {
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/DeleteMobile",
        type: "Post",
        data: JSON.stringify({ MobileId: MobileId }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error")
                {
                    var id = $('#numList_LeadId').val();
                    GenerateAlert(result.status, result.msg, "success", bindLeadMobileData(id));


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function getPrimaryMobileStatus(LeadId, MobileRowId) {
    showLoader();
    $.ajax({
        url: "/SalesRep/LeadDashBoard/setPrimaryMobile",
        type: "Post",
        datatype: "json",
        data: JSON.stringify({ "LeadId": LeadId, "MobileRowId": MobileRowId }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error") {
                    var id = $('#numList_LeadId').val();
                    GenerateAlert(result.status, result.msg, "success", bindLeadMobileData(id));


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}