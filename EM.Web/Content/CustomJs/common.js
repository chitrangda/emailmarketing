﻿function SelectAllCheckBox(input) {
    $(".SelectedCheckBox").click();
}

function initSignalDatePickerWithTime(containerEle) {
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: "down",
        timePicker: false,  // time format here
        locale: {
            format: 'MM-DD-YYYY'
        }
    }, function (start, end, label) {
        containerEle.data('assignSD', start.format('MM-DD-YYYY'));

    }

    );
    containerEle.on("apply.daterangepicker", function (a, b) {
        console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
}

function initSignalDatePicker(containerEle) {
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: "down",
        timePicker: true,  // time format here
        locale: {
            format: 'MM-DD-YYYY hh:mm A'
        }
    }, function (start, end, label) {
    });

    containerEle.on("apply.daterangepicker", function (a, b) {
        console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
}

function initSignalDatePickerWithDate(containerEle, date) {
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        drops: "down",
        "startDate": date, timePicker: true,  // time format here
        locale: {
            format: 'MM/DD/YYYY hh:mm A'
            //format: 'M/DD hh:mm A'
        }
    },
        function (start, end, label) {
            //var curDate = new Date(start);
            containerEle.data('assignSD', start.format('MM/DD/YYYY'));
        });

    containerEle.on("apply.daterangepicker", function (a, b) {
        console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM/DD/YYYY'));
        containerEle.data('assignED', b.endDate.format('MM/DD/YYYY'));
    });
}

function initDatePickerForReportWithDate(containerEle, startDated) {
    //containerEle.data('daterangepicker').remove();
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minDate: startDated,
        startDate: startDated,
        maxDate: new Date(),
        drops: "down",
        timePicker: false,  // time format here
        locale: {
            format: 'MM-DD-YYYY'
            //format: 'M/DD hh:mm A'
        }
    },
        function (start, end, label) {
            //var curDate = new Date(start);
            containerEle.data('assignSD', start.format('MM-DD-YYYY'));
        });
    containerEle.on("apply.daterangepicker", function (a, b) {
        // console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
}

function initDatePickerForReport(containerEle) {
    //containerEle.data('daterangepicker').remove();
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: new Date(),
        drops: "down",
        timePicker: false,  // time format here
        locale: {
            format: 'MM-DD-YYYY'
            //format: 'M/DD hh:mm A'
        }
    },
        function (start, end, label) {
            //var curDate = new Date(start);
            containerEle.data('assignSD', start.format('MM-DD-YYYY'));
            var controlId = $(containerEle).attr("id");

            if (controlId == "fromDate") {
                $("#applyReportFilter").removeAttr("disabled");
                $("#toDate").removeAttr("disabled");
                if ($("#toDate").data('daterangepicker'))
                    $("#toDate").daterangepicker("destroy");
                var minDateVar = new Date(start.format('YYYY-MM-DD'));
                initDatePickerForReportWithDate($('#toDate'), minDateVar);
            }
        });

    containerEle.on("apply.daterangepicker", function (a, b) {
        // console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
    $(containerEle).val("")
}

function initDateTimePicker(containerEle, options) {
    containerEle.datetimepicker(options);
}

function initDateTimeRangePicker(containerEle, options) {
    containerEle.daterangepicker(options);
}


function validateCreditCardNumber(ccNum) {
    var visaPattern = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
    var mastPattern = /^(?:5[1-5][0-9]{14})$/;
    var amexPattern = /^(?:3[47][0-9]{13})$/;
    var discPattern = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
    var dinerclubPatern = /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/;

    var isVisa = visaPattern.test(ccNum) === true;
    var isMast = mastPattern.test(ccNum) === true;
    var isAmex = amexPattern.test(ccNum) === true;
    var isDisc = discPattern.test(ccNum) === true;
    var isdinerc = dinerclubPatern.test(ccNum) === true;

    if (isVisa || isMast || isAmex || isDisc || isdinerc) {
        return true;
    }
    else {
        return false;
    }
}

function zipcodeValidator(oZipCode) {
    var zipcodeRegex = /^\d{5}(-\d{4})?$/;
    if (oZipCode) {
        if (!zipcodeRegex.test(oZipCode.val().trim())) {
            return false;
        } else {
            return true;
        }
    }
    else {
        return false;
    }

}

function mobilenoValidator(oPhnNo) {
    var phnNoRegex = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    if (oPhnNo) {
        if (!phnNoRegex.test(oPhnNo.val().trim())) {
            return false;
        } else {
            return true;
        }
    }
    else {
        return false;
    }

}

function EmailValidator(oEmail) {
    if (oEmail) {
        var emailValidation = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
        if (!emailValidation.test(oEmail.val().trim())) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}

function PasswordValidator(oPassword) {
    if (oPassword) {
        //var passwordValidation = /^(?=.*? [A - Z])(?=.*? [a - z])(?=.*? [0 - 9]).{ 6, }$/
        var passwordValidation = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!.+@$%^&*-]).{6,}$/;
        //var passwordValidation = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*? [$ & +,:;=?@#|[]'"<>.^*()%!{}-]).{6,}$/;
        if (passwordValidation.test(oPassword.val())) {
            return true;
        } else {
            return false;
        }
    }
    else {
        return false;
    }

}

function PhoneValidator(oPhone) {

    if (oPhone) {
        var phoneValidation = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
        if (!phoneValidation.test(oPhone.val().trim())) {
            return false;
        } else {
            return true;
        }
    }
    else {
        return false;
    }

}

function IPValidator(oControl) {
    if (oControl) {
        var ipValidation = /^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;
        if (!ipValidation.test(oControl.val().trim())) {
            return false;
        } else {
            return true;
        }
    }
    else {
        return false;
    }
}
function NumberValidator(oNumber) {

    if (oNumber) {
        var numberValidation = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
        if (!numberValidation.test(oNumber.val().trim())) {
            return false;
        } else {
            return true;
        }
    }
    else {
        return false;
    }

}

function emptyStringValidator(ocontrol) {
    if (ocontrol) {
        if (ocontrol.val().trim() == "") {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}

function showLoader() {
    $("#divLoader").show();
}

function HideLoader() {
    $("#divLoader").hide();
}

/*
 * // Created by Alok on 20/12/2018 Start
 */
function GenerateAlert(otitle, description, state, callbackFun) {
    if (callbackFun) {
        swal({ title: otitle, text: description, type: state }, function () {
            callbackFun();
        });
    }
    else {
        swal(otitle, description, state);

    }


}
function GenerateAlertWithHTML(otitle, html, state, callbackFun) {

    swal({ title: otitle, text: html, type: state, html: true }, function () {
        if (isFunction(callbackFun)) {
            callbackFun();
        }
    });
}

function GenerateAlertWithHTMLCustom(otitle, html, state, buttonText,callbackFun) {

    swal({
        title: otitle, text: html, type: state, html: true,
        confirmButtonText: buttonText
    }, function () {
        if (isFunction(callbackFun)) {
            callbackFun();
        }
    });
}


function SweetAlertConfirm(text, state) {
    swal({
        title: "Infomation",
        text: text,
        type: state,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
    },
        function (isConfirm) {
            return isConfirm;
        });
}

function LoadHTML(AreaName, ControllerName, ActionName) {
    showLoader();

    $(".RequiredField").hide();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}
function LoadHTMLWithoffset(AreaName, ControllerName, ActionName) {
    showLoader();
    var offset = (new Date().getTimezoneOffset() * -1);

    $(".RequiredField").hide();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?offset=` + offset,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}
function LoadHTMLWithId(AreaName, ControllerName, ActionName, id, param) {
    showLoader();
    $(".RequiredField").hide();
    var _url = '';
    if (typeof param === "undefined") {
        _url = `/${AreaName}/${ControllerName}/${ActionName}/` + id;

    }
    else {
        _url = `/${AreaName}/${ControllerName}/${ActionName}?id=` + id + param;
    }
    $.ajax({
        url: _url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}
function LoadHTMLWithIdPageName(AreaName, ControllerName, ActionName, id, pageNumber) {
    showLoader();
    $(".RequiredField").hide();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=${id}&pageNumber=${pageNumber}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },

        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadHTMLPage(AreaName, ControllerName, ActionName, key, value, methodType, controlToRender, callBackFunction) {
    showLoader();
    $(".RequiredField").hide();
    var keyvalue = "";
    for (var i = 0; i < key.length; i++) {
        if (methodType.toLowerCase() == "post") {
            if (i != key.length - 1) {
                keyvalue += `${key[i]}:${value[i]},`;
            }
            else {
                keyvalue += `${key[i]}:${value[i]}`;
            }
        } else {
            if (i != key.length - 1) {
                keyvalue += `${key[i]}=${value[i]}&`;
            }
            else {
                keyvalue += `${key[i]}=${value[i]}`;
            }
        }
    }
    var url = `/${AreaName}/${ControllerName}/${ActionName}/`;
    if (methodType.toLowerCase() == "get") {
        $.ajax({
            url: `${url}?${keyvalue}`,
            type: methodType,
            dataType: "html",
            async: true,
            success: function (result) {
                if (result) {
                    if (typeof callBackFunction != 'undefined' && isFunction(callBackFunction)) {
                        callBackFunction();
                    }
                    else {
                        HideLoader();
                    }
                    $(controlToRender).empty().html(result);

                }
                else {
                    HideLoader();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                HideLoader();
            }
        });
    } else {
        console.log(url);
        var data = `{${keyvalue}}`;
        console.log(data);
        var jsongData = JSON.stringify(data);

        $.ajax({
            url: url,
            type: methodType,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(jsongData),
            dataType: "html",
            async: true,
            success: function (result) {
                HideLoader();
                if (result) {
                    $(controlToRender).empty().html(result);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                HideLoader();
            }
        });
    }

}

function LoadHTMLPagePost(AreaName, ControllerName, ActionName, params, methodType, controlToRender, callBackFunction) {
    showLoader();
    $(".RequiredField").hide();
    var url = `/${AreaName}/${ControllerName}/${ActionName}/`;
    $.ajax({
        url: url,
        type: methodType,
        contentType: "application/json; charset=utf-8",
        data: params,
        dataType: "html",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $(controlToRender).empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadHTMLIdPageNameControl(AreaName, ControllerName, ActionName, id, pageNumber, control) {
    showLoader();
    $(".RequiredField").hide();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=${id}&pageNumber=${pageNumber}&search=''`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $(control).empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadHTMLIdPageNameControl(AreaName, ControllerName, ActionName, page, control) {
    showLoader();
    $(".RequiredField").hide();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?page=${page}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $(control).empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadHTMLWithParam1(AreaName, ControllerName, ActionName, param1, methodType) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}`,
        type: methodType,
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: { "param1": param1 },
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadHTMLWithParam2(AreaName, ControllerName, ActionName, param1, param2, methodType) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}`,
        type: methodType,
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: { "param1": param1, "param2": param2 },
        async: true,
        success: function (result) {
            if (result) {
                $("#ContainerDiv").empty().html(result);
                HideLoader();
            }
            else {
                HideLoader();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}


// js Grid config

function initGrid(oGrid, oFields, oController, isPaging, isSorting, isFilter, isInsert, isEdit, funUpdate, oPagesize, oPageCount, odeleteConfirm) {
    if (typeof (isInsert) === "undefined") {
        isInsert = false;
    }
    if (typeof (isFilter) === "undefined") {
        isFilter = false;
    }
    if (typeof (isEdit) === "undefined") {
        isEdit = false;
    }
    if (typeof (isSorting) === "undefined") {
        isSorting = false;
    }
    if (typeof (isPaging) === "undefined") {
        isPaging = false;
    }
    if (typeof (oPageCount) === "undefined") {
        oPageCount = 5;
    }
    if (typeof (oPagesize) === "undefined") {
        oPagesize = 10;
    }
    if (typeof (odeleteConfirm) === "undefined") {
        odeleteConfirm = "";
    }
    var oConfig = {
        height: "auto",
        width: "100%",
        inserting: isInsert,
        filtering: isFilter,
        editing: isEdit,
        sorting: isSorting,
        paging: isPaging,
        autoload: true,
        pageSize: oPagesize,
        deleteConfirm: odeleteConfirm,
        fields: oFields,
        noDataContent: "No data found",
        controller: oController,
        pageButtonCount: oPageCount,
        pagerFormat: "{first} {prev} {pages} {next} {last}",
        pagePrevText: "Prev",
        pageNextText: "Next",
        pageFirstText: "First",
        pageLastText: "Last",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        pagerContainerClass: "jsgrid-pager-container",
        pagerClass: "pagination",
        pagerNavButtonClass: "page-item page-link",
        pageClass: "page-item page-link",
        currentPageClass: "page-item active page-link",
        onItemUpdated: function (args) {
            funUpdate(args.item);
        }
    }
    oGrid.jsGrid(oConfig);

}

function phoneFormat(input) {
    if (input) {
        input.usPhoneFormat({
            format: '(xxx) xxx-xxxx',
        });
    }
}

// Created by Alok on 01/04/2018
var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e, cLength) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    if (e.target.value.length > cLength) {
        e.target.value = e.target.value.substring(0, cLength);
    }
    return ret;
}

// Created by Alok on 01/04/2018
function yearValidation(year, ev) {

    var text = /^[0-9]+$/;
    if (ev.type == "blur" || year.length == 4 && ev.keyCode != 8 && ev.keyCode != 46) {
        if (year != 0) {
            if ((year != "") && (!text.test(year))) {
                ev.target.value = "";
                //alert("Please Enter Numeric Values Only");
                return false;
            }

            if (year.length != 4) {
                ev.target.value = ev.target.value.substring(0, 4);
                console.log("Year is not proper. Please check");
                return false;
            }
            return true;
        }
    }
}

function take() {

    html2canvas(document.querySelector("#tempdata")).then(function (canvas) {
        var base64image = canvas.toDataURL("image/png");
        var img = document.getElementById("previewImage");
        img.src = base64image;

        return base64image;
        //var block = base64image.split(";");
        //var realData = block[1].split(",")[1];
        //return b64toBlob(realData, "image/png");
    });
}

//function getPreview(callback) {
//    html2canvas(document.querySelector("#tempdata")).then(function (canvas) {

//        var base64image = canvas.toDataURL("image/png");
//        console.log(base64image);
//        //var img = document.getElementById("previewImage");
//        //img.src = base64image;
//        var block = base64image.split(";");
//        if (typeof block[1] != 'undefined') {
//            var realData = block[1].split(",")[1];
//            callback(realData);
//        }
//        else {
//            callback("");

//        }
//        //var block = base64image.split(";");
//        //var realData = block[1].split(",")[1];
//        //return b64toBlob(realData, "image/png");
//    });
//}
function getPreview(callback) {
    var realData = null;
    try {
        html2canvas(document.querySelector("#tempdata"), { logging: true, letterRendering: 1, allowTaint: false, useCORS: false }).then(function (canvas) {
            var base64image = canvas.toDataURL("image/png");
            console.log(base64image);
            //var img = document.getElementById("previewImage");
            //img.src = base64image;
            if (base64image != null && base64image != '') {
                var block = base64image.split(";");
                if (block[1] != null && block[1] != '') {
                    realData = block[1].split(",")[1];
                }
            }

            callback(realData);
        });
    }
    catch (err) {
        console.log(err);
        callback(realData);
    }
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}

function ConfirmBootBox(titleText, messageText) {
    var dialog = bootbox.dialog({
        title: titleText,
        message: messageText,
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function () {
                    return false;
                }
            },
            success: {
                label: "Submit",
                callback: function () {
                    return true;
                }
            }
        }
    });
}

function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

function InitDataTable(controlToRender, url, urlParam, columns, columnDefs, oOrder, nfixedCols, isSearching, isLengthChange) {
    showLoader();
    if (typeof isSearching === "undefined") {
        isSearching = true;
    }
    if (typeof isLengthChange === "undefined") {
        isLengthChnage = true;
    }
    if (typeof oOrder === "undefined") {
        oOrder = [[1, "desc"]];
    }
    if (typeof nfixedCols === "undefined" || nfixedCols == null) {
        nfixedCols = 0;
    }
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        datatype: "json",
        data: urlParam,
        async: true,
        success: function (data) {
            HideLoader();
            var deferred = $.Deferred();
            if (!$.fn.DataTable.isDataTable(controlToRender.selector)) {

                var dTable = controlToRender.DataTable({
                    stateSave: false,
                    "destroy": true,
                    "paging": true,
                    "sScrollX": true,
                    "sScrollY": "500px",
                    "bScrollCollapse": true,
                    "bserverSide": true,
                    "ordering": true,
                    "info": false,
                    "searching": isSearching,
                    "order": oOrder,
                    "lengthChange": isLengthChange,
                    "lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
                    data: data,
                    "columnDefs": columnDefs,
                    columns: columns,
                    fixedColumns: {
                        leftColumns: nfixedCols,
                    },
                    'bProcessing': true,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]

                });
            } else {
                var tbl = controlToRender.DataTable();
                tbl.clear();
                tbl.rows.add(data);
                tbl.draw();
            }
            deferred.resolve(data);
            //return dTable;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();

        }
    });

}

function InitDataTableServerSide(controlToRender, sUrl, columns, columnDefs, oOrder, nfixedCols, isSearching, isLengthChange) {
    showLoader();
    if (typeof isSearching === "undefined") {
        isSearching = true;
    }
    if (typeof isLengthChange === "undefined") {
        isLengthChnage = true;
    }
    if (typeof oOrder === "undefined") {
        oOrder = [[1, "desc"]];
    }
    if (typeof nfixedCols === "undefined" || nfixedCols == null) {
        nfixedCols = 0;
    }
    var dTable = controlToRender.DataTable({
        stateSave: false,
        "destroy": true,
        "paging": true,
        "sScrollX": true,
        "sScrollY": "500px",
        "bScrollCollapse": true,
        "sDom": 'r',
        "processing": true,
        "serverSide": true,
        "ordering": true,
        "info": false,
        "searching": isSearching,
        "order": oOrder,
        "lengthChange": isLengthChange,
        "lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "All"]],
        "ajax": {
            url: sUrl,
            type: "POST",
        },
        "columnDefs": columnDefs,
        columns: columns,
        fixedColumns: {
            leftColumns: nfixedCols,
        },
        //'bProcessing': true,
        //'bLanguage': {
        //    'loadingRecords': '&nbsp;',
        //    'processing': 'Loading...'
        //},
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });

}


function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function getCurLocOffset() {
    var offset = (new Date().getTimezoneOffset() * -1);
    return offset;
}

function getTimeConvert(str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('MM/DD/YYYY, h:mm:ss A');

        $(this).html(_Date);
    })

}
function getTimeConvert1(str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('MM-DD-YYYY, h:mm:ss A');

        $(this).html(_Date);
    })

}

function ipEmptyValidator(ocontrol) {
    if (ocontrol) {
        if (ocontrol.val() == null) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}

function capitalizeWords(str) {
    str = str.trim().split(" ");
    for (let i = 0, x = str.length; i < x; i++) {
        if (str[i] != "") {
            str[i] = str[i][0].toUpperCase() + str[i].substr(1);
        }
    }
    str = str.join(" ");
    console.log(str);
    return str;

}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


function getTimeConvertWithTimeFormat (str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('MMM D YYYY hh:mm A');

        $(this).html(_Date);
    })

}
function getTimeConvertWithTimeFormatInReport(str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format(' dddd, MMM, DD YYYY hh:mm:ss A');

        $(this).html(_Date);
    })

}

function getTimeConvertWithTimeFormatInSingleReport(str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('MM/DD/YYYY hh:mm:ss A');

        $(this).html(_Date);
    })

}


function emailValidate(ocontrol)
{
    if (ocontrol)
    {
        var email = ocontrol.val().trim();
        var host = email.split('@')[1];
        if (host == "srmtechsol.com" || host == "stplinc.com")
        {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return false;
    }
}

function initDatePickerDashboard(containerEle) {
    //containerEle.data('daterangepicker').remove();
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: new Date(),
        drops: "down",
        timePicker: false,  // time format here
        locale: {
            format: 'MM-DD-YYYY'
            //format: 'M/DD hh:mm A'
        }
    },
        function (start, end, label) {
            //var curDate = new Date(start);
            containerEle.data('assignSD', start.format('MM-DD-YYYY'));
            var controlId = $(containerEle).attr("id");

            if (controlId == "fromDate") {
                //$("#applyReportFilter").removeAttr("disabled");
                $("#applyReportFilter").prop("disabled", true);
                $("#toDate").removeAttr("disabled");
                if ($("#toDate").data('daterangepicker'))
                    $("#toDate").daterangepicker("destroy");
                var minDateVar = new Date(start.format('YYYY-MM-DD'));
                initDatePickerForDashboardWithDate($('#toDate'), minDateVar);
            }
        });

    containerEle.on("apply.daterangepicker", function (a, b) {
        // console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
    $(containerEle).val("")
}

function initDatePickerForDashboardWithDate(containerEle, startDated) {
    //containerEle.data('daterangepicker').remove();
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minDate: startDated,
        startDate: startDated,
        maxDate: new Date(),
        drops: "down",
        timePicker: false,  // time format here
        locale: {
            format: 'MM-DD-YYYY'
            //format: 'M/DD hh:mm A'
        }
    },
        function (start, end, label) {
            //var curDate = new Date(start);
            containerEle.data('assignSD', start.format('MM-DD-YYYY'));
            var controlId = $(containerEle).attr("id");

            //if (controlId == "toDate") {
            //    $("#applyReportFilter").removeAttr("disabled");
            //}
        });
    $("#toDate").val("");
    containerEle.on("apply.daterangepicker", function (a, b) {
        // console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
}

function getTimeConvertCampaignList(str)
{

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('MMMM Do  YYYY hh:mm A');

        $(this).html(_Date);
    })

}

function initDatePickerForScheduledDateTime(containerEle,sDate) {
    //containerEle.data('daterangepicker').remove();
    if (typeof sDate == 'undefined') {
        sDate = new Date();
    }
    containerEle.daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minDate: sDate,
        drops: "down",
        timePicker: true,  // time format here
        locale: {
            format: 'MM/DD/YYYY hh:mm A'
            //format: 'M/DD hh:mm A'
        }
    },
        function (start, end, label) {
            //var curDate = new Date(start);
            containerEle.data('assignSD', start.format('MM-DD-YYYY'));
        });

    containerEle.on("apply.daterangepicker", function (a, b) {
        // console.log("apply event fired, start/end dates are " + b.startDate.format("MMMM D, YYYY") + " to " + b.endDate.format("MMMM D, YYYY"));
        //customize
        containerEle.data('assignSD', b.startDate.format('MM-DD-YYYY'));
        containerEle.data('assignED', b.endDate.format('MM-DD-YYYY'));
    });
    $(containerEle).val("")
}

function getTimeConvertSubscriber(str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('dd-MMM-yy');

        $(this).html(_Date);
    })

}

function getTimeConvertWithTimeFormatInList(str) {

    $('*[id^=' + str + ']').each(function (index, value) {
        var _Date = $(this).html();

        _Date = moment(_Date).add(getCurLocOffset(), "minutes").format('MMM DD, YYYY hh:mm:ss A');

        $(this).html(_Date);
    })

}

function preventEnterForSubmittingForm()
{
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }

    })
}