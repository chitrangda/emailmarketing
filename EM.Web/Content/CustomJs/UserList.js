﻿var selectedList = [];

function LoadUserListHTML(PageName, pageNumber, Page, id, name) {
    showLoader();
    if (typeof pageNumber == 'undefined') {
        pageNumber = 0;
    }
    var url = "";
    if (Page != "undeined" && Page != "" && Page == "ViewSubscriber") {
        url = '/User/Subscriber/ViewSubscriber?listId=' + id + `&listName=` + name;
    }
    else if (PageName == "UserList") {
        url = '/User/List/' + PageName + '?Page=' + pageNumber;
    }
    else {
        url = '/User/List/' + PageName + '?Page=' + pageNumber;
    }

    $.ajax({
        url: url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SortList(input) {
    showLoader();
    var PageNumber = $("#PageNumber").val().trim();
    var sortOrder = $(input).find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    getUserListGridView(PageNumber, sortOrder);
}
function DsortList(input) {
    showLoader();
    var PageNumber = $("#PageNumber").val().trim();
    var sortOrder = $(input).find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    getDeltedUserListGridView(PageNumber, sortOrder);
}

function PageList(PageControl, page, TotalRecord) {
    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    }
    else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }
    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    }
    else {
        $("#Next").removeAttr('disabled');

    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    }
    else {
        $("#Back").removeAttr('disabled');

    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function Pagging(direction, a) {
    var PageNumber = $("#PageNumber").val().trim();
    if (PageNumber != 'undefined' && PageNumber != "") {
        if (direction == "Next") {
            var Page = parseInt(PageNumber) + 1;
        } else {
            var Page = parseInt(PageNumber) - 1;
        }
    }
    $("#PageNumber").val(Page);
    PageList($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var sortOrder = $("#ddlSorting").find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    if (a == 1) {
        getDeltedUserListGridView($("#PageNumber").val(), sortOrder)
    }
    else {
        getUserListGridView($("#PageNumber").val(), sortOrder)
    }

}



function getUserListGridView(PageNumber, sortOrder, name) {
    $.ajax({
        url: '/User/List/getUserListGridView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ Page: PageNumber, order: sortOrder, listName: name }),
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $("#gridUserList").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function getDeltedUserListGridView(PageNumber, sortOrder, name) {
    $.ajax({
        url: '/User/List/getDeltedUserListGridView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ Page: PageNumber, order: sortOrder, listName: name }),
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $("#gridUserList").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}


//Validation for List Create
function onAddListBegin() {
    var result = true;
    if (!emptyStringValidator($('#txtListName'))) {
        $('#txtListName').focus();
        $("#lstError").html("List Name is required.");
        result = false;
    }
    else {
        $("#lstError").empty();
    }

    if (!emptyStringValidator($('#txtListEmail'))) {
        $('#txtListEmail').focus();
        $("#lstEmailError").html("Email is required.");
        result = false;
    }

    else {
        if (!EmailValidator($('#txtListEmail'))) {

            $('#txtListEmail').focus();
            $("#lstEmailError").html("Invalid Email");
            result = false;
        }

        else {
            $("#lstEmailError").empty();
        }
    }

    if (!emptyStringValidator($('#txtName'))) {
        $('#txtName').focus();
        $("#lstNameError").html("Name is required.");
        result = false;
    }
    else {
        $("#lstNameError").empty();
    }

    //if (!emptyStringValidator($('#txtListRem'))) {
    //    $('#txtListRem').focus();
    //    $("#lstRemError").html("Remainder is required.");
    //    result = false;
    //}
    //else {
    //    $("#lstRemError").empty();
    //}

    //if (!emptyStringValidator($('#txtCompName'))) {
    //    $('#txtCompName').focus();
    //    $("#lstCompError").html("Company Name is required.");
    //    result = false;
    //}
    //else {
    //    $("#lstCompError").empty();
    //}

    //if (!emptyStringValidator($('#txtAddress1'))) {
    //    $('#txtAddress1').focus();
    //    $("#lstAddError").html("Address is required.");
    //    result = false;
    //}
    //else {
    //    $("#lstAddError").empty();
    //}

    //if (!emptyStringValidator($('#txtCity'))) {
    //    $('#txtCity').focus();
    //    $("#lstCityError").html("City is required.");
    //    result = false;
    //}
    //else {
    //    $("#lstCityError").empty();
    //}

    //if (!emptyStringValidator($('#txtZip'))) {
    //    $('#txtZip').focus();
    //    $("#lstZipError").html("Zip is required.");
    //    result = false;
    //}
    //else {
    //    $("#lstZipError").empty();
    //}



    //if (!emptyStringValidator($('#txtPhone'))) {
    //    $('#txtPhone').focus();
    //    $("#lstPhnError").html("Phone Number is required");
    //    result = false;
    //}
    //else {
    //    if (($('#txtPhone').val().length) < 14) {
    //        $('#txtPhone').focus();
    //        $("#lstPhnError").html("Invalid Mobile Number");
    //        result = false;
    //    }
    //    else {
    //        $("#lstPhnError").empty();

    //    }
    //}

    if (result == true) {
        showLoader();
    }

    return result;
}

function onAddListSucess(data) {
    HideLoader();
    if (data.msg == "List name already exists! Please try another.") {
        $('#txtListName').focus();
        $("#lstError").empty();
        $("#lstError").html(data.msg);
    }
    else {
        GenerateAlert(data.status, data.msg, "success", reloadList);
    }

    //HideLoader();
    //swal({
    //    title: "",
    //    text: data.msg,
    //    type: "success",
    //    confirmButtonColor: "#DD6B55",
    //    confirmButtonText: "Ok",
    //    closeOnConfirm: false
    //},
    //    function (isConfirm) {
    //        window.location.href = "/User/List/Index";
    //    });
}

function onAddListFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function getUserListView(listId) {
    showLoader();
    $.ajax({
        url: '/User/List/AddUserList/' + listId,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function DeleteList() {
    var control = $(".SelectedCheckBox");
    var listId = [];
    $.each(control, function (index, value) {
        if (value.checked) {
            listId.push(value.value);
        }
    });

    if (listId.length > 0) {

        bootbox.confirm({
            message: "Are you sure want to delete this list?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                HideLoader();
                if (result) {

                    AJAXCaller("User", "List", "DeleteList", listId);
                }
                else {
                    bootbox.hideAll();
                }
            }
        });
    }
    else {
        GenerateAlert("Please select to delete the list!", "", "info", false);
    }

}

function AJAXCaller(AreaName, ControllerName, ActionName, data) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result.status) {

                if (ActionName == 'RestoreList') {
                    GenerateAlert(result.msg, "", "success", false);
                    LoadUserListHTML("UserRestoreList");

                }
                else {
                    GenerateAlert(result.msg, "", "success", false);
                    LoadHTML("User", "List", "UserList");
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SelectAllCheckBox(input) {
    //$(".SelectedCheckBox").click();
    selectedList = [];
    if ($(input).is(":checked") == true) {
        $(".SelectedCheckBox").each(function () {
            selectedList.push($(this).val());
            $(this).prop("checked", true);
        });
    }
    else {
        $(".SelectedCheckBox").each(function () {

            $(this).prop("checked", false);
            selectedList = [];
        });
    }
}

function redirectToList() {
    window.location.href = "/User/List/Index";
}


function ImportList() {
    var data = new FormData();
    var files = $("#ListSheet").get(0).files;

    if (files.length > 0) { data.append("ListSheet", files[0]); }
    else {
        GenerateAlert('Please select the file to upload.', '', 'warning', false);
        return false;
    }
    var extension = $("#ListSheet").val().split('.').pop().toUpperCase();
    if (extension != "XLS" && extension != "XLSX") {
        GenerateAlert('Invalid file format, please upload the excel file', '', 'warning', false);
        return false;
    }
    showLoader();
    $.ajax({
        url: '/User/List/ImportList', type: "POST", processData: false,
        data: data, dataType: 'json',
        contentType: false,
        success: function (response) {
            $("#ListSheet").val('');
            if (response.status) {
                HideLoader();
                var URL = "" + response.URL;
                LoadHTMLWithParam1("User", "List", "SaveImportSheetList", URL, "Get");
            } else {
                GenerateAlert(response.msg, "", "error", false);
            }

        },
        error: function (er) { HideLoader(); }

    });
    return false;
}
var a;
function Search(oControl) {
    showLoader();
    var Page = $("#PageNumber").val().trim();
    var name = $(oControl).val().trim();


    getUserListGridView(Page, "", name)
}
function Dsearch(oControl) {
    showLoader();
    var Page = $("#PageNumber").val().trim();
    var name = $(oControl).val().trim();


    getDeltedUserListGridView(Page, "", name)
}


function SaveImportList() {
    $.ajax({
        url: '/User/List/SaveImportSheetList', type: "POST",
        data: JSON.stringify({ "param1": $("#ExcelFilePath").val() }), dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#ListSheet").val('');
            if (response.status) {
                var responseTable = "";
                console.log(response.Content);
                var excelRecord = response.Content;
                responseTable += "<table class='' border='1'><thead><tr><th>Email</th><th>Name</th><th>Status</th></tr></thead><tbody>";
                for (var i = 0; i < excelRecord.SuccessList.length; i++) {
                    responseTable += `<tr><td>${excelRecord.SuccessList[i].FromEmail}</td><td>${excelRecord.SuccessList[i].ListName} </td><td  style='color:green'>Success</td></tr>`;
                }

                for (var i = 0; i < excelRecord.InvalidList.length; i++) {
                    responseTable += `<tr><td>${excelRecord.InvalidList[i].FromEmail}</td><td>${excelRecord.InvalidList[i].ListName} </td><td style='color:red'>Invalid</td></tr>`;
                }

                for (var i = 0; i < excelRecord.DuplicateList.length; i++) {
                    responseTable += `<tr><td>${excelRecord.DuplicateList[i].FromEmail}</td><td>${excelRecord.DuplicateList[i].ListName} </td><td style='color:red'>Duplicate Record</td></tr>`;
                }
                responseTable += "</tbody></html>";
                //console.log(responseTable);
                GenerateAlertWithHTML(response.msg, responseTable, "success");
                LoadUserListHTML("UserList");
            } else {
                GenerateAlert(response.msg, "", "error", false);
            }

        },
        error: function (er) { HideLoader(); }

    });
    return false;
}

function getDeletedUserList() {
    LoadUserListHTML("UserRestoreList");

}

function RestoreList() {
    var control = $(".SelectedCheckBox");
    var listId = [];
    var totalSub = [];
    var total = 0;
    $.each(control, function (index, value) {
        if (value.checked) {
            listId.push(value.value);
        }
    });
    $.each(control, function (index, dataSub) {
        if (dataSub.checked) {
            total += parseInt(($(this).attr("data-Sub")));
            //totalSub.push($(this).attr("data-Sub"));
        }
    });
    if (listId.length > 0) {
        var APIURL = $("#APIURL").val();
        var UserId = $("#UserId").val();
        var URL = APIURL + `UserInfo/GetAuthorizationInfo?UserId=${UserId}`;
        $.ajax({
            url: URL,
            type: "Get",
            dataType: 'json',
            success: function (response) {
                var totalRecoveredRecords = response.TotalSubscriberCreated + total;
                if (response.MaxSubscriber >= totalRecoveredRecords) {
                    bootbox.confirm({
                        message: "Are you sure, you want to restore this list?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            HideLoader();
                            if (result) {

                                AJAXCaller("User", "List", "RestoreList", listId);
                            }
                            else {
                                bootbox.hideAll();
                            }
                        }
                    });
                }
                else {
                    GenerateAlert("You have exhausted your limit. To recover records,please upgarde your plan", "", "info", false);
                }

            }
        });
    }
    else {
        GenerateAlert("Please select to restore the list!", "", "info", false);
    }

}

function RecoverSingle(id, TotalSubscriber) {
    if (id) {
        var APIURL = $("#APIURL").val();
        var UserId = $("#UserId").val();
        var URL = APIURL + `UserInfo/GetAuthorizationInfo?UserId=${UserId}`;
        $.ajax({
            url: URL,
            type: "Get",
            dataType: 'json',
            success: function (response) {
                var totalRecoveredRecords = response.TotalSubscriberCreated + TotalSubscriber;
                if (response.MaxSubscriber >= totalRecoveredRecords) {
                    bootbox.confirm({
                        message: "Are you sure, you want to restore this list?",
                        buttons: {
                            confirm: {
                                label: 'Yes',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            HideLoader();
                            if (result) {

                                AJAXCaller("User", "List", "RestoreList", id);
                            }
                            else {
                                bootbox.hideAll();
                            }
                        }
                    });
                }
                else {
                    HideLoader();
                    GenerateAlert("You have exhausted your limit. To recover records,please upgarde your plan", "", "info", false);
                }
            }

        });
    }
    else {
        HideLoader();
        GenerateAlert("Please select to restore the list!", "", "info", false);
    }


}

function bindState(CountryName) {
    if (CountryName != "United States") {
        $("#State").hide();
        $("#txtState").show();
        $("#txtState").attr('name', 'State')
    }
    else {
        $("#State").show();
        $("#txtState").attr('name', 'notState')
        $("#txtState").hide();
    }
}

function reloadList() {
    window.location.href = "/User/List/Index";
}

function selectchk() {
    selectedList = [];
    $(".SelectedCheckBox").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
        }
    });
    if ($(".SelectedCheckBox").length == selectedList.length) {
        $("#MasterCheckbox").prop("checked", true);
    }
    else {
        $("#MasterCheckbox").prop("checked", false);
    }
}