﻿var selectedList = [];

function SortTemplate(value) {
    showLoader();
    var serachTemplateValue = $("#serachTemplate").val().trim();
    GetTemplateListByPagin($("#PageNumber").val(), value, serachTemplateValue)
}

function TemplatePagging(direction, page) {
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    TemplatePage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#SortTemplate').find('option:selected').val();

    GetTemplateListByPagin($("#PageNumber").val(), Order, "", page);
}

function TemplatePage(PageControl, page, TotalRecord) {

    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }
    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function GetTemplateListByPagin(PageNumber, sortOrder, search, pageType) {
    $.ajax({
        url: '/User/Templates/GetTemplateListByPaging/',
        type: "Post",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ page: parseInt(PageNumber), orderBy: sortOrder, search: search, type: pageType }),
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                if (pageType == "SelectTemplateListGrid") {
                    $("#selectTemplateListGrid").empty().html(result);
                } else {
                    $("#templateListGrid").empty().html(result);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SearchTemplate(type) {
    showLoader();
    var serachTemplateValue = $("#serachTemplate").val().trim();
    GetTemplateListByPagin($("#PageNumber").val(), "", serachTemplateValue, type)
}

function SelectAllCheckBoxTemplate(input) {
    //$(".SelectedCheckBox").click();
    selectedList = [];
    if ($(input).is(":checked") == true) {
        $(".SelectedCheckBox").each(function () {
            selectedList.push($(this).val());
            $(this).prop("checked", true);
        });
    }
    else {
        $(".SelectedCheckBox").each(function () {

            $(this).prop("checked", false);
            selectedList = [];
        });
    }
}

function DeleteTemplate() {
    var control = $(".SelectedCheckBox");

    var templateId = [];
    $.each(control, function (index, value) {
        if (value.checked) {
            templateId.push(value.value);
        }
    });

    if (templateId.length > 0) {

        bootbox.confirm({
            message: "Are you sure want to delete this template?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                HideLoader();
                if (result) {
                    TemplateCaller("User", "Templates", "DeleteTemplate", templateId)
                }
            }
        });
    }
    else {
        GenerateAlert("Please select to delete the template!", "", "info", false);
    }

}

function TemplateCaller(AreaName, ControllerName, ActionName, data) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result.status) {

                GenerateAlert(result.msg, "", "success", loadTemplates);

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function loadTemplates() {
    //
    //LoadHTML("User", "Templates", "Index");
    window.location.reload();

}

function LoadTemplates() {
    var keys = [];
    var values = [];
    keys.push("page");
    keys.push("search");
    keys.push("type");
    values.push(1);
    values.push("");
    values.push("");
    LoadHTMLPage("User", "Templates", "TemplateList", keys, values, "Get", $("#ContainerDiv"));
}

function LoadTemplateCampaign() {
    var keys = [];
    var values = [];
    keys.push("page");
    keys.push("search");
    // keys.push("type");
    values.push(1);
    values.push("");
    // values.push("SelectTemplateList");
    LoadHTMLPage("User", "Templates", "SelectTemplateListGrid", keys, values, "Get", $("#selectTemplateListGrid"));
}

function Bee(htmlFile, jsonFile, name) {
    var titleName = '';
    if (name != '' && name != undefined && $("#TemplateId").val() != '') {
        titleName = 'Are you sure you want to update?';
    } else {
        titleName = 'Please enter the template name!';
    }
    var dialog = bootbox.dialog({
        title: titleName,
        message: "<input type='text' id='templateName' placeholder='Enter Template Name' class='form-control' value='" + name + "'/><br/><span id='errmsg' style='color:red;'></span>",
        buttons: {
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
                callback: function () {
                    HideLoader();
                    $('#templateName').modal('hide');
                }
            },
            success: {
                label: "Submit",
                callback: function () {
                    $("#errmsg").empty();
                    CheckDuplicacy(htmlFile, jsonFile, name, $("#TemplateId").val());
                    return false;
                }
            }
        }
    });
}

function CheckDuplicacy(htmlFile, jsonFile, name, id) {

    var templateName = $("#templateName").val().trim();
    if (templateName == null || templateName == "") {
        $("#errmsg").html("This field cannot be blank.");
        return false;
    }
    else {
        showLoader();
        $.ajax({
            url: '/User/Templates/CheckDuplicacy',
            type: 'POST',
            data: JSON.stringify({ "templateName": templateName, "TemplateId": id }),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async: true,
            success: function (result) {
                if (result.status == true) {
                    HideLoader();
                    $("#errmsg").html("Template name already exists! Please try another");
                    return false;
                }
                else {
                    var templateViewModels = {
                        "TemplateHtml": htmlFile,
                        "TemplateJSON": jsonFile,
                        "TemplateName": templateName,
                        "TemplateId": $("#TemplateId").val()
                    };

                    //var formData = new FormData();
                    //formData.append('templateViewModel', JSON.stringify(templateViewModel));
                    //$('#tempdata').html(htmlFile);
                    //$('#myModal').modal('toggle');
                    //$('#myModal').modal('show');
                    //var tempBase64Image = '';

                    //setTimeout(
                    //    function () {
                    //        getPreview(function (res) {
                    //$('#closeModel').click()
                    $.ajax({
                        type: "POST",
                        url: '/User/Templates/SaveBeeTemplateContent',
                        data: JSON.stringify({
                            "templateViewModel": templateViewModels,
                            //"base64String": res
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        success: function (data) {
                            HideLoader();
                            if ($("#TemplateId").val() == 0) {
                                GenerateAlert("Template created successfully", "", "success", loadTemplates);

                            } else {
                                GenerateAlert("Template updated successfully", "", "success", loadTemplates);
                            }
                            //LoadHTMLPage("User", "Templates", "TemplateList", [], [], "Get", $("#ContainerDiv"));
                        },
                        error: function (errordt) {
                            //console.log("errordt  " + JSON.stringify(errordt));
                        },
                        failure: function (failuredt) {
                            // console.log("failuredt  " + failuredt);
                        }
                    });
                    //})


                    //}, 1000);

                }

            }
        });
    }
}

function selectchk() {
    selectedList = [];
    $(".SelectedCheckBox").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
        }
    });
    if ($(".SelectedCheckBox").length == selectedList.length) {
        $("#MasterCheckbox").prop("checked", true);
    }
    else {
        $("#MasterCheckbox").prop("checked", false);
    }
}


