﻿function onAddPromotion()
{
    var result = true;
    if (!emptyStringValidator($('#PromotionCodeName'))) {
        $('#PromotionCodeName').focus();
        $("#lblPromotionName").html("Promotion Code Name is required");
        result = false;
    }
    else {
        $("#lblPromotionName").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;

}

function onAddPromotionSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        $('#PromotionCodeName').val('');
        GenerateAlert(data.status, data.msg, "success", bindPromotionData);
    }
    else
    {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddPromotionFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function bindPromotionData() {
    var dColumnsDef =
        [{

            "targets": [1],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }];
    var columns =
        [
            { "data": "PromotionCodeName", "title": "Promotion Code", "name": "PromotionCodeName", "autoWidth": true },
            {
                data: null, "title": "Action", render: function (data, type, row) {

                    var clickEditFuncton = 'getEditPromotion(' + data.PromotionCodeId + ')';

                    var clickDeleteFuncton = 'getDeletePromotion(' + data.PromotionCodeId + ')';

                    return '<a href="#!" onclick=\'' + clickEditFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a> <a href="#!" onclick=\'' + clickDeleteFuncton + '\'><img src="/Content/imagesHome/deletet-ic.png" style="width:20px;" alt=""/></a>';


                }
            },
        ];

    InitDataTable($("#promotionTable"), "/Admin/PromotionCode/getPromotionList/", null, columns, dColumnsDef, [[0, "desc"]], 0, false, true);
}

$(document).ready(function (e) {
    $('#promocodeActive').addClass("active");
    bindPromotionData();
});

function getEditPromotion(PromotionId) {
    showLoader();
    $.ajax({
        url: "/Admin/PromotionCode/EditPromotion",
        type: 'Post',
        dataType: 'html',
        data: JSON.stringify({ PromotionId: PromotionId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#editContainer").html(response);
            $('#editPromotion').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onEditPromotionCode() {
    var result = true;
    if (!emptyStringValidator($('#txtPromotion'))) {
        $('#txtPromotion').focus();
        $("#lblPromotionNameError").html("Promotion Code Name is required");
        result = false;
    }
    else {
        $("#lblPromotionNameError").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}

function onEditPromotionSucess(data) {
    HideLoader();
    if (data.status != "Error") {
        $('#txtPromotion').val('');
        $("#editPromotion .close").click();
        GenerateAlert(data.status, data.msg, "success", bindPromotionData);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function Reset() {
    $('#PromotionCodeName').val('');
    
}

function getDeletePromotion(PromotionId) {

    bootbox.confirm({
        message: "Are you sure want to delete this promotion code?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if (result) {

                DeletePromotion(PromotionId)
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function DeletePromotion(PromotionId) {
    showLoader();
    $.ajax({
        url: "/Admin/PromotionCode/DeletePromotion",
        type: "Post",
        data: JSON.stringify({ PromotionId: PromotionId }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error") {
                    GenerateAlert(result.status, result.msg, "success", bindPromotionData);
                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}


