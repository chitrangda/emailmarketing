﻿var subsList = [];
var selectedList = [];
var ListNames = [];

function LoadList(url, id, listId) {
    showLoader();
    var APIURL = `${url}UserList?id=${id}&pageNumber=1&listname=&pageSize=&orderBy=`;
    $.ajax({
        url: APIURL,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $.each(response, function (index, value) {
                    $("#ListId").append($("<option></option>").val(value.ListId).html(value.ListName + `(${value.TotalSubscriber} Subscribers)`).attr("data-val", value.TotalSubscriber));
                });
                if (listId != null && listId != '') {
                    $("#ListId").val(listId);
                }
            }
        },
        failure: function (failureData) {
            HideLoader();
            //console.log("failureData == " + failureData);
        },
        error: function (errorData) {
            HideLoader();
            //console.log("errorData == " + errorData);
        }
    });

    //$.ajax(settings).done(function (response) {
    //    if (response != null && response != "") {
    //        $.each(response, function (index, value) {
    //            HideLoader();
    //            $("#ListId").append($("<option></option>").val(value.ListId).html(value.ListName + `(${value.TotalSubscriber} Subscribers)`).attr("data-val", value.TotalSubscriber));
    //        });
    //        if (listId != null && listId != '') {
    //            HideLoader();
    //            $("#ListId").val(listId);
    //        }
    //    }
    //});
}

function BindFromData() {
    showLoader();
    var listId = $("#ListIdHdn").val();
    EnableDisableCampaignButtonOnChanges();
    var url = $("#APIURL").val();
    var APIURL = `${url}Campaign/GetListDetailsByListId?id=${listId}`;
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": APIURL,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        if (response != null && response != "") {
            HideLoader();
            $("#FromName").val(response.FromName);
            $("#FromEmail").val(response.FromEmail);
        }
    });
}


function onAddCampaign() {
    var result = true;

    if (!emptyStringValidator($('#FromName'))) {
        $('#FromName').focus();
        $("#fromError").html("From Name is required.");
        result = false;
    }
    else {
        $("#fromError").empty();
    }
    if (!emptyStringValidator($('#FromEmail'))) {
        $('#FromEmail').focus();
        $("#EmailError").html("From Email is required.");
        result = false;
    }
    else {
        $("#EmailError").empty();
    }
    if (!emptyStringValidator($('#Subject'))) {
        $('#Subject').focus();
        $("#SubError").html("Subject is required.");
        result = false;
    }
    else {
        $("#SubError").empty();
    }

    if (!emptyStringValidator($('#ScheduleDate'))) {
        $('#ScheduleDate').focus();
        $("#SchError").html("Scheduled Date is required.");
        result = false;
    }
    else {
        $("#SchError").empty();
    }
    if (result == true) {
        showLoader();
    }
    else {
        HideLoader();
    }
    return result;
}

function onAddScheduleCampaign() {
    var result = true;
    if (!emptyStringValidator($('#datetimepicker'))) {
        $('#datetimepicker').focus();
        $("#SchError").html("Scheduled Date is required.");
        result = false;
    }
    else {
        $("#SchError").empty();
    }

}

function SaveCampaign(type, value, timeZoneId, timeZone) {
    showLoader();
    var dtScheduled = new Date(value);
    var result = onAddCampaign();

    if (result) {
        if (type == "Schedule") {
            var mdtSchValue = moment.tz(value, "MM/DD/YYYY h:mm:ss A", timeZone);
            var dtScheduled = mdtSchValue.utc().format("MM/DD/YYYY h:mm:ss A");
            timeZoneId = timeZoneId;
            var SDateTime = value;
        } else {
            var dtScheduled = new Date();
            var SDateTime = new Date();
            timeZoneId = 0;
        }
        //var offSet = dtScheduled.getTimezoneOffset();
        //offSet = (offSet * (-1));
        var campaignViewModel = {
            "TemplateId": $("#TemplateId").val(),
            "CampaignName": $("#CampaignNameHdn").val(),
            "ListId": $("#ListIdHdn").val(),
            "FromName": $("#FromName").val(),
            "FromEmail": $("#FromEmail").val(),
            "Subject": $("#Subject").val(),
            "ListName": $("#toSpan").html(),
            "PreviewText": $("#PreviewText").val(),
            "Status": type,
            "ScheduleDate": dtScheduled,
            "CampaignId": ($("#CampaignId").val() == 'undefined' || $("#CampaignId").val() == "" || $('#isReplicate').val() == '1') ? 0 : $("#CampaignId").val(),
            "ContentImagePath": $("#ContentImagePath").val(),
            "SubscriberIds": $("#subscriberIds").val(),

            "TimeZoneId": timeZoneId,
            "UniqueSubscriberIds": $("#UniqueSubscriberIds").val(),
            "UniqueSubscriberIdsCount": $("#UniqueSubscriberIdsCount").val(),
            "SendingFromEmail": $("#SendingFromEmail").val(),
            "SDateTime": SDateTime
        };
        $('#isReplicate').val('0');
        $("#myModal").modal("hide")
        showLoader();
        $.ajax({
            url: '/User/Campaign/CreateCampaign',
            type: 'Post',
            data: JSON.stringify(campaignViewModel),
            contentType: 'application/json; charset=utf-8',
            async: true,
            success: function (data) {
                if (data.status) {
                    if (data.msg == "We have already sent you a mail for email verification, please first verify your FromEmail address") {

                        swal({
                            title: "",
                            text: data.msg,
                            type: "success",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Resend",
                            cancelButtonColor: "#DD6B55",
                            cancelButtonText: "Ok",
                            showCancelButton: true,
                            showConfirmButton: true,
                            cancelButtonClass: 'btn btn-success',
                            closeOnConfirm: false

                        },

                            function (isConfirm) {
                                if (isConfirm) {
                                    showLoader();
                                    $.ajax({
                                        url: '/User/Campaign/ConfirmEmailSendMail',
                                        type: 'Post',
                                        data: JSON.stringify(campaignViewModel),
                                        contentType: 'application/json; charset=utf-8',
                                        async: true,
                                        success: function (data) {
                                            if (data.status == true) {
                                                HideLoader();
                                                swal({
                                                    title: "",
                                                    text: data.msg,
                                                    type: "success",
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: false
                                                },
                                                    function (isConfirm) {
                                                        window.location.href = "/User/Campaign/Index";
                                                    });

                                            }
                                            else {
                                                HideLoader();
                                                GenerateAlert(data.msg, "", "error");
                                            }
                                        }
                                    });
                                }
                                else {
                                    window.location.href = "/User/Campaign/Index";
                                }

                            }
                        )
                    }
                    else {
                        HideLoader();
                        swal({
                            title: "",
                            text: data.msg,
                            type: "success",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false
                        },
                            function (isConfirm) {
                                window.location.href = "/User/Campaign/Index";
                            });
                    }
                }
                else {
                    HideLoader();
                    if (data.msg == "Email address that you have entered is invalid, please enter valid email address") {
                        $('#EmailError').html(data.msg);
                    }
                    else {
                        GenerateAlert(data.msg, "", "error");
                    }
                }
        },
    failure: function (xhrFailure) {
        HideLoader();

    },
    error: function (xhrError) {
        HideLoader();

    }
});
    }

}

function ResendCampaign(CampaignId) {
    showLoader();
    $.ajax({
        url: "/User/Campaign/ResendCampaign?CampaignId=" + CampaignId,
        type: 'Post',
        data: {},
        contentType: 'application/json; charset=utf-8',
        async: true,
        success: function (data) {
            if (data.status) {
                HideLoader();
                swal({
                    title: "",
                    text: data.msg,
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                    function (isConfirm) {
                        window.location.href = "/User/Campaign/Index";
                    });
                //LoadHTMLPage("User", "Campaign", "CampaignList", [], [], "Get", $("#ContainerDiv"));
            }
            else {
                HideLoader();
                GenerateAlert(data.msg, "", "error");
            }
        },
        failure: function (xhrFailure) {
            HideLoader();

        },
        error: function (xhrError) {
            HideLoader();

        }
    });

    HideLoader();
}

function SweetAlertWithCallBack(text, state) {
    swal({
        title: "Infomation",
        text: text,
        type: state,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
    },
        function (isConfirm) {
            alert(isConfirm);
            return isConfirm;
        });
}

function SortCampaign(value) {
    showLoader();
    var serachCampaignValue = $("#searchCampaign").val().trim();
    GetCampaignListGrid($("#PageNumber").val(), value, serachCampaignValue)
}

function SearchCampaign() {
    showLoader();
    var serachCampaignValue = $("#searchCampaign").val().trim();
    GetCampaignListGrid($("#PageNumber").val(), "", serachCampaignValue)
}

function CampaignPagging(direction) {
    showLoader();
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    CampaignPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())

    var Order = $('#sortCampaign').find('option:selected').val();
    GetCampaignListGrid($("#PageNumber").val(), Order, "");
}

function CampaignPage(PageControl, page, TotalRecord) {

    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }

    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < pageSize) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)

}

function GetCampaignListGrid(PageNumber, sortOrder, search) {
    // var offset = (new Date().getTimezoneOffset() * -1);
    //showLoader();
    $.ajax({
        url: '/User/Campaign/getCampaignListGrid/',
        type: "Post",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ page: parseInt(PageNumber), order: sortOrder, search: search }),
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {
                $("#CampaignListGrid").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function LoadTemplateFromCampaign() {
    var keys = [];
    var values = [];
    keys.push("page");
    keys.push("search");
    keys.push("type");
    values.push(1);
    values.push("");
    values.push("SelectTemplateList");
    LoadHTMLPage("User", "Templates", "TemplateList", keys, values, "Get", $("#ContainerDiv"));
}

function SelectTemplateForCampaign(templateId, templateName) {
    $("#TemplateId").val(templateId);
    $("#selectedTemplateName").text(templateName);
    $("#CreateCampaignTemplateBtn").empty();
    var button = `<a href="#"><img id='editorTemplateImg' src="/Content/imagesUser/template1.jpg" class="img-fluid" style="width:180px;height:227px;"></a><br><a href="#" onclick="LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'],[${templateId}],'Get',$('#ContainerDiv2'));$('#ContainerDiv').hide();">Edit</a>`;
    $("#CreateCampaignTemplateBtn").html(button);
}

function SaveBeeContent(htmlFile, jsonFile) {
    //console.log('onSave', htmlFile);
    showLoader();
    var templateViewModel = {
        "TemplateHtml": htmlFile,
        "TemplateJSON": jsonFile,
        "TemplateName": "",
        "Base64Content": '',
        "TemplatePath": $("#NewContentImagePath").val(),
        "CampaignName": $("#CampaignNameHdn").val()
    };
    $("#ContainerDiv2").empty();
    //$("#ContainerDiv2").html(htmlFile);
    //$('#tempdata').html(htmlFile);
    //$('#myModalImage').modal('toggle');
    //$('#myModalImage').modal('show');
    //var tempBase64Image = '';
    //setTimeout(
    //    function () {
    //getPreview(function (res) {
    //$('#closeModelImage').click()
    //templateViewModel.Base64Content = res;
    $.ajax({
        type: "POST",
        url: '/User/Campaign/SaveBeeTemplateContent',
        data: JSON.stringify(templateViewModel),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (data) {
            //GenerateAlert("Content Created Successfully", "", "success", false);
            SelectTemplateForCampaign(data.TemplateId, data.templateName);
            $("#editorTemplateImg").attr('src', data.TemplatePath);
            if (data.TemplateJSON != null && data.TemplateJSON != '') {
                $("#NewContentImagePath").val(data.TemplatePath);
                $("#IsTemplateEdited").val("True");
                NavigateTo("CampaignListSubscriberView");
            } else {
                $("#IsTemplateEdited").val("False");
            }
            $("body").removeClass("sidenav-toggled");
            HideLoader();
        },
        error: function (errordt) {
            console.log("errordt  " + JSON.stringify(errordt));
            HideLoader();

        },
        failure: function (failuredt) {
            console.log("failuredt  " + failuredt);
            HideLoader();

        }
    });
    //})


    //}, 1000);
}

function CloseCampaignModelView() {
    window.location.href = "/User/Campaign/Index";
}

function NavigateTo(page, type) {

    //console.log(page);
    switch (page) {

        case "CampaignNameView":
        case "CampaignNameEditView":

            showLoader();
            var actionText = '';
            if (page == 'CampaignNameEditView') {
                actionText = "BackToModel";
            }
            LoadHTMLPage("User", "Campaign", "CampaignNameView", ['templateId', 'campaignId', 'actionName'], [$("#TemplateId").val(), $("#CampaignId").val(), actionText], "Get", $("#modelBodyData"));
            $('#myModalCampaignName').modal('toggle');
            $('#myModalCampaignName').modal('show');
            setTimeout(function () {
                $('#CampaignName').focus();
                $("#CampaignName").val($("#CampaignNameHdn").val());
            }, 1000)
            HideLoader();
            break;
        case 'CampaignTemplateList':
            showLoader();
            $("#campaignError").empty();

            if ($("#CampaignName").val() != NaN && $("#CampaignName").val() != "" && jQuery.type($("#CampaignName").val()) != 'undefined') {
                $("#CampaignNameHdn").val($("#CampaignName").val());
                $('#cancelBtn').click();
                $("#modelBodyData").empty();
                if ($("#IsCustomizableTemplate").val() == "1" && ($("#TemplateId").val() == "0" || $("#TemplateId").val() == "") && ($("#CampaignId").val() == "0" || $("#CampaignId").val() == "")) {
                    LoadTemplateFromCampaign();
                } else {
                    LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'], [$("#TemplateId").val()], 'Get', $('#ContainerDiv'));
                }
            }

            else if ($("#CampaignNameHdn").val() != NaN && $("#CampaignNameHdn").val() != "") {
                $('#cancelBtn').click();
                $("#modelBodyData").empty();
                if ($("#IsCustomizableTemplate").val() == "1" && ($("#TemplateId").val() == "0" || $("#TemplateId").val() == "") && ($("#CampaignId").val() == "0" || $("#CampaignId").val() == "")) {
                    LoadTemplateFromCampaign();
                } else {
                    LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'], [$("#TemplateId").val()], 'Get', $('#ContainerDiv'));
                }
            }
            else {
                $("#campaignError").text("Campaign Name is Required!");
            }
            HideLoader();
            break;
        case 'CampaignEditorView':
            showLoader();
            $('#ContainerDiv').empty();
            LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId', 'type'], [$("#TemplateId").val(), type], 'Get', $('#ContainerDiv'));
            HideLoader();
            break;
        case 'CampaignListSubscriberView':
            showLoader();
            getSelectListView();
            break;
        case 'CreateCampaign':
            showLoader();
            getCreateCampaignView();
            break;
        case 'CampaignReplecateNameView':
            showLoader();
            var actionText = '';

            LoadHTMLPage("User", "Campaign", "CampaignReplicateNameView", ['templateId', 'campaignId', 'actionName'], [$("#TemplateId").val(), $("#CampaignId").val(), actionText], "Get", $("#modelBodyDataReplicate"));
            $('#myModalCampaignReplicateName').modal('toggle');
            $('#myModalCampaignReplicateName').modal('show');
            setTimeout(function () {
                $('#CampaignName').focus();
                $("#CampaignName").val($("#CampaignNameHdn").val());
            }, 1000)
            HideLoader();
            break;

        case 'LayoutTemplate':
            showLoader();
            if ($("#CampaignName").val() != NaN && $("#CampaignName").val() != "" && jQuery.type($("#CampaignName").val()) != 'undefined') {
                $("#CampaignNameHdn").val($("#CampaignName").val());
                $('#cancelBtn').click();
                $("#modelBodyData").empty();
                if ($("#IsCustomizableTemplate").val() == "1" && ($("#TemplateId").val() == "0" || $("#TemplateId").val() == "") && ($("#CampaignId").val() == "0" || $("#CampaignId").val() == "")) {
                    LoadHTMLPage("User", "Templates", "LayoutView", [], [], "Get", $("#ContainerDiv"));
                } else {
                    LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'], [$("#TemplateId").val()], 'Get', $('#ContainerDiv'));
                }
            }

            else if ($("#CampaignNameHdn").val() != NaN && $("#CampaignNameHdn").val() != "") {
                $('#cancelBtn').click();
                $("#modelBodyData").empty();
                if ($("#IsCustomizableTemplate").val() == "1" && ($("#TemplateId").val() == "0" || $("#TemplateId").val() == "") && ($("#CampaignId").val() == "0" || $("#CampaignId").val() == "")) {
                    LoadHTMLPage("User", "Templates", "LayoutView", [], [], "Get", $("#ContainerDiv"));
                } else {
                    LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'], [$("#TemplateId").val()], 'Get', $('#ContainerDiv'));
                }
            }
            else {
                $("#campaignError").text("Campaign Name is Required!");
            }
            HideLoader();
            break;

        case 'CheapestEmailTemplate':
            showLoader();
            if ($("#CampaignName").val() != NaN && $("#CampaignName").val() != "" && jQuery.type($("#CampaignName").val()) != 'undefined') {
                $("#CampaignNameHdn").val($("#CampaignName").val());
                CheckDuplicacyCampaign($("#CampaignName").val().trim(), $("#CampaignId").val(), $('#CheckDuplicate').val());
            }
            else if ($("#CampaignNameHdn").val() != NaN && $("#CampaignNameHdn").val() != "" && $("#CampaignName").val() != "") {
                $('#cancelBtn').click();
                $("#modelBodyData").empty();
                //CheckDuplicacyCampaign($("#CampaignName").val().trim(), $("#CampaignId").val());
                if ($("#IsCustomizableTemplate").val() == "1" && ($("#TemplateId").val() == "0" || $("#TemplateId").val() == "") && ($("#CampaignId").val() == "0" || $("#CampaignId").val() == "")) {
                    if ($('#SendCamDash').val() == "0") {
                        LoadHTMLPage("User", "Templates", "CheapestEmailTemplates", [], [], "Get", $("#ContainerDiv"), closeCampaignNameModalPopup);
                    }
                    else {
                        LoadHTMLPage("User", "Templates", "CheapestEmailTemplates", [], [], "Get", $("#ContainerDiv"), closeCampaignNameModalPopup);
                    }

                } else {
                    LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'], [$("#TemplateId").val()], 'Get', $('#ContainerDiv'), closeCampaignNameModalPopup);
                }
            }
            else {
                $("#campaignError").text("Campaign Name is Required!");
            }
            HideLoader();
            break;
    }
}

function closeCampaignNameModalPopup() {
    $("#modelBodyData").empty();
    $("#myModalCampaignName").modal("hide");
    HideLoader();
}

function getSelectListView() {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Campaign/CampaignListSubscriberView",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: null,
        cache: false,
        success: function (result) {
            selectedList = [];
            $('#ContainerDiv').empty().html(result);
            $(".CampaignListSubscriberViewNextBtn").attr("disabled", "disabled");
            $(".templateTempImage").attr('src', $("#NewContentImagePath").val());
            if ($("#SubjectHdn").val() != null && $("#SubjectHdn").val() != "") {
                $(".subjectSpan").text($("#SubjectHdn").val());
            }
            else {
                $(".subjectSpan").text($("#CampaignNameHdn").val());
            }
            $("#toSpan").text($("#ListNamesHdn").val());
            $(".Emailsdiv").html($("#EmailNamesHdn").val().replace(/,/g, '\n'))
            $("#ListId").val($("#ListIdHdn").val());
            var totalsubscriber = 0;
            $.each($("#ListIdHdn").val().split(","), function (index, value) {
                var oCheckbox = $("#list_" + value.trim());
                oCheckbox.prop("checked", true)
                //$("#list_" + value).prop("checked", true);
                totalsubscriber = totalsubscriber + parseInt(oCheckbox.attr("contacts"));
                selectedList.push(value);
            });
            $("#subscriberIds").attr("TotalSubscribers", totalsubscriber);
            if (selectedList.length > 0) {
                $(".CampaignListSubscriberViewNextBtn").removeAttr("disabled");
            } else {
                $(".CampaignListSubscriberViewNextBtn").attr("disabled", "disabled");
            }
            $("#lblTotalSubscribers").html(totalsubscriber);
            if (($(".selectedList").length == selectedList.length) && selectedList[0] != "") {
                $("#Allchk").prop("checked", true);
            }
            else {
                $("#Allchk").prop("checked", false);
            }
            HideLoader();
        }
    });
}

function addList() {
    showLoader();
    selectedList = [];
    ListNames = [];
    var totalsubscriber = 0;
    $(".selectedList").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
            totalsubscriber = totalsubscriber + parseInt($(this).attr("contacts"));
            ListNames.push($(this).attr("ListNamesSelected"));
        }
    });
    if ($(".selectedList").length == selectedList.length) {
        $("#Allchk").prop("checked", true);
    }
    else {
        $("#Allchk").prop("checked", false);
    }
    $("#subscriberIds").attr("TotalSubscribers", totalsubscriber);
    $("#ListIdHdn").val(selectedList.join(", "));
    $("#ListNamesHdn").val(ListNames.join(", "));
    //if (selectedList.length > 0) {
    //    $(".CampaignListSubscriberViewNextBtn").removeAttr("disabled");
    //} else {
    //    $(".CampaignListSubscriberViewNextBtn").attr("disabled", "disabled");
    //}
    $("#lblTotalSubscribers").html(totalsubscriber);
    $("#toSpan").html($("#ListNamesHdn").val());
    GetEmailsByListid();
    HideLoader();
}

function selectAll(oCheck) {
    var totalsubscriber = 0;
    selectedList = [];
    ListNames = [];
    if ($(oCheck).is(":checked") == true) {
        $(".selectedList").each(function () {
            $(this).prop("checked", true);
            selectedList.push($(this).val());
            totalsubscriber = totalsubscriber + parseInt($(this).attr("contacts"));
            ListNames.push($(this).attr("ListNamesSelected"));
        });
    }
    else {
        $(".selectedList").each(function () {
            selectedList = [];
            ListNames = [];
            $(this).prop("checked", false);
        });
    }
    $("#subscriberIds").attr("TotalSubscribers", totalsubscriber);
    $("#ListIdHdn").val(selectedList.join(", "));
    $("#ListNamesHdn").val(ListNames.join(", "));
    //if (selectedList.length > 0) {
    //    $(".CampaignListSubscriberViewNextBtn").removeAttr("disabled");
    //} else {
    //    $(".CampaignListSubscriberViewNextBtn").attr("disabled", "disabled");
    //}
    $("#lblTotalSubscribers").html(totalsubscriber);
    $("#toSpan").html($("#ListNamesHdn").val());
    GetEmailsByListid();
}

function getCreateCampaignView() {
    $.ajax({
        type: "GET",
        url: "/User/Campaign/CreateCampaign",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: { templateId: $("#TemplateId").val(), campaignId: $("#CampaignId").val(), ListIds: $("#ListIdHdn").val() },
        cache: false,
        success: function (result) {
            $('#ContainerDiv').empty().html(result);
            // HideLoader();
            $("#FromName").val($("#FromNameHdn").val());
            $("#FromEmail").val($("#FromEmailHdn").val());
            $("#Subject").val($("#CampaignNameHdn").val());
            $(".subjectSpan").text($("#CampaignNameHdn").val());
            $(".templateTempImage").attr('src', $("#NewContentImagePath").val());
            if ($("#CampaignId").val() != "0" && $("#CampaignId").val() != "") {
                if ($("#SubjectHdn").val() != null && $("#SubjectHdn").val() != "") {
                    $("#Subject").val($("#SubjectHdn").val());
                    $(".subjectSpan").text($("#SubjectHdn").val());
                }
                else {
                    $("#Subject").val($("#CampaignNameHdn").val());
                    $(".subjectSpan").text($("#CampaignNameHdn").val());
                }
                $("#toSpan").text($("#ListNamesHdn").val());
            }
            $(".Emailsdiv").html($("#EmailNamesHdn").val().replace(/,/g, '\n'));
            EnableDisableCampaignButtonOnChanges();
            HideLoader();
        }
    })
}

function EnableDisableCampaignButtonOnChanges() {
    var totalSubs = parseInt($("#subscriberIds").attr("TotalSubscribers"));
    $("#SendError").text("");
    //console.log('totalSubs == ' + totalSubs)
    //console.log('TotalRemainingCredits == ' + parseInt($("#TotalRemainingCredits").val()))

    if ($("#IsTemplateEdited").val() == "True" && (parseInt($("#TotalRemainingCredits").val()) >= totalSubs || $("#MonthlyCredits").val() == "0") && totalSubs != 0 && isNaN(totalSubs) != true) {
        //console.log(parseInt($("#TotalRemainingCredits").val()), $("#IsTemplateEdited").val())
        //$("#sendBtn").removeClass("disbaledLink");
        //$("#sendBtn").removeClass("disabled");
        //$("#sendBtn").removeAttr("disabled");
        $(".sendbuttonclass").removeClass("disbaledLink");
        $(".sendbuttonclass").removeClass("disabled");
        $(".sendbuttonclass").removeAttr("disabled");
        $("#sendBtn").attr("onclick", "SaveCampaign('Queued', '')");


        if ($("#IsEmailSchedule").val() == "True") {
            $("#scheduleBtn").removeClass("disbaledLink");
            $("#scheduleBtn").removeClass("disabled");
            $("#scheduleBtn").removeAttr("disabled");

            $("#scheduleBtn").attr("data-toggle", "modal");
            $("#scheduleBtn").attr("data-target", "#myModal");
        } else {
            $("#scheduleBtn").removeAttr("disabled");
            $("#scheduleBtn").attr("onclick", "GenerateCampaignAlert('Schedule')");
        }
    }
    else {
        $("#sendBtn").removeAttr("onclick");
        $("#scheduleBtn").removeAttr("data-toggle");
        $("#scheduleBtn").removeAttr("data-target");

        $("#scheduleBtn").attr("onclick", "GenerateCampaignAlert('Common')");
        $("#sendBtn").attr("onclick", "GenerateCampaignAlert('Common')");

        //$("#sendBtn").addClass("disbaledLink");
        //$("#sendBtn").addClass("disabled");
        //$("#sendBtn").attr("disabled");
        $(".sendbuttonclass").addClass("disbaledLink");
        $(".sendbuttonclass").addClass("disabled");
        $(".sendbuttonclass").attr("disabled");
        $("#scheduleBtn").addClass("disbaledLink");
        $("#scheduleBtn").addClass("disabled");
        $("#scheduleBtn").attr("disabled");
        if (totalSubs != 0 && isNaN(totalSubs) != true) {
            $("#SendError").text("You Have Exhaust Your Plan Limit");
        }
    }
    if (totalSubs == 0 || isNaN(totalSubs) == true) {
        $("#SendError").text("Please Select List To Send Your Campaign");
    }
}
function GetSubscriberById() {
    var listId = $("#ListId").find("option:selected").val();
    showLoader();
    $("#ListIdHdn").val(listId);
    // EnableDisableCampaignButtonOnChanges();
    var settings = {
        "async": true,
        "url": `/Campaign/GetSubscriberByListId?listId=${listId}`,
        "type": "Get",
        "headers": {
            "cache-control": "no-cache",
        }
    }
    $.ajax(settings).done(function (response) {
        $("#subscriberList").empty();
        if (response) {
            HideLoader();
            var items = "";
            var counter = 0;
            subsList = [];
            $.each(response, function (index, value) {
                counter = counter + 1;
                //items += `<li class='list-group-item'><input type='checkbox' class='subscriberCheckList' value='${value.SubscriberId}' onclick='SelectSubscriberInCampaign();'/> ${value.Email}</li>`;
                items += `<tr><td class="align-middle"><span>${value.Email}</span></td><td class="align-middle text-right">`;

                items += `<input style="" type='checkbox' class='subscriberCheckList' onchange="SelectSubscriberInCampaign($('#subsCheckId${counter} '),'Add',${counter});" value='${value.SubscriberId}' id='subsCheckId${value.SubscriberId}'/><div class="btn-group" role="group" aria-label="Basic example">`;

                //items += '<input type= "checkbox" class="btn btn-light btn_toggle" onclick="SelectSubscriberInCampaign($('#subsCheckId${ counter } '),'Add',${counter});" data-selected='${ value.SubscriberId }>'
                //items += `<button type="button" class="btn btn-light btn_toggle includeBtn" onclick="SelectSubscriberInCampaign($('#subsCheckId${counter}'),'Add',${counter});" id="IncludeSubsId${counter}" data-selected='${value.SubscriberId}'>Include</button>`;
                //items += `<button type='button' onclick="SelectSubscriberInCampaign($('#subsCheckId${counter}'),'Remove',${counter});" class='btn btn-light btn_toggle' id="ExcludeSubsId${counter}" disabled='disabled'>Exclude</button></div></td></tr>`;
                //$("#subscriberList").append($("<li></li>").text(response.Email).attr("class", "list-group-item"));
                subsList.push(value.SubscriberId);

            });

            $("#subscriberList").append(items);
            enableNextButton();
        }
    });
}

function SelectSubscriberInCampaign(control, action, thisControl) {
    //console.log("control", control)
    //console.log("action", action)
    //console.log("thisControl", thisControl)
    //console.log($(control).prop("checked"));


    //if (action == "Add" && $(control).prop("checked") == false) {
    //    control.click();
    //    $(`#IncludeSubsId${thisControl}`).attr("disabled", "disabled");
    //    $(`#ExcludeSubsId${thisControl}`).removeAttr("disabled");
    //}
    //if (action == "Remove" && $(control).prop("checked")) {
    //    control.click();
    //    $(`#ExcludeSubsId${thisControl}`).attr("disabled", "disabled");
    //    $(`#IncludeSubsId${thisControl}`).removeAttr("disabled");
    //}
    //if (action == "Add") {
    //    $(`#IncludeSubsId${thisControl}`).css('color', 'green');
    //    $(`#ExcludeSubsId${thisControl}`).removeAttr("style");
    //}
    //if (action == "Remove") {
    //    $(`#ExcludeSubsId${thisControl}`).css('color', 'red');
    //    $(`#IncludeSubsId${thisControl}`).removeAttr("style");
    //}
    subsList = [];
    $.each($(".subscriberCheckList"), function (index, value) {
        if ($(this).is(":checked")) {
            subsList.push(value.value);
        }
    });
    // console.log(subsList);
    enableNextButton();

}

function enableNextButton() {

    if (subsList.length > 0) {
        $(".CampaignListSubscriberViewNextBtn").removeAttr("disabled");
        if (isEdit != "True") {
            $("#subscriberIds").val(subsList);
        }
        $("#subscriberIds").attr("TotalSubscribers", subsList.length);
    } else {
        $(".CampaignListSubscriberViewNextBtn").attr("disabled", "disabled");
        $("#subscriberIds").val("");
        $("#subscriberIds").removeAttr("TotalSubscribers");
    }
    $.each($("#subscriberIds").val().trim().split(','), function (index, value) {
        console.log(value);
        var control = $("#subsCheckId" + value);
        if (control != null) {
            control.prop("checked", true)
        }
    });
}


function GenerateCampaignAlert(type) {
    if (type == "Schedule") {
        GenerateAlert("Your plan doesn't have schedule feature", "", "info", false);
    } else {
        GenerateAlert($("#SendError").text(), "", "info", false);
    }
}

function LoadSchedulerSection(type, offset, oDate) {
    //var offset = $('#TimeZone').find("option:selected").html().split(".")[1];
    $("input[data-delete='ScheduleDate']").remove();
    $("#ByDate").empty();
    $("#ByHours").empty();
    var currentDate = moment();
    if ($("#TimeZone").val() != 0) {

        currentDate = moment().tz($("#TimeZone :selected").attr("data-abbr"));
    }
    var byDateHTML = `<label class="control-label h5">Delivery Date</label>`;
    byDateHTML += `<input type="text" name="ScheduleDate" id="ScheduleDate" class="form-control" />`;
    byDateHTML += `<span class="RequiredField" id="SchError"></span>`;
    var byHoursHTML = `<select class="form-control" name="ScheduleDate" id="ScheduleDate">`;
    byHoursHTML += `<option value=''>Select by Hours</option>`;
    byHoursHTML += `<option value="${FormatDate(currentDate, 24)}">24 Hours</option>`;
    byHoursHTML += `<option value="${FormatDate(currentDate, 48)}">48 Hours</option>`;
    byHoursHTML += `<option value="${FormatDate(currentDate, 72)}">72 Hours</option>`;
    switch (type) {
        case "ByHours":
            $("#ScheduleType").val('ByHours');
            $("#ByHours").append(byHoursHTML); $("#ByDate").empty(); $("#ByHours").show(); $("#ByDate").hide(); break;
        case "ByDate":
            $("#ScheduleType").val('ByDate');
            if (typeof oDate != 'undefined') {
                $('#ScheduleDate').val(oDate);
            }
            $("#ByDate").append(byDateHTML);
            $("#ByHours").empty();
            $("#ByDate").show();
            $("#ByHours").hide();
            initDatePickerForScheduledDateTime($('#ScheduleDate'), moment().tz($("#TimeZone :selected").attr("data-abbr"))/*.format("MM/DD/YYYY hh:mm")*/);
            break;
    }
}

function FormatDate(startdate, hour) {
    var FD = moment(startdate).add(hour, 'hours');
    return FD.format("MM/DD/YYYY hh:mm A");
}

function getCampaignStat(name, id, date) {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Campaign/GetCampignStats",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ id: parseInt(id), date: date }),
        cache: false,
        processdata: true,
        success: function (result) {
            HideLoader();
            $("#lblTotalDelivered").text(result.delivered);
            $("#lblTotalOpen").text(result.uniqueopen);
            $("#lblTotalClick").text(result.uniqueclick);
            $("#lblTotalSubscribers").text(result.subscribers);
            $("#lblTotalSent").text(result.sents);
            $("#lblTotalBounce").text(result.bounce);
            $("#lblCampaignName").html(name.trim());
            $('#campaignStatModal').modal('toggle');
            $('#campaignStatModal').modal('show');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })

}

function getSchedulePopup() {
    if ($("#CampaignStatus").val().trim() == "Schedule") {
        LoadSchedulerSection('ByDate', $("#ScheduleDateHdn").val());

    }
    $("#myModal").modal("show");
}

function viewEmail(campaignId, CampaignName) {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Campaign/ViewEmail",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ "campaignId": campaignId }),
        cache: false,
        success: function (result) {
            $('#ContainerDiv').empty().html(result);
            $("#lblCampName").empty().html('<i class="app-menu__icon fa fa-folder-open"></i>' + " " + "My Campaign- " + CampaignName);
            HideLoader();

        }
    });
}


function DraftCampaign(htmlFile, jsonFile, Status) {
    showLoader();
    var campaignViewModel = {
        "TemplateId": $("#TemplateId").val(),
        "CampaignName": $("#CampaignNameHdn").val(),
        "CampaignId": $("#isReplicate").val() == "1" ? "0" : $("#CampaignId").val(),
        "Status": Status,
        "HtmlContent": htmlFile,
        "JSONContent": jsonFile,
    };
    //$("#myModal").modal("hide");
    //getPreview1(function (res) {
    //$('#closeModelImage').click()
    //campaignViewModel.ContentImagePath = res;
    //templateViewModel.Base64Content = res;
    $.ajax({
        url: '/User/Campaign/DraftCampaign',
        type: 'Post',
        data: JSON.stringify(campaignViewModel),
        contentType: 'application/json; charset=utf-8',
        async: true,
        success: function (data) {
            if (data.status) {
                HideLoader();
                GenerateAlert("", data.msg, "success");
                window.location.href = "/User/Campaign/Index";
                //LoadHTMLPage("User", "Campaign", "Index", [], [], "Get", $("#ContainerDiv"));
                $("body").removeClass("sidenav-toggled");
            }
            //    swal({
            //        title: "",
            //        text: data.msg,
            //        type: "success",
            //        confirmButtonColor: "#DD6B55",
            //        confirmButtonText: "Ok",
            //        closeOnConfirm: false
            //    },
            //        function (isConfirm) {
            //            window.location.href = "/User/Campaign/Index";
            //        });
            //    
            //}
            else {
                HideLoader();
                GenerateAlert(data.msg, "", "error");
            }
        },
        failure: function (xhrFailure) {
            HideLoader();

        },
        error: function (xhrError) {
            HideLoader();

        }
    });
    // });


    return false;

}


function getPreview1(callback) {
    var realData = null;
    try {
        html2canvas(document.querySelector("#tempdata1"), { logging: true, letterRendering: 1, allowTaint: false, useCORS: false }).then(function (canvas) {
            var base64image = canvas.toDataURL("image/png");
            console.log(base64image);
            //var img = document.getElementById("previewImage");
            //img.src = base64image;
            if (base64image != null && base64image != '') {
                var block = base64image.split(";");
                if (block[1] != null && block[1] != '') {
                    realData = block[1].split(",")[1];
                }
            }

            callback(realData);
        });
    }
    catch (err) {
        console.log(err);
        callback(realData);
    }
}

function getsubject() {
    var subject = $("#Subject").val();
    $(".subjectSpan").html(subject);
}

function getVerifiedDomain() {
    if ($("#FromEmail").val() != "" && $("#FromEmail").val() != null) {
        showLoader();
        //$('#sendBtn').addClass('disabled disbaledLink');
        //$('#scheduleBtn').addClass('disabled disbaledLink');
        $('#scheduleBtn').removeClass('disabled disbaledLink');
        $('#scheduleBtn').removeClass('disabled disbaledLink');
        $("#sendBtn").attr("onclick", "IsDomainVerified('Common',$('#FromEmail').val())");
        $('#scheduleBtn').attr("onclick", "IsDomainVerified('Schedule',$('#FromEmail').val())");
        var model = {
            "FromName": $("#FromName").val(),
            "FromEmail": $("#FromEmail").val(),
            "CampaignId": $('#CampaignId').val()
        };

        $.ajax({
            type: "POST",
            url: "/User/Campaign/ConfirmEmailSendMail",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(model),
            cache: false,
            success: function (result) {
                if (result.status == true) {
                    HideLoader();
                    $('#FromEmail').focus();
                    $('#EmailError').html(result.msg);
                }
                else {
                    HideLoader();
                    $('#EmailError').html('');
                }

            }
        });
    }
    else {
        $('#FromEmail').focus();
        $('#EmailError').html("From Email is required.");
    }
}

function getLayoutTemplate() {

}

function CheckDuplicacyCampaign(campaign, CampaignId, IsReplicateCase) {
    showLoader();
    $.ajax({
        type: 'POST',
        url: '/User/Campaign/CheckDuplicacyCampaign',
        data: JSON.stringify({
            "CampaignName": campaign, "CampaignId": CampaignId, "IsReplicateCase": IsReplicateCase
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            if (result.status == true) {
                $("#campaignError").text("This campaign already exists. Please create a different Campaign Name.");
                //return true;
            }
            else {
                if ($("#IsCustomizableTemplate").val() == "1" && ($("#TemplateId").val() == "0" || $("#TemplateId").val() == "") && ($("#CampaignId").val() == "0" || $("#CampaignId").val() == "")) {
                    LoadHTMLPage("User", "Templates", "CheapestEmailTemplates", [], [], "Get", $("#ContainerDiv"), closeCampaignNameModalPopup);
                } else {
                    $('#myModalCampaignReplicateName').modal('hide');
                    LoadHTMLPage('User', 'Campaign', 'CampaignEditorView', ['templateId'], [$("#TemplateId").val()], 'Get', $('#ContainerDiv'), closeCampaignNameModalPopup);
                }
            }

        }
    });
}

function LoadTimeZone() {
    showLoader();
    var APIURL = $("#APIURL").val();
    var URL = APIURL + 'TimeZone';
    $.ajax({
        url: URL,
        type: "Get",
        dataType: 'json',
        success: function (response) {
            HideLoader();
            if (response != null && response != "") {
                $("#TimeZone").empty().append($("<option></option>").val(0).html("Select Time Zone"));
                $.each(response, function (index, value) {
                    //$("#TimeZone").append($("<option></option>").val(value.TimeZoneID).html(value.TimeZoneName.trim()));
                    $("#TimeZone").append("<option value='" + value.TimeZoneID + "' data-abbr='" + value.TimeZoneAbbr.trim() + "'>" + value.TimeZoneName.trim() + "</option>");
                });
                $("#TimeZone").val($('#TimeZoneId').val());
                $("#TimeZone").attr("onchange", "intCalendarTimezont()");
            }
            else {
                HideLoader();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}
function intCalendarTimezont() {
    var currentDate = moment();
    if ($("#TimeZone").val() != 0) {
        currentDate = moment().tz($("#TimeZone :selected").attr("data-abbr"));
        if ($("input[name='ScheduleType']").val() == "ByDate") {
            initDatePickerForScheduledDateTime($('#ScheduleDate'), moment().tz($("#TimeZone :selected").attr("data-abbr"))/*.format("MM/DD/YYYY hh:mm")*/);
        }
    } else {
        if ($("input[name='ScheduleType']").val() == "ByDate") {
            initDatePickerForScheduledDateTime($('#ScheduleDate'));
        }
    }
    $("#ScheduleDate").empty()
        .append(`<option value=''>Select by Hours</option>`)
        .append(`<option value="${FormatDate(currentDate, 24)}">24 Hours</option>`)
        .append(`<option value="${FormatDate(currentDate, 48)}">48 Hours</option>`)
        .append(`<option value="${FormatDate(currentDate, 72)}">72 Hours</option>`);
}

function IsDomainVerified(type, email) {
    showLoader();
    $.ajax({
        type: 'POST',
        url: '/User/Campaign/IsDomainVerified',
        data: JSON.stringify({
            "Email": email
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            if (result.status == false) {
                HideLoader();
                GenerateAlert("Dns Verification is in Process, Please try after sometime", "", "info", false);
            }
            else {
                $('#EmailError').html('');
                if (type == "Schedule") {
                    HideLoader();
                    $("#scheduleBtn").attr("data-toggle", "modal");
                    $("#scheduleBtn").attr("data-target", "#myModal");
                }
                else {
                    SaveCampaign('Queued', '')
                }
            }
        }
    });
}

function GetEmailsByListid() {
    var listid = $("#ListIdHdn").val();
    $.ajax({
        type: 'POST',
        url: '/User/Campaign/EmailsByList',
        data: JSON.stringify({
            "ListId": listid
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: true,
        success: function (result) {
            $(".Emailsdiv").html(result.join('</br>'));
            $("#EmailNamesHdn").val(result);
        }
    });
}