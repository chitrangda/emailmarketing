﻿

function AJAXCaller(AreaName, ControllerName, ActionName, data, listid) {
    showLoader();
    var ListId = listid;
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data, "ListId": ListId },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            if (result.status) {
                if (ActionName == 'RecoverDeleteSubscriber') {
                    HideLoader();
                    GenerateAlert(result.msg, "", "success", false);
                    LoadSubscriberHTML("User", "Subscriber", "ViewDeletedSubscriber", ListId, null, decodeURI(getUrlVars()["name"]));

                }
                else {
                    HideLoader();
                    GenerateAlert(result.msg, "", "success", false);
                    LoadSubscriberHTML("User", "Subscriber", "ViewSubscriber", ListId, null, decodeURI(getUrlVars()["name"]));

                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function LoadSuppressedSubscriberHTML(AreaName, ControllerName, ActionName, listId, listName) {
    showLoader();

    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?listId=${listId}&Page=&listname=${listName}`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#ContainerDiv").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SuppressedSubscriberPage(PageControl, page, TotalRecord) {
    if ($('#PageNumber').length > 0) {
        $('#PageNumber').val(page);
    } else {
        var pageNumberControl = $("<input/>", {
            type: "hidden",
            name: "Page",
            value: page,
            id: "PageNumber"
        });
        $(".PageNumberClass").append(pageNumberControl);
    }
    if ($('#TotalRecord').length > 0) {
        $('#TotalRecord').val(TotalRecord);
    } else {
        var TotalRecordControl = $("<input/>", {
            type: "hidden",
            name: "TotalRecord",
            value: TotalRecord,
            id: "TotalRecord"
        });
        $(".PageNumberClass").append(TotalRecordControl);
    }



    var FirstRec = (page - 1) * pageSize;
    var LastRec = (page * pageSize);
    var CheckNextDisabled = FirstRec + pageSize;
    if (CheckNextDisabled >= TotalRecord) {
        $("#Next").attr('disabled', 'disabled');
    } else {
        $("#Next").removeAttr('disabled')
    }
    if (FirstRec < 10) {
        $("#Back").attr('disabled', 'disabled');
    } else {
        $("#Back").removeAttr('disabled')
    }
    PageControl.text(` Showing ${FirstRec}-${LastRec} out of ${TotalRecord}`)
}

function PaggingSuppressedSubscriber(direction) {
    var PageNumberControl = $("#PageNumber").val().trim();
    if (PageNumberControl != 'undefined' && PageNumberControl != "") {
        if (direction == "Next") {
            var PageNumber = parseInt(PageNumberControl) + 1;
        } else {
            var PageNumber = parseInt(PageNumberControl) - 1;
        }
    }
    $("#PageNumber").val(PageNumber);
    SubscriberPage($("#PageControl"), $("#PageNumber").val(), $("#TotalRecord").val())
    var Order = $('#SortSubscriber').find('option:selected').val();
    getUserSuppressedSubsvriberGridView($("#PageNumber").val(), Order);
}

function SortSuppressedSubscriber(input) {
    showLoader();
    var PageNumber = $("#PageNumber").val().trim();
    var sortOrder = $(input).find("option:selected").val();
    if (typeof sortOrder === "undefined") {
        sortOrder = "NameAsc";
    }
    getUserSuppressedSubsvriberGridView(PageNumber, sortOrder);
}

function getUserSuppressedSubsvriberGridView(PageNumber, sortOrder, search) {
    var ListId = $("#ListId").val();
    $.ajax({
        url: '/User/Subscriber/getSuppressedSubscriberListGridView/',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: JSON.stringify({ listId: ListId, Page: PageNumber, order: sortOrder, Search: search }),
        async: true,
        success: function (result) {
            if (result) {
                HideLoader();
                $("#gridSuppressedSubscriberList").empty().html(result);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    })
}

function SearchSuppresedSubscriber(oControl) {
    showLoader();
    var Page = $("#PageNumber").val().trim();
    var search = $(oControl).val().trim();
    getUserSuppressedSubsvriberGridView(Page, "NameAsc", search)
}



