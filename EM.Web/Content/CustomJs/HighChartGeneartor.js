﻿var oConfig = [];
var xAxisData = [];
var yAxisData = [];
var arrCampaignIdByFilter = [];

function getEmailStatusChart(startDate, endDate)
{
    //showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Dashboard/GetEmailStatusChart",
        data: JSON.stringify({"startdate": startDate , "endDate" : endDate}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = $.parseJSON(result);
            xAxisData = [];
            yAxisData = [
                {
                    name: 'Open', data: [], color: '#17A2B9'
                },
                {
                    name: 'Delivered', data: [], color: '#28a745'
                },
                {
                    name: 'Clicks', data: [], color: '#FEC107'
                }];
            for (j = 0; j < json.length; j++) {
                var date = new Date(json[j].Date);
                var dd = date.getDate();
                dd = dd < 10 ? "0" + dd : dd;
                var MM = date.getMonth()+1;
                MM = MM < 10 ? "0" + MM : MM;
                var YYYY = date.getUTCFullYear();
                date = YYYY + '-' + MM + '-' + dd;
                xAxisData.push(date);
                yAxisData[0].data.push(
                    json[j].open
                );
                yAxisData[1].data.push(
                    json[j].delivered
                );
                yAxisData[2].data.push(
                    json[j].click
                );
            }
             //HideLoader();
            // create the chart
            var chart = new Highcharts.chart('Chartcontainer1', {
                chart: {
                    type: 'column'
                },
                lang: {
                    noData: 'No data to display'

                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: xAxisData,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
                    crosshair: true,
                },
                yAxis: {
                    tickInterval: 1,
                    title: {
                        text: 'Count',
                    },
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.9,
                        pointWidth: 20,
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    },
                    //series: {
                    //    borderRadiusTopLeft: '40%',
                    //    borderRadiusTopRight: '40%',
                    //    borderRadiusBottomLeft: '40%',
                    //    borderRadiusBottomRight: '40%'
                    //},


                },
                legend: {
                    align: 'left',
                    verticalAlign: 'top',
                    y: -30,
                },
                series: yAxisData
            });
        }
    });
}

function getFilterEmailStatusChart(startDate, endDate) {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Dashboard/GetFilterEmailStatusChart",
        data: JSON.stringify({ "startdate": startDate, "endDate": endDate }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = $.parseJSON(result);
            xAxisData = [];
            yAxisData = [
                {
                    name: 'Open', data: [], color: '#17A2B9'
                },
                {
                    name: 'Delivered', data: [], color: '#28a745'
                },
                {
                    name: 'Clicks', data: [], color: '#FEC107'
                }];
            for (j = 0; j < json.length; j++) {
                xAxisData.push(json[j].date);
                yAxisData[0].data.push(
                    json[j].stats[0].metrics.opens
                );
                yAxisData[1].data.push(
                    json[j].stats[0].metrics.delivered
                );
                yAxisData[2].data.push(
                    json[j].stats[0].metrics.clicks
                );
            }
            // HideLoader();
            // create the chart
            var chart = new Highcharts.chart('Chartcontainer1', {
                chart: {
                    type: 'column'
                },
                lang: {
                    noData: 'No data to display'

                },
                credits: {
                    enabled: false
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: xAxisData,
                    font: 'bold 16px "Trebuchet MS", Verdana, sans-serif',
                    crosshair: true,
                },
                yAxis: {
                    tickInterval: 1,
                    title: {
                        text: 'Count',
                    },
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.9,
                        pointWidth: 20,
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    },
                    //series: {
                    //    borderRadiusTopLeft: '40%',
                    //    borderRadiusTopRight: '40%',
                    //    borderRadiusBottomLeft: '40%',
                    //    borderRadiusBottomRight: '40%'
                    //},


                },
                legend: {
                    align: 'left',
                    verticalAlign: 'top',
                    y: -30,
                },
                series: yAxisData
            });
        }
    });
}

function getAllEmailStatusChart()
{
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Dashboard/GetAllEmailStatusChart",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result)
        {
            var json = $.parseJSON(result);
            if (!json.errors) {
                yAxisData = [
                    {
                        name: 'Open', y: null, color: '#17A2B9'
                    },
                    {
                        name: 'Delivered', y: null, color: '#28a745'
                    },
                    {
                        name: 'Clicks', y: null, color: '#FEC107'
                    },
                    {
                        name: 'Bounce', y: null, color: '#DC3546'
                    }
                ];

                for (j = 0; j < json.length; j++) {
                    yAxisData[0].y = yAxisData[0].y + json[j].open;

                    yAxisData[1].y = yAxisData[1].y + json[j].delivered;

                    yAxisData[2].y = yAxisData[2].y + json[j].click;

                    yAxisData[3].y = yAxisData[3].y + json[j].bounce + json[j].dropped

                }
                if (yAxisData[0].y == 0 || yAxisData[0].y == null) {
                    $("#lblTotalOpen").html("0");
                }
                else {
                    $("#lblTotalOpen").html('<a href="#!" onclick="getOpenClickDetails(\'Open\')">' + yAxisData[0].y + '</a>');
                }
                if (yAxisData[2].y == 0 || yAxisData[2].y == null) {
                    $("#lblTotalClick").html("0");
                }
                else {
                    $("#lblTotalClick").html('<a href="#!" onclick="getOpenClickDetails(\'Click\')">' + yAxisData[2].y + '</a>');
                }
               HideLoader();
            }
            else {
                yAxisData = [];
                HideLoader();
            }
            Highcharts.chart('Chartcontainer2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                legend: {
                    align: 'left',
                    verticalAlign: 'top',
                    y: -10,

                },
                lang: {
                    noData: 'No data to display'

                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: '',
                    colorByPoint: true,
                    data: yAxisData
                }]
            });
        }
    })
}

function getFilterAllEmailStatusChart(startDate, endDate)
{
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Dashboard/getFilterAllEmailStatusChart",
        data: JSON.stringify({ "startdate": startDate, "endDate": endDate }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = $.parseJSON(result);
            if (!json.errors) {
                yAxisData = [
                    {
                        name: 'Open', y: null, color: '#17A2B9'
                    },
                    {
                        name: 'Delivered', y: null, color: '#28a745'
                    },
                    {
                        name: 'Clicks', y: null, color: '#FEC107'
                    },
                    {
                        name: 'Bounce', y: null, color: '#DC3546'
                    }
                ];

                for (j = 0; j < json.length; j++) {
                    yAxisData[0].y = yAxisData[0].y + json[j].Opens;

                    yAxisData[1].y = yAxisData[1].y + json[j].Delivered;

                    yAxisData[2].y = yAxisData[2].y + json[j].Clicks;

                    yAxisData[3].y = yAxisData[3].y + json[j].Bounce;

                }
                HideLoader();
            }
            else {
                yAxisData = [];
                HideLoader();
            }
            Highcharts.chart('Chartcontainer2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                legend: {
                    align: 'left',
                    verticalAlign: 'top',
                    y: -10,

                },
                lang: {
                    noData: 'No data to display'

                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: '',
                    colorByPoint: true,
                    data: yAxisData
                }]
            });
        }
    })
}

function getFillOpenClickByFilter()
{
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Dashboard/GetAllEmailStatusChart",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        processdata: true,
        success: function (result) {
            var json = $.parseJSON(result);
            if (!json.errors) {
                yAxisData = [
                    {
                        name: 'Open', y: null, color: '#17A2B9'
                    },
                    {
                        name: 'Delivered', y: null, color: '#28a745'
                    },
                    {
                        name: 'Clicks', y: null, color: '#FEC107'
                    },
                    {
                        name: 'Bounce', y: null, color: '#DC3546'
                    }
                ];

                for (j = 0; j < json.length; j++) {
                    yAxisData[0].y = yAxisData[0].y + json[j].stats[0].metrics.opens;

                    yAxisData[1].y = yAxisData[1].y + json[j].stats[0].metrics.delivered;

                    yAxisData[2].y = yAxisData[2].y + json[j].stats[0].metrics.clicks;

                    yAxisData[3].y = yAxisData[3].y + json[j].stats[0].metrics.bounces;

                }
                if (yAxisData[0].y == 0) {
                    $("#lblTotalOpen").html("0");
                }
                else {
                    $("#lblTotalOpen").html('<a href="#!" onclick="getOpenClickDetails(\'Open\')">' + yAxisData[0].y + '</a>');
                }
                if (yAxisData[2].y == 0) {
                    $("#lblTotalClick").html("0");
                }
                else {
                    $("#lblTotalClick").html('<a href="#!" onclick="getOpenClickDetails(\'Click\')">' + yAxisData[2].y + '</a>');
                }
                HideLoader();
            }
            else {
                yAxisData = [];
                HideLoader();
            }
        }
    })
}

function getCampaignStatsByFilter(startDate,endDate)
{
    $.each($("#tblCampaign tr"), function () {
        arrCampaignIdByFilter.push($(this).find("td:first").find($("input")).val());
    })

    if (arrCampaignIdByFilter.length != 0) {
        $.ajax({
            type: "POST",
            url: "/User/Dashboard/getCampignStatsByFilter/",
            data: JSON.stringify({
                "campaignIds": arrCampaignIdByFilter.join(), "startDate": startDate , "endDate" : endDate
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result && result != "Error") {
                    var campignStatsJson = $.parseJSON(result);
                    if (campignStatsJson) {
                        $.each(campignStatsJson, function (item, value) {
                            $.each(value.stats, function (innerItem, campignStatsValue)
                            {
                                var ctl_subscriber = $("#subscriber_" + campignStatsValue.name);
                                if (ctl_subscriber) {
                                    var sum = parseInt(ctl_subscriber.html()) + parseInt(campignStatsValue.metrics.requests);
                                    ctl_subscriber.html(sum);
                                }
                                var ctl_sent = $("#sent_" + campignStatsValue.name);
                                if (ctl_sent) {
                                    var sum = parseInt(ctl_sent.html()) + parseInt(campignStatsValue.metrics.delivered);
                                    ctl_sent.html(sum);
                                }
                                var ctl_delivered = $("#delivered_" + campignStatsValue.name);
                                if (ctl_delivered) {
                                    var sum = parseInt(ctl_delivered.html()) + parseInt(campignStatsValue.metrics.delivered);
                                    ctl_delivered.html(sum);
                                }
                                var ctl_openCount = $("#opentCount_" + campignStatsValue.name);
                                if (ctl_openCount) {
                                    var sum = parseInt(ctl_openCount.html()) + parseInt(campignStatsValue.metrics.unique_opens);
                                    ctl_openCount.html(sum);
                                }
                                var ctl_clckCount = $("#clickCount_" + campignStatsValue.name);
                                if (ctl_clckCount) {
                                    var sum = parseInt(ctl_clckCount.html()) + parseInt(campignStatsValue.metrics.unique_clicks);
                                    ctl_clckCount.html(sum);
                                }
                                var ctl_bounce_ = $("#bounce_" + campignStatsValue.name);
                                if (ctl_bounce_) {
                                    var sum = parseInt(ctl_bounce_.html()) + parseInt(campignStatsValue.metrics.bounces) + parseInt(campignStatsValue.metrics.bounce_drops);
                                    ctl_bounce_.html(sum);
                                }
                            })
                        })
                    }
                    
                }
                HideLoader();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                HideLoader();
            }
        })
    }

}