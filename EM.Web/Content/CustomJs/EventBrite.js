﻿var eventBriteoAuthUrl = "https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=" + clientId+"&redirect_uri=";
var eventBriteRedirectUrl = window.location.protocol + '//' + window.location.host + "/User/EventbriteIntegration/Save_EventbriteUserAccessToken";
var selectedEvent = [];

function redirectToAuth() {
    showLoader();
    if (accessCode == "") {
        window.location.href = eventBriteoAuthUrl + eventBriteRedirectUrl;
    }
    else {
        geteventbriteEvents();
    }

}

function geteventbriteEvents() {
    window.location.href = "/User/EventbriteIntegration/GetEventBriteEventslist?accesscode=" + accessCode + "&OrgId=" + orgId;
}

function SelectCEList() {
    showLoader();
    $.ajax({
        type: "GET",
        url: "/User/EventbriteIntegration/ListData",
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        data: null,
        cache: false,
        success: function (result) {
            HideLoader();
            $('#divselectattendee').css("display", "none");
            $('#grideventList').css("display", "block");
            $("#btndiv").hide();
            $("#grideventList").empty().html(result);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function SelectedEventAttendees() {
    $("#btndiv").css("display", "block");
    $("#grideventList").hide();

}

function Sync() {
    bootbox.confirm({
        message: "Are you sure you want to sync the attendees for each event into a separate list?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if (result) {

                $("#ShowMessage").css('visibility', 'visible');
                $("#ShowMessage").addClass('popup_show');
                var control = $(".eventCheck");
                var eventModel = {
                    EventId: 0,
                    EventName: ""

                };

                var eventModelList = [];

                $.each(control, function (index, value) {
                    if (value.checked) {
                        eventModel = new Object();
                        eventModel.EventId = value.value;
                        eventModel.EventName = $(this).attr("attr");

                        eventModelList.push(eventModel);

                    }

                });
                if (eventModelList != null) {
                    $.ajax({
                        url: "/User/EventbriteIntegration/SyncData",
                        type: "Post",
                        data: { eventModelList },
                        success: function (response) {
                            $("#ShowMessage").removeClass('popup_show');
                            $("#ShowMessage").css('visibility', 'hidden');
                            if (response.status == true) {
                                GenerateAlert("", response.msg, "success", redirecttoList);
                            }
                            else {
                                GenerateAlert("", response.msg, "error");
                            }

                        }
                    });
                }
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}


function redirecttoList() {
    window.location.href = '/User/List/Index';
}

function SyncSingleData(id) {
    bootbox.confirm({
        message: "Are you sure want to sync all the attendees in this list?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {

                $("#ShowMessage").css('visibility', 'visible');
                $("#ShowMessage").addClass('popup_show');

                $.ajax({
                    url: "/User/EventbriteIntegration/SyncSingleData",
                    type: "Post",
                    data: { id },
                    success: function (response)
                    {
                        $("#ShowMessage").removeClass('popup_show');
                        $("#ShowMessage").css('visibility', 'hidden');
                        if (response.status == true) {
                            GenerateAlert("", response.msg, "success", redirecttoList);
                        }
                        else {
                            GenerateAlert("", response.msg, "error");
                        }

                    }
                });
            }
            else {
                bootbox.hideAll();
            }
        }
    });

}


function SyncBtnEnableDisable() {
    selectedEvent = [];
    $(".eventCheck").each(function () {
        if ($(this).is(":checked")) {
            selectedEvent.push($(this).val());
        }
    });
    if (selectedEvent.length != 0) {
        $('#SyncBtn').removeAttr('disabled');
    }
    else {
        $('#SyncBtn').attr('disabled', 'disabled');
    }
}

function AddList() {
    var result = true;
    if (!emptyStringValidator($('#txtListName'))) {
        $('#txtListName').focus();
        $("#txtError").html("List Name is required.");
        result = false;
    }
    if (result == true) {
        var ListName = $('#txtListName').val();
        $.ajax({
            url: "/User/EventbriteIntegration/CheckDuplicateListName",
            type: "Post",
            data: { ListName },
            success: function (response) {
                if (response == "True") {

                    $("#CreateListPopUp").modal('hide');
                    $("#ShowMessage").css('visibility', 'visible');
                    $("#ShowMessage").addClass('popup_show');

                    var id = 0;
                    var ListName = $("#txtListName").val();

                    $.ajax({
                        url: "/User/EventbriteIntegration/SyncSingleData",
                        type: "Post",
                        data: { id, ListName },
                        success: function (response) {
                            $("#ShowMessage").removeClass('popup_show');
                            $("#ShowMessage").css('visibility', 'hidden');
                            if (response.status == true) {
                                GenerateAlert("", response.msg, "success", redirecttoList);
                            }
                            else {
                                GenerateAlert("", response.msg, "error");
                            }

                        }
                    });

                }
                else {
                    $('#txtListName').focus();
                    $("#txtError").html("List Name already exists! Please try another one");
                }
            }
        });
    }
}

function CreateList() {
    $("#CreateListPopUp").modal('show');
}
function ResetField() {
    $('#txtListName').val('');
    $("#txtError").html('');
}

function Proceed() {
    $('#divselectattendee').css('display', 'none');
    $('#createMasterSepList').css('display', 'block');
}

function CreateMasterList() {
    // $("#CreateMasterPopUp").modal('hide');
    // $("#CreateMasterListPopUp").modal('show');
    $('#SyncSecBtn').removeAttr('disabled');
    $('#MasterList').css('display', 'block');


}
function CreateSeparateList() {
    $('#SyncSecBtn').removeAttr('disabled');
    $('#MasterList').css('display', 'none');
}

function SyncProceedSingle() {
    var radioValue = $("input[name='AttendeesSelect']:checked").val();
    if (radioValue == "radioCML") {
        AddMasterList();
    }
    else {
        Sync();
    }
}

function AddMasterList() {
    var result = true;
    if (!emptyStringValidator($('#txtListName'))) {
        $('#txtListName').focus();
        $("#txtError").html("List Name is required.");
        result = false;
    }
    if (result == true) {
        var ListName = $('#txtListName').val();
        $("#txtError").html('');
        $.ajax({
            url: "/User/EventbriteIntegration/CheckDuplicateListName",
            type: "Post",
            data: { ListName },
            success: function (response) {
                if (response == "True") {
                    $("#ShowMessage").css('visibility', 'visible');
                    $("#ShowMessage").addClass('popup_show');

                    var control = $(".eventCheck");
                    var eventModel = {
                        EventId: 0,
                        EventName: ""

                    };

                    var eventModelList = [];

                    $.each(control, function (index, value) {
                        if (value.checked) {
                            eventModel = new Object();
                            eventModel.EventId = value.value;
                            eventModel.EventName = $(this).attr("attr");

                            eventModelList.push(eventModel);

                        }

                    });
                    if (eventModelList != null) {
                        $.ajax({
                            url: "/User/EventbriteIntegration/SyncDataInMasterList",
                            type: "Post",
                            data: { eventModelList, ListName },
                            success: function (response) {
                                $("#ShowMessage").removeClass('popup_show');
                                $("#ShowMessage").css('visibility', 'hidden');
                                if (response.status == true) {
                                    GenerateAlert("", response.msg, "success", redirecttoList);
                                }
                                else {
                                    GenerateAlert("", response.msg, "error");
                                }

                            }
                        });

                    }
                }
                else {
                    $('#txtListName').focus();
                    $("#txtError").html("List Name already exists! Please try another one");
                }
            }
        });
    }
}

function ClearFields(){
    $('#txtListName').val('');
    $("#txtError").html('');
    window.location.href = '/User/EventbriteIntegration/GetEventBriteEventslist'
}




