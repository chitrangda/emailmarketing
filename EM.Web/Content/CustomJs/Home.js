﻿// validation method for registration
var message = "";

function RegisterAccValidation(type) {
    var result = true;

    if (type != 1) {
        var current_Date = new Date().setFullYear($('#ExpYear').val(), $('#ExpMonth').val(), 1);
        var today = new Date();
        var current_year = new Date().getFullYear();
        if (!emptyStringValidator($('#ExpMonth')) || $('#ExpMonth').val() == "0") {
            $('#ExpMonth').val("")
            $('#ExpMonth').focus();
            $("#ExMonthError").html("Expiry Month is required.");
            result = false;
        }
        else if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
            $("#ExMonthError").empty();
            result = false;
        }
              else if (current_Date < today) {
            $("#ExMonthError").html(`Month should be greater than today's month`);
            result = false;

        }
        else {
            $("#ExMonthError").empty();
        }

        if (!emptyStringValidator($('#ExpYear')) || $('#ExpYear').val() == "0") {
            $('#ExpYear').val("")
            $('#ExpYear').focus();
            $("#ExYearError").html("Expiry Year is required.");
            result = false;
        }
        else if (($('#ExpYear').val() < current_year) || ($('#ExpYear').val() > 2039)) {
            $("#ExYearError").html(`Year should be in range ${current_year} to 2039 year`);
            result = false;
        }
        else {
            $("#ExYearError").empty();
        }
       
        

        if (!emptyStringValidator($('#txtCredit'))) {
            $('#txtCredit').focus();
            $('#CreditError').html("Credit card is required");
            result = false;
        }
        else {
            if (validateCreditCardNumber($('#txtCredit').val().trim()) == false) {
                $('#txtCredit').focus();
                $('#CreditError').html("Invalid credit card no.");
                result = false;

            }
            else {
                $('#CreditError').empty();
            }
        }

        if (!emptyStringValidator($('#txtpostal'))) {
            $('#txtpostal').focus();
            $('#postalError').html("Zip code is required");
            result = false;
        }
        else {
            if (!zipcodeValidator($("#txtpostal"))) {
                $('#txtpostal').focus();
                $('#postalError').html("Invalid zip code");
                result = false;

            }
            else {
                $('#postalError').empty();
            }
        }

        if ($("#txtState").prop('name') == 'State') {
            if (!emptyStringValidator($('#txtstate'))) {

                $('#txtstate').focus();
                $('#stateError').html("State is required");
                result = false;
            }
            else {
                $('#stateError').empty();
            }
        }

        if (!emptyStringValidator($('#txtCity'))) {
            $('#txtcity').focus();
            $('#cityError').html("City is required");
            result = false;
        }
        else {
            $('#cityError').empty();
        }
        if (!emptyStringValidator($('#txtAddress'))) {
            $('#txtAddress').focus();
            $("#AddressError").html("Address is required.");
            result = false;
        }
        else {
            $("#AddressError").empty();
        }

    }


    if ($('#customCheck2').prop("checked") == false) {
        $("#CheckError").html("Check Privacy Checkbox");
        result = false;
    }
    else {
        $("#CheckError").empty();
    }


    if (!emptyStringValidator($('#TimeZoneID')) || $('#TimeZoneID').val() == "0") {
        $('#TimeZoneID').val("")
        $('#TimeZoneID').focus();
        $("#TimeZoneError").html("Time Zone is required.");
        result = false;
    }
    else {
        $("#TimeZoneError").empty();
    }

    if (!emptyStringValidator($('#txtPhone'))) {
        $('#txtPhone').focus();
        $("#MblError").html("Phone Number is required");
        result = false;
    }
    else {
        if (!mobilenoValidator($('#txtPhone'))) {
            $('#txtPhone').focus();
            $("#MblError").html("Invalid Mobile Number");
            result = false;
        }
        else {
            $("#MblError").empty();

        }
    }

    if (!emptyStringValidator($('#txtlastname'))) {
        $('#txtlastname').focus();
        $("#lnError").html("Last Name is required.");
        result = false;
    }
    else {
        $("#lnError").empty();
    }

    if (!emptyStringValidator($('#txtfirstname'))) {
        $('#txtfirstname').focus();
        $("#fnError").html("First Name is required.");
        result = false;
    }
    else {
        $("#fnError").empty();
    }

    if (!emptyStringValidator($('#txtCompany'))) {
        $('#txtCompany').focus();
        $("#cnError").html("Company Name is required.");
        result = false;
    }
    else {
        $("#cnError").empty();
    }

    if (!emptyStringValidator($('#txEmail'))) {
        $('#txEmail').focus();
        $("#emError").html("Email is required.");
        result = false;
    }
    else if (!EmailValidator($('#txEmail'))) {

        $('#txEmail').focus();
        $("#emError").html("Invalid Email");
        result = false;
    }
    else if (!emailValidate($('#txEmail'))) {
        $('#txEmail').focus();
        $("#emError").html("Please don't use srmtechsol or stplinc domain");
        result = false;
    }
    else {
        $("#emError").empty();
    }

    if (result == true) {
        showLoader();
    }

    return result;
}


//validation method for Login
function LoginAccValidation() {

    var result = true;

    if (!emptyStringValidator($('#txtEmail'))) {
        $('#txtEmail').focus();
        $("#eError").html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtEmail'))) {
            $('#txtEmail').focus();
            $("#eError").html("Invalid Email");
            result = false;
        }
        else {
            $("#eError").empty();
        }
    }


    if (!emptyStringValidator($('#txtPassword'))) {
        $('#txtPassword').focus();
        $("#pError").html("Password is required");
        result = false;
    }
    else {
        $("#pError").empty();
    }
    if (result == true) {
        showLoader();
    }
    return result;
}
//Validation method for Password
function PasswordAccValidation() {
    var result = true;
    if (!emptyStringValidator($('#txtResEmail'))) {
        $('#txtResEmail').focus();
        $('#eError').html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtResEmail'))) {

            $('#txtResEmail').focus();
            $("#eError").html("Invalid Email");
            result = false;
        }
        else {
            $("#eError").empty();
        }
    }
    if (!emptyStringValidator($('#txtResPassword'))) {
        $('#txtResPassword').focus();
        $('#pError').removeClass('msg').addClass('RequiredField');
        $('#pError').html("Password is required");
        result = false;
    }
    else {

        if (!PasswordValidator($('#txtResPassword'))) {
            $('#txtResPassword').focus();
            $('#pError').removeClass('msg').addClass('RequiredField');
            $('#pError').html("Your password must contain at least 6 chars, one uppercase, 1 numeric and 1 special character.");
            result = false;
        }
        else {
            $('#pError').removeClass('RequiredField').addClass('msg');
            $('#pError').html("Your password must contain at least 6 chars, one uppercase, 1 numeric and 1 special character.");
        }
    }
    if (!emptyStringValidator($('#txtConfirmPass'))) {
        $('#txtConfirmPass').focus();
        $('#cError').html("Confirm Password is required");
        result = false;
    }
    else {
        if (($('#txtResPassword').val().trim()) != ($('#txtConfirmPass').val().trim())) {
            $('#txtConfirmPass').focus();
            $('#cError').html("Passwords do not match");
            result = false;
        }
        else {
            $('#cError').empty();
        }
    }
    if (result == true) {
        showLoader();
    }
    return result;
}
//validation for Forgot Password
function ForgotAccValidation() {

    var result = true;
    if (!emptyStringValidator($('#txtForEmail'))) {
        $('#txtForEmail').focus();
        $('#eError').html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtForEmail'))) {

            $('#txtForEmail').focus();
            $("#eError").html("Invalid Email");
            result = false;
        }

        else {
            $("#eError").empty();
        }
    }
    if (result == true) {
        showLoader();
    }
    return result;
}
//validation for Contact Us
function ContAccValidation() {

    var result = true;
    if (!emptyStringValidator($('#txtCntName'))) {
        $('#txtCntName').focus();
        $('#nError').html("Name is required");
        result = false;
    }
    else {

        $('#nError').empty();
    }
    if (!emptyStringValidator($('#txtCntEmail'))) {
        $('#txtCntName').focus();
        $('#eError').html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtCntEmail'))) {
            $('#txtCntEmail').focus();
            $('#eError').html('Invalid Email');
            result = false;
        }
        else {
            $('#eError').empty();
        }
    }

    if (!emptyStringValidator($('#txtPhn'))) {
        $('#txtPhn').focus();
        $('#PhnError').html("Phone is required");
        result = false;
    }
    else {

        if (($('#txtPhn').val().length) < 14) {
            $('#txtPhn').focus();
            $("#PhnError").html("Invalid Mobile Number");
            result = false;
        }
        else {
            $('#PhnError').empty();
        }
    }
    if (!emptyStringValidator($('#txtComm'))) {
        $('#txtComm').focus();
        $('#CmtError').html("Comment is required");
        result = false;
    }
    else {
        $('#CmtError').empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}
//Validation for Quote 
function QuoteValidation() {

    var result = true;
    if (!emptyStringValidator($('#txtQuoteName'))) {
        $('#txtQuoteName').focus();
        $('#nError').html("Name is required");
        result = false;
    }
    else {

        $('#nError').empty();
    }
    if (!emptyStringValidator($('#txtQuoteEmail'))) {
        $('#txtQuoteEmail').focus();
        $('#eError').html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#txtQuoteEmail'))) {
            $('#txtQuoteEmail').focus();
            $('#eError').html('Invalid Email');
            result = false;
        }
        else {
            $('#eError').empty();
        }
    }

    if (!emptyStringValidator($('#txtSub'))) {
        $('#txtSub').focus();
        $('#SubError').html("Subject is required");
        result = false;
    }
    else {
        $('#SubError').empty();
    }

    if (!emptyStringValidator($('#txtMsg'))) {
        $('#txtMsg').focus();
        $('#MsgError').html("Comment is required");
        result = false;
    }
    else {
        $('#MsgError').empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}




function selectPlan(oControl) {
    if (oControl) {
        showLoader();
        window.location = '/Account/Register?Package=' + $(oControl).val() + "&loc=" + $("#ddlLoc").val();

    }
}

function getDetails(oControl, id) {
    if ($("option:selected", oControl).attr("totalEmails") != "0") {
        $("#lblTotalEmails_" + id).html($("option:selected", oControl).attr("totalEmails"));
    }
    $("#lblBillingamount_" + id).html($("option:selected", oControl).attr("billingAmount"));
    $("#lblBillingamountCanada_" + id).html($("option:selected", oControl).attr("billngAmountCanada"));

}

function getPrice(oControl) {
    if (oControl) {
        if (oControl.val() == "US") {
            $.each($(".usprice"), function (index, value) {
                $(this).show();

            });
            $.each($(".canadaprice"), function (index, value) {
                $(this).hide();

            });
            $('#image img').attr("src", "/Content/imagesHome/price-tag.png");
        }
        else {
            $.each($(".usprice"), function (index, value) {
                $(this).hide();

            });
            $.each($(".canadaprice"), function (index, value) {
                $(this).show();

            });
            $('#image img').attr("src", "/Content/imagesHome/pricing-plan-canada.png");
        }
    }
}

///toggle in state on Countery Change
function bindState(CountryName) {
    if (CountryName != "United States") {
        $("#State").hide();
        $("#txtState").show();
        $("#txtState").attr('name', 'State')
    }
    else {
        $("#State").show();
        $("#txtState").attr('name', 'notState')
        $("#txtState").hide();
    }

}

function setRegistratonMessage(smessage) {
    message = smessage;
}
function getRegistratonMessage() {
    return message;
}


