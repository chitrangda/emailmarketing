﻿$(document).ready(function () {
    $('#IPAddressActive').addClass("active");
    $('#SelectedIP').multiselect({
        //includeSelectAllOption: true
    });
});

function onAddIP_Address()
{
    var result = true;
    if (!emptyStringValidator($('#IPPool_Id'))) {
        $('#IPPool_Id').focus();
        $("#lblIPPool_Id").html("Select IP Address");
        result = false;
    }
    else {
        $("#lblIPPool_Id").empty();

    }
    if (!ipEmptyValidator($('#SelectedIP'))) {
        $('#SelectedIP').focus();
        $("#lblName").html("Choose an IP Address");
        result = false;
    }
    else {
        $("#lblName").empty();

    }
    //if (!IPValidator($('#IP'))) {
    //    $('#IPPool_Id').focus();
    //    $("#lblName").html("Invalid IP address");
    //    result = false;
    //}
    //else {
    //    $("#lblIPPool_Id").empty();

    //}
    if (result == true) {
        showLoader();
    }

    return result;

}

function onAddIP_AddressSuccess(data) {
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", redirectToControl);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddIP_AddressFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function redirectToControl() {
    window.location.href = "/Admin/IPAddress/IPAddressData";
}

function bindIPAddressByPoolId() {
    var Id = $('#IPPool_Id').val();
    
   
    var dColumnsDef =
        [{

            "targets": [1],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }];
    var columns =
        [
            { "data": "IP", "title": "IP Address", "name": "IP", "autoWidth": true },
            {
                data: null, "title": "Action", render: function (data, type, row) {

                    var clickEditFuncton = 'getEditIPAddress(' + data.Id + ')';

                    var clickDeleteFuncton = 'getDeleteIPAddress(' + data.Id + ')';

                    return '<a href="#!" onclick=\'' + clickEditFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a> <a href="#!" onclick=\'' + clickDeleteFuncton + '\'><img src="/Content/imagesHome/deletet-ic.png" style="width:20px;" alt=""/></a>';


                }
            },
        ];
    var param = JSON.stringify({ "id": Id });
   
    InitDataTable($("#IPAddressPoolDataTable"), "/Admin/IPAddress/GetIPAddressListBYIPPool/", param, columns, dColumnsDef, [[0, "desc"]], 0, false, true);
   
}

$(document).on("change", "#IPPool_Id", function ()
{
    if ($('#IPPool_Id').val() != "") 
    {
        //$('#BindDatatable').show();
       // bindIPAddressByPoolId();
        bindIPAddress($('#IPPool_Id').val());
    }
    else
    {
        Clearcontrol();
    }
    //bindIPAddressByPoolId();

});

function getEditIPAddress(Id) {
    showLoader();
    $.ajax({
        url: "/Admin/IPAddress/EditIPAddress",
        type: 'Post',
        dataType: 'html',
        data: JSON.stringify({ Id: Id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#editContainer").html(response);
            $('#editIPPool').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onEditIPAddress() {
    var result = true;
    if (!emptyStringValidator($('#txtIPAddressName'))) {
        $('#txtIPAddressName').focus();
        $("#IPAddressError").html("IP Address is required");
        result = false;
    }
    else {
        $("#IPAddressError").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}

function getDeleteIPAddress(Id)
{

    bootbox.confirm({
        message: "Are you sure want to delete this IP Address?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {

            if (result) {

                DeleteIPAddress(Id)
            }
            else {
                bootbox.hideAll();
            }
        }
    });
}

function DeleteIPAddress(Id) {
    showLoader();
    $.ajax({
        url: "/Admin/IPAddress/DeleteIPAddress",
        type: "Post",
        data: JSON.stringify({ Id: Id }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result) {

                HideLoader();
                if (result.status != "Error") {
                    GenerateAlert(result.status, result.msg, "success", bindIPAddressByPoolId);


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function Clearcontrol() {
    $('#IPPool_Id').val('');
   // $('#SelectedIP').multiselect("deselectAll", false);
   // $('#SelectedIP').multiselect("resetText", true);

    $("#SelectedIP option:selected").prop("selected", false);
    $("#SelectedIP").multiselect('refresh');

   // $('select.multiselect').multiselect("deselectAll", false);
   
   // $('#BindDatatable').hide();
    //$('#BindDatatable').remove();
    //$('#BindDatatable').detach();
   //$('#BindDatatable').empty();
   // $('#BindDatatable').html('');
    //redirectToControl();
}

function redirectToControl() {
    window.location.href = "/Admin/IPAddress/IPAddressData";
}

function IPPoolModalPopup()
{
    showLoader();
    $.ajax({
        url: "/Admin/IPAddress/AddIPPool/",
        type: 'Post',
        dataType: 'html',
        contentType: "application/json; charset=utf-8",
        success: function (response)
        {
            HideLoader();
            $("#IPPoolContainer").html(response);
            $("#IPPool").modal('show');
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onAddIP()
{
    var result = true;
    if (!emptyStringValidator($('#IP_Pool_Name'))) {
        $('#IP_Pool_Name').focus();
        $("#lblPPool").html("IP Pool Name is required");
        result = false;
    }
    else {
        $("#lblPPool").empty();

    }
    if (result == true)
    {
        showLoader();
    }

    return result;
}

function onAddIPSucess(data) {
    HideLoader();
    $('#IPPool').modal('hide');
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", redirectToControl);
        //GenerateAlert(data.status, data.msg, "success", bindIPAddressByPoolId);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddIPFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function bindIPAddress(Id)
{
    showLoader();
    $.ajax({
        url: "/Admin/IPAddress/GetIPAddressListBYIPPool/",
        type: 'Post',
        dataType: 'html',
        data: JSON.stringify({ "id": Id }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {           
            $("#SelectedIP").val(jQuery.parseJSON(response));
            $("#SelectedIP").multiselect("refresh");
            HideLoader();
        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}
