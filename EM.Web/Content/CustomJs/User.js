﻿function UploadPic(input) {
    if (window.FormData !== undefined) {

        var fileUpload = $("#UserProfilePhoto").get(0);
        var files = fileUpload.files;

        // Create FormData object  
      //  var fileData = new FormData();
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        // Looping over all files and add it to FormData object  
        for (var i = 0; i < files.length; i++) {

            var fileType = files[i].type;
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                //alert('Invalid file type, please upload file type of image.')
                GenerateAlert("Information Alert Message", "Invalid file type, please upload file type of image.", "warning");
                return false;
            }
            var size = (files[i].size / (1024 * 1024)).toFixed(2);
            if (size > 4) {
                //alert("Image is too big, please upload small image.");
                GenerateAlert("Information Alert Message", "Image is too big, please upload small image.", "warning");
                return false;
            }
            //fileData.append(files[i].name, files[i]);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ProfileImageId').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        // Adding one more key to FormData object  
        //$("#divLoader").show();
        //$.ajax({
        //    url: '/User/Dashboard/UploadPic',
        //    type: "POST",
        //    contentType: false, // Not to set any content header  
        //    processData: false, // Not to process data  
        //    data: fileData,
        //    success: function (result) {
        //        if (result != "") {
        //            $("#ProfileImageId").val(result.path);
        //            $("#ProfileImageId").attr("src", result.path);
        //        }
        //        $("#divLoader").hide();

        //    },
        //    error: function (err) {
        //        $("#divLoader").hide();
        //        GenerateAlert("Something went wrong!", "", "info");
        //    }
        //});
    }
    else {
        $("#divLoader").hide();
        //alert("FormData is not supported.");
        GenerateAlert("Something went wrong!", "", "info");
    }
}
function triggerFileUpload() {
    $('#UserProfilePhoto').trigger('click');
}
///toggle in state on Countery Change
function bindState(CountryName) {
    if (CountryName != "United States") {
        $("#State").hide();
        $("#txtState").show();
        $("#txtState").attr('name', 'State')
    }
    else {
        $("#State").show();
        $("#txtState").attr('name', 'notState')
        $("#txtState").hide();
    }

}
