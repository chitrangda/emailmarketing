﻿$(document).ready(function () {

    $('#repActivityTab').addClass("active");
    bindActivityData(null, null, null);
    $('#txtRepActivityDateRangeFrom').val("");
    $('#txtRepActivityDateRangeTo').val("");
});

function bindActivityData(Repid,fromDate, toDate) {
    var dColumnsDef =
        [
            {

                "targets": [0],
                "visible": true,
                "searchable": false,
                "orderable": false,
                "className": 'text-center'
            }
        ];
    var columns =
        [
            
            {
                data: null, title: "Username", render: function (data, type, row) {
                    if (row.UserId != null) {
                        return "<a class='waves-effect waves-light' href='#!' >" + row.ClientName + "</a><br/><a href='/User/Dashboard/Index/" + row.UserId + "'>Login</a>";
                    }
                    else {
                        return "<span></span>";
                    }
                }
            },
            {
                data: null, title: "Rep Name", render: function (data, type, row) {
                    return "<a class='waves-effect waves-light' href='#!' onclick='getAddEditSaleRepView(\"" + row.RepId + "\")'>" + row.FirstName + " " + row.LastName + "</a><br/><a href='/SalesRep/Dashboard/Index/" + row.RepId + "'>Login</a>";
                }
            },
            { "data": "Category", "title": "Category", "name": "Category", "autoWidth": true },

            { "data": "Description", "title": "Description", "name": "Description", "autoWidth": true },
            { "data": "CreatedDate", "title": "Date", "name": "Date", "autoWidth": true },
           {
                data: null, title: "Add Comments", render: function (data, type, row) {
                    return "";
                }
            },
            
            
        ];
 
    var param = JSON.stringify({ "From": fromDate, "To": toDate });
  
    InitDataTable($("#tdRepActivity"), "RepActivity/GetSaleRepActivityData", param, columns, dColumnsDef, [[0, "desc"]], 0, false, false);
    BindTotal("RepActivity/GetSaleRepActivityData", param);
}

// 
function SearchRepActivity()
{
    
    var RepADateFrom = $('#txtRepActivityDateRangeFrom').val();
    var RepADateTo = $('#txtRepActivityDateRangeTo').val();
    bindActivityData(null, RepADateFrom, RepADateTo);
  
}

$("#btnRepActivity").click(function () {
  
    SearchRepActivity();
  
});


function BindTotal(url,urlParam) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: url,
        datatype: "json",
        data: urlParam,
        success: function (response) {
            $("#DivRepActivityTotalRecords").text(response.length);
            if (response.length > 0) {
                $("#export").removeAttr('disabled');
                $("#export").css('opacity', 'initial');
            }
        },
        failure: function (failureData) {
          

        },
        error: function (errorData) {

            //alert(errorData);
        }
    });
}


function noteClosePopUp(sourceType) {
    if ($('#hiddenFildCheckAddNote').val() == 1) {
        $('#myModalComments').modal('hide');
        setTimeout("delayBindDashBoradGrid('" + sourceType + "')", 1000);
    }
    else {
        $('#myModalComments').modal('hide');
    }
}




function ExportRepActivity()
{
    var RepADateFrom = $('#txtRepActivityDateRangeFrom').val();
    var RepADateTo = $('#txtRepActivityDateRangeTo').val();
    
    location.href = 'RepActivity/ExportSaleRepActivity?From=' + RepADateFrom + '&To=' + RepADateTo ;
}

function ClearRepActivity()
{
    $('#txtRepActivityDateRangeFrom').val("");
   $('#txtRepActivityDateRangeTo').val("");
   $('#ddlRepActivitySalesRep').val(null);
   SearchRepActivity();
}




function ClearControl()
{
    $('#hiddenFildNoteID').val(0);
    $('#txtAreaNotes').val('');
    $('#btnSaveNote').text('Add');
   // $('#charNum').text(500);
    //$('#ddlNoteType').val(0).removeAttr('disabled');
}


function getAddEditSaleRepView(id) {
    window.location.href = "/Admin/SalesRep?id=" + id + "&curView=edit";
}