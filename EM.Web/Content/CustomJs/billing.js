﻿var selectedList = [];

function PyamentList() {
    LoadHTML("User", "Billing", "Index");

}

function onAddBillingBegin() {
    var result = true;

    if ($('#isEdit').val() == "0")
    {
       
            if (!emptyStringValidator($('#CreditCardNo'))) {
                $('#CreditCardNo').focus();
                $("#CreditCardNoError").html("Card Number is required.");
                result = false;
            }
            else {
                var isCreditCardValid = validateCreditCardNumber($('#CreditCardNo').val().trim())

                if (isCreditCardValid) {
                    $("#CreditCardNoError").empty();
                } else {
                    $('#CreditCardNo').focus();
                    $("#CreditCardNoError").html("Card Number is invalid.");
                    result = false;
                }

            }
        
    }
    var current_Date = new Date().setFullYear($('#ExYear').val(), $('#ExMonth').val(), 1);
    var today = new Date();
    var current_year = new Date().getFullYear();
    if (!emptyStringValidator($('#ExMonth')) || $('#ExMonth').val() == "0") {
        $('#ExMonth').val("")
        $('#ExMonth').focus();
        $("#ExMonthError").html("Expiry Month is required.");
        result = false;
    }
    else if (!emptyStringValidator($('#ExYear')) || $('#ExYear').val() == "0") {
        $("#ExMonthError").empty();
        result = false;
    } else if (current_Date < today) {
        $("#ExMonthError").html("Month should be greater than today's month");
        result = false;
    }
    else {
        $("#ExMonthError").empty();
    }

    if (!emptyStringValidator($('#ExYear')) || $('#ExYear').val() == "0") {
        $('#ExYear').val("")
        $('#ExYear').focus();
        $("#ExYearError").html("Expiry Year is required.");
        result = false;
    }
    else if (($('#ExYear').val() < current_year) || ($('#ExYear').val() > 2039)) {
        $("#ExYearError").html(`Year should be in range ${current_year} to 2039 year`);
        result = false;
    }
    else {
        $("#ExYearError").empty();
    }
    
   
    
    if (!emptyStringValidator($('#FirstName'))) {
        $('#FirstName').focus();
        $("#FirstNameError").html("FirstName is required.");
        result = false;
    }
    else {

        $("#FirstNameError").empty();
    }

    //if (!emptyStringValidator($('#LastName'))) {
    //    $('#LastName').focus();
    //    $("#LastNameError").html("LastName is required.");
    //    result = false;
    //}
    //else {
    //    $("#LastNameError").empty();
    //}

    //if (!emptyStringValidator($('#Email'))) {
    //    $('#Email').focus();
    //    $("#EmailError").html("Email is required.");
    //    result = false;
    //}
    //else {
    //    if (!EmailValidator($('#Email'))) {

    //        $('#Email').focus();
    //        $("#EmailError").html("Invalid Email");
    //        result = false;
    //    }
    //    else {
    //        $("#EmailError").empty();
    //    }
    //}

    //if (!emptyStringValidator($('#Phone'))) {
    //    $('#Phone').focus();
    //    $("#PhoneError").html("Phone Number is required");
    //    result = false;
    //}
    //else {
    //    if (($('#Phone').val().length) < 14) {
    //        $('#Phone').focus();
    //        $("#PhoneError").html("Invalid Mobile Number");
    //        result = false;
    //    }
    //    else {
    //        $("#PhoneError").empty();

    //    }
    //}

    if (!emptyStringValidator($('#Phone'))) {
        $('#Phone').focus();
        $("#PhoneError").html("Phone Number is required");
        result = false;
    }
    else {
        if (!mobilenoValidator($('#Phone'))) {
            $('#Phone').focus();
            $("#PhoneError").html("Invalid Mobile Number");
            result = false;
        }
        else {
            $("#PhoneError").empty();

        }
    }

    if (!emptyStringValidator($('#Address1'))) {
        $('#Address1').focus();
        $("#Address1Error").html("Address1 is required.");
        result = false;
    }
    else {
        $("#Address1Error").empty();
    }


    if (!emptyStringValidator($('#City'))) {
        $('#City').focus();
        $("#CityError").html("City is required.");
        result = false;
    }
    else {
        $("#CityError").empty();
    }

    //if (!emptyStringValidator($('#State'))) {
    //    $('#State').focus();
    //    $("#StateError").html("State is required.");
    //    result = false;
    //}
    //else {
    //    $("#StateError").empty();
    //}

    if (!emptyStringValidator($('#State'))) {
        $('#State').focus();
        $("#StateError").html("State is required.");
        result = false;
    }
    else {
        $("#StateError").empty();
    }

    if (!emptyStringValidator($('#Zip'))) {
        $('#Zip').focus();
        $("#ZipError").html("Zip is required.");
        result = false;
    }
    else {
        $("#ZipError").empty();
    }

    //if (!emptyStringValidator($('#Country'))) {
    //    $('#Country').focus();
    //    $("#CountryError").html("Country is required.");
    //    result = false;
    //}
    //else {
    //    $("#CountryError").empty();
    //}

    //if (!emptyStringValidator($('#CardType'))) {
    //    $('#CardType').focus();
    //    $("#CardTypeError").html("Card Type is required.");
    //    result = false;
    //}
    //else {
    //    $("#CardTypeError").empty();
    //}

    if (result == true) {
        if ($("#chkDefault").is(":checked")) {
            $("#IsPrimary").prop("checked", true);
        }
        else {
            $("#IsPrimary").prop("checked", false);
        }
        showLoader();
    }
    return result;
}

function onAddBillingSucess(data) {
    HideLoader();
    {
        if (data.status == true) {
            GenerateAlert(data.msg, "", "success");
            LoadHTML("User", "Billing", "BillingList");
        }
        else {
            GenerateAlert(data.msg, "", "info");
        }

    }
}

function onAddBillingFailure(data) {
    HideLoader();
    GenerateAlert(data.msg, "", "error");
}

function SelectPaymentAllCheckBox(input) {
    //$(".SelectedCheckBox").click();
    selectedList = [];
    if ($(input).is(":checked") == true) {
        $(".SelectedCheckBox").each(function () {
            selectedList.push($(this).val());
            $(this).prop("checked", true);
        });
    }
    else {
        $(".SelectedCheckBox").each(function () {

            $(this).prop("checked", false);
            selectedList = [];
        });
    }
}

function DeletePayment() {
    var control = $(".SelectedCheckBox");
    var Id = [];
    $.each(control, function (index, value) {
        if (value.checked) {
            Id.push(value.value);
        }
    });

    if (Id.length > 0) {
        bootbox.confirm({
            message: "Are you sure want to delete this payment profile?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    showLoader();
                    AJAXPaymentCaller("User", "Billing", "DeletePayment", Id);
                }
            }
        });
    }
    else {
        GenerateAlert("Please select to delete the payment profile!", "", "info");
    }

}

function AJAXPaymentCaller(AreaName, ControllerName, ActionName, data) {
    showLoader();
    $.ajax({
        url: `/${AreaName}/${ControllerName}/${ActionName}?id=` + data,
        type: "Get",
        data: { "id": data },
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result.status) {
                HideLoader();
                GenerateAlert(result.msg, "", "success", PyamentList);
            }
            else {
                HideLoader();
                GenerateAlert(result.msg, "", "info", PyamentList);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function deleteConfirmation(id) {
    bootbox.confirm({
        message: "Are you sure want to delete this payment profile?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "/User/Billing/DeletePayment",
                    data: JSON.stringify({
                        "id": id
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        HideLoader();
                        if (result.status) {
                            HideLoader();

                            GenerateAlert(result.msg, "", "success", PyamentList);
                        }
                        else {
                            HideLoader();
                            GenerateAlert(result.msg, "", "info", PyamentList);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        HideLoader();
                    }
                });
            }
        }
    });
}

function setPrimaryCard(id) {
    showLoader();
    $.ajax({
        type: "POST",
        url: "/User/Billing/setPrimaryCard",
        data: JSON.stringify({
            "id": id
        }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            HideLoader();
            if (result.status) {
                HideLoader();
                GenerateAlert(result.msg, "", "success", PyamentList);
            }
            else {
                HideLoader();
                GenerateAlert(result.msg, "", "info", PyamentList);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function selectchk() {
    selectedList = [];
    $(".SelectedCheckBox").each(function () {
        if ($(this).is(":checked")) {
            selectedList.push($(this).val());
        }
    });
    if ($(".SelectedCheckBox").length == selectedList.length) {
        $("#MasterCheckbox").prop("checked", true);
    }
    else {
        $("#MasterCheckbox").prop("checked", false);
    }
}
