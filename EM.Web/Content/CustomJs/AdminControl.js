﻿function onAddProvider()
{
    var result = true;
    if (!emptyStringValidator($('#ProviderName'))) {
        $('#ProviderName').focus();
        $("#lblProviderName").html("Provider Name is required");
        result = false;
    }
    else {
        $("#lblProviderName").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;

}

function onAddProviderSucess(data)
{
    HideLoader();
    if (data.status != "Error")
    {
        $('#ProviderName').val('');
        GenerateAlert(data.status, data.msg, "success", bindMasterData);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddProviderFailure(data)
{
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");
}

function redirectToControl()
{
    window.location.href = "/Admin/AdminControl/MasterData";
}

function bindMasterData() {
    var dColumnsDef =
        [{

            "targets": [1],
            "visible": true,
            "searchable": false,
            "orderable": false,
            "className": 'text-center'
        }];
    var columns =
        [
            { "data": "ProviderName", "title": "Providers", "name": "ProviderName", "autoWidth": true },
            {
                data: null, "title": "Action", render: function (data, type, row)
                {

                    var clickEditFuncton = 'getEditProviders(' + data.ProviderId + ')';

                    var clickDeleteFuncton = 'getDeleteProviders(' + data.ProviderId + ')';

                    return '<a href="#!" onclick=\'' + clickEditFuncton + '\'><img src="/Content/admin/images/edit-ic.png" style="width:20px;" alt=""/></a> <a href="#!" onclick=\'' + clickDeleteFuncton + '\'><img src="/Content/imagesHome/deletet-ic.png" style="width:20px;" alt=""/></a>';


                }
            },
        ];
    
    InitDataTable($("#masterdataTable"), "/Admin/AdminControl/getProviderList/", null, columns, dColumnsDef, [[0, "desc"]], 0, false, true);
}

$(document).ready(function (e) {
    $('#masterdataActive').addClass("active");
    bindMasterData();
});

function getEditProviders(ProviderId)
{
    showLoader();
    $.ajax({
        url: "/Admin/AdminControl/EditProvider",
        type: 'Post',
        dataType: 'html',
        data: JSON.stringify({ ProviderId: ProviderId }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            HideLoader();
            $("#editContainer").html(response);
            $('#editProvider').modal('show');

        },
        failure: function (failureData) {
            HideLoader();

        },
        error: function (errorData) {
            HideLoader();

        }
    });
}

function onEditProvider()
{
    var result = true;
    if (!emptyStringValidator($('#txtProviderName'))) {
        $('#txtProviderName').focus();
        $("#providerError").html("Provider Name is required");
        result = false;
    }
    else {
        $("#providerError").empty();

    }
    if (result == true) {
        showLoader();
    }
    return result;
}

function Clearcontrol()
{
    $('#ProviderName').val('');

}

function getDeleteProviders(ProviderId)
{
 
        bootbox.confirm({
            message: "Are you sure want to delete this provider?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                
                if (result)
                {

                    DeleteProvider(ProviderId)
                }
                else {
                    bootbox.hideAll();
                }
            }
        });
}

function DeleteProvider(ProviderId)
{
    showLoader();
    $.ajax({
        url: "/Admin/AdminControl/DeleteProvider",
        type: "Post",
        data: JSON.stringify({ ProviderId: ProviderId }),
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (result) {
            HideLoader();
            if (result)
            {

                HideLoader();
                if (result.status != "Error") {
                    GenerateAlert(result.status, result.msg, "success", bindMasterData);


                }
                else {
                    GenerateAlert(result.status, result.msg, "info");
                }
                
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            HideLoader();
        }
    });
}

function onEditProviderSucess(data)
{
    HideLoader();
    if (data.status != "Error") {
        $('#txtProviderName').val('');
        $("#editProvider .close").click();
        GenerateAlert(data.status, data.msg, "success", bindMasterData);
    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}
