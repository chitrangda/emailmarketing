﻿function onAddLeadBegin()
{
    var result = true;
    if (!emptyStringValidator($('#salesPortal_Leads_FirstName'))) {
        $('#salesPortal_Leads_FirstName').focus();
        $("#lblFirstName").html("First Name is required");
        result = false;
    }
    else {
        $("#lblFirstName").empty();

    }
    if (!emptyStringValidator($('#salesPortal_Leads_Email'))) {
        $('#salesPortal_Leads_Email').focus();
        $("#lblEmail").html("Email is required");
        result = false;
    }
    else {
        if (!EmailValidator($('#salesPortal_Leads_Email'))) {
            $('#salesPortal_Leads_Email').focus();
            $("#lblEmail").html("Invalid Email");
            result = false;
        }
        else {
            $("#lblEmail").empty();
        }
    }

    if ($('#salesPortal_Leads_OpportunityAmount').val() != "") {
        if (!NumberValidator($('#salesPortal_Leads_OpportunityAmount'))) {
            $('#salesPortal_Leads_OpportunityAmount').focus();
            $("#lblOpportunityAmount").html("Enter only digits");
            result = false;
        }
        else {
            $("#lblOpportunityAmount").empty();
        }
    }
    if (result == true)
    {
        showLoader();
    }
    return result;  
}
$(document).ready(function (e)
{
    $('#createLead').addClass("active");
    $('#salesPortal_Leads_MobileNo').usPhoneFormat({
        format: '(xxx) xxx-xxxx',
    })
    $('#salesPortal_Leads_PhoneNo').usPhoneFormat({
        format: '(xxx) xxx-xxxx',
    })
});
function onAddLeadSucess(data)
{
    HideLoader();
    if (data.status != "Error") {
        GenerateAlert(data.status, data.msg, "success", Redirect);


    }
    else {
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddLeadFailure(data)
{
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}

function Redirect()
{
    window.location.href = "/SalesRep/Dashboard/Index";
}

function onAddProvider()
{

    var result = true;
    if (!emptyStringValidator($('#providers_ProviderName'))) {
        $('#providers_ProviderName').focus();
        $("#lblProviderName").html("Provider Name is required");
        result = false;
    }
    else {
        $("#lblProviderName").empty();

    }
    if (result == true)
    {
        showLoader();
    }
    return result;
}
function onAddProviderSucess(data)
{
    HideLoader();
    if (data.status != "Error") {
        $('#providers_ProviderName').val('');
        GenerateAlert(data.status, data.msg, "success",false);


    }
    else {
        $('#providers_ProviderName').val('');
        GenerateAlert(data.status, data.msg, "info");
    }
}

function onAddProviderFailure(data) {
    HideLoader();
    GenerateAlert(data.status, data.msg, "info");

}
function RedirectToLead()
{
    window.location.href = "/SalesRep/Leads/CreateNewLead";
}

function ProviderModalPopup()
{

        $.ajax({
            url: "/SalesRep/Leads/AddProviders/",
            type: 'Post',
            dataType: 'html',  
            contentType: "application/json; charset=utf-8",
            success: function () {
                HideLoader();
                $("#ProviderModal").html();
            },
            failure: function (failureData) {
                HideLoader();

            },
            error: function (errorData) {
                HideLoader();

            }
        });
    }


    

    
