USE [master]
GO
/****** Object:  Database [EmailMarketingDb]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE DATABASE [EmailMarketingDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'[EmailMarketingDb]', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLSERVER2014\MSSQL\DATA\[EmailMarketingDb].mdf' , SIZE = 67584KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'[EmailMarketingDb]_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLSERVER2014\MSSQL\DATA\[EmailMarketingDb]_log.ldf' , SIZE = 149696KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EmailMarketingDb] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EmailMarketingDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EmailMarketingDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EmailMarketingDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EmailMarketingDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EmailMarketingDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EmailMarketingDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET RECOVERY FULL 
GO
ALTER DATABASE [EmailMarketingDb] SET  MULTI_USER 
GO
ALTER DATABASE [EmailMarketingDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EmailMarketingDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EmailMarketingDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EmailMarketingDb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EmailMarketingDb] SET DELAYED_DURABILITY = DISABLED 
GO
USE [EmailMarketingDb]
GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitBigInt]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnSplitBigInt](
       -- Add the parameters for the function here
       @input NVARCHAR(4000))
RETURNS @retBigint TABLE( [Value] [BIGINT] NOT NULL )
AS
BEGIN
DECLARE @bigint NVARCHAR(100);
DECLARE @pos [INT];
SET @input = LTRIM(RTRIM(@input)) + ','; -- TRIMMING THE BLANK SPACES
SET @pos = CHARINDEX(',', @input, 1); -- OBTAINING THE STARTING POSITION OF COMMA IN THE GIVEN STRING
IF REPLACE(@input, ',', '') <> '' -- CHECK IF THE STRING EXIST FOR US TO SPLIT
    BEGIN
     WHILE @pos > 0
      BEGIN
       SET @bigint = LTRIM(RTRIM(LEFT(@input, @pos - 1))); -- GET THE 1ST INT VALUE TO BE INSERTED
       IF @bigint <> ''
        BEGIN
            INSERT INTO @retBigint
             ( Value )
            VALUES( CAST(@bigint AS [BIGINT]));
        END;
       SET @input = RIGHT(@input, LEN(@input) - @pos); -- RESETTING THE INPUT STRING BY REMOVING THE INSERTED ONES
       SET @pos = CHARINDEX(',', @input, 1); -- OBTAINING THE STARTING POSITION OF COMMA IN THE RESETTED NEW STRING
      END;
    END;
       RETURN;
       END;
GO
/****** Object:  UserDefinedFunction [dbo].[Func_GetProviderName]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[Func_GetProviderName]
(
	
	@ProviderId VARCHAR(max) -- Please Send Provider Id  in like 1,2,3 
)
RETURNS  Varchar(max)
AS
BEGIN

Declare @x XML 
select @x = cast('<A>'+ replace(@ProviderId,',','</A><A>')+ '</A>' as xml)

DECLARE @Sql VARCHAR(max),@ProviderName AS VARCHAR(max);

 SELECT @ProviderName= Stuff(( 
SELECT
', ' + 
 spp.ProviderName
 
  FROM dbo.SalesPortal_Providers AS spp
WHERE spp.ProviderId IN (select t.value('.', 'int') as inVal
from @x.nodes('/A') as x(t)
)
ORDER BY  spp.ProviderName
                  FOR XML PATH(''),TYPE 
                  ).value('.','VARCHAR(MAX)') 
                 , 1,1,'')  
	
				RETURN @ProviderName

END

GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActionLog]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActionLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MethodName] [nvarchar](100) NULL,
	[ActionName] [nvarchar](100) NULL,
	[ControllerName] [nvarchar](100) NULL,
	[AreaName] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NULL,
	[UserRefId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_ActionLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AnTransactionLog]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AnTransactionLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NULL,
	[AnTransactionId] [nvarchar](150) NULL,
	[TransactionType] [nvarchar](50) NULL,
	[TransactionDate] [datetime] NULL,
	[Amount] [nvarchar](50) NULL,
	[Message] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[PaymentProfileId] [nvarchar](200) NULL,
 CONSTRAINT [PK_AnTransactionLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BeeTemplates]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BeeTemplates](
	[BeeTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[BeeTemplateName] [nvarchar](50) NULL,
	[BeeTemplatePath] [nvarchar](250) NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
	[UserId] [nvarchar](128) NULL,
	[BeeTemplateHtml] [nvarchar](max) NULL,
	[BeeTemplateJson] [text] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_BeeTemplates] PRIMARY KEY CLUSTERED 
(
	[BeeTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BillType]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillType](
	[BillTypeId] [int] IDENTITY(1,1) NOT NULL,
	[BillFrequency] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BillTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Campaign](
	[CampaignId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ListId] [int] NOT NULL,
	[CampaignTypeID] [int] NOT NULL,
	[KeywordId] [int] NOT NULL,
	[Description] [nvarchar](50) NULL,
	[AutomaticResponse] [nvarchar](500) NULL,
	[ForwardedCellNumber] [nvarchar](11) NULL,
	[ForwardedEmail] [nvarchar](60) NULL,
	[SentDate] [datetime] NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
	[HtmlContent] [nvarchar](max) NULL,
	[Subject] [nvarchar](500) NULL,
	[FromName] [nvarchar](100) NULL,
	[FromEmail] [nvarchar](100) NULL,
	[PreviewText] [nvarchar](max) NULL,
	[TemplateId] [int] NULL,
	[CamapignName] [nvarchar](100) NULL,
	[Status] [nvarchar](50) NULL,
	[ClickedCount] [int] NULL CONSTRAINT [DF_Campaign_ClickedCount]  DEFAULT ((0)),
	[OpenCount] [int] NULL CONSTRAINT [DF_Campaign_OpenCount]  DEFAULT ((0)),
	[JSONContent] [nvarchar](max) NULL,
	[ContentImagePath] [nvarchar](100) NULL,
	[SubscriberIds] [nvarchar](max) NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CampaignList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CampaignList](
	[CampaignTypeId] [int] IDENTITY(1,1) NOT NULL,
	[CampaignName] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](128) NULL,
 CONSTRAINT [PK_CampaignList] PRIMARY KEY CLUSTERED 
(
	[CampaignTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignType]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignType](
	[CampaignTypeID] [int] IDENTITY(1,1) NOT NULL,
	[CampaignName] [nvarchar](25) NOT NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CampaignTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClientType]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientType](
	[ClientTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ClientType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ClientType] PRIMARY KEY CLUSTERED 
(
	[ClientTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[County]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[County](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](max) NOT NULL,
	[createddate] [datetime] NULL,
	[modifieddate] [datetime] NULL,
 CONSTRAINT [PK_County] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ELMAH_Error]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ELMAH_Error](
	[ErrorId] [uniqueidentifier] NOT NULL,
	[Application] [nvarchar](60) NOT NULL,
	[Host] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Source] [nvarchar](60) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
	[User] [nvarchar](50) NOT NULL,
	[StatusCode] [int] NOT NULL,
	[TimeUtc] [datetime] NOT NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
	[AllXml] [ntext] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ErrorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailServiceLog]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailServiceLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NULL,
	[Description] [nvarchar](max) NULL,
	[LogDateTime] [datetime] NULL,
	[Createdby] [nvarchar](200) NULL,
 CONSTRAINT [PK_EmailServiceLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Fields]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](128) NULL,
	[ModifiedDate] [datetime] NULL,
	[UserId] [nvarchar](128) NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Fields_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Fields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Industry]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Industry](
	[IndustryID] [int] IDENTITY(1,1) NOT NULL,
	[IndustryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_IndustryType] PRIMARY KEY CLUSTERED 
(
	[IndustryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice](
	[InvoiceID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NULL,
	[OrderID] [nvarchar](50) NULL,
	[AmountCharged] [nvarchar](200) NULL,
	[InvoiceType] [nvarchar](50) NULL,
	[CardType] [nvarchar](20) NULL,
	[CardNumber] [nvarchar](200) NULL,
	[ExpiresYear] [nvarchar](30) NULL,
	[ExpiresMonth] [nvarchar](30) NULL,
	[NameOnCard] [nvarchar](50) NULL,
	[Phone] [nvarchar](20) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](30) NULL,
	[Zip] [nvarchar](10) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedById] [nvarchar](128) NULL,
	[LastModByDate] [datetime] NULL,
	[LastModById] [nvarchar](128) NULL,
	[OfferCode] [nvarchar](50) NULL,
	[TransactionStatus] [nvarchar](50) NULL,
	[TransactionError] [nvarchar](200) NULL,
	[PaymentTypeId] [int] NULL,
	[NoofCredits] [int] NULL,
	[BillingAmount] [nvarchar](50) NULL,
	[AnTransactionId] [nvarchar](150) NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Keyword]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Keyword](
	[KeywordId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[Word] [nchar](15) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[KeywordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[List]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List](
	[ListId] [int] IDENTITY(1,1) NOT NULL,
	[ListName] [varchar](50) NOT NULL,
	[FromEmail] [varchar](50) NULL,
	[FromName] [varchar](50) NULL,
	[IsDeleted] [bit] NULL CONSTRAINT [DF__Group__IsDeleted__300424B4]  DEFAULT ((0)),
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__Group__CreatedDa__30F848ED]  DEFAULT (getdate()),
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL CONSTRAINT [DF__Group__LastModDa__31EC6D26]  DEFAULT (getdate()),
	[Remainder] [varchar](50) NULL,
	[Company] [varchar](50) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[City] [varchar](50) NULL,
	[Zip] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Optin] [bit] NULL,
	[GDPR] [bit] NULL,
	[Subscriber/Unsubscriber] [bit] NULL,
	[Subscriber] [bit] NULL,
	[Unsubscriber] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[ListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ListFields]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldId] [int] NULL,
	[ListId] [int] NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_ListFields_IsActive]  DEFAULT ((0)),
 CONSTRAINT [PK_ListFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NoteType]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NoteType](
	[NoteTypeId] [int] IDENTITY(1,1) NOT NULL,
	[NoteType] [varchar](50) NOT NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_NoteType] PRIMARY KEY CLUSTERED 
(
	[NoteTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Package]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Package](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NULL,
	[BillingAmount] [float] NULL,
	[TotalMonth] [int] NULL,
 CONSTRAINT [PK_PlanPricing] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PaymentType]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentType](
	[PaymentTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentTypeName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PaymentType] PRIMARY KEY CLUSTERED 
(
	[PaymentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Plan]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Plan](
	[PlanId] [int] IDENTITY(1,1) NOT NULL,
	[PlanName] [varchar](200) NULL,
	[NoOfSubscribersMin] [int] NULL,
	[NoOfSubscribersMax] [int] NULL,
	[ContactManagement] [bit] NULL,
	[UnlimitedEmails] [bit] NULL,
	[Automation] [bit] NULL,
	[Segmenting] [bit] NULL,
	[Analytics] [bit] NULL,
	[EmailScheduling] [bit] NULL,
	[CustomizableTemplates] [bit] NULL,
	[EducationalResources] [bit] NULL,
	[LiveSupport] [bit] NULL,
	[NoOfCredits] [nvarchar](50) NULL,
	[TotalEmails] [int] NULL,
	[TotalDay] [int] NULL,
 CONSTRAINT [PK__Plan__755C22B7D81AF5A9] PRIMARY KEY CLUSTERED 
(
	[PlanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Salesportal_AdminRepActivity]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salesportal_AdminRepActivity](
	[RepActivityID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](100) NULL,
	[UserId] [nvarchar](128) NULL,
	[RepId] [nvarchar](128) NULL,
	[Category] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[NoteId] [int] NULL,
	[IsComment] [bit] NULL,
 CONSTRAINT [PK_Salesportal_AdminRepActivity] PRIMARY KEY CLUSTERED 
(
	[RepActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalesPortal_lead_Color]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesPortal_lead_Color](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [nvarchar](50) NULL,
	[hexvalue] [nvarchar](50) NULL,
	[Decription] [nchar](10) NULL,
 CONSTRAINT [PK_SalesPortal_lead_Color] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalesPortal_LeadQuality]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesPortal_LeadQuality](
	[LeadQualityID] [int] IDENTITY(1,1) NOT NULL,
	[LeadQualityName] [nvarchar](50) NOT NULL,
	[Act] [bit] NULL,
 CONSTRAINT [PK_SalesPortal_LeadQuality] PRIMARY KEY CLUSTERED 
(
	[LeadQualityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalesPortal_Leads]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesPortal_Leads](
	[LeadId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[JobTitle] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[IndustryId] [int] NULL,
	[MobileNo] [varchar](14) NULL,
	[PhoneNo] [varchar](14) NULL,
	[Email] [varchar](50) NULL,
	[Website] [varchar](100) NULL,
	[LinkedIn] [varchar](100) NULL,
	[Twitter] [varchar](100) NULL,
	[Instagram] [varchar](100) NULL,
	[Facebook] [varchar](100) NULL,
	[ProviderId] [nvarchar](250) NULL,
	[ProviderName] [nvarchar](50) NULL,
	[HotId] [int] NULL,
	[OpportunityAmount] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](128) NULL,
	[FollowUpDate] [datetime] NULL,
	[SalesRepID] [nvarchar](128) NULL,
	[IsDeleted] [bit] NULL,
	[State] [nvarchar](50) NULL,
	[ColorId] [int] NULL,
	[FollowupTime] [datetime] NULL,
 CONSTRAINT [PK_SalesPortal_Leads] PRIMARY KEY CLUSTERED 
(
	[LeadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesPortal_Leads_EmailIdList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesPortal_Leads_EmailIdList](
	[EmailRowId] [int] IDENTITY(1,1) NOT NULL,
	[LeadId] [int] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[IsPrimaryEmail] [bit] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](128) NULL,
 CONSTRAINT [PK_SalesPortal_Leads_EmailIdList] PRIMARY KEY CLUSTERED 
(
	[EmailRowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesPortal_Leads_MobileNumList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesPortal_Leads_MobileNumList](
	[MobileRowId] [int] IDENTITY(1,1) NOT NULL,
	[LeadId] [int] NOT NULL,
	[MobileNumber] [varchar](12) NOT NULL,
	[IsPrimaryNumber] [bit] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](128) NULL,
 CONSTRAINT [PK_SalesPortal_Leads_MobileNumList] PRIMARY KEY CLUSTERED 
(
	[MobileRowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesPortal_Providers]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesPortal_Providers](
	[ProviderId] [int] IDENTITY(1,1) NOT NULL,
	[ProviderName] [nvarchar](100) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[LastModBy] [nvarchar](128) NULL,
	[LastModOn] [datetime] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_SalesPortal_Providers] PRIMARY KEY CLUSTERED 
(
	[ProviderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalesPortalRepNote]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesPortalRepNote](
	[NoteId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[NoteDescription] [nvarchar](500) NULL,
	[FollowUpDate] [datetime] NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
	[IsDelete] [bit] NULL,
	[IsEditable] [bit] NULL,
	[NoteTypeId] [int] NULL,
	[IsComments] [bit] NULL,
	[TimeZone] [nvarchar](50) NULL,
 CONSTRAINT [PK_SalesPortalRepNotes] PRIMARY KEY CLUSTERED 
(
	[NoteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SalesRepProfile]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesRepProfile](
	[Id] [nvarchar](128) NOT NULL,
	[FirstName] [nvarchar](200) NULL,
	[LastName] [nvarchar](200) NULL,
	[ContactNo] [nvarchar](50) NULL,
	[NoOfClients] [int] NULL,
	[SpentTotal] [float] NULL,
	[ViewRepActivity] [bit] NULL,
	[CanExport] [bit] NULL CONSTRAINT [DF_SalesRepProfile_CanExport]  DEFAULT ((0)),
	[ExportLead] [bit] NULL CONSTRAINT [DF_SalesRepProfile_ExportLead]  DEFAULT ((0)),
	[Manager] [bit] NULL CONSTRAINT [DF_SalesRepProfile_Manager]  DEFAULT ((0)),
	[IsActive] [bit] NULL CONSTRAINT [DF_SalesRepProfile_IsActive]  DEFAULT ((0)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_SalesRepProfile_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](128) NULL,
	[ModifedDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](128) NULL,
 CONSTRAINT [PK_SalesRepProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScheduledCampaign]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduledCampaign](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CampaignId] [int] NULL,
	[ScheduledDateTime] [datetime] NULL,
 CONSTRAINT [PK_ScheduledCampaign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ScheduleFrequency]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleFrequency](
	[FrequencyId] [int] IDENTITY(1,1) NOT NULL,
	[Frequency] [nvarchar](50) NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[FrequencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[State]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[State](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [varchar](max) NOT NULL,
	[CountryId] [int] NULL,
	[modifieddate] [datetime] NULL,
	[createddate] [datetime] NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subscriber]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscriber](
	[SubscriberId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](50) NULL,
	[UserId] [int] NULL,
	[DOB] [datetime] NULL,
	[ZipCode] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF__Contact__IsActiv__2C3393D0]  DEFAULT ((1)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF__Contact__IsDelet__2D27B809]  DEFAULT ((0)),
	[IsOptIn] [bit] NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[OptOutdate] [datetime] NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF__Contact__Created__2E1BDC42]  DEFAULT (getdate()),
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL CONSTRAINT [DF__Contact__LastMod__2F10007B]  DEFAULT (getdate()),
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[State] [nvarchar](50) NULL,
	[PhoneNumber] [varchar](200) NULL,
	[UpdateProfile] [bit] NULL,
	[EmailPermission] [bit] NOT NULL CONSTRAINT [DF_Subscriber_EmailPermission]  DEFAULT ((1)),
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[SubscriberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubscriberList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberList](
	[SubscriberListId] [int] IDENTITY(1,1) NOT NULL,
	[ListId] [int] NOT NULL,
	[SubscriberId] [int] NOT NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
	[IsSubscribe] [bit] NULL CONSTRAINT [DF_SubscriberList_IsSubscribe]  DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[SubscriberListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Template]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[TemplateId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[TemplateName] [nvarchar](50) NULL,
	[TemplatePath] [nvarchar](250) NULL,
	[CreatedById] [nvarchar](128) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModById] [nvarchar](128) NULL,
	[LastModDate] [datetime] NULL,
	[UserId] [nvarchar](128) NULL,
	[TemplateHtml] [nvarchar](max) NOT NULL,
	[TemplateJSON] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK__Template__F87ADD272C68C53B] PRIMARY KEY CLUSTERED 
(
	[TemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TimeZone]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeZone](
	[TimeZoneID] [int] IDENTITY(1,1) NOT NULL,
	[TimeZoneName] [varchar](20) NULL,
	[GMT] [varchar](10) NULL,
 CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED 
(
	[TimeZoneID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserActivityLog]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserActivityLog](
	[LogId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NULL,
	[Description] [text] NULL,
	[DateTime] [datetime] NULL,
	[Createdby] [nvarchar](100) NULL,
 CONSTRAINT [PK_UserActivityLog] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPaymentProfile]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPaymentProfile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[AnPaymentProfileId] [nvarchar](150) NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](150) NULL,
	[Address1] [nvarchar](150) NULL,
	[Address2] [nvarchar](150) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Zip] [nvarchar](50) NULL,
	[Default] [bit] NOT NULL,
	[CreatedBy] [nvarchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModBy] [nvarchar](150) NULL,
	[LastModDate] [datetime] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_UserPaymentProfile_Active]  DEFAULT ((1)),
	[LastFour] [nvarchar](10) NULL,
	[ExMonth] [int] NOT NULL CONSTRAINT [DF__UserPayme__ExMon__59FA5E80]  DEFAULT (datepart(month,getdate())),
	[ExYear] [int] NOT NULL CONSTRAINT [DF__UserPayme__ExYea__5AEE82B9]  DEFAULT (datepart(year,getdate())),
	[Expired]  AS (case when [ExYear]>datepart(year,getdate()) then CONVERT([bit],(0)) when [ExYear]=datepart(year,getdate()) AND [ExMonth]>=datepart(month,getdate()) then CONVERT([bit],(0)) else CONVERT([bit],(1)) end),
	[CreatedById] [nvarchar](128) NULL,
	[LastModById] [nvarchar](128) NULL,
	[CardType] [nvarchar](150) NULL,
	[CreditCardNo] [nchar](100) NULL,
	[IsPrimary] [bit] NOT NULL CONSTRAINT [DF_UserPaymentProfile_IsPrimary]  DEFAULT ((0)),
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_UserPaymentProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPlan]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPlan](
	[UserPlanId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[PackageId] [int] NOT NULL CONSTRAINT [DF_UserPlan_PackageId]  DEFAULT ((0)),
	[MonthlyCredit] [nvarchar](50) NULL CONSTRAINT [DF_UserPlan_MonthlyCredit]  DEFAULT ((0)),
	[totalSpendCredits] [nvarchar](50) NULL CONSTRAINT [DF_UserPlan_totalSpendCredits]  DEFAULT ((0)),
	[totalRemainingCredits] [nvarchar](50) NULL CONSTRAINT [DF_UserPlan_totalRemainingCredits]  DEFAULT ((0)),
	[NoOfSubscribersMax] [int] NULL CONSTRAINT [DF_UserPlan_NoOfSubscribersMax]  DEFAULT ((0)),
	[NoOfSubscribersMin] [int] NULL CONSTRAINT [DF_UserPlan_NoOfSubscribersMin]  DEFAULT ((0)),
	[ContactManagement] [bit] NULL,
	[UnlimitedEmails] [bit] NULL,
	[Automation] [bit] NULL,
	[Segmenting] [bit] NULL,
	[Analytics] [bit] NULL,
	[EmailScheduling] [bit] NULL,
	[CustomizableTemplates] [bit] NULL CONSTRAINT [DF_UserPlan_CustomizableTemplates]  DEFAULT ((0)),
	[EducationalResources] [bit] NULL,
	[LiveSupport] [bit] NULL,
	[PackageUpdateDate] [datetime] NULL,
	[NextPackageUpdateDate] [datetime] NULL,
	[BillingStatus] [int] NULL CONSTRAINT [DF__UserPlan__Billin__2A164134]  DEFAULT ((1)),
	[BillingAmount] [float] NULL,
	[TotalEmails] [int] NULL CONSTRAINT [DF_UserPlan_TotalEmails]  DEFAULT ((0)),
	[TotalDays] [int] NULL CONSTRAINT [DF_UserPlan_TotalDays]  DEFAULT ((0)),
	[IsActive] [bit] NULL CONSTRAINT [DF_UserPlan_IsActive]  DEFAULT ((1)),
 CONSTRAINT [PK_UserPlan] PRIMARY KEY CLUSTERED 
(
	[UserPlanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile](
	[Id] [nvarchar](128) NOT NULL,
	[FirstName] [nvarchar](200) NULL,
	[LastName] [nvarchar](200) NULL,
	[ContactEmail] [nvarchar](200) NULL,
	[OfficePhone] [nvarchar](200) NULL,
	[MobilePhone] [nvarchar](200) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[City] [varchar](200) NULL,
	[State] [nvarchar](200) NULL,
	[Country] [nvarchar](200) NULL,
	[Zip] [nvarchar](50) NULL,
	[ProfileImage] [nvarchar](max) NULL,
	[CreditCardNo] [nvarchar](100) NULL,
	[ExpiryDate] [datetime] NULL,
	[RegisteredDate] [datetime] NULL CONSTRAINT [DF__UserProfi__creat__367C1819]  DEFAULT (getdate()),
	[IsActive] [bit] NULL CONSTRAINT [DF__UserProfi__IsAct__3864608B]  DEFAULT ((1)),
	[CompanyName] [nvarchar](200) NULL,
	[AuthProfileId] [nvarchar](max) NULL,
	[IndustryRefId] [int] NULL,
	[DOB] [datetime] NULL,
	[LastLogin] [datetime] NULL,
	[SalesRepId] [nvarchar](128) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CampaignTypeId] [int] NULL,
	[PromotionCode] [nvarchar](12) NULL,
	[FollowupDate] [datetime] NULL,
	[FollowupTime] [datetime] NULL,
	[TimeZoneId] [int] NULL,
 CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 4/1/2019 11:03:12 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Keyword] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ActionLog]  WITH CHECK ADD  CONSTRAINT [FK_ActionLog_AspNetUsers] FOREIGN KEY([UserRefId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[ActionLog] CHECK CONSTRAINT [FK_ActionLog_AspNetUsers]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK__Campaign__Create__38996AB5] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK__Campaign__Create__38996AB5]
GO
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK__Campaign__LastMo__3B75D760] FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK__Campaign__LastMo__3B75D760]
GO
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK__Campaign__UserId__3C69FB99] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK__Campaign__UserId__3C69FB99]
GO
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_List] FOREIGN KEY([ListId])
REFERENCES [dbo].[List] ([ListId])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_List]
GO
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_Template] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[Template] ([TemplateId])
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_Template]
GO
ALTER TABLE [dbo].[CampaignList]  WITH CHECK ADD  CONSTRAINT [FK_CampaignList_AspNetUsers] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[CampaignList] CHECK CONSTRAINT [FK_CampaignList_AspNetUsers]
GO
ALTER TABLE [dbo].[CampaignType]  WITH CHECK ADD FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[CampaignType]  WITH CHECK ADD FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Fields]  WITH CHECK ADD  CONSTRAINT [FK_Fields_AspNetUsers] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_Fields_AspNetUsers]
GO
ALTER TABLE [dbo].[Fields]  WITH CHECK ADD  CONSTRAINT [FK_Fields_AspNetUsers1] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_Fields_AspNetUsers1]
GO
ALTER TABLE [dbo].[Fields]  WITH CHECK ADD  CONSTRAINT [FK_Fields_AspNetUsers2] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_Fields_AspNetUsers2]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_UserProfile] FOREIGN KEY([LastModById])
REFERENCES [dbo].[UserProfile] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_UserProfile]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_UserProfile1] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[UserProfile] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_UserProfile1]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_UserProfile2] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([Id])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_UserProfile2]
GO
ALTER TABLE [dbo].[Keyword]  WITH CHECK ADD FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Keyword]  WITH CHECK ADD FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Keyword]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[List]  WITH CHECK ADD  CONSTRAINT [FK_List_AspNetUsers] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[List] CHECK CONSTRAINT [FK_List_AspNetUsers]
GO
ALTER TABLE [dbo].[List]  WITH CHECK ADD  CONSTRAINT [FK_List_AspNetUsers1] FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[List] CHECK CONSTRAINT [FK_List_AspNetUsers1]
GO
ALTER TABLE [dbo].[ListFields]  WITH CHECK ADD  CONSTRAINT [FK_ListFields_Fields] FOREIGN KEY([FieldId])
REFERENCES [dbo].[Fields] ([Id])
GO
ALTER TABLE [dbo].[ListFields] CHECK CONSTRAINT [FK_ListFields_Fields]
GO
ALTER TABLE [dbo].[ListFields]  WITH CHECK ADD  CONSTRAINT [FK_ListFields_List] FOREIGN KEY([ListId])
REFERENCES [dbo].[List] ([ListId])
GO
ALTER TABLE [dbo].[ListFields] CHECK CONSTRAINT [FK_ListFields_List]
GO
ALTER TABLE [dbo].[Package]  WITH CHECK ADD  CONSTRAINT [FK_PlanPricing_Plan] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([PlanId])
GO
ALTER TABLE [dbo].[Package] CHECK CONSTRAINT [FK_PlanPricing_Plan]
GO
ALTER TABLE [dbo].[SalesPortalRepNote]  WITH CHECK ADD  CONSTRAINT [FK_SalesPortalRepNotes_AspNetUsers] FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SalesPortalRepNote] CHECK CONSTRAINT [FK_SalesPortalRepNotes_AspNetUsers]
GO
ALTER TABLE [dbo].[SalesPortalRepNote]  WITH CHECK ADD  CONSTRAINT [FK_SalesPortalRepNotes_AspNetUsers1] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SalesPortalRepNote] CHECK CONSTRAINT [FK_SalesPortalRepNotes_AspNetUsers1]
GO
ALTER TABLE [dbo].[SalesPortalRepNote]  WITH CHECK ADD  CONSTRAINT [FK_SalesPortalRepNotes_UserProfile] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([Id])
GO
ALTER TABLE [dbo].[SalesPortalRepNote] CHECK CONSTRAINT [FK_SalesPortalRepNotes_UserProfile]
GO
ALTER TABLE [dbo].[SalesRepProfile]  WITH CHECK ADD  CONSTRAINT [FK_SalesRepProfile_AspNetUsers] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SalesRepProfile] CHECK CONSTRAINT [FK_SalesRepProfile_AspNetUsers]
GO
ALTER TABLE [dbo].[SalesRepProfile]  WITH CHECK ADD  CONSTRAINT [FK_SalesRepProfile_AspNetUsers1] FOREIGN KEY([Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SalesRepProfile] CHECK CONSTRAINT [FK_SalesRepProfile_AspNetUsers1]
GO
ALTER TABLE [dbo].[SalesRepProfile]  WITH CHECK ADD  CONSTRAINT [FK_SalesRepProfile_SalesRepProfile1] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SalesRepProfile] CHECK CONSTRAINT [FK_SalesRepProfile_SalesRepProfile1]
GO
ALTER TABLE [dbo].[ScheduledCampaign]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledCampaign_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([CampaignId])
GO
ALTER TABLE [dbo].[ScheduledCampaign] CHECK CONSTRAINT [FK_ScheduledCampaign_Campaign]
GO
ALTER TABLE [dbo].[ScheduleFrequency]  WITH CHECK ADD FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[ScheduleFrequency]  WITH CHECK ADD FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK__Contact__Created__3F466844] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK__Contact__Created__3F466844]
GO
ALTER TABLE [dbo].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK__Contact__LastMod__403A8C7D] FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK__Contact__LastMod__403A8C7D]
GO
ALTER TABLE [dbo].[SubscriberList]  WITH CHECK ADD  CONSTRAINT [FK__ContactGr__Conta__412EB0B6] FOREIGN KEY([SubscriberId])
REFERENCES [dbo].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [dbo].[SubscriberList] CHECK CONSTRAINT [FK__ContactGr__Conta__412EB0B6]
GO
ALTER TABLE [dbo].[SubscriberList]  WITH CHECK ADD FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SubscriberList]  WITH CHECK ADD FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserPaymentProfile]  WITH CHECK ADD  CONSTRAINT [FK_UserPaymentProfile_AspNetUsers] FOREIGN KEY([LastModById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserPaymentProfile] CHECK CONSTRAINT [FK_UserPaymentProfile_AspNetUsers]
GO
ALTER TABLE [dbo].[UserPaymentProfile]  WITH CHECK ADD  CONSTRAINT [FK_UserPaymentProfile_AspNetUsers1] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserPaymentProfile] CHECK CONSTRAINT [FK_UserPaymentProfile_AspNetUsers1]
GO
ALTER TABLE [dbo].[UserPaymentProfile]  WITH CHECK ADD  CONSTRAINT [FK_UserPaymentProfile_AspNetUsers2] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserPaymentProfile] CHECK CONSTRAINT [FK_UserPaymentProfile_AspNetUsers2]
GO
ALTER TABLE [dbo].[UserPlan]  WITH CHECK ADD  CONSTRAINT [FK__UserPlan__UserId__2BFE89A6] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserPlan] CHECK CONSTRAINT [FK__UserPlan__UserId__2BFE89A6]
GO
ALTER TABLE [dbo].[UserPlan]  WITH CHECK ADD  CONSTRAINT [FK_UserPlan_Package] FOREIGN KEY([PackageId])
REFERENCES [dbo].[Package] ([Id])
GO
ALTER TABLE [dbo].[UserPlan] CHECK CONSTRAINT [FK_UserPlan_Package]
GO
ALTER TABLE [dbo].[UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_UserProfile_AspNetUsers] FOREIGN KEY([Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK_UserProfile_AspNetUsers]
GO
ALTER TABLE [dbo].[UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_UserProfile_Industry] FOREIGN KEY([IndustryRefId])
REFERENCES [dbo].[Industry] ([IndustryID])
GO
ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK_UserProfile_Industry]
GO
ALTER TABLE [dbo].[UserProfile]  WITH CHECK ADD  CONSTRAINT [FK_UserProfile_SalesRepProfile] FOREIGN KEY([SalesRepId])
REFERENCES [dbo].[SalesRepProfile] ([Id])
GO
ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK_UserProfile_SalesRepProfile]
GO
/****** Object:  StoredProcedure [dbo].[ELMAH_LogError]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[ELMAH_LogError]  
  
(  
  
    @ErrorId UNIQUEIDENTIFIER,    
    @Application NVARCHAR(60),    
    @Host NVARCHAR(30),    
    @Type NVARCHAR(100),  
    @Source NVARCHAR(60),    
    @Message NVARCHAR(500),  
    @User NVARCHAR(50),   
    @AllXml NTEXT,    
    @StatusCode INT,   
    @TimeUtc DATETIME  
  
)  
  
AS  
  
SET NOCOUNT ON  
  
INSERT  
  
INTO  
  
    [ELMAH_Error]
(  
  
    [ErrorId],   
    [Application],   
    [Host],  
    [Type],  
    [Source],  
    [Message],    
    [User],    
    [AllXml],    
    [StatusCode],    
    [TimeUtc]  
  
)  
  
VALUES  
  
    (  
  
    @ErrorId,  
    @Application,    
    @Host,    
    @Type,    
    @Source,   
    @Message,    
    @User,   
    @AllXml,   
    @StatusCode,   
    @TimeUtc  
  
)  
GO
/****** Object:  StoredProcedure [dbo].[getSalesRepFollowUp]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[dbo].[getSalesRepFollowUp] '4ae051d4-2166-42ad-91f5-9510248effa8' ,null,null,0
CREATE procedure [dbo].[getSalesRepFollowUp]
@Id nvarchar(128)=NULL,
@dateFrom datetime=NULL,
@dateTo datetime = NULL,
@isCurrent bit = NULL
as
if @dateTo is null
set @dateTo = GETDATE()
select 
upt.Id as Id,
(CONVERT (varchar,upt.FollowupDate,110)) as FollowUpDate,
(CONVERT (varchar(10), upt.FollowupDate,108)) as FollowUpTime,
upt.FirstName + ISNULL(upt.LastName,'') as AccountName,
upt.ClientType as ClientType, upt.PromotionCode as PromotionCode,
upt.IsActive as AccountStatus, (CONVERT (varchar,upt.RegisteredDate,120)) as RegisteredDate,
upt.ContactEmail as Email,
spt.FirstName + ISNULL(spt.lastname,'') as RepName,
pt.PlanName,
uplant.totalSpendCredits
 from
UserProfile upt
Left join SalesRepProfile spt
on upt.SalesRepId = spt.Id
Left join UserPlan uplant
on upt.id = uplant.UserId and uplant.IsActive =1 
left join Package pat
on pat.Id = uplant.PackageId
inner join [plan] pt 
on pat.PlanId = pt.PlanId
where 
upt.FollowupDate is not null and upt.FollowupTime is not null
and
(@id is null or  upt.SalesRepId = @id)
And
(@dateFrom is null or convert(date,upt.FollowupDate) between Convert(date,@dateFrom) and Convert(date,@dateTo))
AND
(@isCurrent is null or 1 = case 
when @isCurrent = 1 and convert(date,upt.FollowupDate)>=Convert(date,@dateTo) then 1
when @isCurrent=0 and Convert(date,upt.FollowupDate)<Convert(date,@dateTo) then 1
else 0
end
)

GO
/****** Object:  StoredProcedure [dbo].[usp_geAllPlans]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_geAllPlans
CREATE procedure [dbo].[usp_geAllPlans]
as
select [Plan].*, Package.id, Package.BillingAmount,Package.TotalMonth from [Plan] inner join Package
on [Plan].PlanId = Package.PlanId
order by [Plan].NoOfSubscribersMax
GO
/****** Object:  StoredProcedure [dbo].[usp_getAdminDashboardData]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_getAdminDashboardData] 
@Name nvarchar(100) = NULL, 
  @Email nvarchar(100) = NULL, 
  @MobileNumber nvarchar(100) = NULL, 
  @NameOnCard nvarchar(100) = NULL, 
  @MumberType nvarchar(100) = NULL, 
  @ReferralCode nvarchar(100) = NULL, 
  @City nvarchar(100) = NULL, 
  @State nvarchar(100) = NULL, 
  @SalesRep nvarchar(100) = NULL, 
  @CampaignType nvarchar(100) = NULL, 
  @ClientType nvarchar(100) = NULL
   AS
    BEGIN 
--SET FMTONLY OFF 
CREATE TABLE #AdminTempData (Id nvarchar(128) NULL,
  Email nvarchar(100) NULL, 
  AccountName nvarchar(100) NULL, 
  Name nvarchar(100) NULL, 
  totalSpendCredits nvarchar(100) NULL, 
  IsActive bit NULL, 
  RegisteredDate nvarchar(100) NULL, 
  Days int NULL, 
  SentToday int NULL, 
  Contacts int NULL, 
  DOB nvarchar(100) NULL, 
  MonthlyCredit nvarchar(100) NULL, 
  BillingAmount int NULL, 
  NextPackageUpdateDate nvarchar(100) NULL, 
  BillingStatus int NULL, 
  CampaignType nvarchar(100) NULL, 
  SalesRepId nvarchar(100) NULL, 
  FollowUpDate nvarchar(100) NULL, 
  FollowUpTime nvarchar(100) NULL, 
  TimeZone nvarchar(100) NULL, 
  SalesRepName nvarchar(100) NULL, 
  ClientType nvarchar(100) NULL, 
  ReferralCode nvarchar(200) NULL, 
  CSVUpload bit NULL, 
  CustomizableTemplates bit NULL, 
  MemberType nvarchar(100) NULL, 
  ChargedDate nvarchar(100) NULL, 
  NameOnCard nvarchar(100) NULL, 
  MobileNumber nvarchar(30) NULL, 
  City nvarchar(50) NULL, 
  State nvarchar(50) NULL,
  FollowUpDateTime datetime null
) INSERT INTO #AdminTempData
SELECT 
  ut.Id, 
  ut.Email, 
  ISNULL(upt.FirstName, '') + ' ' + ISNULL(upt.LastName, '') AS AccountName, 
  rt.Name, 
  pt.totalSpendCredits, 
  upt.IsActive, 
  CONVERT(
    varchar, 
    CONVERT(varchar, upt.RegisteredDate, 110)
  ) AS RegisteredDate, 
  pt.TotalDays - DATEDIFF(
    DAY, 
    pt.PackageUpdateDate, 
    GETDATE()
  ) AS Days, 
  (
    SELECT 
      COUNT(*) 
    FROM 
      Campaign ct 
    WHERE 
      CONVERT(date, ct.SentDate) = CONVERT(
        date, 
        GETDATE()
      ) 
      AND ct.UserId = ut.id
  ) AS SentToday, 
  (
    SELECT 
      COUNT(*) 
    FROM 
      Subscriber st 
    WHERE 
      st.CreatedById = ut.id 
      AND IsDeleted = 0
  ) AS Contacts, 
  CONVERT(
    varchar, 
    CONVERT(varchar, upt.DOB, 110)
  ) AS DOB, 
  pt.MonthlyCredit, 
  pt.BillingAmount, 
  CONVERT(
    varchar, 
    CONVERT(
      varchar, pt.NextPackageUpdateDate, 
      110
    )
  ) AS NextPackageUpdateDate, 
  pt.BillingStatus, 
  ct.CampaignName AS CampaignType, 
  upt.SalesRepId, 
  Convert(varchar,Convert(date,upt.FollowUpDate),110),
  CONVERT(VARCHAR(15),CAST(upt.FollowUpDate AS TIME),100),
  zt.TimeZoneName,
  --    ) 
	--(
	--SELECT 
	--	TOP 1 CONVERT(varchar, FollowUpDate, 110) 
	--FROM 
	--	SalesPortalRepNote 
	--WHERE 
	--	UserId = ut.id 
	--	AND IsDelete = 0 
	--ORDER BY 
	--	LastModDate DESC, 
	--	CreatedDate DESC
	--) AS 'FollowUpDate', 
  --(
  --  SELECT 
  --    TOP 1 CONVERT(
  --      VARCHAR(15), 
  --      CAST(FollowUpDate AS TIME), 
  --      100
  --    ) 
  --  FROM 
  --    SalesPortalRepNote 
  --  WHERE 
  --    UserId = ut.Id 
  --    AND IsDelete = 0 
  --  ORDER BY 
  --    LastModDate DESC, 
  --    CreatedDate DESC
  --) AS 'FollowUpTime', 
  --(
  --  SELECT 
  --    TOP 1 TimeZone 
  --  FROM 
  --    SalesPortalRepNote 
  --  WHERE 
  --    UserId = ut.Id 
  --    AND IsDelete = 0 
  --  ORDER BY 
  --    LastModDate DESC, 
  --    CreatedDate DESC
  --) AS 'TimeZone', 
  srp.FirstName AS SalesRepName, 
  upt.ClientType, 
  upt.PromotionCode AS 'ReferralCode', 
  1 AS 'CSV Upload', 
  pt.CustomizableTemplates, 
  (
    SELECT 
      planname 
    FROM 
      [package] pge 
      INNER JOIN [plan] p ON pge.planid = p.planid 
      INNER JOIN userplan upt ON pge.id = upt.packageid 
    WHERE 
      upt.userid = ut.id 
      AND upt.isactive = 1
  ) AS 'MemberType', 
  (
    CONVERT(
      varchar, pt.PackageUpdateDate, 110
    )
  ) AS ChargedDate, 
  (upp.FirstName + ' ' + upp.LastName) AS NameOnCard, 
  upt.MobilePhone, 
  upt.City, 
  upt.State,
  upt.FollowupTime
FROM 
  AspNetUsers ut 
  INNER JOIN UserProfile upt ON ut.id = upt.id 
  INNER JOIN UserPlan pt ON ut.id = pt.UserId 
  AND pt.IsActive = 1 
  INNER JOIN PACKAGE pat ON pt.PackageId = pat.Id 
  INNER JOIN AspNetUserRoles urt ON ut.id = urt.UserId 
  INNER JOIN AspNetRoles rt ON rt.Id = urt.RoleId 
  LEFT JOIN dbo.SalesRepProfile srp ON upt.SalesRepId = srp.id 
  LEFT JOIN dbo.UserPaymentProfile upp ON ut.Id = upp.UserId 
  AND upp.Active = 1 
  AND upp.IsPrimary = 1 
  LEFT JOIN dbo.CampaignList ct ON upt.CampaignTypeId = ct.CampaignTypeId 
  LEFT JOIN TimeZone zt on upt.TimeZoneId = zt.TimeZoneId
WHERE 
  rt.Name != 'Admin' 
ORDER BY 
  upt.RegisteredDate DESC 
SELECT 
  * 
FROM 
  #AdminTempData
WHERE 
  (
    @Name IS NULL 
    OR (AccountName LIKE '%' + @Name + '%')
  ) 
  AND (
    @Email IS NULL 
    OR (Email LIKE '%' + @Email + '%')
  ) 
  AND (
    @MobileNumber IS NULL 
    OR (
      MobileNumber LIKE '%' + @MobileNumber + '%'
    )
  ) 
  AND (
    @NameOnCard IS NULL 
    OR (
      NameOnCard LIKE '%' + @NameOnCard + '%'
    )
  ) 
  AND (
    @City IS NULL 
    OR (City LIKE '%' + @City + '%')
  ) 
  AND (
    @State IS NULL 
    OR (State LIKE '%' + @State + '%')
  ) 
  AND (
    @SalesRep IS NULL 
    OR (
      SalesRepName LIKE '%' + @SalesRep + '%'
    )
  ) 
  AND (
    @CampaignType IS NULL 
    OR (
      CampaignType LIKE '%' + @CampaignType + '%'
    )
  ) 
  AND (
    @ClientType IS NULL 
    OR (
      ClientType LIKE '%' + @ClientType + '%'
    )
  ) --and ClientType = ISNULL(@ClientType,ClientType)
DROP 
  TABLE #AdminTempData END

GO
/****** Object:  StoredProcedure [dbo].[usp_getAdminData]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  --usp_getSalesRepData 
    --usp_getSalesRepData '2019-01-01','2019-01-30'
  --usp_getSalesRepData null,null,2019
  --usp_getSalesRepData  null,null,2018,'Q4'
  --usp_getSalesRepData null,null,2019,null,1
  --usp_getSalesRepData null,null,null,null,null,null,null,null,null,null,4
  --usp_getSalesRepData null,null,null,null,null,'Grafton'
  --usp_getSalesRepData null,null,null,null,null,null,'Massachusetts'
  --usp_getSalesRepData null,null,null,null,null,null,NULL,'92602'
  --usp_getSalesRepData null,null,null,null,null,null,NULL,NULL,NULL,NULL,NULL,0
  --usp_getSalesRepData null,null,null,null,null,null,NULL,NULL,NULL,NULL,NULL,NULL,2
  --usp_getSalesRepData null,null,null,null,null,null,NULL,NULL,NULL,2
  CREATE PROCEDURE [dbo].[usp_getAdminData] 
  @FromDate date = NULL, 
  @ToDate date = NULL, 
  @Year int = NULL, 
  @Quarter nvarchar(250) = NULL, 
  @Month int = NULL, 
  @City nvarchar(100) = NULL, 
  @State nvarchar(100) = NULL, 
  @Zip nvarchar(50) = NULL, 
  @TimeZone nvarchar(20)= NULL, 
  @Find nvarchar(250) = NULL, 
  @MemberType int = NULL, 
  @Status bit = NULL, 
  @CampaignType int = NULL 
  AS 
  BEGIN 
  --SET FMTONLY OFF
  --CREATE TABLE #AdminTempData (
  --  Id nvarchar(128) NULL,
  --  Email nvarchar(100) NULL,
  --  AccountName nvarchar(100) NULL,
  --  Name nvarchar(100) NULL,
  --  totalSpendCredits nvarchar(100) NULL,
  --  IsActive bit NULL,
  --  RegisteredDate nvarchar(100) NULL,
  --  SentToday int NULL,
  --  Contacts int NULL,
  --  MonthlyCredit nvarchar(100) NULL,
  --  BillingAmount int NULL,
  --  NextPackageUpdateDate nvarchar(100) NULL,
  --  BillingStatus int NULL,
  --  CampaignType nvarchar(100) NULL,
  --  FollowUpDate nvarchar(100) NULL,
  --  ReferralCode nvarchar(200) NULL,
  --  MemberType nvarchar(100) NULL,
  --  ChargedDate nvarchar(100) NULL,
  --  City nvarchar(50) NULL,
  --  State nvarchar(50) NULL,
  --  FollowUpDateTime datetime NULL
  --)
  --INSERT INTO #AdminTempData
  WITH
  CTEData
  as(
SELECT 
  ut.Id, 
  ut.Email, 
  ISNULL(upt.FirstName, '') + ' ' + ISNULL(upt.LastName, '') AS AccountName, 
  ISNULL(pt.totalSpendCredits,0) as totalSpendCredits,
  upt.IsActive, 
  CONVERT(
    varchar, 
    CONVERT(varchar, upt.RegisteredDate, 110)
  ) AS RegisteredDate, 
  (
    SELECT 
      COUNT(*) 
    FROM 
      Campaign ct 
    WHERE 
      CONVERT(date, ct.SentDate) = CONVERT(
        date, 
        GETDATE()
      ) 
      AND ct.UserId = ut.id
  ) AS SentToday, 
  (
    SELECT 
      COUNT(*) 
    FROM 
      Subscriber st 
    WHERE 
      st.CreatedById = ut.id 
      AND IsDeleted = 0
  ) AS Contacts, 
  pt.MonthlyCredit, 
  ISNULL(pt.BillingAmount,0) as BillingAmount,
  CONVERT(
    varchar, 
    CONVERT(
      varchar, pt.NextPackageUpdateDate, 
      110
    )
  ) AS NextPackageUpdateDate, 
  pt.BillingStatus, 
  ct.CampaignName AS CampaignType, 
  (
    SELECT 
      TOP 1 CONVERT(varchar, FollowUpDate, 110) 
    FROM 
      SalesPortalRepNote 
    WHERE 
      UserId = ut.id 
      AND IsDelete = 0 
    ORDER BY 
      LastModDate DESC, 
      CreatedDate DESC
  ) AS 'FollowUpDate', 
  upt.PromotionCode AS 'ReferralCode', 
  (
    SELECT 
      planname 
    FROM 
      [package] pge 
      INNER JOIN [plan] p ON pge.planid = p.planid 
      INNER JOIN userplan upt ON pge.id = upt.packageid 
    WHERE 
      upt.userid = ut.id 
      AND upt.isactive = 1
  ) AS 'MemberType', 
  (
    CONVERT(
      varchar, pt.PackageUpdateDate, 110
    )
  ) AS ChargedDate, 
  upt.City, 
  upt.State, 
  (
    SELECT 
      TOP 1 CONVERT(datetime, FollowUpDate, 110) 
    FROM 
      SalesPortalRepNote 
    WHERE 
      UserId = ut.id 
      AND IsDelete = 0 
    ORDER BY 
      LastModDate DESC, 
      CreatedDate DESC
  ) AS 'FollowUpDateTime' ,
  upt.CampaignTypeId,
  upt.LastLogin,
  pat.PlanId,
  upt.Zip,
  (select top 1 SentDate from Campaign where UserId= ut.id order by CreatedDate desc) as SentDate,
  upt.RegisteredDate as 'RegDate'

FROM 
  AspNetUsers ut 
  INNER JOIN UserProfile upt ON ut.id = upt.id 
  INNER JOIN UserPlan pt ON ut.id = pt.UserId 
  AND pt.IsActive = 1 
  INNER JOIN PACKAGE pat ON pt.PackageId = pat.Id 
  INNER JOIN AspNetUserRoles urt ON ut.id = urt.UserId 
  INNER JOIN AspNetRoles rt ON rt.Id = urt.RoleId 
  LEFT JOIN dbo.SalesRepProfile srp ON upt.SalesRepId = srp.id 
  LEFT JOIN dbo.UserPaymentProfile upp ON ut.Id = upp.UserId 
  AND upp.Active = 1 
  AND upp.IsPrimary = 1 
  LEFT JOIN dbo.CampaignList ct ON upt.CampaignTypeId = ct.CampaignTypeId 
  WHERE 
  rt.Name != 'Admin' 
  )
 select * from CTEData
 WHERE 
   (
    (
      @FromDate IS NULL 
      AND @ToDate IS NULL
    ) 
    OR (
      RegDate BETWEEN @FromDate 
      AND @ToDate
    )
  ) 
  AND (
    @Year IS NULL 
    OR @year = Year(RegDate)
  ) 
  AND (
    @Month IS NULL 
    OR (
      @year = Year(RegDate) 
      AND @month = Month(RegDate)
    )
  ) 
  AND (
    @Quarter IS NULL 
    OR (
      1 = (
        CASE WHEN @Quarter = 'Q1' 
        AND Month(RegDate) BETWEEN 1 
        AND 3 THEN 1 WHEN @Quarter = 'Q2' 
        AND Month(RegDate) BETWEEN 4 
        AND 6 THEN 1 WHEN @Quarter = 'Q3' 
        AND Month(RegDate) BETWEEN 7 
        AND 9 THEN 1 WHEN @Quarter = 'Q4' 
        AND Month(RegDate) BETWEEN 10 
        AND 12 THEN 1 ELSE 0 END
      ) 
      AND @year = Year(RegDate)
    )
  ) 
  AND (
    @City IS NULL 
    OR (City LIKE '%' + @City + '%')
  ) 
  AND (
    @State IS NULL 
    OR (State LIKE '%' + @State + '%')
  ) 
  AND (
    @zip IS NULL 
    OR (Zip LIKE '%' + @Zip + '%')
  ) 
  AND (
    @Find IS NULL 
    OR (
      1 = (
        CASE WHEN @Find = '1' 
        AND BillingStatus = -1 THEN 1 WHEN @Find = '2' 
        AND SentDate IS NOT NULL THEN 1 WHEN @Find = '3' 
        AND SentDate IS NULL THEN 1 WHEN @Find = '4' 
        AND month(RegisteredDate) = MONTH(
          GetDate()
        ) THEN 1 WHEN @Find = '5' 
        AND LastLogin IS NOT NULL THEN 1 WHEN @Find = '6' 
        AND LastLogin IS NULL THEN 1 ELSE 0 END
      )
    )
  ) 
  AND (
    @MemberType IS NULL 
    OR @MemberType = PlanId
  ) 
  AND (
    @Status IS NULL 
    OR @Status = IsActive
  ) 
  AND (
    @CampaignType IS NULL 
    OR @CampaignType = CampaignTypeId
  ) 
  ORDER BY RegisteredDate DESC 


  END

GO
/****** Object:  StoredProcedure [dbo].[usp_getAdminTodays]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[usp_getAdminTodays]
CREATE PROCEDURE [dbo].[usp_getAdminTodays] 
as 
DECLARE
@AllClients int =0,
@NewClientCount int = 0, 
@NewClientBilling nvarchar(100), 
@MonthlyBillDue nvarchar(100), 
@MonthlyBillDeclined nvarchar(100), 
@PackagePurchase nvarchar(100), 
@PackageDeclined nvarchar(100) 
SET
@AllClients = (
 select 
      count(id) 
    from 
      UserProfile
)
SET 
  @NewClientCount = (
    select 
      count(id) 
    from 
      UserProfile 
    where 
      Convert(date, RegisteredDate) = CONVERT(
        date, 
        getdate()
      )
  ) 
SET 
  @NewClientBilling = (
    select 
      ISNULL(
        Convert(
          varchar, 
          count(distinct UserId)
        )+ 'client(s) for $' + convert(
          varchar, 
          sum(
            Convert(float, amount)
          )
        ), 
        '0 client(s) for $0'
      ) 
    from 
      AnTransactionLog 
    where 
      CONVERT(date, TransactionDate) = CONVERT(
        date, 
        getdate()
      ) 
      and CreatedBy = 'Register'
  ) 
SET 
  @MonthlyBillDue = (
    select 
      '0 client(s) for $0'
  ) 
SET 
  @MonthlyBillDeclined = (
    select 
      '0 client(s) for $0'
  ) 
SET 
  @PackagePurchase =(
    select 
      ISNULL(
        Convert(
          varchar, 
          count(distinct UserId)
        )+ 'client(s) for $' + convert(
          varchar, 
          sum(
            Convert(float, amount)
          )
        ), 
        '0 client(s) for $0'
      ) 
    from 
      AnTransactionLog 
    where 
      CONVERT(date, TransactionDate) = CONVERT(
        date, 
        getdate()
      ) 
      and CreatedBy = 'Upgrade'
  ) 
SET 
  @PackageDeclined = (
    select 
      '0 client(s) for $0'
  )

  select
  @AllClients as AllClients,
   @NewClientCount as TotalClient,
   @NewClientBilling as ClientBilling,
  @MonthlyBillDue as BillDue,
  @MonthlyBillDeclined as BillDeclined,
  @PackagePurchase as PacakagePurchase,
  @PackageDeclined as PackageDeclined
GO
/****** Object:  StoredProcedure [dbo].[usp_getAllSalesRep]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Chitra,[usp_getAllSalesRep]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Sales Rep with Filters>

-- Modified By:		Sunil Singh
-- Modified date: <15 Jan 2019>
-- Description:	Changes for temp table replaced with table variable

-- Modified Date: 15 Jan 2019
-- Changes For : Replaced temp table with table variable
-- =============================================
--[usp_getAllSalesRep]
--[usp_getAllSalesRep] 'pa'
--[usp_getAllSalesRep] 'p','t'
CREATE PROCEDURE [dbo].[usp_getAllSalesRep] 
	@FirstName nvarchar(200) = NULL,
	@LastName nvarchar(200)=NULL,
	@NoOfClients int=NULL,
	@SpentTotal float=NULL,
	@ViewRepActivity bit=NULL,
	@CanExport bit=NULL,
	@ExportLead bit=NULL,
	@Manager bit=NULL,
	@IsActive bit=NULL,
	@PageSize int = NULL,
	@StartIndex int = 0
AS
	BEGIN
	  SET FMTONLY OFF;
	  SET NOCOUNT ON

	  DECLARE @totalrecords int;
	  DECLARE @AllSalesRep TABLE(
		Id nvarchar(128),
		Email nvarchar(256),
		FirstName nvarchar(200),
		LastName nvarchar(200),
		NoOfClients int,
		SpentTotal float,
		ViewRepActivity bit,
		CanExport bit,
		ExportLead bit,
		Manager bit,
		IsActive bit,
		CreatedDate datetime,
		RecordNumber int IDENTITY (1, 1) PRIMARY KEY
	  );
  
	  WITH CTE_SaleRepDetails
	  AS 
		(
			SELECT
				 AspNetUsers.Email		
				,SRP.Id
				,SRP.FirstName
				,SRP.LastName
				,SRP.ContactNo
				,SRP.NoOfClients
				,SRP.SpentTotal
				,SRP.ViewRepActivity
				,SRP.CanExport
				,SRP.ExportLead
				,SRP.Manager
				,SRP.IsActive
				,SRP.CreatedDate
				,SRP.CreatedBy
				,SRP.ModifedDate
				,SRP.ModifiedBy
			FROM AspNetUsers
				INNER JOIN SalesRepProfile SRP
					ON AspNetUsers.id = SRP.id
				INNER JOIN AspNetUserRoles 
					ON AspNetUsers.id  = AspNetUserRoles.UserId
			WHERE AspNetUserRoles.RoleId = (select Id from AspNetRoles where Name = 'SalesRep')
		)
		INSERT INTO @AllSalesRep
		SELECT
			id,
			Email,
			FirstName,
			LastName,
			NoOfClients,
			SpentTotal,
			ViewRepActivity,
			CanExport,
			ExportLead,
			Manager,
			IsActive,
			CreatedDate
		FROM CTE_SaleRepDetails cte
		ORDER BY cte.CreatedDate DESC

		SELECT @totalrecords = COUNT(Id) FROM @AllSalesRep

		IF (@PageSize IS NULL)
		SET @PageSize = @totalrecords

		SELECT
			id
			,Email
			,FirstName
			,LastName
			,NoOfClients
			,SpentTotal
			,ViewRepActivity
			,CanExport
			,ExportLead
			,Manager
			,IsActive
			,CONVERT(varchar,CreatedDate,110)+ ' '+CONVERT(varchar, CAST(CreatedDate AS TIME),100) as CreatedDate
			,RecordNumber
			,@totalrecords AS totalrecords
		FROM @AllSalesRep
		WHERE RecordNumber >= (@PageSize * @StartIndex) + 1
			  AND RecordNumber <= (@PageSize * (@StartIndex + 1))
			  AND FirstName like '%'+ ISNULL(@FirstName,FirstName)+'%'
			  AND LastName like '%'+ISNULL(@LastName,LastName)+'%'
			  AND NoOfClients = ISNULL(@NoOfClients,NoOfClients)
			  AND SpentTotal = ISNULL(@SpentTotal,SpentTotal)
			  AND ViewRepActivity = ISNULL(@ViewRepActivity,ViewRepActivity)
			  AND CanExport = ISNULL(@CanExport,CanExport)
			  AND ExportLead = ISNULL(@ExportLead,ExportLead)
			  AND Manager = ISNULL(@Manager,Manager)
			  AND IsActive = ISNULL(@IsActive,IsActive)

END


GO
/****** Object:  StoredProcedure [dbo].[usp_getAllUssers]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Chitra,[usp_getAllUssers]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Users with Filters>

-- Modified By: SuniL Kumar Singh
-- Modified Date: 14 Jan 2019
-- Changes For : Code review and code rework

-- Modified Date: 15 Jan 2019
-- Changes For : Replaced temp table with table variable
-- =============================================
--[usp_getAllUssers] 'Timmghjhgjy'
--[usp_getAllUssers] NULL,'jack'
--[usp_getAllUssers] NULL,NULL,NULL,'york'
CREATE PROCEDURE [dbo].[usp_getAllUssers] 
	@FirstName nvarchar(200) = NULL,
	@LastName nvarchar(200)=NULL,
	@MobilePhone nvarchar(200)=NULL,
	@City nvarchar(200)=NULL,
	@State nvarchar(200)=NULL,
	@Country nvarchar(200)=NULL,
	@Zip nvarchar(50)=NULL,
	@isActive bit=NULL,
	@PageSize int = NULL,
	@StartIndex int = 0
AS
BEGIN
  SET FMTONLY OFF;
  SET NOCOUNT ON

  DECLARE @totalrecords int;
  DECLARE @AllUsers TABLE  (
		Id nvarchar(128)
		,Email nvarchar(256)
		,FirstName nvarchar(200)
		,LastName nvarchar(200)
		,ContactEmail nvarchar(200)
		,OfficePhone nvarchar(200)
		,MobilePhone nvarchar(200)
		,Address1 nvarchar(max)
		,Address2 nvarchar(max)
		,City varchar(200)
		,State nvarchar(200)
		,Country nvarchar(200)
		,Zip nvarchar(50)
		,ProfileImage nvarchar(max)
		,Company nvarchar(200)
		,isActive bit
		,CreatedDate datetime
		,RecordNumber int IDENTITY (1, 1) PRIMARY KEY
		,SalesRepId nvarchar(128)
  );
  WITH CTE_UserDetails
  AS 
	(
	SELECT
		AspNetUsers.Email
		,UP.Id
		,UP.FirstName
		,UP.LastName
		,UP.ContactEmail
		,UP.OfficePhone
		,UP.MobilePhone
		,UP.Address1
		,UP.Address2
		,UP.City
		,UP.State
		,UP.Country
		,UP.Zip
		,UP.ProfileImage
		,UP.CreditCardNo
		,UP.ExpiryDate
		,UP.RegisteredDate
		,UP.IsActive
		,UP.CompanyName
		,UP.SalesRepId
		FROM AspNetUsers
			INNER JOIN UserProfile UP
				ON AspNetUsers.id = UP.id
			INNER JOIN AspNetUserRoles on AspNetUsers.id  = AspNetUserRoles.UserId
		WHERE AspNetUserRoles.RoleId != (select Id from AspNetRoles where Name = 'Admin')
	)
  INSERT INTO @AllUsers
		SELECT
		  id,
		  Email,
		  FirstName,
		  LastName,
		  ContactEmail,
		  OfficePhone,
		  MobilePhone,
		  Address1,
		  Address2,
		  City,
		  State,
		  Country,
		  Zip,
		  ProfileImage,
		  CompanyName,
		  IsActive,
		  RegisteredDate,
		  SalesRepId
		FROM CTE_UserDetails cte
		ORDER BY cte.RegisteredDate DESC

	  SELECT @totalrecords = COUNT(Id) FROM @AllUsers

	  IF (@PageSize IS NULL)
		SET @PageSize = @totalrecords
	
		SELECT
			id
			,Email
			,FirstName
			,LastName
			,ContactEmail
			,OfficePhone
			,MobilePhone
			,Address1
			,Address2
			,City
			,State
			,Country
			,Zip
			,ProfileImage
			,Company
			,isActive
			,CreatedDate
			,RecordNumber
			,@totalrecords AS totalrecords
			,SalesRepId
		  FROM @AllUsers
			  WHERE RecordNumber >= (@PageSize * @StartIndex) + 1
				  AND RecordNumber <= (@PageSize * (@StartIndex + 1))
				  AND 1 = (
				  case when @FirstName is NULL then 1 
				  when @FirstName is not null and FirstName like '%'+ isnull(@FirstName,FirstName)+ '%' then 1
				  else 0
				  end
				  )
				  AND  1= (
				  case when @LastName is NULL then 1
				  when @LAstName is not NULL and LastName like '%'+ isnull(@LastName,LastName)+ '%' then 1
				  else 0
				  end )
				  AND 1 = (
				  case when @MobilePhone is NULL then 1 
				  when @MobilePhone is not null and  MobilePhone like '%'+ isnull(@MobilePhone,MobilePhone)+ '%' then 1
				  else 0
				  end)
				  AND 1 = (
				  case when @City is NULL then 1 
				  when @City is not null and City like '%'+isnull(@City,City)+ '%' then 1
				  else 0
				  end)
				  AND 1 = (
				  case when @State is NULL then 1 
				  when @State is not NULL and [State] like '%'+ isnull(@State,[State]) + '%' then 1
				  else 0
				  end )
				  AND 1 = (
				  case when @Country is NULL then 1 
				  when @Country is not NULL and Country like '%'+isnull(@Country,Country)+ '%' then 1
				  else 0
				  end)
				  and 1 = (
				  case when @zip is NULL then 1
				  when @zip is not null and Zip like '%'+ isnull(@Zip,Zip)+ '%' then 1
				  else 0
				  end)
				  And isActive = isnull(@isActive,isActive)

END
GO
/****** Object:  StoredProcedure [dbo].[usp_getAuthorization]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [usp_getAuthorization] '3d4d1cd1-2695-4756-a2f6-3cf1fcd3765f'
CREATE PROCEDURE [dbo].[usp_getAuthorization] @UserId nvarchar(128) AS BEGIN -- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET 
  FMTONLY OFF;
SET 
  NOCOUNT ON;
CREATE TABLE #USERPLANTEMP
(
  UserId NVARCHAR(128), 
  PackageId int, 
  MonthlyCredit int, 
  TotalSpendCredits int, 
  TotalRemainingCredits int, 
  MaxSubscriber int, 
  MinSubscriber int, 
  CustomizableTemplate bit, 
  TotalEmail int, 
  TotalDays int, 
  TotalSubscriberCreated int,
  EmailScheduling bit
) Declare @TotalSubsCreated int;
Select 
  @TotalSubsCreated = COUNT(*) 
from 
  dbo.SubscriberList SL 
where 
  SL.CreatedById = @UserId INSERT INTO #USERPLANTEMP 
  SELECT UP.UserId,
  UP.PackageId,
  UP.MonthlyCredit,
  UP.totalSpendCredits,
  UP.totalRemainingCredits,
  up.NoOfSubscribersMax,
  up.NoOfSubscribersMin,
  UP.CustomizableTemplates,
  UP.TotalEmails,
  UP.TotalDays,
  @TotalSubsCreated,
  UP.EmailScheduling
  FROM dbo.UserPlan UP 
  WHERE UP.UserId=@UserId
  AND UP.IsActive=1
SELECT 
  * 
FROM 
  #USERPLANTEMP
DROP 
  TABLE #USERPLANTEMP
  END

GO
/****** Object:  StoredProcedure [dbo].[usp_getBeeTemplateList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ALOK,usp_getTemplateList>
-- Create date: <1/15/2019 1:10 PM>
-- Description:	<TO GET THE INFO OF PLAN INFO OF USER>
-- =============================================
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436','',1,10,'NameAsc'
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436','First'
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436',NULL,2,10
CREATE Procedure [dbo].[usp_getBeeTemplateList] 
@Search nvarchar(100) = NULL, 
@PageNumber INT = 1, 
@PageSize INT = NULL,
@OrderBy NVARCHAR(100)='TemplateId'   
as 
--SET FMTONLY OFF
Begin 
	DECLARE 
	@FirstRec     INT, 
     @LastRec      INT, 
     @TotalRecords INT 
         Select (ROW_NUMBER() OVER (ORDER BY 
					case when @OrderBy = 'NameAsc' Then btp.BeeTemplateName end,
                    case when @OrderBy = 'NameDesc' Then btp.BeeTemplateName End desc,
                    case when @OrderBy = 'DateAsc' Then btp.CreatedDate End, 
                    case when @OrderBy = 'DateDesc' Then btp.CreatedDate End desc,
                     case when @OrderBy = 'TemplateId' or @OrderBy is NULL  Then btp.BeeTemplateId End desc 
	                )) as Row, 
			 btp.BeeTemplateId,btp.BeeTemplateJson,btp.BeeTemplateName,btp.BeeTemplatePath,btp.IsDeleted,btp.BeeTemplateHtml into #BeeTemplateTemp
			from 
			  dbo.BeeTemplates as btp
			WHERE 
				btp.IsDeleted = 0
				AND(
					BeeTemplateName like '%'+ ISNULL(@Search,BeeTemplateName)+'%'
					OR CreatedDate like '%'+ ISNULL(@Search,CreatedDate)+'%'
					OR LastModDate like '%'+ ISNULL(@Search,LastModDate)+'%'
					OR Description like '%'+ ISNULL(@Search,Description)+'%'
					OR BeeTemplatePath like '%'+ ISNULL(@Search,BeeTemplatePath)+'%'
				) 
				ORDER BY btp.BeeTemplateId
		SELECT @TotalRecords = Count(*) 
FROM   #BeeTemplateTemp 

IF ( @PageSize IS NULL ) 
    SET @PageSize = @TotalRecords 

SELECT @FirstRec = ( @PageNumber - 1 ) * @PageSize 

SELECT @LastRec = ( @PageNumber * @PageSize ) -- Create a temporary table 
SELECT *, 
        @TotalRecords AS TotalRecord, 
        @PageNumber   AS PageNumber, 
        @PageSize     AS PageSize 
FROM   #BeeTemplateTemp 
WHERE  row > @FirstRec 
        AND row <= @LastRec 

DROP TABLE #BeeTemplateTemp 
	
End


GO
/****** Object:  StoredProcedure [dbo].[usp_getCampaginsSubscriber]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Chitra,[usp_getCampaginsSubscriber]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Users with Filters>

-- Modified By: SuniL Kumar Singh
-- Modified Date: 14 Jan 2019
-- Changes For : Indent and added no count on statement
-- =============================================

CREATE PROCEDURE [dbo].[usp_getCampaginsSubscriber] 
   @CampaignId int
AS
  SET NOCOUNT ON;

	SELECT 
		 ct.CampaignId
		,ct.UserId
		,ct.ListId
		,ct.CampaignTypeID
		,ct.KeywordId
		,ct.Description
		,ct.AutomaticResponse
		,ct.ForwardedCellNumber
		,ct.ForwardedEmail
		,ct.CreatedById
		,ct.CreatedDate
		,ct.LastModById
		,ct.LastModDate
		,ct.HtmlContent
		,ct.Subject
		,ct.FromName
		,ct.FromEmail
		,ct.PreviewText
		,ct.TemplateId
		,ct.CamapignName
		,st.Email
		,st.FirstName
		,st.LastName
		,st.PhoneNumber
	FROM Campaign ct
		INNER JOIN SubscriberList slt 
					ON ct.ListId = slt.ListId
		LEFT JOIN Subscriber st
					 ON slt.SubscriberId = st.SubscriberId
		WHERE ct.CampaignId=@CampaignId and st.SubscriberId in (select value from dbo.fnSplitBigInt(ct.SubscriberIds))
GO
/****** Object:  StoredProcedure [dbo].[usp_getCampaignList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 --[dbo].[usp_getCampaignList] '1e99575f-c92c-4842-9cd3-684e26182436'
 --[dbo].[usp_getCampaignList] '1e99575f-c92c-4842-9cd3-684e26182436','New Campaign Sharing'
CREATE Procedure [dbo].[usp_getCampaignList] 
@UserId nvarchar(100), 
@Search nvarchar(100) = NULL, 
@PageNumber INT = 1, 
@PageSize INT = NULL,
@OrderBy NVARCHAR(100)='CampaignId'
as 
Begin 
--SET FMTONLY OFF
      DECLARE
	  @FirstRec     INT, 
     @LastRec      INT, 
     @TotalRecords INT ,
	  @UserName nvarchar(100)
        Select @UserName = FirstName +' '+ LastName from dbo.UserProfile Where Id=@UserId
       
						Select (ROW_NUMBER() OVER (ORDER BY 
					case when @OrderBy = 'NameAsc' Then cp.CamapignName end,
                    case when @OrderBy = 'NameDesc' Then cp.CamapignName End desc,
                    case when @OrderBy = 'DateAsc' Then cp.CreatedDate End, 
                    case when @OrderBy = 'DateDesc' Then cp.CreatedDate End desc,
                     case when @OrderBy = 'CampaignId' or @OrderBy is NULL  Then cp.CampaignId End desc 
	                )) as Row, 
                          cp.CampaignId,cp.CamapignName,cp.CreatedById,cp.CreatedDate,cp.LastModById,cp.LastModDate,
                          cp.Status,l.ListName , cp.UserId ,@UserName as EditedBy
                          into #CampaignTemp
                        from 
                          dbo.Campaign as cp
                          Left Join dbo.List  as l on cp.ListId = l.ListId
                        WHERE 
                                cp.UserId = @UserId
                                AND(
                                        CamapignName like '%'+ ISNULL(@Search,CamapignName)+'%'
                                        OR cp.CreatedDate like '%'+ ISNULL(@Search,cp.CreatedDate)+'%'
										OR cp.LastModDate like '%' + ISNULL (@Search , cp.LastModDate)+ '%'
                                        OR ListName like '%'+ ISNULL(@Search,ListName)+'%'
                                ) 
                                ORDER BY cp.CampaignId
               SELECT @TotalRecords = Count(*) 
FROM   #CampaignTemp 

IF ( @PageSize IS NULL ) 
    SET @PageSize = @TotalRecords 

SELECT @FirstRec = ( @PageNumber - 1 ) * @PageSize 

SELECT @LastRec = ( @PageNumber * @PageSize ) -- Create a temporary table 
SELECT *, 
        @TotalRecords AS TotalRecord, 
        @PageNumber   AS PageNumber, 
        @PageSize     AS PageSize 
FROM   #CampaignTemp 
WHERE  row > @FirstRec 
        AND row <= @LastRec 

DROP TABLE #CampaignTemp 
	
End


GO
/****** Object:  StoredProcedure [dbo].[usp_getInvoice]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_getInvoice '1e99575f-c92c-4842-9cd3-684e26182436'
CREATE procedure [dbo].[usp_getInvoice] 
@UserId nvarchar(150)
as
Begin 
select 
  invt.*, 
  upt.id, 
  upt.FirstName, 
  upt.LastName, 
  upt.CompanyName 
from 
  Invoice invt 
  inner join AnTransactionLog transt on invt.AnTransactionId = transt.AnTransactionId 
  inner join UserProfile upt on upt.id = transt.UserId
  where upt.id=@UserId
  order by invt.InvoiceID desc
  End

GO
/****** Object:  StoredProcedure [dbo].[usp_getInvoiceDetails]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_getInvoiceDetails 1
CREATE procedure [dbo].[usp_getInvoiceDetails]
@InvoiceID nvarchar(150)
as
Begin 
select 
  invt.*, 
  upt.id,
  upt.CompanyName, 
  ppt.FirstName,
  ppt.Address1,
  ppt.Email,
  CONVERT(varchar,convert(Date,transt.TransactionDate)) as TransactionDate

  
from 
  Invoice invt 
  inner join AnTransactionLog transt on invt.AnTransactionId = transt.AnTransactionId 
  inner join UserProfile upt on upt.id = transt.UserId
  inner join UserPaymentProfile ppt on transt.PaymentProfileId = ppt.AnPaymentProfileId
  where invt.InvoiceID=@InvoiceID
  End

GO
/****** Object:  StoredProcedure [dbo].[USP_GetLeadDashBoard]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GetLeadDashBoard null,null,null,null,1,1
CREATE PROC [dbo].[USP_GetLeadDashBoard]
(
	@SaleRepID NVARCHAR(128)=null,
	@LeadQualityID int =null,
	@TypeOfLead int=null,
	@IndustryId int = null,
	@pageIndex int=null,
	@pageSize int =null
)
AS
BEGIN
Set @pageIndex = isnull(@pageIndex,1);
Set @pageSize =  isnull(@pageSize,9999999);

DECLARE @FirstRec int, @LastRec int

SELECT @FirstRec = (@pageIndex - 1) * @pageSize
SELECT @LastRec = (@pageIndex * @pageSize )

Select Top(@pageSize) T.* 
from (

SELECT ROW_NUMBER()Over(order by srp.FirstName) AS RowId ,
ISNULL(srp.FirstName, N'') + ' ' + ISNULL(srp.LastName, N'') AS SaleRepName ,
       CONVERT(VARCHAR, spl.FollowUpDate,110) FollowUpDate ,
        spl.LeadId ,
        spl.JobTitle ,
        spl.Company ,
        spl.IndustryId ,
        I.IndustryName ,
        spl.MobileNo ,
        spl.PhoneNo ,
        spl.Email ,
        spl.Website ,
        spl.LinkedIn ,
        spl.Twitter ,
        spl.Instagram ,
        spl.Facebook ,
        spl.ProviderId ,
        dbo.Func_GetProviderName( spl.ProviderId ) ProviderName ,
        spl.HotId ,
        splq.LeadQualityName AS HotName ,
        spl.OpportunityAmount ,
        spl.State ,
        spl.FollowupTime ,
        spl.SalesRepID ,
        spl.CreatedOn AS DateAdeed ,
        DATEDIFF(dd, spl.CreatedOn, GETDATE()) AS days
FROM    SalesPortal_Leads AS spl
        INNER JOIN SalesRepProfile AS srp ON spl.SalesRepID = srp.Id
        LEFT OUTER JOIN Industry AS I ON I.IndustryID = spl.IndustryId
        LEFT OUTER JOIN SalesPortal_LeadQuality AS splq ON spl.HotId = splq.LeadQualityID
Where  (@SaleRepID is null Or spl.SalesRepID = @SaleRepID)
and  (@LeadQualityID is null Or spl.HotId = @LeadQualityID) 
and  (@IndustryId is null Or spl.IndustryID = @IndustryId) 
and (@TypeOfLead is null Or @TypeOfLead=( Case When DATEDIFF(dd, spl.CreatedOn, GETDATE()) < 30 then 1 
When  DATEDIFF(dd, spl.CreatedOn, GETDATE()) >= 30  then 2
When DATEDIFF(dd, spl.CreatedOn, GETDATE()) < 90 then 3
When DATEDIFF(dd, spl.CreatedOn, GETDATE()) >=90 then 4 End ))
)T
Where RowId  between @FirstRec and @LastRec

		
End
GO
/****** Object:  StoredProcedure [dbo].[usp_getManageBillingDetail]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--usp_getManageBillingDetail '1e99575f-c92c-4842-9cd3-684e26182436'
--usp_getManageBillingDetail '0a3c195f-b7d8-4a66-bd6a-0bdac7edf0d3'
CREATE Procedure [dbo].[usp_getManageBillingDetail]
@UserId nvarchar(150)
as
Declare
@AccountType varchar(200),
@LastBilling DateTime
select 
ut.Id,
ut.Email,
  upt.FirstName + ' ' + upt.LastName as AccountName, 
  pt.totalSpendCredits, 
  upt.IsActive, 
  Convert(varchar,Convert(Date,upt.RegisteredDate)) as DateRegistered,
  Convert(varchar,Convert(Date,upt.LastLogin)) as LastLogin,
   (
    select 
      count(*) 
    from 
      Campaign ct 
    where 
      convert(date, ct.SentDate)= convert(
        date, 
        getdate()
      ) 
      and ct.UserId = ut.id
  ) as SentToday, 
    (
    select 
      count(*) 
    from 
      Campaign ct 
    where 
   ct.UserId = ut.id
  ) as SentTotal, 
  (
    select 
      COUNT(*) 
    from 
      Subscriber st 
    where 
      st.CreatedById = ut.id 
      and IsDeleted = 0
  ) as Contacts, 
  pt.MonthlyCredit, 
  pt.BillingAmount, 
  Convert(varchar,Convert(Date,pt.NextPackageUpdateDate)) as NextBillingDate, 
  pt.BillingStatus,
  0 as PayGCredits,
  0 as NumKeywords,
  0 as Keywords,
  0 as SMPPReload,
 (select top 1 td.TransactionDate from AnTransactionLog td where 
 td.UserId = ut.id  order by td.TransactionDate desc) as LastBilling,
  0 as OneTimeOwned,
  0 as AmountOwned,
  (select pge.BillingAmount from [Package] pge inner join [Plan] p on pge.PlanId = p.PlanId  
		inner join UserPlan upt on pge.Id = upt.PackageId

		where upt.UserId = @UserId and upt.IsActive=1) as PayGPrice ,
  0 as LastUploadAmount,
  (select PlanName from [Package] pge inner join [Plan] p on pge.PlanId = p.PlanId  
		inner join UserPlan upt on pge.Id = upt.PackageId

		where upt.UserId = @UserId and upt.IsActive=1
		  

  )as AccountType,
  0 as BillType,
  0 as LastUpload
from 
  AspNetUsers ut 
  inner join UserProfile upt on ut.id = upt.id 
  inner join userplan pt on ut.id = pt.UserId 
  and pt.IsActive = 1 
where ut.Id = @UserId


GO
/****** Object:  StoredProcedure [dbo].[usp_getNextBilling]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 CREATE procedure [dbo].[usp_getNextBilling] 
 as 
select 
  upt.userid
  ,uppt.AnPaymentProfileId
  ,uprt.AuthProfileId
  ,pt.BillingAmount
  ,upt.NextPackageUpdateDate
  ,pt.PlanId
  ,plan_t.PlanName
  ,upt.PackageId
  ,upt.UserPlanId
from 
  UserPlan upt 
  left join UserPaymentProfile uppt on upt.UserId = uppt.UserId  and uppt.IsPrimary=1
  inner join userprofile uprt on uprt.Id = uppt.UserId 
  inner join Package pt on upt.PackageId = pt.Id 
  inner join [Plan] plan_t on pt.PlanId = plan_t.PlanId 
 where 
 --upt.userid = '189c71ea-bc6a-44a2-8a19-bc40665993b4'
 --and 
 upt.IsActive = 1
 AND
  CONVERT(
    varchar, 
    getdate(), 
    105
  ) = CONVERT(
    varchar, upt.NextPackageUpdateDate, 
    105
  ) 
  and DATEPART(
    HOUR, 
    GETDATE()
  ) = DATEPART(HOUR, upt.NextPackageUpdateDate) 
  and DATEPART(
    MINUTE, 
    GETDATE()
  ) = DATEPART(
    MINUTE, upt.NextPackageUpdateDate
  )



GO
/****** Object:  StoredProcedure [dbo].[usp_getPackageDetail]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- usp_getPackageDetail 2
CREATE procedure [dbo].[usp_getPackageDetail]
@PackId int
as
select
[Plan].*,
[Package].Id,
[Package].BillingAmount,
[Package].TotalMonth

 from [Plan] inner join Package
on [Plan].PlanId = Package.PlanId
where Package.id=@PackId
GO
/****** Object:  StoredProcedure [dbo].[usp_getSalesRepActivity]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Spusp_getSalesRepActivity  Null,'2019-03-25','2019-03-25'
-- USP_Salesportal_AdminRepActivity  '4ae051d4-2166-42ad-91f5-9510248effa8'
CREATE Proc [dbo].[usp_getSalesRepActivity]
(
@RepId nvarchar(128)=null,
@DateFrom Date=null,
@DateTo Date = null
)
as
begin
if(@DateFrom is null)
begin
set @DateFrom = '1900-01-01'
End
if(@DateTo is null)
begin
set @DateTo = Convert(date, GetDate())
End
SELECT 
	   ISNULL(upt.FirstName,'')+ISNULL(upt.LastName,'') as ClientName,
       RepAct.RepActivityID ,
       RepAct.Username ,
       RepAct.UserId ,
       RepAct.RepId ,
	   srp.FirstName,
	   srp.LastName,
       RepAct.Category ,
       RepAct.Description ,
     Convert(Varchar(50),  RepAct.CreatedDate,100)  CreatedDate,
       RepAct.CreatedBy ,
       RepAct.NoteId ,
	   sprn.NoteDescription,
       RepAct.IsComment 
	   FROM  Salesportal_AdminRepActivity RepAct
	   INNER JOIN dbo.SalesRepProfile AS srp ON srp.ID=RepAct.RepId 
	   Left  JOIN dbo.SalesPortalRepNote AS sprn ON sprn.NoteId= RepAct.NoteId
	   Left Join dbo.UserProfile upt on RepAct.UserId=upt.Id
	   Where Convert(date, RepAct.CreatedDate) between @DateFrom and @DateTo 
	   and    RepAct.RepId = isnull(@RepId,RepAct.RepId )
End
GO
/****** Object:  StoredProcedure [dbo].[usp_getSalesRepDashboardData]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[usp_getSalesRepDashboardData] '4ae051d4-2166-42ad-91f5-9510248effa8'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8', 'Marlwq'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,'vking66'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,NULL,'Vic'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,'(353) 648-5796'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,NULL,NULL,'PlandE'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,NULL,NULL,NULL,'Georgia'
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,NULL,NULL,NULL,EEED,NULL
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2
--[usp_getSalesRepData] '4ae051d4-2166-42ad-91f5-9510248effa8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6ac8e4b9-542b-480b-9da6-11318763cf63'
CREATE PROCEDURE [dbo].[usp_getSalesRepDashboardData]
    @id NVARCHAR(128) NULL,
    @Name NVARCHAR(200) = NULL ,
    @Email NVARCHAR(256) = NULL ,
    @MobilePhone NVARCHAR(200) = NULL ,
    @CardName NVARCHAR(256) = NULL ,
    @PlanName NVARCHAR(200) = NULL ,
    @City NVARCHAR(200) = NULL ,
    @State NVARCHAR(200) = NULL ,
    @ClientType VARCHAR(50) = NULL ,
    @CampaignTypeId INT = NULL ,
    @userId NVARCHAR(128) = NULL
AS
    SELECT 
            ( SELECT    FirstName + ' ' + ISNULL(LastName, '')
              FROM      SalesRepProfile
              WHERE     Id = @id
            ) AS RepName ,
            ( SELECT    CanExport
              FROM      SalesRepProfile
              WHERE     Id = @id
            ) AS CanExport ,
            ut.Id ,
            ut.Email ,
            ISNULL(upt.FirstName, '') + ' ' + ISNULL(upt.LastName, '') AS AccountName ,
            rt.Name ,
            ISNULL(pt.totalSpendCredits, 0) AS totalSpendCredits ,
            upt.IsActive ,
            CONVERT(VARCHAR, CONVERT(DATE, upt.RegisteredDate)) AS RegisteredDate ,
            ISNULL(pt.TotalDays - DATEDIFF(DAY, pt.PackageUpdateDate,
                                           GETDATE()), 0) AS Days ,
            ISNULL(( SELECT COUNT(*)
                     FROM   Campaign ct
                     WHERE  CONVERT(DATE, ct.SentDate) = CONVERT(DATE, GETDATE())
                            AND ct.UserId = ut.Id
                   ), 0) AS SentToday ,
            ( SELECT    COUNT(*)
              FROM      Subscriber st
              WHERE     st.CreatedById = ut.Id
                        AND IsDeleted = 0
            ) AS Contacts ,
            CONVERT(VARCHAR, CONVERT(DATE, upt.DOB)) AS DOB ,
            pt.MonthlyCredit ,
            pt.BillingAmount ,
            CONVERT(VARCHAR, CONVERT(DATE, pt.NextPackageUpdateDate)) AS NextPackageUpdateDate ,
            pt.BillingStatus ,
            plt.PlanName ,
            upt.ClientType ,
            upt.PromotionCode AS 'ReferralCode' ,
            1 AS 'CSV Upload' ,
            ISNULL(pt.CustomizableTemplates, 0) AS CustomizableTemplates ,
            ( CASE pat.TotalMonth
                WHEN 1 THEN 'Monthly'
                WHEN 3 THEN 'Quartly'
                WHEN 6 THEN '6  Month'
                ELSE 'Annualy'
              END ) AS 'MemberType' ,
			    Convert(varchar,Convert(date,upt.FollowUpDate),110) as FollowUpDate ,
  CONVERT(VARCHAR(15),CAST(upt.FollowUpDate AS TIME),100) as FollowUpTime,
  zt.TimeZoneName as TimeZone,
    --        ( SELECT TOP 1
    --                    CONVERT(VARCHAR, FollowUpDate, 110)
    --          FROM      SalesPortalRepNote
    --          WHERE     CreatedById = @id
    --                    AND UserId = ut.Id
    --                    AND IsDelete = 0
    --          ORDER BY  LastModDate ,
    --                    CreatedDate DESC
    --        ) AS 'FollowUpDate' ,
    --        ( SELECT TOP 1
    --                    CONVERT(VARCHAR(15), CAST(FollowUpDate AS TIME), 100)
    --          FROM      SalesPortalRepNote
    --          WHERE     CreatedById = @id
    --                    AND UserId = ut.Id
    --                    AND IsDelete = 0
    --          ORDER BY  LastModDate ,
    --                    CreatedDate DESC
    --        ) AS 'FollowUpTime' ,
			 --( SELECT TOP 1
    --                   TimeZone
    --          FROM      SalesPortalRepNote 
    --          WHERE     CreatedById = @id
    --                    AND UserId = ut.Id
    --                    AND IsDelete = 0
    --          ORDER BY  LastModDate ,
    --                    CreatedDate DESC
    --        ) AS 'TimeZone',
            rep.FirstName + ' ' + rep.LastName AS SalesRepName ,
            uppt.FirstName AS 'NameOnCard' ,
            ct.CampaignName
    FROM    AspNetUsers ut
            INNER JOIN UserProfile upt ON ut.Id = upt.Id
            INNER JOIN UserPlan pt ON ut.Id = pt.UserId
                                      AND pt.IsActive = 1
            LEFT JOIN UserPaymentProfile uppt ON ut.Id = uppt.UserId
                                                 AND uppt.Active = 1
                                                 AND uppt.IsPrimary = 1
            LEFT JOIN CampaignType ct ON ct.CampaignTypeID = upt.CampaignTypeId
            INNER JOIN Package pat ON pt.PackageId = pat.Id
            INNER JOIN [Plan] plt ON pat.PlanId = plt.PlanId
            INNER JOIN AspNetUserRoles urt ON ut.Id = urt.UserId
            INNER JOIN AspNetRoles rt ON rt.Id = urt.RoleId
            INNER JOIN SalesRepProfile rep ON rep.Id = upt.SalesRepId
			  LEFT JOIN TimeZone zt on upt.TimeZoneId = zt.TimeZoneId
    WHERE   rt.Name != 'Admin'
            AND upt.SalesRepId = @id
            AND ( @Name IS NULL
                  OR upt.FirstName LIKE '%' + @Name + '%'
                )
            AND ( @Email IS NULL
                  OR ut.Email LIKE '%' + @Email + '%'
                )
            AND ( @MobilePhone IS NULL
                  OR upt.MobilePhone LIKE '%' + @MobilePhone + '%'
                )
            AND ( @CardName IS NULL
                  OR uppt.FirstName LIKE '%' + @CardName + '%'
                )
            AND ( @PlanName IS NULL
                  OR plt.PlanName LIKE '%' + @PlanName + '%'
                )
            AND ( @City IS NULL
                  OR upt.City LIKE '%' + @City + '%'
                )
            AND ( @State IS NULL
                  OR upt.State LIKE '%' + @State + '%'
                )
            AND ( @ClientType IS NULL
                  OR upt.ClientType LIKE '%' + @ClientType + '%'
                )
            AND ( @CampaignTypeId IS NULL
                  OR upt.CampaignTypeId = @CampaignTypeId
                )
            AND ( @userId IS NULL
                  OR upt.Id LIKE '%' + @userId + '%'
                )
  --AND 1 = (
  --  CASE WHEN @Name IS NULL THEN 1 WHEN @Name IS NOT NULL 
  --  AND upt.FirstName LIKE '%' + isnull(@Name, upt.FirstName)+ '%' THEN 1 ELSE 0 END
  --) 
  --AND 1 = (
  --  CASE WHEN @Email IS NULL THEN 1 WHEN @Email IS NOT NULL 
  --  AND ut.Email LIKE '%' + isnull(@Email, ut.Email)+ '%' THEN 1 ELSE 0 END
  --) 
 
  -- AND 1 = (
  --  CASE WHEN @MobilePhone IS NULL THEN 1 WHEN @MobilePhone IS NOT NULL 
  --  AND upt.MobilePhone LIKE '%' + isnull(@MobilePhone, upt.MobilePhone)+ ')%' THEN 1 ELSE 0 END
  --)
  --AND 1 = (
  --  CASE WHEN @CardName IS NULL THEN 1 WHEN @CardName IS NOT NULL 
  --  AND uppt.FirstName LIKE '%' + isnull(@CardName, uppt.FirstName)+ '%' THEN 1 ELSE 0 END
  --) 
  --AND 1 = (
  --  CASE WHEN @PlanName IS NULL THEN 1 WHEN @PlanName IS NOT NULL 
  --  AND plt.PlanName LIKE '%' + isnull(@PlanName, plt.PlanName)+ '%' THEN 1 ELSE 0 END
  --)
  --AND 1 = (
  --  CASE WHEN @City IS NULL THEN 1 WHEN @City IS NOT NULL 
  --  AND upt.City LIKE '%' + isnull(@City, upt.City)+ '%' THEN 1 ELSE 0 END
  --)
  --AND 1 = (
  --  CASE WHEN @State IS NULL THEN 1 WHEN @State IS NOT NULL 
  --  AND upt.State LIKE '%' + isnull(@State, upt.State)+ '%' THEN 1 ELSE 0 END
  --)
  --AND 1 = (
  --  CASE WHEN @ClientType IS NULL THEN 1 WHEN @ClientType IS NOT NULL 
  --  AND upt.ClientType LIKE '%' + isnull(@ClientType, upt.ClientType)+ '%' THEN 1 ELSE 0 END
  --)
  --AND 1 = (
  --  CASE WHEN @CampaignTypeId IS NULL THEN 1 WHEN @CampaignTypeId IS NOT NULL 
  --  AND upt.CampaignTypeId = @CampaignTypeId THEN 1 ELSE 0 END
  --)
  -- AND 1 = (
  --  CASE WHEN @userId IS NULL THEN 1 WHEN @userId IS NOT NULL 
  --  AND upt.Id = @userId THEN 1 ELSE 0 END
  --)
ORDER BY    upt.RegisteredDate DESC;

GO
/****** Object:  StoredProcedure [dbo].[usp_getSalesRepData]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  --usp_getSalesRepData '7139fcc0-c7f2-4463-9df4-a224dfce60e3'
  --usp_getSalesRepData NULL,'2019-01-01','2019-01-30'
  --usp_getSalesRepData null,null,2019
  --usp_getSalesRepData null,null,2018,'Q4'
  --usp_getSalesRepData null,null,2019,null,1
  --usp_getSalesRepData null,null,null,null,null,null,null,null,null,null,4
  --usp_getSalesRepData null,null,null,null,null,'Grafton'
  --usp_getSalesRepData null,null,null,null,null,null,'Massachusetts'
  --usp_getSalesRepData null,null,null,null,null,null,NULL,'92602'
  --usp_getSalesRepData null,null,null,null,null,null,NULL,NULL,NULL,NULL,NULL,0
  --usp_getSalesRepData null,null,null,null,null,null,NULL,NULL,NULL,NULL,NULL,NULL,2
  --usp_getSalesRepData null,null,null,null,null,null,NULL,NULL,NULL,2
  CREATE PROCEDURE [dbo].[usp_getSalesRepData] 
  @Id nvarchar(128)= NULL, 
  @FromDate date = NULL, 
  @ToDate date = NULL, 
  @Year int = NULL, 
  @Quarter nvarchar(250) = NULL, 
  @Month int = NULL, 
  @City nvarchar(100) = NULL, 
  @State nvarchar(100) = NULL, 
  @Zip nvarchar(50) = NULL, 
  @TimeZone nvarchar(20)= NULL, 
  @Find nvarchar(250) = NULL, 
  @MemberType int = NULL, 
  @Status bit = NULL, 
  @CampaignType int = NULL 
  AS 
  BEGIN 
  --SET FMTONLY OFF
  --CREATE TABLE #AdminTempData (
  --  Id nvarchar(128) NULL,
  --  Email nvarchar(100) NULL,
  --  AccountName nvarchar(100) NULL,
  --  Name nvarchar(100) NULL,
  --  totalSpendCredits nvarchar(100) NULL,
  --  IsActive bit NULL,
  --  RegisteredDate nvarchar(100) NULL,
  --  SentToday int NULL,
  --  Contacts int NULL,
  --  MonthlyCredit nvarchar(100) NULL,
  --  BillingAmount int NULL,
  --  NextPackageUpdateDate nvarchar(100) NULL,
  --  BillingStatus int NULL,
  --  CampaignType nvarchar(100) NULL,
  --  FollowUpDate nvarchar(100) NULL,
  --  ReferralCode nvarchar(200) NULL,
  --  MemberType nvarchar(100) NULL,
  --  ChargedDate nvarchar(100) NULL,
  --  City nvarchar(50) NULL,
  --  State nvarchar(50) NULL,
  --  FollowUpDateTime datetime NULL
  --)
  --INSERT INTO #AdminTempData
  WITH
  CTEData
  as(
SELECT 
  ut.Id, 
  ut.Email, 
  ISNULL(upt.FirstName, '') + ' ' + ISNULL(upt.LastName, '') AS AccountName, 
  ISNULL(pt.totalSpendCredits,0) as totalSpendCredits,
  upt.IsActive, 
  CONVERT(
    varchar, 
    CONVERT(varchar, upt.RegisteredDate, 110)
  ) AS RegisteredDate, 
  (
    SELECT 
      COUNT(*) 
    FROM 
      Campaign ct 
    WHERE 
      CONVERT(date, ct.SentDate) = CONVERT(
        date, 
        GETDATE()
      ) 
      AND ct.UserId = ut.id
  ) AS SentToday, 
  (
    SELECT 
      COUNT(*) 
    FROM 
      Subscriber st 
    WHERE 
      st.CreatedById = ut.id 
      AND IsDeleted = 0
  ) AS Contacts, 
  pt.MonthlyCredit, 
  ISNULL(pt.BillingAmount,0) as BillingAmount,
  CONVERT(
    varchar, 
    CONVERT(
      varchar, pt.NextPackageUpdateDate, 
      110
    )
  ) AS NextPackageUpdateDate, 
  pt.BillingStatus, 
  ct.CampaignName AS CampaignType, 
  (
    SELECT 
      TOP 1 CONVERT(varchar, FollowUpDate, 110) 
    FROM 
      SalesPortalRepNote 
    WHERE 
      UserId = ut.id 
      AND IsDelete = 0 
    ORDER BY 
      LastModDate DESC, 
      CreatedDate DESC
  ) AS 'FollowUpDate', 
  upt.PromotionCode AS 'ReferralCode', 
  (
    SELECT 
      planname 
    FROM 
      [package] pge 
      INNER JOIN [plan] p ON pge.planid = p.planid 
      INNER JOIN userplan upt ON pge.id = upt.packageid 
    WHERE 
      upt.userid = ut.id 
      AND upt.isactive = 1
  ) AS 'MemberType', 
  (
    CONVERT(
      varchar, pt.PackageUpdateDate, 110
    )
  ) AS ChargedDate, 
  upt.City, 
  upt.State, 
  (
    SELECT 
      TOP 1 CONVERT(datetime, FollowUpDate, 110) 
    FROM 
      SalesPortalRepNote 
    WHERE 
      UserId = ut.id 
      AND IsDelete = 0 
    ORDER BY 
      LastModDate DESC, 
      CreatedDate DESC
  ) AS 'FollowUpDateTime' ,
  upt.CampaignTypeId,
  upt.LastLogin,
  pat.PlanId,
  upt.Zip,
  (select top 1 SentDate from Campaign where UserId= ut.id order by CreatedDate desc) as SentDate,
  upt.RegisteredDate as 'RegDate'

FROM 
  AspNetUsers ut 
  INNER JOIN UserProfile upt ON ut.id = upt.id 
  INNER JOIN UserPlan pt ON ut.id = pt.UserId 
  AND pt.IsActive = 1 
  INNER JOIN PACKAGE pat ON pt.PackageId = pat.Id 
  INNER JOIN AspNetUserRoles urt ON ut.id = urt.UserId 
  INNER JOIN AspNetRoles rt ON rt.Id = urt.RoleId 
  LEFT JOIN dbo.SalesRepProfile srp ON upt.SalesRepId = srp.id 
  LEFT JOIN dbo.UserPaymentProfile upp ON ut.Id = upp.UserId 
  AND upp.Active = 1 
  AND upp.IsPrimary = 1 
  LEFT JOIN dbo.CampaignList ct ON upt.CampaignTypeId = ct.CampaignTypeId 
  WHERE 
  rt.Name != 'Admin' 
  AND (@Id is null or upt.salesrepid = @Id)
  )
 select * from CTEData
 WHERE 
   (
    (
      @FromDate IS NULL 
      AND @ToDate IS NULL
    ) 
    OR (
      RegDate BETWEEN @FromDate 
      AND @ToDate
    )
  ) 
  AND (
    @Year IS NULL 
    OR @year = Year(RegDate)
  ) 
  AND (
    @Month IS NULL 
    OR (
      @year = Year(RegDate) 
      AND @month = Month(RegDate)
    )
  ) 
  AND (
    @Quarter IS NULL 
    OR (
      1 = (
        CASE WHEN @Quarter = 'Q1' 
        AND Month(RegDate) BETWEEN 1 
        AND 3 THEN 1 WHEN @Quarter = 'Q2' 
        AND Month(RegDate) BETWEEN 4 
        AND 6 THEN 1 WHEN @Quarter = 'Q3' 
        AND Month(RegDate) BETWEEN 7 
        AND 9 THEN 1 WHEN @Quarter = 'Q4' 
        AND Month(RegDate) BETWEEN 10 
        AND 12 THEN 1 ELSE 0 END
      ) 
      AND @year = Year(RegDate)
    )
  ) 
  AND (
    @City IS NULL 
    OR (City LIKE '%' + @City + '%')
  ) 
  AND (
    @State IS NULL 
    OR (State LIKE '%' + @State + '%')
  ) 
  AND (
    @zip IS NULL 
    OR (Zip LIKE '%' + @Zip + '%')
  ) 
  AND (
    @Find IS NULL 
    OR (
      1 = (
        CASE WHEN @Find = '1' 
        AND BillingStatus = -1 THEN 1 WHEN @Find = '2' 
        AND SentDate IS NOT NULL THEN 1 WHEN @Find = '3' 
        AND SentDate IS NULL THEN 1 WHEN @Find = '4' 
        AND month(RegisteredDate) = MONTH(
          GetDate()
        ) THEN 1 WHEN @Find = '5' 
        AND LastLogin IS NOT NULL THEN 1 WHEN @Find = '6' 
        AND LastLogin IS NULL THEN 1 ELSE 0 END
      )
    )
  ) 
  AND (
    @MemberType IS NULL 
    OR @MemberType = PlanId
  ) 
  AND (
    @Status IS NULL 
    OR @Status = IsActive
  ) 
  AND (
    @CampaignType IS NULL 
    OR @CampaignType = CampaignTypeId
  ) 
  ORDER BY RegisteredDate DESC 


  END

GO
/****** Object:  StoredProcedure [dbo].[usp_getSalesRepTodays]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[usp_getSalesRepTodays] '09fd7d9a-42ee-4eb0-abb1-412ae8454e01'
CREATE PROCEDURE [dbo].[usp_getSalesRepTodays] 
@id nvarchar(128) null
as 
DECLARE
@AllClients int =0,
@NewClientCount int = 0, 
@NewClientBilling nvarchar(100), 
@MonthlyBillDue nvarchar(100), 
@MonthlyBillDeclined nvarchar(100), 
@PackagePurchase nvarchar(100), 
@PackageDeclined nvarchar(100) 
SET 
@AllClients = (
select 
      count(id) 
    from 
      UserProfile 
    where 
       SalesRepId = @id 
	  )
	  SET
  @NewClientCount = (
    select 
      count(id) 
    from 
      UserProfile 
    where 
      Convert(date, RegisteredDate) = CONVERT(
        date, 
        getdate()
      )  and SalesRepId = @id
  ) 
SET 
  @NewClientBilling = (
    select 
      ISNULL(
        Convert(
          varchar, 
          count(distinct atl.UserId)
        )+ 'client(s) for $' + convert(
          varchar, 
          sum(
            Convert(float, atl.Amount)
          )
        ), 
        '0 client(s) for $0'
      ) 
    from 
      AnTransactionLog atl
	  left join dbo.UserProfile  up on atl.UserId=up.Id and up.SalesRepId=@id
    where 
      CONVERT(date, atl.TransactionDate) = CONVERT(
        date, 
        getdate()
      ) 
      and atl.CreatedBy = 'Register'
  ) 
SET 
  @MonthlyBillDue = (
    select 
      '0 client(s) for $0'
  ) 
SET 
  @MonthlyBillDeclined = (
    select 
      '0 client(s) for $0'
  ) 
SET 
  @PackagePurchase =(
    select 
      ISNULL(
        Convert(
          varchar, 
          count(distinct atl.UserId)
        )+ 'client(s) for $' + convert(
          varchar, 
          sum(
            Convert(float, atl.Amount)
          )
        ), 
        '0 client(s) for $0'
      ) 
    from 
      AnTransactionLog atl
	  left join dbo.UserProfile  up on atl.UserId=up.Id and up.SalesRepId=@id
    where 
      CONVERT(date, atl.TransactionDate) = CONVERT(
        date, 
        getdate()
      ) 
      and atl.CreatedBy = 'Upgrade'
  ) 
SET 
  @PackageDeclined = (
    select 
      '0 client(s) for $0'
  )

  select
  @AllClients AS AllClients,
   @NewClientCount as TotalClient,
   @NewClientBilling as ClientBilling,
  @MonthlyBillDue as BillDue,
  @MonthlyBillDeclined as BillDeclined,
  @PackagePurchase as PacakagePurchase,
  @PackageDeclined as PackageDeclined
GO
/****** Object:  StoredProcedure [dbo].[usp_getScheduledCampigan]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[usp_getScheduledCampigan] as Begin 
	select 
		ct.CampaignId
		,ct.UserId
		,ct.ListId
		,ct.CampaignTypeID
		,ct.KeywordId
		,ct.Description
		,ct.AutomaticResponse
		,ct.ForwardedCellNumber
		,ct.ForwardedEmail
		,ct.CreatedById
		,ct.CreatedDate
		,ct.LastModById
		,ct.LastModDate
		,ct.HtmlContent
		,ct.Subject
		,ct.FromName
		,ct.FromEmail
		,ct.PreviewText
		,ct.TemplateId
		,ct.CamapignName
		,ct.Status
	from 
	  Campaign ct 
		  inner join ScheduledCampaign st on ct.CampaignId = st.CampaignId 
		  inner join List lt on ct.ListId = lt.ListId
	where ct.Status <> 'Sent' AND
	  CONVERT(varchar, getdate(),105) = CONVERT(varchar, st.ScheduledDateTime, 105) and 
	  DATEPART(HOUR,GETDATE()) = DATEPART(HOUR, st.ScheduledDateTime) 
	  --and DATEPART(MINUTE,GETDATE()) = DATEPART(MINUTE, st.ScheduledDateTime) 
  End

GO
/****** Object:  StoredProcedure [dbo].[usp_getSendCampaignList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[usp_getSendCampaignList]
--[usp_getSendCampaignList] '1e99575f-c92c-4842-9cd3-684e26182436'
--[usp_getSendCampaignList] '1e99575f-c92c-4842-9cd3-684e26182436','2019-02-19 15:27:34.587'
--[usp_getSendCampaignList] '1e99575f-c92c-4842-9cd3-684e26182436',NULL,NULL,2,10
CREATE PROCEDURE [dbo].[usp_getSendCampaignList] 
@userId nvarchar(128)= NULL, 
@DateFrom datetime = NULL, 
@DateTo datetime = NULL, 
@PageNumber INT = 0, 
@PageSize INT = NULL 
AS 
BEGIN 
SET 
  NOCOUNT ON DECLARE @TotalRecords INT;
IF @DateTo IS NULL 
SET 
  @DateTo = getdate();
DECLARE @SendCampaign TABLE (
  UserId nvarchar(128), 
  CampaignId int, 
  CamapignName nvarchar(100),
  SentDate datetime, 
  RecordNumber int IDENTITY (1, 1) PRIMARY KEY
);
WITH cte_campaign AS (
  SELECT 
    ct.userid, 
    ct.CampaignId, 
	ct.CamapignName,
    ct.SentDate 
  FROM 
    Campaign ct 
  WHERE 
    ct.Status = 'Sent' and ct.SentDate IS NOT NULL
    AND (
      @userId IS NULL 
      OR ct.UserId = @userId
    ) 
    AND (@DateFrom IS NULL OR convert(date, ct.SentDate) BETWEEN convert(date, @DateFrom) 
    AND CONVERT(date, @DateTo))
) 
INSERT INTO @SendCampaign
SELECT 
  cte.UserId, 
  cte.campaignId, 
  cte.CamapignName,
  cte.SentDate 
FROM 
  cte_campaign cte 
ORDER BY
	cte.CampaignId desc
SELECT 
  @totalrecords = COUNT(CampaignId) 
FROM 
  @SendCampaign IF (@PageSize IS NULL) 
SET 
  @PageSize = @totalrecords 
SELECT 
  * ,
  @totalrecords as TotalRecords
FROM 
  @SendCampaign 
WHERE 
  RecordNumber >= (@PageSize * @PageNumber) + 1 
  AND RecordNumber <= (
    @PageSize * (@PageNumber + 1)
  )
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSpendData]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetSpendData]
(
@daysfrom int,
@dayto int ,
@monthsfrom int ,
@monthto int ,
@yearfrom int ,
@yearsto INT,
@SearchText nvarchar(200)=null,
@SaleRepId  nvarchar(128)=null
)
AS
BEGIN

Declare @DateFrom Date , @DateTo DATE ;
Set @DateFrom = Convert(date, Convert(varchar(4), @yearfrom)+'/'+Convert(varchar(2), @monthsfrom)+'/'+Convert(varchar(2), @daysfrom))
Set @DateTo =Convert(date, Convert(varchar(4), @yearsto)+'/'+Convert(varchar(2), @monthto)+'/'+Convert(varchar(2), @dayto))
 SELECT 
		Pvt.*
		
		FROM (

SELECT UP.ID UserID,Srp.Id SalerepID, SRP.FirstName+ ' '+ srp.LastName SaleRepName,
up.FirstName+ ' '+ up.LastName AccountName,
up.PromotionCode,Up.ClientType,srp.SpentTotal, p.PlanName MemberType,
CONVERT(VARCHAR(20),up.RegisteredDate,100 )RegisteredDate,
up.ContactEmail,
up.MobilePhone,
SUM(CONVERT(NUMERIC(18,2),ATL.Amount))Amount,
DATENAME(month,ATL.CreatedDate) AmtMonth
 FROM dbo.UserProfile AS up
INNER JOIN dbo.SalesRepProfile AS srp
ON up.SalesRepId = Srp.Id
INNER JOIN AnTransactionLog ATL ON ATL.UserId=Up.Id AND ATL.AnTransactionId IS NOT NULL
LEFT JOIN  UserPlan UPP ON UPP.UserId=Up.Id AND Upp.IsActive =1
Left JOIN dbo.[Plan] AS p ON P.PlanId = UPP.UserPlanId

Where
(@SaleRepId is null or Srp.Id = @SaleRepId)
and (@SearchText is null Or (up.FirstName+ ' '+ up.LastName)like '%'+@SearchText+'%')
And Convert(Date, ATL.CreatedDate ) between @DateFrom and @DateTo


GROUP BY  UP.ID ,Srp.Id ,SRP.FirstName, srp.LastName ,up.FirstName,up.LastName ,
up.PromotionCode,Up.ClientType,srp.SpentTotal,up.RegisteredDate,up.ContactEmail,up.MobilePhone,
DATENAME(month,ATL.CreatedDate),p.PlanName
 
 )T

 PIVOT
 (
 SUM(Amount) FOR AmtMonth IN ([January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December])
 )Pvt

 END 
GO
/****** Object:  StoredProcedure [dbo].[usp_getSubscriberList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ALOK,usp_getTemplateList>
-- Create date: <12/21/2018 11:47 AM>
-- Description:	<[usp_getSubscriberList]>
-- =============================================
-- [usp_getSubscriberList] 59
-- [usp_getSubscriberList] 16,NULL,1,10
-- [usp_getSubscriberList] 16,'Trenton'
--[usp_getSubscriberList] 16,NULL,1,10
--[usp_getSubscriberList] 16,NULL,1,10,'NameDesc'
CREATE Procedure [dbo].[usp_getSubscriberList] 
	@ListId int, 
	@Search nvarchar(100) = NULL, 
	@PageNumber INT = 1, 
	@PageSize INT = NULL,
	@OrderBy NVARCHAR(100)='SubscriberId'
AS 
Begin 
	DECLARE @FirstRec INT, 
			@LastRec INT, 
			@TotalRecords INT 
	Select (ROW_NUMBER() OVER (ORDER BY case when @OrderBy = 'NameAsc' Then Sub.FirstName end,
										case when @OrderBy = 'NameDesc' Then Sub.FirstName End desc,
										case when @OrderBy = 'DateAsc' Then Sub.createddate End, 
										case when @OrderBy = 'DateDesc' Then Sub.createddate End desc,
										case when @OrderBy = 'SubscriberId' or @OrderBy is NULL  Then Sub.SubscriberId End desc )) AS Row, 
		  Sub.SubscriberId, 
		  SL.ListId, 
		  Sub.FirstName, 
		  Sub.LastName, 
		  Sub.DOB, 
		  Sub.Email, 
		  Sub.EmailPermission, 
		  Sub.Address1, 
		  Sub.Address2, 
		  Sub.City, 
		  Sub.State, 
		  Sub.Country, 
		  Sub.ZipCode, 
		  Sub.PhoneNumber, 
		  Sub.CreatedDate, 
		  Sub.LastModDate, 
		  SL.IsSubscribe into #temp
	from 
	  dbo.SubscriberList as SL 
		LEFT JOIN dbo.Subscriber as Sub 
			on SL.SubscriberId = Sub.SubscriberId 
	WHERE 
		SL.ListId = @ListId 
		AND Sub.IsDeleted = 0 
		And (SL.IsSubscribe = 1 or sl.IsSubscribe is null)
		AND(
			FirstName like '%'+ ISNULL(@Search,FirstName)+'%'
			OR LastName like '%'+ ISNULL(@Search,LastName)+'%'
			OR Email like '%'+ISNULL(@Search,Email)+'%'
			OR PhoneNumber like '%'+ISNULL(@Search,PhoneNumber)+'%'
			OR City like '%'+ISNULL(@Search,City)+'%'
			OR State like '%'+ISNULL(@Search,State)+'%'
			OR ZipCode like '%'+ISNULL(@Search,ZipCode)+'%'
			OR Country like '%'+ISNULL(@Search,Country)+'%'
			OR Address1 like '%'+ISNULL(@Search,Address1)+'%'
		)
	SELECT 
		@TotalRecords = Count(*) 
	FROM #temp 

  IF (@PageSize IS NULL) 
    SET 
		@PageSize = @TotalRecords 
		SELECT @FirstRec = (@PageNumber -1) * @PageSize 
		SELECT @LastRec = (@PageNumber * @PageSize) 

	SELECT *, 
		@TotalRecords AS TotalRecord, 
		@PageNumber AS CurrentPage, 
		@PageSize AS PageSize 
	FROM 
		#temp 
	WHERE 
		row > @FirstRec AND row <= @LastRec 
	DROP TABLE #temp
End

GO
/****** Object:  StoredProcedure [dbo].[usp_getTemplateList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ALOK,usp_getTemplateList>
-- Create date: <1/15/2019 1:10 PM>
-- Description:	<TO GET THE INFO OF PLAN INFO OF USER>
-- =============================================
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436','',1,10,'NameAsc'
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436','First'
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436',NULL,2,10
CREATE Procedure [dbo].[usp_getTemplateList] 
@UserId nvarchar(100), 
@Search nvarchar(100) = NULL, 
@PageNumber INT = 1, 
@PageSize INT = NULL,
@OrderBy NVARCHAR(100)='TemplateId'   
as 
Begin 
	DECLARE 
	@FirstRec     INT, 
     @LastRec      INT, 
     @TotalRecords INT ,
	@UserName nvarchar(100)
	Select @UserName = FirstName +' '+ LastName from dbo.UserProfile Where Id=@UserId	

         Select (ROW_NUMBER() OVER (ORDER BY 
					case when @OrderBy = 'NameAsc' Then tp.TemplateName end,
                    case when @OrderBy = 'NameDesc' Then TemplateName End desc,
                    case when @OrderBy = 'DateAsc' Then tp.CreatedDate End, 
                    case when @OrderBy = 'DateDesc' Then tp.CreatedDate End desc,
                     case when @OrderBy = 'TemplateId' or @OrderBy is NULL  Then tp.TemplateId End desc 
	                )) as Row, 
			  tp.TemplateId,tp.TemplateName,tp.UserId,tp.TemplateHtml,tp.CreatedDate,tp.CreatedById,tp.LastModById,
			  tp.LastModDate,tp.Description,tp.TemplatePath,@UserName as EditedBy into #TemplateTemp
			from 
			  dbo.Template as tp
			WHERE 
				tp.UserId = @UserId
				AND tp.IsDeleted = 0
				AND(
					TemplateName like '%'+ ISNULL(@Search,TemplateName)+'%'
					OR CreatedDate like '%'+ ISNULL(@Search,CreatedDate)+'%'
					OR LastModDate like '%'+ ISNULL(@Search,LastModDate)+'%'
					OR Description like '%'+ ISNULL(@Search,Description)+'%'
					OR TemplatePath like '%'+ ISNULL(@Search,TemplatePath)+'%'
				) 
				ORDER BY tp.TemplateId
		SELECT @TotalRecords = Count(*) 
FROM   #TemplateTemp 

IF ( @PageSize IS NULL ) 
    SET @PageSize = @TotalRecords 

SELECT @FirstRec = ( @PageNumber - 1 ) * @PageSize 

SELECT @LastRec = ( @PageNumber * @PageSize ) -- Create a temporary table 
SELECT *, 
        @TotalRecords AS TotalRecord, 
        @PageNumber   AS PageNumber, 
        @PageSize     AS PageSize 
FROM   #TemplateTemp 
WHERE  row > @FirstRec 
        AND row <= @LastRec 

DROP TABLE #TemplateTemp 
	
End


GO
/****** Object:  StoredProcedure [dbo].[usp_getUnSubscriberList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--[dbo].[usp_getUnSubscriberList] 56
CREATE Procedure [dbo].[usp_getUnSubscriberList] 
@ListId int, 
@Search nvarchar(100) = NULL, 
@PageNumber INT = 1, 
@PageSize INT = NULL, 
@OrderBy NVARCHAR(100)='SubscriberId'
as 
Begin 
DECLARE @FirstRec INT, 
@LastRec INT, 
@TotalRecords INT 
Select (ROW_NUMBER() OVER (ORDER BY 
					case when @OrderBy = 'NameAsc' Then Sub.FirstName end,
case when @OrderBy = 'NameDesc' Then Sub.FirstName End desc,
case when @OrderBy = 'DateAsc' Then Sub.createddate End, 
case when @OrderBy = 'DateDesc' Then Sub.createddate End desc,
case when @OrderBy = 'SubscriberId' or @OrderBy is NULL  Then Sub.SubscriberId End desc 
	)) as Row, 
	   
  Sub.SubscriberId, 
  SL.ListId, 
  Sub.FirstName, 
  Sub.LastName, 
  Sub.DOB, 
  Sub.Email, 
  Sub.EmailPermission, 
  Sub.Address1, 
  Sub.Address2, 
  Sub.City, 
  Sub.State, 
  Sub.Country, 
  Sub.ZipCode, 
  Sub.PhoneNumber, 
  Sub.CreatedDate, 
  Sub.LastModDate, 
  SL.IsSubscribe into #temp
from 
  dbo.SubscriberList as SL 
  LEFT JOIN dbo.Subscriber as Sub on SL.SubscriberId = Sub.SubscriberId 
WHERE 
  SL.ListId = @ListId 
 And SL.IsSubscribe = 0 
 AND Sub.IsDeleted = 0 
  AND(
  FirstName like '%'+ISNULL(@Search,FirstName)+'%'
  OR LastName like '%'+ISNULL(@Search,LastName)+'%'
  OR Email like '%'+ISNULL(@Search,Email)+'%'
  OR PhoneNumber like '%'+ISNULL(@Search,PhoneNumber)+'%'
  OR City like '%'+ISNULL(@Search,City)+'%'
  OR State like '%'+ISNULL(@Search,State)+'%'
  OR ZipCode like '%'+ISNULL(@Search,ZipCode)+'%'
  OR Country like '%'+ISNULL(@Search,Country)+'%'
  OR Address1 like '%'+ISNULL(@Search,Address1)+'%'
  	)

SELECT 
  @TotalRecords = Count(*) 
FROM 
  #temp 
  IF (@PageSize IS NULL) 
SET 
  @PageSize = @TotalRecords 
SELECT 
  @FirstRec = (@PageNumber -1) * @PageSize 
SELECT 
  @LastRec = (@PageNumber * @PageSize) 
SELECT 
  *, 
  @TotalRecords AS TotalRecord, 
  @PageNumber AS CurrentPage, 
  @PageSize AS PageSize 
FROM 
  #temp 
WHERE 
  row > @FirstRec 
  AND row <= @LastRec 
DROP 
  TABLE #temp
  End

GO
/****** Object:  StoredProcedure [dbo].[usp_getUpComingBillingDetails]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[dbo].[usp_getUpComingBillingDetails]
--[dbo].[usp_getUpComingBillingDetails] '2019-03-10'
  -- [dbo].[usp_getUpComingBillingDetails] '2019-03-01' , '2019-07-01' 
  -- [dbo].[usp_getUpComingBillingDetails] '2019-03-01' , '2019-07-01',NULL,0 
  -- [dbo].[usp_getUpComingBillingDetails] NULL ,NULL ,NULL,0 
  --[dbo].[usp_getUpComingBillingDetails] NULL ,NULL ,NULL,0 ,'1e99575f-c92c-4842-9cd3-684e26182436'
  --[dbo].[usp_getUpComingBillingDetails] NULL,NULL,NULL,0,'53d2f35c-8910-40d3-aa4e-f2e0faba6c9f'
  CREATE PROCEDURE [dbo].[usp_getUpComingBillingDetails] 
  @dateFrom DATE = NULL, 
  @dateTo DATE = NULL, 
  @PageSize INT = NULL, 
  @StartIndex INT = 0,
  @userId nvarchar(128) = NULL
AS BEGIN 
SET NOCOUNT ON 
DECLARE @totalrecords INT;
DECLARE @UpComingBilling TABLE (
  id NVARCHAR(128), 
  email NVARCHAR(256), 
  accountname NVARCHAR(200), 
  NAME NVARCHAR(200), 
  spenttotal FLOAT, 
  isactive BIT, 
  registereddate NVARCHAR(200), 
  days INT, 
  senttoday INT, 
  contacts INT, 
  monthlycredit FLOAT, 
  monthlybilling FLOAT, 
  nextbillingdate NVARCHAR(200), 
  billingstatus INT, 
  membertype VARCHAR(200), 
  chargeddate VARCHAR(200), 
  referralcode VARCHAR(200),
  reload BIT,
  followup VARCHAR(200),
  PlanId Int,
  recordnumber INT IDENTITY (1, 1) PRIMARY KEY
);
WITH cte_upcomingbillingdetail AS (
  SELECT 
    ut.id, 
    ut.email, 
    isNull(upt.firstname,'') + ' ' + ISNULL(upt.lastname,'') AS AccountName, 
    rt.NAME, 
    pt.totalspendcredits AS SpentTotal, 
    upt.isactive, 
    CONVERT(
      VARCHAR, 
      CONVERT(nvarchar ,upt.registereddate,110)
    ) AS RegisteredDate, 
    pt.totaldays - Datediff(
      day, 
      pt.packageupdatedate, 
      Getdate ()
    ) AS Days, 
    (
      SELECT 
        Count(*) 
      FROM 
        campaign ct 
      WHERE 
        CONVERT(DATE, ct.sentdate) = CONVERT(
          DATE, 
          Getdate()
        ) 
        AND ct.userid = ut.id
    ) AS SentToday, 
    (
      SELECT 
        Count(*) 
      FROM 
        subscriber st 
      WHERE 
        st.createdbyid = ut.id 
        AND isdeleted = 0
    ) AS Contacts, 
    pt.monthlycredit, 
    pt.billingamount AS MonthlyBilling, 
    CONVERT(
      VARCHAR, 
      CONVERT(DATE, pt.nextpackageupdatedate)
    ) AS NextBillingDate, 
    pt.billingstatus, 
    (
      SELECT 
        planname 
      FROM 
        [package] pge 
        INNER JOIN [plan] p ON pge.planid = p.planid 
        INNER JOIN userplan upt ON pge.id = upt.packageid 
      WHERE 
        upt.userid = ut.id 
        AND upt.isactive = 1
    ) AS MemberType, 
    (
     CONVERT(DATE,pt.PackageUpdateDate) )AS ChargedDate ,
	 upt.PromotionCode as Referralcode,
	NULL as Reload, 
	CONVERT(varchar,upt.FollowUpDate,110) as FollowUp,
	--(SELECT  TOP 1 CONVERT(varchar,nt.FollowUpDate,110)  
	--From 
	--SalesPortalRepNote nt
	--WHERE 
	--nt.UserId=ut.id and nt.IsDelete = 0
	--ORDER BY 
	--nt.LastModDate,CreatedDate DESC) AS FollowUp,
	(select PlanId from Package where Package.Id = pt.PackageId) as PlanId
  FROM 
    aspnetusers ut 
    INNER JOIN userprofile upt ON ut.id = upt.id 
    INNER JOIN userplan pt ON ut.id = pt.userid 
    AND pt.isactive = 1 
    INNER JOIN aspnetuserroles urt ON ut.id = urt.userid 
    INNER JOIN aspnetroles rt ON rt.id = urt.roleid 
  WHERE 
    rt.NAME != 'Admin' 
    and pt.NextPackageUpdateDate is not null 
	 AND 1 =  (
    CASE WHEN @dateFrom IS NULL AND @dateTo IS NULL THEN 1 
	WHEN @dateFrom IS NOT NULL AND @dateTo IS NULL AND convert(date, pt.nextpackageupdatedate) >= @dateFrom Then 1
	WHEN @dateTo IS NOT NULL and @dateFrom IS NOT NULL AND pt.nextpackageupdatedate BETWEEN @dateFrom AND @dateTo Then 1
	Else 0
	End
	)
   AND 1 = (
    CASE WHEN @userId IS NULL THEN 1 WHEN @userId IS NOT NULL 
    AND ut.id = @userId THEN 1 ELSE 0 END
  )
    --and pt.NextPackageUpdateDate > GETDATE()
) INSERT INTO @UpComingBilling 
SELECT 
  id, 
  email, 
  accountname, 
  NAME, 
  spenttotal, 
  isactive, 
  registereddate, 
  days, 
  senttoday, 
  contacts, 
  monthlycredit, 
  monthlybilling, 
  nextbillingdate, 
  billingstatus, 
  membertype, 
  chargeddate,
  reload,
  referralcode ,
    followup,
	PlanId
FROM 
  cte_upcomingbillingdetail up 
ORDER BY 
  up.registereddate DESC 
SELECT 
  @totalrecords = Count(id) 
FROM 
  @UpComingBilling IF (@PageSize IS NULL) 
SET 
  @PageSize = @totalrecords 
SELECT 
  id, 
  email, 
  accountname, 
  NAME, 
  spenttotal, 
  isactive, 
  registereddate, 
  days, 
  senttoday, 
  contacts, 
  monthlycredit, 
  monthlybilling, 
  nextbillingdate, 
  billingstatus, 
  membertype, 
  chargeddate,
  referralcode,
  reload,
  followup ,
  PlanId,
  recordnumber, 
  @totalrecords AS totalrecords 
FROM 
  @UpComingBilling 
WHERE 
  recordnumber >= (@PageSize * @StartIndex) + 1 
  AND recordnumber <= (
    @PageSize * (@StartIndex + 1)
  ) 
  ORDER BY nextbillingdate desc
 END

GO
/****** Object:  StoredProcedure [dbo].[usp_getUserDashboardData]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--usp_getUserDashboardData '1e99575f-c92c-4842-9cd3-684e26182436'
--usp_getUserDashboardData '390a6a23-756b-4906-a990-69c0bf0e45e4'
--usp_getUserDashboardData '390a6a23-756b-4906-a990-69c0bf0e45e4'
CREATE PROCEDURE [dbo].[usp_getUserDashboardData] 
@UserId NVARCHAR(128) 
AS 
BEGIN 
DECLARE 
@name NVARCHAR(100), 
@TotalContact INT, 
@TotalOpen INT, 
@TotalClicked INT, 
@TotalUnsubscibers INT,
@TotalEmails INT,
@DayLeft INT,
@PlanName varchar(200)
SET 
  @name = (
    SELECT 
      Isnull(firstname, '') + ' ' + Isnull(lastname, '') 
    FROM 
      userprofile 
    WHERE 
      id = @UserId
  ) 
SET 
  @TotalContact = (
    SELECT 
      Count(*) 
    FROM 
      subscriber 
    WHERE 
      createdbyid = @UserId 
      AND isdeleted = 0
  ) 
SET 
  @TotalOpen = (
    SELECT 
      Count(OpenCount) 
    FROM 
      Campaign 
    WHERE 
      CreatedById = @UserId
  ) 
SET 
  @TotalClicked = (
    SELECT 
      count(*) 
    FROM 
      Campaign 
    WHERE 
      CreatedById = @UserId
  ) 
SET 
  @TotalUnsubscibers = (
    select 
      count(st.SubscriberId) 
    from 
      SubscriberList slt 
      inner join Subscriber st on slt.SubscriberId = st.SubscriberId 
    where 
      st.CreatedById = @UserId 
      and st.IsDeleted = 0 
      and slt.IsSubscribe = 0
  ) 
SET
	@TotalEmails = (
	select upt.totalRemainingCredits from UserPlan upt where upt.UserId = @UserId and IsActive=1
		
	
	)
SET
	@DayLeft = (
	select (upt.TotalDays -DATEDIFF(day, upt.PackageUpdateDate, GETDATE())) from UserPlan upt where upt.userid = @UserId and IsActive=1
	
	)
SET
@PlanName =(
          select PlanName from [Package] pge inner join [Plan] p on pge.PlanId = p.PlanId  
		inner join UserPlan upt on pge.Id = upt.PackageId

		where upt.UserId = @UserId and upt.IsActive=1
		  

)
if exists(
    select 
      1 
    from 
      campaign 
    where 
      userid = @UserId
  ) 
  BEGIN
	SELECT 
	  TOP 5 @name AS NAME, 
	  ISNULL(@TotalContact, 0) AS TotalContact, 
	  ISNULL(@TotalOpen, 0) AS TotalOpen, 
	  ISNULL(@TotalClicked, 0) AS TotalClicked, 
	  ISNULL(@TotalUnsubscibers, 0) AS TotalUnsubscribers, 
	  ISNULL(@TotalEmails,0) as TotalEmails,
	  ISNULL(@DayLeft,0) as DaysLeft,
	  ISNULL(@PlanName,Null) as PlanName,
	  ct.CampaignId,
	  ct.camapignname, 
	  ct.sentdate, 
	  ISNULL(ct.clickedcount, 0) AS clickedcount, 
	  ISNULL(ct.opencount, 0) AS opencount, 
	  ct.status, 
	  scheduledcampaign.scheduleddatetime, 
	  ct.createddate, 
	  (
		SELECT 
		  Count(SL.SubscriberId) 
		FROM 
		  dbo.subscriberlist AS SL 
		  inner join Subscriber st on sl.SubscriberId = st.SubscriberId 
		WHERE 
		  SL.listid = ct.listid 
		  AND st.IsDeleted = 0 
		  AND SL.IsSubscribe = 1
	  ) AS TotalSubscriber 
	FROM 
	  campaign ct 
	  LEFT JOIN scheduledcampaign ON ct.campaignid = scheduledcampaign.campaignid 
	WHERE 
	  ct.userid = @UserId 
	ORDER BY 
	  ct.campaignid DESC 
	End 
	ELSE 
	Begin 
	Select 
	  @name AS NAME, 
	  ISNULL(@TotalContact, 0) AS TotalContact, 
	  ISNULL(@TotalOpen, 0) AS TotalOpen, 
	  ISNULL(@TotalClicked, 0) AS TotalClicked, 
	  ISNULL(@TotalUnsubscibers, 0) AS TotalUnsubscribers,
	  ISNULL(@TotalEmails,0) as TotalEmails,
	  ISNULL(@DayLeft,0) as DaysLeft,
	  ISNULL(@PlanName,Null) as PlanName,
	  -1 as CampaignId,
	  NULL as camapignname,
	  NULL as sentdate,
	  0 as clickedcount,
	  0 as opencount,
	  NULL as [status],
	  NULL as scheduledcampaign,
	  NULL as scheduleddatetime,
	  getdate() as createddate,
	  NULL as TotalSubscriber
  End 
  END

GO
/****** Object:  StoredProcedure [dbo].[usp_getUserDetails]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_getUserDetails '1eacd183-b1f7-44b8-936b-13ee21dedf41'
CREATE PROCEDURE [dbo].[usp_getUserDetails] 
	@id nvarchar(128) = NULL
AS
  SELECT
    U.Email,
	UProf.Id,
	UProf.FirstName,
	UProf.LastName,
	UProf.ContactEmail,
	UProf.OfficePhone,
	UProf.MobilePhone,
	UProf.Address1,
	UProf.Address2,
	UProf.City,
	UProf.State,
	UProf.Country,
	UProf.Zip,
	UProf.ProfileImage,
	UProf.CreditCardNo,
	UProf.ExpiryDate,
	UProf.RegisteredDate,
	UProf.IsActive,
	UProf.CompanyName,
	UProf.AuthProfileId,
	UProf.IndustryRefId,
	UProf.DOB,
	UProf.LastLogin,
	UProf.SalesRepId,
	UProf.ClientType,
	UProf.CampaignTypeId,
    UPlan.MonthlyCredit,
    UPlan.BillingAmount,
    UPlan.BillingStatus,
    UPlan.NextPackageUpdateDate,
    UPlan.PackageId,
    UPlan.CustomizableTemplates,
    UPProf.FirstName as NameOnCard,
	UProf.FollowUpDate,
	zt.TimeZoneName,
	UProf.FollowUpTime,
	(select PlanId from Package where Id = UPlan.PackageId) as PlanId,
	UProf.PromotionCode
  FROM AspNetUsers U
		INNER JOIN UserProfile UProf
				ON U.Id = UProf.Id
		LEFT JOIN UserPlan UPlan
				ON UPlan.userid = UProf.Id AND UPlan.IsActive = 1
		LEFT JOIN UserPaymentProfile UPProf
				ON UPProf.UserId = UProf.Id	AND UPProf.IsPrimary = 1
		LEFT JOIN TimeZone zt on UProf.TimeZoneId = zt.TimeZoneId
		--LEFT JOIN SalesPortalRepNote  SPRep
		--		ON SPRep.UserId=UProf.Id AND SPRep.CreatedById = UProf.SalesRepId
  WHERE U.Id = @id
GO
/****** Object:  StoredProcedure [dbo].[usp_getUserList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436' 
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436','P1' 
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436',NULL,1,10,NULL
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436',NULL,1,10,'NameDesc'
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436','t',1,NULL,'NameDesc'
CREATE PROCEDURE [dbo].[usp_getUserList] 
@CreatedById NVARCHAR(100),
@ListName    NVARCHAR(100)= NULL, 
@PageNumber  INT = 1, 
@PageSize    INT = NULL,
@OrderBy NVARCHAR(100)='listid'
AS 
Begin
--SET FMTONLY OFF
DECLARE @FirstRec     INT, 
        @LastRec      INT, 
        @TotalRecords INT 

SELECT ( Row_number() 
            OVER ( 
                ORDER BY 
				case when @OrderBy = 'NameAsc' Then l.ListName end,
case when @OrderBy = 'NameDesc' Then l.ListName End desc,
case when @OrderBy = 'DateAsc' Then l.createddate End, 
case when @OrderBy = 'DateDesc' Then l.createddate End desc,
case when @OrderBy = 'listid' or @OrderBy is NULL  Then listid End desc 
				
				) ) AS ROW, 
        L.ListId, 
        L.ListName, 
        L.CreatedDate, 
        '0'                              AS Opens, 
        '0'                              AS Clicked, 
        --@TotalRecords AS TotalRecord,  
        --@PageNumber AS CurrentPage,  
        --@PageSize AS PageSize,  
        (SELECT Count(SL.listid) 
        FROM   dbo.subscriberlist AS SL 
        WHERE  L.listid = SL.listid)    AS TotalSubscriber 
		INTO   #temp 
		FROM   dbo.list AS L 
		WHERE  L.createdbyid  = @CreatedById 
        AND L.isdeleted = 0 
        AND @ListName IS NULL 
        OR listname LIKE '%'+ @ListName + '%' 
--declare @totalrecords int 
SELECT @TotalRecords = Count(*) 
FROM   #temp 

IF ( @PageSize IS NULL ) 
    SET @PageSize = @TotalRecords 

SELECT @FirstRec = ( @PageNumber - 1 ) * @PageSize 

SELECT @LastRec = ( @PageNumber * @PageSize ) -- Create a temporary table 
SELECT *, 
        @TotalRecords AS TotalRecord, 
        @PageNumber   AS CurrentPage, 
        @PageSize     AS PageSize 
FROM   #temp 
WHERE  row > @FirstRec 
        AND row <= @LastRec 

DROP TABLE #temp 

End
GO
/****** Object:  StoredProcedure [dbo].[usp_getUserNoteList]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_getUserNoteList '10128a2a-baac-470d-a065-5e7217c16b45'
CREATE PROCEDURE [dbo].[usp_getUserNoteList]
@userid nvarchar(128)
AS
  SELECT
    SalesPortalRepNote.*,
    ISNULL(SalesRepProfile.FirstName + ' ' + SalesRepProfile.LastName, 'Admin') AS 'CreatedBy'
  FROM SalesPortalRepNote
  LEFT JOIN SalesRepProfile
    ON SalesPortalRepNote.CreatedById = SalesRepProfile.Id
  WHERE SalesPortalRepNote.UserId = @userid
  AND SalesPortalRepNote.IsDelete = 0
  ORDER BY CreatedDate DESC
GO
/****** Object:  StoredProcedure [dbo].[usp_updateSalesRepClientsTotal]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_updateSalesRepClientsTotal]
as
update SalesRepProfile set NoOfClients = 
(select count(*)
from userprofile where SalesRepId = SalesRepProfile.Id)
GO
/****** Object:  StoredProcedure [dbo].[USP_UserActivityLog]    Script Date: 4/1/2019 11:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- USP_UserActivityLog 'f43ad717-54e0-441a-ae9c-25e0b184547f',null,null

CREATE PROC [dbo].[USP_UserActivityLog]
(
	@UserId NVARCHAR(128)=NULL,
	@DateFrom DATE =Null,-- For Initial Date
	@DateTo DATE =NULL
)
AS
BEGIN
IF(@DateFrom IS null)
BEGIN
SET @DateFrom = '1900/01/01'
end
IF(@DateTo IS null)
BEGIN
SET @DateTo = GETDATE()
end
SELECT  up.FirstName ,
        up.LastName ,
        ual.LogId ,
        ual.UserId ,
        ual.Description ,
        CONVERT(VARCHAR(30), ual.[DateTime],100) CreateDate ,
        ual.Createdby
FROM    dbo.UserActivityLog AS ual
        LEFT JOIN dbo.UserProfile AS up ON ual.UserId = up.Id
		WHERE (@UserId IS NULL or Ual.UserId = @UserId)
		AND CONVERT(DATE, ual.[DateTime]) BETWEEN @DateFrom  AND @DateTo 

END
        
GO
USE [master]
GO
ALTER DATABASE [EmailMarketingDb] SET  READ_WRITE 
GO
