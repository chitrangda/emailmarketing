﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EM.Web.Attributes
{
    public class LogActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.Log("OnActionExecuting", filterContext.RouteData);
            base.OnActionExecuting(filterContext);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.Log("OnActionExecuted", filterContext.RouteData);
            base.OnActionExecuted(filterContext);
        }
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            this.Log("OnResultExecuting", filterContext.RouteData);
            base.OnResultExecuting(filterContext);
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            this.Log("OnResultExecuted", filterContext.RouteData);
            base.OnResultExecuted(filterContext);
        }
        private void Log(string methodName, RouteData routeData)
        {
            //var UserDetails = (dynamic)null;
            //if (HttpContext.Current.Session["UserDetails"] !=null || HttpContext.Current.Session["AdminDetails"] != null)
            //{
            //    UserDetails = HttpContext.Current.Session["AdminDetails"] == null ? HttpContext.Current.Session["UserDetails"] as UserInfoViewModel : HttpContext.Current.Session["AdminDetails"] as UserInfoViewModel;
            //}
            //var areaName = routeData.DataTokens["area"];
            //var controllerName = routeData.Values["controller"];
            //var actionName = routeData.Values["action"];
            //if (UserDetails !=null)
            //{
            //    var userId = UserDetails.Id == null ? "null" : UserDetails.Id;
            //    string message = string.Format("MethodName : {0} , ActionName : {1} , ControllerName : {2} , AreaName : {3} , CreatedAt : {4} , CreatedBy : {5}" + Environment.NewLine + "", methodName, actionName, controllerName, areaName, DateTime.UtcNow.ToString(), userId);
            //    Helpers.CreateLog.ActionLog(message);
            //    //Factory.EmailMarketingDbEntities _context = new Factory.EmailMarketingDbEntities();
            //    //Factory.ActionLog actionLog = new Factory.ActionLog();
            //    //actionLog.ActionName = actionName.ToString();
            //    //actionLog.ControllerName = controllerName.ToString();
            //    //actionLog.MethodName = methodName;
            //    //actionLog.AreaName = areaName == null ? "" : areaName.ToString();
            //    //actionLog.CreatedAt = DateTime.UtcNow;
            //    //actionLog.UserRefId = UserDetails.Id == null ? "" : UserDetails.Id;
            //    //_context.ActionLogs.Add(actionLog);
            //    //_context.SaveChanges();
            //}
            
        }
    }
}