﻿using EM.Web.Attributes;
using System.Web;
using System.Web.Mvc;

namespace EM.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new LogActionFilterAttribute());
            filters.Add(new NoCacheAttribute());
        }
    }
}
