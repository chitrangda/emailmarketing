﻿using System.Web;
using System.Web.Optimization;

namespace EM.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.min.js",
                         "~/Content/jsHome/popper.min.js",
                         "~/Content/jsHome/bootstrap.min.js",
                         "~/Content/jsHome/holder.min.js",
                         "~/Content/jsHome/video.min.js",
                         "~/Content/jsUser/plugins/moment.min.js",
                         "~/Content/jsUser/daterangepicker.min.js",
                         "~/Content/jsHome/custom.js",
                         "~/Content/CustomJs/common.js",
                         "~/Content/jsUser/plugins/sweetalert.min.js",
                         "~/Content/jsUser/plugins/bootbox.min.js",
                         "~/Content/jsUser/plugins/html2canvas.min.js"
                         ));
            bundles.Add(new ScriptBundle("~/bundles/UserJQuery").Include(
                         "~/Scripts/jquery-3.3.1.min.js",
                       "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Content/jsUser/popper.min.js",
                        "~/Content/jsUser/bootstrap.min.js",
                        "~/Content/jsUser/main.js",
                        "~/Content/jsUser/plugins/pace.min.js",
                        "~/Content/jsUser/plugins/bootstrap-notify.min.js",
                        "~/Content/CustomJs/common.js",
                        "~/Content/jsUser/plugins/sweetalert.min.js",
                        "~/Content/jsUser/plugins/moment.min.js",
                        "~/Scripts/moment-timezone-with-data.js",
                        "~/Content/jsUser/daterangepicker.min.js",
                         "~/Content/jsUser/plugins/bootbox.min.js",
                         "~/Content/jsUser/plugins/html2canvas.min.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/AdminJS").Include(
                        "~/Content/admin/js/jquery-2.2.3.min.js",
                       "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Content/jsUser/popper.min.js",
                          "~/Content/admin/js/tether.js",
                        "~/Content/admin/js/bootstrap.min.js",
                        "~/Content/admin/js/mdb.min.js",
                        "~/Content/jsUser/plugins/pace.min.js",
                        "~/Content/jsUser/plugins/bootstrap-notify.min.js",
                        "~/Content/CustomJs/common.js",
                        "~/Content/jsUser/plugins/sweetalert.min.js",
                        "~/Content/jsUser/plugins/moment.min.js",
                        "~/Content/jsUser/daterangepicker.min.js",
                         "~/Content/jsUser/plugins/bootbox.min.js",
                        "~/Content/admin/js/jquery.dataTables.min.js",
                        "~/Content/admin/js/dataTables.bootstrap.min.js",
                        "~/Content/admin/js/dataTables.fixedColumns.min.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/daterangepicker").Include("~/Content/jsUser/daterangepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/AdminCSS").Include(
                       "~/Content/admin/css/bootstrap.min.css",
                       "~/Content/admin/css/mdb.min.css",
                       "~/Content/admin/css/style.css",
                       "~/Content/admin/css/responsive.css",
                       "~/Content/cssUser/daterangepicker.css",
                       "~/Content/admin/css/jsgrid.min.css",
                       "~/Content/admin/css/jsgrid-theme.min.css",
                   "~/Content/Loader.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/cssUser/main.css",
                        "~/Content/cssHome/font-awesome.min.css",
                        "~/Content/cssHome/bootstrap.min.css",
                        "~/Content/cssHome/style.css",
                      "~/Content/cssHome/video-js.min.css",
                        "~/Content/cssHome/responsive.css",
                        "~/Content/cssUser/daterangepicker.css",
                    "~/Content/Loader.css"));

        }
    }
}
