﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMIPayment
{
    public class PaymentStatus
    {
        public string result { get; set; }

        public string resulttext { get; set; }

        public string resultcode { get; set; }

        public string transactionid { get; set; }
        public string amount { get; set; }
        public string customervaultid { get; set; }

        public string Company { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string OrderId { get; set; }

        public string CCNumber { get; set; }

        public string CCEXp { get; set; }

        public string OrderDesc { get; set; }

        public string PoNumber { get; set; }

        public string CustomField { get; set; }

        public string CustomField2 { get; set; }

        public string CustomField3 { get; set; }

        public string TransCode { get; set; }

        public string TransCreatedBy { get; set; }

    }
}
