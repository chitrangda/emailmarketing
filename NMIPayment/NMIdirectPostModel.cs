﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMIPayment
{
    public class NMIdirectPostModel
    {
        public  string username { get; set; }
        public string password { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public decimal amount { get; set; }
        public string ccnumber { get; set; }
        public string ccexp { get; set; }
        public string cvv { get; set; }


    }
}
