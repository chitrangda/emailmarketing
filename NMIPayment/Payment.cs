﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Net;
using System.IO;
using System.Net.Http;
using EM.Helpers;
using System.Collections.Specialized;
using System.Web;
using EM.Factory;
using System.Configuration;

namespace NMIPayment
{
    public class Payment : IDisposable
    {
        private bool _disposed;

        public string Transaction(PaymentInfo paymentInfo, bool isProduction = false, bool isNewCustomer = true,string createdBy="")
        {

            _disposed = false;
            var status = string.Empty;
            XmlDocument xmlRequest = new XmlDocument();

            XmlDeclaration xmlDecl = xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");

            XmlElement root = xmlRequest.DocumentElement;
            xmlRequest.InsertBefore(xmlDecl, root);


            XmlElement xmlSale = xmlRequest.CreateElement("sale");

            XmlElement xmlApiKey = xmlRequest.CreateElement("api-key");

            xmlApiKey.InnerText = isProduction ? getProductionKey() : getStagingKey();

            xmlSale.AppendChild(xmlApiKey);

            XmlElement xmlRedirectUrl = xmlRequest.CreateElement("redirect-url");
            //xmlRedirectUrl.InnerText = HttpContext.Current.Request["HTTP_REFERER"];
            xmlRedirectUrl.InnerText = paymentInfo.NavigateUrl;
            xmlSale.AppendChild(xmlRedirectUrl);

            if (paymentInfo.amount != null)
            {
                XmlElement xmlAmount = xmlRequest.CreateElement("amount");
                xmlAmount.InnerText = paymentInfo.amount;
                xmlSale.AppendChild(xmlAmount);
            }



            if (paymentInfo.orderInfo != null)
            {
                XmlElement xmlCurrency = xmlRequest.CreateElement("currency");
                xmlCurrency.InnerText = paymentInfo.orderInfo.Currency != null ? paymentInfo.orderInfo.Currency : "USD";
                xmlSale.AppendChild(xmlCurrency);


                XmlElement xmlOrderId = xmlRequest.CreateElement("order-id");
                xmlOrderId.InnerText = paymentInfo.orderInfo.OrderId;
                xmlSale.AppendChild(xmlOrderId);

                XmlElement xmlOrderDescription = xmlRequest.CreateElement("order-description");
                xmlOrderDescription.InnerText = paymentInfo.orderInfo.OrderDesc;
                xmlSale.AppendChild(xmlOrderDescription);

                XmlElement xmlTax = xmlRequest.CreateElement("tax-amount");
                xmlTax.InnerText = paymentInfo.orderInfo.TaxAmount.ToString();
                xmlSale.AppendChild(xmlTax);

                XmlElement xmlShipping = xmlRequest.CreateElement("shipping-amount");
                xmlShipping.InnerText = "0.0";
                xmlSale.AppendChild(xmlShipping);

                if (paymentInfo.orderInfo.PoNumber != null)
                {
                    XmlElement xmlPoNumber = xmlRequest.CreateElement("po-number");
                    xmlPoNumber.InnerText = paymentInfo.orderInfo.PoNumber;
                    xmlSale.AppendChild(xmlPoNumber);
                }

                if (paymentInfo.orderInfo.CustomField != null)
                {
                    XmlElement xmlCustomField = xmlRequest.CreateElement("merchant-defined-field-1");
                    xmlCustomField.InnerText = paymentInfo.orderInfo.CustomField;
                    xmlSale.AppendChild(xmlCustomField);
                }
                if (paymentInfo.orderInfo.CustomField2 != null)
                {
                    XmlElement xmlCustomField2 = xmlRequest.CreateElement("merchant-defined-field-2");
                    xmlCustomField2.InnerText = paymentInfo.orderInfo.CustomField2;
                    xmlSale.AppendChild(xmlCustomField2);
                }

                if (paymentInfo.orderInfo.CustomField3 != null)
                {
                    XmlElement xmlCustomField4 = xmlRequest.CreateElement("merchant-defined-field-4");
                    xmlCustomField4.InnerText = paymentInfo.orderInfo.CustomField3;
                    xmlSale.AppendChild(xmlCustomField4);
                }
            }
            if (paymentInfo.TransCode != null)
            {
                XmlElement xmlCustomField3 = xmlRequest.CreateElement("merchant-defined-field-3");
                xmlCustomField3.InnerText = paymentInfo.TransCode;
                xmlSale.AppendChild(xmlCustomField3);
            }
            if (!isNewCustomer)
            {
                XmlElement xmlCustomerVaultId = xmlRequest.CreateElement("customer-vault-id");
                xmlCustomerVaultId.InnerText = paymentInfo.CustomerVaultId;
                xmlSale.AppendChild(xmlCustomerVaultId);

            }
            else
            {
                XmlElement xmlAddCustomer = xmlRequest.CreateElement("add-customer");

                XmlElement xmlCustomerVaultId = xmlRequest.CreateElement("customer-vault-id");
                xmlCustomerVaultId.InnerText = paymentInfo.CustomerVaultId;
                xmlAddCustomer.AppendChild(xmlCustomerVaultId);

                xmlSale.AppendChild(xmlAddCustomer);
            }

            if (paymentInfo.billingInfo != null)
            {
                XmlElement xmlBillingAddress = xmlRequest.CreateElement("billing");

                XmlElement xmlFirstName = xmlRequest.CreateElement("first-name");
                xmlFirstName.InnerText = paymentInfo.billingInfo.FirstName;
                xmlBillingAddress.AppendChild(xmlFirstName);

                XmlElement xmlLastName = xmlRequest.CreateElement("last-name");
                xmlLastName.InnerText = paymentInfo.billingInfo.LastName;
                xmlBillingAddress.AppendChild(xmlLastName);
                if (paymentInfo.billingInfo.Address1 != null && paymentInfo.billingInfo.Address1 != "")
                {
                    XmlElement xmlAddress1 = xmlRequest.CreateElement("address1");
                    xmlAddress1.InnerText = paymentInfo.billingInfo.Address1;
                    xmlBillingAddress.AppendChild(xmlAddress1);
                }

                XmlElement xmlCity = xmlRequest.CreateElement("city");
                xmlCity.InnerText = paymentInfo.billingInfo.City;
                xmlBillingAddress.AppendChild(xmlCity);

                XmlElement xmlState = xmlRequest.CreateElement("state");
                xmlState.InnerText = paymentInfo.billingInfo.State;
                xmlBillingAddress.AppendChild(xmlState);

                XmlElement xmlZip = xmlRequest.CreateElement("postal");
                xmlZip.InnerText = paymentInfo.billingInfo.Zip;
                xmlBillingAddress.AppendChild(xmlZip);

                XmlElement xmlCountry = xmlRequest.CreateElement("country");
                xmlCountry.InnerText = paymentInfo.billingInfo.Country;
                xmlBillingAddress.AppendChild(xmlCountry);

                XmlElement xmlPhone = xmlRequest.CreateElement("phone");
                xmlPhone.InnerText = paymentInfo.billingInfo.Phone;
                xmlBillingAddress.AppendChild(xmlPhone);


                XmlElement xmlEmail = xmlRequest.CreateElement("email");
                xmlEmail.InnerText = paymentInfo.billingInfo.Email;
                xmlBillingAddress.AppendChild(xmlEmail);
                if (paymentInfo.billingInfo.Company != null && paymentInfo.billingInfo.Company != "")
                {
                    XmlElement xmlCompany = xmlRequest.CreateElement("company");
                    xmlCompany.InnerText = paymentInfo.billingInfo.Company;
                    xmlBillingAddress.AppendChild(xmlCompany);
                }
                if (paymentInfo.billingInfo.Address2 != null && paymentInfo.billingInfo.Address2 != "")
                {
                    XmlElement xmlAddress2 = xmlRequest.CreateElement("address2");
                    xmlAddress2.InnerText = paymentInfo.billingInfo.Address2;
                    xmlBillingAddress.AppendChild(xmlAddress2);
                }
                if (paymentInfo.billingInfo.Fax != null && paymentInfo.billingInfo.Fax != "")
                {
                    XmlElement xmlFax = xmlRequest.CreateElement("fax");
                    xmlFax.InnerText = paymentInfo.billingInfo.Fax;
                    xmlBillingAddress.AppendChild(xmlFax);
                }



                xmlSale.AppendChild(xmlBillingAddress);
            }

            //////////
            if (paymentInfo.shippingInfo != null)
            {
                XmlElement xmlShippingAddress = xmlRequest.CreateElement("shipping");

                XmlElement xmlSFirstName = xmlRequest.CreateElement("first-name");
                xmlSFirstName.InnerText = paymentInfo.shippingInfo.FirstName;
                xmlShippingAddress.AppendChild(xmlSFirstName);

                XmlElement xmlSLastName = xmlRequest.CreateElement("last-name");
                xmlSLastName.InnerText = paymentInfo.shippingInfo.LastName;
                xmlShippingAddress.AppendChild(xmlSLastName);
                if (paymentInfo.shippingInfo.Address1 != null && paymentInfo.shippingInfo.Address1 != "")
                {
                    XmlElement xmlSAddress1 = xmlRequest.CreateElement("address1");
                    xmlSAddress1.InnerText = paymentInfo.shippingInfo.Address1;
                    xmlShippingAddress.AppendChild(xmlSAddress1);
                }

                XmlElement xmlSCity = xmlRequest.CreateElement("city");
                xmlSCity.InnerText = paymentInfo.shippingInfo.Address2;
                xmlShippingAddress.AppendChild(xmlSCity);

                XmlElement xmlSState = xmlRequest.CreateElement("state");
                xmlSState.InnerText = paymentInfo.shippingInfo.State;
                xmlShippingAddress.AppendChild(xmlSState);

                XmlElement xmlSZip = xmlRequest.CreateElement("postal");
                xmlSZip.InnerText = paymentInfo.shippingInfo.Zip;
                xmlShippingAddress.AppendChild(xmlSZip);

                XmlElement xmlSCountry = xmlRequest.CreateElement("country");
                xmlSCountry.InnerText = paymentInfo.shippingInfo.Country;
                xmlShippingAddress.AppendChild(xmlSCountry);

                XmlElement xmlSPhone = xmlRequest.CreateElement("phone");
                xmlSPhone.InnerText = paymentInfo.shippingInfo.Phone;
                xmlShippingAddress.AppendChild(xmlSPhone);
                if (paymentInfo.shippingInfo.Company != null && paymentInfo.shippingInfo.Company != "")
                {
                    XmlElement xmlSCompany = xmlRequest.CreateElement("company");
                    xmlSCompany.InnerText = paymentInfo.shippingInfo.Company;
                    xmlShippingAddress.AppendChild(xmlSCompany);
                }
                if (paymentInfo.shippingInfo.Address2 != null && paymentInfo.shippingInfo.Address2 != "")
                {
                    XmlElement xmlSAddress2 = xmlRequest.CreateElement("address2");
                    xmlSAddress2.InnerText = paymentInfo.shippingInfo.Address2;
                    xmlShippingAddress.AppendChild(xmlSAddress2);
                }
                if (paymentInfo.shippingInfo.Fax != null && paymentInfo.shippingInfo.Fax != "")
                {
                    XmlElement xmlSFax = xmlRequest.CreateElement("fax");
                    xmlSFax.InnerText = paymentInfo.shippingInfo.Fax;
                    xmlShippingAddress.AppendChild(xmlSFax);
                }


                xmlSale.AppendChild(xmlShippingAddress);
            }


            xmlRequest.AppendChild(xmlSale);


            string responseFromServer = sendXMLRequest(xmlRequest, isProduction);
            //Console.WriteLine(responseFromServer);
            XmlReader responseReader = XmlReader.Create(new StringReader(responseFromServer));
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(responseReader);
            XmlNodeList response = xDoc.GetElementsByTagName("result");
            XmlNodeList responseCode = xDoc.GetElementsByTagName("result-code");
            XmlNodeList responseText = xDoc.GetElementsByTagName("result-text");
            if (response[0].InnerText.Equals("1"))
            {
                XmlNodeList formUrl = xDoc.GetElementsByTagName("form-url");
                responseReader.Close();
                string token = formUrl[0].InnerText.ToString().Substring((formUrl[0].InnerText.LastIndexOf("/") + 1));
                using (var db = new EmailMarketingDbEntities())
                {
                    AnTransactionLog transLog = new AnTransactionLog();
                    transLog.CreatedBy = createdBy;
                    transLog.TransactionType = "Credit Card";
                    transLog.Amount = paymentInfo.amount;
                    transLog.CreatedDate = DateTime.Now;
                    transLog.UserId = paymentInfo.orderInfo.CustomField;
                    transLog.PaymentProfileId = paymentInfo.CustomerVaultId;
                    transLog.Message = paymentInfo.orderInfo.OrderDesc;
                    transLog.TransCode = token;
                    db.AnTransactionLogs.Add(transLog);
                    db.SaveChanges();

                }
                sendCreditCardDetails(formUrl[0].InnerText, paymentInfo.CCNumber, paymentInfo.CCEXp, paymentInfo.CCV, isNewCustomer);
                return token;
            }
            else
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    AnTransactionLog transLog = new AnTransactionLog();
                    transLog.TransactionDate = DateTime.Now;
                    transLog.CreatedBy = "Register";
                    transLog.TransactionType = "Credit Card";
                    transLog.Amount = paymentInfo.amount;
                    transLog.CreatedDate = DateTime.Now;
                    transLog.UserId = paymentInfo.CustomerVaultId;
                    transLog.StatusCode = responseCode[0].InnerText;
                    transLog.StatusText = responseText[0].InnerText;
                    db.AnTransactionLogs.Add(transLog);

                }
                return null;

            }
        }

        public void CreateUpdateCustomer(PaymentInfo paymentInfo, bool isProduction = false, bool isNewCustomer = true)
        {
            XmlDocument xmlRequest = new XmlDocument();

            XmlDeclaration xmlDecl = xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");

            XmlElement root = xmlRequest.DocumentElement;
            xmlRequest.InsertBefore(xmlDecl, root);

            XmlElement xmlAddCustomer = null;
            if (isNewCustomer)
            {
                xmlAddCustomer = xmlRequest.CreateElement("add-customer");
            }
            else
            {
                xmlAddCustomer = xmlRequest.CreateElement("update-customer");

            }
            XmlElement xmlApiKey = xmlRequest.CreateElement("api-key");
            xmlApiKey.InnerText = isProduction ? getProductionKey() : getStagingKey();
            xmlAddCustomer.AppendChild(xmlApiKey);

            XmlElement xmlRedirectUrl = xmlRequest.CreateElement("redirect-url");
            xmlRedirectUrl.InnerText = paymentInfo.NavigateUrl;
            xmlAddCustomer.AppendChild(xmlRedirectUrl);

            XmlElement xmlCustomerVaultId = xmlRequest.CreateElement("customer-vault-id");
            xmlCustomerVaultId.InnerText = paymentInfo.CustomerVaultId;
            xmlAddCustomer.AppendChild(xmlCustomerVaultId);

            XmlElement xmlBillingAddress = xmlRequest.CreateElement("billing");

            XmlElement xmlFirstName = xmlRequest.CreateElement("first-name");
            xmlFirstName.InnerText = paymentInfo.billingInfo.FirstName;
            xmlBillingAddress.AppendChild(xmlFirstName);

            XmlElement xmlLastName = xmlRequest.CreateElement("last-name");
            xmlLastName.InnerText = paymentInfo.billingInfo.LastName;
            xmlBillingAddress.AppendChild(xmlLastName);

            XmlElement xmlAddress1 = xmlRequest.CreateElement("address1");
            xmlAddress1.InnerText = paymentInfo.billingInfo.Address1;
            xmlBillingAddress.AppendChild(xmlAddress1);

            XmlElement xmlCity = xmlRequest.CreateElement("city");
            xmlCity.InnerText = paymentInfo.billingInfo.City;
            xmlBillingAddress.AppendChild(xmlCity);

            XmlElement xmlState = xmlRequest.CreateElement("state");
            xmlState.InnerText = paymentInfo.billingInfo.State;
            xmlBillingAddress.AppendChild(xmlState);

            XmlElement xmlZip = xmlRequest.CreateElement("postal");
            xmlZip.InnerText = paymentInfo.billingInfo.Zip;
            xmlBillingAddress.AppendChild(xmlZip);

            XmlElement xmlCountry = xmlRequest.CreateElement("country");
            xmlCountry.InnerText = paymentInfo.billingInfo.Country;
            xmlBillingAddress.AppendChild(xmlCountry);

            XmlElement xmlPhone = xmlRequest.CreateElement("phone");
            xmlPhone.InnerText = paymentInfo.billingInfo.Phone;
            xmlBillingAddress.AppendChild(xmlPhone);


            XmlElement xmlEmail = xmlRequest.CreateElement("email");
            xmlEmail.InnerText = paymentInfo.billingInfo.Email;
            xmlBillingAddress.AppendChild(xmlEmail);

            XmlElement xmlCompany = xmlRequest.CreateElement("company");
            xmlCompany.InnerText = paymentInfo.billingInfo.Company;
            xmlBillingAddress.AppendChild(xmlCompany);

            XmlElement xmlAddress2 = xmlRequest.CreateElement("address2");
            xmlAddress2.InnerText = paymentInfo.billingInfo.Address2;
            xmlBillingAddress.AppendChild(xmlAddress2);

            XmlElement xmlFax = xmlRequest.CreateElement("fax");
            xmlFax.InnerText = paymentInfo.billingInfo.Fax;
            xmlBillingAddress.AppendChild(xmlFax);



            xmlAddCustomer.AppendChild(xmlBillingAddress);

            //////////

            XmlElement xmlShippingAddress = xmlRequest.CreateElement("shipping");

            XmlElement xmlSFirstName = xmlRequest.CreateElement("first-name");
            xmlSFirstName.InnerText = paymentInfo.shippingInfo.FirstName;
            xmlShippingAddress.AppendChild(xmlSFirstName);

            XmlElement xmlSLastName = xmlRequest.CreateElement("last-name");
            xmlSLastName.InnerText = paymentInfo.shippingInfo.LastName;
            xmlShippingAddress.AppendChild(xmlSLastName);

            XmlElement xmlSAddress1 = xmlRequest.CreateElement("address1");
            xmlSAddress1.InnerText = paymentInfo.shippingInfo.Address1;
            xmlShippingAddress.AppendChild(xmlSAddress1);

            XmlElement xmlSCity = xmlRequest.CreateElement("city");
            xmlSCity.InnerText = paymentInfo.shippingInfo.Address2;
            xmlShippingAddress.AppendChild(xmlSCity);

            XmlElement xmlSState = xmlRequest.CreateElement("state");
            xmlSState.InnerText = paymentInfo.shippingInfo.State;
            xmlShippingAddress.AppendChild(xmlSState);

            XmlElement xmlSZip = xmlRequest.CreateElement("postal");
            xmlSZip.InnerText = paymentInfo.shippingInfo.Zip;
            xmlShippingAddress.AppendChild(xmlSZip);

            XmlElement xmlSCountry = xmlRequest.CreateElement("country");
            xmlSCountry.InnerText = paymentInfo.shippingInfo.Country;
            xmlShippingAddress.AppendChild(xmlSCountry);

            XmlElement xmlSPhone = xmlRequest.CreateElement("phone");
            xmlSPhone.InnerText = paymentInfo.shippingInfo.Phone;
            xmlShippingAddress.AppendChild(xmlSPhone);

            XmlElement xmlSCompany = xmlRequest.CreateElement("company");
            xmlSCompany.InnerText = paymentInfo.shippingInfo.Company;
            xmlShippingAddress.AppendChild(xmlSCompany);

            XmlElement xmlSAddress2 = xmlRequest.CreateElement("address2");
            xmlSAddress2.InnerText = paymentInfo.shippingInfo.Address2;
            xmlShippingAddress.AppendChild(xmlSAddress2);

            XmlElement xmlSFax = xmlRequest.CreateElement("fax");
            xmlFax.InnerText = paymentInfo.shippingInfo.Fax;
            xmlShippingAddress.AppendChild(xmlSFax);


            xmlAddCustomer.AppendChild(xmlShippingAddress);

            xmlRequest.AppendChild(xmlAddCustomer);


            string responseFromServer = sendXMLRequest(xmlRequest, isProduction);
            //Console.WriteLine(responseFromServer);
            XmlReader responseReader = XmlReader.Create(new StringReader(responseFromServer));
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(responseReader);
            XmlNodeList response = xDoc.GetElementsByTagName("result");
            if (response[0].InnerText.Equals("1"))
            {
                XmlNodeList formUrl = xDoc.GetElementsByTagName("form-url");
                responseReader.Close();
                sendCreditCardDetails(formUrl[0].InnerText, paymentInfo.CCNumber, paymentInfo.CCEXp, paymentInfo.CCV);
            }


        }

        public static string sendXMLRequest(XmlDocument xmlRequest, bool isProduction = false)
        {
            try
            {
                string uri = isProduction == true ? getProductionUrl() : getStagingUrl();

                WebRequest req = WebRequest.Create(uri);
                req.Method = "POST";        // Post method
                req.ContentType = "text/xml";     // content type
                                                  // Wrap the request stream with a text-based writer
                StreamWriter writer = new StreamWriter(req.GetRequestStream());
                // Write the XML text into the stream

                xmlRequest.Save(writer);

                writer.Close();
                // Send the data to the webserver
                WebResponse rsp = req.GetResponse();

                Stream dataStream = rsp.GetResponseStream();
                // Open the stream using a StreamReader 
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
                rsp.Close();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
                return null;
            }


        }

        static void sendCreditCardDetails(string url, string CCNumber, string ExpiryDate, string CCV, bool isNewCustomer = true)
        {
            try
            {
                if (isNewCustomer)
                {
                    using (WebClient client = new WebClient())
                    {

                        NameValueCollection vals = new NameValueCollection();
                        vals.Add("billing-cc-number", CCNumber);
                        vals.Add("billing-cc-exp", ExpiryDate);
                        vals.Add("billing-cvv", CCV);

                        client.UploadValues(url, vals);
                    }
                }
                else
                {
                    using (WebClient client = new WebClient())
                    {
                        NameValueCollection vals = new NameValueCollection();
                        client.UploadValues(url, vals);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public static PaymentStatus getPaymentstatus(string tokenId, bool isProduction = false)
        {

            //MessageBox.Show(Request["token-id"]);
            XmlDocument xmlRequest = new XmlDocument();

            XmlDeclaration xmlDecl = xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");

            XmlElement root = xmlRequest.DocumentElement;
            xmlRequest.InsertBefore(xmlDecl, root);


            XmlElement xmlCompleteTransaction = xmlRequest.CreateElement("complete-action");

            XmlElement xmlApiKey = xmlRequest.CreateElement("api-key");

            xmlApiKey.InnerText = isProduction == true ? getProductionKey() : getStagingKey();

            xmlCompleteTransaction.AppendChild(xmlApiKey);


            XmlElement xmlTokenId = xmlRequest.CreateElement("token-id");
            xmlTokenId.InnerText = tokenId;
            xmlCompleteTransaction.AppendChild(xmlTokenId);


            xmlRequest.AppendChild(xmlCompleteTransaction);


            string responseFromServer = sendXMLRequest(xmlRequest, isProduction);
            PaymentStatus ps = new PaymentStatus();
            XmlReader responseReader = XmlReader.Create(new StringReader(responseFromServer));
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(responseReader);
            XmlNodeList response = xDoc.GetElementsByTagName("result");
            XmlNodeList responseText = xDoc.GetElementsByTagName("result-text");
            XmlNodeList responseCode = xDoc.GetElementsByTagName("result-code");
            XmlNodeList transactionId = xDoc.GetElementsByTagName("transaction-id");
            XmlNodeList amount = xDoc.GetElementsByTagName("amount");
            XmlNodeList customervaultid = xDoc.GetElementsByTagName("customer-vault-id");
            XmlNodeList firstName = xDoc.GetElementsByTagName("first-name");
            XmlNodeList lastName = xDoc.GetElementsByTagName("last-name");
            XmlNodeList address = xDoc.GetElementsByTagName("address1");
            XmlNodeList city = xDoc.GetElementsByTagName("city");
            XmlNodeList state = xDoc.GetElementsByTagName("state");
            XmlNodeList zip = xDoc.GetElementsByTagName("postal");
            XmlNodeList country = xDoc.GetElementsByTagName("country");
            XmlNodeList phone = xDoc.GetElementsByTagName("phone");
            XmlNodeList email = xDoc.GetElementsByTagName("email");
            XmlNodeList ccNumber = xDoc.GetElementsByTagName("cc-number");
            XmlNodeList ccExp = xDoc.GetElementsByTagName("cc-exp");
            XmlNodeList companyName = xDoc.GetElementsByTagName("company");
            XmlNodeList orderId = xDoc.GetElementsByTagName("order-id");
            XmlNodeList orderDesc = xDoc.GetElementsByTagName("order-description");
            XmlNodeList poNumber = xDoc.GetElementsByTagName("po-number");
            XmlNodeList customField = xDoc.GetElementsByTagName("merchant-defined-field-1");
            XmlNodeList customField2 = xDoc.GetElementsByTagName("merchant-defined-field-2");
            XmlNodeList customField3 = xDoc.GetElementsByTagName("merchant-defined-field-3");
            XmlNodeList customField4 = xDoc.GetElementsByTagName("merchant-defined-field-4");


            responseReader.Close();
            ps.result = response.Count > 0 ? response[0].InnerText.ToString() : "";
            ps.resulttext = responseText.Count > 0 ? responseText[0].InnerText.ToString() : "";
            ps.resultcode = responseCode.Count > 0 ? responseCode[0].InnerText.ToString() : "";
            ps.transactionid = transactionId.Count > 0 ? transactionId[0].InnerText.ToString() : "";
            ps.amount = amount.Count > 0 ? amount[0].InnerText.ToString() : "";
            ps.customervaultid = customervaultid.Count > 0 ? customervaultid[0].InnerText.ToString() : "";
            ps.CCNumber = ccNumber.Count > 0 ? ccNumber[0].InnerText.ToString() : "";
            ps.CCEXp = ccExp.Count > 0 ? ccExp[0].InnerText.ToString() : "";
            ps.FirstName = firstName.Count > 0 ? firstName[0].InnerText.ToString() : "";
            ps.Address1 = address.Count > 0 ? address[0].InnerText.ToString() : "";
            ps.LastName = lastName.Count > 0 ? lastName[0].InnerText.ToString() : "";
            ps.City = city.Count > 0 ? city[0].InnerText.ToString() : "";
            ps.State = state.Count > 0 ? state[0].InnerText.ToString() : "";
            ps.Zip = zip.Count > 0 ? zip[0].InnerText.ToString() : "";
            ps.Country = country.Count > 0 ? country[0].InnerText.ToString() : "";
            ps.Phone = phone.Count > 0 ? phone[0].InnerText.ToString() : "";
            ps.Email = email.Count > 0 ? email[0].InnerText.ToString() : "";
            ps.OrderId = orderId.Count > 0 ? orderId[0].InnerText.ToString() : "";
            ps.OrderDesc = orderDesc.Count > 0 ? orderDesc[0].InnerText.ToString() : "";
            ps.PoNumber = poNumber.Count > 0 ? poNumber[0].InnerText.ToString() : "";
            ps.CustomField = customField.Count > 0 ? customField[0].InnerText.ToString() : "";
            ps.CustomField2 = customField2.Count > 0 ? customField2[0].InnerText.ToString() : "";
            ps.TransCode = customField3.Count > 0 ? customField3[0].InnerText.ToString() : "";
            ps.CustomField3 = customField4.Count > 0 ? customField4[0].InnerText.ToString() : "";

            return ps;
        }

        public static string getStagingUrl()
        {
            return "https://secure.networkmerchants.com/api/v2/three-step";
        }

        public static string getProductionUrl()
        {
            return "https://secure.nmi.com/api/v2/three-step";
            // return "https://secure.networkmerchants.com/api/v2/three-step";
        }

        public static string getStagingKey()
        {
            return "2F822Rw39fx762MaV7Yy86jXGTC7sCDy";
        }

        public static string getProductionKey()
        {
            //return System.Configuration.ConfigurationManager.AppSettings["NMIProdKey"];
            return "Tb5r6es7pxhrhu6sV6X8vb375dExkQnB";
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            //
            if (!this._disposed)
            {
                if (disposing)
                {
                    //
                }
                _disposed = true;
            }
        }

        public static PaymentStatus DirectPost(NMIdirectPostModel model, bool isProduction = false)
        {
            try
            {
                string uri = "https://secure.networkmerchants.com/api/transact.php";
                String username = "demo";
                String password = "password";
                if (isProduction)
                {
                    username = "CheapestEmail";
                    password = "FanLight227";
                }
                String strPost = "username=" + username;
                strPost += "&password=" + password;
                strPost += "&firstname=" + model.firstname + "&lastname=" + model.lastname;
                strPost += "&address1=" + model.address1 + "&city=" + model.city + "&state=" + model.state;
                strPost += "&zip=" + model.zip + "&payment=creditcard&type=sale";
                strPost += "&amount=" + model.amount + "&ccnumber=" + model.ccnumber + "&ccexp=" + model.ccexp + "&cvv=" + model.cvv;

                WebRequest req = WebRequest.Create(uri);
                req.Method = "POST";        // Post method
                req.ContentType = "application/x-www-form-urlencoded";
                req.ContentLength = strPost.Length;
                // Wrap the request stream with a text-based writer
                StreamWriter writer = new StreamWriter(req.GetRequestStream());
                // Write the XML text into the stream
                writer.Write(strPost);
                writer.Close();
                // Send the data to the webserver
                WebResponse rsp = req.GetResponse();

                Stream dataStream = rsp.GetResponseStream();
                // Open the stream using a StreamReader 
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
                rsp.Close();

                string[] responseArr = responseFromServer.Split('&');
                PaymentStatus ps = new PaymentStatus();

                foreach (string oResponse in responseArr)
                {
                    string[] sResponse = oResponse.Split('=');
                    if (sResponse != null && sResponse[0] == "response")
                    {
                        ps.result = sResponse[1];
                    }
                    else if (sResponse != null && sResponse[0] == "responsetext")
                    {
                        ps.resulttext = sResponse[1];
                    }
                    else if (sResponse != null && sResponse[0] == "response_code")
                    {
                        ps.resultcode = sResponse[1];
                    }
                    else if (sResponse != null && sResponse[0] == "transactionid")
                    {
                        ps.transactionid = sResponse[1];
                    }
                    else if (sResponse != null && sResponse[0] == "authcode")
                    {
                        ps.TransCode = sResponse[1];
                    }
                }
                return ps;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message);
                return null;
            }
        }

        public static PaymentStatus DeclineTransaction(bool isProduction = false)
        {
            XmlDocument xmlRequest = new XmlDocument();

            XmlDeclaration xmlDecl = xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");

            XmlElement root = xmlRequest.DocumentElement;
            xmlRequest.InsertBefore(xmlDecl, root);
            XmlElement xmlDeclinePayment = xmlRequest.CreateElement("void");


            XmlElement xmlApiKey = xmlRequest.CreateElement("api-key");
            xmlApiKey.InnerText = isProduction ? getProductionKey() : getStagingKey();
            xmlDeclinePayment.AppendChild(xmlApiKey);


            XmlElement xmlTransaction = xmlRequest.CreateElement("transaction-id");
            xmlTransaction.InnerText = "4883854892";
            xmlDeclinePayment.AppendChild(xmlTransaction);

            xmlRequest.AppendChild(xmlDeclinePayment);

            string responseFromServer = sendXMLRequest(xmlRequest, isProduction);
            PaymentStatus ps = new PaymentStatus();
            return ps;
      


}


    }
}
