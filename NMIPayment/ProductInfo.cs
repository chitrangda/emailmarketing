﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMIPayment
{
    class ProductInfo
    {
        public string ProductCode { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public string UnitOfMeasure { get; set; }

        public double TotalAmount { get; set; }

        public double DiscountAmount { get; set; }

        public double TaxAmount { get; set; }

        public double TaxRate { get; set; }

    }
}
