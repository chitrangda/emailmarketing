﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMIPayment
{
   public class OrderInfo
    {
        public double Amount { get; set; }

        public string Currency { get; set; }

        public string OrderId { get; set; }

        public string OrderDesc { get; set; }

        public double TaxAmount { get; set; }

        public double ShippingAmount { get; set; }

        public string PoNumber { get; set; }

        public string CustomField { get; set; }

        public string CustomField2 { get; set; }

        public string CustomField3 { get; set; }


    }
}
