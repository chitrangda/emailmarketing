﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMIPayment
{
    public class PaymentInfo
    {
        public string CustomerVaultId { get; set; }

        public OrderInfo orderInfo { get; set; }

        public ShippingInfo shippingInfo { get; set; }

        public BillingInfo billingInfo { get; set; }

        public string NavigateUrl { get; set; }

        public string CCNumber { get; set; }

        public string CCEXp { get; set; }

        public string CCV { get; set; }

        public string result { get; set; }

        public string resulttext { get; set; }

        public string resultcode { get; set; }

        public string transactionid { get; set; }

        public string amount { get; set; }

        public string TransCode { get; set; }

    }
}
