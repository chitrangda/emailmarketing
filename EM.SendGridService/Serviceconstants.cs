﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace EM.SendGridService
{
    public class ServiceConstants
    {
        public static string SendGridKey = ConfigurationManager.AppSettings["SendGridKey"].ToString();

        public static int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["MaxSubscriberLimit"].ToString());

        public static string appUrl = ConfigurationManager.AppSettings["appUrl"].ToString();

        public static string FirstName = "{**FIRSTNAME**}";

        public static string LastName = "{**LASTNAME**}";

        public static string PhoneNo = "{**PHONENO**}";

        public static string Email = "{**EMAIL**}";


    }
    public enum CampaginStatus
    {
        Draft,
        Sent,
        Schedule,
        Failed
    }
}
