﻿using EM.Factory;
using EM.Helpers;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EM.SendGridService
{
    public class SendGridEmails
    {
        SendGridClient sendGridClient;
        SendGridAPIHelper sendGridapi = new SendGridAPIHelper();


        public SendGridEmails()
        {
            var apiKey = ServiceConstants.SendGridKey;
            sendGridClient = new SendGridClient(apiKey);
        }

        public async Task<string> SendGridMultipleEmail(string UserId, int? CampaignId, string campaignSubject, string templateContentHtml, string fromCampaign, string fromName, List<Contacts> subscribers, bool isFirstName, bool isLastName, bool isEmail, bool isPhoneNo, SendGridMessage oEmail)
        {

            //LogService.WriteToLog("Message Creation Start", "Campaign_" + CampaignId);
            oEmail = AddSubstitutions(oEmail, subscribers, templateContentHtml, CampaignId.ToString(), UserId, isFirstName, isLastName, isEmail, isPhoneNo);
            if (oEmail != null)
            {
                //LogService.WriteToLog("Email Start Sending", "Campaign_" + CampaignId);
                var response1 = await sendGridClient.SendEmailAsync(oEmail);
                //Console.WriteLine(response1.StatusCode + "-" + response1.Body.ReadAsStringAsync().Result);
                //LogService.WriteToLog(response1.StatusCode.ToString(), "Campaign_" + CampaignId);
                //try
                //{
                //    LogService.WriteToLog(response1.StatusCode.ToString(), "Campaign_" + CampaignId);
                //}
                //catch (Exception e)
                //{
                //    //Console.WriteLine(e.Message);
                //    LogService.WriteToLog(e.Message, "Campaign_exception_" + CampaignId);
                //}
                string emails = string.Join(",", subscribers.Select(r => r.Email).ToArray());
                await UpdateCampaignEmailStatus(CampaignId, emails, response1.StatusCode, response1.Body.ReadAsStringAsync().Result,subscribers.Count());

                if (response1.StatusCode == System.Net.HttpStatusCode.Accepted)
                {

                    //  await UpdateCredits(UserId, subscribers.Count());




                }
                else
                {
                    LogService.WriteToLog(response1.Body.ReadAsStringAsync().Result, "Campaign_" + CampaignId);

                }

                return response1.StatusCode.ToString();

            }
            else
            {
                //LogService.WriteToLog("Email Message is null", "Campaign_" + CampaignId);
                return "";
            }
        }



        public async Task SendGridSingleEmail(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            var from = new EmailAddress(senderEmail, senderName);
            var to = new EmailAddress(recipientEmail, recipientName);
            var oEmail = MailHelper.CreateSingleEmail(from, to, subject, null, contentHtml);
            var response = await sendGridClient.SendEmailAsync(oEmail);
            LogService.WriteToLog(response.StatusCode.ToString(), Id);
        }

        //public async Task<SendGridMessage> CreateMessage(string UserId, string CampaignId, string fromEmail, string fromName, List<Contacts> subscribers, string subject, string htmlBody, bool isFirstName, bool isLastName, bool isEmail, bool isPhoneNo)
        //{
        //    string ipPoolName = null;
        //    var context = new EmailMarketingDbEntities();
        //    var userProfile = context.UserProfiles.Find(UserId);

        //    if (userProfile != null)
        //    {
        //        ipPoolName = userProfile.IP_Pool != null ? userProfile.IP_Pool.Name : null;
        //    }
        //    string address = userProfile.Address1 + " " + userProfile.City + "<br/>" + userProfile.State + " " + userProfile.Country + ", " + userProfile.Zip;
        //    var msg = new SendGridMessage();
        //    LogService.WriteToLog("Function AddSubstitutions Start", "Campaign_" + CampaignId);
        //    msg = AddSubstitutions(msg, subscribers, htmlBody, userProfile.CompanyName, address, CampaignId, UserId, isFirstName, isLastName, isEmail, isPhoneNo);
        //    LogService.WriteToLog("Function AddSubstitution Ends", "Campaign_" + CampaignId);
        //    msg.From = new EmailAddress(fromEmail, fromName);
        //    msg.Subject = subject;
        //    //msg.SetSpamCheck(true);
        //    if (ipPoolName != null && sendGridapi.getIP(ipPoolName) != false)
        //    {
        //        msg.SetIpPoolName(ipPoolName);
        //    }
        //    msg.SetClickTracking(true, false);
        //    msg.HtmlContent = htmlBody;
        //    msg.AddCategories(new List<string>() { UserId, CampaignId });
        //    return await Task.FromResult<SendGridMessage>(msg);

        //}
        public async Task<SendGridMessage> CreateMessage(string UserId, string CampaignId, string fromEmail, string fromName, string subject, string htmlBody, bool isFirstName, bool isLastName, bool isEmail, bool isPhoneNo)
        {

            string ipPoolName = null;
            var context = new EmailMarketingDbEntities();
            var userProfile = context.UserProfiles.Find(UserId);

            if (userProfile != null)
            {
                ipPoolName = userProfile.IP_Pool != null ? userProfile.IP_Pool.Name : null;
            }
            string address = userProfile.Address1 + " " + userProfile.City + "<br/>" + userProfile.State + " " + userProfile.Country + ", " + userProfile.Zip;
            var msg = new SendGridMessage();
            //LogService.WriteToLog("Function AddSubstitutions Start", "Campaign_" + CampaignId);
            //msg = AddSubstitutions(msg, subscribers, htmlBody, userProfile.CompanyName, address, CampaignId, UserId, isFirstName, isLastName, isEmail, isPhoneNo);
            //LogService.WriteToLog("Function AddSubstitution Ends", "Campaign_" + CampaignId);
            msg.From = new EmailAddress(fromEmail, fromName);
            msg.Subject = subject;
            //msg.SetSpamCheck(true);
            if (ipPoolName != null && sendGridapi.getIP(ipPoolName) != false)
            {
                msg.SetIpPoolName(ipPoolName);
            }
            msg.SetClickTracking(true, false);
            msg.HtmlContent = htmlBody;
            msg.AddCategories(new List<string>() { UserId, CampaignId ,"Staging"});
            return await Task.FromResult<SendGridMessage>(msg);

        }

        public SendGridMessage AddSubstitutions(SendGridMessage eMessage, List<Contacts> subscribers, string content, string CampaignId, string UserId, bool isFirstName = false, bool isLastName = false, bool isEmail = false, bool isPhoneNo = false)
        {
            //string footerContent = "<div style='background-color: #e6e6e6;line-height:25px'><p style='font-size: 12px; text-align: center;'>To unsubscribe please <a href='" + url + "'>click here </a></p><p style='font-size: 14px; text-align: center;'>" + address + "</p></div>";
            //eMessage.SetFooterSetting(true, footerContent, "");
            eMessage.Personalizations = new List<Personalization>();


            foreach (var contact in subscribers)
            {
                try
                {
                    var name = contact.FirstName + " " + contact.LastName;
                    string url = string.Format(ServiceConstants.appUrl + "Home/Unsubscribe?id={0}&e={1}&uid={2}", CampaignId, contact.Email, UserId);
                    var personalization = new Personalization();
                    personalization.Tos = new List<EmailAddress>();
                    personalization.Tos.Add(new EmailAddress() { Name = name, Email = contact.Email });
                    personalization.Substitutions = new Dictionary<string, string>();
                    if (isFirstName)
                    {
                        personalization.Substitutions.Add(ServiceConstants.FirstName, contact.FirstName);
                    }
                    if (isLastName)
                    {
                        personalization.Substitutions.Add(ServiceConstants.LastName, contact.LastName);
                    }
                    if (isEmail)
                    {
                        personalization.Substitutions.Add(ServiceConstants.Email, contact.Email);
                    }
                    if (isPhoneNo)
                    {
                        personalization.Substitutions.Add(ServiceConstants.PhoneNo, contact.PhoneNo);
                    }
                    personalization.Substitutions.Add("%tag2%", url);
                    eMessage.Personalizations.Add(personalization);
                }
                catch (Exception ex)
                {
                    LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");
                }
            }


            return eMessage;
        }

        public async Task UpdateCredits(string UserId, int totalSendEmails)
        {
            try
            {
                using (var context = new EmailMarketingDbEntities())
                {
                    var userPlan = context.UserPlans.Where(r => r.UserId == UserId && r.IsActive == true).FirstOrDefault();
                    if (userPlan != null)
                    {
                        userPlan.totalSpendCredits = Convert.ToString(Convert.ToInt32(userPlan.totalSpendCredits) + totalSendEmails);
                        userPlan.totalRemainingCredits = Convert.ToString(userPlan.TotalEmails.Value - totalSendEmails);
                        await context.SaveChangesAsync();

                    }
                }
            }
            catch(Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");

            }
        }
        public async Task UpdateCampaignEmailStatus(int? CampaignId, string subscribers, System.Net.HttpStatusCode code, string description, int SubscriberCount)
        {
            try
            {
                //remove exiting status
                using (var context = new EmailMarketingDbEntities())
                {
                    //var listCampaignEmailStatus = context.CampaignEmailStatus.Where(x => x.CampaignId == CampaignId).ToList();
                    //if (listCampaignEmailStatus != null && listCampaignEmailStatus.Count() > 0)
                    //{
                    //    context.CampaignEmailStatus.RemoveRange(listCampaignEmailStatus);
                    //    await context.SaveChangesAsync();
                    //}


                    //add email sent status

                    CampaignEmailStatu campaign = new CampaignEmailStatu();
                    campaign.CampaignId = CampaignId;
                    campaign.Email = subscribers;
                    campaign.SentDateTime = DateTime.Now;
                    campaign.SendGridResponse = code.ToString();
                    campaign.Description = description;
                    if (code == System.Net.HttpStatusCode.Accepted)
                    {
                        campaign.Status = CampaginStatus.Sent.ToString();
                        var Details = context.Campaigns.Where(x => x.CampaignId == CampaignId).FirstOrDefault();
                        {
                            if (Details != null)
                            {
                                var userPlan = context.UserPlans.Where(r => r.UserId == Details.UserId && r.IsActive == true).FirstOrDefault();
                                if (userPlan != null)
                                {
                                    int subscriberNo = SubscriberCount;
                                    userPlan.totalSpendCredits = Convert.ToString(Convert.ToInt32(userPlan.totalSpendCredits) + subscriberNo);
                                    userPlan.totalRemainingCredits = Convert.ToString(Convert.ToInt32(userPlan.totalRemainingCredits) - SubscriberCount);
                                    await context.SaveChangesAsync();

                                }
                            }
                        }
                    }
                    else
                    {
                        campaign.Status = CampaginStatus.Failed.ToString();
                    }
                    context.CampaignEmailStatus.Add(campaign);
                    await context.SaveChangesAsync();
                    //LogService.WriteToLog("Function UpdateCampaignEmailStatus Start", "Campaign_" + CampaignId);
                }
            }
            catch(Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");

            }
        }
    }

}

