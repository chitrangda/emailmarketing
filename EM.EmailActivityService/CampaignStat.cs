﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;
using EM.Helpers;
using EM.Factory.ViewModels;
using Newtonsoft.Json;

namespace EM.EmailActivityService
{
    class CampaignStat
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        SendGridAPIHelper sendGridApi = new SendGridAPIHelper();
        public async Task Init()
        {
            await GetSendCampaigns();
        }

        public async Task GetSendCampaigns()
        {
            try
            {
                var sendCampaignsList = db.usp_getSentCampaigns().ToList();
                foreach (var oCampaign in sendCampaignsList)
                {
                    Console.WriteLine(oCampaign.CampaignId);
                    await GetCampaignSubscribers(oCampaign.CampaignId, oCampaign.TotalSubscriber);
                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message, "");

            }

        }

        public async Task GetCampaignSubscribers(int? CampaignId, int? TotalSubsciber)
        {
            var tasks = new List<Task>();
            if (TotalSubsciber <= 50)
            {
                string[] subscriberArr = db.usp_getCampaginsSubscriberForService_Updated(CampaignId, 1, 50).Select(r => r.Email).ToArray();
                if (subscriberArr.Length > 0)
                {
                    await GetAndSaveStat(CampaignId, subscriberArr);
                }


            }
            else
            {
                int page = 1;
                while (true)
                {
                    Console.WriteLine(page + " " + DateTime.Now);
                    string[] subscriberArr = db.usp_getCampaginsSubscriberForService_Updated(CampaignId, page, 50).Select(r => r.Email).ToArray();
                    if (subscriberArr.Length > 0)
                    {
                        tasks.Add(await Task.Factory.StartNew(async () => await GetAndSaveStat(CampaignId, subscriberArr)));
                        page++;

                    }
                    else
                    {
                        break;
                    }
                }
                try
                {
                    Task.WaitAll(tasks.ToArray());
                }
                catch (AggregateException e)
                {
                    for (int j = 0; j < e.InnerExceptions.Count; j++)
                    {
                        LogService.WriteToLog(e.InnerExceptions[j].ToString(), "Campaign_" + CampaignId.Value.ToString());
                    }
                }
            }
        }

        public async Task GetAndSaveStat(int? CampaignId, string[] emailArr)
        {
            try
            {
                var response = await sendGridApi.getEmailActivityByCampaign(CampaignId.Value.ToString(), emailArr, 50);
                if (response != null && response != "")
                {
                    var responseData = JsonConvert.DeserializeObject<EmailActivityViewModel>(response);
                    foreach (var oData in responseData.messages)
                    {
                        try
                        {
                            using (var db2 = new EmailMarketingDbEntities())
                            {
                                var oEmailActivity = db2.EmailActivities.SingleOrDefault(r => r.CampaignId == CampaignId && r.to_email == oData.to_email);
                                if (oEmailActivity != null)
                                {
                                    oEmailActivity.clicks_count = oData.clicks_count;
                                    oEmailActivity.opens_count = oData.opens_count;
                                    oEmailActivity.last_event_time = oData.last_event_time;
                                    await db2.SaveChangesAsync();
                                }
                                else
                                {
                                    oEmailActivity = new EmailActivity();
                                    oEmailActivity.CampaignId = CampaignId;
                                    oEmailActivity.to_email = oData.to_email;
                                    oEmailActivity.from_email = oData.from_email;
                                    oEmailActivity.subject = oData.subject;
                                    oEmailActivity.status = oData.status;
                                    oEmailActivity.opens_count = oData.opens_count;
                                    oEmailActivity.clicks_count = oData.clicks_count;
                                    oEmailActivity.last_event_time = oData.last_event_time;
                                    db2.EmailActivities.Add(oEmailActivity);
                                    await db2.SaveChangesAsync();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogService.WriteToLog(ex.Message, "Campaign_" + CampaignId.Value.ToString());
                        }
                    }


                }
            }
            catch(Exception ex)
            {
                LogService.WriteToLog(ex.Message, "");

            }
        }

    }
}
