﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Industries.aspx.cs" Inherits="EM.StaticWeb.Industries" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid header-industries">
        <h1>Industries</h1>
        <p class="nomargin">Various Industries are using <span class="text-bold">Cheapest<span style="color: #ffba00;">eMail</span></span></p>
    </div>

    <div class="container-fluid padding-top-30 padding-bottom-50">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <p>Cheapest<span class="text-blue" >eMail</span> has worked with businesses and organizations representing diverse industries. Each client and each industry presents particular challenges. Cheapest<span class="text-blue" >eMail</span> takes the time to understand our clients businesses, their industries and the competitive landscape.</p>
                    <h2 class="margin-top-40 margin-bottom-40">Here are some of the key industries using Cheapest<span class="text-blue" >eMail</span></h2>
                </div>
            </div>
            <div class="row margin-bottom-50">
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/restaurants-icon.png"></p>
                    <p>Restaurants</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/bars-nightclub-icon.png"></p>
                    <p>Bars &amp; Nightclubs</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/church-icon.png"></p>
                    <p>Churches &amp; Social Groups</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/college-icon.png"></p>
                    <p>College/Universities</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/real-estate.png"></p>
                    <p>Real Estate</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/non-profit-ngo.png"></p>
                    <p>Non-Profit / NGO</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/banking.png"></p>
                    <p>Banking Industries</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/financial.png"></p>
                    <p>Financial Companies</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/medical.png"></p>
                    <p>Medical &amp; Healthcare</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/placement.png"></p>
                    <p>Placement Companies</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/ecommerce.png"></p>
                    <p>ECommerce Industries</p>
                </div>
                <div class="col-md-3 padding-bottom-40 text-center">
                    <p class="margin-bottom-7">
                        <img src="images/forex.png"></p>
                    <p>Forex/Trade</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Section 6 Start -->
  <%--  <div class="section-eight">
        <span class="section_eight_bg">
            <img class="img-fluid bottom-img" src="images/email-marketing.png"></span>
        <div class="container">
            <h2 class="h2 text-center text-white">Contact our support team today.</h2>
            <h5 class="h5 mrg-b30 text-center text-white">Live customer solutions is available 4AM-8PM ET Mon-Fri and 9AM-5PM ET Sat-Sun.</h5>

            <div class="row">
                <div class="col-md-4 padding-top-20">
                    <img src="images/phone-icon.png" class="img-fluid margin-right-15" align="left">
                    <span class="text-white nomargin text-bold text-20">Call Us</span>
                    <br>
                    <span class="text-white">Toll Free: +1 888-351-8398</span>
                </div>
                <div class="col-md-4 padding-top-20">
                    <img src="images/live-chat.png" class="img-fluid margin-right-15" align="left">
                    <span class="text-white nomargin text-bold text-20">Live Chat: <span class="text-warning">Online</span></span>
                    <br>
                    <span class="text-white">Chat with our customer solutions team during our available hours.</span>
                </div>
                <div class="col-md-4 padding-top-20">
                    <img src="images/email-icon.png" class="img-fluid margin-right-15" align="left">
                    <span class="text-white nomargin text-bold text-20">Email Us</span>
                    <br>
                    <span class="text-white">Email us any time, you'll receive a prompt and thorough response.</span>
                </div>
            </div>


        </div>
    </div>--%>

    <!-- End Section 6 -->

    <!-- Section 7 Start -->
   <%-- <div class="section-seven">
        <div class="container">
            <div class="row">
                <ul class="companies-list">
                    <li>
                        <img src="images/macdonal.jpg" alt=""></li>
                    <li>
                        <img src="images/bmw.jpg" alt=""></li>
                    <li>
                        <img src="images/harley-davidson.jpg" alt=""></li>
                    <li>
                        <img src="images/honda.jpg" alt=""></li>
                    <li>
                        <img src="images/LAX.jpg" alt=""></li>
                    <li>
                        <img src="images/msuzuki.jpg" alt=""></li>
                    <li>
                        <img src="images/edible.jpg" alt=""></li>
                    <li>
                        <img src="images/hrblockl.jpg" alt=""></li>
                    <li>
                        <img src="images/secured.jpg" alt=""></li>
                </ul>
            </div>
        </div>
    </div>--%>
    <!-- End Section 7 -->
    <!-- End Section 7 -->
</asp:Content>
