﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="SuccessStories.aspx.cs" Inherits="EM.StaticWeb.SuccessStories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid header-success-story">
        <h1>Learn from the Success of Others</h1>
        <span>Read how people like you have changed their lives simply by doing things they love.<br>
            You'll find stories about Cheapest<span class="text-blue">eMail</span> users building, launching and growing their businesses.</span>
    </div>

    <div class="container">
        <div class="row margin-top-50">
            <div class="col-md-6 margin-bottom-30 testimonial-bg-home">
                <p class="padding-left-32">Cheapest<span class="text-blue" >eMail</span> offers high deliverability and exceptional customer solutions. Anytime we've had an issue or question, they've been quick to offer support and even send resources with instructions...We've realized the importance of creating auto responders to continue educating our list of subscribers. While it's great to add people to a list, you have to keep in contact with them so that you stay top-of-mind, with relevant content.</p>
                <p class="padding-left-32">
                    <img src="images/user1.jpg" class="rounded-circle float-left margin-right-15">
                    <p class="text-bold nomargin">Christian K., IBM</p>
                    <p>Fremont, CA</p>
                </p>
            </div>
            <div class="col-md-6 margin-bottom-30 testimonial-bg-home">
                <p class="padding-left-32">The customer service is amazing. I can pick up the phone and I get through quickly to someone who is incredibly knowledgeable. Often they create a solution I couldn't have anticipated.</p>
                <p class="padding-left-32">
                    <img src="images/testmonial-1.jpg" class="rounded-circle float-left margin-right-15">
                    <p class="text-bold nomargin">John C., Smart Business Revolution</p>
                    <p>Austin, TX</p>
                </p>
            </div>
            <div class="col-md-6 margin-bottom-30 testimonial-bg-home">
                <p class="padding-left-32">I love how easy Cheapest<span class="text-blue" >eMail</span> is to use. The ready made templates are fantastic and make my emails look stunning and professional. It is incredibly easy to manage multiple lists, sign up forms, run campaigns, schedule broadcasts and track conversions.</p>
                <p class="padding-left-32">
                    <img src="images/user2.jpg" class="rounded-circle float-left margin-right-15">
                    <p class="text-bold nomargin">Jeanine L., TheBusinessRef</p>
                    <p>Los Angeles, CA</p>
                </p>
            </div>
            <div class="col-md-6 margin-bottom-30 testimonial-bg-home">
                <p class="padding-left-32">The thing I like best is the customized sign up boxes you can create. Everyone offers the newsletter templates but the customized signups are key to integrating into sites. Plus, I love the javascript versions so when a change is made on the form it automatically shows up on the website without having to add it again.</p>
                <p class="padding-left-32">
                    <img src="images/user3.jpg" class="rounded-circle float-left margin-right-15">
                    <p class="text-bold nomargin">Gene S., Sansom Media, LLC</p>
                    <p>Oakland, CA</p>
                </p>
            </div>
            <div class="col-md-6 margin-bottom-30 testimonial-bg-home">
                <p class="padding-left-32">Cheapest<span class="text-blue" >eMail</span> offers high deliverability and exceptional customer solutions. Anytime we've had an issue or question, they've been quick to offer support and even send resources with instructions...We've realized the importance of creating auto responders to continue educating our list of subscribers. While it's great to add people to a list, you have to keep in contact with them so that you stay top-of-mind, with relevant content.</p>
                <p class="padding-left-32">
                    <img src="images/user4.jpg" class="rounded-circle float-left margin-right-15">
                    <p class="text-bold nomargin">Penny Albritton</p>
                    <p>Fremont, CA</p>
                </p>
            </div>
            <div class="col-md-6 margin-bottom-30 testimonial-bg-home">
                <p class="padding-left-32">The customer service is amazing. I can pick up the phone and I get through quickly to someone who is incredibly knowledgeable. Often they create a solution I couldn't have anticipated.</p>
                <p class="padding-left-32">
                    <img src="images/user5.jpg" class="rounded-circle float-left margin-right-15">
                    <p class="text-bold nomargin">Priscilla Alexis Canales</p>
                    <p>Austin, TX</p>
                </p>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
</asp:Content>

