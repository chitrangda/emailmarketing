﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="EM.StaticWeb.TermOfUse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    <div class="container-fluid header-about">
        <h1>Cheapest<span class="text-warning" style="font-size: 60px;">eMail</span></h1>
        <span>Term Of Use</span>
    </div>--%>
    <div class="container-fluid header-about">
        <h1>Term Of Use</h1>
    </div>
    <div class="container-fluid padding-top-30 padding-bottom-50">

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="text-center" style="font-size: 14px;">
                        BEFORE YOU CHECK THE BOX AND CLICK ON THE “I ACCEPT” OR “I AGREE” BUTTON, CAREFULLY READ THE TERMS AND CONDITIONS OF THIS AGREEMENT. BY CLICKING ON THE “I ACCEPT” OR “I AGREE” BUTTON OR USING THE CHEAPEST EMAIL SERVICES IN ANY MANNER (INCLUDING BY SENDING ANY EMAIL THROUGH THE CHEAPEST EMAIL SOFTWARE PLATFORM), YOU ARE AGREEING TO BE BOUND BY AND ARE BECOMING A PARTY TO THIS AGREEMENT. IF YOU DO NOT AGREE TO ALL OF THE TERMS OF THIS AGREEMENT, PLEASE DO NOT USE OUR SERVICES.
                                    WE RESERVE THE RIGHT TO CHANGE THE TERMS OF THIS AGREEMENT IN THE FUTURE AND ANY CHANGES WILL APPLY TO YOUR USE OF THE OUR SERVICES AFTER THE DATE OF SUCH CHANGE.
                    </p>
                    <ol class="orderList term-of-use" style="font-size: 13px; line-height: 18px;">
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>ACCEPTANCE OF TERMS</strong></span>
                            <br>
                            Cheapest Email, LLC, an Illinois limited liability company (hereinafter referred to as "Cheapest Email", "we," "us" or "our"), 
                                    makes this website, http://cheapestemail.com (the "Site"), including all information, documents, communications, files, images, text, 
                                    videos, audio files, graphics, software and products available through the Site (collectively, the "Materials") and all services 
                                    operated by Cheapest Email and third parties through the Site (collectively, the "Services"), available for your use subject to the terms and conditions 
                                    set forth in this document and any changes to this document that Cheapest Email may publish from time to time (collectively, the "Terms of Use" or "Agreement"). 
                                    The term “Services” as used in this Agreement includes the Cheapest Email software platform which enables users to utilize the system to send out email in large 
                                    volume to their selected recipients. "Cheapest Email Content" as used in this Agreement means all content created and provided by Cheapest Email on or through the Site. 
                                    "User Content" means all submissions you make to or through Cheapest Email or the Site to be transmitted via our email platform. The terms "you" and "yours" refers to all users of the Site, 
                                    and it also includes (for all purposes, including indemnification and representations) the company and/or entity on whose behalf you are using the Site or sending emails through the Site or our Services.
                                    <br>
                            <br>
                            Cheapest Email reserves the right to change the Terms of Use and other guidelines or rules posted on the Site from time to time at its sole discretion.
                                    Your continued use of the Site, or any Materials or Services accessible through it, after such notice has been posted constitutes your acceptance of the changes. 
                                    Your use of the Site will be subject to the most current version of the Terms of Use, rules and guidelines posted on the Site at the time of such use. 
                                    You should periodically check these Terms of Use to view the then current terms. 
                                    If you breach any of the Terms of Use, we may terminate your authorization to use this Site.
                                    <br>
                            <br>
                            BY ACCESSING AND USING THIS WEBSITE; COMPLETING ANY REGISTRATION PROCESS ASSOCIATED WITH CHEAPEST EMAIL; 
                                    RECEIVING A PASSWORD FOR SERVICE; OR OPTING IN TO ANY CHEAPEST EMAIL SERVICES AND/OR CLIENT SERVICES; 
                                    YOU ARE STATING THAT YOU AGREE THAT YOU HAVE READ, UNDERSTAND, AND ACCEPT ALL OF THE TERMS AND CONDITIONS CONTAINED IN THIS AGREEMENT. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>COMPLIANCE WITH APPLICABLE LAWS AND REGULATIONS</strong></span><br>
                            By utilizing the Services or making use of the Site in any manner (including to send out emails through the system), 
                                    you are expressly representing that you are and will remain in full and absolute compliance with the The Controlling the Assault of Non-Solicited Pornography And Marketing Act (“CAN-SPAM Act”) 
                                    and all related regulations. You are hereby representing that you are familiar with and are (and will remain) in full compliance with the applicable provisions of the CAN-SPAM Act. 
                                    You also represent that you will remain informed of any amendments or changes to the CAN-SPAM Act and its regulations and will remain fully compliant with any such future amendments. 
                                    You will likewise ensure that all emails contain a proper “Unsubscribe” link to enable recipients to opt out of receiving future such emails. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>SERVICE LIMITATIONS</strong></span><br>
                            You understand that the Cheapest Email Services provided through the Site comprise of an email software platform.
                                    You understand and agree that Cheapest Email provides no advertising strategy services or marketing services.
                                    We do not review, approve or assist you with the creation or drafting of your emails. You, as the platform user, will fully control: 
                                    (i) the drafting and creation of emails, (ii) their content, (iii) the recipients to whom such emails will be sent; and 
                                    (iv) the timing of when the emails will be sent. We serve merely as a conduit and carrier of your emails. 
                                    Consequently, we will not assist you with the creation of emails, nor will we review or approve your emails.
                                    It is entirely your obligation and duty to ensure that your practices and activities are in full compliance with applicable laws or regulations, 
                                    and including the CAN-SPAM Act and any laws and regulations applicable to email marketing. In sum, you understand and agree that you are entirely 
                                    responsible for any applicable laws and regulations (including the CAN-SPAM Act) relating to advertising and emailing.
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>ENFORCEMENT</strong></span><br>
                            If for any reason a court of competent jurisdiction finds any provision of these Terms of Use, 
                                    or portion thereof, to be unenforceable, that provision shall be enforced to the maximum extent 
                                    permissible so as to effect the intent of the parties as reflected by that provision, and the
                                    remainder of the Terms of Use shall continue in full force and effect. 
                                    Any failure by Cheapest Email to enforce or exercise any provision of these Terms of Use or related right shall not constitute a waiver of that right or provision. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>ELIGIBILITY</strong></span><br>
                            You must be at least 18 years old to be eligible to use the Services (21 years of age for designated venues where the age of majority is 21).  
                                    By sending any emails through the Cheapest Email Services, you are representing that you are at least 18. You further certify that you will not send email in
                                    violation of the CAN-SPAM Act. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>INTELLECTUAL PROPERTY; LIMITED LICENSE
                                        TO USERS
                            </strong></span>
                            <br>
                            The Materials and Services on this Site, as well as their selection and arrangement, are protected by copyright, trademark, patent, and/or other 
                                    intellectual property laws, and any unauthorized use of the Materials or Services at this Site may violate such laws and these Terms of Use. 
                                    Except as expressly provided herein, Cheapest Email does not grant any express or implied rights to use the Materials and Services. 
                                    You agree not to copy, republish, frame, reverse engineer, or create derivative works based on the Site, its Materials, or its Services 
                                    or their selection and arrangement, except as expressly authorized herein. In addition, you agree not to use any data mining, robots, 
                                    or similar data gathering and extraction methods in connection with the Site. 
                                    <br>
                            <br>
                            In addition to the Materials and Services offered by Cheapest Email, this Site may also make available materials, 
                                    information, and services provided by third parties (collectively, the "Third Party Services"). 
                                    The Third Party Services may be governed by separate license agreements that accompany such services. 
                                    Cheapest Email offers no guarantees and assumes no responsibility or liability of any type with respect 
                                    to the Third Party Services or products. You agree that you will not hold Cheapest Email responsible or 
                                    liable with respect to the Third Party Services or seek to do so. 
                                    <br>
                            <br>
                            Except as expressly indicated to the contrary elsewhere on this Site, you may view, download, and print the Cheapest Email Content available on this Site subject to the following conditions: 
                                    <br>
                            <br>
                            <ol>
                                <li>The Cheapest Email Content may not be modified or altered in any way. 
                                </li>
                                <li>You may not remove any copyright or other proprietary notices contained in the Cheapest Email Content. 
                                </li>
                                <li>Cheapest Email reserves the right to revoke the authorization 
                                            to use the Services and to view, download, 
                                            and print the Cheapest Email Content available on this Site at any time, and any such 
                                            use shall be discontinued immediately upon notice from Cheapest Email.
                                </li>
                                <li>The rights granted to you constitute a license and not a transfer of title.</li>
                            </ol>
                            <br>
                            The rights specified above to view, download, and print the Cheapest Email Content available on this Site are not applicable to the design or layout of this Site. 
                                    Elements of this Site are protected by trade dress and other laws and may not be copied or imitated in whole or in part. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>TRADEMARK INFORMATION</strong></span><br>
                            The trademarks, logos, and service marks, including the "Cheapest Email" trademark ("Marks") displayed on this Site are the property of Cheapest Email or other third parties. 
                                    You are not permitted to use the Marks without the prior written consent of Cheapest Email or such third party that may own the Marks. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>LICENSE TO EXPRESS TEXT FOR USER CONTENT</strong></span><br>
                            By submitting any User Content (such as email drafts to be transmitted to your database of recipients) to or through the Site you grant Cheapest Email the 
                                    following type of license: For User Content that is intended to be saved on the Site or transmitted through the Services,
                                    you grant Cheapest Email a worldwide, royalty-free, non-exclusive license to modify (but only for purposes of formatting, 
                                    maintenance, or Site administration) and reproduce such User Content in the manner directed by you. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>DIGITAL MILLENNIUM COPYRIGHT ACT</strong></span><br>
                            If you are a copyright owner or an agent thereof and believe that any User Content or other content infringes upon your copyrights, you may submit a
                                    notification pursuant to the Digital Millennium Copyright Act ("DMCA") by providing us with the following information in
                                    writing (see 17 U.S.C 512(c)(3) for further detail): A physical or electronic signature of a person authorized to act on 
                                    behalf of the owner of an exclusive right that is allegedly infringed; Identification of the copyrighted work claimed to 
                                    have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a 
                                    representative list of such works at that site; Identification of the material that is claimed to be infringing or to be 
                                    the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably
                                    sufficient to permit the service provider to locate the material; Information reasonably sufficient to permit us to contact you, 
                                    such as an address, telephone number, and, if available, an electronic mail; A statement that you have a good faith belief that 
                                    use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and A statement 
                                    that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of 
                                    the owner of an exclusive right that is allegedly infringed. You may send the notification to cesupport@cheapestemail.com
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>PROHIBITED COMMUNICATIONS</strong></span><br>
                            You may submit/email only User Content to/through the Site, that is (a) owned by you, (b) submitted with the express permission of the owner or 
                                    within the scope of the license to such content, or (c) in the public domain. 
                                    You are prohibited from transmitting emails through the Cheapest Email platform that 
                                    contain any unlawful, threatening, harassing, libelous, offensive, defamatory, obscene, or 
                                    pornographic materials, or other materials that would violate any law or the rights of others, 
                                    including, without limitation, laws against copyright infringement, and rights of privacy and publicity. 
                                    Violation of these restrictions may result in denial of or limitations on access by you to this Site. 
                                    You agree that you will not use Cheapest Email Services to spam recipients. Cheapest Email may (but is not obligated to) monitor your 
                                    conduct, to determine whether you are violating the terms and conditions of this Agreement. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>USER CONDUCT</strong></span><br>
                            In using the Site, including all Services and Materials available through it, you agree: not to disrupt or interfere with any other user's enjoyment of the Site or affiliated or linked sites; not to upload, post, or otherwise transmit through the Site any viruses or other harmful, disruptive, or destructive files; not to create a false identity; not to use or attempt to use another's account, password, service, or system without authorization from Cheapest Email; not to disrupt or interfere with the security of, or otherwise cause harm to, the Site, or any Services, Materials, system resources, accounts, passwords, servers, or networks connected to or accessible through the Site or any linked sites. You understand and agree that sending unsolicited email or spam through the Site is expressly prohibited by these terms. As the user, you are required to accept total responsibility for your behavior and conduct while using the Services.  
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>MANAGING CONTENT AND COMMUNICATIONS</strong></span><br>
                            Cheapest Email reserves the right, in its sole discretion, to delete or remove User Content from the Site and to restrict, suspend, or terminate your access to all or part of this Site, at any time without prior notice or liability. Cheapest Email may, but is not obligated to, monitor or review outgoing and incoming email. To the maximum extent permitted by law, Cheapest Email will have no liability related to your User Content. We may, for example, delete all your User Content (including all email drafts, lists of contacts/recipients and all records stored on our systems) without any notice or liability to you. You are therefore advised to backup all such information. Cheapest Email disclaims all liability with respect to the misuse, loss, modification, or unavailability of any User Content. You agree and represent that you are entirely responsible for all User Content that you upload or transmit/send through the Cheapest Email platform. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>USER COMMENTS AND FEEDBACK</strong></span><br>
                            Any comments, feedback, notes, messages, ideas, suggestions or other communications (collectively, "Comments") sent by you to us shall be and remain the exclusive property of Cheapest Email. Your submission of any such Comments shall constitute an assignment to Cheapest Email of all-worldwide rights, titles and interests in all copyrights and other intellectual property rights in the Comments. We will be entitled to use, reproduce, disclose, publish and distribute any Comments you submit for any purpose whatsoever, without restriction and without compensating you in any way. You agree not to submit any Comments that may be submitted in violation of law or any agreement or obligation to keep the content of such Comments confidential. Cheapest Email expressly disclaims any interest in any Comments that you are not authorized to submit. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>PASSWORDS</strong></span><br>
                            You understand that you are solely responsible for maintaining the confidentiality of your password. You shall be solely and fully responsible for all activities that occur under your Username and Password. Cheapest Email shall not be responsible for any loss, claim or other liability that may arise from the unauthorized use of any username, mobile number, and/or password. If a password is lost or stolen, it is your responsibility to notify Cheapest Email so that the missing password can be deactivated and a new one assigned. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>RESERVATION OF RIGHT TO REFUSE USERS/SERVICE</strong></span><br>
                            Cheapest Email reserves the right to refuse or terminate Services granted to you for any reason whatsoever, at our sole discretion and without notice to you. Cheapest Email shall have no liability with respect to such termination of services except to refund prepaid and unused service fees, if any.
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>RESALE OF SERVICE</strong></span><br>
                            You agree not to resell the Service, use of, or access to the Service. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>WARRANTIES AND DISCLAIMERS</strong></span><br>
                            Except as expressly provided otherwise in a written agreement between you and Cheapest Email or you and a third party with respect to such party's materials or services, this Site, and all Materials and Services accessible through this Site, including the email software platform, are provided "as is" without warranty of any kind, either express or implied, including, but not limited to, the implied warranties of merchantability or fitness for a particular purpose, or the warranty of non-infringement. Without limiting the foregoing, Cheapest Email makes no warranty that (i) the Services and Materials will meet your requirements; (ii) the Services and Materials will be uninterrupted, timely, secure, or error-free; (iii) the results that may be obtained from the use of the Services or Materials will be effective, accurate, or reliable; (iv) the quality of any Services or Materials obtained or accessible by you through the Site will meet your expectations; and (v) any errors in the Services or Materials obtained through the Site, or any defects in the Site, its Services or Materials, will be corrected. You understand and acknowledge that (i) Cheapest Email does not control, endorse, or accept responsibility for any content, email, products, or services offered by third parties or site users through the Site; (ii) Cheapest Email makes no representation or warranties whatsoever about any such third parties or site users, their content, products, or services; (iii) any dealings you may have with such third parties or site users are at your own risk; and (iv) Cheapest Email shall not be liable or responsible for any content, products, or services offered by third parties or site users. 
                                    <br>
                            <br>
                            By sending emails through our Services, you understand that some emails may not be delivered due to various technical, network, and/or other reasons. 
                                    <br>
                            <br>
                            The use of the Services or the downloading or other use of any Materials through the Site is done at your own discretion and risk and with your agreement that you will be solely responsible for any damage to your computer system, loss of data, or other harm that results from such activities. Cheapest Email assumes no liability for any computer virus or other similar software code that is downloaded to your computer from the Site or in connection with any Services or Materials offered through the Site. No advice or information, whether oral or written, obtained by you from Cheapest Email or through or from the Site shall create any warranty not expressly stated in these Terms of Use. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>PERSONAL INFORMATION AND PRIVACY</strong></span><br>
                            You understand and agree that we may disclose information about you if we have a good faith belief that we are required to do so by law or legal process, to respond to claims, or to protect the rights, property, or safety of Cheapest Email or others. Please refer to our Privacy Policy for more information about the manner in which we protect and use your information. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>LIMITATION OF LIABILITY</strong></span><br>
                            In no event, including, without limitation, negligence, shall Cheapest Email, its subsidiaries, affiliates, agents, officers, directors, employees, partners, or suppliers be liable to you or any third party for any special, punitive, incidental, indirect, or consequential damages of any kind, or any damages whatsoever, including, without limitation, those resulting from your violation of this Agreement, your violation of the CAN-SPAM Act, loss of use, data, or profits, whether or not Cheapest Email has been advised of the possibility of such damages, and on any theory of liability, arising out of or in connection with the use of or the inability to use this Site, its Services, or Materials, the statements or actions of any third party on or through the Site, any dealings with other users or other third parties, any unauthorized access to or alteration of your transmissions or data, any information that is sent or received or not sent or received, any failure to store or loss of data, files, or other content, any Services available through the Site that are delayed or interrupted, or any website referenced or linked to or from this Site.  
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>INDEMNIFICATION</strong></span><br>
                            You agree to defend, indemnify and hold harmless Cheapest Email, its independent contractors, service providers and consultants, and their respective owners, directors, employees and agents, from and against any third party claims, damages, loss, liability, costs and expenses (including, but not limited to, reasonable attorney's fees) arising out of your use of the  Site or the Cheapest Email Services, including without limitation any actual or threatened suit, demand, or claim made against Cheapest Email or its employees, agents, owners, directors, independent contractors, service providers, or consultants, arising out of or relating to your conduct, your violation of this Agreement, your violation of the CAN-SPAM Act and its regulations, or your violation of the rights of any third party or any law respecting third parties. Without limiting the foregoing, you understand and agree that you will be solely responsible, and fully indemnify Cheapest Email, for any violations relating to CAN-SPAM Act and applicable regulations resulting from your use of the Site or its Services. If your violation or alleged violation causes us harm or requires us to defend ourselves in a legal (or administrative) action, you will fully indemnify us and reimburse us for all expenses, losses and injuries resulting from such action, including attorney fees and court costs relating to our defense of such action and the cost of collection (including attorney fees and court costs). 
                                    <br>
                            <br>
                            You are solely responsible for any legal liability arising out of or relating to (1) the emails, and/or (2) any product and/or service which your email recipients can utilize or access through the email you send to them. You represent and warrant that the your emails comply with Cheapest Email and industry standards and policies; and that the use, reproduction, distribution, or transmission of the emails will not violate any criminal laws or any rights of any third parties, including, but not limited to, such violations as infringement or misappropriation of any copyright, patent, trademark, trade secret, music, image, or other proprietary or property right, false advertising, unfair competition, defamation, invasion of privacy or rights, violation of any anti-discrimination law or regulation, or any other right of any person or entity. You agree to jointly and severally indemnify Cheapest Email and to hold Cheapest Email harmless from any and all liability, loss, damages, claims, or causes of action, and expense of any nature (including reasonable legal fees and expenses that may be incurred by Cheapest Email), arising out of or related to Cheapest Email performance under this agreement, the copying, printing, distributing, or publishing of your email, and/or your breach of any of the foregoing representations and warranties. You grant Cheapest Email the right to use, reproduce, and distribute your email. You agree to request that Cheapest Email be listed as an additional insured on any policy issued to you pursuant to which there could be coverage for any of the forms of legal liability described in this paragraph. You shall not include any advertising via Cheapest Email Services that is misleading, unfair or deceptive. All advertising via Cheapest Email shall comply with all applicable laws and regulations. You shall indemnify Cheapest Email and hold Cheapest Email harmless against all loss, liability, damage and expense of any nature (including legal fees) arising out of your breach of this clause or this Agreement.
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>ASSIGNMENT</strong></span><br>
                            You may not assign any of your rights or delegate any obligations hereunder, in whole or in part, whether voluntarily or by operation of law, without the prior written consent of Cheapest Email Any such purported assignment or delegation by you without the appropriate prior written consent of Cheapest Email will be null and void and of no force or effect. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;">
                            <span style="text-decoration: underline"><strong>APPLICABLE LAW</strong></span><br>
                            These Terms shall be governed by and construed in accordance with the laws of the State of Illinois, without resort to its conflict of law provisions. You agree that any action at law or in equity arising out of or relating to this Agreement or your use of the Site or Service shall be filed only in the state and federal courts located in Cook County, Illinois and you hereby irrevocably and unconditionally consent and submit to the exclusive jurisdiction of such courts over any suit, action or proceeding arising out of or relating to this Agreement or your use of this Site or Services. 
                        </li>
                    </ol>

                </div>

            </div>
        </div>
    </div>
</asp:Content>
