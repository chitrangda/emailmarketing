﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="EM.StaticWeb.AboutUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid header-about">
        <h1>Cheapest<span class="text-warning" style="font-size: 60px;">eMail</span></h1>
        <span>Cheapest<span class="text-blue">eMail</span> is a fast growing email marketing automation platform</span>
    </div>

    <div class="container-fluid padding-top-30 padding-bottom-50">
        <div class="container">
            <h2 class="text-center text-bold text-35 margin-bottom-30 margin-top-20">About Us</h2>
            <p class="text-center">It helps millions of customers from small e-commerce shops to big online retailers & more other industries. Find your audience, build a relationship and sell your products. Reach your business goals with <span class="text-bold">Cheapest<span class="text-blue">eMail's</span></span> email marketing and automation platform.</p>
            <p class="text-center"><span class="text-bold">Cheapest<span class="text-blue">eMail</span></span> is a leading marketing platform for small business. We democratize cutting-edge marketing technology for small businesses, creating innovative products that empower our customers to grow. Millions of people and businesses around the world trust <span class="text-bold">Cheapest<span class="text-blue">eMail</span></span> to publish the right content, to the right person, at the right place, at the right time.</p>

            <h2 class="text-center text-bold text-35 margin-bottom-30 margin-top-40">Dedicated Customer Support</h2>
            <p class="text-center"><span class="text-bold">Cheapest<span class="text-blue">eMail</span>'s</span> team is growing quickly to support thousands of new customers every day. As a privately owned, founder-led company, we're able to work fast and respond to our customers’ needs without anything getting in our way. We invest in engineering, research, customer support, and great design. We also invest in our employees.</p>
            <p class="text-center">Our employees enjoy the stability of a time-tested and successful product and the excitement of developing new products and features. We’re always looking for smart, creative people to join our team.</p>
            <p class="text-center margin-top-40"><a href="<%=ConfigurationManager.AppSettings["pricing_url"] %>" class="btn btn-outline-primary btn-lg text-uppercase padding-left-40 padding-right-40">See Our Plans</a></p>
        </div>
    </div>

</asp:Content>

