﻿<%@ Page Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="EM.StaticWeb.PrivacyPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <div class="container-fluid header-about">
        <h1>Cheapest<span class="text-warning" style="font-size: 60px;">eMail</span></h1>
        <span>Privacy Policy</span>
    </div>--%>
    <div class="container-fluid header-about">
        <h1>Privacy Policy</h1>
    </div>
    <div class="container-fluid padding-top-30 padding-bottom-50">

        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <p class="text-center" style="font-size: 14px;">
                        Privacy Policy discloses the privacy practices for the website located at www.CheapestEmail.com (the “site”) operated by Cheapest Email, LLC, an Illinois limited liability company. We are committed to protecting your privacy online. Please read the information below to learn the following regarding your use of this site:
                                 
                    </p>
                    <ul class="orderList term-of-use" style="font-size: 13px; line-height: 18px;">
                        <li style="margin: 5px 0px 20px 0px;"><a href="#what-info-collect">What information do we collect from you?</a></li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#where-info-collect">Where do we collect information from you and how do we use it?</a></li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#with-whom-share">With whom do we share your information?</a> </li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#personally-identifiable-info">How can you update, correct or delete your Personally Identifiable Information? </a></li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#what-choices-collect">What are your choices regarding collection, use and distribution of your information?</a></li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#what-security-precautions">What security precautions are in place to protect against the loss, misuse or alteration of your information?</a></li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#do-we-use-cookies">Do we use “cookies”?</a></li>
                        <li style="margin: 5px 0px 20px 0px;"><a href="#what-about-privacy">What should you know about privacy policies and data collection at any third party websites accessible from our site?
                        </a>
                            <br>
                            <br />
                            You acknowledge that this Privacy Policy is part of our website Terms of Use, and by accessing or using our site, you agree to be bound by all of its terms and conditions. If you do not agree to these terms, please do not access or use the site.
                            <br>
                            <br />
                            We reserve the right to change this Privacy Policy at any time. Such changes, modifications, additions or deletions shall be effective immediately upon notice thereof, which may be given by means including, but not limited to issuing an e-mail to the e-mail address listed by registered users or posting the revised Policy on this page. You acknowledge and agree that it is your responsibility to maintain a valid e-mail address as a registered user, review this site and this Policy periodically and to be aware of any modifications. Your continued use of the site after such modifications will constitute your: (a) acknowledgment of the modified Policy; and (b) agreement to abide and be bound by the modified Policy.
                        </li>
                    </ul>
                    <ol class="orderList term-of-use" style="font-size: 13px; line-height: 18px;">
                        <li style="margin: 5px 0px 20px 0px;"><a name="what-info-collect"></a>
                            <span style="text-decoration: underline"><strong>What information do we collect from you?</strong></span>
                            <br />
                            <br />
                            In order to better provide you with our services, we collect two types of information about our users: Personally Identifiable Information and Non-Personally Identifiable Information. Our primary goal in collecting information from you is to provide you with a smooth, efficient, and customized experience while using our site.
                          <br />
                            <br />
                            <span><strong>Personally Identifiable Information:</strong></span>
                            This refers to information that lets us know the specifics of who you are. This information may include, for example, your first and last name, email address, physical address, and your payment information. 
                                <br />
                            <br />
                            <span><strong>Non-Personally Identifiable Information:</strong></span>
                            This refers to information that does not by itself identify a specific individual. We gather certain information about you based upon where you visit on our site in several ways. This information is compiled and analyzed on both a personal and an aggregated basis. This information may include the site’s Uniform Resource Locator (“URL”) that you just came from, which URL you go to next, what browser you are using, what type of device you are using, and your Internet Protocol (“IP”) address. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;"><a name="with-whom-share"></a>
                            <span style="text-decoration: underline"><strong>With whom do we share your information?</strong></span>
                            <br />
                            <br />
                            We do not collect any Personally Identifiable Information about you unless you voluntarily provide it to us. You provide certain Personally Identifiable Information to us when you register for a user account to utilize our services, for example.
In addition, we may also collect, or our third party ad server and/or content server may collect, certain Non-Personally Identifiable Information. We use your IP address to diagnose problems with our servers, software, to administer our site and to gather demographic information.
          <br />
                            <br />
                            We will use your Personally Identifiable Information to provide our services to you. We will also use Personally Identifiable Information to enhance the operation of our site and software application, improve our marketing and promotional efforts, statistically analyze site use, improve our product and service offerings, and customize our site’s content, layout, and services. We may use Personally Identifiable Information to deliver information to you and to contact you regarding administrative notices and regarding product or service offerings which we believe may be of interest to you. You will be given an opportunity to opt out of receiving promotional materials from us. We may also use Personally Identifiable Information to troubleshoot problems and enforce our agreements with you, including our Site Terms of Use and this Private Policy. While you may opt-out of receiving our promotional communications by taking the measures outlined in such communications, you may not opt-out of receiving administrative communications.  
                        </li>
                        <li style="margin: 5px 0px 20px 0px;"><a name="where-info-collect"></a>
                            <span style="text-decoration: underline"><strong>Where do we collect information from you and how do we use it?</strong></span>
                            <br />
                            <br />
                            We do not sell, trade, or rent your Personally Identifiable Information to others. We may utilize the services of third parties and provide some of our services through contractual arrangements with affiliates, services providers, partners and other third parties, and to facilitate such arrangements we reserve the right to share with such third parties Personally Identifiable Information, provided that such third party agree to not share the information. For example, we utilize third party credit card processing services and will share your credit card information with such services providers to facilitate payment  
                            <br />
                            <br />
                            Occasionally we may be required by law enforcement or judicial authorities to provide Personally Identifiable Information to the appropriate governmental authorities. We will disclose Personally Identifiable Information upon receipt of a court order, subpoena, or to cooperate with a law enforcement investigation. We fully cooperate with law enforcement agencies in identifying those who use our services for illegal activities. We reserve the right to report to law enforcement agencies any activities that we in good faith believe to be unlawful.  
                            <br />
                            <br />
                            We may also provide Non-Personally Identifiable Information about our customers’ traffic patterns, and related site information to third party advertisers, but these statistics do not include Personally Identifiable Information.
                        </li>

                        <li style="margin: 5px 0px 20px 0px;"><a name="personally-identifiable-info"></a>
                            <span style="text-decoration: underline"><strong>How can you update or correct your Personally Identifiable Information?</strong></span>
                            <br />
                            <br />
                            We believe you should have the ability to access and edit the Personally Identifiable Information that you have provided to us. You may change any of your Personally Identifiable Information in your account online at any time by logging on to your account and editing your information.
                            <br />
                            <br />
                            We encourage you to promptly update your Personally Identifiable Information if it changes. You may ask to have the information on your account deleted or removed; however, because we may keep track of past transactions, you may not be able to delete information associated with past transactions on the site. In addition, it may be impossible to completely delete your information without some residual information because of backups.</li>

                        <li style="margin: 5px 0px 20px 0px;"><a name="what-choices-collect"></a>
                            <span style="text-decoration: underline"><strong>What are your choices regarding collection, use, and distribution of your information?</strong></span>
                            <br />
                            <br />
                            We may occasionally send you direct mail (including e-mail) about products and services that we feel may be of interest to you. You will have the ability to opt out of receiving such communications from us by following the instructions in such communications. 
                        </li>
                        <li style="margin: 5px 0px 20px 0px;"><a name="what-security-precautions"></a>
                            <span style="text-decoration: underline"><strong>What security precautions are in place to protect against the loss, misuse, or alteration of your information?</strong></span>
                            <br />
                            <br />
                            At our site you can be assured that your Personally Identifiable Information is secure, consistent with current industry standards. The importance of security for all Personally Identifiable Information associated with our users is of utmost concern to us. Your Personally Identifiable Information is protected in several ways. Access by you to your Personally Identifiable Information is available through a password and unique customer ID selected by you. This password is encrypted. We recommend that you do not divulge your password to anyone. In addition, your Personally Identifiable Information resides on a secure server that only our selected personnel and contractors have access to via password. We encrypt your Personally Identifiable Information and thereby prevent unauthorized parties from viewing such information when it is transmitted to us.<br />
                            Occasionally we may be required by law enforcement or judicial authorities to provide Personally Identifiable Information to the appropriate governmental authorities. We will disclose Personally Identifiable Information upon receipt of a court order, subpoena, or to cooperate with a law enforcement investigation. We fully cooperate with law enforcement agencies in identifying those who use our services for illegal activities. We reserve the right to report to law enforcement agencies any activities that we in good faith believe to be unlawful.  
                            <br />
                            <br />
                            Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your Personally Identifiable Information, you acknowledge that: (a) there are security and privacy limitations of the Internet which are beyond our control; (b) the security, integrity and privacy of any and all information and data exchanged between you and us through this site cannot be guaranteed; and (c) any such information and data may be viewed or tampered with in transit by a third party.
                        </li>
                        <li style="margin: 5px 0px 20px 0px;"><a name="do-we-use-cookies"></a>
                            <span style="text-decoration: underline"><strong>Do we use “cookies”?</strong></span>
                            <br />

                            When you use our site we may store cookies on your computer or device in order to facilitate and customize your use of our site. A cookie is a small data text file, which a website stores on your device that can later be retrieved to identify you to us. The cookies make your use of the site easier, make the site run more smoothly and help us to maintain a secure site. You are always free to decline our cookies if your browser permits, but some parts of our site  may not work properly in that case.  

                        </li>

                        <li style="margin: 5px 0px 20px 0px;"><a name="what-about-privacy"></a>
                            <span style="text-decoration: underline"><strong>What should you know about privacy policies and data collection at any third party sites accessible from our site?</strong></span>
                            <br />
                            <br />
                            Our site may feature links to third parties. Except as otherwise discussed in this Privacy Policy, this document only addresses the use and disclosure of information we collect from you. Other sites accessible through our site or App have their own privacy policies and data collection, use and disclosure practices. Please consult each site’s privacy policy. 
                             <br />
                            <br />
                            If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us by sending us an email to cesupport@cheapestemail.com.
                             <br />
                            <br />
                            This Privacy Policy is effective as of 6/1/2019.
                        </li>



                    </ol>


                </div>
            </div>
        </div>
    </div>
   
</asp:Content>

