﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="Features.aspx.cs" Inherits="EM.StaticWeb.Features" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid header-features">
        <h1>Cheapest<span class="text-blue" style="font-size: 60px;">eMail</span>Features</h1>
        <span>Create and send modern, professional email newsletters to your audience with our powerful email marketing and automation tools.</span>
        <p class="margin-top-20"><a href="<%=ConfigurationManager.AppSettings["contactus_url"] %>" class="btn btn-outline-light btn-lg text-uppercase text-bold">Schedule A Demo</a></p>
    </div>

    <div class="container-fluid features padding-bottom-40">
        <div class="container">
            <h2 class="text-center text-bold text-35">Features you won’t find anywhere else</h2>
            <div class="row">
                <div class="col-sm-6 col-md-4 text-center padding-bottom-30">
                    <a href="#drag-drop-editor">
                        <p>
                            <img src="images/drag-drop-feature.png" alt=""></p>
                        <h4 class="text-22 text-bold">Drag & Drop Editor</h4>
                        <p>Design professional, high-converting emails with our easy-to-use interface</p>
                    </a>
                </div>


                <div class="col-sm-6 col-md-4 text-center padding-bottom-30">
                    <a href="#performance-reporting">
                        <p>
                            <img src="images/performance-reporting.png" alt=""></p>
                        <h4 class="text-22 text-bold">Performance Reporting</h4>
                        <p>Track and analyze campaign results for more profitable data-driven marketing</p>
                    </a>
                </div>

                <div class="col-sm-6 col-md-4 text-center padding-bottom-30">
                    <a href="#automation">
                        <p>
                            <img src="images/automation-icon.png" alt=""></p>
                        <h4 class="text-22 text-bold">Automation</h4>
                        <p>Deliver the right message at exactly the right time with automated campaigns</p>
                    </a>
                </div>

                <div class="col-sm-6 col-md-4 text-center padding-bottom-30">
                    <a href="#!">
                        <p>
                            <img src="images/subscriber-management.png" alt=""></p>
                        <h4 class="text-22 text-bold">Subscriber Management</h4>
                        <p>Utilize a variety of tools and services to grow and manage a healthy email list</p>
                    </a>
                </div>

                <div class="col-sm-6 col-md-4 text-center padding-bottom-30">
                    <a href="#!">
                        <p>
                            <img src="images/template-icon.png" alt=""></p>
                        <h4 class="text-22 text-bold">HTML Email Templates</h4>
                        <p>Customize and send professional email newsletters with ease</p>
                    </a>
                </div>

                <div class="col-sm-6 col-md-4 text-center padding-bottom-30">
                    <a href="#!">
                        <p>
                            <img src="images/deliverability-icon.png" alt=""></p>
                        <h4 class="text-22 text-bold">Email Deliverability Rate</h4>
                        <p>Get industry-leading deliverability and ensure your emails make it to the inbox</p>
                    </a>
                </div>

            </div>

            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="text-bold text-35">Additional Features</h1>
                </div>
                <div class="col-sm-6 col-md-4">
                    <ul class="features-list">
                        <li>Pre-built design template library</li>
                        <li>Responsive email layouts</li>
                        <li>HTML editor option</li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-4">
                    <ul class="features-list">
                        <li>Instant Spam Check</li>
                        <li>Customizable merge fields</li>
                        <li>Complete send time control</li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-4">
                    <ul class="features-list">
                        <li>Subscriber engagement score</li>
                        <li>Segment by custom fields</li>
                        <li>Performance Reporting</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <a name="drag-drop-editor"></a>
    <div class="container-fluid features-details margin-top-25 margin-bottom-20">
        <div class="container padding-top-25 padding-bottom-20">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="text-bold text-35 text-center nomargin">Drag & Drop Editor</h1>
                    <p class="text-center margin-top-0">
                        <img src="images/gray-line.jpg"></p>
                    <h3 class="text-center">Design and build great looking emails to match any style or brand within minutes.</h3>

                    <ul class="features-details-list">
                        <li>Build your own layout or choose a design from our customizable template library</li>
                        <li>Automatic responsive templates lets you deliver emails designed for all devices</li>
                        <li>Spam Check allows you to test your messaging before ever sending a single email</li>
                        <li>Want even more control? Our HTML builder gives you the option of coding your own custom emails</li>
                    </ul>

                </div>
                <div class="col-md-6">
                    <img src="images/drag-drop-editor.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <a name="automation"></a>
    <div class="container-fluid features-details-gray margin-top-25 margin-bottom-20">
        <div class="container padding-top-25 padding-bottom-20">
            <div class="row">
                <div class="col-md-6">
                    <img src="images/automation-screen.jpg" alt="" class="img-fluid margin-bottom-20">
                </div>
                <div class="col-md-6">
                    <h1 class="text-bold text-35 text-center nomargin">Automation</h1>
                    <p class="text-center margin-top-0">
                        <img src="images/gray-line.jpg"></p>
                    <h3 class="text-center">Whether it’s an engaging welcome series or a perfectly-timed nudge to revisit your site, automated campaigns take your marketing to the next level.</h3>
                    <ul class="features-details-list">
                        <li>Set it and forget it — automation gives you complete control over your campaign send times</li>
                        <li>Our easy-to-use platform lets you build entire time-based campaigns in minutes</li>
                        <li>New to automated marketing? Our Strategic Services team is always happy to help you with your campaigns</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


        <a name="performance-reporting"></a>
    <div class="container-fluid features-details margin-top-25 margin-bottom-20">
        <div class="container padding-top-25 padding-bottom-20">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="text-bold text-35 text-center nomargin">Performance Reporting</h1>
                    <p class="text-center margin-top-0">
                        <img src="images/gray-line.jpg"></p>
                    <h3 class="text-center">Learn what works best (and what doesn’t) for your audience to ensure your campaigns remain profitable.</h3>

                    <ul class="features-details-list">
                        <li>From big picture tracking to keeping an eye on just one individual’s click behavior, our reporting always keeps you informed of the health of your campaigns</li>
                        <li>Real-time data ensures that your decisions are based on the most current information</li>
                        <li>Our support team is always happy to help you make sense of your vital metrics if you need a helping hand</li>
                    </ul>

                </div>
                <div class="col-md-6">
                    <img src="images/performance-reporting-img.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

</asp:Content>