﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="PricingPlans.aspx.cs" Inherits="EM.StaticWeb.PricingPlans" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid pricing-page-bg">
        <div class="container padding-top-70">
            <h1 class="text-center text-bold">Try Cheapest<span class="text-blue" >eMail</span> Free for 30 Days</h1>
            <h4 class="text-center margin-bottom-30">And get all the tools you need to succeed.</h4>
            <div class="row">
                <div class="col-md-4">
                    <img src="images/price-tag.png" class="img-fluid">
                </div>
                <div class="col-md-8">
                    <h4 class="text-bold">What's included in your account:</h4>
                    <ul class="check-list">
                        <li>Access to our email experts who are here 7 days a week to help you grow.</li>
                        <li>Easy-to-use tools to help you create emails, sign up forms and more to engage your subscribers.</li>
                        <li>Industry-leading deliverability to help make sure your emails make it to the inbox.</li>
                        <li>Simple email automation to get the right message to the right person at the right time.</li>
                    </ul>
                    <p class="margin-top-40"><a href="#" class="btn btn-primary padding-15 padding-left-40 padding-right-40 text-uppercase text-bold">Get Started for Free</a></p>
                </div>
            </div>

            <div class="row">
                <div class="col-12 padding-top-40">
                    <h3 class="text-center text-bold margin-top-30">Choose the right plan, at the right price.</h3>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <p class="text-center">Choose the plan that’s right for your business. Whether you’re just getting started with email marketing or well down the path to personalization, we’ve got you covered.</p>
                </div>
                <div class="col-md-2"></div>
            </div>

            <div class="row">
                <div class="col-12 pricing-table margin-top-30 padding-25 padding-bottom-45">
                    <div class="table-responsive">
                        <table class="table table-striped text-16">
                            <thead>
                                <tr>
                                    <th style="width: 224px;">&nbsp;</th>
                                    <th class="text-18 text-bold text-center">$15/mo</th>
                                    <th class="text-18 text-bold text-center">$29/mo</th>
                                    <th class="text-18 text-bold text-center">$49/mo</th>
                                    <th class="text-18 text-bold text-center">$69/mo</th>
                                    <th class="text-18 text-bold text-center">$149/mo</th>
                                    <th class="text-18 text-bold text-center"><a href="#" class="btn btn-outline-primary padding-left-20 padding-right-20">Get a Quote</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-bold">Subscribers <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Pay as you grow and reach big milestones with email."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">0-500</td>
                                    <td class="text-center">501-2,500</td>
                                    <td class="text-center">2,501-5,000</td>
                                    <td class="text-center">5,001-10,000</td>
                                    <td class="text-center">10,001-25,000</td>
                                    <td class="text-center">25,001+</td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Contact Management <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Manage your contacts with name, email, phone number."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Unlimited Emails <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="You use us to send emails, so send as many as you want!"><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Automation <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Save time and delight subscribers with an automated educational course or welcome series."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Segmenting <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="One email doesn't always fit all. Send targeted content to subscribers who want it most."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Analytics <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Improve your emails when you track performance like opens, clicks and more."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Email Scheduling <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="You can draft an email now, then schedule it to be sent automatically at the perfect time."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Customizable Templates <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Choose a basic layout, pre-built theme, or a template based on the message you need to communicate."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Educational Resources <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Keep your email and digital marketing skills sharp with our blog, videos, guides and more."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td class="text-bold">Live Support <a href="#" data-toggle="tooltip" data-placement="right" data-custom-class="tooltip-danger" title="Get instant reply from our support staff at 24x7."><i class="fa fa-question-circle-o text-18" aria-hidden="true"></i></a></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                    <td class="text-center">
                                        <img src="images/check-mark-green.png" class="img-fluid"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-center">
                                        <select class="form-control">
                                            <option value="">$15 / 1 month</option>
                                            <option value="">$11 / 3 months</option>
                                            <option value="">$10 / 6 months</option>
                                        </select>
                                    </td>
                                    <td class="text-center">
                                        <select class="form-control">
                                            <option value="">$15 / 1 month</option>
                                            <option value="">$11 / 3 months</option>
                                            <option value="">$10 / 6 months</option>
                                        </select>
                                    </td>
                                    <td class="text-center">
                                        <select class="form-control">
                                            <option value="">$15 / 1 month</option>
                                            <option value="">$11 / 3 months</option>
                                            <option value="">$10 / 6 months</option>
                                        </select>
                                    </td>
                                    <td class="text-center">
                                        <select class="form-control">
                                            <option value="">$15 / 1 month</option>
                                            <option value="">$11 / 3 months</option>
                                            <option value="">$10 / 6 months</option>
                                        </select>
                                    </td>
                                    <td class="text-center">
                                        <select class="form-control">
                                            <option value="">$15 / 1 month</option>
                                            <option value="">$11 / 3 months</option>
                                            <option value="">$10 / 6 months</option>
                                        </select>
                                    </td>
                                    <td class="text-center"><a href="#" class="btn btn-outline-primary padding-left-20 padding-right-20">Get a Quote</a></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary form-control">Select Plan</button>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary form-control">Select Plan</button>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary form-control">Select Plan</button>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary form-control">Select Plan</button>
                                    </td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary form-control">Select Plan</button>
                                    </td>
                                    <td class="text-center">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <h5 class="text-bold text-center margin-top-15 margin-bottom-0">Quarterly and Annual billing options also available.</h5>
                    <p class="text-center nomargin">You can select monthly, quarterly or annually billing options from the dropdown.</p>

                </div>
            </div>

            <div class="row padding-bottom-50">
                <div class="col-12">
                    <h3 class="text-center margin-top-45 margin-bottom-40 text-bold">Frequently Asked Questions</h3>
                </div>
                <div class="col-md-6">
                    <h5 class="text-bold nomargin text-18">Does the free trial limit my access to features?</h5>
                    <p class="margin-bottom-30">Your free trial includes access to all features.</p>
                    <h5 class="text-bold nomargin text-18">Can I change my plan later?</h5>
                    <p class="margin-bottom-30">Of course! You can change your billing plan at any time in your Control Panel.</p>
                    <h5 class="text-bold nomargin text-18">What payment methods do you offer?</h5>
                    <p class="margin-bottom-30">We accept all major credit cards, PayPal, and (in select countries) direct debit. We also accept checks and money orders.</p>
                    <h5 class="text-bold nomargin text-18">Am I locked into a contract?</h5>
                    <p class="margin-bottom-30">There are no long-term contracts with our monthly plans.</p>
                </div>
                <div class="col-md-6">
                    <h5 class="text-bold nomargin text-18">Are there any setup fees?</h5>
                    <p class="margin-bottom-30">There are no setup fees or hidden charges with any of our account plans. Order your first month for free and get instant access to your new campaign.</p>
                    <h5 class="text-bold nomargin text-18">Do you have pricing for high volume senders?</h5>
                    <p class="margin-bottom-30">Yes. If you want to send a list of more than 25,001 subscribers, please contact support and we will give best discounted price for a high volume plan for you.</p>
                    <h5 class="text-bold nomargin text-18">Do you offer discount on any plan?</h5>
                    <p class="margin-bottom-30">Yes. we are currently offering flat discount 25% on quarterly and 35% on yearly plan.</p>
                </div>

            </div>

        </div>
    </div>


    <!-- Section 6 Start -->
    <div class="section-eight">
        <span class="section_eight_bg">
            <img class="img-fluid bottom-img" src="images/email-marketing.png"></span>
        <div class="container">
            <h2 class="h2 text-center text-white">Contact our support team today.</h2>
            <h5 class="h5 mrg-b30 text-center text-white">Live customer solutions is available 4AM-8PM ET Mon-Fri and 9AM-5PM ET Sat-Sun.</h5>

            <div class="row">
                <div class="col-md-4 padding-top-20">
                    <img src="images/phone-icon.png" class="img-fluid margin-right-15" align="left">
                    <span class="text-white nomargin text-bold text-20">Call Us</span>
                    <br>
                    <span class="text-white">Toll Free: +1 888-351-8398</span>
                </div>
                <div class="col-md-4 padding-top-20">
                    <img src="images/live-chat.png" class="img-fluid margin-right-15" align="left">
                    <span class="text-white nomargin text-bold text-20">Live Chat: <span class="text-warning">Online</span></span>
                    <br>
                    <span class="text-white">Chat with our customer solutions team during our available hours.</span>
                </div>
                <div class="col-md-4 padding-top-20">
                    <img src="images/email-icon.png" class="img-fluid margin-right-15" align="left">
                    <span class="text-white nomargin text-bold text-20">Email Us</span>
                    <br>
                    <span class="text-white">Email us any time, you'll receive a prompt and thorough response.</span>
                </div>
            </div>


        </div>
    </div>

    <!-- End Section 6 -->

    <!-- Section 7 Start -->
    <div class="section-seven">
        <div class="container">
            <div class="row">
                <ul class="companies-list">
                    <li>
                        <img src="images/macdonal.jpg" alt=""></li>
                    <li>
                        <img src="images/bmw.jpg" alt=""></li>
                    <li>
                        <img src="images/harley-davidson.jpg" alt=""></li>
                    <li>
                        <img src="images/honda.jpg" alt=""></li>
                    <li>
                        <img src="images/LAX.jpg" alt=""></li>
                    <li>
                        <img src="images/msuzuki.jpg" alt=""></li>
                    <li>
                        <img src="images/edible.jpg" alt=""></li>
                    <li>
                        <img src="images/hrblockl.jpg" alt=""></li>
                    <li>
                        <img src="images/secured.jpg" alt=""></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Section 7 -->
</asp:Content>

