﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="RefundPolicy.aspx.cs" Inherits="EM.StaticWeb.RefundPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid header-about">
        <h1>Refund Policy?</h1>
    </div>
    <div class="container-fluid padding-top-30 padding-bottom-50">
        <div class="container padding-top-30 padding-bottom-30">
            <p>
                Cheapest Email is a month-to-month, pay-as-you-go service that does not require any contracts.
            This means we do not provide refunds for services already used unless a system malfunction caused a problem or your account was terminated without cause.
            </p>
            <p>
                Your account will be billed every 30 days and you can cancel at any time.
            You won’t be charged again, but you are responsible for any charges already incurred.
            </p>
            <p>
                Simply email us at <a href="mailto:Support@CheapestEmail.com">Support@CheapestEmail.com</a> requesting to cancel your account with your registered email address on file.
            </p>
        </div>
    </div>
</asp:Content>
