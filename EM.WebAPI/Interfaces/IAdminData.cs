﻿using System;
using System.Collections.Generic;
using EM.Factory;

namespace EM.WebAPI.Interfaces
{
    public interface IAdminData
    {
        List<usp_getAdminData_Result> Get(DateTime? FromDate, DateTime? ToDate, int? Year, string Quarter, int? Month, string City, string State, string Zip, string TimeZone, string Find,int? MemberType,bool? Status, int? CampaignType);

    }
}
