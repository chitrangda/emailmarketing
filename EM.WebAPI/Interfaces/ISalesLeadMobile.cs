﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface ISalesLeadMobile
    {
         List<USP_GetLeads_MobileNumList_Result> Get(int leadId);

        bool Post(SalesPortal_Leads_MobileNumList model);

        bool Delete(int leadId);

        SalesPortal_Leads_MobileNumList Get(int leadId, int EmailRowId);

        bool Put(int LeadId, int EmailRowId);
      

    }
}