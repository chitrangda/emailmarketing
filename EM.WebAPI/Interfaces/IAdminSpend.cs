﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IAdminSpend
    {
        List<USP_GetSpendData_Result> Get(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText);
    }
}