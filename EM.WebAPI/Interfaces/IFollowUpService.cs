﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IFollowUpService
    {
        List<getSalesRepFollowUp_Result> Get(string id,DateTime? datefrom,DateTime? dateto, bool? IsCurrent);
        usp_getUserDetails_Result GetClientData(string id);

        bool UpdateFollowUpData(usp_getUserDetails_Result model);

    }
}