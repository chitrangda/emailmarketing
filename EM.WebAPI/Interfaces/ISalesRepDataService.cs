﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface ISalesRepDataService
    {
        usp_getSalesRepTodays_Result Get(string id);

        List<usp_getSalesRepDashboardData_Result> GetSalesRepData(string id);

        List<usp_getSalesRepDashboardData_Result> GetFilterSalesRepData(string id, string key, string value, string clientType,string MemberType);

        List<SalesRepProfile> Get();

        usp_getUserDetails_Result GetClientData(string id);

        bool UpdateClientData(usp_getUserDetails_Result model);

        List<usp_getSalesRepData_Result> GetData(string id,DateTime? FromDate, DateTime? ToDate, int? Year, string Quarter, int? Month, string City, string State, string Zip, string TimeZone, string Find, int? MemberType, bool? Status, int? CampaignType);
    }
}
