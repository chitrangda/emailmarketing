﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;

namespace EM.WebAPI.Interfaces
{
    public interface ITransactionLogService
    {
        AnTransactionLog Get(int id);
        int Post(AnTransactionLog model);
        usp_getTransactionInfo_Result Get(string token);
    }
}
