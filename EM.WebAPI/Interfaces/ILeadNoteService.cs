﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
   public interface ILeadNoteService
    {
        bool Post(SalesPortal_LeadNote SalesPortalLeadNote);

        List<usp_getLeadNoteList_Result> Get(int LeadId);

        bool Delete(int id, string userid);
    }
}
