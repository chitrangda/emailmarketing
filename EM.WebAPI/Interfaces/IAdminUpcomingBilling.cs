﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IAdminUpcomingBilling
    {
      List<usp_getUpComingBillingDetails_Result> Get(DateTime? dateFrom , DateTime? dateTo , int? pageIndex, int? pageSize);

    usp_getUpComingBillingDetails_Result Get(DateTime? dateFrom, DateTime? dateTo, int? pageIndex, int? pageSize,string Id);

        bool Post(usp_getUpComingBillingDetails_Result model);
    }
}