﻿using EM.Factory.ViewModels;
using System.Collections.Generic;

namespace EM.WebAPI.Interfaces
{
    public interface IIndustryService
    {
        List<IndustryViewModel> Get();
    }
}
