﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;

namespace EM.WebAPI.Interfaces
{
    public interface IUserDashboard
    {
        List<usp_getUserDashboardData_Result> Get(string UserId);
        List<usp_getUserDashboardData_Result> Get(string UserId, DateTime? dateFrom, DateTime? dateTo);
        List<usp_getEmailActivity_Result> GetEmailActivity(string UserId, DateTime? dateFrom, DateTime? dateTo);
        List<usp_getSubscriberDetails_Result> Get(string UserId, int PageNumber, int? PageSize);
        List<usp_getUnSubscriberDetails_Result> GetUnsubscriber(string UserId, int PageNumber, int? PageSize);
        List<usp_getAllEmailActivity_Result> GetAllEmailActivity(string UserId, DateTime? dateFrom, DateTime? dateTo);
        List<usp_getDashboardReport_Result> GetDashboardReport(string UserId , int PageNumber, int? PageSize);

    }
}