﻿using EM.Factory;
using System.Collections.Generic;

namespace EM.WebAPI.Interfaces
{
    public interface IAdminDashboardService
    {
        usp_getAdminTodays_Result Get();

        List<usp_getAdminDashboardData_Result> GetAdminData(string Name, string Email, string MobileNumber, string NameOnCard, string MumberType, string ReferralCode, string City, string State,string SalesRep,string MemberType, string ClientType);
    }
}
