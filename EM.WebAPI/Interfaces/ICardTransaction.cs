﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;

namespace EM.WebAPI.Interfaces
{
    public interface ICardTransaction
    {
        List<AnTransactionLog> Get(string UserId);
    }
}
