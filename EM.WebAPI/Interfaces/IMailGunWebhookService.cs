﻿using EM.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IMailGunWebhookService
    {

        Task SaveDeliveredEvents(DeliveredMailGunModel oEventview);

        Task SavePermanentFailedEvents(FailedMailGunModel oEventview);

        Task SaveTempFailedEvents(FailedMailGunModel oEventview);

        Task SaveOpenEvents(OpenClickMailGunModel oEventview);

        Task SaveClickEvents(OpenClickMailGunModel oEventview);

        Task SaveComplainedEvents(ComplainedMailGunModel eventList);
    }
}