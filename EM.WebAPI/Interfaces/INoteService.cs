﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface INoteService
    {
        bool Post(SalesPortalRepNote salesPortalRepNote);

        List<usp_getUserNoteList_Result> Get(string id);

        bool Delete(int id, string userid);
    } 
}