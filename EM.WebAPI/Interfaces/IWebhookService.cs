﻿using EM.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IWebhookService
    {
        Task SaveEvents(EventView eventList);
    }
}