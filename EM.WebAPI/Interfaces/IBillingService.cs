﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface IBillingService
    {
        UserPaymentProfileViewModel Get(int id);

        List<UserPaymentProfileViewModel> Get(string id);

        string Post(UserPaymentProfileViewModel userPaymentProfileViewModel);

        bool Delete(List<int> id);

        bool Delete(int? id);

        bool setPrimary(int? id, string userId);


    }
}
