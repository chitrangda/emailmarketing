﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IPaymentTypeService
    {
        List<PaymentTypeViewModel> Get();

    }
}