﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IManageUserService
    {
        AccountDetailsViewModel getAccountDetails(string userId);

        string Put(AccountDetailsViewModel model);

        usp_getManageBillingDetail_Result Get(string id);
        List<USP_UserActivityLog_Result> getActivityLog(string Userid,DateTime? fromDate, DateTime? ToDate);
    }
}