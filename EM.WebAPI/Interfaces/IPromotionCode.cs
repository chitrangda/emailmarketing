﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IPromotionCode
    {
        List<PromotionCode> Get();

        bool Post(PromotionCode model);

        PromotionCode Get(int id);

        bool Delete(int id);
    }
}