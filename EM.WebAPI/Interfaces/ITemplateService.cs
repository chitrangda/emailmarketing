﻿using EM.Factory;
using EM.Factory.ViewModels;
using System.Collections.Generic;

namespace EM.WebAPI.Interfaces
{
    public interface ITemplateService
    {
        bool Post(TemplateViewModel templateViewModel);
        List<usp_getTemplateList_Result> Get(string id, int? pageNumber, int? pageSize, string search , string orderBy);
        TemplateViewModel Get(int? id , string type);
        bool Delete(List<int> id);

        List<usp_getBeeTemplateList_Result> Get(int? pageNumber, int? pageSize, string search, string orderBy, string version);
    }
}