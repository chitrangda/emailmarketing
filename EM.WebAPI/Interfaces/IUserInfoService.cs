﻿using System.Collections.Generic;
using EM.Factory.ViewModels;
using EM.Factory;

namespace EM.WebAPI.Interfaces
{
    public interface IUserInfoService
    {
        UserInfoViewModel Get(string id);

        List<UserInfoViewModel> Get(string FirstName, string LastName, string MobilePhone, string City, string State, string Country, string Zip, bool? isActive,int? PageSize, int? StartIndex);
       // List<usp_getAllUssersNew_Result> Get(string search, int? PageSize, int? StartIndex);

        bool Put(UserInfoViewModel userInfoViewModel);

        bool Put(string id, bool accountStatus);

        usp_getAuthorization_Result GetAuthorizationInfo(string userId);
    }
}
