﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface IIPPoolService
    {
        List<IP_Pool> Get();

        bool Post(IP_Pool model);

        IP_Pool Get(int id);

        bool Delete(int id);
    }
}
