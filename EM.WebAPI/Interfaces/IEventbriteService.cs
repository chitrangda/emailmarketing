﻿using EM.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface IEventbriteService
    {
        Task CreateList(string EventName, string UserId, string eventId);

        Task CreateAttendeeSubscriber(Profile attendeeProfile, string attendeeId, string eventId, string UserId, string EventbriteUserId);

        Task getEvent(string eventId, string EventbriteUserId);

        Task getAttendee(string eventId, string attendeeId, string EventbriteUserId);

    }
}
