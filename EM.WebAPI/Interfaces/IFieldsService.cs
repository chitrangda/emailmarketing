﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface IFieldsService
    {
        List<FieldsViewModel> Get(int listId);
        bool ActivateDeactivate(List<int> fields, int listId, string action);
    }
}
