﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface ISubscriberService
    {
        List<usp_getSubscriberList_Result> Get(int? listId, string search, int? pageNumber, int? pageSize,string orderBy);
        List<usp_getDeletedSubscriberList_Result> Get1(int? listId, string search, int? pageNumber, int? pageSize, string orderBy);

        SubscriberViewModel Get(int? subscriberId);

        int Post(SubscriberViewModel subscriberViewModel);

        List<SubscriberViewModel> Post(List<SubscriberViewModel> subscriberViewModels);

        List<SubscriberViewModel> Post1(List<SubscriberViewModel> subscriberViewModels);

        List<SubscriberViewModel> GetNotExistSubscribers(List<SubscriberViewModel> subscriberViewModels);

        bool Delete(List<int> id,int ListId);

        bool Delete(int? id);
        bool RecoverDeleteSubscriber(List<int> id , int listId);

        bool Put(List<int> id , int ListId);

        bool Put(int? id, bool isSubsribe,int ListId);

        List<usp_getUnSubscriberList_Result> GetUnsubscribers(int? listId, string search, int? pageNumber, int? pageSize , string orderBy);

        List<usp_getSuppressedSubscribersByListId_Result> GetSuppressedSubscribers(int? listId , string search, int? pageNumber, int? pageSize, string orderBy);

        List<SubscriberViewModel> SuppressedSubscriber(List<SubscriberViewModel> subscriberViewModels);
        List<SubscriberViewModel> EventbriteSyncingAttendees (List<SubscriberViewModel> subscriberViewModels);

    }

    
}
