﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;
using EM.Factory.ViewModels;

namespace EM.WebAPI.Interfaces
{
    public interface ISalesRepService
    {
        SalesRepInfoViewModel Get(string id);
        List<usp_getAllSalesRep_Result> Get(string FirstName, string LastName, int? NoOfClients, double? SpentTotal, bool? ViewRepActivity, bool? CanExport, bool? ExportLead, bool? Manager, bool? IsActive, int? pageIndex, int? pageSize);
        bool Put(SalesRepProfile model);
    }
}
