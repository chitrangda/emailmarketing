﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface ISalesLeadEmail
    {
        List<USP_GetLeadEmail_Result> Get(int leadId);

        bool Post(SalesPortal_Leads_EmailIdList model);

        bool Delete(int leadId);

        SalesPortal_Leads_EmailIdList Get(int leadId, int EmailRowId);

        bool Put(int LeadId, int EmailRowId);
      




    }
}