﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;

namespace EM.WebAPI.Interfaces
{
    public interface IInvoiceService
    {
        string Post(InvoiceViewModel Model);

        InvoiceViewModel getNewInvoice(string userid);

        List<usp_getInvoice_Result> getUSerInvoice(string userid);

        usp_getInvoiceDetails_Result getInvoiceDetails(string invoiceId);
    }
}