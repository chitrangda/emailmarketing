﻿using EM.Factory;
using EM.Factory.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
     public interface IIPAddressService
    {
        List<IP_Address> Get();

        int PostIPAddress(IPAddressViewModel model);

        IP_Address Get(int id);

        List<IP_Address> GetByPoolId(int id);

        bool Delete(int id);

        bool Post(IPAddressViewModel model);
    }
}
