﻿using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface IUserListService
    {
        List<EM.Factory.usp_getUserList_Result> Get(string id, string listName, int pageNumber, int? pageSize, string orderby);
        ListViewModel GetListById(int id);
        bool Post(ListViewModel lstViewModel);
        bool Delete(List<int> id);
        bool RestoreDelete(List<int> id);
        List<ListViewModel> Post(List<ListViewModel> lstViewModels);
        ListViewModel GetRecent(string userid);
        List<EM.Factory.usp_getDeletedUserList_Result> GetDeleted(string id, string listName, int pageNumber, int? pageSize, string orderby);
    }
}
