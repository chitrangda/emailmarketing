﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface ICampaignService
    {

        ListViewModel GetListDetailsByListId(int id);

        CampaignViewModel Post(CampaignViewModel model);

        List<usp_getCampaignList_Result> Get(string id,string Search, int? pageNumber, int? pageSize, string orderBy);

        CampaignViewModel Get(int? id);

        List<string> Get(string userid);

        List<usp_getSendCampaignList_Result> GetSendCampaign(string userid,int? pageNumber, int? pageSize, DateTime? fromDate = null, DateTime? toDate = null);

        List<usp_getCampaignReport_Result> GetCampaigns(string userid, int? pageNumber, int? pageSize, DateTime? fromDate = null, DateTime? toDate = null);

        usp_getSendCampaignList_Result GetSendCampaignDetails(string userid,int? CampaignId);

        List<usp_getCampaignRecipient_Result> GetRecipientsEmails(int CampaignId, int? pageNumber , int? PageSize);

        List<usp_CampaignUnsubscriber_Result> GetUnSubscribeEmails(int CampaignId, String UserId, int? pageNumber, int? PageSize, string EmailSearch);

        List<usp_getCampaignActivityStat_Result> GetCampaignStat(int CamapignId , string Stat ,int PageNumber , int? PageSize , string Email);

        List<usp_getCampaignLinkStat_Result> GetCampaignLinkStat(int CampaignId);

        usp_getActivityStat_Result Stat(int CampaignId, string Email);

        usp_getSingleCampaignReport_Result SingleCampaignReport(string userId , int CampaignId);

        List<usp_getReportChartData_Result> GetReportChart(DateTime? dateFrom, DateTime? dateTo,string aggregated_by,int CampaignId);

        List<usp_getDidNotOpenEmails_Result> GetDidntEmail(int CampaignId,string UserId, int PageNumber, int? PageSize , string EmailSearch);

        List<usp_getSuppressedSubscribers_Result> GetSuppressedEmails(int CampaignId, int PageNumber, int? PageSize, string EmailSearch);

       usp_UnSubscribefromCampaign_Result Put(int id, string email, string uid);

        List<usp_getDeliveredEmails_Result> GetDeliveredEmail(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch);
        List<usp_getRecipientData_Result> GetSubscriberEmail(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch);


    }
}