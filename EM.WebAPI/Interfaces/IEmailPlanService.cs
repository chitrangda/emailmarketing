﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.WebAPI.Interfaces
{
    public interface IEmailPlanService
    {
        List<usp_geAllPlans_Result> Get();
        PlanPricingsViewModel Get(int id);
        usp_getPackageDetail_Result GetPlanByID(int id , string UserId = null);

        List<Plan> GetPlan();

        List<PlanPricingsViewModel> GetPlanPricing(int id);
    }
}
