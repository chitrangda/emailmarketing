﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;
using System.Collections.Generic;

namespace EM.WebAPI.Interfaces
{
    public interface IRepActivity
    {
        List<usp_getSalesRepActivity_Result> Get();
        List<usp_getSalesRepActivity_Result> GetGridData(string RepId, DateTime? From, DateTime? To);
        List<SalesRepProfile> getSaleRepProfile();
    }
}
