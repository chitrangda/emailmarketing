﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Interfaces
{
    public interface IProvidersService
    {
        List<SalesPortal_Providers> Get();

        bool Post(SalesPortal_Providers model);

        SalesPortal_Providers Get(int id);

        bool Delete(int id);

    }
}