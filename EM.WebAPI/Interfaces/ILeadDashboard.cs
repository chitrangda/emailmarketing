﻿using System.Collections.Generic;
using EM.Factory;


namespace EM.WebAPI.Interfaces
{
    public interface ILeadDashboardService
    {
        List<USP_GetLeadDashBoard_Result> Get();
        List<USP_GetLeadDashBoard_Result> Get(string RepId, int? LeadQualityID,
            int? TypeOfLead, int? IndustryId, int? pageIndex, int? pageSize);

        USP_GetLeadDashBoard_Result Get(string RepId, int leadId);

    }
}