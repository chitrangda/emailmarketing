﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class EventTypeModel
    {

        public Nullable<int> CampaignId { get; set; }
        public string email { get; set; }
        public string @event { get; set; }
        public string url { get; set; }
        public string reason { get; set; }
        public Nullable<long> Event_TimeStamp { get; set; }
        public string response { get; set; }
        public string UserId { get; set; }
    }
}