﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class PayloadEventViewModel
    {
        public string api_url { get; set; }
        public Config config { get; set; }
    }
    public class Config
    {
        public string action { get; set; }

        public string endpoint_url { get; set; }
        public string user_id { get; set; }
        public string webhook_id { get; set; }

    }

}