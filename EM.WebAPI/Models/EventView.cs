﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class EventView
    {
        public string email { get; set; }

        public long timestamp { get; set; }

        public string @event { get; set; }

        public string sg_event_id { get; set; }

        public string sg_message_id { get; set; }

        public List<string> category { get; set; }

        public string response { get; set; }

        public string attempt { get; set; }

        public string url { get; set; }

        public string reason { get; set; }


    }
}