﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class ComplainedMailGunModel
    {
        public ComplainedSignature signature { get; set; }

        [JsonProperty(PropertyName = "event-data")]
        public ComplainedEventData eventdata { get; set; }
    }

    public class ComplainedSignature
    {
        public long timestamp { get; set; }

        public string token { get; set; }

        public string signature { get; set; }
    }
    public class ComplainedEventData
    {
        public string @event { get; set; }

        public string id { get; set; }

        public double timestamp { get; set; }

        [JsonProperty(PropertyName = "log-level")]
        public string loglevel { get; set; }

        public string recipient { get; set; }

        public string[] tags { get; set; }

        public string[] campaigns { get; set; }

        [JsonProperty(PropertyName = "user-variables")]
        public List<string> uservariables { get; set; }

        public ComplainedFlags flags { get; set; }

        public ComplainedMessage message { get; set; }

    }

    public class ComplainedFlags
    {
        [JsonProperty(PropertyName = "is-test-mode")]
        public bool istestmode { get; set; }
    }

    public class ComplainedMessage
    {
        public ComplainedHeaders headers { get; set; }

        public string[] attachments { get; set; }

        public int size { get; set; }
    }

    public class ComplainedHeaders
    {
        public string to { get; set; }

        [JsonProperty(PropertyName = "message-id")]
        public string messageid { get; set; }

        public string from { get; set; }

        public string subject { get; set; }
    }
}