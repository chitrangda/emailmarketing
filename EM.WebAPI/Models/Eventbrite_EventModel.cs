﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{

    public class Event
    {
        public Name name { get; set; }

        public Description description { get; set; }

        public string id { get; set; }

        public string url { get; set; }

        public Start start { get; set; }

        public End end { get; set; }

        [JsonProperty(PropertyName = "organization_id")]
        public string organizationid { get; set; }

        public DateTime created { get; set; }

        public DateTime changed { get; set; }

        public int capacity { get; set; }

        [JsonProperty(PropertyName = "capacity_is_custom")]
        public bool capacityiscustom { get; set; }

        public string status { get; set; }

        public string currency { get; set; }

        public bool listed { get; set; }

        public bool shareable { get; set; }

        [JsonProperty(PropertyName = "invite_only")]
        public bool inviteonly { get; set; }

        public bool online_event { get; set; }

        [JsonProperty(PropertyName = "show_remaining")]
        public bool showremaining { get; set; }

        [JsonProperty(PropertyName = "tx_time_limit")]
        public int txtimelimit { get; set; }

        [JsonProperty(PropertyName = "hide_start_date")]
        public bool hidestartdate { get; set; }

        [JsonProperty(PropertyName = "hide_end_date")]
        public bool hideenddate { get; set; }

        public string locale { get; set; }

        [JsonProperty(PropertyName = "is_locked")]
        public bool islocked { get; set; }

        [JsonProperty(PropertyName = "privacy_setting")]
        public string privacysetting { get; set; }

        [JsonProperty(PropertyName = "is_series")]
        public bool isseries { get; set; }

        [JsonProperty(PropertyName = "is_series_parent")]
        public bool isseriesparent { get; set; }

        [JsonProperty(PropertyName = "inventory_type")]
        public string inventorytype { get; set; }

        [JsonProperty(PropertyName = "is_reserved_seating")]
        public bool isreservedseating { get; set; }

        [JsonProperty(PropertyName = "show_pick_a_seat")]
        public bool showpickaseat { get; set; }

        [JsonProperty(PropertyName = "show_seatmap_thumbnail")]
        public bool showseatmapthumbnail { get; set; }

        [JsonProperty(PropertyName = "show_colors_in_seatmap_thumbnail")]
        public bool showcolorsinseatmapthumbnail { get; set; }

        public string source { get; set; }

        [JsonProperty(PropertyName = "is_free")]
        public bool isfree { get; set; }

        public string version { get; set; }

        public string summary { get; set; }

        [JsonProperty(PropertyName = "logo_id")]
        public string logoid { get; set; }

        [JsonProperty(PropertyName = "organizer_id")]
        public string organizerid { get; set; }

        [JsonProperty(PropertyName = "venue_id")]
        public string venue_id { get; set; }

        [JsonProperty(PropertyName = "category_id")]
        public string categoryid { get; set; }

        [JsonProperty(PropertyName = "subcategory_id")]
        public string subcategoryid { get; set; }

        [JsonProperty(PropertyName = "format_id")]
        public string formatid { get; set; }

        [JsonProperty(PropertyName = "resource_uri")]
        public string resourceuri { get; set; }

        [JsonProperty(PropertyName = "is_externally_ticketed")]
        public bool isexternallyticketed { get; set; }

        public string logo { get; set; }



    }

    public class Name
    {
        public string text { get; set; }

        public string html { get; set; }

    }

    public class Description
    {
        public string text { get; set; }

        public string html { get; set; }

    }

    public class Start
    {
        public string timezone { get; set; }

        public DateTime local { get; set; }

        public DateTime utc { get; set; }
    }

    public class End
    {
        public string timezone { get; set; }

        public DateTime local { get; set; }

        public DateTime utc { get; set; }
    }
}







