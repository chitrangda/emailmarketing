﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class FailedMailGunModel
    {
        public FailedSignature signature { get; set; }

        [JsonProperty(PropertyName = "event-data")]
        public FailedEventData eventdata { get; set; }

    }

    public class FailedSignature
    {
        public long timestamp { get; set; }

        public string token { get; set; }

        public string signature { get; set; }
    }

    public class FailedEventData
    {
        public string @event { get; set; }

        public string id { get; set; }

        public double timestamp { get; set; }

        [JsonProperty(PropertyName = "log-level")]
        public string loglevel { get; set; }

        public string severity { get; set; }

        public string reason { get; set; }

        public FailedEnvelope envelope { get; set; }

        public FailedFlags flags { get; set; }

        [JsonProperty(PropertyName = "delivery-status")]
        public FailedDeliverystatus deliverystatus { get; set; }

        public FailedMessage message { get; set; }

        public FailedStorage storage { get; set; }

        public string recipient { get; set; }

        [JsonProperty(PropertyName = "recipient-domain")]
        public string recipientdomain { get; set; }

        public string[] campaigns { get; set; }

        public string[] tags { get; set; }

        [JsonProperty(PropertyName = "user-variables")]
        public List<string> uservariables { get; set; }

    }

    public class FailedEnvelope
    {
        public string transport { get; set; }

        public string sender { get; set; }

        public string targets { get; set; }

        [JsonProperty(PropertyName = "sending-ip")]
        public string sendingip { get; set; }
    }

    public class FailedFlags
    {
        [JsonProperty(PropertyName = "is-routed")]
        public bool isrouted { get; set; }

        [JsonProperty(PropertyName = "is-authenticated")]
        public bool isauthenticated { get; set; }

        [JsonProperty(PropertyName = "is-system-test")]
        public bool issystemtest { get; set; }

        [JsonProperty(PropertyName = "is-test-mode")]
        public bool istestmode { get; set; }

        [JsonProperty(PropertyName = "is-delayed-bounce")]
        public bool isdelayedbounce { get; set; }

        
    }

    public class FailedDeliverystatus
    {
        [JsonProperty(PropertyName = "attempt-no")]
        public int attemptno { get; set; }

        public string message { get; set; }

        public int code { get; set; }

        public string description { get; set; }

        [JsonProperty(PropertyName = "session-seconds")]
        public double sessionseconds { get; set; }

        public bool tls { get; set; }

        [JsonProperty(PropertyName = "mx-host")]
        public string mxhost { get; set; }

        [JsonProperty(PropertyName = "retry-seconds")]
        public int retryseconds { get; set; }

        [JsonProperty(PropertyName = "certificate-verified")]
        public bool certificateverified { get; set; }
  
    }

    public class FailedMessage
    {
        public FailedHeaders headers { get; set; }

        public string[] attachments { get; set; }

        public int size { get; set; }
    }

    public class FailedHeaders
    {
        public string to { get; set; }

        [JsonProperty(PropertyName = "message-id")]
        public string messageid { get; set; }

        public string from { get; set; }

        public string subject { get; set; }
    }

    public class FailedStorage
    {
        public string url { get; set; }

        public string key { get; set; }
    }


}