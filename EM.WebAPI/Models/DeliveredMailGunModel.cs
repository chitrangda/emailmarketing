﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class DeliveredMailGunModel
    {
        public Signature signature { get; set; }

        [JsonProperty(PropertyName = "event-data")]
        public EventData eventdata { get; set; }
    }

    public class Signature
    {
        public long timestamp { get; set; }

        public string token { get; set; }

        public string signature { get; set; }
    }



    public class EventData
    {
        public string @event { get; set; }

        public string id { get; set; }

        public double timestamp { get; set; }

        [JsonProperty(PropertyName = "log-level")]
        public string loglevel { get; set; }

        public string method { get; set; }

        public Envelope envelope { get; set; }

        public Flags flags { get; set; }

        [JsonProperty(PropertyName = "delivery-status")]
        public Deliverystatus deliverystatus { get; set; }

        public Message message { get; set; }

        public Storage storage { get; set; }

        public string recipient { get; set; }

        [JsonProperty(PropertyName = "recipient-domain")]
        public string recipientdomain { get; set; }

        public string[] campaigns { get; set; }

        public string[] tags { get; set; }

        [JsonProperty(PropertyName = "user-variables")]
        public List<string> uservariables { get; set; }

    }

    public class GeoLocation
    {
        public string country { get; set; }


        public string region { get; set; }

        public string city { get; set; }
    }

    public class ClientInfo
    {
        public string clienttype { get; set; }

        public string clientos { get; set; }

        public string devicetype { get; set; }

        public string clientname { get; set; }

        public string useragent { get; set; }


    }

    public class Message
    {
        public Headers headers { get; set; }

        public string[] attachments { get; set; }

        public int size { get; set; }
    }

    public class Headers
    {
        public string to { get; set; }

        [JsonProperty(PropertyName = "message-id")]
        public string messageid { get; set; }

        public string from { get; set; }

        public string subject { get; set; }
    }

    public class Envelope
    {
        public string transport { get; set; }

        public string sender { get; set; }

        [JsonProperty(PropertyName = "sending-ip")]
        public string sendingip { get; set; }

        public string targets { get; set; }
    }
    public class Flags
    {
        [JsonProperty(PropertyName = "is-routed")]
        public bool isrouted { get; set; }

        [JsonProperty(PropertyName = "is-authenticated")]
        public bool isauthenticated { get; set; }

        [JsonProperty(PropertyName = "is-system-test")]
        public bool issystemtest { get; set; }

        [JsonProperty(PropertyName = "is-test-mode")]
        public bool istestmode { get; set; }

    }

    public class Deliverystatus
    {
        public bool tls { get; set; }

        [JsonProperty(PropertyName = "mx-host")]
        public string mxhost { get; set; }

        public int code { get; set; }

        public string description { get; set; }

        [JsonProperty(PropertyName = "session-seconds")]
        public double sessionseconds { get; set; }

        public bool utf8 { get; set; }

        [JsonProperty(PropertyName = "attempt-no")]
        public int attemptno { get; set; }

        public string message { get; set; }

        [JsonProperty(PropertyName = "certificate-verified")]
        public bool certificateverified { get; set; }
    }

    public class Storage
    {
        public string url { get; set; }

        public string key { get; set; }
    }
}
