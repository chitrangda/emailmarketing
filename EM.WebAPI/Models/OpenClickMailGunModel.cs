﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Models
{
    public class OpenClickMailGunModel
    {
        public OpenClickSignature signature { get; set; }

        [JsonProperty(PropertyName = "event-data")]
        public OpenClickEventData eventdata { get; set; }
    }

    public class OpenClickSignature
    {
        public long timestamp { get; set; }

        public string token { get; set; }

        public string signature { get; set; }
    }

    public class OpenClickEventData
    {
        public string @event { get; set; }

        public string id { get; set; }

        public double timestamp { get; set; }

        [JsonProperty(PropertyName = "log-level")]
        public string loglevel { get; set; }

        public string recipient { get; set; }

        public OpenClickGeoLocation geolocation { get; set; }

        public string[] tags { get; set; }

        public string[] campaigns { get; set; }

        [JsonProperty(PropertyName = "user-variables")]
        public List<string> uservariables { get; set; }

        public string ip { get; set; }

        public OpenClickClientInfo openclientinfo { get; set; }

        public OpenClickMessage message { get; set; }

        public string url { get; set; }

    }

    public class OpenClickGeoLocation
    {
        public string country { get; set; }

        public string region { get; set; }

        public string city { get; set; }
    }

    public class OpenClickClientInfo
    {
        [JsonProperty(PropertyName = "client-type")]
        public string clienttype { get; set; }

        [JsonProperty(PropertyName = "client-os")]
        public string clientos { get; set; }

        [JsonProperty(PropertyName = "device-type")]
        public string devicetype { get; set; }

        [JsonProperty(PropertyName = "client-name")]
        public string clientname { get; set; }

        [JsonProperty(PropertyName = "user-agent")]
        public string useragent { get; set; }

    }

    public class OpenClickMessage
    {
        public OpenClickHeaders headers { get; set; }
    }

    public class OpenClickHeaders
    {
        [JsonProperty(PropertyName = "message-id")]
        public string messageid { get; set; }
    }

}