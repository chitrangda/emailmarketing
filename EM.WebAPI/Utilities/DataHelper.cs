﻿using EM.Factory;
using EM.Factory.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EM.WebAPI.Utilities
{
    public class DataHelper
    {
        public static TemplateViewModel GetDefaultTemplate()
        {
            var templateValues = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~") + "/Utilities/DefaultTemplateValue.txt");
            var tempData = templateValues.Split('^');
            TemplateViewModel templateViewModel = new TemplateViewModel();
            templateViewModel.TemplateJSON = tempData[0].ToString();
            templateViewModel.TemplateHtml = tempData[1].ToString();
            return templateViewModel;
        }

        public static void writeSendGridEventLog(string response)
        {
            //var ServerPath = HttpContext.Current.Server.MapPath("~/EventNotificationLogs/") + "CampaignEventLogFile_" + DateTime.Now.ToString("ddMMyy")+"_"+DateTime.Now.Hour+"_"+DateTime.Now.Minute+".txt";
            var ServerPathJsonFile = HttpContext.Current.Server.MapPath("~/EventNotificationLogs/") + "CampaignEventLogFile_" + DateTime.Now.ToString("ddMMyy") + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + Guid.NewGuid() + ".json";
            string message = response;
            using (StreamWriter writer = new StreamWriter(ServerPathJsonFile, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
            //message = Environment.NewLine;
            //message += string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            //message += string.Format("Event: {0}", response);
            //message += "------------------------------------------------------------------------";
            //using (StreamWriter writer = new StreamWriter(ServerPath, true))
            //{
            //    writer.WriteLine(message);
            //    writer.Close();
            //}
        }

        public static void writeEventLogTxt(string response, string folderName = "", string Tag = "")
        {
            string ServerPath = "";
            if (folderName != "" && Tag != "")
            {
                ServerPath = "~/EventNotificationLogs/" + folderName + "/" + Tag;
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ServerPath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ServerPath));

                }

            }
            else if (folderName == "" && Tag != "")
            {
                ServerPath = "~/EventNotificationLogs/" + Tag;
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ServerPath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ServerPath));

                }


            }
            else if (folderName != "" && Tag == "")
            {
                ServerPath = "~/EventNotificationLogs/" + folderName;
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(ServerPath)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ServerPath));

                }
            }
            else
            {
                ServerPath = "~/EventNotificationLogs";

            }
            ServerPath = ServerPath + "/EventLogFile_" + DateTime.Now.ToString("ddMMyy") + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + ".txt";
            ServerPath = HttpContext.Current.Server.MapPath(ServerPath);

            string message = response;
            using (StreamWriter writer = new StreamWriter(ServerPath, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
            //message = Environment.NewLine;
            //message += string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            //message += string.Format("Event: {0}", response);
            //message += "------------------------------------------------------------------------";
            //using (StreamWriter writer = new StreamWriter(ServerPath, true))
            //{
            //    writer.WriteLine(message);
            //    writer.Close();
            //}
        }

        public async static Task CreateLog(string userid, string Description, string CreatedBY)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                try
                {
                    EmailServiceLog obj = new EmailServiceLog();
                    obj.UserId = userid;
                    obj.Description = Description;
                    obj.LogDateTime = DateTime.Now;
                    obj.Createdby = CreatedBY;
                    db.EmailServiceLogs.Add(obj);
                    await db.SaveChangesAsync();
                }
                catch (Exception)
                {
                }

            }
        }

        public static void writeEmailEventLogTxt(Exception ex = null, string json = "")
        {

            try
            {
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~") + "/Logs/"))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "/Logs/");
                }
                var ServerPath = HttpContext.Current.Server.MapPath("~/Logs/") + "EventErrorLog_" + DateTime.Now.ToString("ddMMyy") + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + ".txt";
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));

                message += Environment.NewLine;
                if (ex != null)
                {
                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;
                    message += string.Format("Message: {0}", ex.Message);
                    message += Environment.NewLine;
                    message += Environment.NewLine;
                    message += string.Format("StackTrace: {0}", ex.StackTrace);
                    message += Environment.NewLine;
                    message += Environment.NewLine;
                    message += string.Format("Source: {0}", ex.Source);
                    message += Environment.NewLine;
                    message += Environment.NewLine;
                    message += string.Format("InnerException: {0}", ex.InnerException);
                    message += Environment.NewLine;
                    message += Environment.NewLine;
                    message += string.Format("JSON: {0}", json);
                }
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                using (StreamWriter writer = new StreamWriter(ServerPath, true))
                {
                    writer.WriteLine(message);
                    writer.Close();
                }
            }
            catch
            {

            }
        }
    }
}