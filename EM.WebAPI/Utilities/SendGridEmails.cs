﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using EM.Helpers;
using System.Collections.Generic;
using System;

namespace EM.WebAPI.Utilities
{
    public class SendGridEmails
    {
        SendGridClient sendGridClient;
        SendGridAPIHelper sendGridapi = new SendGridAPIHelper();

        public SendGridEmails()
        {
            var apiKey = System.Configuration.ConfigurationManager.AppSettings["SendGridKey"];
            sendGridClient = new SendGridClient(apiKey);
        }

        public async Task SendGridSingleEmail(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            var from = new EmailAddress(senderEmail, senderName);
            var to = new EmailAddress(recipientEmail, recipientName);
            var oEmail = MailHelper.CreateSingleEmail(from, to, subject, null, contentHtml);
            oEmail.AddBcc(System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"], System.Configuration.ConfigurationManager.AppSettings["InvoiceName"]);
            var response = await sendGridClient.SendEmailAsync(oEmail);
            
            //LogService.WriteToLog(response.StatusCode.ToString(), Id);
        }

        public void SendGridSingleEmailSync(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            try
            {
                var from = new EmailAddress(senderEmail, senderName);
                var to = new EmailAddress(recipientEmail, recipientName);
                var oEmail = MailHelper.CreateSingleEmail(from, to, subject, null, contentHtml);
                oEmail.AddBcc(System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"], System.Configuration.ConfigurationManager.AppSettings["InvoiceName"]);
                sendGridClient.SendEmailAsync(oEmail).Wait();
            }
            catch(Exception ex)
            {
                CreateLog.ActionLog(ex.Message);
            }

            //LogService.WriteToLog(response.StatusCode.ToString(), Id);
        }

    }

}