﻿using EM.Helpers;
using EM.PaymentService;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Utilities
{
    public class MailGunEmailHelper
    {
        static string apiKey = System.Configuration.ConfigurationManager.AppSettings["MailgunKey"].ToString();
        static string url = "https://api.mailgun.net/v3";
        static string senderDomain = "billing.cheapestemail.com";
        public void sendSingleEmailInvoice(string Id, string subject, string contentHtml, string senderEmail, string senderName, string recipientEmail, string recipientName)
        {
            //string senderemailhost = System.Configuration.ConfigurationManager.AppSettings["BillingEmailhost"];
            //RestClient client = new RestClient();
            //client.BaseUrl = new Uri(url);
            //client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
            //RestRequest request = new RestRequest();
            //request.AddParameter("domain", senderDomain, ParameterType.UrlSegment);
            //request.Resource = senderDomain + "/messages";
            //request.AddParameter("from", senderName + " <" + senderemailhost + ">");// Excited User <mailgun@sandbox5c97fa2b49464f5093726913d09d8c77.mailgun.org>");
            //request.AddParameter("to", recipientEmail);
            //request.AddParameter("bcc", System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]);
            //request.AddParameter("subject", subject);
            //request.AddParameter("html", contentHtml);
            //request.AddParameter("h:Reply-To", senderEmail);
            //request.Method = Method.POST;
            ////var response = client.Execute(request);
            //var response = client.ExecutePostTaskAsync(request);
            //LogService.WriteToLog(response.Status.ToString(), Id);

            Mail oMail = new Mail();
            oMail.SendMail(recipientEmail, subject, contentHtml, senderName, "mailSettings/Billing", recipientName, System.Configuration.ConfigurationManager.AppSettings["InvoiceEmail"]).Wait();


        }
    }
}