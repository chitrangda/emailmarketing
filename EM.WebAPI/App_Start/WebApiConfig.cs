﻿using EM.WebAPI.Interfaces;
using EM.WebAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace EM.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<IEmailPlanService, EmailPlanService>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserInfoService, UserInfoServices>(new HierarchicalLifetimeManager());
            container.RegisterType<ITemplateService, TemplateService>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserListService, UserListServices>(new HierarchicalLifetimeManager());
            container.RegisterType<ISubscriberService, SubscriberService>(new HierarchicalLifetimeManager());
            container.RegisterType<ISalesRepService, SalesRepService>(new HierarchicalLifetimeManager());
            container.RegisterType<ITransactionLogService, TransactionLogService>(new HierarchicalLifetimeManager());
            container.RegisterType<IBillingService, BillingService>(new HierarchicalLifetimeManager());
            container.RegisterType<ICampaignService, CampaignService>(new HierarchicalLifetimeManager());
            container.RegisterType<IFieldsService, FieldsService>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserDashboard, UserDashboardService>(new HierarchicalLifetimeManager());
            container.RegisterType<ICardTransaction, CardTransactionService>(new HierarchicalLifetimeManager());
            container.RegisterType<IIndustryService, IndustryService>(new HierarchicalLifetimeManager());
            container.RegisterType<IInvoiceService, InvoiceService>(new HierarchicalLifetimeManager());
            container.RegisterType<IPaymentTypeService, PaymentTypeService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdminDashboardService, AdminDashboardService>(new HierarchicalLifetimeManager());
            container.RegisterType<IManageUserService, ManageUserService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdminUpcomingBilling, AdminUpcomingBillingService>(new HierarchicalLifetimeManager());
            container.RegisterType<ISalesRepDataService, SalesRepDataService>(new HierarchicalLifetimeManager());
            container.RegisterType<INoteService, NoteService>(new HierarchicalLifetimeManager());
            container.RegisterType<ICampaignListService, CampaignListService>(new HierarchicalLifetimeManager());
            container.RegisterType<IClientTypeService, ClientTypeService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdminData, AdminDataService>(new HierarchicalLifetimeManager());
            container.RegisterType<IProvidersService, ProvidersService>(new HierarchicalLifetimeManager());
            container.RegisterType<ILeadService, LeadService>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepActivity, RepActivityService>(new HierarchicalLifetimeManager());
            container.RegisterType<IFollowUpService, FollowUpService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAdminSpend,AdminSpendService>(new HierarchicalLifetimeManager());
            container.RegisterType<ILeadDashboardService, LeadDashBoardService>(new HierarchicalLifetimeManager());
            container.RegisterType<ISalesRepSpend, SalesRepSpendService>(new HierarchicalLifetimeManager());
            container.RegisterType<ILeadNoteService, LeadNoteService>(new HierarchicalLifetimeManager());
            container.RegisterType<ISalesLeadEmail, SalesLeadEmailService>(new HierarchicalLifetimeManager());
            container.RegisterType<IIPPoolService, IPPoolService>(new HierarchicalLifetimeManager());
            container.RegisterType<ISalesLeadMobile, SalesLeadMobileService>(new HierarchicalLifetimeManager());
            container.RegisterType<IIPAddressService,IPAddressService>(new HierarchicalLifetimeManager());
            container.RegisterType<IPromotionCode, PromotionCodeService>(new HierarchicalLifetimeManager());
            container.RegisterType<IMailGunWebhookService, MailGunWebhookService>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);
            //Register CORS services

            var cors = new System.Web.Http.Cors.EnableCorsAttribute("http://app.emailmarket.srmtechsol.com", "*", "*"); // local
            cors.Origins.Add("http://18.233.130.90:81");
            cors.Origins.Add("http://localhost:62115");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
