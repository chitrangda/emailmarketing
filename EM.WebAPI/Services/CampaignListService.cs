﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class CampaignListService : ICampaignListService
    {
        private readonly EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        public List<CampaignList> Get()
        {
            var campaignListResult = _context.CampaignLists.ToList();
            var campaignList = (from ss in campaignListResult
                              select new CampaignList
                              {
                                  CampaignTypeId = ss.CampaignTypeId,
                                  CampaignName = ss.CampaignName
                              }).ToList();
            return campaignList;
        }
    }
}