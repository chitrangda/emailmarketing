﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class LeadService : ILeadService
    {
        private readonly EmailMarketingDbEntities _context = new EmailMarketingDbEntities();

        public bool Post(SalesPortal_Leads model)
        {
            if (model.LeadId == 0)
            {
                SalesPortal_Leads lead = new SalesPortal_Leads();
                lead.LeadId = model.LeadId;
                lead.FirstName = model.FirstName;
                lead.LastName = model.LastName;
                lead.JobTitle = model.JobTitle;
                lead.Company = model.Company;
                lead.IndustryId = model.IndustryId;
                lead.MobileNo = model.MobileNo;
                lead.PhoneNo = model.PhoneNo;
                lead.Email = model.Email;
                lead.Website = model.Website;
                lead.ProviderId = model.ProviderId;
                lead.ProviderName = model.ProviderName;
                lead.LinkedIn = model.LinkedIn;
                lead.Twitter = model.Twitter;
                lead.Facebook = model.Facebook;
                lead.Instagram = model.Instagram;
                lead.HotId = model.HotId;
                lead.OpportunityAmount = model.OpportunityAmount;
                lead.CreatedBy = model.CreatedBy;
                lead.CreatedOn = DateTime.UtcNow;
                lead.IsDeleted = false;
                lead.SalesRepID = model.SalesRepID;
                _context.SalesPortal_Leads.Add(lead);
                _context.SaveChanges();
                //if (model.Email != null)
                //{
                //    SalesPortal_Leads_EmailIdList idList = new SalesPortal_Leads_EmailIdList();
                //    idList.Email = model.Email;
                //    idList.LeadId = model.LeadId;
                //    idList.IsPrimaryEmail = true;
                //    idList.CreatedOn = DateTime.UtcNow;
                //    idList.CreatedBy = model.CreatedBy;
                //    _context.SalesPortal_Leads_EmailIdList.Add(idList);
                //    _context.SaveChanges();
                //}
                //if(model.MobileNo != null)
                //{
                //    SalesPortal_Leads_MobileNumList mobileNumList = new SalesPortal_Leads_MobileNumList();
                //    mobileNumList.LeadId = model.LeadId;
                //    mobileNumList.MobileNumber = model.MobileNo;
                //    mobileNumList.CreatedBy = model.CreatedBy;
                //    mobileNumList.CreatedOn = DateTime.UtcNow;
                //    mobileNumList.IsPrimaryNumber = true;
                //    _context.SalesPortal_Leads_MobileNumList.Add(mobileNumList);
                //    _context.SaveChanges();
                //}
                
                return true;
            }
            else
            {
                var lead = _context.SalesPortal_Leads.Where(x => x.LeadId == model.LeadId).FirstOrDefault();
                if (lead != null)
                {
                    lead.LeadId = model.LeadId;
                    lead.FirstName = model.FirstName;
                    lead.LastName = model.LastName;
                    lead.JobTitle = model.JobTitle;
                    lead.Company = model.Company;
                    lead.IndustryId = model.IndustryId;
                    lead.PhoneNo = model.PhoneNo;
                    lead.Website = model.Website;
                    lead.ProviderId = model.ProviderId;
                    lead.ProviderName = model.ProviderName;
                    lead.LinkedIn = model.LinkedIn;
                    lead.Twitter = model.Twitter;
                    lead.Facebook = model.Facebook;
                    lead.Instagram = model.Instagram;
                    lead.HotId = model.HotId;
                    lead.UpdatedBy = model.UpdatedBy;
                    lead.UpdatedOn = DateTime.UtcNow;
                    lead.IsDeleted = false;
                    lead.SalesRepID = model.SalesRepID;
                    lead.OpportunityAmount = model.OpportunityAmount;
                    lead.State = model.State;
                    lead.ColorId = model.ColorId;
                    if (model.FollowUpDate != null)
                    {
                        lead.FollowUpDate = model.FollowUpDate;
                    }
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
    }
}