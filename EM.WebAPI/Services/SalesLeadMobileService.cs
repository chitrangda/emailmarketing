﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class SalesLeadMobileService : ISalesLeadMobile
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public bool Delete(int leadId)
        {
            var mobile = db.SalesPortal_Leads_MobileNumList.AsEnumerable().Where(x => x.MobileRowId == leadId).FirstOrDefault();
            mobile.IsDelete = true;
            db.SaveChanges();
            return true;
        }

        public List<USP_GetLeads_MobileNumList_Result> Get(int leadId)
        {
            return db.USP_GetLeads_MobileNumList(leadId).ToList();
        }

        public SalesPortal_Leads_MobileNumList Get(int leadId, int EmailRowId)
        {
            var mobileIdDetails = db.SalesPortal_Leads_MobileNumList.Where(x => x.MobileRowId == EmailRowId && x.LeadId == leadId).FirstOrDefault();
            if (mobileIdDetails != null)
            {
                var response = new SalesPortal_Leads_MobileNumList()
                {
                    MobileRowId = mobileIdDetails.MobileRowId,
                    LeadId = mobileIdDetails.LeadId,
                    MobileNumber = mobileIdDetails.MobileNumber,
                    IsPrimaryNumber = mobileIdDetails.IsPrimaryNumber

                };
                return response;
            }
            else
            {
                return null;
            }
        }

        public bool Post(SalesPortal_Leads_MobileNumList model)
        {
            if (model.MobileRowId == 0)
            {

                SalesPortal_Leads_MobileNumList mobileList = new SalesPortal_Leads_MobileNumList();
                mobileList.LeadId = model.LeadId;
                mobileList.MobileNumber = model.MobileNumber;
                mobileList.CreatedBy = model.CreatedBy;
                mobileList.CreatedOn = DateTime.UtcNow;
                mobileList.IsDelete = false;
                mobileList.IsPrimaryNumber = false;
                db.SalesPortal_Leads_MobileNumList.Add(mobileList);
                db.SaveChanges();
                return true;
            }
            else
            {
                var mobile = db.SalesPortal_Leads_MobileNumList.Find(model.MobileRowId);
                if (mobile != null)
                {
                    mobile.LeadId = model.LeadId;
                    mobile.MobileNumber = model.MobileNumber;
                    mobile.UpdatedOn = DateTime.UtcNow;
                    mobile.UpdatedBy = model.UpdatedBy;
                    mobile.IsDelete = false;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool Put(int LeadId, int MobileRowId)
        {

            var mobilePrimary = db.SalesPortal_Leads_MobileNumList.Where(x => x.LeadId == LeadId && x.MobileRowId == MobileRowId).FirstOrDefault();
            var IsPrimary = db.SalesPortal_Leads_MobileNumList.Where(x => x.LeadId == LeadId && x.MobileRowId != MobileRowId).ToList();
            if (IsPrimary.Count > 0)
            {
                IsPrimary.ForEach(x => x.IsPrimaryNumber = false);
                db.SaveChanges();
            }
            if (mobilePrimary != null)
            {
                mobilePrimary.IsPrimaryNumber = true;
                db.SaveChanges();
                return true;
            }
            else
            {
                return true;
            }
        }

    }
}