﻿using System.Collections.Generic;
using System.Linq;
using EM.Factory;
using EM.WebAPI.Interfaces;

namespace EM.WebAPI.Services
{
    public class AdminDashboardService : IAdminDashboardService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public usp_getAdminTodays_Result Get()
        {
            return db.usp_getAdminTodays().FirstOrDefault();
        }

        public List<usp_getAdminDashboardData_Result> GetAdminData(string Name, string Email, string MobileNumber,string NameOnCard,string MumberType, string ReferralCode,string City,string State,string SalesRep,string MemberType, string ClientType)
        {
            return db.usp_getAdminDashboardData(Name, Email, MobileNumber, NameOnCard, MumberType, ReferralCode, City, State, SalesRep, MemberType, ClientType).ToList();
        }
    }
}