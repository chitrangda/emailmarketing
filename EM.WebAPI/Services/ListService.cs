﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class ListService : IListService
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        public List<ListViewModel> Get()
        {
            try
            {
                var lists = _context.Lists.ToList();
                var response = (from list in lists.AsEnumerable()
                                select new ListViewModel
                                {
                                    ListId = list.ListId,
                                    ListName = list.ListName
                                }).ToList();
                return response;
            }
            catch (Exception)
            {
                return new List<ListViewModel>();
            }
        }
    }
}