﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class ProvidersService : IProvidersService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public bool Delete(int id)
        {
            
                var provider = db.SalesPortal_Providers.AsEnumerable().Where(x => x.ProviderId == id).FirstOrDefault();
                provider.IsDelete = true;
                db.SaveChanges();
         
            return true;
        }

        public List<SalesPortal_Providers> Get()
        {

            var providersListResult = db.SalesPortal_Providers.Where(x => x.IsDelete == false).ToList();
            if (providersListResult != null)
            {
                var _providers = (from u in providersListResult
                                  select new SalesPortal_Providers
                                  {
                                      ProviderId = u.ProviderId,
                                      ProviderName=u.ProviderName
               
                                }
                            ).ToList();
                return _providers;
            }
            else
            {
                return null;
            }
        }

        public SalesPortal_Providers Get(int id)
        {
            var providersListResult = db.SalesPortal_Providers.SingleOrDefault(x => x.ProviderId == id);
            if (providersListResult != null)
            {
                var _providers = new SalesPortal_Providers()
                {
                    ProviderId = providersListResult.ProviderId,
                    ProviderName = providersListResult.ProviderName
                };
                return _providers;
            }
            else
            {
                return null;
            }
        }

        public bool Post(SalesPortal_Providers model)
        {
            if (model.ProviderId == 0)
            {
                var provider = db.SalesPortal_Providers.FirstOrDefault(x => x.ProviderName == model.ProviderName && x.IsDelete == true);
                if (provider == null)
                {
                    SalesPortal_Providers salesPortal_Providers = new SalesPortal_Providers();
                    salesPortal_Providers.ProviderId = model.ProviderId;
                    salesPortal_Providers.ProviderName = model.ProviderName;
                    salesPortal_Providers.CreatedBy = model.CreatedBy;
                    salesPortal_Providers.CreatedOn = DateTime.UtcNow;
                    salesPortal_Providers.IsDelete = false;
                    db.SalesPortal_Providers.Add(salesPortal_Providers);
                    db.SaveChanges();
                }
                else
                {
                    provider.IsDelete = false;
                    db.SaveChanges();
                }
                return true;
            }
            else
            {
                var providers = db.SalesPortal_Providers.Find(model.ProviderId);
                if (providers != null)
                {
                    providers.ProviderId = model.ProviderId;
                    providers.ProviderName = model.ProviderName;
                    providers.LastModBy = model.LastModBy;
                    providers.LastModOn = DateTime.UtcNow;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}