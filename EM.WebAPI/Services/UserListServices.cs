﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class UserListServices : IUserListService
    {
        private EmailMarketingDbEntities db = new EmailMarketingDbEntities();



        public List<usp_getUserList_Result> Get(string id, string listname, int pageNumber, int? pageSize, string orderby)
        {
            var subscribersList = db.usp_getUserList(id, listname, pageNumber, pageSize, orderby).ToList();
            return subscribersList;
        }
        public List<usp_getDeletedUserList_Result> GetDeleted(string id, string listname, int pageNumber, int? pageSize, string orderby)
        {
            var subscribersList = db.usp_getDeletedUserList(id, listname, pageNumber, pageSize, orderby).ToList();
            return subscribersList;
        }

        public ListViewModel GetListById(int id)
        {
            var _listuser = db.Lists.SingleOrDefault(s => s.ListId == id);
            if (_listuser != null)
            {
                var response = new ListViewModel()
                {
                    ListId = _listuser.ListId,
                    ListName = _listuser.ListName,
                    FromEmail = _listuser.FromEmail,
                    FromName = _listuser.FromName,
                    Remainder = _listuser.Remainder,
                    Company = _listuser.Company,
                    Address1 = _listuser.Address1,
                    Address2 = _listuser.Address2,
                    City = _listuser.City,
                    Zip = _listuser.Zip,
                    Country = _listuser.Country,
                    Phone = _listuser.Phone,
                    Optin = _listuser.Optin,
                    GDPR = _listuser.GDPR,
                    Subscriber_Unsubscriber = _listuser.Subscriber_Unsubscriber,
                    Subscriber = _listuser.Subscriber,
                    Unsubscriber = _listuser.Unsubscriber,
                    State=_listuser.State
                };
                return response;
            }
            else
            {
                return null;
            }
        }

        public bool Post(ListViewModel lstViewModel)
        {
                 //var userDetails = db.UserProfiles.Where(x => x.Id == lstViewModel.CreatedById).FirstOrDefault();
                //var timezonedetails = db.TimeZones.Where(x => x.TimeZoneID == userDetails.TimeZoneId).FirstOrDefault();
                //System.TimeZone localzone = System.TimeZone.CurrentTimeZone;
                //DateTime currentdate = DateTime.Now;
                //int currentyear = currentdate.Year;
                //System.Globalization.DaylightTime daylight = localzone.GetDaylightChanges(currentyear);
                if (lstViewModel == null)
                {
                    return false;
                }
                List list;
                List listM = null;
                if (lstViewModel.ListId != 0)
                {
                    list = db.Lists.Find(lstViewModel.ListId);
                }
                else
                {
                    listM = db.Lists.Where(x => x.ListName == lstViewModel.ListName && x.CreatedById == lstViewModel.CreatedById && x.IsDeleted == true).FirstOrDefault();
                    if(listM != null)
                    {
                        list = db.Lists.Find(listM.ListId);
                    }
                    else
                    {
                        list = new List();
                    }
                   
                }

                list.ListName = lstViewModel.ListName!=null? lstViewModel.ListName.Trim(): lstViewModel.ListName;
                list.FromEmail = lstViewModel.FromEmail != null ? lstViewModel.FromEmail.Trim(): lstViewModel.FromEmail;
                list.FromName = lstViewModel.FromName!=null? lstViewModel.FromName.Trim(): lstViewModel.FromName;
                list.Remainder = lstViewModel.Remainder;
                list.Company = lstViewModel.Company != null ? lstViewModel.Company.Trim(): lstViewModel.Company;
                list.Address1 = lstViewModel.Address1!=null? lstViewModel.Address1.Trim(): lstViewModel.Address1;
                list.Address2 = lstViewModel.Address2!=null? lstViewModel.Address2.Trim(): lstViewModel.Address2;
                list.City = lstViewModel.City!=null? lstViewModel.City.Trim(): lstViewModel.City;
                list.Zip = lstViewModel.Zip!=null? lstViewModel.Zip.Trim(): lstViewModel.Zip;
                list.State = lstViewModel.State!=null? lstViewModel.State.Trim(): lstViewModel.State;
                list.Country = lstViewModel.Country!=null? lstViewModel.Country.Trim(): lstViewModel.Country;
                list.Phone = lstViewModel.Phone;
                list.Optin = lstViewModel.Optin;
                list.GDPR = lstViewModel.GDPR;
                list.Subscriber_Unsubscriber = lstViewModel.Subscriber_Unsubscriber;
                list.Subscriber = lstViewModel.Subscriber;
                list.Unsubscriber = lstViewModel.Unsubscriber;
                list.LastModById = lstViewModel.CreatedById;
                list.ListNameDomainWise = lstViewModel.ListNameDomainWise;
                //if (timezonedetails != null)
                //{
                //    if (localzone.IsDaylightSavingTime(DateTime.Now))
                //    {
                //        list.LastModDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT) + 1);
                //    }
                //    else
                //    {
                //        list.LastModDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT));
                //    }
                //}
                //else
                //{
                    list.LastModDate = DateTime.Now;
               // }
                //list.LastModDate = DateTime.Now;
                if (lstViewModel.ListId == 0)
                {
                    list.CreatedById = lstViewModel.CreatedById;
                    ////if (timezonedetails != null)
                    ////{
                    ////    if (localzone.IsDaylightSavingTime(DateTime.Now))
                    ////    {
                    ////        list.CreatedDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT) + 1);
                    ////    }
                    ////    else
                    ////    {
                    //        list.CreatedDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT));
                    //    }
                    //}
                    //else
                    //{
                        list.CreatedDate = DateTime.Now;
                   // }
                    //list.CreatedDate = DateTime.Now;
                    list.IsDeleted = false;
                    list.IsActive = true;
                    if (listM == null)
                    {
                        db.Lists.Add(list);
                    }
                }
                db.SaveChanges();
                return true;

        }

        public List<ListViewModel> Post(List<ListViewModel> lstViewModels)
        {
            List<ListViewModel> listViewModels = new List<ListViewModel>();
                //var userDetails = db.UserProfiles.Where(x => x.Id == lstViewModels.FirstOrDefault().CreatedById).FirstOrDefault();
                //var timezonedetails = db.TimeZones.Where(x => x.TimeZoneID == userDetails.TimeZoneId).FirstOrDefault();
                //System.TimeZone localzone = System.TimeZone.CurrentTimeZone;
                //DateTime currentdate = DateTime.Now;
                //int currentyear = currentdate.Year;
                //System.Globalization.DaylightTime daylight = localzone.GetDaylightChanges(currentyear);
                foreach (var lstViewModel in lstViewModels)
                {
                  
                    var listData = db.Lists.Where(x => x.ListName.ToLower() == lstViewModel.ListName.Trim().ToLower() && x.CreatedById == lstViewModel.CreatedById.Trim()).FirstOrDefault();
                    if (listData == null)
                    {
                        var list = new List();
                        list.ListName = lstViewModel.ListName != null ? lstViewModel.ListName.Trim() : lstViewModel.ListName;
                        list.FromEmail = lstViewModel.FromEmail != null ? lstViewModel.FromEmail.Trim() : lstViewModel.FromEmail;
                        list.FromName = lstViewModel.FromName != null ? lstViewModel.FromName.Trim() : lstViewModel.FromName;
                        list.Remainder = lstViewModel.Remainder;
                        list.Company = lstViewModel.Company!=null? lstViewModel.Company.Trim(): lstViewModel.Company;
                        list.Address1 = lstViewModel.Address1 != null ? lstViewModel.Address1.Trim() : lstViewModel.Address1;
                        list.Address2 = lstViewModel.Address2 != null ? lstViewModel.Address2.Trim() : lstViewModel.Address2;
                        list.City = lstViewModel.City != null ? lstViewModel.City.Trim() : lstViewModel.City;
                        list.Zip = lstViewModel.Zip != null ? lstViewModel.Zip.Trim() : lstViewModel.Zip;
                        list.Country = lstViewModel.Country != null ? lstViewModel.Country.Trim() : lstViewModel.Country;
                        list.Phone = lstViewModel.Phone;
                        list.Optin = lstViewModel.Optin;
                        list.GDPR = lstViewModel.GDPR;
                        list.Subscriber_Unsubscriber = lstViewModel.Subscriber_Unsubscriber;
                        list.Subscriber = lstViewModel.Subscriber;
                        list.Unsubscriber = lstViewModel.Unsubscriber;
                        list.LastModById = lstViewModel.CreatedById;
                        list.CreatedById = lstViewModel.CreatedById;
                        //if (timezonedetails != null)
                        //{
                        //    if (localzone.IsDaylightSavingTime(DateTime.Now))
                        //    {
                        //        list.CreatedDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT) + 1);
                        //    }
                        //    else
                        //    {
                        //        list.CreatedDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT));
                        //    }
                        //}
                        //else
                        //{
                            list.LastModDate = DateTime.Now;
                        //}
                        list.IsDeleted = false;
                        list.IsActive = true;
                        db.Lists.Add(list);
                        db.SaveChanges();
                    }
                    else
                    {
                        //listData.ListName = listData.ListName != null ? listData.ListName.Trim() : listData.ListName;
                        listData.FromEmail = lstViewModel.FromEmail != null ? lstViewModel.FromEmail.Trim() : lstViewModel.FromEmail;
                        listData.FromName = lstViewModel.FromName != null ? lstViewModel.FromName.Trim() : lstViewModel.FromName;
                        listData.Company = lstViewModel.Company != null ? lstViewModel.Company.Trim() : lstViewModel.Company;
                        listData.Address1 = lstViewModel.Address1 != null ? lstViewModel.Address1.Trim() : lstViewModel.Address1;
                        listData.Address2 = lstViewModel.Address2 != null ? lstViewModel.Address2.Trim() : lstViewModel.Address2;
                        listData.City = lstViewModel.City != null ? lstViewModel.City.Trim() : lstViewModel.City;
                        listData.Zip = lstViewModel.Zip != null ? lstViewModel.Zip.Trim() : lstViewModel.Zip;
                        listData.Country = lstViewModel.Country != null ? lstViewModel.Country.Trim() : lstViewModel.Country;
                        listData.LastModById = lstViewModel.CreatedById;
                        listData.CreatedById = lstViewModel.CreatedById;
                        listData.Phone = lstViewModel.Phone;
                        //if (timezonedetails != null)
                        //{
                        //    if (localzone.IsDaylightSavingTime(DateTime.Now))
                        //    {
                        //        listData.LastModDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT) + 1);
                        //    }
                        //    else
                        //    {
                        //        listData.LastModDate = DateTime.UtcNow.AddHours(Convert.ToDouble(timezonedetails.GMT));
                        //    }
                        //}
                        //else
                        //{
                            listData.LastModDate = DateTime.Now;
                        //}
                        if (listData.IsDeleted == false)
                        {
                          listViewModels.Add(lstViewModel);
                        }
                        listData.IsDeleted = false;
                        db.SaveChanges();
                    }
                            
                }
                return listViewModels;
        }

        public bool Delete(List<int> id)
        {
            foreach (var item in id)
            {
                var list = db.Lists.AsEnumerable().Where(x => x.ListId == item).FirstOrDefault();
                list.IsDeleted = true;
                db.SaveChanges();
            }
            return true;
        }

        public bool RestoreDelete(List<int> id)
        {
            foreach (var item in id)
            {
                var list = db.Lists.AsEnumerable().Where(x => x.ListId == item).FirstOrDefault();
                list.IsDeleted = false;
                db.SaveChanges();
            }
            return true;
        }

        public ListViewModel GetRecent(string userid)
        {
            var _listuser = db.Lists.Where(s => s.CreatedById == userid).OrderByDescending(r => r.ListId).FirstOrDefault();
            if (_listuser != null)
            {
                var response = new ListViewModel()
                {
                    ListId = _listuser.ListId,
                    ListName = _listuser.ListName,
                    FromEmail = _listuser.FromEmail,
                    FromName = _listuser.FromName,
                    Remainder = _listuser.Remainder,
                    Company = _listuser.Company,
                    Address1 = _listuser.Address1,
                    Address2 = _listuser.Address2,
                    City = _listuser.City,
                    Zip = _listuser.Zip,
                    Country = _listuser.Country,
                    Phone = _listuser.Phone,
                    Optin = _listuser.Optin,
                    GDPR = _listuser.GDPR,
                    Subscriber_Unsubscriber = _listuser.Subscriber_Unsubscriber,
                    Subscriber = _listuser.Subscriber,
                    Unsubscriber = _listuser.Unsubscriber
                };
                return response;
            }
            else
            {
                return null;
            }
        }

    }
}



