﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class EmailPlanService : IEmailPlanService
    {
        private EmailMarketingDbEntities db = new EmailMarketingDbEntities();


        public List<usp_geAllPlans_Result> Get()
        {
            var response = db.usp_geAllPlans().OrderBy(x => x.NoOfSubscribersMax).ToList();
            return response;
        }

        public PlanPricingsViewModel Get(int id)
        {
            var package = (from pack in db.Packages
                           select new PlanPricingsViewModel
                           {
                               Id = pack.Id,
                               PlanId = pack.PlanId,
                               BillingAmount = pack.BillingAmount,
                               //TotalMonth = pack.TotalMonth,
                           }).Where(x => x.Id == id).FirstOrDefault();
            if (package == null)
            {
                return null;
            }
            return package;
        }

        public List<Plan> GetPlan()
        {
            var planListResult = db.Plans.ToList();
            var planList = (from ss in planListResult
                            select new Plan
                            {
                                PlanId = ss.PlanId,
                                PlanName = ss.PlanName,
                                ContactManagement = ss.ContactManagement,
                                UnlimitedEmails = ss.UnlimitedEmails,
                                CustomizableTemplates = ss.CustomizableTemplates,
                                EducationalResources = ss.EducationalResources,
                                LiveSupport = ss.LiveSupport,
                                EmailScheduling = ss.EmailScheduling
                            }).ToList();
            return planList;
        }

        public usp_getPackageDetail_Result GetPlanByID(int id , string UserId = null)
        {
            return db.usp_getPackageDetail(id, UserId).SingleOrDefault();
        }

        public List<PlanPricingsViewModel> GetPlanPricing(int id)
        {
            if (id > 0)
            {
                var planPricings = db.Packages.AsEnumerable().Where(x => x.PlanId == id).ToList();
                var planPricingsResponse = (from pp in planPricings
                                            select new PlanPricingsViewModel
                                            {
                                                Id = pp.Id,
                                                PlanId = pp.PlanId,
                                                BillingAmount = pp.BillingAmount,
                                                NoOfSubscribersMax=pp.NoOfSubscribersMax,
                                                //TotalMonth = pp.TotalMonth,
                                            }).OrderBy(x => x.NoOfSubscribersMax).ToList();
                return planPricingsResponse;
            }
            return null;
        }

    }
}