﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class RepActivityService : IRepActivity
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public List<usp_getSalesRepActivity_Result> Get()
        {
            return db.usp_getSalesRepActivity(null,null,null).ToList();
        }
        public List<usp_getSalesRepActivity_Result> GetGridData(string RepId, DateTime? From, DateTime? To)
        {
            return db.usp_getSalesRepActivity(RepId, From, To).ToList();
        }
        public  List<SalesRepProfile> getSaleRepProfile()
        {
            var SaleRepList = from SRP in db.SalesRepProfiles
                              //orderby SRP.FirstName
                              select SRP;
            return SaleRepList.ToList();
        }
    }
}