﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class FieldsService : IFieldsService
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        public List<FieldsViewModel> Get(int listId)
        {
            var fields = (from f in _context.Fields.AsEnumerable()
                          select new FieldsViewModel
                          {
                              FieldName = f.FieldName,
                              Id = f.Id,
                              IsActivated = IsActivated(f.Id, listId)
                          }).ToList();
            return fields;


        }

        public bool IsActivated(int fiedlsId, int listId)
        {
            var fields = _context.ListFields.AsEnumerable().Where(x => Convert.ToInt32(x.FieldId) == fiedlsId && Convert.ToInt32(x.ListId) == listId).FirstOrDefault();
            if (fields is null)
            {
                return false;
            }
            else
            {
                return fields.IsActive;
            }
        }
        public bool ActivateDeactivate(List<int> fields, int listId, string action)
        {
            if (fields.Count() < 1 && listId < 1)
            {
                return false;
            }
            foreach (var item in fields)
            {
                var field = _context.ListFields.Where(x => x.FieldId == item && x.ListId == listId).FirstOrDefault();
                if (field is null)
                {
                    field = new ListField();
                    field.ListId = listId;
                    field.FieldId = item;
                    field.IsActive = action.ToLower() == "activate" ? true : false;
                    _context.ListFields.Add(field);
                    _context.SaveChanges();
                }
                else
                {
                    field.IsActive = action.ToLower() == "activate" ? true : false;
                    _context.SaveChanges();
                }
           
            }
            return true;
        }
    }
}