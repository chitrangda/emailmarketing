﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class ClientTypeService : IClientTypeService
    {
        private readonly EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        public List<ClientType> Get()
        {
            return _context.ClientTypes.ToList();
        }
    }
}