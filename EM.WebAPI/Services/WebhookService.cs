﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using MoreLinq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using EM.Helpers;
using System.Data;
using EM.WebAPI.Models;
using System.Collections.Generic;
using EM.WebAPI.Utilities;

namespace EM.WebAPI.Services
{
    public class WebhookService : IWebhookService
    {
        public async Task SaveEvents(EventView eventList)
        {
            List<EventTypeModel> events = new List<EventTypeModel>();
            using (var db = new EmailMarketingDbEntities())
            {
                if (eventList.category != null)
                {
                    string userid = eventList.category[0];
                    int campaignId = Convert.ToInt32(eventList.category[1]);
                    events.Add(new EventTypeModel()
                    {
                        CampaignId = Convert.ToInt32(campaignId),
                        email = eventList.email,
                        Event_TimeStamp = eventList.timestamp,
                        @event = eventList.@event,
                        reason = eventList.reason,
                        url = eventList.url,
                        response = eventList.response,
                        UserId = eventList.category[0]

                    });

                }
                await BulkInsertEvents(events.ToDataTable());
            }
        }

        async Task BulkInsertEvents(DataTable dtEvents)
        {
            try
            {
                String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandText = "dbo.usp_insertEmailEvent"; //The name of the above mentioned stored procedure.  
                    SqlParameter pram = cmd.Parameters.AddWithValue("@Events", dtEvents); //Here"@MyUDTableType" is the User-defined Table Type as a parameter. 
                    conn.Open();
                    int res = await cmd.ExecuteNonQueryAsync();
                    conn.Close();
                }
            }
            catch (System.Data.Common.DbException ex)
            {
                await DataHelper.CreateLog(null, ex.Message, "Web API DBI");

            }
            catch (Exception ex)
            {
                await DataHelper.CreateLog(null, ex.Message, "Web API Bulk Insert");
            }

        }
    }
}