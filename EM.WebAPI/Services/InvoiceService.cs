﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class InvoiceService : IInvoiceService
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        BillingService billingService = new BillingService();
        UserInfoServices userInfoServices = new UserInfoServices();


        public string Post(InvoiceViewModel Model)
        {
           
                return CardPayment(Model);
        }

        public string CardPayment(InvoiceViewModel Model)
        {
            if (Model.OrderID != null)
            {
                //AnTranResponse tranres = null;
                //var payemntProfile = _context.UserPaymentProfiles.Where(r => r.UserId == Model.UserId && r.IsPrimary == true).FirstOrDefault();
                //var payemntProfile = _context.UserPaymentProfiles.Where(r => r.UserId == Model.UserId && r.IsPrimary == true).FirstOrDefault();

                if (Model.UserPaymentProfileId==0)
                {
                    UserPaymentProfileViewModel paymentModel = new UserPaymentProfileViewModel();
                    paymentModel.CreditCardNo = Model.CardNumber;
                    paymentModel.CVV = Model.VerificationNo;
                    paymentModel.UserId = Model.UserId;
                    paymentModel.City = Model.City;
                    paymentModel.Address1 = Model.Address;
                    paymentModel.FirstName = Model.NameOnCard;
                    paymentModel.Phone = Model.Phone;
                    paymentModel.State = Model.State;
                    paymentModel.Zip = Model.Zip;
                    paymentModel.IsDeleted = false;
                    paymentModel.IsPrimary = true;
                    paymentModel.CreatedById = Model.CreatedById;
                    paymentModel.ExMonth = Convert.ToInt32(Model.ExpiresMonth);
                    paymentModel.ExYear = Convert.ToInt32(Model.ExpiresYear);
                    paymentModel.AnPaymentProfileId = Model.AnPaymentProfileId;
                    paymentModel.Email = Model.Email;
                    var result = billingService.Post(paymentModel);
                    return result;
                }
                else
                {
                    return "Success";
                }
                //else
                //{
                //    UserPaymentProfileViewModel paymentModel = new UserPaymentProfileViewModel();
                //    paymentModel.CreditCardNo = Model.CardNumber;
                //    paymentModel.CVV = Model.VerificationNo;
                //    paymentModel.UserId = Model.UserId;
                //    paymentModel.City = Model.City;
                //    paymentModel.Address1 = Model.Address;
                //    paymentModel.FirstName = Model.NameOnCard;
                //    paymentModel.Phone = Model.Phone;
                //    paymentModel.State = Model.State;
                //    paymentModel.Zip = Model.Zip;
                //    paymentModel.IsDeleted = false;
                //    paymentModel.IsPrimary = true;
                //    paymentModel.CreatedById = Model.CreatedById;
                //    paymentModel.ExMonth = Convert.ToInt32(Model.ExpiresMonth);
                //    paymentModel.ExYear = Convert.ToInt32(Model.ExpiresYear);
                //    var result = billingService.Post(paymentModel);
                //    return result;
                //}
            }
            else
            {
                return "error";

            }
        }


        public InvoiceViewModel getNewInvoice(string userid)
        {
            InvoiceViewModel invoice = new InvoiceViewModel();
            var _cardDetails = billingService.Get(userid) != null ? billingService.Get(userid).FirstOrDefault(r => r.IsPrimary == true) : null;
            if (_cardDetails != null)
            {
                invoice.NameOnCard = _cardDetails.FirstName + " " + _cardDetails.LastName;
                invoice.Address = _cardDetails.Address1;
                invoice.CardNumber = _cardDetails.CreditCardNo;
                invoice.City = _cardDetails.City;
                invoice.VerificationNo = _cardDetails.CVV;
                invoice.Zip = _cardDetails.Zip;
                invoice.ExpiresMonth = _cardDetails.ExMonth;
                invoice.ExpiresYear = _cardDetails.ExYear;
                invoice.Phone = _cardDetails.Phone;
                invoice.UserPaymentProfileId = _cardDetails.Id;
                invoice.AnPaymentProfileId = _cardDetails.AnPaymentProfileId;
                invoice.State = _cardDetails.State;
                invoice.CardType = _cardDetails.CardType;
                invoice.Email = _cardDetails.Email;
            }
            else
            {
                var _profileDetails = userInfoServices.Get(userid);
                invoice.NameOnCard = _profileDetails.FirstName + " " + _profileDetails.LastName;
                invoice.Address = _profileDetails.Address1;
                invoice.City = _profileDetails.City;
                invoice.Zip = _profileDetails.Zip;
                invoice.Phone = _profileDetails.MobilePhone;
                invoice.State = _profileDetails.State;
                invoice.Email = _profileDetails.Email;

            }
            return invoice;
        }

        public List<usp_getInvoice_Result> getUSerInvoice(string userid)
        {
            return _context.usp_getInvoice(userid).ToList();
        }

        public usp_getInvoiceDetails_Result getInvoiceDetails(string invoiceId)
        {
            return _context.usp_getInvoiceDetails(invoiceId).FirstOrDefault();
        }


    }
}
//if (result == "success")
//{
//    var curUserDetails = billingService.Get(paymentModel.UserId);
//    var PaymentProfileId = curUserDetails.Where(x => x.UserId == Model.UserId && x.IsPrimary == true).SingleOrDefault().AnPaymentProfileId;

//    var CurUserProfile = userInfoServices.Get(Model.UserId);
//    var ProfileId = CurUserProfile.AuthProfileId;
//    using (PaymentSession paySession = new PaymentSession())
//    {

//        tranres = paySession.ChargeCustomerProfile(PaymentProfileId, ProfileId, Convert.ToDecimal(Model.AmountCharged), Model.OrderID);
//        if (paySession.Status && tranres.ResponseCode == "1")
//        {
//            AnTransactionLog transLog = new AnTransactionLog();
//            transLog.AnTransactionId = tranres.TransId;
//            transLog.TransactionDate = DateTime.Now;
//            transLog.CreatedBy = "AdminInvoice";
//            transLog.Message = "approval Code: " + tranres.ApprovalCode;
//            transLog.TransactionType = "Credit Card";
//            transLog.Amount = Model.AmountCharged.ToString();
//            transLog.TransactionDate = DateTime.Now;
//            transLog.CreatedDate = DateTime.Now;
//            transLog.PaymentProfileId = PaymentProfileId;
//            transLog.UserId = Model.UserId;
//            transLog.PaymentProfileId = PaymentProfileId;
//            _context.AnTransactionLogs.Add(transLog);


//            // create invoice 
//            var invoiceModel = new Invoice();
//            invoiceModel.InvoiceID = Model.InvoiceID;
//            invoiceModel.UserId = Model.UserId;
//            invoiceModel.AmountCharged = Model.AmountCharged;
//            invoiceModel.OrderID = Model.OrderID;
//            invoiceModel.CardNumber = Model.CardNumber;
//            invoiceModel.ExpiresMonth = Model.ExpiresMonth.ToString();
//            invoiceModel.ExpiresYear = Model.ExpiresYear.ToString();
//            invoiceModel.NameOnCard = Model.NameOnCard;
//            invoiceModel.Phone = Model.Phone;
//            invoiceModel.Address = Model.Address;
//            invoiceModel.City = Model.City;
//            invoiceModel.State = Model.State;
//            invoiceModel.Zip = Model.Zip;
//            invoiceModel.CreatedById = Model.CreatedById;
//            invoiceModel.CreatedDate = DateTime.Now;
//            invoiceModel.AnTransactionId = tranres.TransId;
//            invoiceModel.InvoiceType = Model.InvoiceType;
//            _context.Invoices.Add(invoiceModel);
//            _context.SaveChanges();

//            return "success";
//        }
//        else
//        {
//            return paySession.Message;
//        }
//    }
//}
//else

//{
//    return "error";
//}


//else
//{
//    using (PaymentSession paySession = new PaymentSession())
//    {
//        var curUserDetails = billingService.Get(Model.UserId);
//        var PaymentProfileId = curUserDetails.Where(x => x.UserId == Model.UserId && x.IsPrimary == true).SingleOrDefault().AnPaymentProfileId;

//        var CurUserProfile = userInfoServices.Get(Model.UserId);
//        var ProfileId = CurUserProfile.AuthProfileId;

//        tranres = paySession.ChargeCustomerProfile(PaymentProfileId, ProfileId, Convert.ToDecimal(Model.AmountCharged), Model.OrderID);
//        if (paySession.Status && tranres.ResponseCode == "1")
//        {
//            AnTransactionLog transLog = new AnTransactionLog();
//            transLog.AnTransactionId = tranres.TransId;
//            transLog.TransactionDate = DateTime.Now;
//            transLog.CreatedBy = "AdminInvoice";
//            transLog.Message = "approval Code: " + tranres.ApprovalCode;
//            transLog.TransactionType = "Credit Card";
//            transLog.Amount = Model.AmountCharged.ToString();
//            transLog.TransactionDate = DateTime.Now;
//            transLog.CreatedDate = DateTime.Now;
//            transLog.PaymentProfileId = PaymentProfileId;
//            transLog.UserId = Model.UserId;
//            _context.AnTransactionLogs.Add(transLog);


//            // create invoice 
//            var invoiceModel = new Invoice();
//            invoiceModel.InvoiceID = Model.InvoiceID;
//            invoiceModel.UserId = Model.UserId;
//            invoiceModel.AmountCharged = Model.AmountCharged;
//            invoiceModel.OrderID = Model.OrderID;
//            invoiceModel.CardNumber = Model.CardNumber;
//            invoiceModel.ExpiresMonth = Model.ExpiresMonth.ToString();
//            invoiceModel.ExpiresYear = Model.ExpiresYear.ToString();
//            invoiceModel.NameOnCard = Model.NameOnCard;
//            invoiceModel.Phone = Model.Phone;
//            invoiceModel.Address = Model.Address;
//            invoiceModel.City = Model.City;
//            invoiceModel.State = Model.State;
//            invoiceModel.Zip = Model.Zip;
//            invoiceModel.CreatedById = Model.CreatedById;
//            invoiceModel.CreatedDate = DateTime.Now;
//            invoiceModel.AnTransactionId = tranres.TransId;
//            invoiceModel.InvoiceType = Model.InvoiceType;
//            _context.Invoices.Add(invoiceModel);
//            _context.SaveChanges();

//            return "success";
//        }
//        else
//        {
//            return paySession.Message;
//        }

//    }
//}

//AnTranResponse tranres = null;
//var payemntProfile = _context.UserPaymentProfiles.Where(r => r.UserId == Model.UserId && r.IsPrimary == true).FirstOrDefault();
//if (payemntProfile == null)
//{
//    UserPaymentProfileViewModel paymentModel = new UserPaymentProfileViewModel();
//    paymentModel.CreditCardNo = Model.CardNumber;
//    paymentModel.CVV = Model.VerificationNo;
//    paymentModel.UserId = Model.UserId;
//    paymentModel.City = Model.City;
//    paymentModel.Address1 = Model.Address;
//    paymentModel.FirstName = Model.NameOnCard;
//    paymentModel.Phone = Model.Phone;
//    paymentModel.State = Model.State;
//    paymentModel.Zip = Model.Zip;
//    paymentModel.IsDeleted = false;
//    paymentModel.IsPrimary = true;
//    paymentModel.CreatedById = Model.CreatedById;
//    paymentModel.ExMonth = Model.ExpiresMonth;
//    paymentModel.ExYear = Model.ExpiresYear;
//    var result = billingService.Post(paymentModel);
//    if (result == "success")
//    {
//        var curUserDetails = billingService.Get(paymentModel.UserId);
//        var PaymentProfileId = curUserDetails.Where(x => x.UserId == Model.UserId && x.IsPrimary == true).SingleOrDefault().AnPaymentProfileId;

//        var CurUserProfile = userInfoServices.Get(Model.UserId);
//        var ProfileId = CurUserProfile.AuthProfileId;
//        using (PaymentSession paySession = new PaymentSession())
//        {

//            tranres = paySession.ChargeCustomerProfile(PaymentProfileId, ProfileId, Convert.ToDecimal(Model.AmountCharged), Model.OrderID);
//            if (paySession.Status && tranres.ResponseCode == "1")
//            {
//                AnTransactionLog transLog = new AnTransactionLog();
//                transLog.AnTransactionId = tranres.TransId;
//                transLog.TransactionDate = DateTime.Now;
//                transLog.CreatedBy = "AdminInvoice";
//                transLog.Message = "approval Code: " + tranres.ApprovalCode;
//                transLog.TransactionType = "Credit Card";
//                transLog.Amount = Model.AmountCharged.ToString();
//                transLog.TransactionDate = DateTime.Now;
//                transLog.CreatedDate = DateTime.Now;
//                transLog.PaymentProfileId = PaymentProfileId;
//                transLog.UserId = Model.UserId;
//                transLog.PaymentProfileId = PaymentProfileId;
//                _context.AnTransactionLogs.Add(transLog);


//                // create invoice 
//                var invoiceModel = new Invoice();
//                invoiceModel.InvoiceID = Model.InvoiceID;
//                invoiceModel.UserId = Model.UserId;
//                invoiceModel.AmountCharged = Model.AmountCharged;
//                invoiceModel.OrderID = tranres.TransId;
//                invoiceModel.CardNumber = Model.CardNumber;
//                invoiceModel.ExpiresMonth = Model.ExpiresMonth.ToString();
//                invoiceModel.ExpiresYear = Model.ExpiresYear.ToString();
//                invoiceModel.NameOnCard = Model.NameOnCard;
//                invoiceModel.Phone = Model.Phone;
//                invoiceModel.Address = Model.Address;
//                invoiceModel.City = Model.City;
//                invoiceModel.State = Model.State;
//                invoiceModel.Zip = Model.Zip;
//                invoiceModel.CreatedById = Model.CreatedById;
//                invoiceModel.CreatedDate = DateTime.Now;
//                invoiceModel.AnTransactionId = tranres.TransId;
//                invoiceModel.InvoiceType = Model.InvoiceType;
//                _context.Invoices.Add(invoiceModel);
//                _context.SaveChanges();

//                return "success";
//            }
//            else
//            {
//                return paySession.Message;
//            }
//        }
//    }
//    else

//    {
//        return "error";
//    }

//}
//else
//{
//    using (PaymentSession paySession = new PaymentSession())
//    {
//        var curUserDetails = billingService.Get(Model.UserId);
//        var PaymentProfileId = curUserDetails.Where(x => x.UserId == Model.UserId && x.IsPrimary == true).SingleOrDefault().AnPaymentProfileId;

//        var CurUserProfile = userInfoServices.Get(Model.UserId);
//        var ProfileId = CurUserProfile.AuthProfileId;

//        tranres = paySession.ChargeCustomerProfile(PaymentProfileId, ProfileId, Convert.ToDecimal(Model.AmountCharged), Model.OrderID);
//        if (paySession.Status && tranres.ResponseCode == "1")
//        {
//            AnTransactionLog transLog = new AnTransactionLog();
//            transLog.AnTransactionId = tranres.TransId;
//            transLog.TransactionDate = DateTime.Now;
//            transLog.CreatedBy = "AdminInvoice";
//            transLog.Message = "approval Code: " + tranres.ApprovalCode;
//            transLog.TransactionType = "Credit Card";
//            transLog.Amount = Model.AmountCharged.ToString();
//            transLog.TransactionDate = DateTime.Now;
//            transLog.CreatedDate = DateTime.Now;
//            transLog.PaymentProfileId = PaymentProfileId;
//            transLog.UserId = Model.UserId;
//            _context.AnTransactionLogs.Add(transLog);


//            // create invoice 
//            var invoiceModel = new Invoice();
//            invoiceModel.InvoiceID = Model.InvoiceID;
//            invoiceModel.UserId = Model.UserId;
//            invoiceModel.AmountCharged = Model.AmountCharged;
//            invoiceModel.OrderID = tranres.TransId;
//            invoiceModel.CardNumber = Model.CardNumber;
//            invoiceModel.ExpiresMonth = Model.ExpiresMonth.ToString();
//            invoiceModel.ExpiresYear = Model.ExpiresYear.ToString();
//            invoiceModel.NameOnCard = Model.NameOnCard;
//            invoiceModel.Phone = Model.Phone;
//            invoiceModel.Address = Model.Address;
//            invoiceModel.City = Model.City;
//            invoiceModel.State = Model.State;
//            invoiceModel.Zip = Model.Zip;
//            invoiceModel.CreatedById = Model.CreatedById;
//            invoiceModel.CreatedDate = DateTime.Now;
//            invoiceModel.AnTransactionId = tranres.TransId;
//            invoiceModel.InvoiceType = Model.InvoiceType;
//            _context.Invoices.Add(invoiceModel);
//            _context.SaveChanges();

//            return "success";
//        }
//        else
//        {
//            return paySession.Message;
//        }

//    }
//}
