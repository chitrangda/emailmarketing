﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EM.WebAPI.Services
{
    public class IndustryService : IIndustryService
    {
        private readonly EmailMarketingDbEntities _context = new EmailMarketingDbEntities();

        public List<IndustryViewModel> Get()
        {

            var industryListResult = _context.Industries.ToList();
            var industryList = (from ss in industryListResult
                                select new IndustryViewModel
                                {
                                    IndustryID = ss.IndustryID,
                                    IndustryName = ss.IndustryName
                                }).ToList();
            return industryList;


        }
    }
}