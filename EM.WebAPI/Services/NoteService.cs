﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class NoteService : INoteService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public List<usp_getUserNoteList_Result> Get(string id)
        {
            return db.usp_getUserNoteList(id).ToList();
        }

        public bool Post(SalesPortalRepNote salesPortalRepNote)
        {
            if(salesPortalRepNote.NoteId == 0)
            {
                SalesPortalRepNote notes = new SalesPortalRepNote();
                notes.UserId = salesPortalRepNote.UserId;
                notes.NoteDescription = salesPortalRepNote.NoteDescription;
                notes.FollowUpDate = salesPortalRepNote.FollowUpDate;
                notes.CreatedById = salesPortalRepNote.CreatedById;
                notes.CreatedDate = DateTime.UtcNow;
                notes.IsDelete = false;
                notes.NoteTypeId = Convert.ToInt32(salesPortalRepNote.NoteTypeId);
                notes.TimeZone = salesPortalRepNote.TimeZone;
                db.SalesPortalRepNotes.Add(notes);
                db.SaveChanges();
                return true;

            }
            else
            {
                var note = db.SalesPortalRepNotes.Find(salesPortalRepNote.NoteId);
                if (note != null)
                {
                    note.UserId = salesPortalRepNote.UserId;
                    note.NoteDescription = salesPortalRepNote.NoteDescription;
                    note.FollowUpDate = salesPortalRepNote.FollowUpDate;
                    note.LastModById = salesPortalRepNote.LastModById;
                    note.LastModDate = DateTime.UtcNow;
                    note.IsDelete = false;
                    note.NoteTypeId = Convert.ToInt32(salesPortalRepNote.NoteTypeId);
                    note.TimeZone = salesPortalRepNote.TimeZone;
                    db.SaveChanges();

                }
                return false;
            }
        }

        public bool Delete(int id,string userid)
        {
            var note = db.SalesPortalRepNotes.Find(id);
            if(note!=null)
            {
                note.IsDelete = true;
                note.LastModById = userid;
                note.LastModDate = DateTime.UtcNow;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}