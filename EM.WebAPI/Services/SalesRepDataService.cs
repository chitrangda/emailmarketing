﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class SalesRepDataService : ISalesRepDataService
    {
        private readonly EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        EmailPlanService emailPlan = new EmailPlanService();
        public List<usp_getSalesRepDashboardData_Result> GetSalesRepData(string id)
        {
            return db.usp_getSalesRepDashboardData(id, null, null, null, null, null, null, null, null, null).ToList();
        }

        public usp_getSalesRepTodays_Result Get(string id)
        {
            return db.usp_getSalesRepTodays(id).FirstOrDefault();
        }

        public List<usp_getSalesRepDashboardData_Result> GetFilterSalesRepData(string id, string key, string value, string clientType, string MemberType)
        {
            if (MemberType != null)
            {
                return db.usp_getSalesRepDashboardData(id, null, null, null, null, null, null, null, MemberType, null).ToList();

            }
            else
            {
                if (key == "Name")
                {
                    return db.usp_getSalesRepDashboardData(id, value, null, null, null, null, null, clientType, null, null).ToList();
                }
                else if (key == "Email")
                {
                    return db.usp_getSalesRepDashboardData(id, null, value, null, null, null, null, clientType, null, null).ToList();
                }
                else if (key == "MobilePhone")
                {
                    return db.usp_getSalesRepDashboardData(id, null, null, value, null, null, null, clientType, null, null).ToList();
                }
                else if (key == "CardName")
                {
                    return db.usp_getSalesRepDashboardData(id, null, null, null, value, null, null, clientType, null, null).ToList();
                }
                else if (key == "City")
                {
                    return db.usp_getSalesRepDashboardData(id, null, null, null, null, value, null, clientType, null, null).ToList();
                }
                else if (key == "State")
                {
                    return db.usp_getSalesRepDashboardData(id, null, null, null, null, null, value, clientType, null, null).ToList();
                }
                else
                {
                    return db.usp_getSalesRepDashboardData(id, null, null, null, null, null, null, clientType, null, null).ToList();

                }
            }
        }

        public List<SalesRepProfile> Get()
        {
            var salesRepListResult = db.SalesRepProfiles.ToList();
            var salesRepList = (from ss in salesRepListResult
                                select new SalesRepProfile
                                {
                                    Id = ss.Id,
                                    FirstName = ss.FirstName,
                                    ContactNo = ss.ContactNo
                                }).ToList();
            return salesRepList;
        }

        public usp_getUserDetails_Result GetClientData(string id)
        {
            return db.usp_getUserDetails(id).FirstOrDefault();
        }

        public bool UpdateClientData(usp_getUserDetails_Result model)
        {
            if (model != null)
            {
                var userProfileDetails = db.UserProfiles.Find(model.Id);
                var userPlanDetails = db.UserPlans.FirstOrDefault(r => r.UserId == model.Id && r.IsActive == true);
                //var userNoteDetails = db.SalesPortalRepNotes.Where(r => r.UserId == model.Id && r.CreatedById == userProfileDetails.SalesRepId).OrderByDescending(r=>r.LastModDate).ThenByDescending(r=>r.CreatedDate).FirstOrDefault();
                if (userProfileDetails != null)
                {
                    if (model.SalesRepId != null && (model.SalesRepId != userProfileDetails.SalesRepId || userProfileDetails.SalesRepId == null))
                    {
                        string oldSalesRepId = userProfileDetails.SalesRepId;
                        var oldSlaesRepDetails = db.SalesRepProfiles.Find(oldSalesRepId);
                        if (oldSlaesRepDetails != null)
                        {
                            oldSlaesRepDetails.NoOfClients = oldSlaesRepDetails.NoOfClients - 1;
                        }
                        var newSlaesRepDetails = db.SalesRepProfiles.Find(model.SalesRepId);
                        if (newSlaesRepDetails != null)
                        {
                            newSlaesRepDetails.NoOfClients = newSlaesRepDetails.NoOfClients + 1;
                        }

                        userProfileDetails.SalesRepId = model.SalesRepId;

                    }
                    if (model.FollowUpDate != null)
                    {
                        DateTime t;
                        DateTime.TryParse(model.FollowUpDate, out t);
                        userProfileDetails.FollowupDate = t;
                        userProfileDetails.FollowupTime = t;
                    }
                    userProfileDetails.IsActive = model.IsActive;
                    userProfileDetails.ClientType = model.ClientType;
                    userProfileDetails.TimeZoneId = model.TimeZoneID;
                    userProfileDetails.IPPoolId = model.IPPoolId;
                    userProfileDetails.IsCSVActive = model.IsCSVActive;

                }

                if (userPlanDetails != null)
                {

                    if (model.PackageId != userPlanDetails.PackageId)
                    {
                        var _curPlan = db.UserPlans.Where(r => r.UserId == model.Id && r.IsActive == true).FirstOrDefault();
                        if (_curPlan != null)
                        {
                            _curPlan.IsActive = false;

                        }
                        var planDetail = emailPlan.GetPlanByID(model.PackageId.Value, model.Id);

                        UserPlan oPlan = new UserPlan();
                        oPlan.UserId = model.Id;
                        oPlan.TotalEmails = planDetail.NoOfCredits;
                        oPlan.TotalDays = 30;
                        oPlan.PackageId = model.PackageId.Value;
                        oPlan.MonthlyCredit = planDetail.NoOfCredits.ToString();
                        oPlan.NoOfSubscribersMin = 0;
                        oPlan.NoOfSubscribersMax = planDetail.NoOfSubscribersMax;
                        oPlan.PackageUpdateDate = DateTime.Now;
                        oPlan.BillingAmount = planDetail.BillingAmount != null ? planDetail.BillingAmount.Value : 0;
                        oPlan.totalRemainingCredits = planDetail.NoOfCredits.ToString();
                        oPlan.ContactManagement = planDetail.ContactManagement;
                        oPlan.UnlimitedEmails = planDetail.UnlimitedEmails;
                        oPlan.EmailScheduling = planDetail.EmailScheduling;
                        oPlan.EducationalResources = planDetail.EducationalResources;
                        oPlan.LiveSupport = planDetail.LiveSupport;
                        oPlan.CustomizableTemplates = planDetail.CustomizableTemplates;
                        oPlan.IsActive = true;
                        oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                        oPlan.BillingStatus = 1;
                        oPlan.totalSpendCredits = "0";
                        db.UserPlans.Add(oPlan);
                        db.SaveChanges();
                    }
                    userPlanDetails = db.UserPlans.Where(x => x.UserId == model.Id && x.IsActive == true).FirstOrDefault();
                    if (model.NextPackageUpdateDate != null)
                    {
                        userPlanDetails.NextPackageUpdateDate = model.NextPackageUpdateDate;
                    }
                    if (model.MonthlyCredit == "Unlimited Emails")
                    {
                        var credits = 0;
                        userPlanDetails.MonthlyCredit = credits.ToString();
                        userPlanDetails.TotalEmails = credits;
                        userPlanDetails.totalRemainingCredits = credits.ToString();
                    }
                    else
                    {
                        userPlanDetails.MonthlyCredit = Convert.ToString(model.MonthlyCredit);
                        userPlanDetails.totalRemainingCredits = Convert.ToString(model.MonthlyCredit);
                        userPlanDetails.TotalEmails = Convert.ToInt32(model.MonthlyCredit);
                    }
                    userPlanDetails.BillingAmount = model.BillingAmount;
                    if (model.PackageUpdateDate != null)
                    {
                        userPlanDetails.PackageUpdateDate = Convert.ToDateTime(model.PackageUpdateDate);

                    }

                }

                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<usp_getSalesRepData_Result> GetData(string id, DateTime? FromDate, DateTime? ToDate, int? Year, string Quarter, int? Month, string City, string State, string Zip, string TimeZone, string Find, int? MemberType, bool? Status, int? CampaignType)
        {
            return db.usp_getSalesRepData(id, FromDate, ToDate, Year, Quarter, Month, City, State, Zip, TimeZone, Find, MemberType, Status, CampaignType).ToList();
        }

    }
}