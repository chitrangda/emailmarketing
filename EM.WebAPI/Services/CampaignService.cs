﻿using EM.WebAPI.Interfaces;
using EM.WebAPI.Utilities;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Helpers;
using EM.Factory;
using EM.Factory.ViewModels;


namespace EM.WebAPI.Services
{
    public class CampaignService : ICampaignService
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();

        public List<usp_getCampaignList_Result> Get(string id, string Search, int? pageNumber, int? pageSize, string orderBy)
        {
            var creatorName = _context.UserProfiles.Where(x => x.Id == id).Select(x => new { Name = x.FirstName + " " + x.LastName }).FirstOrDefault();
            var campaigns = _context.usp_getCampaignList(id, Search, pageNumber, pageSize, orderBy).ToList();
            return campaigns;
        }

        public CampaignViewModel Get(int? id)
        {
            if (id == null || id < 1)
            {
                return new CampaignViewModel();
            }
            List<int> ListIds = new List<int>();
            List<string> listnames = new List<string>();
            CampaignViewModel campaignViewModel = new CampaignViewModel();
            var campaigns = _context.Campaigns.SingleOrDefault(x => x.CampaignId == id);
            if (campaigns.ListId != null)
            {
                if (campaigns.ListId.Contains(","))
                {
                    ListIds = campaigns.ListId.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    ListIds.Add(Convert.ToInt32(campaigns.ListId));
                }
                foreach (var listid in ListIds)
                {
                    var ListName = _context.Lists.Where(r => r.ListId == listid).Select(r => r.ListName).FirstOrDefault();
                    listnames.Add(ListName);
                }
                var emails = _context.usp_getEmailsbyListId(String.Join(",", ListIds.ToArray())).ToArray();
                campaignViewModel.ListEmails = String.Join(",", emails);
            }
            string result = String.Join(",", listnames.ToArray());
            campaignViewModel.CampaignId = campaigns.CampaignId;
            campaignViewModel.CampaignName = campaigns.CamapignName;
            campaignViewModel.ListId = campaigns.ListId;
            campaignViewModel.FromName = campaigns.FromName;
            campaignViewModel.FromEmail = campaigns.FromEmail;
            campaignViewModel.Subject = campaigns.Subject;
            campaignViewModel.PreviewText = campaigns.PreviewText;
            campaignViewModel.UserId = campaigns.UserId;
            campaignViewModel.CreatedById = campaigns.CreatedById;
            campaignViewModel.CreatedDate = campaigns.CreatedDate;
            campaignViewModel.Status = campaigns.Status;
            campaignViewModel.KeywordId = campaigns.KeywordId;
            campaignViewModel.CampaignTypeID = campaigns.CampaignTypeID;
            campaignViewModel.TemplateId = Convert.ToInt32(campaigns.TemplateId);
            campaignViewModel.JSONContent = campaigns.JSONContent;
            campaignViewModel.HtmlContent = campaigns.HtmlContent;
            campaignViewModel.ContentImagePath = campaigns.ContentImagePath;
            campaignViewModel.SubscriberIds = campaigns.SubscriberIds;
            campaignViewModel.SendingFromEmail = campaigns.SendingFromEmail;
            campaignViewModel.ListName = result;
            if (campaigns.Status.ToLower() == "schedule")
            {
                var scheduleCampaign = _context.ScheduledCampaigns.Where(x => x.CampaignId == campaigns.CampaignId).FirstOrDefault();
                if (scheduleCampaign != null)
                {
                    campaignViewModel.ScheduleDate = Convert.ToDateTime(scheduleCampaign.SDateTime);
                    campaignViewModel.TimeZoneID = Convert.ToInt32(scheduleCampaign.TimeZoneID);
                }
            }
            return campaignViewModel;
        }

        public ListViewModel GetListDetailsByListId(int id)
        {
            var listDetails = _context.Lists.Where(x => x.ListId == id).FirstOrDefault();
            ListViewModel listViewModel = new ListViewModel();
            listViewModel.FromEmail = listDetails.FromEmail;
            listViewModel.FromName = listDetails.FromName;
            listViewModel.ListId = listDetails.ListId;
            return listViewModel;
        }

        public CampaignViewModel Post(CampaignViewModel model)
        {
            using (var context = new EmailMarketingDbEntities())
            {

                //var timezoneDetails = context.TimeZones.Where(x => x.TimeZoneID == model.TimeZoneID).FirstOrDefault();
                //System.TimeZone localZone = System.TimeZone.CurrentTimeZone;
                //DateTime currentDate = DateTime.Now;
                //int currentYear = currentDate.Year;
                //System.Globalization.DaylightTime daylight = localZone.GetDaylightChanges(currentYear);

                context.Database.CommandTimeout = 180;
                var campaign = context.Campaigns.Where(x => x.CampaignId == model.CampaignId || x.CamapignName == model.CampaignName).FirstOrDefault();
                //if (model.ListId != null)
                //{
                //    //var subscriberData = Getsubscriberjoinids(model.ListId); ;
                //    //model.SubscriberIds = subscriberData.SubscriberIds;
                //    //model.SubscriberCount = subscriberData.SubscriberCount != null ? subscriberData.SubscriberCount.Value : 0;
                //     //model.SubscriberIds = model.UniqueSubscriberIds;
                //     //model.SubscriberCount =model.UniqueSubscriberIdsCount;
                //    //var listIdsArr = model.ListId.Split(',').ToList();
                //    //var t = context.SubscriberLists.Where(r => listIdsArr.Contains(r.ListId.ToString()));
                //    //var subscriberIdArr = context.SubscriberLists.Where(r => listIdsArr.Contains(r.ListId.ToString()) && r.IsSubscribe == true && r.IsDeleted == false).Select(r => r.SubscriberId).Distinct().ToArray();
                //    //model.SubscriberIds = string.Join(",", subscriberIdArr);
                //}
                if (campaign != null)
                {
                    campaign.CamapignName = model.CampaignName;
                    campaign.ListId = model.ListId;
                    campaign.FromName = model.FromName;
                    campaign.FromEmail = model.FromEmail;
                    campaign.Subject = model.Subject;
                    campaign.PreviewText = model.PreviewText;
                    campaign.UserId = model.UserId;
                    campaign.LastModById = model.CreatedById;
                    campaign.ContentImagePath = model.ContentImagePath;
                    //  campaign.LastModDate = DateTime.UtcNow.AddMinutes(model.Offset);
                    campaign.LastModDate = DateTime.Now;
                    campaign.Status = model.Status;
                    campaign.KeywordId = model.KeywordId;
                    campaign.CampaignTypeID = model.CampaignTypeID;
                    campaign.TemplateId = model.TemplateId == 0 ? null : model.TemplateId;
                    campaign.JSONContent = model.JSONContent == null ? string.Empty : model.JSONContent;
                    campaign.HtmlContent = model.HtmlContent == null ? string.Empty : model.HtmlContent;
                    campaign.SubscriberIds = model.UniqueSubscriberIds;
                    campaign.SubscriberCount = model.UniqueSubscriberIdsCount;
                    campaign.SendingFromEmail = model.SendingFromEmail;
                    context.SaveChanges();



                    if (model.Status.ToLower() == "schedule")
                    {
                        var scheduleCampaign = context.ScheduledCampaigns.Where(x => x.CampaignId == model.CampaignId).FirstOrDefault();
                        if (scheduleCampaign != null)
                        {
                            scheduleCampaign.CampaignId = campaign.CampaignId;
                            scheduleCampaign.ScheduledDateTime = model.ScheduleDate;
                            //if (localZone.IsDaylightSavingTime(DateTime.Now))
                            //{
                            //    scheduleCampaign.ScheduledDateTime = model.ScheduleDate.AddHours(-Convert.ToDouble(timezoneDetails.GMT) - 1);
                            //}
                            //else
                            //{
                            //    scheduleCampaign.ScheduledDateTime = model.ScheduleDate.AddHours(-Convert.ToDouble(timezoneDetails.GMT));
                            //}
                            scheduleCampaign.SDateTime = model.SDateTime;
                            scheduleCampaign.TimeZoneID = model.TimeZoneID;
                            scheduleCampaign.Status = CampaignStatus.Queued;
                            context.SaveChanges();
                        }
                        else
                        {
                            ScheduledCampaign Model = new ScheduledCampaign();
                            Model.CampaignId = campaign.CampaignId;
                            Model.ScheduledDateTime = model.ScheduleDate;
                            Model.SDateTime = model.SDateTime;
                            Model.TimeZoneID = model.TimeZoneID;
                            Model.Status = CampaignStatus.Queued;
                            context.ScheduledCampaigns.Add(Model);
                            context.SaveChanges();
                        }
                    }
                    else if (model.Status.ToLower() == "queued")//Added this code to schedule campaign for service when user chooses to send email.
                    {
                        var scheduleCampaign = context.ScheduledCampaigns.Where(x => x.CampaignId == model.CampaignId).FirstOrDefault();
                        if (scheduleCampaign != null)
                        {
                            scheduleCampaign.CampaignId = campaign.CampaignId;
                            scheduleCampaign.ScheduledDateTime = DateTime.Now;
                            scheduleCampaign.Status = CampaignStatus.Queued;
                            context.SaveChanges();
                        }
                        else
                        {
                            ScheduledCampaign Model = new ScheduledCampaign();
                            Model.CampaignId = campaign.CampaignId;
                            Model.ScheduledDateTime = DateTime.Now;
                            Model.Status = CampaignStatus.Queued;
                            context.ScheduledCampaigns.Add(Model);
                            context.SaveChanges();
                        }
                    }

                    // save each subscriber of campaign in table
                    if (model.Status.ToLower() == "queued" || model.Status.ToLower() == "schedule")
                    {
                        context.usp_insertCampaignSubscribers(model.CampaignId, model.UniqueSubscriberIds, model.UserId);
                    }
                    return model;
                }
                else
                {
                    campaign = new Campaign();
                    campaign.CamapignName = model.CampaignName;
                    campaign.ListId = model.ListId;
                    campaign.FromName = model.FromName;
                    campaign.FromEmail = model.FromEmail;
                    campaign.Subject = model.Subject;
                    campaign.PreviewText = model.PreviewText;
                    campaign.UserId = model.UserId;
                    campaign.CreatedById = model.CreatedById;
                    // campaign.CreatedDate = DateTime.UtcNow.AddMinutes(model.Offset);
                    campaign.CreatedDate = DateTime.Now;
                    //if(model.Status == "Queued")
                    //{
                    //    var domain = context.VerifiedDomains.Where(x => x.)
                    //}

                    campaign.Status = model.Status;
                    campaign.KeywordId = model.KeywordId;
                    campaign.ContentImagePath = model.ContentImagePath;
                    campaign.CampaignTypeID = model.CampaignTypeID;
                    campaign.TemplateId = model.TemplateId == 0 ? null : model.TemplateId;
                    campaign.JSONContent = model.JSONContent == null ? string.Empty : model.JSONContent;
                    campaign.HtmlContent = model.HtmlContent == null ? string.Empty : model.HtmlContent;
                    campaign.SubscriberIds = model.UniqueSubscriberIds;
                    campaign.SubscriberCount = model.UniqueSubscriberIdsCount;
                    campaign.LastModDate = DateTime.Now;
                    campaign.SendingFromEmail = model.SendingFromEmail;
                    context.Campaigns.Add(campaign);
                    context.SaveChanges();
                    if (model.Status.ToLower() == "schedule")
                    {
                        ScheduledCampaign Model = new ScheduledCampaign();
                        Model.CampaignId = campaign.CampaignId;
                        Model.ScheduledDateTime = model.ScheduleDate;
                        Model.SDateTime = model.SDateTime;
                        Model.TimeZoneID = model.TimeZoneID;
                        Model.Status = CampaignStatus.Queued;
                        context.ScheduledCampaigns.Add(Model);
                        context.SaveChanges();
                    }
                    else if (model.Status.ToLower() == "queued")//Added this code to schedule campaign for service when user chooses to send email.
                    {
                        ScheduledCampaign Model = new ScheduledCampaign();
                        Model.CampaignId = campaign.CampaignId;
                        //Model.ScheduledDateTime = DateTime.Now.AddMinutes(model.Offset);
                        Model.ScheduledDateTime = DateTime.Now;
                        Model.Status = CampaignStatus.Queued;
                        context.ScheduledCampaigns.Add(Model);
                        context.SaveChanges();
                    }
                    model.CampaignId = campaign.CampaignId;

                    // save each subscriber of campaign in table
                    if (model.Status.ToLower() == "queued" || model.Status.ToLower() == "schedule")
                    {
                        context.usp_insertCampaignSubscribers(model.CampaignId, model.UniqueSubscriberIds, model.UserId);
                    }
                    return model;
                }



            }
        }



        //public bool Post(Scheduledmodel scheduledmodel)
        //{
        //    try
        //    {
        //        var scheduled = _context.ScheduledCampaigns.AsEnumerable().Where(x => x.CampaignId == scheduledmodel.CampaignId).FirstOrDefault();
        //        if (scheduled == null)
        //        {
        //            scheduled.CampaignId = scheduledmodel.CampaignId;
        //            scheduled.ScheduledDateTime = scheduledmodel.ScheduledDateTime;
        //            _context.ScheduledCampaigns.Add(scheduled);
        //            _context.SaveChanges();
        //            return true;
        //        }
        //        else
        //        {

        //        }
        //        return true;
        //    }
        //    catch(Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public List<string> Get(string userid)
        {
            return _context.Campaigns.Where(r => r.UserId == userid).Select(r => r.CampaignId.ToString()).ToList();
        }

        public List<usp_getSendCampaignList_Result> GetSendCampaign(string userid, int? pageNumber, int? pageSize, DateTime? fromDate = null, DateTime? toDate = null)
        {
            return _context.usp_getSendCampaignList(userid, fromDate, toDate, pageNumber, pageSize, null).ToList();
        }

        public List<usp_getCampaignReport_Result> GetCampaigns(string userid, int? pageNumber, int? pageSize, DateTime? fromDate = null, DateTime? toDate = null)
        {
            return _context.usp_getCampaignReport(userid, fromDate, toDate, pageNumber, pageSize).ToList();
        }

        public usp_getSendCampaignList_Result GetSendCampaignDetails(string userid, int? CampaignId)
        {
            return _context.usp_getSendCampaignList(userid, null, null, 1, null, CampaignId).SingleOrDefault();
        }

        public List<usp_getCampaignRecipient_Result> GetRecipientsEmails(int CampaignId, int? pageNumber, int? PageSize)
        {
            return _context.usp_getCampaignRecipient(CampaignId, pageNumber, PageSize).ToList();
        }

        public List<usp_CampaignUnsubscriber_Result> GetUnSubscribeEmails(int CampaignId, string UserId, int? pageNumber, int? PageSize, string EmailSearch)
        {
            return _context.usp_CampaignUnsubscriber(CampaignId, UserId, pageNumber, PageSize, EmailSearch).ToList();
        }
        public List<usp_getCampaignActivityStat_Result> GetCampaignStat(int CamapignId, string Stat, int PageNumber, int? PageSize, string Email)
        {
            return _context.usp_getCampaignActivityStat(CamapignId, Stat, PageNumber, PageSize, Email).ToList();
        }

        public List<usp_getCampaignLinkStat_Result> GetCampaignLinkStat(int CampaignId)
        {
            return _context.usp_getCampaignLinkStat(CampaignId).ToList();
        }

        public usp_getActivityStat_Result Stat(int CampaignId, string Email)
        {
            return _context.usp_getActivityStat(CampaignId, Email).SingleOrDefault();
        }

        public usp_getJoinSubscriberIds_Result Getsubscriberjoinids(string ListId)
        {
            usp_getJoinSubscriberIds_Result model = new usp_getJoinSubscriberIds_Result();
            model = _context.usp_getJoinSubscriberIds(ListId).SingleOrDefault();
            return model;
        }

        public usp_getSingleCampaignReport_Result SingleCampaignReport(string userId, int CampaignId)
        {
            return _context.usp_getSingleCampaignReport(userId, CampaignId).SingleOrDefault();
        }


        public List<usp_getReportChartData_Result> GetReportChart(DateTime? dateFrom, DateTime? dateTo, string aggregated_by, int CampaignId)
        {

                return _context.usp_getReportChartData(dateFrom, dateTo, aggregated_by, CampaignId).ToList();
          
        }

        public List<usp_getDidNotOpenEmails_Result> GetDidntEmail(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch)
        {
            return _context.usp_getDidNotOpenEmails(CampaignId, UserId, PageNumber, PageSize, EmailSearch).ToList();
        }

        public List<usp_getSuppressedSubscribers_Result> GetSuppressedEmails(int CampaignId, int PageNumber, int? PageSize, string EmailSearch)
        {
            return _context.usp_getSuppressedSubscribers(CampaignId, PageNumber, PageSize, EmailSearch).ToList();
        }

        public usp_UnSubscribefromCampaign_Result Put(int id, string email, string uid)
        {

            var result =  _context.usp_UnSubscribefromCampaign(id, email, uid).SingleOrDefault();
            return result;
            
        }

        public List<usp_getDeliveredEmails_Result> GetDeliveredEmail(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch)
        {
            return _context.usp_getDeliveredEmails(CampaignId, UserId,PageNumber, PageSize, EmailSearch).ToList();
        }

        public List<usp_getRecipientData_Result> GetSubscriberEmail(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch)
        {
            return _context.usp_getRecipientData(CampaignId, UserId, PageNumber, PageSize, EmailSearch).ToList();
        }

        //public CampaignViewModel GetRecipientsEmails(int CampaignId)
        //{
        //    CampaignViewModel campaignViewModel = new CampaignViewModel();
        //    List<int> SubscriberIds = new List<int>();
        //    List<string> SubscriberEmail = new List<string>();
        //    string SubsId= _context.Campaigns.Where(r => r.CampaignId == CampaignId).Select(r => r.SubscriberIds.ToString()).FirstOrDefault();
        //    if (SubsId.Contains(","))
        //    {
        //        SubscriberIds = SubsId.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
        //    }
        //   string SubEmail=null;
        //   foreach(var subid in SubscriberIds)
        //    {
        //        SubEmail = _context.Subscribers.Where(r => r.SubscriberId == subid).Select(r => r.Email).FirstOrDefault();
        //        SubscriberEmail.Add(SubEmail);
        //    }
        //    campaignViewModel.EmailRecipients = SubscriberEmail;
        //   return campaignViewModel;
        //}
    }
}