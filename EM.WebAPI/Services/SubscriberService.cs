﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using MoreLinq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace EM.WebAPI.Services
{
    public class SubscriberService : ISubscriberService
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();

        public bool Delete(List<int> id, int ListId)
        {
            foreach (var subscriberId in id)
            {
                var oSubscriberList = _context.SubscriberLists.FirstOrDefault(r => r.SubscriberId == subscriberId && r.ListId == ListId);
                if (oSubscriberList != null)
                {
                    oSubscriberList.IsDeleted = true;
                }
                //var subscriber = _context.Subscribers.AsEnumerable().Where(x => x.SubscriberId == item).FirstOrDefault();
                //if (subscriber != null)
                //{
                //    var subscribers = _context.SubscriberLists.AsEnumerable().Where(y => y.SubscriberId == item).Where(y => y.ListId == ListId).FirstOrDefault();
                //    subscribers.IsSubscribe = false;
                //}
                //subscriber.IsDeleted = true;
                _context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int? id)
        {
            if (id == null)
            {
                return false;
            }
            try
            {
                var subscriberDelete = _context.SubscriberLists.AsEnumerable().Where(x => x.ListId == id).ToList();
                if (subscriberDelete.Count > 0)
                {
                    subscriberDelete.ForEach(x => x.IsDeleted = true);
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            { return false; }
            return false;
        }

        public List<usp_getSubscriberList_Result> Get(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {

            return _context.usp_getSubscriberList(listId, search, pageNumber, pageSize, orderBy).ToList();
        }

        public SubscriberViewModel Get(int? subscriberId)
        {
            SubscriberViewModel subscriberViewModel = new SubscriberViewModel();
            var subscriber = _context.Subscribers.SingleOrDefault(x => x.SubscriberId == subscriberId);
            if (subscriber != null)
            {
                var DOB = Convert.ToDateTime(subscriber.DOB);
                subscriberViewModel.Email = subscriber.Email;
                subscriberViewModel.FirstName = subscriber.FirstName;
                subscriberViewModel.LastName = subscriber.LastName;
                subscriberViewModel.City = subscriber.City;
                subscriberViewModel.State = subscriber.State;
                subscriberViewModel.Country = subscriber.Country;
                subscriberViewModel.Address1 = subscriber.Address1;
                subscriberViewModel.Address2 = subscriber.Address2;
                subscriberViewModel.ZipCode = subscriber.ZipCode;
                subscriberViewModel.PhoneNumber = subscriber.PhoneNumber;
                subscriberViewModel.DOB = subscriber.DOB;
                subscriberViewModel.ZipCode = subscriber.ZipCode;
                subscriberViewModel.CreatedById = subscriber.CreatedById;
                subscriberViewModel.EmailPermission = Convert.ToBoolean(subscriber.EmailPermission);
            }

            return subscriberViewModel;
        }

        public List<usp_getUnSubscriberList_Result> GetUnsubscribers(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {
            return _context.usp_getUnSubscriberList(listId, search, pageNumber, pageSize, orderBy).ToList();
        }

        public int Post(SubscriberViewModel subscriberViewModel)
        {

            try
            {
                int checkduplicate = 2;

                var subscriber = _context.Subscribers.Where(x => x.Email.ToLower() == subscriberViewModel.Email.Trim().ToLower() && x.CreatedById == subscriberViewModel.CreatedById).FirstOrDefault();
                if (subscriber != null)
                {
                    if (subscriber.IsSubscribed == true)
                    {
                        if (subscriber.IsSuppressed == false)
                        {
                            subscriber.Email = subscriberViewModel.Email != null ? subscriberViewModel.Email.Trim() : subscriberViewModel.Email;
                            if (subscriberViewModel.FirstName != null && subscriberViewModel.FirstName.Trim() != string.Empty)
                            {
                                subscriber.FirstName = subscriberViewModel.FirstName.Trim();
                            }
                            if (subscriberViewModel.LastName != null && subscriberViewModel.LastName.Trim() != string.Empty)
                            {
                                subscriber.LastName = subscriberViewModel.LastName.Trim();
                            }
                            if (subscriberViewModel.City != null && subscriberViewModel.City.Trim() != string.Empty)
                            {
                                subscriber.City = subscriberViewModel.City.Trim();
                            }
                            if (subscriberViewModel.State != null && subscriberViewModel.State.Trim() != string.Empty)
                            {
                                subscriber.State = subscriberViewModel.State.Trim();
                            }
                            if (subscriberViewModel.Country != null && subscriberViewModel.Country.Trim() != string.Empty)
                            {
                                subscriber.Country = subscriberViewModel.Country.Trim();
                            }
                            if (subscriberViewModel.Address1 != null && subscriberViewModel.Address1.Trim() != string.Empty)
                            {
                                subscriber.Address1 = subscriberViewModel.Address1.Trim();
                            }
                            if (subscriberViewModel.Address2 != null && subscriberViewModel.Address2.Trim() != string.Empty)
                            {
                                subscriber.Address2 = subscriberViewModel.Address2.Trim();
                            }
                            if (subscriberViewModel.ZipCode != null && subscriberViewModel.ZipCode.Trim() != string.Empty)
                            {
                                subscriber.ZipCode = subscriberViewModel.ZipCode.Trim();
                            }
                            if (subscriberViewModel.PhoneNumber != null && subscriberViewModel.PhoneNumber.Trim() != string.Empty)
                            {
                                subscriber.PhoneNumber = subscriberViewModel.PhoneNumber.Trim();
                            }
                            if (subscriberViewModel.DOB != null)
                            {
                                subscriber.DOB = subscriberViewModel.DOB;
                            }
                            subscriber.LastModById = subscriberViewModel.CreatedById;
                            subscriber.LastModDate = DateTime.Now;
                            subscriber.EmailPermission = Convert.ToBoolean(subscriberViewModel.EmailPermission);
                            subscriber.IsSuppressed = false;
                            subscriber.IsSubscribed = true;
                            var oSubscriberList = _context.SubscriberLists.Where(r => r.SubscriberId == subscriber.SubscriberId && r.ListId == subscriberViewModel.ListId).SingleOrDefault();
                            if (oSubscriberList == null)
                            {
                                checkduplicate = 2;
                                SubscriberList subscriberList = new SubscriberList();
                                subscriberList.ListId = subscriberViewModel.ListId;
                                subscriberList.SubscriberId = subscriber.SubscriberId;
                                subscriberList.CreatedDate = DateTime.Now;
                                subscriberList.CreatedById = subscriberViewModel.CreatedById;
                                //subscriberList.IsSubscribe = true;
                                subscriberList.IsDeleted = false;
                                _context.SubscriberLists.Add(subscriberList);
                            }
                            else
                            {
                                if (oSubscriberList.IsDeleted == false)
                                {
                                    checkduplicate = 2;
                                }
                                else
                                {
                                    oSubscriberList.IsDeleted = false;
                                }
                            }

                            _context.SaveChanges();
                            return checkduplicate;
                        }
                        else
                        {
                            return 3;
                        }
                    }
                    else
                    {
                        return 4;
                    }
                }
                else
                {
                    checkduplicate = 1;
                    subscriber = new Subscriber();
                    subscriber.Email = subscriberViewModel.Email != null ? subscriberViewModel.Email.Trim() : subscriberViewModel.Email;
                    if (subscriberViewModel.FirstName != null && subscriberViewModel.FirstName.Trim() != string.Empty)
                    {
                        subscriber.FirstName = subscriberViewModel.FirstName.Trim();
                    }
                    if (subscriberViewModel.LastName != null && subscriberViewModel.LastName.Trim() != string.Empty)
                    {
                        subscriber.LastName = subscriberViewModel.LastName.Trim();
                    }
                    if (subscriberViewModel.City != null && subscriberViewModel.City.Trim() != string.Empty)
                    {
                        subscriber.City = subscriberViewModel.City.Trim();
                    }
                    if (subscriberViewModel.State != null && subscriberViewModel.State.Trim() != string.Empty)
                    {
                        subscriber.State = subscriberViewModel.State.Trim();
                    }
                    if (subscriberViewModel.Country != null && subscriberViewModel.Country.Trim() != string.Empty)
                    {
                        subscriber.Country = subscriberViewModel.Country.Trim();
                    }
                    if (subscriberViewModel.Address1 != null && subscriberViewModel.Address1.Trim() != string.Empty)
                    {
                        subscriber.Address1 = subscriberViewModel.Address1.Trim();
                    }
                    if (subscriberViewModel.Address2 != null && subscriberViewModel.Address2.Trim() != string.Empty)
                    {
                        subscriber.Address2 = subscriberViewModel.Address2.Trim();
                    }
                    if (subscriberViewModel.ZipCode != null && subscriberViewModel.ZipCode.Trim() != string.Empty)
                    {
                        subscriber.ZipCode = subscriberViewModel.ZipCode.Trim();
                    }
                    if (subscriberViewModel.PhoneNumber != null && subscriberViewModel.PhoneNumber.Trim() != string.Empty)
                    {
                        subscriber.PhoneNumber = subscriberViewModel.PhoneNumber.Trim();
                    }
                    if (subscriberViewModel.DOB != null)
                    {
                        subscriber.DOB = subscriberViewModel.DOB;
                    }
                    subscriber.CreatedById = subscriberViewModel.CreatedById;
                    subscriber.CreatedDate = DateTime.Now;
                    subscriber.EmailPermission = subscriberViewModel.EmailPermission;
                    subscriber.IsSuppressed = false;
                    subscriber.IsSubscribed = true;
                    _context.Subscribers.Add(subscriber);
                    _context.SaveChanges();
                    SubscriberList subscriberList = new SubscriberList();
                    subscriberList.ListId = subscriberViewModel.ListId;
                    subscriberList.SubscriberId = subscriber.SubscriberId;
                    subscriberList.CreatedDate = DateTime.Now;
                    subscriberList.CreatedById = subscriberViewModel.CreatedById;
                    //subscriberList.IsSubscribe = true;
                    subscriberList.IsDeleted = false;
                    _context.SubscriberLists.Add(subscriberList);
                    _context.SaveChanges();
                }

                return checkduplicate;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public List<SubscriberViewModel> Post(List<SubscriberViewModel> subscriberViewModels)
        {
            List<SubscriberViewModel> subs = new List<SubscriberViewModel>();
            foreach (var subscriberViewModel in subscriberViewModels)
            {
                var checkSubscriber = _context.Subscribers.Where(x => x.Email == subscriberViewModel.Email && x.CreatedById == subscriberViewModel.CreatedById).FirstOrDefault();
                if (checkSubscriber == null)
                {
                    var subscriber = new Subscriber();
                    subscriber.Email = subscriberViewModel.Email;
                    subscriber.FirstName = subscriberViewModel.FirstName;
                    subscriber.LastName = subscriberViewModel.LastName;
                    subscriber.City = subscriberViewModel.City;
                    subscriber.State = subscriberViewModel.State;
                    subscriber.Country = subscriberViewModel.Country;
                    subscriber.Address1 = subscriberViewModel.Address1;
                    subscriber.Address2 = subscriberViewModel.Address2;
                    subscriber.ZipCode = subscriberViewModel.ZipCode;
                    subscriber.PhoneNumber = subscriberViewModel.PhoneNumber;
                    subscriber.DOB = subscriberViewModel.DOB;
                    subscriber.ZipCode = subscriberViewModel.ZipCode;
                    subscriber.CreatedById = subscriberViewModel.CreatedById;
                    subscriber.CreatedDate = DateTime.UtcNow;
                    subscriber.EmailPermission = subscriberViewModel.EmailPermission;
                    subscriber.IsActive = true;
                    _context.Subscribers.Add(subscriber);
                    _context.SaveChanges();
                    SubscriberList subscriberList = new SubscriberList();
                    subscriberList.ListId = subscriberViewModel.ListId;
                    subscriberList.SubscriberId = subscriber.SubscriberId;
                    //  subscriberList.IsSubscribe = true;
                    subscriberList.IsDeleted = false;
                    subscriberList.CreatedDate = DateTime.Now;
                    subscriberList.CreatedById = subscriberViewModel.CreatedById;
                    subscriberList.LastModDate = DateTime.Now;
                    _context.SubscriberLists.Add(subscriberList);
                    _context.SaveChanges();
                }
                else
                {

                    checkSubscriber.FirstName = subscriberViewModel.FirstName != "" && subscriberViewModel.FirstName != null ? subscriberViewModel.FirstName : checkSubscriber.FirstName;
                    checkSubscriber.LastName = subscriberViewModel.LastName != "" && subscriberViewModel.LastName != null ? subscriberViewModel.LastName : checkSubscriber.LastName;
                    checkSubscriber.City = subscriberViewModel.City != "" && subscriberViewModel.City != null ? subscriberViewModel.City : checkSubscriber.City;
                    checkSubscriber.State = subscriberViewModel.State != "" && subscriberViewModel.State != null ? subscriberViewModel.State : checkSubscriber.State;
                    checkSubscriber.Country = subscriberViewModel.Country != "" && subscriberViewModel.Country != null ? subscriberViewModel.Country : checkSubscriber.Country;
                    checkSubscriber.Address1 = subscriberViewModel.Address1 != "" && subscriberViewModel.Address1 != null ? subscriberViewModel.Address1 : checkSubscriber.Address1;
                    checkSubscriber.Address2 = subscriberViewModel.Address2 != "" && subscriberViewModel.Address2 != null ? subscriberViewModel.Address2 : checkSubscriber.Address2;
                    checkSubscriber.ZipCode = subscriberViewModel.ZipCode != "" && subscriberViewModel.ZipCode != null ? subscriberViewModel.ZipCode : checkSubscriber.ZipCode;
                    checkSubscriber.PhoneNumber = subscriberViewModel.PhoneNumber != "" && subscriberViewModel.PhoneNumber != null ? subscriberViewModel.PhoneNumber : checkSubscriber.PhoneNumber;
                    checkSubscriber.DOB = subscriberViewModel.DOB != null ? subscriberViewModel.DOB : checkSubscriber.DOB;
                    //subscriber.ZipCode = subscriberViewModel.City != "" && subscriberViewModel.City != null ? subscriberViewModel.City : subscriber.City;
                    checkSubscriber.LastModById = subscriberViewModel.CreatedById;
                    checkSubscriber.LastModDate = DateTime.Now;
                    //subscriber.EmailPermission = Convert.ToBoolean(subscriberViewModel.EmailPermission);
                    var isAddedinList = _context.SubscriberLists.Where(r => r.SubscriberId == checkSubscriber.SubscriberId && r.ListId == subscriberViewModel.ListId).SingleOrDefault();
                    if (isAddedinList == null)
                    {
                        SubscriberList subscriberList = new SubscriberList();
                        subscriberList.ListId = subscriberViewModel.ListId;
                        subscriberList.SubscriberId = checkSubscriber.SubscriberId;
                        subscriberList.CreatedDate = DateTime.UtcNow;
                        subscriberList.CreatedById = subscriberViewModel.CreatedById;
                        //subscriberList.IsSubscribe = true;
                        subscriberList.IsDeleted = false;
                        _context.SubscriberLists.Add(subscriberList);
                        _context.SaveChanges();
                    }
                    //else
                    //{
                    //    isAddedinList.IsSubscribe = true;
                    //    _context.SaveChanges();
                    //}
                }
            }
            return subs;

        }

        public List<SubscriberViewModel> Post1(List<SubscriberViewModel> subscriberViewModels)
        {
            //var dt = subscriberViewModels.Select(r => new { r.Email, r.FirstName, r.LastName, r.City, r.Address1, r.Address2, r.State, r.ZipCode, r.Country, r.PhoneNumber }).ToDataTable();
            //List<SubscriberViewModel> subs = new List<SubscriberViewModel>();
            //String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
            //SqlConnection conn = new SqlConnection(con);
            //SqlCommand cmd = conn.CreateCommand();
            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.CommandTimeout = 1000;
            //cmd.CommandText = "dbo.usp_ImportSubscriber"; //The name of the above mentioned stored procedure.  
            //SqlParameter pram = cmd.Parameters.AddWithValue("@ImportSubscriber", dt); //Here"@MyUDTableType" is the User-defined Table Type as a parameter. 
            //cmd.Parameters.AddWithValue("@CreatedById", Convert.ToString(subscriberViewModels[0].CreatedById));
            //cmd.Parameters.AddWithValue("@ListId", subscriberViewModels[0].ListId);
            //conn.Open();
            //DataSet result = new DataSet();
            //SqlDataAdapter da = new SqlDataAdapter();
            //var a = cmd.ExecuteNonQuery();
            //da.SelectCommand = cmd;
            //da.Fill(result);

            XDocument SubscriberDetails = new XDocument(new XDeclaration("1.0", "UTF - 8", "yes"),
           new XElement("dataroot",
           from subscriber in subscriberViewModels
           select new XElement("SubscribersDetails",
           new XElement("Email", subscriber.Email),
           new XElement("FirstName", subscriber.FirstName),
           new XElement("LastName", subscriber.LastName),
           new XElement("City", subscriber.City),
           new XElement("Address1", subscriber.Address1),
           new XElement("Address2", subscriber.Address2),
           new XElement("State", subscriber.State),
           new XElement("ZipCode", subscriber.ZipCode),
           new XElement("Country", subscriber.Country),
           new XElement("PhoneNumber", subscriber.PhoneNumber))));
            //var dt = subscriberViewModels.Select(r => new { r.Email, r.FirstName, r.LastName, r.City, r.Address1, r.Address2, r.State, r.ZipCode, r.Country, r.PhoneNumber }).ToDataTable();
            List<SubscriberViewModel> subs = new List<SubscriberViewModel>();
            String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(con);
            SqlCommand cmd = new SqlCommand("dbo.usp_NewImportSubscriber", conn);
            cmd.CommandTimeout = 1000;
            SqlParameter pram = cmd.Parameters.AddWithValue("@ImportSubscriber", SubscriberDetails.ToString());
            cmd.Parameters.AddWithValue("@CreatedById", Convert.ToString(subscriberViewModels[0].CreatedById));
            cmd.Parameters.AddWithValue("@ListId", subscriberViewModels[0].ListId);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            //var a = cmd.ExecuteNonQuery();
            da.SelectCommand = cmd;
            da.Fill(ds);
            subs = ds.Tables[0].AsEnumerable().Select(dataRow => new SubscriberViewModel { result = dataRow.Field<int>("Result") }).ToList();
            conn.Close();

            return subs;
        }

        public List<SubscriberViewModel> GetNotExistSubscribers(List<SubscriberViewModel> subscriberViewModels)
        {

            XDocument SubscriberDetails = new XDocument(new XDeclaration("1.0", "UTF - 8", "yes"),
            new XElement("dataroot",
            from subscriber in subscriberViewModels
            select new XElement("SubscribersDetails",
            new XElement("Email", subscriber.Email),
            new XElement("FirstName", subscriber.FirstName),
            new XElement("LastName", subscriber.LastName),
            new XElement("City", subscriber.City),
            new XElement("Address1", subscriber.Address1),
            new XElement("Address2", subscriber.Address2),
            new XElement("State", subscriber.State),
            new XElement("ZipCode", subscriber.ZipCode),
            new XElement("Country", subscriber.Country),
            new XElement("PhoneNumber", subscriber.PhoneNumber))));
            //var dt = subscriberViewModels.Select(r => new { r.Email, r.FirstName, r.LastName, r.City, r.Address1, r.Address2, r.State, r.ZipCode, r.Country, r.PhoneNumber }).ToDataTable();
            List<SubscriberViewModel> subs = new List<SubscriberViewModel>();
            String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(con);
            SqlCommand cmd = new SqlCommand("dbo.usp_NotExistSubscriber", conn);
            cmd.CommandTimeout = 1000;
            SqlParameter pram = cmd.Parameters.AddWithValue("@ImportSubscriber", SubscriberDetails.ToString());
            cmd.Parameters.AddWithValue("@CreatedById", Convert.ToString(subscriberViewModels[0].CreatedById));
            cmd.Parameters.AddWithValue("@ListId", subscriberViewModels[0].ListId);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            //var a = cmd.ExecuteNonQuery();
            da.SelectCommand = cmd;
            da.Fill(ds);

            subs = ds.Tables[0].AsEnumerable().Select(dataRow => new SubscriberViewModel
            {
                Email = dataRow.Field<string>("Email"),
                FirstName = dataRow.Field<string>("FirstName"),
                LastName = dataRow.Field<string>("LastName"),
                Address1 = dataRow.Field<string>("Address1"),
                Address2 = dataRow.Field<string>("Address2"),
                City = dataRow.Field<string>("City"),
                State = dataRow.Field<string>("State"),
                ZipCode = dataRow.Field<string>("ZipCode"),
                Country = dataRow.Field<string>("Country"),
                PhoneNumber = dataRow.Field<string>("PhoneNumber"),
                ListId = subscriberViewModels[0].ListId,
                CreatedById = subscriberViewModels[0].CreatedById
            }).ToList();
            conn.Close();
            return subs;
        }


        public bool Put(int? id, bool isSubsribe, int ListId)
        {
            if (id == null)
            {
                return false;
            }
            var subscriber = _context.Subscribers.Where(x => x.SubscriberId == id).FirstOrDefault();
            if (subscriber != null)
            {
                subscriber.IsSubscribed = isSubsribe;
                _context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool Put(List<int> id, int ListId)
        {
            foreach (var item in id)
            {
                var subsciber = _context.Subscribers.FirstOrDefault(x => x.SubscriberId == item);
                if (subsciber != null)
                {
                    subsciber.IsSubscribed = true;
                    _context.SaveChanges();
                }
            }
            return true;
        }

        public List<usp_getDeletedSubscriberList_Result> Get1(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {
            return _context.usp_getDeletedSubscriberList(listId, search, pageNumber, pageSize, orderBy).ToList();
        }

        public bool RecoverDeleteSubscriber(List<int> id, int listId)
        {
            foreach (var item in id)
            {
                var subsciber = _context.SubscriberLists.FirstOrDefault(x => x.SubscriberId == item && x.ListId == listId);
                if (subsciber != null)
                {
                    subsciber.IsDeleted = false;
                    _context.SaveChanges();
                }
            }
            return true;
        }

        public List<usp_getSuppressedSubscribersByListId_Result> GetSuppressedSubscribers(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {
            return _context.usp_getSuppressedSubscribersByListId(listId, search, pageNumber, pageSize, orderBy).ToList();
        }

        public List<SubscriberViewModel> SuppressedSubscriber(List<SubscriberViewModel> subscriberViewModels)
        {
            var dt = subscriberViewModels.Select(r => new { r.Email, r.FirstName, r.LastName, r.City, r.Address1, r.Address2, r.State, r.ZipCode, r.Country, r.PhoneNumber }).ToDataTable();
            List<SubscriberViewModel> subs = new List<SubscriberViewModel>();
            String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(con);
            try
            {
                SqlCommand cmd = new SqlCommand("dbo.usp_CheckSuppressedSubscribers", conn);
                cmd.CommandTimeout = 1000;
                cmd.Parameters.AddWithValue("@ImportSubscriber", dt); //Here"@MyUDTableType" is the User-defined Table Type as a parameter. 
                cmd.Parameters.AddWithValue("@CreatedById", Convert.ToString(subscriberViewModels[0].CreatedById));
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                da.Fill(ds);
                // var a = cmd.ExecuteReader();
                conn.Close();
                subs = ds.Tables[0].AsEnumerable().Select(dataRow => new SubscriberViewModel { Email = dataRow.Field<string>("Email"), FirstName = dataRow.Field<string>("FirstName"), LastName = dataRow.Field<string>("LastName"), CreatedById = dataRow.Field<string>("CreatedById") }).ToList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                conn.Close();
            }
            return subs;
        }

        public static string ObjectToXml(List<SubscriberListViewModel> subscriberViewModels)

        {

            System.IO.StringWriter output = new System.IO.StringWriter();

            XmlSerializer xs = new XmlSerializer(subscriberViewModels.GetType());

            xs.Serialize(output, subscriberViewModels);

            return output.ToString();

        }

        public List<SubscriberViewModel> EventbriteSyncingAttendees(List<SubscriberViewModel> subscriberViewModels)
        {
            XDocument SubscriberDetails = new XDocument(new XDeclaration("1.0", "UTF - 8", "yes"),
           new XElement("dataroot",
           from subscriber in subscriberViewModels
           select new XElement("SubscribersDetails",
           new XElement("Email", subscriber.Email),
           new XElement("FirstName", subscriber.FirstName),
           new XElement("LastName", subscriber.LastName),
           new XElement("City", subscriber.City),
           new XElement("Address1", subscriber.Address1),
           new XElement("Address2", subscriber.Address2),
           new XElement("State", subscriber.State),
           new XElement("ZipCode", subscriber.ZipCode),
           new XElement("Country", subscriber.Country),
           new XElement("PhoneNumber", subscriber.PhoneNumber),
           new XElement("Eventbrite_AttendeeId", subscriber.Eventbrite_AttendeeId))));
            List<SubscriberViewModel> subs = new List<SubscriberViewModel>();
            String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(con);
            SqlCommand cmd = new SqlCommand("dbo.usp_EventbriteSyncing", conn);
            cmd.CommandTimeout = 1000;
            SqlParameter pram = cmd.Parameters.AddWithValue("@ImportSubscriber", SubscriberDetails.ToString());
            cmd.Parameters.AddWithValue("@CreatedById", Convert.ToString(subscriberViewModels[0].CreatedById));
            cmd.Parameters.AddWithValue("@ListId", subscriberViewModels[0].ListId);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            //var a = cmd.ExecuteNonQuery();
            da.SelectCommand = cmd;
            da.Fill(ds);
            subs = ds.Tables[0].AsEnumerable().Select(dataRow => new SubscriberViewModel { result = dataRow.Field<int>("Result") }).ToList();
            conn.Close();

            return subs;

        }
    }
}