﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class UserDashboardService : IUserDashboard
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public List<usp_getUserDashboardData_Result> Get(string UserId)
        {
                return db.usp_getUserDashboardData(UserId,null,null).ToList();
        }

        public List<usp_getUserDashboardData_Result> Get(string UserId,DateTime? dateFrom,DateTime? dateTo)
        {

                return db.usp_getUserDashboardData(UserId, dateFrom, dateTo).ToList();

        }

        public List<usp_getEmailActivity_Result> GetEmailActivity(string UserId, DateTime? dateFrom, DateTime? dateTo)
        {
                return db.usp_getEmailActivity(dateFrom, dateTo, UserId).ToList();
        }
        public List<usp_getSubscriberDetails_Result> Get(string UserId, int PageNumber, int? PageSize)
        {
            return db.usp_getSubscriberDetails(UserId , PageNumber, PageSize).ToList();
        }

        public List<usp_getUnSubscriberDetails_Result> GetUnsubscriber(string UserId, int PageNumber, int? PageSize)
        {
            return db.usp_getUnSubscriberDetails(UserId, PageNumber, PageSize).ToList();
        }

        public List<usp_getAllEmailActivity_Result> GetAllEmailActivity(string UserId, DateTime? dateFrom, DateTime? dateTo)
        {
                return db.usp_getAllEmailActivity(UserId, dateFrom, dateTo).ToList();
        }

        public List<usp_getDashboardReport_Result> GetDashboardReport(string UserId, int PageNumber, int? PageSize)
        {
            return db.usp_getDashboardReport(UserId, PageNumber, PageSize).ToList();
        }
    }
}