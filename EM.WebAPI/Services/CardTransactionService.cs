﻿using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;

namespace EM.WebAPI.Services
{
    public class CardTransactionService : ICardTransaction
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        public List<AnTransactionLog> Get(string UserId)
        {
            return _context.AnTransactionLogs.Where(r => r.UserId == UserId && r.AnTransactionId != null).OrderByDescending(r => r.CreatedDate).ToList();
        }
    }
}