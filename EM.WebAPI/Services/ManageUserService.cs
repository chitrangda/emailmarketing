﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class ManageUserService : IManageUserService
    {
        private EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        BillingService billingService = new BillingService();
        UserInfoServices userInfoServices = new UserInfoServices();
        IndustryService industryService = new IndustryService();

        public usp_getManageBillingDetail_Result Get(string id)
        {

            var billinglist = db.usp_getManageBillingDetail(id).SingleOrDefault();
            return billinglist;
        }

        public AccountDetailsViewModel getAccountDetails(string userId)
        {
            AccountDetailsViewModel model = new AccountDetailsViewModel();

            var _profileDetils = userInfoServices.Get(userId);
            if (_profileDetils != null)
            {
                model.UserId = userId;
                model.UserName = _profileDetils.FirstName;
                model.FirstName = _profileDetils.FirstName;
                model.LastName = _profileDetils.LastName;
                model.IndustryRefId = _profileDetils.IndustryRefId;
                model.CompanyName = _profileDetils.CompanyName;
                model.Address = _profileDetils.Address1;
                model.City = _profileDetils.City;
                model.State = _profileDetils.State;
                model.Zip = _profileDetils.Zip;
                model.Country = _profileDetils.Country;
                model.Password = db.AspNetUsers.Where(x => x.UserProfile.Id == userId).SingleOrDefault().PasswordHash;
                model.MobileNumber = _profileDetils.MobilePhone;
                model.Email = _profileDetils.Email;
                model.PhoneNumber = _profileDetils.MobilePhone;
                model.BillingState = _profileDetils.State;

            }
            var _cardDetails = billingService.Get(userId) != null ? billingService.Get(userId).FirstOrDefault(r => r.IsPrimary == true) : null;
            if (_cardDetails != null)
            {
                model.CardHolderName = _cardDetails.FirstName;
                model.CardNumber = _cardDetails.CreditCardNo;
                model.CVv = _cardDetails.CVV;
                model.ExpMonth = _cardDetails.ExMonth;
                model.ExpYear = _cardDetails.ExYear;
                model.BillingAddress = _cardDetails.Address1;
                model.BillingCity = _cardDetails.City;
                model.BillingZip = _cardDetails.Zip;
                model.BillingPhone = _cardDetails.Phone;

            }


            return model;
        }

        public string Put(AccountDetailsViewModel model)
        {

            var details = db.UserProfiles.Where(x => x.Id == model.UserId).FirstOrDefault();
            if (details != null)
            {
                details.FirstName = model.FirstName;
                details.LastName = model.LastName;
                details.IndustryRefId = model.IndustryRefId;
                details.CompanyName = model.CompanyName;
                details.Address1 = model.Address;
                details.City = model.City;
                details.State = model.State;
                details.Zip = model.Zip;
                details.Country = model.Country;
                details.MobilePhone = model.MobileNumber;
                details.ContactEmail = model.Email;
                details.OfficePhone = model.PhoneNumber;
                db.SaveChanges();
            }


            var billing = db.UserPaymentProfiles.Where(x => x.UserId == model.UserId && x.CreditCardNo == model.CardNumber).FirstOrDefault();
            if (billing != null)
            {
                billing.FirstName = model.CardHolderName;
                billing.CreditCardNo = model.CardNumber;
                billing.LastFour = model.CVv;
                billing.ExMonth = model.ExpMonth;
                billing.ExYear = model.ExpYear;
                billing.Address1 = model.BillingAddress;
                billing.City = model.BillingCity;
                billing.State = model.BillingState;
                billing.Zip = model.BillingZip;
                billing.Phone = model.BillingPhone;
                billing.LastModById = model.LastModById;
                billing.LastModDate = DateTime.UtcNow;
                db.SaveChanges();
                return "success";
            }
            else
            {
                if (!string.IsNullOrEmpty(model.CardNumber))
                {
                    UserPaymentProfileViewModel paymentModel = new UserPaymentProfileViewModel();
                    paymentModel.CreditCardNo = model.CardNumber;
                    paymentModel.CVV = model.CVv;
                    paymentModel.UserId = model.UserId;
                    paymentModel.City = model.BillingCity;
                    paymentModel.Address1 = model.BillingAddress;
                    paymentModel.FirstName = model.CardHolderName;
                    paymentModel.Phone = model.BillingPhone;
                    paymentModel.State = model.BillingState;
                    paymentModel.Zip = model.BillingZip;
                    paymentModel.IsDeleted = false;
                    paymentModel.IsPrimary = true;
                    paymentModel.CreatedById = model.CreatedById;
                    paymentModel.ExMonth = model.ExpMonth;
                    paymentModel.ExYear = model.ExpYear;
                    var result = billingService.Post(paymentModel);
                    return result;
                }
                else
                {
                    return "success";
                }

            }



        }

       public List<USP_UserActivityLog_Result> getActivityLog(string Userid, DateTime? fromDate, DateTime? ToDate)
        {
            var ActivityList = db.USP_UserActivityLog(Userid, fromDate, ToDate).ToList();
            return ActivityList;

        }
    }
}
