﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.Helpers;

namespace EM.WebAPI.Services
{
    public class IPPoolService : IIPPoolService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        SendGridAPIHelper sendGridApi = new SendGridAPIHelper();
        public List<IP_Pool> Get()
        {
            var IPPoolList = db.IP_Pool.Where(x => x.IsDelete == false).ToList();
            if (IPPoolList != null)
            {
                var Ip_Pool = (from u in IPPoolList
                               select new IP_Pool
                               {
                                   Id = u.Id,
                                   Name = u.Name
                               }).ToList();
                return Ip_Pool;
            }
            else
            {
                return null;
            }

        }

        public IP_Pool Get(int id)
        {
            var IPPoolList = db.IP_Pool.SingleOrDefault(x => x.Id == id);
            if (IPPoolList != null)
            {
                var Ip_Pool = new IP_Pool()
                {
                    Id = IPPoolList.Id,
                    Name = IPPoolList.Name
                };
                return Ip_Pool;
            }
            else
            {
                return null;
            }
        }

        public bool Post(IP_Pool model)
        {
            if (model.Id == 0)
            {
                IP_Pool iP_Pool = new IP_Pool();
                iP_Pool.Id = model.Id;
                iP_Pool.Name = model.Name.Trim();
                iP_Pool.CreatedOn = DateTime.UtcNow;
                iP_Pool.CreatedBy = model.CreatedBy;
                iP_Pool.IsDelete = false;
                db.IP_Pool.Add(iP_Pool);
                db.SaveChanges();
                return true;
                //if (sendGridApi.setIpPoolName(model.Name.Trim()) == true)
                //{
                //    db.SaveChanges();
                //    return true;

                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                var _iPPool = db.IP_Pool.Find(model.Id);
                if (_iPPool != null)
                {
                    //if (sendGridApi.updateIpPoolName(model.Name, _iPPool.Name) == true)
                    //{
                        _iPPool.Id = model.Id;
                        _iPPool.Name = model.Name;
                        _iPPool.LastModBy = model.LastModBy;
                        _iPPool.LastModOn = DateTime.UtcNow;
                        db.SaveChanges();
                        return true;
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                }
                return false;
            }
        }

        public bool Delete(int id)
        {

            var Ip_pool = db.IP_Pool.AsEnumerable().Where(x => x.Id == id).FirstOrDefault();
            //if (sendGridApi.removeIpPoolName(Ip_pool.Name) == true)
            //{
                Ip_pool.IsDelete = true;
                db.SaveChanges();

                return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
    }
}