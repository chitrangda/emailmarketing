﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class AdminSpendService : IAdminSpend
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public List<USP_GetSpendData_Result> Get(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText)
        {
            return db.USP_GetSpendData(startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText, null).ToList();
        }
    }
}