﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class IPAddressService : IIPAddressService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public List<IP_Address> Get()
        {
            var IPAddressList = db.IP_Address.Where(x => x.IsDelete == false).ToList();
            if (IPAddressList != null)
            {
                var Ip_Address = (from u in IPAddressList
                                  select new IP_Address
                                  {
                                      Id = u.Id,
                                      IP = u.IP
                                  }).ToList();
                return Ip_Address;
            }
            else
            {
                return null;
            }

        }

        public IP_Address Get(int id)
        {
            var IPAddressList = db.IP_Address.SingleOrDefault(x => x.Id == id);
            if (IPAddressList != null)
            {
                var IP_Address = new IP_Address()
                {
                    Id = IPAddressList.Id,
                    IP = IPAddressList.IP
                };
                return IP_Address;
            }
            else
            {
                return null;
            }
        }

        public List<IP_Address> GetByPoolId(int id)
        {
            var IPAddressList = db.IP_Address.Where(x => x.IPPool_Id == id).Where(x => x.IsDelete == false).ToList();
            if (IPAddressList != null)
            {
                var Ip_Address = (from u in IPAddressList
                                  select new IP_Address
                                  {
                                      Id = u.Id,
                                      IP = u.IP,
                                      IsDelete = u.IsDelete
                                  }).ToList();
                return Ip_Address;
            }
            else
            {
                return null;
            }

        }

        public int PostIPAddress(IPAddressViewModel model)
        {
            var IPAddressList = db.IP_Address.Where(x => x.IPPool_Id == model.IPPool_Id).ToList();
            {
                if (IPAddressList != null)
                {
                    foreach (var item in IPAddressList)
                    {
                        var IP = db.IP_Address.AsEnumerable().Where(x => x.Id == item.Id).FirstOrDefault();
                        IP.IsDelete = true;
                        db.SaveChanges();
                    }
                }
            }
            if (model.Id == 0)
            {
                foreach (var item in model.SelectedIP)
                {
                    var obj = db.IP_Address.Where(a => a.IPPool_Id == model.IPPool_Id && a.IsDelete == true).Where(a => a.IP == item).FirstOrDefault();
                    if (obj == null)
                    {
                        var obj2 = db.IP_Address.Where(a => a.IPPool_Id == model.IPPool_Id && a.IsDelete == false).Where(a => a.IP == item).FirstOrDefault();
                        if (obj2 == null)
                        {
                            IP_Address IP_Address = new IP_Address();
                            IP_Address.Id = model.Id;
                            IP_Address.IP = item;
                            IP_Address.CreatedOn = DateTime.UtcNow;
                            IP_Address.CreatedBy = model.CreatedBy;
                            IP_Address.IsDelete = false;
                            IP_Address.IPPool_Id = model.IPPool_Id;
                            db.IP_Address.Add(IP_Address);
                            db.SaveChanges();
                            //return 1;
                        }
                        else
                        {
                            // return 3;
                        }
                    }
                    else
                    {
                        obj.IsDelete = false;
                        db.SaveChanges();
                        //return 1;
                    }
                }
                return 1;
            }
            else
            {
                var _iPAddress = db.IP_Address.Find(model.Id);
                if (_iPAddress != null)
                {
                    _iPAddress.Id = model.Id;
                    _iPAddress.IP = model.IP;
                    _iPAddress.LastModBy = model.LastModBy;
                    _iPAddress.LastModOn = DateTime.UtcNow;
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
        }

        public bool Delete(int id)
        {
            var IP_Address = db.IP_Address.AsEnumerable().Where(x => x.Id == id).FirstOrDefault();
            IP_Address.IsDelete = true;
            db.SaveChanges();

            return true;
        }

        public bool Post(IPAddressViewModel model)
        {
            if (model.IP_Pool.Id == 0)
            {
                IP_Pool iP_Pool = new IP_Pool();
                iP_Pool.Id = model.IP_Pool.Id;
                iP_Pool.Name = model.IP_Pool.Name.Trim();
                iP_Pool.CreatedOn = DateTime.UtcNow;
                iP_Pool.CreatedBy = model.IP_Pool.CreatedBy;
                iP_Pool.IsDelete = false;
                db.IP_Pool.Add(iP_Pool);
                db.SaveChanges();
                return true;
                //if (sendGridApi.setIpPoolName(model.Name.Trim()) == true)
                //{
                //    db.SaveChanges();
                //    return true;

                //}
                //else
                //{
                //    return false;
                //}
            }
            else
            {
                var _iPPool = db.IP_Pool.Find(model.Id);
                if (_iPPool != null)
                {
                    _iPPool.Id = model.IP_Pool.Id;
                    _iPPool.Name = model.IP_Pool.Name;
                    _iPPool.LastModBy = model.IP_Pool.LastModBy;
                    _iPPool.LastModOn = DateTime.UtcNow;
                    db.SaveChanges();
                    return true;
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                }
                return false;
            }
        }
    }
}