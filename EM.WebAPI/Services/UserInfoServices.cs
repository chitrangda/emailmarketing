﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;

namespace EM.WebAPI.Services
{
    public class UserInfoServices : IUserInfoService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public UserInfoViewModel Get(string id)
        {
            var _user = db.AspNetUsers.SingleOrDefault(s => s.Id == id);
            string _PaymentProfileId = db.UserPaymentProfiles.FirstOrDefault(r => r.UserId==id && r.IsPrimary == true && r.IsDeleted == false && r.Expired == false) != null ? db.UserPaymentProfiles.FirstOrDefault(r => r.UserId==id && r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).AnPaymentProfileId : null;
            string _cardType = db.UserPaymentProfiles.FirstOrDefault(r => r.UserId == id && r.IsPrimary == true && r.IsDeleted == false && r.Expired == false) != null ? db.UserPaymentProfiles.FirstOrDefault(r => r.UserId == id && r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).CardType : null;
            var _bounceRate = db.usp_getBounceRate(id).FirstOrDefault();
            //if (_user.UserPaymentProfiles2.Where(r => r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).FirstOrDefault() != null)
            //{
            //    _PaymentProfileId = _user.UserPaymentProfiles2.Where(r => r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).FirstOrDefault().AnPaymentProfileId;

            //}
            //else if (_user.UserPaymentProfiles1.Where(r => r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).FirstOrDefault() != null)
            //{
            //    _PaymentProfileId = _user.UserPaymentProfiles1.Where(r => r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).FirstOrDefault().AnPaymentProfileId;

            //}
            //else if(_user.UserPaymentProfiles.Where(r => r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).FirstOrDefault() != null)
            //{
            //    _PaymentProfileId = _user.UserPaymentProfiles.Where(r => r.IsPrimary == true && r.IsDeleted == false && r.Expired == false).FirstOrDefault().AnPaymentProfileId;

            //}
            if (_user != null)
            {
                var response = new UserInfoViewModel()
                {
                    Id = _user.Id,
                    FirstName = _user.UserProfile.FirstName,
                    LastName = _user.UserProfile.LastName,
                    ProfileImage = _user.UserProfile.ProfileImage,
                    City = _user.UserProfile.City,
                    State = _user.UserProfile.State,
                    Country = _user.UserProfile.Country,
                    Zip = _user.UserProfile.Zip,
                    MobilePhone = _user.UserProfile.MobilePhone,
                    Email = _user.Email,
                    CompanyName = _user.UserProfile.CompanyName,
                    CreditCardNumber = _user.UserProfile.CreditCardNo,
                    ExpiryDate = _user.UserProfile.ExpiryDate,
                    RegisterDate = _user.UserProfile.RegisteredDate,
                    AuthProfileId = _user.UserProfile.AuthProfileId,
                    Address1 = _user.UserProfile.Address1,
                    IndustryRefId = _user.UserProfile.IndustryRefId,
                    PaymentProfileId = _PaymentProfileId,
                    DOB = _user.UserProfile.DOB,
                    planId = _user.UserPlans.FirstOrDefault(r => r.IsActive == true).Package.Plan.PlanId,
                    SelectedPackakgeId = _user.UserPlans.FirstOrDefault(r => r.IsActive == true).PackageId,
                    TimeZoneID = _user.UserProfile.TimeZoneId,
                    CardType = _cardType,
                    SendingFromEmail = _user.UserProfile.SendingFromEmail,
                    BounceRate = _bounceRate,
                    EmailSending=_user.UserProfile.EmailSending


                };
                return response;
            }
            else
            {
                return null;
            }
        }

        public List<UserInfoViewModel> Get(string FirstName,string LastName,string MobilePhone,string City,string State,string Country,string Zip,bool? isActive, int? PageSize, int? StartIndex)
        {
            var _users = db.usp_getAllUssers(FirstName,LastName, MobilePhone, City,State,Country,Zip,isActive, PageSize, StartIndex);
            if (_users != null)
            {
                var response = (from u in _users
                                select new UserInfoViewModel
                                {
                                    Id = u.id,
                                    FirstName = u.FirstName,
                                    LastName = u.LastName,
                                    ProfileImage = u.ProfileImage,
                                    CreatedDate = u.CreatedDate,
                                    City = u.City,
                                    State = u.State,
                                    Country = u.Country,
                                    Zip = u.Zip,
                                    MobilePhone = u.MobilePhone,
                                    Email = u.Email,
                                    CompanyName = u.Company,
                                    isActive = u.isActive,
                                    SalesRepId= u.SalesRepId
                                }
                            ).ToList();
                return response;
            }
            else
            {
                return null;
            }
        }

        //public List<usp_getAllUssersNew_Result> Get(string search, int? PageSize, int? StartIndex)
        //{
        //    var _users = db.usp_getAllUssersNew(search, PageSize, StartIndex).ToList();
        //    return _users;
        //}

        public usp_getAuthorization_Result GetAuthorizationInfo(string userId)
        {
                var result = db.usp_getAuthorization(userId).FirstOrDefault();
                return result;
        }

        public bool Put(UserInfoViewModel userInfoViewModel)
        {
                if (userInfoViewModel == null)
                {
                    return false;
                }
                var userInfo = db.AspNetUsers.Where(x => x.Id == userInfoViewModel.Id).FirstOrDefault();
                userInfo.UserProfile.FirstName = userInfoViewModel.FirstName!=null?userInfoViewModel.FirstName.Trim():userInfoViewModel.FirstName;
                userInfo.UserProfile.LastName = userInfoViewModel.LastName!=null?userInfoViewModel.LastName.Trim():userInfoViewModel.LastName;
                userInfo.UserProfile.MobilePhone = userInfoViewModel.MobilePhone!=null?userInfoViewModel.MobilePhone.Trim():userInfoViewModel.MobilePhone;
                userInfo.UserProfile.City = userInfoViewModel.City!=null?userInfoViewModel.City.Trim():userInfoViewModel.City;
                userInfo.UserProfile.State = userInfoViewModel.State!=null?userInfoViewModel.State.Trim():userInfoViewModel.State;
                userInfo.UserProfile.Country = userInfoViewModel.Country;
                userInfo.UserProfile.Zip = userInfoViewModel.Zip;
                userInfo.UserProfile.ProfileImage = userInfoViewModel.ProfileImage;
                userInfo.UserProfile.CreditCardNo = userInfoViewModel.CreditCardNumber;
                userInfo.UserProfile.ExpiryDate = userInfoViewModel.ExpiryDate;
                userInfo.UserProfile.CompanyName = userInfoViewModel.CompanyName!=null?userInfoViewModel.CompanyName.Trim():userInfoViewModel.CompanyName;
                userInfo.UserProfile.Address1 = userInfoViewModel.Address1!=null?userInfoViewModel.Address1.Trim():userInfoViewModel.Address1;
                userInfo.UserProfile.IndustryRefId = userInfoViewModel.IndustryRefId;
                userInfo.UserProfile.DOB = userInfoViewModel.DOB;
                userInfo.UserProfile.TimeZoneId = userInfoViewModel.TimeZoneID;
                if (userInfoViewModel.isActive!=null)
                {
                    userInfo.UserProfile.IsActive = userInfoViewModel.isActive;
                }
                db.SaveChanges();
                return true;
        }

        public bool Put(string id,bool accountStatus)
        {
                var userInfo = db.UserProfiles.Find(id);
                if(userInfo!=null)
                {
                    userInfo.IsActive = accountStatus;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
        }
    }
}