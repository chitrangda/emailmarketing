﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class AdminUpcomingBillingService : IAdminUpcomingBilling
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        EmailPlanService emailPlan = new EmailPlanService();

        public List<usp_getUpComingBillingDetails_Result> Get(DateTime? dateFrom, DateTime? dateTo, int? pageIndex, int? pageSize)
        {
            return db.usp_getUpComingBillingDetails(dateFrom, dateTo, pageSize, pageIndex, null).ToList();
        }

        public usp_getUpComingBillingDetails_Result Get(DateTime? dateFrom, DateTime? dateTo, int? pageIndex, int? pageSize, string Id)
        {
            return db.usp_getUpComingBillingDetails(null, null, null, 0, Id).FirstOrDefault();
        }

        public bool Post(usp_getUpComingBillingDetails_Result model)
        {
            if (model != null)
            {
                var billing = db.UserPlans.Where(x => x.UserId == model.id && x.IsActive == true).FirstOrDefault();
                var profile = db.UserProfiles.Where(x => x.Id == model.id).FirstOrDefault();

                if (billing != null)
                {
                    if (model.PackageId != billing.PackageId)
                    {
                        var _curPlan = db.UserPlans.Where(r => r.UserId == model.id && r.IsActive == true).FirstOrDefault();
                        if (_curPlan != null)
                        {
                            _curPlan.IsActive = false;

                        }

                        var planDetail = emailPlan.GetPlanByID(model.PackageId.Value, model.id);
                        UserPlan oPlan = new UserPlan();
                        oPlan.UserId = model.id;
                        oPlan.TotalDays = 30;
                        oPlan.PackageId = model.PackageId.Value;
                        billing.MonthlyCredit = planDetail.NoOfCredits.ToString();
                        billing.totalRemainingCredits = planDetail.NoOfCredits.ToString();
                        billing.TotalEmails = planDetail.NoOfCredits;
                        oPlan.NoOfSubscribersMin = 0;
                        oPlan.NoOfSubscribersMax = planDetail.NoOfSubscribersMax;
                        oPlan.PackageUpdateDate = DateTime.Now;
                        oPlan.BillingAmount = planDetail.BillingAmount != null ? planDetail.BillingAmount.Value : 0;
                        oPlan.ContactManagement = planDetail.ContactManagement;
                        oPlan.UnlimitedEmails = planDetail.UnlimitedEmails;
                        oPlan.EmailScheduling = planDetail.EmailScheduling;
                        oPlan.EducationalResources = planDetail.EducationalResources;
                        oPlan.LiveSupport = planDetail.LiveSupport;
                        oPlan.CustomizableTemplates = planDetail.CustomizableTemplates;
                        oPlan.IsActive = true;
                        oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                        oPlan.BillingStatus = 1;
                        oPlan.totalSpendCredits = "0";
                        db.UserPlans.Add(oPlan);
                        db.SaveChanges();


                    }
                    billing = db.UserPlans.Where(x => x.UserId == model.id && x.IsActive == true).FirstOrDefault();
                    if (model.nextbillingdate != null)
                    {
                        billing.NextPackageUpdateDate = Convert.ToDateTime(model.nextbillingdate);
                    }
                    if (model.monthlycredit == "Unlimited Emails")
                    {
                        var credits = 0;
                        billing.MonthlyCredit = credits.ToString();
                        billing.TotalEmails = credits;
                        billing.totalRemainingCredits = credits.ToString();
                    }
                    else
                    {
                        billing.MonthlyCredit = Convert.ToString(model.monthlycredit);
                        billing.totalRemainingCredits = Convert.ToString(model.monthlycredit);
                        billing.TotalEmails = Convert.ToInt32(model.monthlycredit);
                    }
                    billing.BillingAmount = model.monthlybilling;

                    if (model.chargeddate != null)
                    {
                        billing.PackageUpdateDate = Convert.ToDateTime(model.chargeddate);

                    }
                }
                if (profile != null)
                {
                    profile.IsActive = model.isactive;
                    profile.PromotionCode = model.referralcode;
                    if (model.followupdate != null)
                    {
                        DateTime t;
                        DateTime.TryParse(model.followupdate, out t);
                        profile.FollowupDate = t;
                        profile.FollowupTime = t;

                    }
                }
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

