﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class LeadDashBoardService: ILeadDashboardService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public List<USP_GetLeadDashBoard_Result> Get()
        {
            return db.USP_GetLeadDashBoard(null, null, null, null, null, null,null).ToList();
        }
        public List<USP_GetLeadDashBoard_Result> Get(string RepId, int? LeadQualityID,
            int? TypeOfLead, int? IndustryId, int? pageIndex, int? pageSize)
        {
            return db.USP_GetLeadDashBoard( RepId,LeadQualityID,TypeOfLead,IndustryId,pageIndex,pageSize,null).ToList();
        }

        public USP_GetLeadDashBoard_Result Get(string RepId,int leadId)
        {
            return db.USP_GetLeadDashBoard(RepId, null, null, null, null, null, leadId).FirstOrDefault();
        }
    }
}
