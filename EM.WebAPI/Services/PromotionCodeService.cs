﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class PromotionCodeService : IPromotionCode
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public bool Delete(int id)
        {
            var promotion = db.PromotionCodes.AsEnumerable().Where(x => x.PromotionCodeId == id).FirstOrDefault();
            promotion.IsDelete = true;
            db.SaveChanges();
            return true;
        }

        public List<PromotionCode> Get()
        {
            var promotionList = db.PromotionCodes.Where(x => x.IsDelete == false).ToList();
            if (promotionList != null)
            {
                var _promotion = (from u in promotionList
                                  select new PromotionCode
                                  {
                                      PromotionCodeId = u.PromotionCodeId,
                                      PromotionCodeName = u.PromotionCodeName

                                  }
                            ).ToList();
                return _promotion;
            }
            else
            {
                return null;
            }
        }

        public PromotionCode Get(int id)
        {
            var promotionList = db.PromotionCodes.SingleOrDefault(x => x.PromotionCodeId == id);
            if (promotionList != null)
            {
                var _promotion = new PromotionCode()
                {
                    PromotionCodeId = promotionList.PromotionCodeId,
                    PromotionCodeName = promotionList.PromotionCodeName
                };
                return _promotion;
            }
            else
            {
                return null;
            }
        }

        public bool Post(PromotionCode model)
        {
            if (model.PromotionCodeId == 0)
            {

                var promotion = db.PromotionCodes.FirstOrDefault(x => x.PromotionCodeName == model.PromotionCodeName && x.IsDelete == true);
                if (promotion == null)
                {
                    PromotionCode promotionCode = new PromotionCode();
                    promotionCode.PromotionCodeId = model.PromotionCodeId;
                    promotionCode.PromotionCodeName = model.PromotionCodeName;
                    promotionCode.CreatedById = model.CreatedById;
                    promotionCode.CreatedDate = DateTime.UtcNow;
                    promotionCode.IsDelete = false;
                    db.PromotionCodes.Add(promotionCode);
                    db.SaveChanges();
                }
                else
                {
                    promotion.IsDelete = false;
                    db.SaveChanges();
                }
                return true;
            }
            else
            {
                var _promotion = db.PromotionCodes.Find(model.PromotionCodeId);
                if (_promotion != null)
                {
                    _promotion.PromotionCodeId = model.PromotionCodeId;
                    _promotion.PromotionCodeName = model.PromotionCodeName;
                    _promotion.LastModById = model.LastModById;
                    _promotion.LastModDate = DateTime.UtcNow;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}