﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class FollowUpService : IFollowUpService
    {
        private readonly EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public List<getSalesRepFollowUp_Result> Get(string id, DateTime? datefrom, DateTime? dateto, bool? IsCurrent)
        {
            return db.getSalesRepFollowUp(id, datefrom, dateto, IsCurrent).ToList();
        }
        public usp_getUserDetails_Result GetClientData(string id)
        {
            return db.usp_getUserDetails(id).FirstOrDefault();
        }

        public bool UpdateFollowUpData(usp_getUserDetails_Result model)
        {
            if (model != null)
            {
                var userProfileDetails = db.UserProfiles.Find(model.Id);
                var userPlanDetails = db.UserPlans.FirstOrDefault(r => r.UserId == model.Id && r.IsActive == true);
                if (userProfileDetails != null)
                {
                    if (model.SalesRepId != null && (model.SalesRepId != userProfileDetails.SalesRepId || userProfileDetails.SalesRepId == null))
                    {
                        string oldSalesRepId = userProfileDetails.SalesRepId;
                        var oldSlaesRepDetails = db.SalesRepProfiles.Find(oldSalesRepId);
                        if (oldSlaesRepDetails != null)
                        {
                            oldSlaesRepDetails.NoOfClients = oldSlaesRepDetails.NoOfClients - 1;
                        }
                        var newSlaesRepDetails = db.SalesRepProfiles.Find(model.SalesRepId);
                        if (newSlaesRepDetails != null)
                        {
                            newSlaesRepDetails.NoOfClients = newSlaesRepDetails.NoOfClients + 1;
                        }

                        userProfileDetails.SalesRepId = model.SalesRepId;
                        
                    }
                    userProfileDetails.IsActive = model.IsActive;
                    userProfileDetails.ClientType = model.ClientType;
                    userProfileDetails.PromotionCode = model.PromotionCode;
                    if (model.FollowUpDate != null)
                    {
                        DateTime t;
                        DateTime.TryParse(model.FollowUpDate, out t);
                        userProfileDetails.FollowupDate = t;
                        userProfileDetails.FollowupTime = t;
                    }


                }
                if (userPlanDetails != null)
                {
                    if (model.PackageId != userPlanDetails.PackageId)
                    {
                        var _curPlan = db.UserPlans.Where(r => r.UserId == model.Id && r.IsActive == true).FirstOrDefault();
                        if (_curPlan != null)
                        {
                            _curPlan.IsActive = false;

                        }
                        string APIURL = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["ApiURL"], "EmailPlans/GetPlanByID", model.PackageId);
                        var apiResult = (new APICallHelper()).Get(APIURL).Content.ReadAsStringAsync().Result;
                        if (apiResult != null)
                        {
                            var planDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<usp_getPackageDetail_Result>(apiResult.ToString());

                            UserPlan oPlan = new UserPlan();
                            oPlan.UserId = model.Id;
                            oPlan.TotalEmails = planDetail.NoOfCredits;
                            oPlan.TotalDays = 30;
                            oPlan.PackageId = model.PackageId.Value;
                            oPlan.MonthlyCredit = planDetail.NoOfCredits.ToString();
                            oPlan.NoOfSubscribersMin = 0;
                            oPlan.NoOfSubscribersMax = planDetail.NoOfSubscribersMax;
                            oPlan.PackageUpdateDate = DateTime.Now;
                            oPlan.BillingAmount = planDetail.BillingAmount != null ? planDetail.BillingAmount.Value : 0;
                            oPlan.totalRemainingCredits = planDetail.NoOfCredits.ToString();
                            oPlan.ContactManagement = planDetail.ContactManagement;
                            oPlan.UnlimitedEmails = planDetail.UnlimitedEmails;
                            oPlan.EmailScheduling = planDetail.EmailScheduling;
                            oPlan.EducationalResources = planDetail.EducationalResources;
                            oPlan.LiveSupport = planDetail.LiveSupport;
                            oPlan.CustomizableTemplates = planDetail.CustomizableTemplates;
                            oPlan.IsActive = true;
                            oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                            oPlan.BillingStatus = 1;
                            oPlan.totalSpendCredits = "0";
                            db.UserPlans.Add(oPlan);
                            db.SaveChanges();
                        }

                    }
                  
                   
                }


                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}