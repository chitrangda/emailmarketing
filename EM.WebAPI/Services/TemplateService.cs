﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class TemplateService : ITemplateService
    {
        private EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public bool Delete(List<int> id)
        {
            foreach (var item in id)
            {
                var template = db.Templates.AsEnumerable().Where(x => x.TemplateId == item).FirstOrDefault();
                template.IsDeleted = true;
                db.SaveChanges();
            }
            return true;
        }

        public List<usp_getTemplateList_Result> Get(string id, int? pageNumber, int? pageSize, string search, string orderBy)
        {

            var templates = db.usp_getTemplateList(id, search, pageNumber, pageSize, orderBy).ToList();
            foreach (var item in templates)
            {
                if (item.TemplatePath == "")
                {
                    item.TemplatePath = "/Content/imagesUser/template.jpg";
                }
            }
            return templates;
        }

        public TemplateViewModel Get(int? id, string type)
        {
            if (id != null && Convert.ToInt32(id) > 0)
            {

                if (type == "layout")
                {
                    var beelayout = db.BeeTemplates.AsEnumerable().Where(x => x.BeeTemplateId == Convert.ToInt32(id)).FirstOrDefault();
                    TemplateViewModel model = new TemplateViewModel();
                    model.TemplateId = beelayout.BeeTemplateId;
                    model.TemplateName = beelayout.BeeTemplateName;
                    model.UserId = beelayout.UserId;
                    model.TemplateHtml = beelayout.BeeTemplateHtml;
                    model.TemplateJSON = beelayout.BeeTemplateJson;
                    model.CreatedDate = beelayout.CreatedDate;
                    model.CreatedById = beelayout.CreatedById;
                    model.LastModById = beelayout.LastModById;
                    model.LastModDate = beelayout.LastModDate;
                    model.Description = beelayout.Description;
                    model.TemplatePath = beelayout.BeeTemplatePath;
                    return model;
                }
                else
                {
                    var template = db.Templates.AsEnumerable().Where(x => x.TemplateId == Convert.ToInt32(id)).FirstOrDefault();
                    TemplateViewModel usp_GetTemplateList_Result = new TemplateViewModel();
                    usp_GetTemplateList_Result.TemplateId = template.TemplateId;
                    usp_GetTemplateList_Result.TemplateName = template.TemplateName;
                    usp_GetTemplateList_Result.UserId = template.UserId;
                    usp_GetTemplateList_Result.TemplateHtml = template.TemplateHtml;
                    usp_GetTemplateList_Result.TemplateJSON = template.TemplateJSON;
                    usp_GetTemplateList_Result.CreatedDate = template.CreatedDate;
                    usp_GetTemplateList_Result.CreatedById = template.CreatedById;
                    usp_GetTemplateList_Result.LastModById = template.LastModById;
                    usp_GetTemplateList_Result.LastModDate = template.LastModDate;
                    usp_GetTemplateList_Result.Description = template.Description;
                    usp_GetTemplateList_Result.TemplatePath = template.TemplatePath;
                    return usp_GetTemplateList_Result;
                }
            }
            else
            {
                return DataHelper.GetDefaultTemplate();
            }

        }

        public List<usp_getBeeTemplateList_Result> Get(int? pageNumber, int? pageSize, string search, string orderBy, string version)
        {
            var Beetemplates = db.usp_getBeeTemplateList(search, pageNumber, pageSize, orderBy, version).ToList();
            return Beetemplates;
        }

        public bool Post(TemplateViewModel templateViewModel)
        {
            if (templateViewModel == null)
            {
                return false;
            }
            var template = db.Templates.Where(x => x.TemplateId == templateViewModel.TemplateId).FirstOrDefault();
            if (template != null)
            {
                template.TemplateHtml = templateViewModel.TemplateHtml;
                template.TemplateJSON = templateViewModel.TemplateJSON;
                template.LastModDate = DateTime.Now;
                template.LastModById = templateViewModel.UserId;
                template.UserId = templateViewModel.UserId;
                template.TemplateName = templateViewModel.TemplateName;
                template.TemplatePath = templateViewModel.TemplatePath;
                db.SaveChanges();
            }
            else
            {


                template = new Template();
                template.TemplateHtml = templateViewModel.TemplateHtml;
                template.TemplateJSON = templateViewModel.TemplateJSON;
                template.CreatedDate = DateTime.Now;
                template.UserId = templateViewModel.UserId;
                template.CreatedById = templateViewModel.UserId;
                template.TemplateName = templateViewModel.TemplateName;
                template.IsDeleted = false;
                template.TemplatePath = templateViewModel.TemplatePath;
                template.LastModDate = DateTime.Now;
                template.LastModById = templateViewModel.UserId;
                db.Templates.Add(template);
                db.SaveChanges();
            }

            return true;
        }
    }
}