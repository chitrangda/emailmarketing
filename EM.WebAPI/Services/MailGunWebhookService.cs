﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace EM.WebAPI.Services
{
    public class MailGunWebhookService : IMailGunWebhookService
    {

        public async Task SaveDeliveredEvents(DeliveredMailGunModel oEventview)
        {
            if (oEventview.eventdata.tags != null && oEventview.eventdata.tags.Length > 0)
            {
                //List<EventTypeModel> events = new List<EventTypeModel>();
                using (var db = new EmailMarketingDbEntities())
                {

                    //db.usp_insertMailgunDelivredEvent(Convert.ToInt32(oEventview.eventdata.tags[1]), oEventview.eventdata.message.headers.to, oEventview.signature.timestamp, oEventview.eventdata.tags[2]);
                    EmailEvent oEvent = new EmailEvent();
                    oEvent.CampaignId = Convert.ToInt32(oEventview.eventdata.tags[1]);
                    oEvent.email = oEventview.eventdata.recipient;
                    oEvent.@event = "Delivered";
                    oEvent.Event_TimeStamp = oEventview.signature.timestamp;
                    oEvent.EventDate = DateTime.Now;
                    db.EmailEvents.Add(oEvent);
                    await db.SaveChangesAsync();

                    //var oSubsriber = db.Subscribers.Where(r => r.Email == oEventview.eventdata.recipient).FirstOrDefault();
                    //if (oSubsriber != null)
                    //{
                    //    oSubsriber.IsSuppressed = true;
                    //    await db.SaveChangesAsync();
                    //}



                    var oDropEvents = db.EmailEvents.Where(r => r.email == oEventview.eventdata.recipient && (r.@event == "dropped" || r.@event == "bounce")).ToList();
                    if (oDropEvents != null && oDropEvents.Count() > 0)
                    {
                        db.EmailEvents.RemoveRange(oDropEvents);
                        await db.SaveChangesAsync();
                    }

                    var oCampaign = db.Campaigns.Where(r => r.CampaignId == oEvent.CampaignId).FirstOrDefault();
                    if (oCampaign != null)
                    {
                        oCampaign.DeliveredCount = oCampaign.DeliveredCount + 1;
                        oCampaign.BouncedCount = oCampaign.DeliveredCount - oDropEvents.Count();

                        await db.SaveChangesAsync();

                    }

                    var oUserPlan = db.UserPlans.Where(r => r.UserId == oEventview.eventdata.tags[2] && r.IsActive == true).FirstOrDefault();
                    if (oUserPlan != null)
                    {
                        oUserPlan.totalSpendCredits = oUserPlan.totalSpendCredits + 1;
                        oUserPlan.totalRemainingCredits = Convert.ToString(Convert.ToInt32(oUserPlan.totalRemainingCredits) - 1);
                        await db.SaveChangesAsync();

                    }
                    var oSubsriber = db.Subscribers.Where(r => r.Email == oEventview.eventdata.recipient).FirstOrDefault();
                    if (oSubsriber != null)
                    {
                        oSubsriber.IsSuppressed = false;
                        await db.SaveChangesAsync();
                    }


                }
            }
        }

        public async Task SavePermanentFailedEvents(FailedMailGunModel oEventview)
        {
            if (oEventview.eventdata.tags != null && oEventview.eventdata.tags.Length > 0)
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    //db.usp_insertMailgunFailedEvent(Convert.ToInt32(oEventview.eventdata.tags[1]), oEventview.eventdata.recipient, oEventview.signature.timestamp, "bounce", oEventview.eventdata.deliverystatus.message);
                    EmailEvent oEvent = new EmailEvent();
                    oEvent.CampaignId = Convert.ToInt32(oEventview.eventdata.tags[1]);
                    oEvent.email = oEventview.eventdata.recipient;
                    oEvent.@event = "bounce";
                    oEvent.Event_TimeStamp = oEventview.signature.timestamp;
                    oEvent.EventDate = DateTime.Now;
                    oEvent.reason = oEventview.eventdata.deliverystatus.message;
                    db.EmailEvents.Add(oEvent);
                    await db.SaveChangesAsync();

                    var oSubsriber = db.Subscribers.Where(r => r.Email == oEventview.eventdata.recipient).FirstOrDefault();
                    if (oSubsriber != null)
                    {
                        oSubsriber.IsSuppressed = true;
                        await db.SaveChangesAsync();
                    }

                    var oCampaign = db.Campaigns.Where(r => r.CampaignId == oEvent.CampaignId).FirstOrDefault();
                    if (oCampaign != null)
                    {
                        oCampaign.BouncedCount = oCampaign.BouncedCount + 1;
                        await db.SaveChangesAsync();

                    }

                    var oDropEvents = db.EmailEvents.Where(r => r.email == oEventview.eventdata.recipient && r.@event == "dropped").ToList();
                    if (oDropEvents != null && oDropEvents.Count() > 0)
                    {
                        db.EmailEvents.RemoveRange(oDropEvents);
                        await db.SaveChangesAsync();
                    }


                }
            }
        }

        public async Task SaveTempFailedEvents(FailedMailGunModel oEventview)
        {
            if (oEventview.eventdata.tags != null && oEventview.eventdata.tags.Length > 0)
            {
                using (var db = new EmailMarketingDbEntities())
                {

                    //db.usp_insertMailgunFailedEvent(Convert.ToInt32(oEventview.eventdata.tags[1]), oEventview.eventdata.recipient, oEventview.signature.timestamp, "dropped", oEventview.eventdata.deliverystatus.message);
                    EmailEvent oEvent = new EmailEvent();
                    oEvent.CampaignId = Convert.ToInt32(oEventview.eventdata.tags[1]);
                    oEvent.email = oEventview.eventdata.recipient;
                    oEvent.@event = "dropped";
                    oEvent.Event_TimeStamp = oEventview.signature.timestamp;
                    oEvent.EventDate = DateTime.Now;
                    oEvent.reason = oEventview.eventdata.deliverystatus.message;
                    db.EmailEvents.Add(oEvent);
                    await db.SaveChangesAsync();

                    var oCampaign = db.Campaigns.Where(r => r.CampaignId == oEvent.CampaignId).FirstOrDefault();
                    if (oCampaign != null)
                    {
                        oCampaign.BouncedCount = oCampaign.BouncedCount + 1;
                        await db.SaveChangesAsync();

                    }
                }
            }
        }

        public async Task SaveOpenEvents(OpenClickMailGunModel oEventview)
        {
            if (oEventview.eventdata.tags != null && oEventview.eventdata.tags.Length > 0)
            {
                using (var db = new EmailMarketingDbEntities())
                {

                    db.usp_insertMailgunOpenClickEvent(Convert.ToInt32(oEventview.eventdata.tags[1]), oEventview.eventdata.recipient, oEventview.signature.timestamp, "open", null);

                }

            }
        }

        public async Task SaveClickEvents(OpenClickMailGunModel oEventview)
        {
            if (oEventview.eventdata.tags != null && oEventview.eventdata.tags.Length > 0)
            {
                using (var db = new EmailMarketingDbEntities())
                {

                    db.usp_insertMailgunOpenClickEvent(Convert.ToInt32(oEventview.eventdata.tags[1]), oEventview.eventdata.recipient, oEventview.signature.timestamp, "click", oEventview.eventdata.url);

                }

            }

        }

        public async Task SaveComplainedEvents(ComplainedMailGunModel oEventview)
        {
            if (oEventview.eventdata.tags != null && oEventview.eventdata.tags.Length > 0)
            {
                using (var db = new EmailMarketingDbEntities())
                {

                    db.usp_insertMailgunSpamComplainedEvent(Convert.ToInt32(oEventview.eventdata.tags[1]), oEventview.eventdata.recipient, oEventview.signature.timestamp, "complained");

                }

            }
        }

        async Task BulkInsertEvents(DataTable dtEvents)
        {
            try
            {
                String con = System.Configuration.ConfigurationManager.ConnectionStrings["EmConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(con))
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandText = "dbo.usp_insertEmailEvent"; //The name of the above mentioned stored procedure.  
                    SqlParameter pram = cmd.Parameters.AddWithValue("@Events", dtEvents); //Here"@MyUDTableType" is the User-defined Table Type as a parameter. 
                    conn.Open();
                    int res = await cmd.ExecuteNonQueryAsync();
                    conn.Close();
                }
            }
            catch (System.Data.Common.DbException ex)
            {
                await DataHelper.CreateLog(null, ex.Message, "WebHook API DBI");

            }
            catch (Exception ex)
            {
                await DataHelper.CreateLog(null, ex.Message, "WebHook API Bulk Insert");
            }

        }
    }
}