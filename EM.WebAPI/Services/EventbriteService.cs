﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EM.WebAPI.Services
{
    public class EventbriteService : IEventbriteService
    {
        public async Task CreateList(string EventName, string UserId, string eventId)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                var oList = db.Lists.Where(r => r.CreatedById == UserId && r.Eventbrite_EventId==eventId).FirstOrDefault();
                if (oList == null)
                {
                    List list = new List();
                    list.ListName = EventName;
                    list.Eventbrite_EventId = eventId;
                    list.CreatedById = UserId;
                    list.CreatedDate = DateTime.Now;
                    list.IsActive = true;
                    list.IsDeleted = false;
                    db.Lists.Add(list);
                    await db.SaveChangesAsync();
                }
                else
                {
                    oList.ListName = EventName;
                    await db.SaveChangesAsync();

                }
            }
        }

        public async Task CreateAttendeeSubscriber(Profile attendeeProfile, string attendeeId,string eventId,string UserId, string EventbriteUserId)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                var oList = db.Lists.Where(r => r.Eventbrite_EventId == eventId && r.CreatedById == UserId).FirstOrDefault();
                if (oList == null)
                {
                    await getEvent(eventId, EventbriteUserId);
                    oList = db.Lists.Where(r => r.Eventbrite_EventId == eventId && r.CreatedById == UserId).FirstOrDefault();
                }
                var oSubscriber = db.Subscribers.Where(r => r.Email.ToLower() == attendeeProfile.email.ToLower() && r.CreatedById == UserId).FirstOrDefault();
                if (oSubscriber == null)
                {
                    Subscriber subscriber = new Subscriber();
                    subscriber.FirstName = attendeeProfile.firstname;
                    subscriber.LastName = attendeeProfile.lastname;
                    subscriber.Email = attendeeProfile.email;
                    subscriber.CreatedById = UserId;
                    subscriber.CreatedDate = DateTime.Now;
                    subscriber.IsSubscribed = true;
                    subscriber.IsActive = true;
                    subscriber.IsSuppressed = false;
                    db.Subscribers.Add(subscriber);
                    await db.SaveChangesAsync();

                    SubscriberList subscriberList = new SubscriberList();
                    subscriberList.ListId = oList.ListId;
                    subscriberList.SubscriberId = subscriber.SubscriberId;
                    subscriberList.IsDeleted = false;
                    subscriberList.IsSubscribe = true;
                    subscriberList.CreatedById = UserId;
                    subscriberList.CreatedDate = DateTime.Now;
                    db.SubscriberLists.Add(subscriberList);
                    await db.SaveChangesAsync();
                }
                else
                {
                    var oSubscriberList = db.SubscriberLists.Where(r => r.SubscriberId == oSubscriber.SubscriberId && r.ListId == oList.ListId && r.CreatedById == UserId).FirstOrDefault();
                    if (oSubscriberList == null)
                    {
                        SubscriberList subscriberList = new SubscriberList();
                        subscriberList.ListId = oList.ListId;
                        subscriberList.SubscriberId = oSubscriber.SubscriberId;
                        subscriberList.IsDeleted = false;
                        subscriberList.IsSubscribe = true;
                        subscriberList.CreatedById = UserId;
                        subscriberList.CreatedDate = DateTime.Now;
                        db.SubscriberLists.Add(subscriberList);
                        await db.SaveChangesAsync();
                    }
                    oSubscriber.FirstName = attendeeProfile.firstname;
                    oSubscriber.LastName = attendeeProfile.lastname;
                    await db.SaveChangesAsync();

                }
            }
        }

        public async Task getEvent(string eventId, string EventbriteUserId)
        {

            string UserAccessToken = "";
            string userId = "";
            using (var db = new EmailMarketingDbEntities())
            {
                var oEventbriteUserAccess = db.Eventbrite_User_AccessCode.Where(r => r.EventBrite_User_Id == EventbriteUserId).FirstOrDefault();
                if (oEventbriteUserAccess != null)
                {
                    UserAccessToken = oEventbriteUserAccess.AccessCode;
                    userId = oEventbriteUserAccess.UserId;

                }
            }
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/events/" + eventId + "/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.Method = Method.GET;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var oEvent = JsonConvert.DeserializeObject<Event>(response.Content);
                await CreateList(oEvent.name.text, userId,oEvent.id);
            }



        }


        public async Task getAttendee(string eventId,string attendeeId, string EventbriteUserId)
        {

            string UserAccessToken = "";
            string userId = "";
            using (var db = new EmailMarketingDbEntities())
            {
                var oEventbriteUserAccess = db.Eventbrite_User_AccessCode.Where(r => r.EventBrite_User_Id == EventbriteUserId).FirstOrDefault();
                if (oEventbriteUserAccess != null)
                {
                    UserAccessToken = oEventbriteUserAccess.AccessCode;
                    userId = oEventbriteUserAccess.UserId;

                }
            }
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://www.eventbriteapi.com/v3/events/"+eventId+"/attendees/"+attendeeId+"/");
            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", "Bearer " + UserAccessToken);
            request.Method = Method.GET;
            var response = client.Execute(request);
            if (response != null && response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var oAttendee = JsonConvert.DeserializeObject<Attendee>(response.Content);
                await CreateAttendeeSubscriber(oAttendee.profile,oAttendee.id, eventId, userId, EventbriteUserId);
            }
        }
    }
}