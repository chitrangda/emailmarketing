﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class SalesLeadEmailService : ISalesLeadEmail
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public bool Delete(int leadId)
        {

            var email = db.SalesPortal_Leads_EmailIdList.AsEnumerable().Where(x => x.EmailRowId == leadId).FirstOrDefault();
            email.IsDelete = true;
            db.SaveChanges();

            return true;
        }

        public List<USP_GetLeadEmail_Result> Get(int leadId)
        {
            return db.USP_GetLeadEmail(leadId, null).ToList();
        }

        public SalesPortal_Leads_EmailIdList Get(int leadId, int EmailRowId)
        {
            var emailIdDetails = db.SalesPortal_Leads_EmailIdList.Where(x => x.EmailRowId == EmailRowId && x.LeadId == leadId).FirstOrDefault();
            if (emailIdDetails != null)
            {
                var response = new SalesPortal_Leads_EmailIdList()
                {
                    EmailRowId = emailIdDetails.EmailRowId,
                    LeadId = emailIdDetails.LeadId,
                    Email = emailIdDetails.Email,
                    IsPrimaryEmail = emailIdDetails.IsPrimaryEmail

                };
                return response;
            }
            else
            {
                return null;
            }
        }

        public bool Post(SalesPortal_Leads_EmailIdList model)
        {
            if (model.EmailRowId == 0)
            {

                SalesPortal_Leads_EmailIdList idList = new SalesPortal_Leads_EmailIdList();
                idList.LeadId = model.LeadId;
                idList.Email = model.Email;
                idList.CreatedBy = model.CreatedBy;
                idList.CreatedOn = DateTime.UtcNow;
                idList.IsDelete = false;
                idList.IsPrimaryEmail = false;
                db.SalesPortal_Leads_EmailIdList.Add(idList);
                db.SaveChanges();
                return true;
            }
            else
            {
                var email = db.SalesPortal_Leads_EmailIdList.Find(model.EmailRowId);
                if (email != null)
                {
                    email.LeadId = model.LeadId;
                    email.Email = model.Email;
                    email.UpdatedOn = DateTime.UtcNow;
                    email.UpdatedBy = model.UpdatedBy;
                    email.IsDelete = false;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool Put(int LeadId, int EmailRowId)
        {
            var emailPrimary = db.SalesPortal_Leads_EmailIdList.Where(x => x.LeadId == LeadId && x.EmailRowId == EmailRowId).FirstOrDefault();
            var IsPrimary = db.SalesPortal_Leads_EmailIdList.Where(x => x.LeadId == LeadId && x.EmailRowId != EmailRowId).ToList();
            if (IsPrimary.Count > 0)
            {
                IsPrimary.ForEach(x => x.IsPrimaryEmail = false);
                db.SaveChanges();
            }
            if (emailPrimary != null )
            {
                emailPrimary.IsPrimaryEmail = true;
                db.SaveChanges();
                return true;
            }
            else
            {
                return true;
            }
        }
    }
}
