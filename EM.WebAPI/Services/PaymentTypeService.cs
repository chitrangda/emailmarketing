﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class PaymentTypeService : IPaymentTypeService
    {
        private readonly EmailMarketingDbEntities _context = new EmailMarketingDbEntities();
        public List<PaymentTypeViewModel> Get()
        {
                var paymentTypeListResult = _context.PaymentTypes.ToList();
                var paymentList = (from ss in paymentTypeListResult
                                    select new PaymentTypeViewModel
                                    {
                                       PaymentTypeId =ss.PaymentTypeId,
                                       PaymentTypeName =ss.PaymentTypeName
                                    }).ToList();
                return paymentList;
        }
    }
}