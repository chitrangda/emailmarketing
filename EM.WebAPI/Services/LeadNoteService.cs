﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class LeadNoteService : ILeadNoteService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        bool ILeadNoteService.Delete(int id, string userId)
        {
            var note = db.SalesPortal_LeadNote.Find(id);
            if (note != null)
            {
                note.IsDelete = true;
                note.LastModById = userId;
                note.LastModDate = DateTime.UtcNow;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        List<usp_getLeadNoteList_Result> ILeadNoteService.Get(int LeadId)
        {
            return db.usp_getLeadNoteList(LeadId).ToList();
        }

        bool ILeadNoteService.Post(SalesPortal_LeadNote SalesPortalLeadNote)
        {
            if (SalesPortalLeadNote.NoteId == 0)
            {
                SalesPortal_LeadNote notes = new SalesPortal_LeadNote();
                notes.NoteId = SalesPortalLeadNote.NoteId;
                notes.LeadId = SalesPortalLeadNote.LeadId;
                notes.NoteDescription = SalesPortalLeadNote.NoteDescription;
                notes.FollowUpDate = SalesPortalLeadNote.FollowUpDate;
                notes.CreatedById = SalesPortalLeadNote.CreatedById;
                notes.CreatedDate = DateTime.UtcNow;
                notes.IsDelete = false;
                notes.NoteTypeId = Convert.ToInt32(SalesPortalLeadNote.NoteTypeId);
                notes.TimeZone = SalesPortalLeadNote.TimeZone;
                db.SalesPortal_LeadNote.Add(notes);
                db.SaveChanges();
                return true;

            }
            else
            {
                var note = db.SalesPortal_LeadNote.Find(SalesPortalLeadNote.NoteId);
                if (note != null)
                {
                    note.LeadId = SalesPortalLeadNote.LeadId;
                    note.NoteDescription = SalesPortalLeadNote.NoteDescription;
                    note.FollowUpDate = SalesPortalLeadNote.FollowUpDate;
                    note.LastModById = SalesPortalLeadNote.LastModById;
                    note.LastModDate = DateTime.UtcNow;
                    note.IsDelete = false;
                    note.NoteTypeId = Convert.ToInt32(SalesPortalLeadNote.NoteTypeId);
                    note.TimeZone = SalesPortalLeadNote.TimeZone;
                    db.SaveChanges();

                }
                return false;
            }
        }
    }
}