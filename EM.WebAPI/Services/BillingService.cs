﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.WebAPI.Services
{
    public class BillingService : IBillingService
    {
        private EmailMarketingDbEntities _context = new EmailMarketingDbEntities();

        public UserPaymentProfileViewModel Get(int id)
        {
            var paymentProfile = _context.UserPaymentProfiles.Where(x => x.Id == id).FirstOrDefault();
            UserPaymentProfileViewModel userPaymentProfileViewModel = new UserPaymentProfileViewModel();
            userPaymentProfileViewModel.IsPrimary = true;
            if (paymentProfile != null)
            {
                userPaymentProfileViewModel.Id = paymentProfile.Id;
                userPaymentProfileViewModel.UserId = paymentProfile.UserId;
                userPaymentProfileViewModel.AnPaymentProfileId = paymentProfile.AnPaymentProfileId;
                userPaymentProfileViewModel.FirstName = paymentProfile.FirstName;
                userPaymentProfileViewModel.LastName = paymentProfile.LastName;
                userPaymentProfileViewModel.Phone = paymentProfile.Phone;
                userPaymentProfileViewModel.Email = paymentProfile.Email;
                userPaymentProfileViewModel.Address1 = paymentProfile.Address1;
                userPaymentProfileViewModel.Address2 = paymentProfile.Address2;
                userPaymentProfileViewModel.City = paymentProfile.City;
                userPaymentProfileViewModel.State = paymentProfile.State;
                userPaymentProfileViewModel.Country = paymentProfile.Country;
                userPaymentProfileViewModel.Zip = paymentProfile.Zip;
                userPaymentProfileViewModel.Default = paymentProfile.Default;
                userPaymentProfileViewModel.LastModBy = paymentProfile.LastModBy;
                userPaymentProfileViewModel.Active = paymentProfile.Active;
                userPaymentProfileViewModel.LastFour = paymentProfile.LastFour;
                userPaymentProfileViewModel.ExMonth = paymentProfile.ExMonth;
                userPaymentProfileViewModel.ExYear = paymentProfile.ExYear;
                userPaymentProfileViewModel.Expired = IsCreditCardExpired(paymentProfile.ExYear, paymentProfile.ExMonth);
                userPaymentProfileViewModel.CreatedById = paymentProfile.CreatedById;
                userPaymentProfileViewModel.LastModById = paymentProfile.LastModById;
                userPaymentProfileViewModel.CardType = paymentProfile.CardType;
                userPaymentProfileViewModel.CreditCardNo = paymentProfile.CreditCardNo;
                userPaymentProfileViewModel.CreatedDate = paymentProfile.CreatedDate;
                userPaymentProfileViewModel.LastModDate = paymentProfile.LastModDate;
                userPaymentProfileViewModel.IsPrimary = paymentProfile.IsPrimary;
            }
            return userPaymentProfileViewModel;
        }

        public List<UserPaymentProfileViewModel> Get(string id)
        {
            var paymentProfile = _context.UserPaymentProfiles.Where(x => x.UserId == id && x.IsDeleted == false);
            var userPaymentProfiles = (from pp in paymentProfile.AsEnumerable()
                                       select new UserPaymentProfileViewModel
                                       {
                                           Id = pp.Id,
                                           UserId = pp.UserId,
                                           AnPaymentProfileId = pp.AnPaymentProfileId,
                                           FirstName = pp.FirstName,
                                           LastName = pp.LastName,
                                           Phone = pp.Phone,
                                           Email = pp.Email,
                                           Address1 = pp.Address1,
                                           Address2 = pp.Address2,
                                           City = pp.City,
                                           State = pp.State,
                                           Country = pp.Country,
                                           Zip = pp.Zip,
                                           Default = pp.Default,
                                           LastModBy = pp.LastModBy,
                                           Active = pp.Active,
                                           LastFour = pp.LastFour,
                                           ExMonth = pp.ExMonth,
                                           ExYear = pp.ExYear,
                                           Expired = IsCreditCardExpired(pp.ExYear, pp.ExMonth),
                                           CreatedById = pp.CreatedById,
                                           LastModById = pp.LastModById,
                                           CardType = pp.CardType,
                                           CreditCardNo = pp.CreditCardNo,
                                           CreatedDate = pp.CreatedDate,
                                           LastModDate = pp.LastModDate,
                                           IsPrimary = pp.IsPrimary
                                       }).ToList();
            return userPaymentProfiles.OrderByDescending(r => r.IsPrimary).ToList();
        }

        public string Post(UserPaymentProfileViewModel userPaymentProfileViewModel)
        {
            if (userPaymentProfileViewModel == null)
            {
                return "error";
            }
            if (userPaymentProfileViewModel.Id > 0)
            {
                var userPaymentProfile = _context.UserPaymentProfiles.AsEnumerable().Where(x => x.Id == userPaymentProfileViewModel.Id && x.UserId == userPaymentProfileViewModel.UserId).FirstOrDefault();
                userPaymentProfile.Id = userPaymentProfileViewModel.Id;
                userPaymentProfile.UserId = userPaymentProfileViewModel.UserId;
                userPaymentProfile.AnPaymentProfileId = userPaymentProfileViewModel.UserId + "_" + userPaymentProfileViewModel.Id;
                userPaymentProfile.FirstName = userPaymentProfileViewModel.FirstName;
                userPaymentProfile.LastName = userPaymentProfileViewModel.LastName;
                userPaymentProfile.Phone = userPaymentProfileViewModel.Phone;
                userPaymentProfile.Email = userPaymentProfileViewModel.Email;
                userPaymentProfile.Address1 = userPaymentProfileViewModel.Address1;
                userPaymentProfile.Address2 = userPaymentProfileViewModel.Address2;
                userPaymentProfile.City = userPaymentProfileViewModel.City;
                userPaymentProfile.State = userPaymentProfileViewModel.State;
                userPaymentProfile.Country = userPaymentProfileViewModel.Country;
                userPaymentProfile.Zip = userPaymentProfileViewModel.Zip;
                userPaymentProfile.Default = userPaymentProfileViewModel.Default;
                userPaymentProfile.LastModBy = userPaymentProfileViewModel.LastModById;
                userPaymentProfile.Active = userPaymentProfileViewModel.Active;
                userPaymentProfile.LastFour = userPaymentProfileViewModel.LastFour;
                userPaymentProfile.ExMonth = userPaymentProfileViewModel.ExMonth;
                userPaymentProfile.ExYear = userPaymentProfileViewModel.ExYear;
                userPaymentProfile.Expired = userPaymentProfileViewModel.Expired;
                userPaymentProfile.LastModById = userPaymentProfileViewModel.CreatedById;
                userPaymentProfile.CardType = userPaymentProfileViewModel.CardType;
                userPaymentProfile.CreditCardNo = userPaymentProfileViewModel.CreditCardNo.Trim();
                userPaymentProfile.LastModDate = DateTime.UtcNow;
                userPaymentProfile.IsPrimary = userPaymentProfileViewModel.IsPrimary;
                userPaymentProfile.Active = true;
                _context.SaveChanges();
                setPrimaryFalse(userPaymentProfile);
                return "success";
            }
            else
            {
                var userPaymentProfile = new UserPaymentProfile();
                userPaymentProfile.AnPaymentProfileId = userPaymentProfileViewModel.AnPaymentProfileId;
                userPaymentProfile.UserId = userPaymentProfileViewModel.UserId;
                userPaymentProfile.FirstName = userPaymentProfileViewModel.FirstName;
                userPaymentProfile.LastName = userPaymentProfileViewModel.LastName;
                userPaymentProfile.Phone = userPaymentProfileViewModel.Phone;
                userPaymentProfile.Email = userPaymentProfileViewModel.Email;
                userPaymentProfile.Address1 = userPaymentProfileViewModel.Address1;
                userPaymentProfile.Address2 = userPaymentProfileViewModel.Address2;
                userPaymentProfile.City = userPaymentProfileViewModel.City;
                userPaymentProfile.State = userPaymentProfileViewModel.State;
                userPaymentProfile.Country = userPaymentProfileViewModel.Country;
                userPaymentProfile.Zip = userPaymentProfileViewModel.Zip;
                userPaymentProfile.Default = userPaymentProfileViewModel.Default;
                userPaymentProfile.LastModBy = userPaymentProfileViewModel.LastModBy;
                userPaymentProfile.Active = userPaymentProfileViewModel.Active;
                userPaymentProfile.LastFour = userPaymentProfileViewModel.LastFour;
                userPaymentProfile.ExMonth = userPaymentProfileViewModel.ExMonth;
                userPaymentProfile.ExYear = userPaymentProfileViewModel.ExYear;
                userPaymentProfile.Expired = userPaymentProfileViewModel.Expired;
                userPaymentProfile.CreatedById = userPaymentProfileViewModel.CreatedById;
                userPaymentProfile.CreditCardNo = EM.Helpers.Utilities.MaskCreditCardNo(userPaymentProfileViewModel.CreditCardNo.Trim());
                userPaymentProfile.CardType = EM.Helpers.Utilities.getCardType(userPaymentProfileViewModel.CreditCardNo.Trim());
                userPaymentProfile.CreatedDate = DateTime.UtcNow;
                userPaymentProfile.Active = true;
                userPaymentProfile.IsDeleted = false;
                userPaymentProfile.IsPrimary = true;
                _context.UserPaymentProfiles.Add(userPaymentProfile);
                _context.SaveChanges();
                //userPaymentProfile.AnPaymentProfileId = userPaymentProfileViewModel.UserId + "_" + userPaymentProfileViewModel.Id;
                //_context.SaveChanges();

                setPrimaryFalse(userPaymentProfile);
                return "success";
            }
        }

        public bool IsCreditCardExpired(int year, int month)
        {
            var currentDateTime = DateTime.UtcNow;
            if (year < currentDateTime.Year || year > 2039)
            {
                return true;
            }
            if ((year < currentDateTime.Year || year > 2039) && month < currentDateTime.Month)
            {
                return true;
            }
            return false;
        }

        public bool Delete(List<int> id)
        {
            foreach (var item in id)
            {
                var payment = _context.UserPaymentProfiles.AsEnumerable().Where(x => x.Id == item).FirstOrDefault();
                _context.UserPaymentProfiles.Remove(payment);
                _context.SaveChanges();
            }
            return true;
        }

        public bool Delete(int? id)
        {
            var payment = _context.UserPaymentProfiles.AsEnumerable().Where(x => x.Id == id).FirstOrDefault();
            _context.UserPaymentProfiles.Remove(payment);
            _context.SaveChanges();
            return true;
        }

        public bool setPrimary(int? id, string userId)
        {
            var paymentMethodList = _context.UserPaymentProfiles.Where(r => r.UserId == userId).ToList();
            if (paymentMethodList != null && paymentMethodList.Count() > 0)
            {
                paymentMethodList.ForEach(r => r.IsPrimary = false);
                _context.SaveChanges();
            }
            var curPaymentMethod = _context.UserPaymentProfiles.Find(id);
            if (curPaymentMethod != null)
            {
                curPaymentMethod.IsPrimary = true;
                _context.SaveChanges();
            }
            return true;
        }

        void setPrimaryFalse(UserPaymentProfile userPaymentProfile)
        {
            //set false isPrimary for other cards if this isPrimary is true

            if (userPaymentProfile.IsPrimary == true)
            {
                var IsPrimary = _context.UserPaymentProfiles.Where(y => y.Id != userPaymentProfile.Id && y.UserId == userPaymentProfile.UserId).ToList();
                if (IsPrimary.Count > 0)
                {
                    IsPrimary.ForEach(x => x.IsPrimary = false);
                    _context.SaveChanges();
                }
            }
        }
    }

}