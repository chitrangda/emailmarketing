﻿using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;

namespace EM.WebAPI.Services
{
    public class TransactionLogService : ITransactionLogService
    {
        private EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public AnTransactionLog Get(int id)
        {
            var _transaction = db.AnTransactionLogs.Find(id);
            return _transaction;
        }

        public usp_getTransactionInfo_Result Get(string token)
        {
            var _transaction = db.usp_getTransactionInfo(token).FirstOrDefault();
            return _transaction;
        }

        public int Post(AnTransactionLog model)
        {
            db.AnTransactionLogs.Add(model);
            db.SaveChanges();
            return model.Id;
        }

    }
}