﻿using System;
using System.Collections.Generic;
using System.Linq;
using EM.Factory;
using EM.WebAPI.Interfaces;

namespace EM.WebAPI.Services
{
    public class AdminDataService : IAdminData
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public List<usp_getAdminData_Result> Get(DateTime? FromDate, DateTime? ToDate, int? Year, string Quarter, int? Month, string City, string State, string Zip, string TimeZone, string Find, int? MemberType, bool? Status, int? CampaignType)
        {
            return db.usp_getAdminData(FromDate, ToDate, Year, Quarter, Month, City, State, Zip, TimeZone, Find, MemberType, Status, CampaignType).ToList();
        }

    }
}