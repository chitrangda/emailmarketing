﻿using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.Factory;
using EM.Factory.ViewModels;

namespace EM.WebAPI.Services
{
    public class SalesRepService : ISalesRepService
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        public SalesRepInfoViewModel Get(string id)
        {
            var result = db.SalesRepProfiles.Find(id);
            if(result !=null)
            {
                var response = new SalesRepInfoViewModel()
                {
                    Id = result.Id,
                    FirstName = result.FirstName,
                    LastName = result.LastName,
                    Email = result.AspNetUser1.Email,
                    IsActive = result.IsActive,
                    NoOfClients = result.NoOfClients,
                    SpentTotal = result.SpentTotal,
                    ViewRepActivity = result.ViewRepActivity,
                    CanExport = result.CanExport,
                    ExportLead = result.ExportLead,
                    Manager = result.Manager,
                    CreatedDate = result.CreatedDate,
                    ContactNo=result.ContactNo
                };
                return response;

            }
            else
            {
                return null;
            }
        }

        public List<usp_getAllSalesRep_Result> Get(string FirstName,string LastName,int? NoOfClients,double? SpentTotal,bool? ViewRepActivity,bool? CanExport,bool? ExportLead,bool? Manager,bool? IsActive, int? pageIndex,int? pageSize)
        {
            return db.usp_getAllSalesRep(FirstName,LastName, NoOfClients, SpentTotal,ViewRepActivity,CanExport,ExportLead,Manager,IsActive, pageSize, pageIndex).ToList();
        }

        public bool Put(SalesRepProfile model)
        {
            var _saleRepUser = db.SalesRepProfiles.Find(model.Id);
            if(_saleRepUser!=null)
            {
                _saleRepUser.FirstName = model.FirstName;
                _saleRepUser.LastName = model.LastName;
                _saleRepUser.NoOfClients = model.NoOfClients;
                _saleRepUser.SpentTotal = model.SpentTotal;
                _saleRepUser.ViewRepActivity = model.ViewRepActivity;
                _saleRepUser.Manager = model.Manager;
                _saleRepUser.CanExport = model.CanExport;
                _saleRepUser.IsActive = model.IsActive;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}