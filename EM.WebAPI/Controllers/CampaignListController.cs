﻿using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CampaignList")]
    public class CampaignListController : ApiController
    {
        private readonly ICampaignListService _campaignListService;
        public CampaignListController(ICampaignListService campaignListService)
        {
            _campaignListService = campaignListService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _campaignListService.Get();
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLogFile.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
