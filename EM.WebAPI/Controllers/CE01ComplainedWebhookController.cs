﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CE01ComplainedWebhook")]
    public class CE01ComplainedWebhookController : ApiController
    {
        IMailGunWebhookService mailGunWebhookService;

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public CE01ComplainedWebhookController(IMailGunWebhookService _webhookService)
        {
            mailGunWebhookService = _webhookService;
        }

        public async Task Post([FromBody]ComplainedMailGunModel value)
        {
            try
            {
                string output = JsonConvert.SerializeObject(value);
                DataHelper.writeEventLogTxt(output);
            }
            catch
            {

            }
            try
            {
                await mailGunWebhookService.SaveComplainedEvents(value);
            }
            catch (Exception ex)
            {
                string output = JsonConvert.SerializeObject(value);
                DataHelper.writeEmailEventLogTxt(ex, output);
            }

        }

        public string Get()
        {
            return "value";
        }
    }
}
