﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/SalesLeadEmail")]
    public class SalesLeadEmailController : ApiController
    {
        private readonly ISalesLeadEmail salesLeadEmail;
        public SalesLeadEmailController(ISalesLeadEmail _salesLeadEmail)
        {
            salesLeadEmail = _salesLeadEmail;
        }
        public IHttpActionResult Get(int leadId)
        {
            try
            {
                return Ok(salesLeadEmail.Get(leadId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Post(SalesPortal_Leads_EmailIdList emailIdList)
        {
            try
            {

                return Ok(salesLeadEmail.Post(emailIdList));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Delete(int leadId)
        {
            try
            {

                return Ok(salesLeadEmail.Delete(leadId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Get(int leadId , int EmailRowId)
        {
            try
            {
                return Ok(salesLeadEmail.Get(leadId, EmailRowId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Put(int LeadId, int EmailRowId)
        {
            try
            {
                return Ok(salesLeadEmail.Put(LeadId, EmailRowId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
