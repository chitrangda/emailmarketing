﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using EM.Helpers;
using EM.WebAPI.Interfaces;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/RepActivity")]
    public class RepActivityController : ApiController
    {
        private readonly IRepActivity repActivityService;
        public RepActivityController(IRepActivity _repActivityService)
        {
            repActivityService = _repActivityService;
        }

        public IHttpActionResult Get()
        {
            try
            {
                return Ok(repActivityService.Get());
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetGridData")]
        public IHttpActionResult GetGridData(string RepId, DateTime? From, DateTime? To)
        {
            try
            {
                return Ok(repActivityService.GetGridData(RepId, From, To));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("getSaleRepProfile")]
        public IHttpActionResult getSaleRepProfile()
        {
            try
            {
                return Ok(repActivityService.getSaleRepProfile());
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        // GET: RepActivity

    }
}