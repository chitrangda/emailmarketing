﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/FollowUp")]
    public class FollowUpController : ApiController
    {
        private readonly IFollowUpService _followUpService;
        public FollowUpController(IFollowUpService followUpService)
        {
            _followUpService = followUpService;
        }

        [Route("GetGridData")]
        public IHttpActionResult GetGridData(string id, DateTime? datefrom, DateTime? dateto, bool? IsCurrent)
        {
            try
            {
                return Ok(_followUpService.Get(id,datefrom,dateto, IsCurrent));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetClientData")]
        public IHttpActionResult GetClientData(string id)
        {
            try
            {
                return Ok(_followUpService.GetClientData(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Post(usp_getUserDetails_Result model)
        {
            try
            {
                if (_followUpService.UpdateFollowUpData(model)==true)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
