﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/EventbriteEventCreated")]
    public class EventbriteEventCreatedController : ApiController
    {
        IEventbriteService eventbriteservice;
        public EventbriteEventCreatedController(IEventbriteService _eventbriteservice)
        {
            eventbriteservice = _eventbriteservice;
        }

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public async Task Post([FromBody]PayloadEventViewModel value)
        {

            try
            {
                string[] arUrl = value.api_url.Split('/');
                DataHelper.writeEventLogTxt(JsonConvert.SerializeObject(value));
                await eventbriteservice.getEvent(arUrl[arUrl.Length - 2], value.config.user_id);
            }

            catch (Exception ex)
            {
                string output = JsonConvert.SerializeObject(value);
                DataHelper.writeEmailEventLogTxt(ex, output);
            }

        }

    }
}
