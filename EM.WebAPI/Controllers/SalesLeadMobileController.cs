﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/SalesLeadMobile")]
    public class SalesLeadMobileController : ApiController
    {
        private readonly ISalesLeadMobile salesLeadMobile;
        public SalesLeadMobileController(ISalesLeadMobile _salesLeadMobile)
        {
            salesLeadMobile = _salesLeadMobile;
        }
        public IHttpActionResult Get(int leadId)
        {
            try
            {
                return Ok(salesLeadMobile.Get(leadId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Post(SalesPortal_Leads_MobileNumList mobileIdList)
        {
            try
            {

                return Ok(salesLeadMobile.Post(mobileIdList));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Delete(int leadId)
        {
            try
            {

                return Ok(salesLeadMobile.Delete(leadId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Get(int leadId, int MobileRowId)
        {
            try
            {
                return Ok(salesLeadMobile.Get(leadId, MobileRowId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Put(int LeadId, int MobileRowId)
        {
            try
            {
                return Ok(salesLeadMobile.Put(LeadId, MobileRowId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
