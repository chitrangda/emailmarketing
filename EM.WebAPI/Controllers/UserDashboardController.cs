﻿using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/UserDashboard")]
    public class UserDashboardController : ApiController
    {
        private IUserDashboard _userDashboard;
        // GET: api/UserDashboard
        public UserDashboardController(IUserDashboard userDashboardService)
        {
            _userDashboard = userDashboardService;
        }

        public IHttpActionResult Get(string id)
        {
            try
            {
                var response = _userDashboard.Get(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetLatestCampaignByFilter")]
        public IHttpActionResult GetLatestCampaignByFilter(string id,DateTime? dateFrom,DateTime? dateTo)
        {
            try
            {
                var response = _userDashboard.Get(id, dateFrom, dateTo);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetEmailActivity")]
        public IHttpActionResult GetEmailActivity(string id, DateTime? dateFrom, DateTime? dateTo)
        {
            try
            {
                return Ok(_userDashboard.GetEmailActivity(id, dateFrom, dateTo));
                
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Get(string id , int PageNumber, int PageSize)
        {
            try
            {
                var response = _userDashboard.Get(id, PageNumber, PageSize);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetUnSubscriber")]
        public IHttpActionResult GetUnSubscriber(string id, int PageNumber, int PageSize)
        {
            try
            {
                var response = _userDashboard.GetUnsubscriber(id, PageNumber, PageSize);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetAllEmailActivity")]
        public IHttpActionResult GetAllEmailActivity(string id, DateTime? dateFrom, DateTime? dateTo)
        {
            try
            {
                return Ok(_userDashboard.GetAllEmailActivity(id, dateFrom, dateTo));

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetDashboardReport")]
        public IHttpActionResult GetDashboardReport(string id, int PageNumber, int PageSize)
        {
            try
            {
                return Ok(_userDashboard.GetDashboardReport(id, PageNumber, PageSize));

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        // POST: api/UserDashboard
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/UserDashboard/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/UserDashboard/5
        public void Delete(int id)
        {
        }
    }
}
