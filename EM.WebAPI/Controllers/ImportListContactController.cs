﻿using EM.Factory;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using System.Web.Http.Cors;
using System;
using EM.Helpers;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/ImportListContact")]
    public class ImportListContactController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        
        public IEnumerable<EM.Factory.ViewModels.ListViewModel> Get(string Id)
        {
            try
            {
                var list = (from lst in db.Lists
                           where lst.IsDeleted==false && lst.CreatedById==Id
                              select new EM.Factory.ViewModels.ListViewModel{
                                  ListId= lst.ListId,
                                  ListName=lst.ListName
                              }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}