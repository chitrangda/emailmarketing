﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CE01DeliveredWebhook")]
    public class CE01DeliveredWebhookController : ApiController
    {
        IMailGunWebhookService mailGunWebhookService;

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public CE01DeliveredWebhookController(IMailGunWebhookService _webhookService)
        {
            mailGunWebhookService = _webhookService;
        }

        // POST: api/WebHook
        public async Task Post([FromBody]DeliveredMailGunModel value)
        {
            try
            {
                string output = JsonConvert.SerializeObject(value);
                if (value.eventdata.tags != null && value.eventdata.tags.Length > 0)
                {
                    DataHelper.writeEventLogTxt(output, "delivered", value.eventdata.tags[1]);

                }
                else
                {
                    DataHelper.writeEventLogTxt(output, "delivered");
                }
            }
            catch
            {

            }
            try
            {
                await mailGunWebhookService.SaveDeliveredEvents(value);
            }
            catch (Exception ex)
            {
                string output = JsonConvert.SerializeObject(value);
                DataHelper.writeEmailEventLogTxt(ex, output);
            }


        }
        public string Get()
        {
            return "value";
        }
    }
}
