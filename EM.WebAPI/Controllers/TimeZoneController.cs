﻿using EM.Factory;
using EM.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/TimeZone")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TimeZoneController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public List<Factory.TimeZone> Get()
        {
            try
            {
                return db.TimeZones.ToList();
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

    }
}
