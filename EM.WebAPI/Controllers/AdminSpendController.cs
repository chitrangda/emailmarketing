﻿using EM.Helpers;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/AdminSpend")]
    public class AdminSpendController : ApiController
    {
        private readonly IAdminSpend adminSpendService;
        public AdminSpendController(IAdminSpend _adminSpendService)
        {
            adminSpendService = _adminSpendService;
        }

        // GET api/AdminData
        public IHttpActionResult Get(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText)
        {
            try
            {
                return Ok(adminSpendService.Get(startDate,endDate,startMonth,endMonth,startYear,endYear,SearchText));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
