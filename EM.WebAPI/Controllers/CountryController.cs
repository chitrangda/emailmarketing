﻿using EM.Factory;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using System.Web.Http.Cors;
using System;
using EM.Helpers;
using System.Configuration;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Country")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CountryController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();


        [Route("GetCountry")]

        public List<Country> Get()
        {
            try
            {
                string[] c = ConfigurationManager.AppSettings.Get("CountryIds").ToString().Split(',');
                return db.Countries.Where(r => c.Contains(r.CountryId.ToString())).ToList();
                //var country = from count in db.Countries
                //              select count;
                //return country;
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        [Route("GetStates")]
        public List<State> GetStates()
        {
            try
            {
                return db.States.ToList();
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        // GET: api/Country/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Country
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Country/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Country/5
        public void Delete(int id)
        {
        }
    }
}
