﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CE01OpenWebhook")]
    public class CE01OpenWebhookController : ApiController
    {
        IMailGunWebhookService mailGunWebhookService;

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public CE01OpenWebhookController(IMailGunWebhookService _webhookService)
        {
            mailGunWebhookService = _webhookService;
        }

        // POST: api/WebHook
        public async Task Post([FromBody]OpenClickMailGunModel value)
        {
            try
            {
                string output = JsonConvert.SerializeObject(value);
                if (value.eventdata.tags != null && value.eventdata.tags.Length > 0)
                {
                    DataHelper.writeEventLogTxt(output, "open", value.eventdata.tags[1]);

                }
                else
                {
                    DataHelper.writeEventLogTxt(output, "open");
                }
            }
            catch
            {

            }
            try
            {
                await mailGunWebhookService.SaveOpenEvents(value);
            }
            catch (Exception ex)
            {
                string output = JsonConvert.SerializeObject(value);
                DataHelper.writeEmailEventLogTxt(ex, output);
            }

        }
        public string Get()
        {
            return "value";
        }
    }
}
