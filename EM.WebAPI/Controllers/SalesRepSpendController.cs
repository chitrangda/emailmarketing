﻿using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/SalesRepSpend")]
    public class SalesRepSpendController : ApiController
    {
        private readonly ISalesRepSpend salesRepSpend;
        public SalesRepSpendController(ISalesRepSpend _salesRepSpend)
        {
            salesRepSpend = _salesRepSpend;
        }
        public IHttpActionResult Get(int? startDate, int? endDate, int? startMonth, int? endMonth, int? startYear, int? endYear, string SearchText,string SalesRepId)
        {
            try
            {
                return Ok(salesRepSpend.Get(startDate, endDate, startMonth, endMonth, startYear, endYear, SearchText, SalesRepId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }

}

