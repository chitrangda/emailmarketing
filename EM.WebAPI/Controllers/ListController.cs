﻿using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    public class ListController : ApiController
    {
        IListService _listService;
        public ListController(IListService listService)
        {
            _listService = listService;
        }

        public IHttpActionResult Get()
        {
            return Ok(_listService.Get());
        }
    }
}
