﻿using EM.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using Newtonsoft.Json;
using EM.WebAPI.Utilities;
using System.Threading.Tasks;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/MailGunFailedWebhook")]
    public class MailGunFailedWebhookController : ApiController
    {
        IMailGunWebhookService mailGunWebhookService;

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public MailGunFailedWebhookController(IMailGunWebhookService _webhookService)
        {
            mailGunWebhookService = _webhookService;
        }

        // POST: api/WebHook
        public async Task Post([FromBody]List<FailedMailGunModel> value)
        {
            //string output = JsonConvert.SerializeObject(value);
            //DataHelper.writeEventLogTxt(output);
            try
            {
                try
                {
                    using (var db = new EmailMarketingDbEntities())
                    {
                        try
                        {
                            string output = JsonConvert.SerializeObject(value);
                            EmailEventJson eej = new EmailEventJson();
                            eej.JSON = output;
                            eej.JsonDate = DateTime.Now;
                            db.EmailEventJsons.Add(eej);
                            await db.SaveChangesAsync();
                        }

                        catch (Exception ex)
                        {
                            await DataHelper.CreateLog(null, ex.Message, "WebHook API JSON");
                        }

                    }
                }
                catch (Exception ex)
                {
                    await DataHelper.CreateLog(null, ex.Message, "WebHook API Post");
                }

                if (value != null)
                {
                    var tasks = new List<Task>();
                    int i = 0;
                    var total = value.Count;
                    while (total > 0)
                    {
                        var eventBatch = value.Skip(i * 1000).Take(1000).ToList();
                        tasks.Add(await Task.Factory.StartNew(async () => await mailGunWebhookService.SaveFailedEvents(eventBatch)));
                        total -= eventBatch.Count;
                        i++;
                    }
                    Task.WaitAll();
                }
            }
            catch (Exception)
            {
                string output = JsonConvert.SerializeObject(value);
                try
                {
                    using (var db = new EmailMarketingDbEntities())
                    {
                        try
                        {
                            EmailEventJson eej = new EmailEventJson();
                            eej.JSON = output;
                            db.EmailEventJsons.Add(eej);
                            await db.SaveChangesAsync();
                        }

                        catch (Exception ex1)
                        {
                            await DataHelper.CreateLog(null, ex1.Message, "WebHook API Post");
                        }

                    }
                }
                catch (Exception ex2)
                {
                    await DataHelper.CreateLog(null, ex2.Message, "WebHook API Post");
                }
            }
            //return Ok();

        }

        public string Get(int id)
        {
            return "value";
        }
    }
}