﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EM.Factory;
using System.Data.Entity;
using EM.WebAPI.Interfaces;
using System.Web.Http.Cors;
using EM.Factory.ViewModels;
using EM.Helpers;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/UserInfo")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserInfoController : ApiController
    {
        // GET: api/UserInfo
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        private readonly IUserInfoService userinfoService;

        public UserInfoController(IUserInfoService _userinfoService)
        {
            userinfoService = _userinfoService;
        }

        public IHttpActionResult Get(string FirstName, string LastName, string MobilePhone, string City, string State, string Country, string Zip, bool? isActive, int? PageSize, int? StartIndex)
        {
            try
            {
                return Ok(userinfoService.Get(FirstName, LastName, MobilePhone, City, State, Country, Zip, isActive, PageSize, StartIndex));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        //public IHttpActionResult Get(string search, int? PageSize, int? StartIndex)
        //{

        //    try
        //    {
        //        return Ok(userinfoService.Get(search, PageSize, StartIndex));
        //    }
        //    catch (Exception ex)
        //    {
        //        CreateLog.Log(ex.Message, ex);
        //        return InternalServerError();
        //    }
        //}

        // GET: api/UserInfo/5
        public IHttpActionResult Get(string id)
        {

            try
            {
                return Ok(userinfoService.Get(id));

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }



        }

        [Route("GetAuthorizationInfo/{userId?}")]
        public IHttpActionResult GetAuthorizationInfo(string userId)
        {
            try
            {
                var response = userinfoService.GetAuthorizationInfo(userId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        // POST: api/UserInfo
        public void Post([FromBody]string value)
        {
        }

        public IHttpActionResult Put([FromUri] string id, [FromUri] bool status)
        {
            try
            {
                var response = userinfoService.Put(id, status);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        // PUT: api/UserInfo/5
        [HttpPut]
        public IHttpActionResult Put([FromBody]UserInfoViewModel user)
        {
            try
            {
                var response = userinfoService.Put(user);
                return Ok(response);
            }
          catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        // DELETE: api/UserInfo/5
        public void Delete(int id)
        {
        }
    }
}
