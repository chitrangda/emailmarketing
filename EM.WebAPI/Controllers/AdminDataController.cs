﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using EM.Helpers;
using EM.WebAPI.Interfaces;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/AdminData")]
    public class AdminDataController : ApiController
    {
        private readonly IAdminData adminDataService;
        public AdminDataController(IAdminData _adminDataService)
        {
            adminDataService = _adminDataService;
        }


        // GET api/<controller>
        public IHttpActionResult Get(DateTime? FromDate=null, DateTime? ToDate=null, int? Year=null, string Quarter=null, int? Month=null, string City=null, string State=null, string Zip=null, string TimeZone=null, string Find=null, int? MemberType=null, bool? Status=null, int? CampaignType=null)
        {
            try
            {
                return Ok(adminDataService.Get(FromDate,ToDate,Year,Quarter,Month,City,State,Zip,TimeZone,Find,MemberType,Status,CampaignType));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

   
        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}