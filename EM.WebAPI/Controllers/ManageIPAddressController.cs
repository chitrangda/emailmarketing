﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/ManageIPAddress")]
    public class ManageIPAddressController : ApiController
    {
        private readonly IIPAddressService _iPAddressService;
        public ManageIPAddressController(IIPAddressService iPAddressService)
        {
            _iPAddressService = iPAddressService;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var response = _iPAddressService.Get();
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
       
        [HttpPost]
        public IHttpActionResult PostIP(IPAddressViewModel model)
        {
            try
            {
                var response = _iPAddressService.PostIPAddress(model);
                if (response == 1)
                {
                    return Ok(response);
                }
                else if(response==2)
                {
                    return NotFound();
                }
                else if (response == 3)
                {
                    return Conflict();
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
       
        public IHttpActionResult Get(int id)
        {
            try
            {
                var response = _iPAddressService.Get(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        [HttpGet]
       [Route("GetByPoolId")]
        public IHttpActionResult GetByPoolId(int id)
        {
            try
            {
                var response = _iPAddressService.GetByPoolId(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                var response = _iPAddressService.Delete(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        [Route("PostIPPool")]
        public IHttpActionResult Post(IPAddressViewModel model)
        {
            try
            {
                var response = _iPAddressService.Post(model);
                if (response == true)
                {
                    return Ok(response);
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}