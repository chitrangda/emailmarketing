﻿using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    public class IndustryController : ApiController
    {
        private readonly IIndustryService _industryService;
        public IndustryController(IIndustryService industryService)
        {
            _industryService = industryService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _industryService.Get();
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLogFile.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
