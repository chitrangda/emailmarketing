﻿using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Subscriber")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SubscriberController : ApiController
    {
        ISubscriberService _subscriberService;
        public SubscriberController(ISubscriberService subscriberService)
        {
            _subscriberService = subscriberService;
        }

        public List<Factory.usp_getSubscriberList_Result> Get(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {
            
            try
            {
                var response = _subscriberService.Get(listId, search, pageNumber, pageSize,orderBy);
                return response;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
        public IHttpActionResult Get(int? subscriberId)
        {
            try
            {
                var response = _subscriberService.Get(subscriberId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Post([FromBody]SubscriberViewModel subscriberViewModel)
        {
            try
            {
                var response = _subscriberService.Post(subscriberViewModel);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("SubscriberListPost")]
        public IHttpActionResult Post([FromBody]List<SubscriberViewModel> subscriberViewModels)
        {
            try
            {
                var response = _subscriberService.Post(subscriberViewModels);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("SubscriberListPost1")]
        public IHttpActionResult Post1([FromBody]List<SubscriberViewModel> subscriberViewModels)
        {
            try
            {
                var response = _subscriberService.Post1(subscriberViewModels);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("NotExistSubscriber")]
        public IHttpActionResult PostSubscriber([FromBody]List<SubscriberViewModel> subscriberViewModels)
        {
            try
            {
                var response = _subscriberService.GetNotExistSubscribers(subscriberViewModels);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Delete(string id,int ListId)
        {
            try
            {
                List<int> subscriberId = new List<int>();
                if (id.Contains(","))
                {
                    subscriberId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int subsId = Convert.ToInt32(id);
                    subscriberId.Add(subsId);
                }
                var response = _subscriberService.Delete(subscriberId, ListId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
            
        }
        [Route("DeleteAll")]
        public IHttpActionResult Delete(int? listId)
        {

            try
            {
                var response = _subscriberService.Delete(listId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("SetSubsribeStatus")]
        public IHttpActionResult Put(int? id, bool isSubsribe,int ListId)
        {
            try
            {
                var response = _subscriberService.Put(id,isSubsribe,ListId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }
        [Route("UnsubscriberList")]
        public List<Factory.usp_getUnSubscriberList_Result> GetUnsubscribers(int? listId, string serach, int? pageNumber, int? pageSize, string orderBy)
        {
           
            try
            {
                var response = _subscriberService.GetUnsubscribers(listId, serach, pageNumber, pageSize ,orderBy);
                return response;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
        [Route("SubscriberList")]
        public IHttpActionResult Put(string id , int ListId)
        {
            try
            {
                List<int> subscriberId = new List<int>();
                if (id.Contains(","))
                {
                    subscriberId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int subsId = Convert.ToInt32(id);
                    subscriberId.Add(subsId);
                }
                var response = _subscriberService.Put(subscriberId,ListId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
           
        }

        [Route("DeletedSubscriberList")]
        public List<Factory.usp_getDeletedSubscriberList_Result> GetDeletedSubscriberList(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {

            try
            {
                var response = _subscriberService.Get1(listId, search, pageNumber, pageSize, orderBy);
                return response;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        [HttpGet]
        [Route("RecoverDeleteSubscriber")]
        public IHttpActionResult RecoverDeleteSubscriber(string id , int listId)
        {
            try
            {
                List<int> subscriberId = new List<int>();
                if (id.Contains(","))
                {
                    subscriberId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int subsId = Convert.ToInt32(id);
                    subscriberId.Add(subsId);
                }
                var response = _subscriberService.RecoverDeleteSubscriber(subscriberId , listId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }


        [Route("SuppressedSubscriberList")]
        public List<Factory.usp_getSuppressedSubscribersByListId_Result> GetSuppressedSubscriberEmail(int? listId, string search, int? pageNumber, int? pageSize, string orderBy)
        {

            try
            {
                var response = _subscriberService.GetSuppressedSubscribers(listId, search, pageNumber, pageSize, orderBy);
                return response;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        [Route("CheckSuppressedSubscriber")]
        public IHttpActionResult PostSuppressedSubscriber([FromBody]List<SubscriberViewModel> subscriberViewModels)
        {
            try
            {
                var response = _subscriberService.SuppressedSubscriber(subscriberViewModels);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("EventbriteSyncingAttendees")]
        public IHttpActionResult EventbriteSyncingAttendees([FromBody]List<SubscriberViewModel> subscriberViewModels)
        {
            try
            {
                var response = _subscriberService.EventbriteSyncingAttendees(subscriberViewModels);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


    }
}
