﻿using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Billing")]
    // Created on 3:21 PM 1/3/2019 By Alok Pandey
    public class BillingController : ApiController
    {
        private IBillingService _billingService;
        public BillingController(IBillingService billingService)
        {
            _billingService = billingService;
        }

        [Route("GetPaymentPofileById/{id?}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var response = _billingService.Get(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
          
        }

        public IHttpActionResult Get(string id)
        {
            try
            {
                var response = _billingService.Get(id);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Post(UserPaymentProfileViewModel userPaymentProfileViewModel)
        {
            try
            { 
            var response = _billingService.Post(userPaymentProfileViewModel);
            return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
            
        }

        public IHttpActionResult Delete(string id)
        {
            try
            {
                List<int> paymentId = new List<int>();
                if (id.Contains(","))
                {
                    paymentId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int pytId = Convert.ToInt32(id);
                    paymentId.Add(pytId);
                }
                var response = _billingService.Delete(paymentId);
                return Ok(response);
                
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
           
        }

        [Route("DeleteSingle/{id?}")]
        public IHttpActionResult Delete(int? id)
        {
            try
            {
                var response = _billingService.Delete(id);
                return Ok();
                }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();

            }
        }

        public IHttpActionResult Put(int id,string UserId)
        {
            try
            {
                var response = _billingService.setPrimary(id, UserId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }   

        }

    }
}
