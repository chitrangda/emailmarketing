﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/SalesRepData")]
    public class SalesRepDataController : ApiController
    {
        private readonly ISalesRepDataService _salesRepDataService;
        public SalesRepDataController(ISalesRepDataService salesRepDataService)
        {
            _salesRepDataService = salesRepDataService;
        }
        public IHttpActionResult Get(string id)
        {
            try
            {
                return Ok(_salesRepDataService.Get(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetGridData")]
        public IHttpActionResult GetGridData(string id)
        {
            try
            {
                return Ok(_salesRepDataService.GetSalesRepData(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("FilterSalesRepData")]
        public IHttpActionResult Get(string id,string key,string value, string clientType,string MemberType)
        {
            try
            {
                return Ok(_salesRepDataService.GetFilterSalesRepData(id,key,value,clientType, MemberType));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetSalesRep")]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(_salesRepDataService.Get());

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetClientData")]
        public IHttpActionResult GetClientData(string id)
        {
            try
            {
                return Ok(_salesRepDataService.GetClientData(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Post(usp_getUserDetails_Result model)
        {
            try
            {
                if (_salesRepDataService.UpdateClientData(model) == true)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetData")]
        public IHttpActionResult GetData(string id, DateTime? FromDate=null, DateTime? ToDate=null, int? Year=null, string Quarter=null, int? Month=null, string City = null, string State = null, string Zip = null, string TimeZone = null, string Find = null, int? MemberType = null, bool? Status = null, int? CampaignType = null)
        {
            try
            {
                return Ok(_salesRepDataService.GetData(id,FromDate,ToDate,Year,Quarter,Month,City,State,Zip,TimeZone,Find,MemberType,Status,CampaignType));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
