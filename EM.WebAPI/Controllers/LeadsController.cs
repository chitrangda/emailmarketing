﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Leads")]
    public class LeadsController : ApiController
    {
        private ILeadService _leadService;
        public LeadsController(ILeadService leadService)
        {
            _leadService = leadService;
        }
        public IHttpActionResult Post(SalesPortal_Leads model)
        {
            try
            {
                var response = _leadService.Post(model);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();

            }


        }

    }
}
