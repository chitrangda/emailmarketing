﻿using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/ManageUser")]
    public class ManageUserController : ApiController
    {
        private IManageUserService _manageUserService;
        public ManageUserController(IManageUserService manageUserService)
        {
            _manageUserService = manageUserService;
        }
        [Route("GetAccountDetails")]
        public IHttpActionResult GetNewInvoice(string id)
        {
            try
            {
                return Ok(_manageUserService.getAccountDetails(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Put(AccountDetailsViewModel model)
        {
            try
            {
                var response = _manageUserService.Put(model);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetBillingDetails")]
        public IHttpActionResult Get(string id)
        {
            try
            {
                return Ok(_manageUserService.Get(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetUserActivityLog")]
        public IHttpActionResult getActivityLog(string Userid, DateTime? fromDate, DateTime? ToDate)
        {
            try
            {
                return Ok(_manageUserService.getActivityLog(Userid, fromDate, ToDate));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
