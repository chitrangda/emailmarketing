﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/AdminUpcomingBilling")]
    public class AdminUpComingBillingController : ApiController
    {
        private readonly IAdminUpcomingBilling adminUpComingBillingService;
        public AdminUpComingBillingController(IAdminUpcomingBilling _adminUpComingBillingService)
        {
            adminUpComingBillingService = _adminUpComingBillingService;
        }
        public IHttpActionResult Get(DateTime? dateFrom , DateTime? dateTo , int? pageIndex , int? pageSize)
        {
            try
            {
                return Ok(adminUpComingBillingService.Get(dateFrom , dateTo , pageIndex, pageSize));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetDetails")]
        public IHttpActionResult Get(DateTime? dateFrom, DateTime? dateTo, int? pageIndex, int? pageSize,string Id)
        {
            try
            {
                return Ok(adminUpComingBillingService.Get(null,null,null,null,Id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Post(usp_getUpComingBillingDetails_Result model)
        {
            try
            {
                return Ok(adminUpComingBillingService.Post(model));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
