﻿using EM.Factory;
using EM.Helpers;
using Newtonsoft.Json;
using NMIPayment;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/BillingPaymentService")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BillingPaymentServiceController : ApiController
    {
        EmailMarketingDbEntities dbContext = new EmailMarketingDbEntities();


        [AllowAnonymous]
        public IHttpActionResult Get(string TokenId)
        {
            //var token = Request.GetQueryNameValuePairs();
            //string TokenId = token.LastOrDefault(x => x.Key == "token-id").Value;
            try
            {
                if (TokenId != null)
                {
                    CreateLog.Log(TokenId);
                    PaymentStatus paymentStatus = new PaymentStatus();
                    paymentStatus = Payment.getPaymentstatus(TokenId, false);
                    CreateLog.Log(TokenId);
                    string output = JsonConvert.SerializeObject(paymentStatus);
                    CreateLog.Log(output);

                    if (paymentStatus != null)
                    {

                        AnTransactionLog transLog = dbContext.AnTransactionLogs.Where(r => r.TransCode == TokenId).FirstOrDefault();
                        if (transLog != null)
                        {
                            transLog.AnTransactionId = paymentStatus.transactionid;
                            transLog.TransactionDate = DateTime.Now;
                            transLog.StatusCode = paymentStatus.resultcode;
                            transLog.StatusText = paymentStatus.resulttext;
                            dbContext.SaveChanges();
                        }
                        if (paymentStatus.result == "1")
                        {
                            int PackageId = Convert.ToInt32(paymentStatus.CustomField2);
                            int UserPlanId = Convert.ToInt32(paymentStatus.PoNumber);
                            string UserId = paymentStatus.CustomField;

                            //inactive existing plan
                            try
                            {
                                var _curPlan = dbContext.UserPlans.Where(r => r.UserId == UserId && r.IsActive == true).FirstOrDefault();
                                if (_curPlan != null)
                                {
                                    _curPlan.IsActive = false;
                                    dbContext.SaveChanges();
                                }
                                var planDetail = dbContext.Packages.SingleOrDefault(r => r.Id == PackageId);
                                var planName = dbContext.Plans.SingleOrDefault(r => r.PlanId == planDetail.PlanId);

                                //create new plan
                                UserPlan oPlan = new UserPlan();
                                oPlan.UserId = UserId;
                                oPlan.TotalEmails = Convert.ToInt32(_curPlan.MonthlyCredit);
                                oPlan.TotalDays = 30;
                                oPlan.PackageId = PackageId;
                                oPlan.MonthlyCredit = _curPlan.MonthlyCredit;
                                oPlan.NoOfSubscribersMin = 0;
                                oPlan.NoOfSubscribersMax = _curPlan.NoOfSubscribersMax;
                                oPlan.PackageUpdateDate = DateTime.Now;
                                oPlan.BillingAmount = _curPlan.BillingAmount != null ? _curPlan.BillingAmount.Value : 0;
                                oPlan.totalRemainingCredits = _curPlan.MonthlyCredit;
                                oPlan.ContactManagement = _curPlan.ContactManagement;
                                oPlan.UnlimitedEmails = _curPlan.UnlimitedEmails;
                                oPlan.EmailScheduling = _curPlan.EmailScheduling;
                                oPlan.EducationalResources = _curPlan.EducationalResources;
                                oPlan.LiveSupport = _curPlan.LiveSupport;
                                oPlan.CustomizableTemplates = _curPlan.CustomizableTemplates;
                                oPlan.totalSpendCredits = "0";
                                oPlan.IsActive = true;
                                oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                                oPlan.BillingStatus = 1;
                                dbContext.UserPlans.Add(oPlan);
                                dbContext.SaveChanges();

                                //create invoice
                                Invoice invoiceModel = new Invoice();
                                invoiceModel.InvoiceID = invoiceModel.InvoiceID;
                                invoiceModel.UserId = paymentStatus.CustomField;
                                invoiceModel.AmountCharged = paymentStatus.amount;
                                invoiceModel.OrderID = paymentStatus.OrderId;
                                invoiceModel.CardNumber = paymentStatus.CCNumber;
                                invoiceModel.CardType = paymentStatus.CustomField3;
                                invoiceModel.ExpiresMonth = paymentStatus.CCEXp.Substring(0, 2);
                                invoiceModel.ExpiresYear = DateTime.Now.Year.ToString().Substring(0, 2) + paymentStatus.CCEXp.Substring(2, 2);
                                invoiceModel.NameOnCard = paymentStatus.FirstName + " " + paymentStatus.LastName;
                                invoiceModel.Phone = paymentStatus.Phone;
                                invoiceModel.Address = paymentStatus.Address1;
                                invoiceModel.City = paymentStatus.City;
                                invoiceModel.State = paymentStatus.State;
                                invoiceModel.Zip = paymentStatus.Zip;
                                invoiceModel.CreatedById = paymentStatus.CustomField;
                                invoiceModel.CreatedDate = DateTime.Now;
                                invoiceModel.AnTransactionId = paymentStatus.transactionid;
                                invoiceModel.InvoiceType = paymentStatus.OrderDesc;
                                dbContext.Invoices.Add(invoiceModel);
                                dbContext.SaveChanges();
                                //EM.WebAPI.Utilities.SendGridEmails sendGrid = new EM.WebAPI.Utilities.SendGridEmails();
                                EM.WebAPI.Utilities.MailGunEmailHelper _mailGun = new EM.WebAPI.Utilities.MailGunEmailHelper();
                                string _creditCardNo = "XXXX-XXXX-XXXX-" + paymentStatus.CCNumber.Substring(paymentStatus.CCNumber.Length - 4);
                                _mailGun.sendSingleEmailInvoice(paymentStatus.CustomField, "Monthly Billing Invoice", EmailHtml.getInvoiceSend(paymentStatus.FirstName + " " + paymentStatus.LastName,
                                    DateTime.Now.ToString("MM/dd/yyyy"), "Your Monthly Billing for your " + planName.PlanName + " Plan. Your card has been charged in the amount of",
                                    paymentStatus.amount, (paymentStatus.FirstName + " " + paymentStatus.LastName),
                                   paymentStatus.Address1, paymentStatus.City, paymentStatus.State, paymentStatus.Zip,
                                    paymentStatus.CustomField3, _creditCardNo,
                                     paymentStatus.Phone, paymentStatus.transactionid, planName.PlanName + " Plan", oPlan.NoOfSubscribersMax.ToString() + " Subscribers", ""),
                                    System.Configuration.ConfigurationManager.AppSettings["BillingEmail"],
                                    System.Configuration.ConfigurationManager.AppSettings["BillingName"],
                                    paymentStatus.Email,
                                    paymentStatus.FirstName + "" + paymentStatus.LastName);

                                //sendGrid.SendGridSingleEmailSync(paymentStatus.CustomField, "Monthly Invoice",
                                //    EmailHtml.getInvoiceSend(paymentStatus.FirstName + " " + paymentStatus.LastName,
                                //    DateTime.Now.ToString("MM/dd/yyyy"), "Monthly Invoice", paymentStatus.amount,
                                //    (paymentStatus.FirstName + " " + paymentStatus.LastName),
                                //    paymentStatus.Address1, paymentStatus.City, paymentStatus.State, paymentStatus.Zip, 
                                //    paymentStatus.CustomField3, 
                                //    paymentStatus.CCNumber,
                                //    paymentStatus.Phone, paymentStatus.transactionid, "", "" ,""), 
                                //    System.Configuration.ConfigurationManager.AppSettings["SupportEmail"], 
                                //    System.Configuration.ConfigurationManager.AppSettings["SupportName"], 
                                //    paymentStatus.Email, 
                                //    paymentStatus.FirstName + "" + paymentStatus.LastName);

                                return Ok();
                            }
                            catch (Exception ex)
                            {
                                //EM.PaymentService.LogService.CreateLog(UserId, ex.Message, "Payment Service - ChargeCustomers");
                                CreateLog.Log(ex.Message, ex);
                                return InternalServerError();
                            }
                        }
                        else
                        {

                            var oTrans = dbContext.usp_getTransactionInfo(TokenId).FirstOrDefault();

                            if (oTrans.UserId != null && oTrans.UserId != "")
                            {
                                var _curPlan = dbContext.UserPlans.Where(r => r.UserId == oTrans.UserId && r.IsActive == true).FirstOrDefault();
                                if (_curPlan != null)
                                {
                                    _curPlan.IsActive = false;
                                    dbContext.SaveChanges();
                                }
                                var oUser = dbContext.UserProfiles.Where(r => r.Id == oTrans.UserId).FirstOrDefault();
                                if (oUser != null)
                                {
                                    oUser.IsActive = false;
                                    dbContext.SaveChanges();

                                }
                            }
                            //EM.WebAPI.Utilities.SendGridEmails sendGrid = new EM.WebAPI.Utilities.SendGridEmails();
                            EM.WebAPI.Utilities.MailGunEmailHelper _mailGun = new EM.WebAPI.Utilities.MailGunEmailHelper();
                            string _creditCardNo = "XXXX-XXXX-XXXX-" + oTrans.CreditCardNo.Substring(oTrans.CreditCardNo.Length - 4);
                            _mailGun.sendSingleEmailInvoice(
                                oTrans.ContactEmail,
                                "Monthly Billing Invoice",
                                EmailHtml.getInvoiceSend(
                                    oTrans.FirstName + " " + oTrans.LastName,
                                    DateTime.Now.ToString("MM/dd/yyyy"),
                                   "Your Monthly Billing for your " + oTrans.Message + " Plan. Your card has not been charged in the amount of",
                                    oTrans.Amount,
                                    oTrans.FirstName + " " + oTrans.LastName,
                                    oTrans.Address1, oTrans.City, oTrans.State, oTrans.Zip, oTrans.CardType, _creditCardNo,
                                    oTrans.MobilePhone, oTrans.AnTransactionId, oTrans.Message, "", ""),
                                System.Configuration.ConfigurationManager.AppSettings["BillingEmail"],
                                System.Configuration.ConfigurationManager.AppSettings["BillingName"],
                                oTrans.ContactEmail,
                                oTrans.FirstName + "" + oTrans.LastName);


                        }
                    }
                    return Ok();

                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }
    }
}
