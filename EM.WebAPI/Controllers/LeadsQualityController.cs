﻿using EM.Factory;
using EM.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/LeadsQuality")]
    public class LeadsQualityController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public IEnumerable<SalesPortal_LeadQuality> Get()
        {
            try
            {
                var quality = from count in db.SalesPortal_LeadQuality
                              select count;
                return quality;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}
