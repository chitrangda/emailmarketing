﻿using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Invoice")]
    public class InvoiceController : ApiController
    {
        private IInvoiceService _invoiceService;
        public InvoiceController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        public IHttpActionResult Post(InvoiceViewModel invoiceViewModel)
        {
            try
            {
                var response = _invoiceService.Post(invoiceViewModel);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();

            }


        }

        [Route("GetNewInvoice")]
        public IHttpActionResult GetNewInvoice(string id)
        {
            try
            {
                return Ok(_invoiceService.getNewInvoice(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetUserInvoice")]
        public IHttpActionResult GetUserInvoice(string id)
        {
            try
            {
                return Ok(_invoiceService.getUSerInvoice(id));
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message,ex);
                return InternalServerError();
            }
        }

        [Route("GetInvoiceDetails")]
        public IHttpActionResult GetInvoiceDetails(string id)
        {
            try
            {
                return Ok(_invoiceService.getInvoiceDetails(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

    }
}
