﻿using EM.Factory.ViewModels;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EM.Factory;
using EM.Helpers;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Campaign")]
    public class CampaignController : ApiController
    {
        private ICampaignService _campaignService;
        public CampaignController(ICampaignService campaignService)
        {
            _campaignService = campaignService;
        }
        [Route("GetListDetailsByListId/{id?}")]
        public IHttpActionResult GetListDetailsByListId(int id)
        {
            try
            {
                var response = _campaignService.GetListDetailsByListId(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
            
        }

        public IHttpActionResult Post([FromBody] CampaignViewModel model)
        {
            try
            {
                var response = _campaignService.Post(model);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

      public IHttpActionResult Get(string id,string Search, int? pageNumber , int? pageSize , string orderBy)
        {
            try
            {
                var response = _campaignService.Get(id, Search, pageNumber, pageSize, orderBy);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }

        [Route("GetCampaignById/{id?}")]
        public IHttpActionResult Get(int? id)
        {
            try
            {
                var response = _campaignService.Get(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetCampaignByUserId/{id?}")]
        public IHttpActionResult Get(string id)
        {
            try
            {
                var response = _campaignService.Get(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetSendCampaign")]
        public IHttpActionResult GetSendCampaign(string userid, int? pageNumber, int? pageSize, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                var response = _campaignService.GetSendCampaign(userid, pageNumber, pageSize ,fromDate, toDate);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetCampaigns")]
        public IHttpActionResult GetCampaigns(string userid, int? pageNumber, int? pageSize, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                var response = _campaignService.GetCampaigns(userid, pageNumber, pageSize, fromDate, toDate);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetSendCampaignDetail")]
        public IHttpActionResult GetSendCampaignDetail(string userid, int CampaignId)
        {
            try
            {
                var response = _campaignService.GetSendCampaignDetails(userid, CampaignId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetRecipientsEmails")]
        public IHttpActionResult GetRecipientsEmails(int campaignId, int? pageNumber , int? PageSize)
        {
            try
            {
                var response = _campaignService.GetRecipientsEmails(campaignId, pageNumber , PageSize);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetUnSubscribeEmails")]
        public IHttpActionResult GetUnSubscribeEmails(int campaignId, string UserId, int? pageNumber, int? PageSize, string EmailSearch)
        {
            try
            {
                var response = _campaignService.GetUnSubscribeEmails(campaignId,UserId,pageNumber,PageSize,EmailSearch);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


        [Route("GetCampaignStat")]
        public IHttpActionResult Get(int CampaignId, string Stat, int PageNumber, int? PageSize, string Email)
        {
            try
            {
                var response = _campaignService.GetCampaignStat(CampaignId, Stat , PageNumber ,PageSize , Email);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetCampaignLinkStat")]
        public IHttpActionResult GetCampaignLinkStat(int CampaignId)
        {
            try
            {
                var response = _campaignService.GetCampaignLinkStat(CampaignId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


        [Route("GetStat")]
        public IHttpActionResult GetStat(int CampaignId, string Email)
        {
            try
            {
                var response = _campaignService.Stat(CampaignId, Email);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetSingleCampaignReport")]
        public IHttpActionResult GetSingleCampaignReport(string UserId, int CampaignId)
        {
            try
            {
                var response = _campaignService.SingleCampaignReport(UserId, CampaignId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("getReportChartData")]
        public IHttpActionResult getReportChartData(DateTime? dateFrom, DateTime? dateTo, string aggregated_by, int CampaignId)
        {
            try
            {
                return Ok(_campaignService.GetReportChart(dateFrom, dateTo, aggregated_by, CampaignId));

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetCampaignDidntOpen")]
        public IHttpActionResult GetCamapignDidntOpenDetails( int CampaignId , string UserId, int PageNumber, int? PageSize, string EmailSearch)
        {
            try
            {
                var response = _campaignService.GetDidntEmail(CampaignId, UserId, PageNumber , PageSize , EmailSearch);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetSuppressedEmails")]
        public IHttpActionResult GetSuppressedEmails(int CampaignId, int PageNumber, int? PageSize, string EmailSearch)
        {
            try
            {
                var response = _campaignService.GetSuppressedEmails(CampaignId, PageNumber, PageSize, EmailSearch);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [HttpPut]
        [Route("UnsubscribedfromCampaign")]
        public IHttpActionResult UnsubscribedfromCampaign(int id, string email, string uid)
        {
            try
            {
               var response = _campaignService.Put(id,email,uid);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetCampaignDeliveredEmails")]
        public IHttpActionResult GetCampaignDeliveredEmails(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch)
        {
            try
            {
                var response = _campaignService.GetDeliveredEmail(CampaignId, UserId, PageNumber, PageSize, EmailSearch);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetCampaignRecipientEmails")]
        public IHttpActionResult GetCampaignRecipientEmails(int CampaignId, string UserId, int PageNumber, int? PageSize, string EmailSearch)
        {
            try
            {
                var response = _campaignService.GetSubscriberEmail(CampaignId, UserId, PageNumber, PageSize, EmailSearch);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

    }
}
