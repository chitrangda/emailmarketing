﻿using EM.Factory;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    public class EmailListAPIController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        // GET: api/Group
        public IEnumerable<List> Get()
        {
            var group = from p in db.Lists
                        where p.IsDeleted == false
                        select p;
            return group;
        }
        // GET: api/Group/5
        public IHttpActionResult Get(int id)
        {
            var group = db.Lists.Where(i => i.GroupId == id).FirstOrDefault();
            if (group == null)
            {
                return NotFound();
            }
            return Ok(group);
        }
        [HttpPost]
        [Route("api/AddGroup")]
        // POST: api/Group
        public IHttpActionResult Post([FromBody]List group)
        {
            db.Lists.Add(group);
            db.SaveChanges();
            return Ok();
        }
        [HttpPut]
        [Route("api/UpdateGroup")]

        // PUT: api/Group/5
        public IHttpActionResult Put(int id, [FromBody]List group)
        {
            var groups = db.Lists.Where(i => i.GroupId == group.GroupId).FirstOrDefault();
            if (groups == null)
            {
                return NotFound();
            }
            groups.GroupName = groups.GroupName;
            db.SaveChanges();
            return Ok();
        }

        // DELETE: api/Group/5
        public IHttpActionResult Delete(int id)
        {
            List group = db.Lists.Find(id);
            if (group == null)
            {
                return NotFound();
            }
            group.IsDeleted = true;
            db.SaveChanges();
            return Ok(group);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
