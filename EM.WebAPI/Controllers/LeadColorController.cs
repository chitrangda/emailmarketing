﻿using EM.Factory;
using EM.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/LeadColor")]
    public class LeadColorController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public IEnumerable<SalesPortal_lead_Color> Get()
        {
            try
            {
                var color = from count in db.SalesPortal_lead_Color
                              select count;
                return color;
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}
