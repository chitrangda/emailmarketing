﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using EM.Helpers;
using EM.WebAPI.Interfaces;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/AdminDashboard")]
    public class AdminDashboardController : ApiController
    {

        private readonly IAdminDashboardService adminDataService;
        public AdminDashboardController(IAdminDashboardService _adminDataService)
        {
            adminDataService = _adminDataService;
        }

        // GET api/AdminData
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(adminDataService.Get());
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetGridData")]
        public IHttpActionResult GetGridData(string Name, string Email, string MobileNumber, string NameOnCard, string MumberType, string ReferralCode, string City, string State, string SalesRep,string MemberType,string ClientType)
        {
            try
            {
                return Ok(adminDataService.GetAdminData(Name, Email, MobileNumber, NameOnCard, MumberType, ReferralCode, City, State, SalesRep, MemberType, ClientType));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


    }
}