﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Notes")]
    public class NotesController : ApiController
    {
        private readonly INoteService noteService;
        public NotesController(INoteService _noteService)
        {
            noteService = _noteService;
        }

        public IHttpActionResult Post(SalesPortalRepNote model)
        {
            try
            {
                return Ok(noteService.Post(model));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Get(string id)
        {
            try
            {
                return Ok(noteService.Get(id));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Delete(int id, string userid)
        {
            try
            {
                if (noteService.Delete(id,userid) == true)
                    return Ok();
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
