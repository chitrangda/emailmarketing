﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CE01ClickWebhook")]
    public class CE01ClickWebhookController : ApiController
    {
        IMailGunWebhookService mailGunWebhookService;

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public CE01ClickWebhookController(IMailGunWebhookService _webhookService)
        {
            mailGunWebhookService = _webhookService;
        }

        public async Task Post([FromBody]OpenClickMailGunModel value)
        {
            try
            {
                string output = JsonConvert.SerializeObject(value);
                if (value.eventdata.tags != null && value.eventdata.tags.Length > 0)
                {
                    DataHelper.writeEventLogTxt(output, "click", value.eventdata.tags[1]);

                }
                else
                {
                    DataHelper.writeEventLogTxt(output, "click");
                }

            }
            catch
            {

            }
            try
            {
                await mailGunWebhookService.SaveClickEvents(value);
            }
            catch (Exception ex)
            {
                //await DataHelper.CreateLog(null, ex.Message, "WebHook API Post");
                string output = JsonConvert.SerializeObject(value);
                DataHelper.writeEmailEventLogTxt(ex, output);
            }
        }

        public string Get()
        {
            return "value";
        }
    }
}
