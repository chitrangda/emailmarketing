﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Providers")]
    public class ProvidersController : ApiController
    {
        private readonly IProvidersService _providersService;
        public ProvidersController(IProvidersService providersService)
        {
            _providersService = providersService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _providersService.Get();
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
        public IHttpActionResult Post(SalesPortal_Providers model)
        {
            try
            {
                var response = _providersService.Post(model);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
        public IHttpActionResult Get(int id)
        {
            try
            {
                var response = _providersService.Get(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                var response = _providersService.Delete(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}
