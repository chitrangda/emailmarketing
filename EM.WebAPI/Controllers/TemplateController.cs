﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Template")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class TemplateController : ApiController
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        ITemplateService templateService;
        public TemplateController(ITemplateService _templateService)
        {
            templateService = _templateService;
        }

        public IHttpActionResult Post([FromBody]TemplateViewModel template)
        {
            try
            {
                var response = templateService.Post(template);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }
        public IHttpActionResult Get(string id, int? pageNumber, int? pageSize, string search, string orderBy)
        {
            try
            {
                var response = templateService.Get(id, pageNumber, pageSize, search, orderBy);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetTemplateById/{id?}")]
        public IHttpActionResult Get(int? id,string type)
        {
            try
            {
                var response = templateService.Get(id,type);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }
        public IHttpActionResult Delete(string id)
        {
            try
            {
                
                List<int> templateId = new List<int>();
                if (id.Contains(","))
                {
                    templateId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int tmpId = Convert.ToInt32(id);
                    templateId.Add(tmpId);
                }
                var response = templateService.Delete(templateId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetLayout")]
        public IHttpActionResult Get(int? pageNumber, int? pageSize, string search, string orderBy, string version="v2")
        {
            try
            {
                var response = templateService.Get(pageNumber, pageSize, search, orderBy,version);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


    }
}
