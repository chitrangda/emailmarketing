﻿using EM.WebAPI.Interfaces;
using System;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/ClientType")]
    public class ClientTypeController : ApiController
    {
        private readonly IClientTypeService _clientTypeService;
        public ClientTypeController(IClientTypeService clientTypeService)
        {
            _clientTypeService = clientTypeService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _clientTypeService.Get();
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLogFile.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
