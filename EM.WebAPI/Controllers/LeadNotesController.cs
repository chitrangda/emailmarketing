﻿using EM.Factory;
using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/LeadNotes")]
    public class LeadNotesController : ApiController
    {
        private readonly ILeadNoteService leadNoteService;
        public LeadNotesController(ILeadNoteService _leadNoteService)
        {
            leadNoteService = _leadNoteService;
        }

        public IHttpActionResult Post(SalesPortal_LeadNote model)
        {
            try
            {
                return Ok(leadNoteService.Post(model));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Get(int LeadId)
        {
            try
            {
                return Ok(leadNoteService.Get(LeadId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        public IHttpActionResult Delete(int id, string userid)
        {
            try
            {
                if (leadNoteService.Delete(id, userid) == true)
                    return Ok();
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}