﻿using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CardTransaction")]
    public class CardTransactionController : ApiController
    {
        // GET api/<controller>
        private ICardTransaction cardTransService;

        public CardTransactionController(ICardTransaction _cardTrans)
        {
            cardTransService = _cardTrans;
        }

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public IHttpActionResult Get(string id)
        {
            try
            {
                var response = cardTransService.Get(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}