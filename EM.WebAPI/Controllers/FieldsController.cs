﻿using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/Fields")]
    public class FieldsController : ApiController
    {
        private IFieldsService _fieldsService;
        public FieldsController(IFieldsService fieldsService)
        {
            _fieldsService = fieldsService;
        }

        public IHttpActionResult Get(int listId)
        {
            try
            {
                var response = _fieldsService.Get(listId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("ActivateDeactivate/{fields?}/{listId?}/{actionName?}")]
        public IHttpActionResult Get(string fields, int listId, string actionName)
        {
            try
            {
                List<int> fieldsList = new List<int>();
                if (fields.Contains(","))
                {
                    fieldsList = fields.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int lstId = Convert.ToInt32(fields);
                    fieldsList.Add(lstId);
                }
                return Ok(_fieldsService.ActivateDeactivate(fieldsList, listId, actionName));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
    
}
