﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Models;
using Newtonsoft.Json;
using EM.WebAPI.Utilities;
using EM.WebAPI.Interfaces;
using System.Threading.Tasks;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/WebHook")]
    public class WebHookController : ApiController
    {
        private readonly IWebhookService webhookService;
        public WebHookController(IWebhookService _webhookService)
        {
            webhookService = _webhookService;
        }

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        // GET: api/WebHook
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WebHook/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/WebHook
        public async Task Post([FromBody]EventView value)
        {
            try
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    try
                    {
                        string output = JsonConvert.SerializeObject(value);
                        EmailEventJson eej = new EmailEventJson();
                        eej.JSON = output;
                        eej.JsonDate = DateTime.Now;
                        db.EmailEventJsons.Add(eej);
                        await db.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        await DataHelper.CreateLog((value.category[0]), ex.Message, "Web API JSON");
                    }
                }
               await webhookService.SaveEvents(value);
            }
            catch (Exception ex)
            {
                await DataHelper.CreateLog((value.category[0]), ex.Message, "Web API");
            }

        }

        // PUT: api/WebHook/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WebHook/5
        public void Delete(int id)
        {
        }
    }
}
