﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/ManageIPPool")]
    public class ManageIPPoolController:ApiController
    {
        private readonly IIPPoolService _iPPoolService;
        public ManageIPPoolController(IIPPoolService iPPoolService)
        {
            _iPPoolService = iPPoolService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _iPPoolService.Get();
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        public IHttpActionResult Post(IP_Pool model)
        {
            try
            {
                var response = _iPPoolService.Post(model);
                if (response == true)
                {
                    return Ok(response);
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                var response = _iPPoolService.Get(id);

                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                var response = _iPPoolService.Delete(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}