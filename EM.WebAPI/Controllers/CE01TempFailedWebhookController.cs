﻿using EM.Factory;
using EM.WebAPI.Interfaces;
using EM.WebAPI.Models;
using EM.WebAPI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/CE01TempFailedWebhook")]
    public class CE01TempFailedWebhookController : ApiController
    {
        IMailGunWebhookService mailGunWebhookService;

        EmailMarketingDbEntities db = new EmailMarketingDbEntities();

        public CE01TempFailedWebhookController(IMailGunWebhookService _webhookService)
        {
            mailGunWebhookService = _webhookService;
        }

        // POST: api/WebHook
        public async Task Post([FromBody]FailedMailGunModel value)
        {
            try
            {
                string output = JsonConvert.SerializeObject(value);
                if (value.eventdata.tags != null && value.eventdata.tags.Length > 0)
                {
                    DataHelper.writeEventLogTxt(output, "Tempfailed", value.eventdata.tags[1]);

                }
                else
                {
                    DataHelper.writeEventLogTxt(output, "Tempfailed");
                }
            }
            catch
            {

            }
            //try
            //{
            //    await mailGunWebhookService.SaveTempFailedEvents(value);
            //}
            //catch (Exception ex)
            //{
            //    DataHelper.writeEmailEventLogTxt(ex);
            //    //await DataHelper.CreateLog(null, ex.Message, "WebHook API Post");
            //}


        }
        public string Get()
        {
            return "value";
        }
    }
}
