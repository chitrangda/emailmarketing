﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/ManageSalesRep")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ManageSalesRepController : ApiController
    {
        ISalesRepService _salesRep;
        public ManageSalesRepController(ISalesRepService salesRep)
        {
            _salesRep = salesRep;
        }

        [HttpGet]
        public IHttpActionResult Get([FromUri]string FirstName, [FromUri] string LastName, [FromUri] int? NoOfClients, [FromUri] double? SpentTotal, [FromUri] bool? ViewRepActivity, [FromUri] bool? CanExport, [FromUri] bool? ExportLead, [FromUri] bool? Manager, [FromUri] bool? IsActive, [FromUri] int? pageIndex, [FromUri] int? pageSize)
        {
            try
            {
                return Ok(_salesRep.Get(FirstName, LastName, NoOfClients, SpentTotal, ViewRepActivity, CanExport, ExportLead, Manager, IsActive,pageIndex, pageSize));
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [HttpGet]
        public IHttpActionResult Get([FromUri] string id)
        {
            try
            {
                return Ok(_salesRep.Get(id));
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [HttpPut]
        public IHttpActionResult Put([FromBody]SalesRepProfile model)
        {
            try
            {
                return Ok(_salesRep.Put(model));
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
