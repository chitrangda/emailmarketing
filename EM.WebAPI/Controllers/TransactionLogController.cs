﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/TransactionLog")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TransactionLogController : ApiController
    {
        ITransactionLogService _transactionLog;
        public TransactionLogController(ITransactionLogService transactionLog)
        {
            _transactionLog = transactionLog;
        }

        // GET api/<controller>/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var _response = _transactionLog.Get(id);
                return Ok(_response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


        public IHttpActionResult Get(string token)
        {
            try
            {
                var _response = _transactionLog.Get(token);
                return Ok(_response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        // POST api/<controller>
        public IHttpActionResult Post([FromBody]AnTransactionLog model)
        {
            try
            {
                return Ok(_transactionLog.Post(model));

            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

      
    }
}