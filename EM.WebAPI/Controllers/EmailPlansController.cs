﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/EmailPlans")]
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class EmailPlansController : ApiController
    {
        IEmailPlanService emailPlanServices;
       // GET: api/EmailPlans

        public EmailPlansController(IEmailPlanService emailPlanService)
        {
            emailPlanServices = emailPlanService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var planPricing = emailPlanServices.Get();
            return Ok(planPricing);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetPlanByID/{id?}")]
        public IHttpActionResult GetPlanByID(int id , string UserId = null)
        {
            try
            {
                var planPricing = emailPlanServices.GetPlanByID(id, UserId);
                return Ok(planPricing);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        public IHttpActionResult Get(int id)
        {
            try
            {
                var planPricing = emailPlanServices.Get(id);
                return Ok(planPricing);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetPlan")]
        public IHttpActionResult GetPlan()
        {
            try
            {
                var planPricing = emailPlanServices.GetPlan();
                return Ok(planPricing);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("GetPlanPricing")]
        public IHttpActionResult GetPlanPricing(int id)
        {
            try
            {
                var planPricing = emailPlanServices.GetPlanPricing(id);
                return Ok(planPricing);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


    }
}
