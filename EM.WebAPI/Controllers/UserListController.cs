﻿using EM.Factory.ViewModels;
using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/UserList")]
    [EnableCors(origins: "http://localhost:62115", headers: "*", methods: "*")]
    public class UserListController : ApiController
    {
        IUserListService _userList;
        public UserListController(IUserListService userList)
        {
            _userList = userList;
        }
        
        public IHttpActionResult Get(string id,  string listName, int pageNumber, int? pageSize,string orderBy)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var response = _userList.Get(id, listName, pageNumber, pageSize, orderBy);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();

            }
        }
        [HttpGet]
        [Route("DeletedList")]
        public IHttpActionResult GetDeleted(string id, string listName, int pageNumber, int? pageSize, string orderBy)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var response = _userList.GetDeleted(id, listName, pageNumber, pageSize, orderBy);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();

            }
        }

        public IHttpActionResult GetListById(int id)
        {
            try
            {
                var response = _userList.GetListById(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();

            }
        }
        public IHttpActionResult Post([FromBody]ListViewModel list)
        {
            try
            {
                var response = _userList.Post(list);
                return Ok(response);

            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }


        }

        public IHttpActionResult Delete(string id)
        {
            try
            {
               
                List<int> listId = new List<int>();
                if (id.Contains(","))
                {
                    listId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int lstId = Convert.ToInt32(id);
                    listId.Add(lstId);
                }
                var response = _userList.Delete(listId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
            
        }

      
       [HttpGet]
        [Route("RestoreDelete")]
        public IHttpActionResult RestoreDelete(string id)
        {
            try
            {

                List<int> listId = new List<int>();
                if (id.Contains(","))
                {
                    listId = id.Split(',').AsEnumerable().Select(x => Convert.ToInt32(x)).ToList();
                }
                else
                {
                    int lstId = Convert.ToInt32(id);
                    listId.Add(lstId);
                }
                var response = _userList.RestoreDelete(listId);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }

        }


        [Route("ListPost")]
        public IHttpActionResult Post([FromBody]List<ListViewModel> list)
        {
            try
            {
                var response = _userList.Post(list);
                return Ok(response);
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("GetRecent")]
        public IHttpActionResult GetRecent(string id)
        {
           
            try
            {
               var response = _userList.GetRecent(id);
                return Ok(response);
            }
            catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }


    }
}

