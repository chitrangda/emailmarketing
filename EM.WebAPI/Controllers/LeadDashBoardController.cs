﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using EM.Helpers;
using EM.WebAPI.Interfaces;


namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/LeadDashBoard")]
    public class LeadDashBoardController : ApiController
    {
        private readonly ILeadDashboardService leadDashboardService;
        public LeadDashBoardController(ILeadDashboardService _leadDashboard)
        {
            leadDashboardService = _leadDashboard;
        }
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(leadDashboardService.Get());
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }

        [Route("LeadDashBoardData")]
        public IHttpActionResult Get(string RepId, int? LeadQualityID,int? TypeOfLead, int? IndustryId, int? pageIndex, int? pageSize)
        {
            try
            {
               return Ok(leadDashboardService.Get(RepId, LeadQualityID, TypeOfLead, IndustryId, pageIndex, pageSize));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
        [Route("EditLeadDetails")]
        public IHttpActionResult Get(string RepId , int leadId)
        {
            try
            {
                return Ok(leadDashboardService.Get(RepId,leadId));
            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}