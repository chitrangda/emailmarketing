﻿using EM.Factory;
using EM.Helpers;
using EM.WebAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/PromotionCode")]
    public class PromotionCodeController : ApiController
    {
        private readonly PromotionCodeService _promotionCodeService;
        public PromotionCodeController(PromotionCodeService promotionCodeService)
        {
            _promotionCodeService = promotionCodeService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _promotionCodeService.Get();
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
        public IHttpActionResult Post(PromotionCode model)
        {
            try
            {
                var response = _promotionCodeService.Post(model);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
        public IHttpActionResult Get(int id)
        {
            try
            {
                var response = _promotionCodeService.Get(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                var response = _promotionCodeService.Delete(id);
                return Ok(response);

            }
            catch (Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return null;
            }
        }
    }
}
