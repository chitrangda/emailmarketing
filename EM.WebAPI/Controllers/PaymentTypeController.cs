﻿using EM.Helpers;
using EM.WebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EM.WebAPI.Controllers
{
    [RoutePrefix("api/PaymentType")]
  
    public class PaymentTypeController : ApiController
    {
        private readonly IPaymentTypeService _paymentTypeService;
        public PaymentTypeController(IPaymentTypeService paymentTypeService)
        {
            _paymentTypeService = paymentTypeService;
        }
        public IHttpActionResult Get()
        {
            try
            {
                var response = _paymentTypeService.Get();
                return Ok(response);
            }
           catch(Exception ex)
            {
                CreateLog.Log(ex.Message, ex);
                return InternalServerError();
            }
        }
    }
}
