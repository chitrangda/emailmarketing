﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_getAuthorization] @UserId NVARCHAR(128)
AS
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET FMTONLY OFF;
	SET NOCOUNT ON;

	CREATE TABLE #USERPLANTEMP (
		UserId NVARCHAR(128)
		,PackageId INT
		,MonthlyCredit INT
		,TotalSpendCredits INT
		,TotalRemainingCredits INT
		,MaxSubscriber INT
		,MinSubscriber INT
		,CustomizableTemplate BIT
		,TotalEmail INT
		,TotalDays INT
		,TotalSubscriberCreated INT
		,EmailScheduling BIT
		)

	DECLARE @TotalSubsCreated INT;

	SELECT @TotalSubsCreated = COUNT(*)
	FROM dbo.SubscriberList SL
	WHERE SL.CreatedById = @UserId

	INSERT INTO #USERPLANTEMP
	SELECT UP.UserId
		,UP.PackageId
		,UP.MonthlyCredit
		,UP.totalSpendCredits
		,UP.totalRemainingCredits
		,up.NoOfSubscribersMax
		,up.NoOfSubscribersMin
		,UP.CustomizableTemplates
		,UP.TotalEmails
		,UP.TotalDays
		,@TotalSubsCreated
		,UP.EmailScheduling
	FROM dbo.UserPlan UP
	WHERE UP.UserId = @UserId
		AND UP.IsActive = 1

	SELECT *
	FROM #USERPLANTEMP

	DROP TABLE #USERPLANTEMP
END
GO

