﻿
-- =============================================
-- Author:		<Chitra,[usp_getAllUssers]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Users with Filters>

-- Modified By: SuniL Kumar Singh
-- Modified Date: 14 Jan 2019
-- Changes For : Code review and code rework

-- Modified Date: 15 Jan 2019
-- Changes For : Replaced temp table with table variable

------------Testing-----------------------
--[usp_getAllUssers]
--[usp_getAllUssers] NULL,'jack'
--[usp_getAllUssers] NULL,NULL,NULL,'york'

-- =============================================
CREATE PROCEDURE [dbo].[usp_getAllUssers] @FirstName NVARCHAR(200) = NULL
	,@LastName NVARCHAR(200) = NULL
	,@MobilePhone NVARCHAR(200) = NULL
	,@City NVARCHAR(200) = NULL
	,@State NVARCHAR(200) = NULL
	,@Country NVARCHAR(200) = NULL
	,@Zip NVARCHAR(50) = NULL
	,@isActive BIT = NULL
	,@PageSize INT = NULL
	,@StartIndex INT = 0
AS
BEGIN
	SET FMTONLY OFF;
	SET NOCOUNT ON

	DECLARE @totalrecords INT;
	DECLARE @AllUsers TABLE (
		Id NVARCHAR(128)
		,Email NVARCHAR(256)
		,FirstName NVARCHAR(200)
		,LastName NVARCHAR(200)
		,ContactEmail NVARCHAR(200)
		,OfficePhone NVARCHAR(200)
		,MobilePhone NVARCHAR(200)
		,Address1 NVARCHAR(max)
		,Address2 NVARCHAR(max)
		,City VARCHAR(200)
		,STATE NVARCHAR(200)
		,Country NVARCHAR(200)
		,Zip NVARCHAR(50)
		,ProfileImage NVARCHAR(max)
		,Company NVARCHAR(200)
		,isActive BIT
		,CreatedDate DATETIME
		,RecordNumber INT IDENTITY(1, 1) PRIMARY KEY
		);

	WITH CTE_UserDetails
	AS (
		SELECT AspNetUsers.Email
			,UP.Id
			,UP.FirstName
			,UP.LastName
			,UP.ContactEmail
			,UP.OfficePhone
			,UP.MobilePhone
			,UP.Address1
			,UP.Address2
			,UP.City
			,UP.STATE
			,UP.Country
			,UP.Zip
			,UP.ProfileImage
			,UP.CreditCardNo
			,UP.ExpiryDate
			,UP.RegisteredDate
			,UP.IsActive
			,UP.CompanyName
		FROM AspNetUsers
		INNER JOIN UserProfile UP ON AspNetUsers.id = UP.id
		INNER JOIN AspNetUserRoles ON AspNetUsers.id = AspNetUserRoles.UserId
		WHERE AspNetUserRoles.RoleId != '4989ef15-6b78-43bd-b5b1-88d06396b4bf'
		)
	INSERT INTO @AllUsers
	SELECT id
		,Email
		,FirstName
		,LastName
		,ContactEmail
		,OfficePhone
		,MobilePhone
		,Address1
		,Address2
		,City
		,STATE
		,Country
		,Zip
		,ProfileImage
		,CompanyName
		,IsActive
		,RegisteredDate
	FROM CTE_UserDetails cte
	ORDER BY cte.RegisteredDate DESC

	SELECT @totalrecords = COUNT(Id)
	FROM @AllUsers

	IF (@PageSize IS NULL)
		SET @PageSize = @totalrecords

	SELECT id
		,Email
		,FirstName
		,LastName
		,ContactEmail
		,OfficePhone
		,MobilePhone
		,Address1
		,Address2
		,City
		,STATE
		,Country
		,Zip
		,ProfileImage
		,Company
		,isActive
		,CreatedDate
		,RecordNumber
		,@totalrecords AS totalrecords
	FROM @AllUsers
	WHERE RecordNumber >= (@PageSize * @StartIndex) + 1
		AND RecordNumber <= (@PageSize * (@StartIndex + 1))
		AND FirstName LIKE '%' + isnull(@FirstName, FirstName) + '%'
		AND LastName LIKE '%' + isnull(@LastName, LastName) + '%'
		AND MobilePhone LIKE '%' + isnull(@MobilePhone, MobilePhone) + '%'
		AND City LIKE '%' + isnull(@City, City) + '%'
		AND [State] LIKE '%' + isnull(@State, [State]) + '%'
		AND Country LIKE '%' + isnull(@Country, Country) + '%'
		AND Zip LIKE '%' + isnull(@Zip, Zip) + '%'
		AND isActive = isnull(@isActive, isActive)
END
GO

