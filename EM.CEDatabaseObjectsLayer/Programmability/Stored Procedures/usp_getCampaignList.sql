﻿-- =============================================

-- Author:		<Chitra,[usp_getCampaginsSubscriber]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Users with Filters>

----------------------Testing--------------------------
 --[dbo].[usp_getCampaignList] '1e99575f-c92c-4842-9cd3-684e26182436'
-- =============================================


CREATE PROCEDURE [dbo].[usp_getCampaignList] @UserId NVARCHAR(100)
	,@Search NVARCHAR(100) = NULL
	,@PageNumber INT = 1
	,@PageSize INT = NULL
	,@OrderBy NVARCHAR(100) = 'CampaignId'
AS
BEGIN
	DECLARE @FirstRec INT
		,@LastRec INT
		,@TotalRecords INT
		,@UserName NVARCHAR(100)

	SELECT @UserName = FirstName + ' ' + LastName
	FROM dbo.UserProfile
	WHERE Id = @UserId

	SELECT (
			ROW_NUMBER() OVER (
				ORDER BY CASE 
						WHEN @OrderBy = 'NameAsc'
							THEN cp.CamapignName
						END
					,CASE 
						WHEN @OrderBy = 'NameDesc'
							THEN cp.CamapignName
						END DESC
					,CASE 
						WHEN @OrderBy = 'DateAsc'
							THEN cp.CreatedDate
						END
					,CASE 
						WHEN @OrderBy = 'DateDesc'
							THEN cp.CreatedDate
						END DESC
					,CASE 
						WHEN @OrderBy = 'CampaignId'
							OR @OrderBy IS NULL
							THEN cp.CampaignId
						END DESC
				)
			) AS Row
		,cp.CampaignId
		,cp.CamapignName
		,cp.CreatedById
		,cp.CreatedDate
		,cp.LastModById
		,cp.LastModDate
		,cp.STATUS
		,l.ListName
		,cp.UserId
		,@UserName AS EditedBy
	INTO #CampaignTemp
	FROM dbo.Campaign AS cp
	LEFT JOIN dbo.List AS l ON cp.ListId = l.ListId
	WHERE cp.UserId = @UserId
		AND (
			CamapignName LIKE '%' + ISNULL(@Search, CamapignName) + '%'
			OR cp.CreatedDate LIKE '%' + ISNULL(@Search, cp.CreatedDate) + '%'
			OR cp.LastModDate LIKE '%' + ISNULL(@Search, cp.LastModDate) + '%'
			OR ListName LIKE '%' + ISNULL(@Search, ListName) + '%'
			)
	ORDER BY cp.CampaignId

	SELECT @TotalRecords = Count(*)
	FROM #CampaignTemp

	IF (@PageSize IS NULL)
		SET @PageSize = @TotalRecords

	SELECT @FirstRec = (@PageNumber - 1) * @PageSize

	SELECT @LastRec = (@PageNumber * @PageSize) -- Create a temporary table 

	SELECT *
		,@TotalRecords AS TotalRecord
		,@PageNumber AS PageNumber
		,@PageSize AS PageSize
	FROM #CampaignTemp
	WHERE row > @FirstRec
		AND row <= @LastRec

	DROP TABLE #CampaignTemp
END
GO

