﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------
--usp_geAllPlans
-- =============================================


CREATE PROCEDURE [dbo].[usp_geAllPlans]
AS
SELECT [Plan].*
	,Package.id
	,Package.BillingAmount
	,Package.TotalMonth
FROM [Plan]
INNER JOIN Package ON [Plan].PlanId = Package.PlanId
ORDER BY [Plan].NoOfSubscribersMax
GO

