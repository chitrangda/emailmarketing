﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

--usp_getUserDashboardData '1e99575f-c92c-4842-9cd3-684e26182436'
--usp_getUserDashboardData '390a6a23-756b-4906-a990-69c0bf0e45e4'
--usp_getUserDashboardData '390a6a23-756b-4906-a990-69c0bf0e45e4'
-- =============================================

CREATE PROCEDURE [dbo].[usp_getUserDashboardData] @UserId NVARCHAR(128)
AS
BEGIN
	DECLARE @name NVARCHAR(100)
		,@TotalContact INT
		,@TotalOpen INT
		,@TotalClicked INT
		,@TotalUnsubscibers INT
		,@TotalEmails INT
		,@DayLeft INT
		,@PlanName VARCHAR(200)

	SET @name = (
			SELECT Isnull(firstname, '') + ' ' + Isnull(lastname, '')
			FROM userprofile
			WHERE id = @UserId
			)
	SET @TotalContact = (
			SELECT Count(*)
			FROM subscriber
			WHERE createdbyid = @UserId
				AND isdeleted = 0
			)
	SET @TotalOpen = (
			SELECT Count(OpenCount)
			FROM Campaign
			WHERE CreatedById = @UserId
			)
	SET @TotalClicked = (
			SELECT count(*)
			FROM Campaign
			WHERE CreatedById = @UserId
			)
	SET @TotalUnsubscibers = (
			SELECT count(st.SubscriberId)
			FROM SubscriberList slt
			INNER JOIN Subscriber st ON slt.SubscriberId = st.SubscriberId
			WHERE st.CreatedById = @UserId
				AND st.IsDeleted = 0
				AND slt.IsSubscribe = 0
			)
	SET @TotalEmails = (
			SELECT upt.totalRemainingCredits
			FROM UserPlan upt
			WHERE upt.UserId = @UserId
				AND IsActive = 1
			)
	SET @DayLeft = (
			SELECT (upt.TotalDays - DATEDIFF(day, upt.PackageUpdateDate, GETDATE()))
			FROM UserPlan upt
			WHERE upt.userid = @UserId
				AND IsActive = 1
			)
	SET @PlanName = (
			SELECT PlanName
			FROM [Package] pge
			INNER JOIN [Plan] p ON pge.PlanId = p.PlanId
			INNER JOIN UserPlan upt ON pge.Id = upt.PackageId
			WHERE upt.UserId = @UserId
				AND upt.IsActive = 1
			)

	IF EXISTS (
			SELECT 1
			FROM campaign
			WHERE userid = @UserId
			)
	BEGIN
		SELECT TOP 5 @name AS NAME
			,ISNULL(@TotalContact, 0) AS TotalContact
			,ISNULL(@TotalOpen, 0) AS TotalOpen
			,ISNULL(@TotalClicked, 0) AS TotalClicked
			,ISNULL(@TotalUnsubscibers, 0) AS TotalUnsubscribers
			,ISNULL(@TotalEmails, 0) AS TotalEmails
			,ISNULL(@DayLeft, 0) AS DaysLeft
			,ISNULL(@PlanName, NULL) AS PlanName
			,ct.CampaignId
			,ct.camapignname
			,ct.sentdate
			,ISNULL(ct.clickedcount, 0) AS clickedcount
			,ISNULL(ct.opencount, 0) AS opencount
			,ct.STATUS
			,scheduledcampaign.scheduleddatetime
			,ct.createddate
			,(
				SELECT Count(SL.SubscriberId)
				FROM dbo.subscriberlist AS SL
				INNER JOIN Subscriber st ON sl.SubscriberId = st.SubscriberId
				WHERE SL.listid = ct.listid
					AND st.IsDeleted = 0
					AND SL.IsSubscribe = 1
				) AS TotalSubscriber
		FROM campaign ct
		LEFT JOIN scheduledcampaign ON ct.campaignid = scheduledcampaign.campaignid
		WHERE ct.userid = @UserId
		ORDER BY ct.campaignid DESC
	END
	ELSE
	BEGIN
		SELECT @name AS NAME
			,ISNULL(@TotalContact, 0) AS TotalContact
			,ISNULL(@TotalOpen, 0) AS TotalOpen
			,ISNULL(@TotalClicked, 0) AS TotalClicked
			,ISNULL(@TotalUnsubscibers, 0) AS TotalUnsubscribers
			,ISNULL(@TotalEmails, 0) AS TotalEmails
			,ISNULL(@DayLeft, 0) AS DaysLeft
			,ISNULL(@PlanName, NULL) AS PlanName
			,- 1 AS CampaignId
			,NULL AS camapignname
			,NULL AS sentdate
			,0 AS clickedcount
			,0 AS opencount
			,NULL AS [status]
			,NULL AS scheduledcampaign
			,NULL AS scheduleddatetime
			,getdate() AS createddate
			,NULL AS TotalSubscriber
	END
END
GO

