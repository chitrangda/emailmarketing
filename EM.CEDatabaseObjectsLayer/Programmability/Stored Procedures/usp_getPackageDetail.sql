﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------
-- usp_getPackageDetail 2
-- =============================================


CREATE PROCEDURE [dbo].[usp_getPackageDetail] @PackId INT
AS
SELECT [Plan].*
	,[Package].Id
	,[Package].BillingAmount
	,[Package].TotalMonth
FROM [Plan]
INNER JOIN Package ON [Plan].PlanId = Package.PlanId
WHERE Package.id = @PackId
GO




