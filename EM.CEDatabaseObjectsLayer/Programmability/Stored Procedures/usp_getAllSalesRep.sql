﻿
-- =============================================
-- Author:		<Chitra,[usp_getAllSalesRep]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Sales Rep with Filters>

-- Modified By:		Sunil Singh
-- Modified date: <15 Jan 2019>
-- Description:	Changes for temp table replaced with table variable

-- Modified Date: 15 Jan 2019
-- Changes For : Replaced temp table with table variable

-----------Testing--------------------------

--[usp_getAllSalesRep]
--[usp_getAllSalesRep] 'pa'
--[usp_getAllSalesRep] 'p','t'

-- =============================================

CREATE PROCEDURE [dbo].[usp_getAllSalesRep] @FirstName NVARCHAR(200) = NULL
	,@LastName NVARCHAR(200) = NULL
	,@NoOfClients INT = NULL
	,@SpentTotal FLOAT = NULL
	,@ViewRepActivity BIT = NULL
	,@CanExport BIT = NULL
	,@ExportLead BIT = NULL
	,@Manager BIT = NULL
	,@IsActive BIT = NULL
	,@PageSize INT = NULL
	,@StartIndex INT = 0
AS
BEGIN
	SET FMTONLY OFF;
	SET NOCOUNT ON

	DECLARE @totalrecords INT;
	DECLARE @AllSalesRep TABLE (
		Id NVARCHAR(128)
		,Email NVARCHAR(256)
		,FirstName NVARCHAR(200)
		,LastName NVARCHAR(200)
		,NoOfClients INT
		,SpentTotal FLOAT
		,ViewRepActivity BIT
		,CanExport BIT
		,ExportLead BIT
		,Manager BIT
		,IsActive BIT
		,CreatedDate DATETIME
		,RecordNumber INT IDENTITY(1, 1) PRIMARY KEY
		);

	WITH CTE_SaleRepDetails
	AS (
		SELECT AspNetUsers.Email
			,SRP.Id
			,SRP.FirstName
			,SRP.LastName
			,SRP.ContactNo
			,SRP.NoOfClients
			,SRP.SpentTotal
			,SRP.ViewRepActivity
			,SRP.CanExport
			,SRP.ExportLead
			,SRP.Manager
			,SRP.IsActive
			,SRP.CreatedDate
			,SRP.CreatedBy
			,SRP.ModifedDate
			,SRP.ModifiedBy
		FROM AspNetUsers
		INNER JOIN SalesRepProfile SRP ON AspNetUsers.id = SRP.id
		INNER JOIN AspNetUserRoles ON AspNetUsers.id = AspNetUserRoles.UserId
		WHERE AspNetUserRoles.RoleId = '4dbdd2ed-394b-4093-a6b4-6a5bff1db830'
		)
	INSERT INTO @AllSalesRep
	SELECT id
		,Email
		,FirstName
		,LastName
		,NoOfClients
		,SpentTotal
		,ViewRepActivity
		,CanExport
		,ExportLead
		,Manager
		,IsActive
		,CreatedDate
	FROM CTE_SaleRepDetails cte
	ORDER BY cte.CreatedDate DESC

	SELECT @totalrecords = COUNT(Id)
	FROM @AllSalesRep

	IF (@PageSize IS NULL)
		SET @PageSize = @totalrecords

	SELECT id
		,Email
		,FirstName
		,LastName
		,NoOfClients
		,SpentTotal
		,ViewRepActivity
		,CanExport
		,ExportLead
		,Manager
		,IsActive
		,CreatedDate
		,RecordNumber
		,@totalrecords AS totalrecords
	FROM @AllSalesRep
	WHERE RecordNumber >= (@PageSize * @StartIndex) + 1
		AND RecordNumber <= (@PageSize * (@StartIndex + 1))
		AND FirstName LIKE '%' + ISNULL(@FirstName, FirstName) + '%'
		AND LastName LIKE '%' + ISNULL(@LastName, LastName) + '%'
		AND NoOfClients = ISNULL(@NoOfClients, NoOfClients)
		AND SpentTotal = ISNULL(@SpentTotal, SpentTotal)
		AND ViewRepActivity = ISNULL(@ViewRepActivity, ViewRepActivity)
		AND CanExport = ISNULL(@CanExport, CanExport)
		AND ExportLead = ISNULL(@ExportLead, ExportLead)
		AND Manager = ISNULL(@Manager, Manager)
		AND IsActive = ISNULL(@IsActive, IsActive)
END
GO



