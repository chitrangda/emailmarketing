﻿-- =============================================
-- Author:		<ALOK,usp_getUserList>
-- Create date: <12/18/2018 3:01 PM>
-- Description:	<usp_getUserList>

----------------------Testing--------------------------
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436' 
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436','P1' 
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436',NULL,1,10,NULL
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436',NULL,1,10,'NameDesc'
--dbo.usp_getUserList '1e99575f-c92c-4842-9cd3-684e26182436','t',1,NULL,'NameDesc'
-- =============================================

CREATE PROCEDURE [dbo].[usp_getUserList] @CreatedById NVARCHAR(100)
	,@ListName NVARCHAR(100) = NULL
	,@PageNumber INT = 1
	,@PageSize INT = NULL
	,@OrderBy NVARCHAR(100) = 'listid'
AS
BEGIN
	DECLARE @FirstRec INT
		,@LastRec INT
		,@TotalRecords INT

	SELECT (
			Row_number() OVER (
				ORDER BY CASE 
						WHEN @OrderBy = 'NameAsc'
							THEN l.ListName
						END
					,CASE 
						WHEN @OrderBy = 'NameDesc'
							THEN l.ListName
						END DESC
					,CASE 
						WHEN @OrderBy = 'DateAsc'
							THEN l.createddate
						END
					,CASE 
						WHEN @OrderBy = 'DateDesc'
							THEN l.createddate
						END DESC
					,CASE 
						WHEN @OrderBy = 'listid'
							OR @OrderBy IS NULL
							THEN listid
						END DESC
				)
			) AS ROW
		,L.listid
		,L.listname
		,L.createddate
		,'0' AS Opens
		,'0' AS Clicked
		,
		--@TotalRecords AS TotalRecord,  
		--@PageNumber AS CurrentPage,  
		--@PageSize AS PageSize,  
		(
			SELECT Count(SL.listid)
			FROM dbo.subscriberlist AS SL
			WHERE L.listid = SL.listid
			) AS TotalSubscriber
	INTO #temp
	FROM dbo.list AS L
	WHERE L.createdbyid = @CreatedById
		AND L.isdeleted = 0
		AND @ListName IS NULL
		OR listname LIKE '%' + @ListName + '%'

	--declare @totalrecords int 
	SELECT @TotalRecords = Count(*)
	FROM #temp

	IF (@PageSize IS NULL)
		SET @PageSize = @TotalRecords

	SELECT @FirstRec = (@PageNumber - 1) * @PageSize

	SELECT @LastRec = (@PageNumber * @PageSize) -- Create a temporary table 

	SELECT *
		,@TotalRecords AS TotalRecord
		,@PageNumber AS CurrentPage
		,@PageSize AS PageSize
	FROM #temp
	WHERE row > @FirstRec
		AND row <= @LastRec

	DROP TABLE #temp
END
GO

