﻿-- =============================================
-- Author:		<ALOK,usp_getTemplateList>
-- Create date: <1/15/2019 1:10 PM>
-- Description:	<TO GET THE INFO OF PLAN INFO OF USER>

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436','',1,10,'NameAsc'
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436','First'
--[usp_getTemplateList] '1e99575f-c92c-4842-9cd3-684e26182436',NULL,2,10
-- =============================================


CREATE PROCEDURE [dbo].[usp_getTemplateList] @UserId NVARCHAR(100)
	,@Search NVARCHAR(100) = NULL
	,@PageNumber INT = 1
	,@PageSize INT = NULL
	,@OrderBy NVARCHAR(100) = 'TemplateId'
AS
BEGIN
	DECLARE @FirstRec INT
		,@LastRec INT
		,@TotalRecords INT
		,@UserName NVARCHAR(100)

	SELECT @UserName = FirstName + ' ' + LastName
	FROM dbo.UserProfile
	WHERE Id = @UserId

	SELECT (
			ROW_NUMBER() OVER (
				ORDER BY CASE 
						WHEN @OrderBy = 'NameAsc'
							THEN tp.TemplateName
						END
					,CASE 
						WHEN @OrderBy = 'NameDesc'
							THEN TemplateName
						END DESC
					,CASE 
						WHEN @OrderBy = 'DateAsc'
							THEN tp.CreatedDate
						END
					,CASE 
						WHEN @OrderBy = 'DateDesc'
							THEN tp.CreatedDate
						END DESC
					,CASE 
						WHEN @OrderBy = 'TemplateId'
							OR @OrderBy IS NULL
							THEN tp.TemplateId
						END DESC
				)
			) AS Row
		,tp.TemplateId
		,tp.TemplateName
		,tp.UserId
		,tp.TemplateHtml
		,tp.CreatedDate
		,tp.CreatedById
		,tp.LastModById
		,tp.LastModDate
		,tp.Description
		,tp.TemplatePath
		,@UserName AS EditedBy
	INTO #TemplateTemp
	FROM dbo.Template AS tp
	WHERE tp.UserId = @UserId
		AND tp.IsDeleted = 0
		AND (
			TemplateName LIKE '%' + ISNULL(@Search, TemplateName) + '%'
			OR CreatedDate LIKE '%' + ISNULL(@Search, CreatedDate) + '%'
			OR LastModDate LIKE '%' + ISNULL(@Search, LastModDate) + '%'
			OR Description LIKE '%' + ISNULL(@Search, Description) + '%'
			OR TemplatePath LIKE '%' + ISNULL(@Search, TemplatePath) + '%'
			)
	ORDER BY tp.TemplateId

	SELECT @TotalRecords = Count(*)
	FROM #TemplateTemp

	IF (@PageSize IS NULL)
		SET @PageSize = @TotalRecords

	SELECT @FirstRec = (@PageNumber - 1) * @PageSize

	SELECT @LastRec = (@PageNumber * @PageSize) -- Create a temporary table 

	SELECT *
		,@TotalRecords AS TotalRecord
		,@PageNumber AS PageNumber
		,@PageSize AS PageSize
	FROM #TemplateTemp
	WHERE row > @FirstRec
		AND row <= @LastRec

	DROP TABLE #TemplateTemp
END
GO

