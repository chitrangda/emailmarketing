﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

CREATE PROCEDURE [dbo].[usp_getScheduledCampigan]
AS
BEGIN
	SELECT ct.CampaignId
		,ct.UserId
		,ct.ListId
		,ct.CampaignTypeID
		,ct.KeywordId
		,ct.Description
		,ct.AutomaticResponse
		,ct.ForwardedCellNumber
		,ct.ForwardedEmail
		,ct.CreatedById
		,ct.CreatedDate
		,ct.LastModById
		,ct.LastModDate
		,ct.HtmlContent
		,ct.Subject
		,ct.FromName
		,ct.FromEmail
		,ct.PreviewText
		,ct.TemplateId
		,ct.CamapignName
		,ct.STATUS
		,lt.FromEmail
		,lt.FromName
	FROM Campaign ct
	INNER JOIN ScheduledCampaign st ON ct.CampaignId = st.CampaignId
	INNER JOIN List lt ON ct.ListId = lt.ListId
	WHERE CONVERT(VARCHAR, getdate(), 105) = CONVERT(VARCHAR, st.ScheduledDateTime, 105)
		AND DATEPART(HOUR, GETDATE()) = DATEPART(HOUR, st.ScheduledDateTime)
		AND DATEPART(MINUTE, GETDATE()) = DATEPART(MINUTE, st.ScheduledDateTime)
END
GO




