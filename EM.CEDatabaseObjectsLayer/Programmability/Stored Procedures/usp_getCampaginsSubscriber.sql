﻿
-- =============================================
-- Author:		<Chitra,[usp_getCampaginsSubscriber]>
-- Create date: <14 Jan 2019>
-- Description:	<Get all Users with Filters>

-- Modified By: SuniL Kumar Singh
-- Modified Date: 14 Jan 2019
-- Changes For : Indent and added no count on statement
-- =============================================

CREATE PROCEDURE [dbo].[usp_getCampaginsSubscriber] @CampaignId INT
AS
SET NOCOUNT ON;

SELECT ct.CampaignId
	,ct.UserId
	,ct.ListId
	,ct.CampaignTypeID
	,ct.KeywordId
	,ct.Description
	,ct.AutomaticResponse
	,ct.ForwardedCellNumber
	,ct.ForwardedEmail
	,ct.CreatedById
	,ct.CreatedDate
	,ct.LastModById
	,ct.LastModDate
	,ct.HtmlContent
	,ct.Subject
	,ct.FromName
	,ct.FromEmail
	,ct.PreviewText
	,ct.TemplateId
	,ct.CamapignName
	,st.Email
	,st.FirstName
	,st.LastName
	,st.PhoneNumber
FROM Campaign ct
INNER JOIN SubscriberList slt ON ct.ListId = slt.ListId
LEFT JOIN Subscriber st ON slt.SubscriberId = st.SubscriberId
WHERE ct.CampaignId = @CampaignId
GO

