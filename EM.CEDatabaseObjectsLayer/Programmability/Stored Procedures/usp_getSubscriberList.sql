﻿-- =============================================
-- Author:		<ALOK,usp_getTemplateList>
-- Create date: <12/21/2018 11:47 AM>
-- Description:	<[usp_getSubscriberList]>

----------------Testing-------------------------
-- [usp_getSubscriberList] 59
-- [usp_getSubscriberList] 16,NULL,1,10
-- [usp_getSubscriberList] 16,'Trenton'
--[usp_getSubscriberList] 16,NULL,1,10
--[usp_getSubscriberList] 16,NULL,1,10,'NameDesc'
-- =============================================
CREATE PROCEDURE [dbo].[usp_getSubscriberList] @ListId INT
	,@Search NVARCHAR(100) = NULL
	,@PageNumber INT = 1
	,@PageSize INT = NULL
	,@OrderBy NVARCHAR(100) = 'SubscriberId'
AS
BEGIN
	DECLARE @FirstRec INT
		,@LastRec INT
		,@TotalRecords INT

	SELECT (
			ROW_NUMBER() OVER (
				ORDER BY CASE 
						WHEN @OrderBy = 'NameAsc'
							THEN Sub.FirstName
						END
					,CASE 
						WHEN @OrderBy = 'NameDesc'
							THEN Sub.FirstName
						END DESC
					,CASE 
						WHEN @OrderBy = 'DateAsc'
							THEN Sub.createddate
						END
					,CASE 
						WHEN @OrderBy = 'DateDesc'
							THEN Sub.createddate
						END DESC
					,CASE 
						WHEN @OrderBy = 'SubscriberId'
							OR @OrderBy IS NULL
							THEN Sub.SubscriberId
						END DESC
				)
			) AS Row
		,Sub.SubscriberId
		,SL.ListId
		,Sub.FirstName
		,Sub.LastName
		,Sub.DOB
		,Sub.Email
		,Sub.EmailPermission
		,Sub.Address1
		,Sub.Address2
		,Sub.City
		,Sub.STATE
		,Sub.Country
		,Sub.ZipCode
		,Sub.PhoneNumber
		,Sub.CreatedDate
		,Sub.LastModDate
		,SL.IsSubscribe
	INTO #temp
	FROM dbo.SubscriberList AS SL
	LEFT JOIN dbo.Subscriber AS Sub ON SL.SubscriberId = Sub.SubscriberId
	WHERE SL.ListId = @ListId
		AND Sub.IsDeleted = 0
		AND (
			SL.IsSubscribe = 1
			OR sl.IsSubscribe IS NULL
			)
		AND (
			FirstName LIKE '%' + ISNULL(@Search, FirstName) + '%'
			OR LastName LIKE '%' + ISNULL(@Search, LastName) + '%'
			OR Email LIKE '%' + ISNULL(@Search, Email) + '%'
			OR PhoneNumber LIKE '%' + ISNULL(@Search, PhoneNumber) + '%'
			OR City LIKE '%' + ISNULL(@Search, City) + '%'
			OR STATE LIKE '%' + ISNULL(@Search, STATE) + '%'
			OR ZipCode LIKE '%' + ISNULL(@Search, ZipCode) + '%'
			OR Country LIKE '%' + ISNULL(@Search, Country) + '%'
			OR Address1 LIKE '%' + ISNULL(@Search, Address1) + '%'
			)

	SELECT @TotalRecords = Count(*)
	FROM #temp

	IF (@PageSize IS NULL)
		SET @PageSize = @TotalRecords

	SELECT @FirstRec = (@PageNumber - 1) * @PageSize

	SELECT @LastRec = (@PageNumber * @PageSize)

	SELECT *
		,@TotalRecords AS TotalRecord
		,@PageNumber AS CurrentPage
		,@PageSize AS PageSize
	FROM #temp
	WHERE row > @FirstRec
		AND row <= @LastRec

	DROP TABLE #temp
END
GO


