﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SalesRepProfile] (
	[Id] [nvarchar](128) NOT NULL
	,[FirstName] [nvarchar](200) NULL
	,[LastName] [nvarchar](200) NULL
	,[ContactNo] [nvarchar](50) NULL
	,[NoOfClients] [int] NULL
	,[SpentTotal] [float] NULL
	,[ViewRepActivity] [bit] NULL
	,[CanExport] [bit] NULL
	,[ExportLead] [bit] NULL
	,[Manager] [bit] NULL
	,[IsActive] [bit] NULL
	,[CreatedDate] [datetime] NULL
	,[CreatedBy] [nvarchar](128) NULL
	,[ModifedDate] [datetime] NULL
	,[ModifiedBy] [nvarchar](128) NULL
	,CONSTRAINT [PK_SalesRepProfile] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SalesRepProfile] ADD CONSTRAINT [DF_SalesRepProfile_CanExport] DEFAULT((0))
FOR [CanExport]
GO

ALTER TABLE [dbo].[SalesRepProfile] ADD CONSTRAINT [DF_SalesRepProfile_ExportLead] DEFAULT((0))
FOR [ExportLead]
GO

ALTER TABLE [dbo].[SalesRepProfile] ADD CONSTRAINT [DF_SalesRepProfile_Manager] DEFAULT((0))
FOR [Manager]
GO

ALTER TABLE [dbo].[SalesRepProfile] ADD CONSTRAINT [DF_SalesRepProfile_IsActive] DEFAULT((0))
FOR [IsActive]
GO

ALTER TABLE [dbo].[SalesRepProfile] ADD CONSTRAINT [DF_SalesRepProfile_CreatedDate] DEFAULT(getdate())
FOR [CreatedDate]
GO

ALTER TABLE [dbo].[SalesRepProfile]
	WITH CHECK ADD CONSTRAINT [FK_SalesRepProfile_AspNetUsers] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[SalesRepProfile] CHECK CONSTRAINT [FK_SalesRepProfile_AspNetUsers]
GO

ALTER TABLE [dbo].[SalesRepProfile]
	WITH CHECK ADD CONSTRAINT [FK_SalesRepProfile_AspNetUsers1] FOREIGN KEY ([Id]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[SalesRepProfile] CHECK CONSTRAINT [FK_SalesRepProfile_AspNetUsers1]
GO

ALTER TABLE [dbo].[SalesRepProfile]
	WITH CHECK ADD CONSTRAINT [FK_SalesRepProfile_SalesRepProfile1] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[SalesRepProfile] CHECK CONSTRAINT [FK_SalesRepProfile_SalesRepProfile1]
GO

