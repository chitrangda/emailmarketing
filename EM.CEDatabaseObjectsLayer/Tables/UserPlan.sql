﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserPlan] (
	[UserPlanId] [int] IDENTITY(1, 1) NOT NULL
	,[UserId] [nvarchar](128) NOT NULL
	,[PackageId] [int] NOT NULL
	,[MonthlyCredit] [nvarchar](50) NULL
	,[totalSpendCredits] [nvarchar](50) NULL
	,[totalRemainingCredits] [nvarchar](50) NULL
	,[NoOfSubscribersMax] [int] NULL
	,[NoOfSubscribersMin] [int] NULL
	,[ContactManagement] [bit] NULL
	,[UnlimitedEmails] [bit] NULL
	,[Automation] [bit] NULL
	,[Segmenting] [bit] NULL
	,[Analytics] [bit] NULL
	,[EmailScheduling] [bit] NULL
	,[CustomizableTemplates] [bit] NULL
	,[EducationalResources] [bit] NULL
	,[LiveSupport] [bit] NULL
	,[PackageUpdateDate] [datetime] NULL
	,[NextPackageUpdateDate] [datetime] NULL
	,[BillingStatus] [int] NULL
	,[BillingAmount] [float] NULL
	,[TotalEmails] [int] NULL
	,[TotalDays] [int] NULL
	,[IsActive] [bit] NULL
	,CONSTRAINT [PK_UserPlan] PRIMARY KEY CLUSTERED ([UserPlanId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_PackageId] DEFAULT((0))
FOR [PackageId]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_MonthlyCredit] DEFAULT((0))
FOR [MonthlyCredit]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_totalSpendCredits] DEFAULT((0))
FOR [totalSpendCredits]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_totalRemainingCredits] DEFAULT((0))
FOR [totalRemainingCredits]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_NoOfSubscribersMax] DEFAULT((0))
FOR [NoOfSubscribersMax]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_NoOfSubscribersMin] DEFAULT((0))
FOR [NoOfSubscribersMin]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_CustomizableTemplates] DEFAULT((0))
FOR [CustomizableTemplates]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF__UserPlan__Billin__2A164134] DEFAULT((1))
FOR [BillingStatus]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_TotalEmails] DEFAULT((0))
FOR [TotalEmails]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_TotalDays] DEFAULT((0))
FOR [TotalDays]
GO

ALTER TABLE [dbo].[UserPlan] ADD CONSTRAINT [DF_UserPlan_IsActive] DEFAULT((1))
FOR [IsActive]
GO

ALTER TABLE [dbo].[UserPlan]
	WITH CHECK ADD CONSTRAINT [FK__UserPlan__UserId__2BFE89A6] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[UserPlan] CHECK CONSTRAINT [FK__UserPlan__UserId__2BFE89A6]
GO

ALTER TABLE [dbo].[UserPlan]
	WITH CHECK ADD CONSTRAINT [FK_UserPlan_Package] FOREIGN KEY ([PackageId]) REFERENCES [dbo].[Package]([Id])
GO

ALTER TABLE [dbo].[UserPlan] CHECK CONSTRAINT [FK_UserPlan_Package]
GO

