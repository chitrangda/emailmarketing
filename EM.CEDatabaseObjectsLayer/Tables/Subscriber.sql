﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Subscriber] (
	[SubscriberId] [int] IDENTITY(1, 1) NOT NULL
	,[Email] [varchar](50) NULL
	,[UserId] [int] NULL
	,[DOB] [datetime] NULL
	,[ZipCode] [varchar](50) NULL
	,[City] [varchar](50) NULL
	,[Country] [varchar](50) NULL
	,[IsActive] [bit] NOT NULL
	,[IsDeleted] [bit] NOT NULL
	,[IsOptIn] [bit] NOT NULL
	,[FirstName] [varchar](50) NULL
	,[LastName] [varchar](50) NULL
	,[OptOutdate] [datetime] NULL
	,[CreatedById] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NOT NULL
	,[LastModById] [nvarchar](128) NULL
	,[LastModDate] [datetime] NULL
	,[Address1] [nvarchar](max) NULL
	,[Address2] [nvarchar](max) NULL
	,[State] [nvarchar](50) NULL
	,[PhoneNumber] [varchar](200) NULL
	,[UpdateProfile] [bit] NULL
	,[EmailPermission] [bit] NOT NULL
	,CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([SubscriberId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Subscriber] ADD CONSTRAINT [DF__Contact__IsActiv__2C3393D0] DEFAULT((1))
FOR [IsActive]
GO

ALTER TABLE [dbo].[Subscriber] ADD CONSTRAINT [DF__Contact__IsDelet__2D27B809] DEFAULT((0))
FOR [IsDeleted]
GO

ALTER TABLE [dbo].[Subscriber] ADD CONSTRAINT [DF__Contact__Created__2E1BDC42] DEFAULT(getdate())
FOR [CreatedDate]
GO

ALTER TABLE [dbo].[Subscriber] ADD CONSTRAINT [DF__Contact__LastMod__2F10007B] DEFAULT(getdate())
FOR [LastModDate]
GO

ALTER TABLE [dbo].[Subscriber] ADD CONSTRAINT [DF_Subscriber_EmailPermission] DEFAULT((1))
FOR [EmailPermission]
GO

ALTER TABLE [dbo].[Subscriber]
	WITH CHECK ADD CONSTRAINT [FK__Contact__Created__3F466844] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK__Contact__Created__3F466844]
GO

ALTER TABLE [dbo].[Subscriber]
	WITH CHECK ADD CONSTRAINT [FK__Contact__LastMod__403A8C7D] FOREIGN KEY ([LastModById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Subscriber] CHECK CONSTRAINT [FK__Contact__LastMod__403A8C7D]
GO

