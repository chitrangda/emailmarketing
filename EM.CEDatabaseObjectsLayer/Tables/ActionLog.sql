﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	<Get All Action Log>

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

/****** Object:  Table [dbo].[ActionLog]    Script Date: 1/31/2019 6:04:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ActionLog] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[MethodName] [nvarchar](100) NULL
	,[ActionName] [nvarchar](100) NULL
	,[ControllerName] [nvarchar](100) NULL
	,[AreaName] [nvarchar](100) NULL
	,[CreatedAt] [datetime] NULL
	,[UserRefId] [nvarchar](128) NOT NULL
	,CONSTRAINT [PK_ActionLog] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ActionLog]
	WITH CHECK ADD CONSTRAINT [FK_ActionLog_AspNetUsers] FOREIGN KEY ([UserRefId]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[ActionLog] CHECK CONSTRAINT [FK_ActionLog_AspNetUsers]
GO

