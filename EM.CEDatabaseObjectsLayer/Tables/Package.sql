﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Package] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[PlanId] [int] NULL
	,[BillingAmount] [float] NULL
	,[TotalMonth] [int] NULL
	,CONSTRAINT [PK_PlanPricing] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Package]
	WITH CHECK ADD CONSTRAINT [FK_PlanPricing_Plan] FOREIGN KEY ([PlanId]) REFERENCES [dbo].[Plan]([PlanId])
GO

ALTER TABLE [dbo].[Package] CHECK CONSTRAINT [FK_PlanPricing_Plan]
GO


