﻿
-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UserProfile] (
	[Id] [nvarchar](128) NOT NULL
	,[FirstName] [nvarchar](200) NULL
	,[LastName] [nvarchar](200) NULL
	,[ContactEmail] [nvarchar](200) NULL
	,[OfficePhone] [nvarchar](200) NULL
	,[MobilePhone] [nvarchar](200) NULL
	,[Address1] [nvarchar](max) NULL
	,[Address2] [nvarchar](max) NULL
	,[City] [varchar](200) NULL
	,[State] [nvarchar](200) NULL
	,[Country] [nvarchar](200) NULL
	,[Zip] [nvarchar](50) NULL
	,[ProfileImage] [nvarchar](max) NULL
	,[CreditCardNo] [nvarchar](100) NULL
	,[ExpiryDate] [datetime] NULL
	,[RegisteredDate] [datetime] NULL
	,[IsActive] [bit] NULL
	,[CompanyName] [nvarchar](200) NULL
	,[AuthProfileId] [nvarchar](max) NULL
	,CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[UserProfile] ADD DEFAULT(getdate())
FOR [RegisteredDate]
GO

ALTER TABLE [dbo].[UserProfile] ADD DEFAULT((1))
FOR [IsActive]
GO

ALTER TABLE [dbo].[UserProfile]
	WITH CHECK ADD CONSTRAINT [FK_UserProfile_AspNetUsers] FOREIGN KEY ([Id]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[UserProfile] CHECK CONSTRAINT [FK_UserProfile_AspNetUsers]
GO

