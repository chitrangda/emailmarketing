﻿-- =============================================
-- Created By:	Sunil Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AnTransactionLog] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[UserId] [nvarchar](128) NULL
	,[AnTransactionId] [nvarchar](150) NULL
	,[TransactionType] [nvarchar](50) NULL
	,[TransactionDate] [datetime] NULL
	,[Amount] [nvarchar](50) NULL
	,[Message] [nvarchar](500) NULL
	,[CreatedBy] [nvarchar](150) NULL
	,[CreatedDate] [datetime] NULL
	,CONSTRAINT [PK_AnTransactionLog] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO


