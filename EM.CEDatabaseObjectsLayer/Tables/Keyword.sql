﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Keyword] (
	[KeywordId] [int] IDENTITY(1, 1) NOT NULL
	,[UserId] [nvarchar](128) NOT NULL
	,[Word] [nchar](15) NOT NULL
	,[StartDate] [datetime] NULL
	,[EndDate] [datetime] NULL
	,[IsDeleted] [bit] NOT NULL
	,[CreatedById] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NOT NULL
	,[LastModById] [nvarchar](128) NULL
	,[LastModDate] [datetime] NULL
	,PRIMARY KEY CLUSTERED ([KeywordId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Keyword] ADD DEFAULT((0))
FOR [IsDeleted]
GO

ALTER TABLE [dbo].[Keyword]
	WITH CHECK ADD FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Keyword]
	WITH CHECK ADD FOREIGN KEY ([LastModById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Keyword]
	WITH CHECK ADD FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

