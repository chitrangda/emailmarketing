﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[List] (
	[ListId] [int] IDENTITY(1, 1) NOT NULL
	,[ListName] [varchar](50) NOT NULL
	,[FromEmail] [varchar](50) NULL
	,[FromName] [varchar](50) NULL
	,[IsDeleted] [bit] NULL
	,[CreatedById] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NOT NULL
	,[LastModById] [nvarchar](128) NULL
	,[LastModDate] [datetime] NULL
	,[Remainder] [varchar](50) NULL
	,[Company] [varchar](50) NULL
	,[Address1] [varchar](max) NULL
	,[Address2] [varchar](max) NULL
	,[City] [varchar](50) NULL
	,[Zip] [nvarchar](50) NULL
	,[Country] [nvarchar](50) NULL
	,[Phone] [nvarchar](50) NULL
	,[Optin] [bit] NULL
	,[GDPR] [bit] NULL
	,[Subscriber/Unsubscriber] [bit] NULL
	,[Subscriber] [bit] NULL
	,[Unsubscriber] [bit] NULL
	,[IsActive] [bit] NULL
	,CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([ListId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[List] ADD CONSTRAINT [DF__Group__IsDeleted__300424B4] DEFAULT((0))
FOR [IsDeleted]
GO

ALTER TABLE [dbo].[List] ADD CONSTRAINT [DF__Group__CreatedDa__30F848ED] DEFAULT(getdate())
FOR [CreatedDate]
GO

ALTER TABLE [dbo].[List] ADD CONSTRAINT [DF__Group__LastModDa__31EC6D26] DEFAULT(getdate())
FOR [LastModDate]
GO

ALTER TABLE [dbo].[List]
	WITH CHECK ADD CONSTRAINT [FK_List_AspNetUsers] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[List] CHECK CONSTRAINT [FK_List_AspNetUsers]
GO

ALTER TABLE [dbo].[List]
	WITH CHECK ADD CONSTRAINT [FK_List_AspNetUsers1] FOREIGN KEY ([LastModById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[List] CHECK CONSTRAINT [FK_List_AspNetUsers1]
GO

