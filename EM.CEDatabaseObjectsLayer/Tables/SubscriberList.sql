﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SubscriberList] (
	[SubscriberListId] [int] IDENTITY(1, 1) NOT NULL
	,[ListId] [int] NOT NULL
	,[SubscriberId] [int] NOT NULL
	,[CreatedById] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NOT NULL
	,[LastModById] [nvarchar](128) NULL
	,[LastModDate] [datetime] NULL
	,[IsSubscribe] [bit] NULL
	,PRIMARY KEY CLUSTERED ([SubscriberListId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SubscriberList] ADD CONSTRAINT [DF_SubscriberList_IsSubscribe] DEFAULT((1))
FOR [IsSubscribe]
GO

ALTER TABLE [dbo].[SubscriberList]
	WITH CHECK ADD CONSTRAINT [FK__ContactGr__Conta__412EB0B6] FOREIGN KEY ([SubscriberId]) REFERENCES [dbo].[Subscriber]([SubscriberId])
GO

ALTER TABLE [dbo].[SubscriberList] CHECK CONSTRAINT [FK__ContactGr__Conta__412EB0B6]
GO

ALTER TABLE [dbo].[SubscriberList]
	WITH CHECK ADD FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[SubscriberList]
	WITH CHECK ADD FOREIGN KEY ([LastModById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

