﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailServiceLog] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[UserId] [nvarchar](128) NULL
	,[Description] [nvarchar](max) NULL
	,[LogDateTime] [datetime] NULL
	,[Createdby] [nvarchar](200) NULL
	,CONSTRAINT [PK_EmailServiceLog] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

