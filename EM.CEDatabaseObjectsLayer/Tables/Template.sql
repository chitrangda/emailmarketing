﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Template] (
	[TemplateId] [int] IDENTITY(1, 1) NOT NULL
	,[Description] [nvarchar](50) NULL
	,[TemplateName] [nvarchar](50) NULL
	,[TemplatePath] [nvarchar](250) NULL
	,[CreatedById] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NOT NULL
	,[LastModById] [nvarchar](128) NULL
	,[LastModDate] [datetime] NULL
	,[UserId] [nvarchar](128) NULL
	,[TemplateHtml] [nvarchar](max) NOT NULL
	,[TemplateJSON] [nvarchar](max) NULL
	,[IsDeleted] [bit] NULL
	,CONSTRAINT [PK__Template__F87ADD272C68C53B] PRIMARY KEY CLUSTERED ([TemplateId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

