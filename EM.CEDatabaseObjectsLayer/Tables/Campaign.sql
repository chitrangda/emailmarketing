﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Campaign] (
	[CampaignId] [int] IDENTITY(1, 1) NOT NULL
	,[UserId] [nvarchar](128) NOT NULL
	,[ListId] [int] NOT NULL
	,[CampaignTypeID] [int] NOT NULL
	,[KeywordId] [int] NOT NULL
	,[Description] [nvarchar](50) NULL
	,[AutomaticResponse] [nvarchar](500) NULL
	,[ForwardedCellNumber] [nvarchar](11) NULL
	,[ForwardedEmail] [nvarchar](60) NULL
	,[SentDate] [datetime] NULL
	,[CreatedById] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NOT NULL
	,[LastModById] [nvarchar](128) NULL
	,[LastModDate] [datetime] NULL
	,[HtmlContent] [nvarchar](max) NULL
	,[Subject] [nvarchar](500) NULL
	,[FromName] [nvarchar](100) NULL
	,[FromEmail] [nvarchar](100) NULL
	,[PreviewText] [nvarchar](max) NULL
	,[TemplateId] [int] NULL
	,[CamapignName] [nvarchar](100) NULL
	,[Status] [nvarchar](50) NULL
	,[ClickedCount] [int] NULL
	,[OpenCount] [int] NULL
	,[JSONContent] [nvarchar](max) NULL
	,[ContentImagePath] [nvarchar](100) NULL
	,CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED ([CampaignId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Campaign] ADD CONSTRAINT [DF_Campaign_ClickedCount] DEFAULT((0))
FOR [ClickedCount]
GO

ALTER TABLE [dbo].[Campaign] ADD CONSTRAINT [DF_Campaign_OpenCount] DEFAULT((0))
FOR [OpenCount]
GO

ALTER TABLE [dbo].[Campaign]
	WITH CHECK ADD CONSTRAINT [FK__Campaign__Create__38996AB5] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK__Campaign__Create__38996AB5]
GO

ALTER TABLE [dbo].[Campaign]
	WITH CHECK ADD CONSTRAINT [FK__Campaign__LastMo__3B75D760] FOREIGN KEY ([LastModById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK__Campaign__LastMo__3B75D760]
GO

ALTER TABLE [dbo].[Campaign]
	WITH CHECK ADD CONSTRAINT [FK__Campaign__UserId__3C69FB99] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK__Campaign__UserId__3C69FB99]
GO

ALTER TABLE [dbo].[Campaign]
	WITH CHECK ADD CONSTRAINT [FK_Campaign_List] FOREIGN KEY ([ListId]) REFERENCES [dbo].[List]([ListId])
GO

ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_List]
GO

ALTER TABLE [dbo].[Campaign]
	WITH CHECK ADD CONSTRAINT [FK_Campaign_Template] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[Template]([TemplateId])
GO

ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_Template]
GO

