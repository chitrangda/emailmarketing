﻿
-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ScheduledCampaign] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[CampaignId] [int] NULL
	,[ScheduledDateTime] [datetime] NULL
	,[Status] [int]
	,CONSTRAINT [PK_ScheduledCampaign] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ScheduledCampaign]
	WITH CHECK ADD CONSTRAINT [FK_ScheduledCampaign_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[Campaign]([CampaignId])
GO

ALTER TABLE [dbo].[ScheduledCampaign] CHECK CONSTRAINT [FK_ScheduledCampaign_Campaign]
GO

