﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ListFields] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[FieldId] [int] NULL
	,[ListId] [int] NULL
	,[IsActive] [bit] NOT NULL
	,CONSTRAINT [PK_ListFields] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ListFields] ADD CONSTRAINT [DF_ListFields_IsActive] DEFAULT((0))
FOR [IsActive]
GO

ALTER TABLE [dbo].[ListFields]
	WITH CHECK ADD CONSTRAINT [FK_ListFields_Fields] FOREIGN KEY ([FieldId]) REFERENCES [dbo].[Fields]([Id])
GO

ALTER TABLE [dbo].[ListFields] CHECK CONSTRAINT [FK_ListFields_Fields]
GO

ALTER TABLE [dbo].[ListFields]
	WITH CHECK ADD CONSTRAINT [FK_ListFields_List] FOREIGN KEY ([ListId]) REFERENCES [dbo].[List]([ListId])
GO

ALTER TABLE [dbo].[ListFields] CHECK CONSTRAINT [FK_ListFields_List]
GO

