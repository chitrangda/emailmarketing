﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Fields] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[FieldName] [nvarchar](100) NULL
	,[CreatedBy] [nvarchar](128) NULL
	,[CreatedDate] [datetime] NULL
	,[ModifiedBy] [nvarchar](128) NULL
	,[ModifiedDate] [datetime] NULL
	,[UserId] [nvarchar](128) NULL
	,[IsDeleted] [bit] NOT NULL
	,CONSTRAINT [PK_Fields] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Fields] ADD CONSTRAINT [DF_Fields_IsDeleted] DEFAULT((0))
FOR [IsDeleted]
GO

ALTER TABLE [dbo].[Fields]
	WITH CHECK ADD CONSTRAINT [FK_Fields_AspNetUsers] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_Fields_AspNetUsers]
GO

ALTER TABLE [dbo].[Fields]
	WITH CHECK ADD CONSTRAINT [FK_Fields_AspNetUsers1] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_Fields_AspNetUsers1]
GO

ALTER TABLE [dbo].[Fields]
	WITH CHECK ADD CONSTRAINT [FK_Fields_AspNetUsers2] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[Fields] CHECK CONSTRAINT [FK_Fields_AspNetUsers2]
GO

