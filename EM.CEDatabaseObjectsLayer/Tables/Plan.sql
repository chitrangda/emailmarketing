﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Plan] (
	[PlanId] [int] IDENTITY(1, 1) NOT NULL
	,[PlanName] [varchar](200) NULL
	,[NoOfSubscribersMin] [int] NULL
	,[NoOfSubscribersMax] [int] NULL
	,[ContactManagement] [bit] NULL
	,[UnlimitedEmails] [bit] NULL
	,[Automation] [bit] NULL
	,[Segmenting] [bit] NULL
	,[Analytics] [bit] NULL
	,[EmailScheduling] [bit] NULL
	,[CustomizableTemplates] [bit] NULL
	,[EducationalResources] [bit] NULL
	,[LiveSupport] [bit] NULL
	,[NoOfCredits] [nvarchar](50) NULL
	,[TotalEmails] [int] NULL
	,[TotalDay] [int] NULL
	,CONSTRAINT [PK__Plan__755C22B7D81AF5A9] PRIMARY KEY CLUSTERED ([PlanId] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

