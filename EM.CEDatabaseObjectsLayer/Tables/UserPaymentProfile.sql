﻿-- =============================================
-- Created By:	Chitra Singh
-- Create date: <31 Jan 2019>
-- Description:	< >

-- Modified By:	
-- Create date: <>
-- Description:	<>

---------------Testing------------------------------

-- =============================================

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserPaymentProfile] (
	[Id] [int] IDENTITY(1, 1) NOT NULL
	,[UserId] [nvarchar](128) NOT NULL
	,[AnPaymentProfileId] [nvarchar](150) NULL
	,[FirstName] [nvarchar](50) NULL
	,[LastName] [nvarchar](50) NULL
	,[Phone] [nvarchar](50) NULL
	,[Email] [nvarchar](150) NULL
	,[Address1] [nvarchar](150) NULL
	,[Address2] [nvarchar](150) NULL
	,[City] [nvarchar](50) NULL
	,[State] [nvarchar](50) NULL
	,[Country] [nvarchar](50) NULL
	,[Zip] [nvarchar](50) NULL
	,[Default] [bit] NOT NULL
	,[CreatedBy] [nvarchar](150) NULL
	,[CreatedDate] [datetime] NULL
	,[LastModBy] [nvarchar](150) NULL
	,[LastModDate] [datetime] NULL
	,[Active] [bit] NOT NULL
	,[LastFour] [nvarchar](10) NULL
	,[ExMonth] [int] NOT NULL
	,[ExYear] [int] NOT NULL
	,[Expired] AS (
		CASE 
			WHEN [ExYear] > datepart(year, getdate())
				THEN CONVERT([bit], (0))
			WHEN [ExYear] = datepart(year, getdate())
				AND [ExMonth] >= datepart(month, getdate())
				THEN CONVERT([bit], (0))
			ELSE CONVERT([bit], (1))
			END
		)
	,[CreatedById] [nvarchar](128) NULL
	,[LastModById] [nvarchar](128) NULL
	,[CardType] [nvarchar](150) NULL
	,[CreditCardNo] [nchar](100) NULL
	,[IsPrimary] [bit] NOT NULL
	,[IsDeleted] [bit] NULL
	,CONSTRAINT [PK_UserPaymentProfile] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (
		PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON
		) ON [PRIMARY]
	) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserPaymentProfile] ADD CONSTRAINT [DF_UserPaymentProfile_Active] DEFAULT((1))
FOR [Active]
GO

ALTER TABLE [dbo].[UserPaymentProfile] ADD CONSTRAINT [DF__UserPayme__ExMon__59FA5E80] DEFAULT(datepart(month, getdate()))
FOR [ExMonth]
GO

ALTER TABLE [dbo].[UserPaymentProfile] ADD CONSTRAINT [DF__UserPayme__ExYea__5AEE82B9] DEFAULT(datepart(year, getdate()))
FOR [ExYear]
GO

ALTER TABLE [dbo].[UserPaymentProfile] ADD CONSTRAINT [DF_UserPaymentProfile_IsPrimary] DEFAULT((0))
FOR [IsPrimary]
GO

ALTER TABLE [dbo].[UserPaymentProfile]
	WITH CHECK ADD CONSTRAINT [FK_UserPaymentProfile_AspNetUsers] FOREIGN KEY ([LastModById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[UserPaymentProfile] CHECK CONSTRAINT [FK_UserPaymentProfile_AspNetUsers]
GO

ALTER TABLE [dbo].[UserPaymentProfile]
	WITH CHECK ADD CONSTRAINT [FK_UserPaymentProfile_AspNetUsers1] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[UserPaymentProfile] CHECK CONSTRAINT [FK_UserPaymentProfile_AspNetUsers1]
GO

ALTER TABLE [dbo].[UserPaymentProfile]
	WITH CHECK ADD CONSTRAINT [FK_UserPaymentProfile_AspNetUsers2] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers]([Id])
GO

ALTER TABLE [dbo].[UserPaymentProfile] CHECK CONSTRAINT [FK_UserPaymentProfile_AspNetUsers2]
GO

