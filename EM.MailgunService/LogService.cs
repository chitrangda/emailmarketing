﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using EM.Factory;


namespace EM.MailgunService
{
    public class LogService
    {
        public static int CreateLog(string userid, string Description, string CreatedBY)
        {
            using (var db = new EmailMarketingDbEntities())
            {
                try
                {
                    EmailServiceLog obj = new EmailServiceLog();
                    obj.UserId = userid;
                    obj.Description = Description;
                    obj.LogDateTime = DateTime.Now;
                    obj.Createdby = CreatedBY;

                    db.EmailServiceLogs.Add(obj);
                    db.SaveChanges();
                    return 1;
                }
                catch (Exception)
                {
                    return -1;
                }

            }
        }

        public static void WriteToLog(string msg, string id)
        {
            try
            {
                //get current directory and then prepare to create a file name Logger.txt
                Assembly a = Assembly.GetEntryAssembly();
                string exeDir = Path.GetDirectoryName(a.Location);
                string Dir = exeDir + @"\Logs";
                if (!Directory.Exists(Dir))
                {
                    Directory.CreateDirectory(Dir);
                }
                string filename = "Logger_" + id + "_" + Guid.NewGuid() + ".txt";
                string path = Dir + "\\" + filename;
                if (!File.Exists(path))
                {
                    File.Create(path);
                }
                using (FileStream stream = File.Open(path, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                {
                    string logLine = System.String.Format("{0:G}:\n{1}.", System.DateTime.Now, msg);
                    //stream.Write(logLine); //write the string to file

                    Byte[] info = new UTF8Encoding(true).GetBytes(logLine);
                    // Add some information to the file.
                    stream.Write(info, 0, info.Length);
                    byte[] newline = Encoding.ASCII.GetBytes(Environment.NewLine);
                    stream.Write(newline, 0, newline.Length);
                    //stream.Flush();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}