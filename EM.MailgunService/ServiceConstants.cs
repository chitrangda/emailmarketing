﻿using System;
using System.Configuration;

namespace EM.MailgunService
{
    public class ServiceConstants
    {
        public static string MailgunKey = ConfigurationManager.AppSettings["MailgunKey"].ToString();

        public static int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["MaxSubscriberLimit"].ToString());

        public static string appUrl = ConfigurationManager.AppSettings["appUrl"].ToString();

        public static string FirstName = "%recipient.FirstName%";

        public static string LastName = "%recipient.LastName%";

        public static string PhoneNo = "%recipient.PhoneNo%";

        public static string Email = "%recipient.Email%";

        public static string[] whilelabelDomainsArr = { "gmail.com", "yahoo.com", "hotmail.com", "rediffmail.com", "outlook.com", "yopmail.com" };

        public static bool isTestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["isTestMode"].ToString());
    }
    public enum CampaginStatus
    {
        Draft,
        Sent,
        Schedule,
        Failed
    }
}
