﻿using System;
using System.IO;
using RestSharp;
using RestSharp.Authenticators;
using EM.Factory;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Web.Script.Serialization;
using System.Linq;

namespace EM.MailgunService
{
    class SendEmails
    {
        public async Task<RestRequest> CreateRequest(string UserId, string CampaignId, string fromEmail, string fromName, string subject, string htmlBody,string rplymail)
        {
            var host = fromEmail.Split('@')[1];
            if (Array.IndexOf(ServiceConstants.whilelabelDomainsArr, host) != -1)
            {
                host = "cheapestemail.com";
            }
            RestRequest request = new RestRequest();
            request.AddParameter("domain", host, ParameterType.UrlSegment);
            request.Resource = host+"/messages";
            request.AddParameter("from", fromName + " <" + fromEmail+">");// "fromName User <mailgun@YOUR_DOMAIN_NAME>");
            request.AddParameter("subject", subject);
            request.AddParameter("html", htmlBody);
            request.AddParameter("o:tag", UserId);
            request.AddParameter("o:tag", CampaignId);
            request.AddParameter("o:tag", "Production");
            request.AddParameter("o:dkim", true);
            request.AddParameter("o:tracking", true);
            request.AddParameter("o:tracking-clicks", true);
            request.AddParameter("o:tracking-opens", true);
            request.AddParameter("o:testmode", true);
            request.AddParameter("h:Reply-To", rplymail);
            request.Method = Method.POST;
            return await Task.FromResult<RestRequest>(request);

        }

        public async Task<string> SendCampaign(string UserId, int? CampaignId, string campaignSubject, string templateContentHtml, string fromEmail, string fromName, List<Recipient> subscribers, bool isFirstName, bool isLastName, bool isEmail, bool isPhoneNo, string rplymail)
        {

            //oRequest = AddRecipientVariables(oRequest, subscribers, templateContentHtml, CampaignId.ToString(), UserId, isFirstName, isLastName, isEmail, isPhoneNo);
            var host = fromEmail.Split('@')[1];
            if (Array.IndexOf(ServiceConstants.whilelabelDomainsArr, host) != -1)
            {
                host = "cheapestemail.com";
            }
            RestRequest request = new RestRequest();
            request.AddParameter("domain", host, ParameterType.UrlSegment);
            request.Resource = host + "/messages";
            request.AddParameter("from", fromName + " <" + fromEmail + ">");// "fromName User <mailgun@YOUR_DOMAIN_NAME>");
            request.AddParameter("subject", campaignSubject);
            request.AddParameter("html", templateContentHtml);
            request.AddParameter("o:tag", UserId);
            request.AddParameter("o:tag", CampaignId);
            request.AddParameter("o:tag", "Production");
            request.AddParameter("o:dkim", true);
            request.AddParameter("o:tracking", true);
            request.AddParameter("o:tracking-clicks", true);
            request.AddParameter("o:tracking-opens", true);
            if (ServiceConstants.isTestMode == true)
            {
                request.AddParameter("o:testmode", true);
            }
            request.AddParameter("h:Reply-To", rplymail);
            request.Method = Method.POST;
            request = AddRecipientVariables(request, subscribers, templateContentHtml, CampaignId.ToString(), UserId, isFirstName, isLastName, isEmail, isPhoneNo);
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", ServiceConstants.MailgunKey);
            var response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                LogService.WriteToLog(response.Content, "Campaign_" + CampaignId);

            }
            string emails = string.Join(",", subscribers.Select(r => r.Email).ToArray());
            await UpdateCampaignEmailStatus(CampaignId, emails, response.StatusCode, response.Content);

            return response.StatusCode.ToString();



        }

        public RestRequest AddRecipientVariables(RestRequest oRequest, List<Recipient> subscribers, string content, string CampaignId, string UserId, bool isFirstName = false, bool isLastName = false, bool isEmail = false, bool isPhoneNo = false)
        {
            Dictionary<string, Recipient> d = new Dictionary<string, Recipient>();
            foreach (var contact in subscribers)
            {
                try
                {
                    var name = contact.FirstName + " " + contact.LastName;
                    string url = string.Format(ServiceConstants.appUrl + "Home/Unsubscribe?id={0}&e={1}&uid={2}", CampaignId, contact.Email, UserId);
                    Recipient oRecipient = new Recipient();
                    //if (isFirstName)
                    //{
                        oRecipient.FirstName = contact.FirstName;
                    //}

                    //if (isLastName)
                    //{
                        oRecipient.LastName = contact.LastName;

                    //}
                    //if (isEmail)
                    //{
                        oRecipient.Email = contact.Email;

                    //}
                    //if (isPhoneNo)
                    //{
                        oRecipient.PhoneNo = contact.PhoneNo;

                    //}
                    oRecipient.unsubUrl = url;
                    oRequest.AddParameter("to", contact.Email);
                    d[contact.Email] = oRecipient;

                }
                catch (Exception ex)
                {
                    LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "Campaign_" + CampaignId);
                }
            }
            oRequest.AddParameter("recipient-variables", new JavaScriptSerializer().Serialize(d));
            return oRequest;
        }

        public async Task UpdateCampaignEmailStatus(int? CampaignId, string subscribers, System.Net.HttpStatusCode code, string description)
        {
            try
            {
                using (var context = new EmailMarketingDbEntities())
                {
                    //add email sent status

                    CampaignEmailStatu campaign = new CampaignEmailStatu();
                    campaign.CampaignId = CampaignId;
                    campaign.Email = subscribers;
                    campaign.SentDateTime = DateTime.Now;
                    campaign.SendGridResponse = code.ToString();
                    campaign.Description = description;
                    if (code == System.Net.HttpStatusCode.OK)
                    {
                        campaign.Status = CampaginStatus.Sent.ToString();
                    }
                    else
                    {
                        campaign.Status = CampaginStatus.Failed.ToString();
                    }
                    context.CampaignEmailStatus.Add(campaign);
                    await context.SaveChangesAsync();
                    //LogService.WriteToLog("Function UpdateCampaignEmailStatus Start", "Campaign_" + CampaignId);
                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");

            }
        }
    }
}
