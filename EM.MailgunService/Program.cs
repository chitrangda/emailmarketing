﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace EM.MailgunService
{
    class Program
    {
        static void Main(string[] args)
        {
            ScheduledCampaignDetails oScheduledCampaign = new ScheduledCampaignDetails();
            oScheduledCampaign.InitSend().Wait();

            //TestScript().Wait(); 

            //TestPstbinScript().Wait(); 

            //TestScript1().Wait();
            Console.ReadLine();
        }

        static async Task TestScript()
        {
            var apiKey = "b37915d49cc569675e291efd25193d2f-2dfb0afe-04c3fe7f"; // asish patel
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "cheapestemail.com", ParameterType.UrlSegment);
            request.Resource = "cheapestemail.com/messages";
            request.AddParameter("from", "Carley Rexrode <jcrexrode@cheapestemail.com>");
            //request.AddParameter("to", "chitra.06singh@gmail.com");
            request.AddParameter("to", "aashi.kt@gmail.com");
            //request.AddParameter("to", "avpatel31@gmail.com");
            //request.AddParameter("subject", "Hello %recipient.FirstName% Testing some Mailgun awesomness 04-Nov-2019");
            //request.AddParameter("text", "%recipient.FirstName% %recipient.LastName% Testing some Mailgun awesomness! 31-Oct-2019");
            request.AddParameter("subject", "Hello Testing some Mailgun awesomness");
            request.AddParameter("text", "Testing some Mailgun awesomness!");
            request.AddParameter("o:tracking", true);
            request.AddParameter("o:tracking-clicks", true);
            request.AddParameter("o:tracking-opens", true);
            request.AddParameter("o:testmode", true);

            //request.AddParameter("recipient-variables", "{\"chitra.06singh@gmail.com\": {\"FirstName\":\"Chitra\", \"LastName\":\"Singh\"}, \"avpatel31@gmail.com\": {\"FirstName\":\"Ashish\", \"LastName\": \"Patel\"}}");
            //request.AddParameter("o:tag", "1af4ef6b-a6c0-4a26-92da-f66daf606bda");
            //request.AddParameter("o:tag", "267");
            request.Method = Method.POST;
            //var response = client.Execute(request);
            var response = await client.ExecutePostTaskAsync(request);
            Console.WriteLine(response.Content);
            Console.WriteLine(response.StatusCode);
        }

        static async Task TestScript1()
        {
            var apiKey = "b37915d49cc569675e291efd25193d2f-2dfb0afe-04c3fe7f"; // asish patel
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "cheapestemail.com", ParameterType.UrlSegment);
            request.Resource = "cheapestemail.com/messages";
            request.AddParameter("from", "Carley Rexrode <jcrexrode@cheapestemail.com>");
            //request.AddParameter("from", "Chitra Singh <chitra.06singh@gmail.com>");
            // request.AddParameter("to", "aashi.kt@gmail.com");
            request.AddParameter("to", "chitra.06singh@gmail.com");
            request.AddParameter("subject", "Hello Testing some Mailgun awesomness");
            //request.AddParameter("text", "Testing some Mailgun awesomness!");
            request.AddParameter("html", "<p><a href='https://www.cheapestemail.com/' target='_blank' rel='noopener'> CheapestEmail </a></p>");
            request.AddParameter("o:tracking", true);
            request.AddParameter("o:tracking-clicks", "htmlonly");
            request.AddParameter("o:tracking-opens", true);
            request.Method = Method.POST;
            //var response = client.Execute(request);
            var response = await client.ExecutePostTaskAsync(request);
            Console.WriteLine(response.Content);
            Console.WriteLine(response.StatusCode);
        }


        static async Task TestPstbinScript()
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3/");
            client.Authenticator =
                new HttpBasicAuthenticator("api", "b37915d49cc569675e291efd25193d2f-2dfb0afe-04c3fe7f");
            RestRequest request = new RestRequest();
            request.Resource = "domains/cheapestemail.com/webhooks";
            request.AddParameter("id", "click");
            request.AddParameter("url", "http://bin.mailgun.net/1f37ed3e");
            request.Method = Method.POST;
            var response = await client.ExecutePostTaskAsync(request);
            Console.WriteLine(response.Content);
            Console.WriteLine(response.StatusCode);
        }


        void getStatus(IRestResponse response)
        {
            Console.WriteLine(response.Content);
            Console.WriteLine(response.StatusCode);

        }
    }
}
