﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EM.Factory;
using RestSharp;

namespace EM.MailgunService
{
    class ScheduledCampaignDetails
    {
        EmailMarketingDbEntities db = new EmailMarketingDbEntities();
        SendEmails emailService = new SendEmails();

        public async Task InitSend()
        {
            await GetScheduledCampaign();
            //await UpdateSentStatus(1,1);
        }
        public async Task GetScheduledCampaign()
        {
            try
            {
                var lstCampigan = db.usp_getScheduledCampigan().ToList();

                if (lstCampigan != null && lstCampigan.Count() > 0)
                {
                    foreach (var campaign in lstCampigan)
                    {
                        try
                        {
                            await GetCampaignDetails(campaign.CampaignId, campaign.ScheduledCampaignId);
                        }
                        catch (Exception ex)
                        {
                            LogService.WriteToLog(ex.Message, "Error");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message, "Error");
            }
        }

        public async Task GetCampaignDetails(int? campaignId, int ScheduledCampaignId)
        {
            //LogService.WriteToLog("Data is fetching from usp_getCampaignDetail", "Campaign_" + campaignId);
            var campaignInfo = db.usp_getCampaignDetail(campaignId).FirstOrDefault();
            if (campaignInfo != null)
            {
                bool isFirstName = false, isLastName = false, isEmail = false, isPhoneNo = false;
                //if (campaignInfo.HtmlContent.IndexOf(ServiceConstants.FirstName) != -1)
                //{
                //    isFirstName = true;
                //}
                //if (campaignInfo.HtmlContent.IndexOf(ServiceConstants.LastName) != -1)
                //{
                //    isLastName = true;
                //}
                //if (campaignInfo.HtmlContent.IndexOf(ServiceConstants.Email) != -1)
                //{
                //    isEmail = true;
                //}
                //if (campaignInfo.HtmlContent.IndexOf(ServiceConstants.PhoneNo) != -1)
                //{
                //    isPhoneNo = true;
                //}
                //var oRequest = await emailService.CreateRequest(campaignInfo.UserId, campaignInfo.CampaignId.ToString(), campaignInfo.FromEmail, campaignInfo.FromName, campaignInfo.Subject, campaignInfo.HtmlContent,campaignInfo.OriginalEmail);

                if (campaignInfo.TotalSubscriber > 1000)
                {
                    await SendSubscriberBunch(campaignInfo.UserId, campaignInfo.CampaignId, campaignInfo.Subject, campaignInfo.HtmlContent, campaignInfo.FromEmail, campaignInfo.FromName, ScheduledCampaignId, isFirstName, isLastName, isEmail, isPhoneNo, campaignInfo.OriginalEmail);
                }
                else
                {
                    await SendAllSubscriber(campaignInfo.UserId, campaignInfo.CampaignId, campaignInfo.Subject, campaignInfo.HtmlContent, campaignInfo.FromEmail, campaignInfo.FromName, ScheduledCampaignId, isFirstName, isLastName, isEmail, isPhoneNo, campaignInfo.OriginalEmail);

                }

            }
        }

        public async Task SendAllSubscriber(string UserId, int? campaignId, string campaignsubject, string campaignEmailContent, string fromEmail, string fromName, int ScheduledCampaignId, bool isFirstName, bool isLastName, bool isEmail, bool isPhoneNo, string rplymail)
        {
            try
            {
                int page = 1;
                bool flag = false;


                var campaignInfo = db.usp_getCampaginsSubscriberForService_Updated(campaignId, page, 1000).ToList();
                if (campaignInfo != null && campaignInfo.Count() > 0)
                {
                    List<Recipient> subscribers = await ParseContacts(campaignInfo);

                    var response = await emailService.SendCampaign(UserId, campaignId, campaignsubject, campaignEmailContent, fromEmail, fromName, subscribers, isFirstName, isLastName, isEmail, isPhoneNo, rplymail);
                    if (response == "OK")
                    {
                        flag = true;

                    }

                }


                if (flag == true)
                {
                    await UpdateSentStatus(campaignId, ScheduledCampaignId);

                }
                else
                {
                    await UpdateFailureStatus(campaignId, ScheduledCampaignId);

                }

            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "Campaign_" + campaignId);
               
            }
        }

        public async Task SendSubscriberBunch(string UserId, int? campaignId, string campaignsubject, string campaignEmailContent, string fromEmail, string fromName, int ScheduledCampaignId, bool isFirstName, bool isLastName, bool isEmail, bool isPhoneNo, string rplymail)
        {
            try
            {
                int page = 1;
                string response;
                var tasks = new List<Task<string>>();
                while (true)
                {
                    var subscriberList = db.usp_getCampaginsSubscriberForService_Updated(campaignId, page, ServiceConstants.PageSize).ToList();
                    if (subscriberList != null && subscriberList.Count() > 0)
                    {

                        int i = 0;
                        var total = subscriberList.Count;
                        while (total > 0)
                        {
                            var subscribers_set = subscriberList.Skip(i * 1000).Take(1000).ToList();
                            List<Recipient> subscribers = await ParseContacts(subscribers_set);
                            tasks.Add(await Task.Factory.StartNew(async () => response = await emailService.SendCampaign(UserId, campaignId, campaignsubject, campaignEmailContent, fromEmail, fromName, subscribers, isFirstName, isLastName, isEmail, isPhoneNo, rplymail)));
                            total -= subscribers_set.Count;
                            i++;
                        }
                        page++;

                    }
                    else
                    {
                        break;
                    }
                }
                try
                {
                    // Wait for all the tasks to finish.
                    Task.WaitAll(tasks.ToArray());
                    await UpdateCampaignStatus(campaignId, ScheduledCampaignId);

                }
                catch (AggregateException e)
                {
                    //Console.WriteLine("\nThe following exceptions have been thrown by WaitAll(): (THIS WAS EXPECTED)");
                    for (int j = 0; j < e.InnerExceptions.Count; j++)
                    {
                        LogService.WriteToLog(e.InnerExceptions[j].ToString(), "Campaign_" + campaignId);

                        //Console.WriteLine("\n-------------------------------------------------\n{0}", e.InnerExceptions[j].ToString());
                    }
                    await UpdateFailureStatus(campaignId, ScheduledCampaignId);

                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "Campaign_" + campaignId);
                await UpdateFailureStatus(campaignId, ScheduledCampaignId);
            }
        }


        public async Task<List<Recipient>> ParseContacts(List<usp_getCampaginsSubscriberForService_Updated_Result> campaginSubscribers)
        {
            List<Recipient> subscribers = new List<Recipient>();
            if (campaginSubscribers != null)
            {
                campaginSubscribers.ForEach(r => subscribers.Add(new Recipient() { Email = r.Email, FirstName = r.FirstName, LastName = r.LastName, PhoneNo = r.PhoneNumber }));

            }
            return await Task.FromResult<List<Recipient>>(subscribers);
        }

        public async Task UpdateCampaignStatus(int? campaginId, int? ScheduledCampaignId)
        {
            try
            {
                using (var context = new EmailMarketingDbEntities())
                {
                    var campaignStatus = context.CampaignEmailStatus.Where(x => x.CampaignId == campaginId).ToList();
                    if (campaignStatus.Select(n => n.Status).Contains("Failed"))
                    {
                        await UpdateFailureStatus(campaginId, ScheduledCampaignId);
                    }

                    else
                    {
                        await UpdateSentStatus(campaginId, ScheduledCampaignId);
                    }
                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");

            }
        }

        public async Task UpdateSentStatus(int? campaginId, int? ScheduledCampaignId)
        {
            try
            {
                using (var context = new EmailMarketingDbEntities())
                {
                    var campagin = await context.Campaigns.FindAsync(campaginId);
                    if (campagin != null)
                    {
                        campagin.Status = CampaginStatus.Sent.ToString();
                        campagin.SentDate = DateTime.Now;
                        campagin.ProcessedCount = campagin.SubscriberCount;
                        context.SaveChanges();
                    }

                    var scheduleCampaign = db.ScheduledCampaigns.Where(x => x.Id == ScheduledCampaignId).FirstOrDefault();
                    if (scheduleCampaign != null)
                    {
                        scheduleCampaign.Status = CampaignStatus.Sent;
                        db.SaveChanges();
                    }


                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");

            }
        }

        public async Task UpdateFailureStatus(int? campaginId, int? ScheduledCampaignId)
        {
            try
            {
                using (var context = new EmailMarketingDbEntities())
                {
                    var campagin = await context.Campaigns.FindAsync(campaginId);
                    if (campagin != null)
                    {
                        campagin.Status = CampaginStatus.Failed.ToString();
                        campagin.SentDate = null;
                        context.SaveChanges();
                    }

                    var scheduleCampaign = db.ScheduledCampaigns.Where(x => x.Id == ScheduledCampaignId).FirstOrDefault();
                    if (scheduleCampaign != null)
                    {
                        scheduleCampaign.Status = CampaignStatus.Failed;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message + "\n" + ex.StackTrace, "");

            }
        }

    }
}
