﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.MailgunService
{
    public class Recipient
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }

        public string unsubUrl { get; set; }

    }

    public class RecipientEmail
    {
        public Recipient recipient { get; set; }

    }
}
