﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;
using System.Data;
using System.Configuration;

namespace EM.EventRecoveryService
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] filePaths = Directory.GetFiles(ConfigurationManager.AppSettings["path"]);
            foreach(string jsonFile in filePaths)
            {
                try
                {
                    LoadJson(jsonFile);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);

                }

            }
        }

        public static void LoadJson(string jsonFile)
        {
            try
            {
                List<EventView> eventsList = null;
                using (StreamReader r = new StreamReader(jsonFile))
                {
                    string json = r.ReadToEnd();
                    eventsList = JsonConvert.DeserializeObject<List<EventView>>(json);
                }
                saveEventData(eventsList);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }

        public static void saveEventData(List<EventView> eventsList)
        {
            EmailMarketingDbEntities db = new EmailMarketingDbEntities();
            if (eventsList != null && eventsList.Count() > 0)
            {
                try
                {
                    foreach (EventView oEventview in eventsList)
                    {
                        try
                        {
                            if (oEventview.category != null)
                            {
                                string userid = oEventview.category[0];
                                int campaignId = Convert.ToInt32(oEventview.category[1]);
                                var oCampaign = db.Campaigns.Where(r => r.CampaignId == campaignId && r.UserId == userid).SingleOrDefault();
                                if (oCampaign != null)
                                {
                                    var oEmailevent = db.EmailEvents.Where(r => r.CampaignId == campaignId && r.email == oEventview.email && r.@event == oEventview.@event).FirstOrDefault();
                                    if (oEmailevent == null)
                                    {

                                        EmailEvent oEvent = new EmailEvent();
                                        oEvent.CampaignId = Convert.ToInt32(campaignId);
                                        oEvent.email = oEventview.email;
                                        oEvent.Event_TimeStamp = oEventview.timestamp;
                                        oEvent.@event = oEventview.@event;
                                        oEvent.reason = oEventview.reason;
                                        oEvent.url = oEventview.url;
                                        oEvent.response = oEventview.response;
                                        oEvent.EventDate = DateTime.Now.AddSeconds(-30);
                                        db.EmailEvents.Add(oEvent);
                                        db.SaveChanges();

                                        if (oEvent.@event == "bounce" || oEvent.@event == "dropped")
                                        {
                                            var subscriberlist = db.Subscribers.Where(r => r.Email == oEventview.email).ToList();
                                            foreach (var subscriber in subscriberlist)
                                            {
                                                subscriber.IsSuppressed = true;
                                                db.SaveChanges();
                                            }

                                        }
                                        else if (oEvent.@event == "delivered")
                                        {
                                            var oBounceEventList = db.EmailEvents.Where(r => r.CampaignId == campaignId && r.email == oEventview.email && (r.@event == "bounce" || r.@event == "dropped" || r.@event == "deferred")).ToList();
                                            if (oBounceEventList != null && oBounceEventList.Count() > 0)
                                            {
                                                foreach (var oBounceEvent in oBounceEventList)
                                                {
                                                    db.EmailEvents.Remove(oBounceEvent);
                                                    db.SaveChanges();
                                                }
                                            }
                                            var subscriberlist = db.Subscribers.Where(r => r.Email == oEventview.email && r.IsSuppressed == true).FirstOrDefault();
                                            if (subscriberlist != null)
                                            {
                                                subscriberlist.IsSuppressed = false;
                                                db.SaveChanges();
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);

                }

            }
        }
    }
}
