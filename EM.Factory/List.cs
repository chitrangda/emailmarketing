//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    using System.Collections.Generic;
    
    public partial class List
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public List()
        {
            this.ListFields = new HashSet<ListField>();
        }
    
        public int ListId { get; set; }
        public string ListName { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string CreatedById { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModById { get; set; }
        public Nullable<System.DateTime> LastModDate { get; set; }
        public string Remainder { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public Nullable<bool> Optin { get; set; }
        public Nullable<bool> GDPR { get; set; }
        public Nullable<bool> Subscriber_Unsubscriber { get; set; }
        public Nullable<bool> Subscriber { get; set; }
        public Nullable<bool> Unsubscriber { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }
        public string State { get; set; }
        public string ListNameDomainWise { get; set; }
        public string Eventbrite_EventId { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ListField> ListFields { get; set; }
    }
}
