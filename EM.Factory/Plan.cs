//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    using System.Collections.Generic;
    
    public partial class Plan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Plan()
        {
            this.Packages = new HashSet<Package>();
        }
    
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string Descrption { get; set; }
        public Nullable<bool> ContactManagement { get; set; }
        public Nullable<bool> UnlimitedEmails { get; set; }
        public Nullable<bool> CustomizableTemplates { get; set; }
        public Nullable<bool> EducationalResources { get; set; }
        public Nullable<bool> LiveSupport { get; set; }
        public Nullable<bool> EmailScheduling { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Package> Packages { get; set; }
    }
}
