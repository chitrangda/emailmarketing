//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    using System.Collections.Generic;
    
    public partial class CampaignList
    {
        public int CampaignTypeId { get; set; }
        public string CampaignName { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedById { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
