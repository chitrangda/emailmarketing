//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    
    public partial class usp_getUserDashboardData_Result
    {
        public Nullable<int> TotalContact { get; set; }
        public string NAME { get; set; }
        public string camapignname { get; set; }
        public Nullable<System.DateTime> sentdate { get; set; }
        public string status { get; set; }
        public Nullable<System.DateTime> scheduleddatetime { get; set; }
        public System.DateTime createddate { get; set; }
        public Nullable<int> TotalOpen { get; set; }
        public Nullable<int> TotalClicked { get; set; }
        public Nullable<int> TotalUnsubscribers { get; set; }
        public Nullable<int> TotalEmails { get; set; }
        public int DaysLeft { get; set; }
        public string PlanName { get; set; }
        public int CampaignId { get; set; }
        public string Subject { get; set; }
        public string FromEmail { get; set; }
        public int Subscribers { get; set; }
        public int Delivered { get; set; }
        public int Opens { get; set; }
        public int Clicks { get; set; }
        public int Bounce { get; set; }
        public int Processed { get; set; }
        public Nullable<int> SubscriberMax { get; set; }
    }
}
