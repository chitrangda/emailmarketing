﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Factory.ViewModels
{
    public class AdminTodayViewModel
    {
        public Nullable<int> TotalClient { get; set; }
        public string ClientBilling { get; set; }
        public string BillDue { get; set; }
        public string BillDeclined { get; set; }
        public string PacakagePurchase { get; set; }
        public string PackageDeclined { get; set; }
        public Nullable<int> AllClients { get; set; }
        public Nullable<int> ClientType { get; set; }

        public Nullable<int> CampaignType { get; set; }

        public SelectList ClientTypeList { get; set; }

        public SelectList CampaignList { get; set; }

        public string Id { get; set; }

        public SelectList MemberTypeList { get; set; }
        public Nullable<int> MemberType { get; set; }


    }
}