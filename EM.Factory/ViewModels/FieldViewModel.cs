﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class FieldsViewModel
    {
        public int Id { get; set; }
        public string FieldName { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string UserId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActivated { get; set; }
    }
}