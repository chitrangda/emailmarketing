﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class TemplateViewModel
    {
        public int TemplateId { get; set; }
        public string Description { get; set; }
        public string TemplateName { get; set; }
        public string TemplatePath { get; set; }
        public string CreatedById { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string LastModById { get; set; }
        public Nullable<System.DateTime> LastModDate { get; set; }
        public string UserId { get; set; }
        public string TemplateHtml { get; set; }
        public string TemplateJSON { get; set; }
        public string Base64Content { get; set; }

        public Nullable<int> TotalRecord { get; set; }
        public Nullable<int> PageNumber { get; set; }
        public Nullable<int> PageSize { get; set; }
        public string EditedBy { get; set; }

        public string BeeTemplateJson { get; set; }
        public string BeeTemplateName { get; set; }
        public string BeeTemplatePath { get; set; }

        public int BeeTemplateId { get; set; }
        public string CampaignName { get; set; }

    }
}