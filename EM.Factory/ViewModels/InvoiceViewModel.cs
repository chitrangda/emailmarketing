﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Factory.ViewModels
{
    public class InvoiceViewModel
    {
        public int InvoiceID { get; set; }
        public string UserId { get; set; }
        public string OrderID { get; set; }
        public string AmountCharged { get; set; }
        public string InvoiceType { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string VerificationNo { get; set; }
        public string NameOnCard { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedById { get; set; }
        public System.DateTime LastModByDate { get; set; }
        public string LastModById { get; set; }
        public string OfferCode { get; set; }
        public string TransactionStatus { get; set; }
        public string TransactionError { get; set; }
        public Nullable<int> PaymentTypeId { get; set; }
      
        public Nullable<int> NoofCredits { get; set; }
        public string BillingAmount { get; set; }

        public double? PackageBillingAmount { get; set; }

        public int? SelectedPackakgeId { get; set; }

        public int ExpiresYear { get; set; }

        public int ExpiresMonth { get; set; }

        public SelectList PaymnetTypeList { get; set; }

        public IEnumerable<SelectListItem> YearList { get; set; }

        public IEnumerable<SelectListItem> MonthList { get; set; }

        public string Message { get; set; }

        public int UserPaymentProfileId { get; set; }

        public string AnPaymentProfileId { get; set; }

        public string Email { get; set; }
    }
}