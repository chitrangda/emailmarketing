﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class ManageBillingViewModel
    {
        public DateTime DateRegistered { get; set; }
        public DateTime LastLogin { get; set; }
        public int SentToday { get; set; }
        public int SentTotal { get; set; }
        public int Contacts { get; set; }
        public DateTime LastUpload { get; set; }
        public int LastUploadAmount { get; set; }
        public bool AccountStatus { get; set; }
        public int MonthlyCredit { get; set; }
        public int PAYGCredit { get; set; }
        public int NumKeywords { get; set; }
        public string Keywords { get; set; }
        public bool BillingStatus { get; set; }
        public int SMPPReload { get; set; }
        public double BillingAmount { get; set; }
        public DateTime NextBilling { get; set; }
        public DateTime LastBilling { get; set; }
        public string OneTimeOwed { get; set; }
        public bool AmountOwed { get; set; }
        public double PAYGPrice { get; set; }
        public int AccountType { get; set; }
        public int BillType { get; set; }
    }
}