﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class IndustryViewModel
    {
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
    }
}