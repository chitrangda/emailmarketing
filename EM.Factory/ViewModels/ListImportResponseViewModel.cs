﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class ListImportResponseViewModel
    {
        public List<ListViewModel> DuplicateList { get; set; }
        public List<ListViewModel> InvalidList { get; set; }
        public List<ListViewModel> SuccessList { get; set; }
    }
}