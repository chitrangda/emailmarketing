﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class RouteViewModel
    {
        public string message { get; set; }

        public Route route { get; set; }
    }
    public class Route
    {
        public string description { get; set; }

        public DateTime created_at { get; set; }

        public List<string> actions { get; set; }

        public int priority { get; set; }
        public string expression { get; set; }
        public string id { get; set; }


    }
}