﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class SendGridIPAddressViewModel
    {
        public string ip { get; set; }

        public string[] subusers { get; set; }
        public string[] pools { get; set; }
        public bool warmup { get; set; }
        public string start_date { get; set; }
        public bool whitelabeled { get; set; }
        public string assigned_at { get; set; }

    }

}