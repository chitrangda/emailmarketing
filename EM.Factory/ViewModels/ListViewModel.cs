﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class ListViewModel
    {
        [Key]
        public int ListId { get; set; }
        [Required]
        public string ListName { get; set; }
        public string Remainder { get; set; }
        public string Company { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string State { get; set; }

        public string Phone { get; set; }
        public Nullable<bool> Optin { get; set; }
        public Nullable<bool> GDPR { get; set; }
        public Nullable<bool> Subscriber_Unsubscriber { get; set; }
        public Nullable<bool> Subscriber { get; set; }
        public Nullable<bool> Unsubscriber { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string CreatedById { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModById { get; set; }
        public Nullable<System.DateTime> LastModDate { get; set; }
        public string ListNameDomainWise { get; set; }

    }
}