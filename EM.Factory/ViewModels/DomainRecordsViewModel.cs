﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class DomainRecordsViewModel
    {
        public string type { get; set; }
        public string data { get; set; }
        public string name { get; set; }
        public int ttl { get; set; }
    }
}