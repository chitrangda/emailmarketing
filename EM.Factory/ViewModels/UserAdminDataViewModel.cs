﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class UserAdminDataViewModel
    {
        public string MonthlyCredit {get;set;}
        public double? BillingAmount {get;set;}
        public int? BillingStatus {get;set;}
        public DateTime? NextPackageUpdateDate { get;set;}
        public bool IsActive { get; set; }
        public string SalesRepId { get; set; }
        public string OldSalesRepId { get; set; }
        public DateTime? DOB { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool CSVUpload { get; set; }
        public string UserId { get; set; }
        public bool CustomizableTemplates { get; set; }
        public string ClientType { get; set; }
        public int CampaignTypeId { get; set; }
        public int PlanTypeId { get; set; }
        public int PlanDuration { get; set; }
        public DateTime? FollowUpDate { get; set; }
        public string ChargedDate { get; set; }
    
        public string TimeZone { get; set; }

        public int IpPool { get; set; }
    }
}