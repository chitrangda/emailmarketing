﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Factory.ViewModels
{
    public class UserPaymentProfileViewModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string AnPaymentProfileId { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Email { get; set; }
        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        [Required]
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        [Required]
        public string Zip { get; set; }
        public bool Default { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string LastModBy { get; set; }
        public Nullable<System.DateTime> LastModDate { get; set; }
        public bool Active { get; set; }
        public string LastFour { get; set; }
        [Required]
        public int ExMonth { get; set; }
        [Required]
        public int ExYear { get; set; }
        public Nullable<bool> Expired { get; set; }
        public string CreatedById { get; set; }
        public string LastModById { get; set; }
        public string CardType { get; set; }
        [Required]
        public string CreditCardNo { get; set; }
        public bool IsPrimary { get; set; }

        public string CVV { get; set; }

        public bool IsDeleted { get; set; }

        public IEnumerable<SelectListItem> YearList { get; set; }

        public IEnumerable<SelectListItem> MonthList { get; set; }


    }
}