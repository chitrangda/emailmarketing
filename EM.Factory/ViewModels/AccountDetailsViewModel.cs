﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Factory.ViewModels
{
    public class AccountDetailsViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ChangePassword { get; set; }
        public string VerifyPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> IndustryRefId { get; set; }
        public string CompanyName { get; set; }
        public string  Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string MobileNumber { get; set; }

        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int AffiliateId { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }

        public string CVv { get; set; }

        public string CardType { get; set; }
        public int ExpMonth { get; set; }
        public int ExpYear { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingPhone { get; set; }

        public IEnumerable<SelectListItem> YearList { get; set; }

        public IEnumerable<SelectListItem> MonthList { get; set; }

        public IEnumerable<SelectListItem> IndustryList { get; set; }

        public IEnumerable<SelectListItem> CountryList { get; set; }
        public IEnumerable<SelectListItem> StateList { get; set; }

        public string CreatedById { get; set; }

        public string LastModById { get; set; }

        public ListViewModel listmodel { get; set; }



    }
}