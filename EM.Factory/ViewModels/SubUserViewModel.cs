﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class SubUserViewModel
    {
        public int id { get; set; }

        public bool disabled { get; set; }

        public string email { get; set; }

        public string username { get; set; }
    }
}