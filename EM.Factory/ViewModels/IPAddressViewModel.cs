﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Factory.ViewModels
{
    public class IPAddressViewModel
    {
        public int Id { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string LastModBy { get; set; }
        public Nullable<System.DateTime> LastModOn { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<int> IPPool_Id { get; set; }
        public string IP { get; set; }

        public virtual IP_Pool IP_Pool { get; set; }

        public IEnumerable<SelectListItem> IPAddressList { get; set; }

        public string[] SelectedIP { get; set; }
    }
}