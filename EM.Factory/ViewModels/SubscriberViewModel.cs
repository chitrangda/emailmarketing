﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class SubscriberViewModel
    {
        public int SubscriberId { get; set; } = 0;
        [Required]
        public int ListId { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public bool EmailPermission { get; set; }
        public string CreatedById { get; set; }
        public int result { get; set; }

        public string ListName { get; set; }
        public bool countInvalidheader { get; set; }
        public bool invalidEmail { get; set; }
        public string validHeaders { get; set; }

        public string Eventbrite_AttendeeId { get; set; }



    }
}