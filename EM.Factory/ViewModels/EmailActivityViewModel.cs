﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class EmailActivityViewModel
    {
        public List<Message> messages { get; set; }
    }
    public class Message
    {
        public string from_email { get; set; }
        public string msg_id { get; set; }
        public string subject { get; set; }
        public string to_email { get; set; }
        public string status { get; set; }
        public int opens_count { get; set; }
        public int clicks_count { get; set; }
        public DateTime last_event_time { get; set; }
    }
}