﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class CampaignViewModel
    {
        public int CampaignId { get; set; }
        public string UserId { get; set; }
        public string ListId { get; set; }
        public int CampaignTypeID { get; set; }
        public int KeywordId { get; set; }
        public string Description { get; set; }
        public string AutomaticResponse { get; set; }
        public string ForwardedCellNumber { get; set; }
        public string ForwardedEmail { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public string CreatedById { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string LastModById { get; set; }
        public System.DateTime LastModDate { get; set; }
        public string HtmlContent { get; set; }
        public string JSONContent { get; set; }
        public string Subject { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string PreviewText { get; set; }
        public int? TemplateId { get; set; }
        public string CampaignName { get; set; }
        public string Status { get; set; }
        public DateTime ScheduleDate { get; set; }
        public string ContentImagePath { get; set; }
        public string SubscriberIds { get; set; }
        public int SubscriberCount { get; set; }
        public DateTime SentDate { get; set; }
        public string SentDateString { get; set; }
        public int Subscribers { get; set; }
        public int Opens { get; set; }
        public int Clicks { get; set; }
        public int Delivered { get; set; }
        public int Sents { get; set; }
        public int Offset { get; set; }
        public List<string> EmailRecipients { get; set; }
        public string ListName { get; set; }
        public DateTime SDateTime { get; set; }
        public int TimeZoneID { get; set; }
        public string TimeZoneName { get; set; }

        public string UniqueSubscriberIds { get; set; }
        public int UniqueSubscriberIdsCount { get; set; }

        public string SendingFromEmail { get; set; }

        public int SpamComplained { get; set; }

        public string ListEmails { get; set; }
    }
}