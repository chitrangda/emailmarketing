﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class SalesRepInfoViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage ="Email is erequired.")]
        [EmailAddress(ErrorMessage ="Inavlid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "First Name is erequired.")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        //[RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Invalid Mobile Number.")]
        [DataType(DataType.PhoneNumber)]
        public string ContactNo { get; set; }

        public Nullable<int> NoOfClients { get; set; }

        public Nullable<double> SpentTotal { get; set; }

        public Nullable<bool> ViewRepActivity { get; set; }

        public Nullable<bool> CanExport { get; set; }

        public Nullable<bool> ExportLead { get; set; }

        public Nullable<bool> Manager { get; set; }

        public Nullable<bool> IsActive { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}