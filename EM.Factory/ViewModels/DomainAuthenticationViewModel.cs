﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class DomainAuthenticationViewModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public string Subdomain { get; set; }

        public string Domain { get; set; }

        public string UserName { get; set; }

        public bool Valid { get; set; }

        public DNS dns { get; set; }

    }

    public class DNS
    {
        public MailCname Mail_Cname { get; set; }
        public Dkim1 dkim1 { get; set; }

        public Dkim2 dkim2 { get; set; }

    }

    public class MailCname
    {
        public bool valid { get; set; }
        public string type { get; set; }

        public string host { get; set; }

        public string data { get; set; }

    }

    public class Dkim1
    {
        public bool valid { get; set; }
        public string type { get; set; }

        public string host { get; set; }

        public string data { get; set; }

    }
    public class Dkim2
    {
        public bool valid { get; set; }
        public string type { get; set; }

        public string host { get; set; }

        public string data { get; set; }

    }

}


 