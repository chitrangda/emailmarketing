﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class SubscriberListViewModel
    {
        public int ListId { get; set; }
        public string ListName { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Opens { get; set; }
        public string Clicked { get; set; }
        public int TotalRecord { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalSubscriber { get; set; }
    }
}