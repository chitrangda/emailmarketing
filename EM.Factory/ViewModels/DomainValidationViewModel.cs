﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class DomainValidationViewModel
    {
        public int Id { get; set; }

        public bool Valid { get; set; }

        public Validation_Results validation_results { get; set; }

    }

    public class Validation_Results
    {
        public VMailCname Mail_Cname { get; set; }
        public VDkim1 dkim1 { get; set; }

        public VDkim2 dkim2 { get; set; }

    }

    public class VMailCname
    {
        public bool valid { get; set; }

        public string reason { get; set; }
    }

    public class VDkim1
    {
        public bool valid { get; set; }

        public string reason { get; set; }

    }
    public class VDkim2
    {
        public bool valid { get; set; }

        public string reason { get; set; }

    }

}