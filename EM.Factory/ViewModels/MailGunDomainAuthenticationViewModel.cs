﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class MailGunDomainAuthenticationViewModel
    {
        public Domain domain { get; set; }

        public List<ReceivingDnsRecord> receiving_dns_records { get; set; }
        public List<SendingDnsRecord> sending_dns_records { get; set; }

        public string message { get; set; }
    }
    public class Domain
    {
       
        public string created_at { get; set; }

        public string id { get; set; }
        public string smtp_login { get; set; }
        public string name { get; set; }
        public string smtp_password { get; set; }
        public bool wildcard { get; set; }
        public string spam_action { get; set; }
        public string state { get; set; }
    }
    public class ReceivingDnsRecord
    {
        public string priority { get; set; }
        public string record_type { get; set; }
        public string valid { get; set; }
        public string value { get; set; }
    }

    public class SendingDnsRecord
    {
        public string record_type { get; set; }
        public string valid { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }
}