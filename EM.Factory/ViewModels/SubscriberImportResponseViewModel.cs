﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class SubscriberImportResponseViewModel
    {
        public List<SubscriberViewModel> DuplicateSubscribers { get; set; }

        public List<SubscriberViewModel> InvalidSubscribers { get; set; }
        public List<SubscriberViewModel> SuccessSubscribers { get; set; }
    }
}