﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EM.Factory.ViewModels
{
    public class UserInfoViewModel
    {
        public string Id { get; set; }


        public string Email { get; set; }
        public string Password { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        public string ProfileImage { get; set; }

        public DateTime? CreatedDate { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$", ErrorMessage = "Invalid mobile no")]
        public string MobilePhone { get; set; }

        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required. ")]
        public string State { get; set; }

        [Required(ErrorMessage = "Country is required.")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Postal Code is required.")]
        [DataType(DataType.PostalCode)]

        [RegularExpression(@"^\d{5}(-\d{4})?$", ErrorMessage = "Invalid zip code")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "Company Name is required.")]
        public string CompanyName { get; set; }

        [DataType(DataType.CreditCard)]
        public string CreditCardNumber { get; set; }

        public string CardType { get; set; }

        public Nullable<System.DateTime> ExpiryDate { get; set; }

        public bool? isActive { get; set; }

        public DateTime? RegisterDate { get; set; }

        public string AuthProfileId { get; set; }

        public string PaymentProfileId { get; set; }
        public string Address1 { get; set; }
        public Nullable<int> IndustryRefId { get; set; }

        public decimal? PackageBillingAmount { get; set; }

        public int? SelectedPackakgeId { get; set; }

        public Nullable<System.DateTime> DOB { get; set; }
        public string SalesRepId { get; set; }

        public IEnumerable<SelectListItem> PlanList { get; set; }

        public IEnumerable<SelectListItem> SalesRepList { get; set; }

        public string PromotionCode { get; set; }

        public string RepName { get; set; }

        public string SalesRepContact { get; set; }

        public int planId { get; set; }

        public int TotalRecord { get; set; }

        public int? TimeZoneID { get; set; }

        public string SendingFromEmail { get; set; }

        public decimal? BounceRate { get; set; }

        public bool? EmailSending { get; set; }






    }
}