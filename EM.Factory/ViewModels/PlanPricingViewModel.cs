﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EM.Factory.ViewModels
{
    public class PlanPricingViewModel
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public Nullable<bool> ContactManagement { get; set; }
        public Nullable<bool> UnlimitedEmails { get; set; }
        public Nullable<bool> Automation { get; set; }
        public Nullable<bool> Segmenting { get; set; }
        public Nullable<bool> Analytics { get; set; }
        public Nullable<bool> EmailScheduling { get; set; }
        public Nullable<bool> CustomizableTemplates { get; set; }
        public Nullable<bool> EducationalResources { get; set; }
        public Nullable<bool> LiveSupport { get; set; }
        public Nullable<int> NoOfSubscribersMax { get; set; }
        public Nullable<int> NoOfSubscribersMin { get; set; }
        public Nullable<int> TotalEmails { get; set; }

        public string NoOfCredits { get; set; }

        public Nullable<int> TotalDays { get; set; }
        public List<PlanPricingsViewModel> planPricingsViewModels { get; set; }
    }
    public class PlanPricingsViewModel
    {
        public int Id { get; set; }
        public Nullable<int> PlanId { get; set; }
        public Nullable<decimal> BillingAmount { get; set; }
        public Nullable<int> TotalMonth { get; set; }
        public Nullable<int> NoOfSubscribersMax { get; set; }
    }
}