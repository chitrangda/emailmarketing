//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    
    public partial class usp_getDashboardReport_Result
    {
        public Nullable<int> Row { get; set; }
        public string UserId { get; set; }
        public Nullable<int> CampaignId { get; set; }
        public string CamapignName { get; set; }
        public Nullable<int> Opens { get; set; }
        public Nullable<int> Clicks { get; set; }
        public int RecordNumber { get; set; }
        public Nullable<int> TotalRecord { get; set; }
        public Nullable<int> PageNumber { get; set; }
        public Nullable<int> PageSize { get; set; }
    }
}
