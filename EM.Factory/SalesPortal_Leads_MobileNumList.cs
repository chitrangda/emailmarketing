//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesPortal_Leads_MobileNumList
    {
        public int MobileRowId { get; set; }
        public int LeadId { get; set; }
        public string MobileNumber { get; set; }
        public bool IsPrimaryNumber { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<bool> IsDelete { get; set; }
    }
}
