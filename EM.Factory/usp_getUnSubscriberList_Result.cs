//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    
    public partial class usp_getUnSubscriberList_Result
    {
        public Nullable<long> Row { get; set; }
        public Nullable<int> SubscriberId { get; set; }
        public int ListId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Email { get; set; }
        public Nullable<bool> EmailPermission { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> LastModDate { get; set; }
        public Nullable<int> TotalRecord { get; set; }
        public Nullable<int> CurrentPage { get; set; }
        public Nullable<int> PageSize { get; set; }
        public Nullable<bool> IsSubscribe { get; set; }
    }
}
