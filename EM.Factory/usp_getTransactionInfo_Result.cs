//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    
    public partial class usp_getTransactionInfo_Result
    {
        public string UserId { get; set; }
        public string AnTransactionId { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public string Amount { get; set; }
        public string Message { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string CardType { get; set; }
        public string CreditCardNo { get; set; }
        public string MobilePhone { get; set; }
        public string ContactEmail { get; set; }
    }
}
