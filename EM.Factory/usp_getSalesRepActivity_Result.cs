//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EM.Factory
{
    using System;
    
    public partial class usp_getSalesRepActivity_Result
    {
        public int RepActivityID { get; set; }
        public string Username { get; set; }
        public string UserId { get; set; }
        public string RepId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<int> NoteId { get; set; }
        public string NoteDescription { get; set; }
        public Nullable<bool> IsComment { get; set; }
        public string ClientName { get; set; }
    }
}
