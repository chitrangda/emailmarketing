﻿using EM.Factory;
using NMIPayment;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EM.PaymentService
{
    public class Payment : IDisposable
    {
        private bool _disposed;

        public async void Transaction(PaymentInfo paymentInfo, bool isProduction = false)
        {

            _disposed = false;
            var status = string.Empty;
            XmlDocument xmlRequest = new XmlDocument();

            XmlDeclaration xmlDecl = xmlRequest.CreateXmlDeclaration("1.0", "UTF-8", "yes");

            XmlElement root = xmlRequest.DocumentElement;
            xmlRequest.InsertBefore(xmlDecl, root);


            XmlElement xmlSale = xmlRequest.CreateElement("sale");

            XmlElement xmlApiKey = xmlRequest.CreateElement("api-key");

            xmlApiKey.InnerText = isProduction ? getProductionKey() : getStagingKey();

            xmlSale.AppendChild(xmlApiKey);

            XmlElement xmlRedirectUrl = xmlRequest.CreateElement("redirect-url");
            //xmlRedirectUrl.InnerText = HttpContext.Current.Request["HTTP_REFERER"];
            xmlRedirectUrl.InnerText = paymentInfo.NavigateUrl;
            xmlSale.AppendChild(xmlRedirectUrl);

            if (paymentInfo.amount != null)
            {
                XmlElement xmlAmount = xmlRequest.CreateElement("amount");
                xmlAmount.InnerText = paymentInfo.amount;
                xmlSale.AppendChild(xmlAmount);
            }

           if (paymentInfo.orderInfo != null)
            {
                XmlElement xmlCurrency = xmlRequest.CreateElement("currency");
                xmlCurrency.InnerText = paymentInfo.orderInfo.Currency != null ? paymentInfo.orderInfo.Currency : "USD";
                xmlSale.AppendChild(xmlCurrency);


                XmlElement xmlOrderId = xmlRequest.CreateElement("order-id");
                xmlOrderId.InnerText = paymentInfo.orderInfo.OrderId;
                xmlSale.AppendChild(xmlOrderId);

                XmlElement xmlOrderDescription = xmlRequest.CreateElement("order-description");
                xmlOrderDescription.InnerText = paymentInfo.orderInfo.OrderDesc;
                xmlSale.AppendChild(xmlOrderDescription);

                XmlElement xmlTax = xmlRequest.CreateElement("tax-amount");
                xmlTax.InnerText = paymentInfo.orderInfo.TaxAmount.ToString();
                xmlSale.AppendChild(xmlTax);

                XmlElement xmlShipping = xmlRequest.CreateElement("shipping-amount");
                xmlShipping.InnerText = "0.0";
                xmlSale.AppendChild(xmlShipping);

                if (paymentInfo.orderInfo.PoNumber != null)
                {
                    XmlElement xmlPoNumber = xmlRequest.CreateElement("po-number");
                    xmlPoNumber.InnerText = paymentInfo.orderInfo.PoNumber;
                    xmlSale.AppendChild(xmlPoNumber);
                }

                if (paymentInfo.orderInfo.CustomField != null)
                {
                    XmlElement xmlCustomField = xmlRequest.CreateElement("merchant-defined-field-1");
                    xmlCustomField.InnerText = paymentInfo.orderInfo.CustomField;
                    xmlSale.AppendChild(xmlCustomField);
                }
                if (paymentInfo.orderInfo.CustomField2 != null)
                {
                    XmlElement xmlCustomField2 = xmlRequest.CreateElement("merchant-defined-field-2");
                    xmlCustomField2.InnerText = paymentInfo.orderInfo.CustomField2;
                    xmlSale.AppendChild(xmlCustomField2);
                }
                if (paymentInfo.TransCode != null)
                {
                    XmlElement xmlCustomField3 = xmlRequest.CreateElement("merchant-defined-field-3");
                    xmlCustomField3.InnerText = paymentInfo.TransCode;
                    xmlSale.AppendChild(xmlCustomField3);
                }
                if (paymentInfo.orderInfo.CustomField3 != null)
                {
                    XmlElement xmlCustomField4 = xmlRequest.CreateElement("merchant-defined-field-4");
                    xmlCustomField4.InnerText = paymentInfo.orderInfo.CustomField3;
                    xmlSale.AppendChild(xmlCustomField4);
                }
            }


            XmlElement xmlCustomerVaultId = xmlRequest.CreateElement("customer-vault-id");
            xmlCustomerVaultId.InnerText = paymentInfo.CustomerVaultId;
            xmlSale.AppendChild(xmlCustomerVaultId);

            xmlRequest.AppendChild(xmlSale);


            string responseFromServer = await sendXMLRequest(xmlRequest, isProduction);
            //Console.WriteLine(responseFromServer);
            XmlReader responseReader = XmlReader.Create(new StringReader(responseFromServer));
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(responseReader);
            XmlNodeList response = xDoc.GetElementsByTagName("result");
            XmlNodeList responseCode = xDoc.GetElementsByTagName("result-code");
            XmlNodeList responseText = xDoc.GetElementsByTagName("result-text");
            if (response[0].InnerText.Equals("1"))
            {
                XmlNodeList formUrl = xDoc.GetElementsByTagName("form-url");
                responseReader.Close();
                string token = formUrl[0].InnerText.ToString().Substring((formUrl[0].InnerText.LastIndexOf("/") + 1));
                using (var db = new EmailMarketingDbEntities())
                {
                    AnTransactionLog transLog = new AnTransactionLog();
                    transLog.CreatedBy = "Monthly Payment Service";
                    transLog.TransactionType = "Credit Card";
                    transLog.Amount = paymentInfo.amount;
                    transLog.CreatedDate = DateTime.Now;
                    transLog.UserId = paymentInfo.orderInfo.CustomField;
                    transLog.PaymentProfileId = paymentInfo.CustomerVaultId;
                    transLog.Message = paymentInfo.orderInfo.OrderDesc;
                    transLog.TransCode = token;
                    db.AnTransactionLogs.Add(transLog);
                    db.SaveChanges();

                }
                sendCreditCardDetails(formUrl[0].InnerText, paymentInfo.CCNumber, paymentInfo.CCEXp, paymentInfo.CCV);
            }
            else
            {
                using (var db = new EmailMarketingDbEntities())
                {
                    AnTransactionLog transLog = new AnTransactionLog();
                    transLog.TransactionDate = DateTime.Now;
                    transLog.CreatedBy = "Monthly Payment Service";
                    transLog.TransactionType = "Credit Card";
                    transLog.Amount = paymentInfo.amount;
                    transLog.CreatedDate = DateTime.Now;
                    transLog.UserId = paymentInfo.CustomerVaultId;
                    transLog.StatusCode = responseCode[0].InnerText;
                    transLog.StatusText = responseText[0].InnerText;
                    db.AnTransactionLogs.Add(transLog);

                    var curPlan = db.UserPlans.Find(paymentInfo.orderInfo.PoNumber);
                    if (curPlan != null)
                    {
                        curPlan.BillingStatus = -1;
                        db.SaveChanges();
                    }
                }

            }
        }


        public async static Task<string> sendXMLRequest(XmlDocument xmlRequest, bool isProduction = false)
        {
            try
            {
                string uri = isProduction == true ? getProductionUrl() : getStagingUrl();

                WebRequest req = WebRequest.Create(uri);
                req.Method = "POST";        // Post method
                req.ContentType = "text/xml";     // content type
                                                  // Wrap the request stream with a text-based writer
                StreamWriter writer = new StreamWriter(req.GetRequestStream());
                // Write the XML text into the stream

                xmlRequest.Save(writer);

                writer.Close();
                // Send the data to the webserver
                WebResponse rsp = req.GetResponse();

                Stream dataStream = rsp.GetResponseStream();
                // Open the stream using a StreamReader 
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                reader.Close();
                dataStream.Close();
                rsp.Close();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        static async void sendCreditCardDetails(string url, string CCNumber, string ExpiryDate, string CCV)
        {
            try
            {

                using (WebClient client = new WebClient())
                {
                    NameValueCollection vals = new NameValueCollection();
                    client.UploadValues(url, vals);
                }


            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message, DateTime.Now.ToString("MMDDYY_hhmmss"));
            }

        }

        
        public static string getStagingUrl()
        {
            return "https://secure.nmi.com/api/v2/three-step";
        }

        public static string getProductionUrl()
        {
            return "https://secure.nmi.com/api/v2/three-step";
        }

        public static string getStagingKey()
        {
            return "2F822Rw39fx762MaV7Yy86jXGTC7sCDy";
        }

        public static string getProductionKey()
        {
            return "Tb5r6es7pxhrhu6sV6X8vb375dExkQnB";
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            //
            if (!this._disposed)
            {
                if (disposing)
                {
                    //
                }
                _disposed = true;
            }
        }

    }
}
