﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EM.Factory;
using EM.Helpers;
using NMIPayment;

namespace EM.PaymentService
{
    class PaymentTransaction
    {
        EmailMarketingDbEntities dbContext = new EmailMarketingDbEntities();
        public async Task GetBillingUsers()
        {
            var billingList = dbContext.usp_getNextBilling().ToList();
            if (billingList != null)
            {
                foreach (var billing in billingList)
                {
                    try
                    {
                        await ChargeCustomers(billing);
                    }
                    catch (Exception ex)
                    {
                        CreateLog.ActionLog(ex.Message);
                    }
                }
            }


        }

        public async Task ChargeCustomers(usp_getNextBilling_Result billingInfo)
        {
            try
            {
                if (billingInfo != null)
                {
                    if (billingInfo.AnPaymentProfileId != null)
                    {
                        using (Payment pp = new Payment())
                        {
                            decimal amount = Convert.ToDecimal(billingInfo.BillingAmount);
                            PaymentInfo profile = new PaymentInfo();
                            profile.NavigateUrl = ConfigurationManager.AppSettings["PaymentStatusUrl"] ;
                            profile.amount = amount.ToString();
                            profile.orderInfo = new OrderInfo();
                            profile.orderInfo.OrderId = billingInfo.PackageId.ToString() + "_" + billingInfo.userid + "_" + DateTime.Now.ToString("MMddyy");
                            profile.orderInfo.OrderDesc = billingInfo.PlanName;
                            profile.orderInfo.TaxAmount = 0.00;
                            profile.orderInfo.PoNumber = Convert.ToString(billingInfo.UserPlanId);
                            profile.orderInfo.CustomField = billingInfo.userid;
                            profile.orderInfo.CustomField3 = billingInfo.CardType;
                            profile.orderInfo.CustomField2 = billingInfo.PackageId.ToString();
                            profile.CustomerVaultId = billingInfo.AnPaymentProfileId;
                            pp.Transaction(profile, false);
                        }

                    }
                    else if (billingInfo.BillingAmount == 0)
                    {
                        //create new plan
                        var _curPlan = dbContext.UserPlans.Where(r => r.UserId == billingInfo.userid && r.IsActive == true).FirstOrDefault();
                        if (_curPlan != null)
                        {
                            _curPlan.IsActive = false;
                            dbContext.SaveChanges();
                        }
                        UserPlan oPlan = new UserPlan();
                        oPlan.UserId = billingInfo.userid;
                        oPlan.TotalEmails = Convert.ToInt32(_curPlan.MonthlyCredit);
                        oPlan.TotalDays = 30;
                        oPlan.PackageId = billingInfo.PackageId;
                        oPlan.MonthlyCredit = _curPlan.MonthlyCredit;
                        oPlan.NoOfSubscribersMin = 0;
                        oPlan.NoOfSubscribersMax = _curPlan.NoOfSubscribersMax;
                        oPlan.BillingAmount = _curPlan.BillingAmount != null ? _curPlan.BillingAmount.Value : 0;
                        oPlan.totalRemainingCredits = _curPlan.MonthlyCredit;
                        oPlan.ContactManagement = _curPlan.ContactManagement;
                        oPlan.UnlimitedEmails = _curPlan.UnlimitedEmails;
                        oPlan.EmailScheduling = _curPlan.EmailScheduling;
                        oPlan.EducationalResources = _curPlan.EducationalResources;
                        oPlan.LiveSupport = _curPlan.LiveSupport;
                        oPlan.CustomizableTemplates = _curPlan.CustomizableTemplates;
                        oPlan.totalSpendCredits = "0";
                        oPlan.IsActive = true;
                        oPlan.PackageUpdateDate = DateTime.Now;
                        oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                        oPlan.BillingStatus = 1;
                        dbContext.UserPlans.Add(oPlan);
                        dbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogService.CreateLog(billingInfo.userid, ex.Message, "Payment Service - ChargeCustomers");
            }
        }
        //AnTranResponse tranres = null;
        //using (PaymentSession paySession = new PaymentSession())
        //{
        //    decimal _amount = billingInfo.BillingAmount != null ? (decimal)billingInfo.BillingAmount.Value : 0;
        //    tranres = paySession.ChargeCustomerProfile(billingInfo.AnPaymentProfileId, billingInfo.AuthProfileId, _amount, billingInfo.PlanName);
        //    if (paySession.Status && tranres.ResponseCode == "1")
        //    {
        //        AnTransactionLog transLog = new AnTransactionLog();
        //        transLog.AnTransactionId = tranres.TransId;
        //        transLog.TransactionDate = DateTime.Now;
        //        transLog.CreatedBy = "Monthly Payment";
        //        transLog.Message = "approval Code: " + tranres.ApprovalCode;
        //        transLog.TransactionType = "Credit Card";
        //        transLog.Amount = _amount.ToString();
        //        transLog.TransactionDate = DateTime.Now;
        //        transLog.UserId = billingInfo.userid;
        //        transLog.CreatedDate = DateTime.Now;
        //        transLog.PaymentProfileId = billingInfo.AnPaymentProfileId;
        //        dbContext.AnTransactionLogs.Add(transLog);
        //        await dbContext.SaveChangesAsync();
        //        await SavePlan(billingInfo);

        //    }
        //    else
        //    {
        //        AnTransactionLog transLog = new AnTransactionLog();
        //        transLog.AnTransactionId = null;
        //        transLog.TransactionDate = DateTime.Now;
        //        transLog.CreatedBy = "Monthly Payment";
        //        transLog.Message = paySession.Message;
        //        transLog.TransactionType = "Credit Card";
        //        transLog.Amount = _amount.ToString();
        //        transLog.TransactionDate = DateTime.Now;
        //        transLog.UserId = billingInfo.userid;
        //        transLog.CreatedDate = DateTime.Now;
        //        transLog.PaymentProfileId = billingInfo.AnPaymentProfileId;
        //        dbContext.AnTransactionLogs.Add(transLog);
        //        await dbContext.SaveChangesAsync();
        //        await SaveBillingStatus(billingInfo.UserPlanId, billingInfo.userid);
        //        LogService.CreateLog(billingInfo.userid, "Payment failed- " + paySession.Message, "Payment Service - ChargeCustomers");
        //    }

        //}



        public async Task SavePlan(usp_getNextBilling_Result billingInfo)
        {
            try
            {
                var _curPlan = dbContext.UserPlans.Where(r => r.UserId == billingInfo.userid && r.IsActive == true).FirstOrDefault();
                if (_curPlan != null)
                {
                    _curPlan.IsActive = false;
                    dbContext.SaveChanges();
                }
                var planDetail = dbContext.Packages.SingleOrDefault(r => r.Id == billingInfo.PackageId);
                UserPlan oPlan = new UserPlan();
                oPlan.UserId = billingInfo.userid;
                oPlan.TotalEmails = planDetail.NoOfCredits;
                oPlan.TotalDays = 30;
                oPlan.PackageId = billingInfo.PackageId;
                oPlan.MonthlyCredit = planDetail.NoOfCredits.ToString();
                oPlan.NoOfSubscribersMin = 0;
                oPlan.NoOfSubscribersMax = planDetail.NoOfSubscribersMax;
                oPlan.PackageUpdateDate = DateTime.Now;
                oPlan.BillingAmount = planDetail.BillingAmount != null ? planDetail.BillingAmount.Value : 0;
                oPlan.totalRemainingCredits = planDetail.NoOfCredits.ToString();
                oPlan.ContactManagement = planDetail.Plan.ContactManagement;
                oPlan.UnlimitedEmails = planDetail.Plan.UnlimitedEmails;
                //oPlan.Automation = planDetail.Plan.Automation;
                //oPlan.Segmenting = planDetail.Plan.Segmenting;
                //oPlan.Analytics = planDetail.Plan.Analytics;
                oPlan.EmailScheduling = planDetail.Plan.EmailScheduling;
                oPlan.EducationalResources = planDetail.Plan.EducationalResources;
                oPlan.LiveSupport = planDetail.Plan.LiveSupport;
                oPlan.CustomizableTemplates = planDetail.Plan.CustomizableTemplates;
                oPlan.totalSpendCredits = "0";
                oPlan.IsActive = true;
                oPlan.NextPackageUpdateDate = DateTime.Now.AddDays(30);
                oPlan.BillingStatus = 1;
                dbContext.UserPlans.Add(oPlan);
                await dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                LogService.CreateLog(billingInfo.userid, ex.Message, "Payment Service - SavePlan");
            }
        }

        public async Task SaveBillingStatus(int userPlanId, string userid)
        {
            try
            {
                var curPlan = dbContext.UserPlans.Find(userPlanId);
                if (curPlan != null)
                {
                    curPlan.BillingStatus = -1;
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                LogService.CreateLog(userid, ex.Message, "Payment Service - SaveBillingStatus");
            }
        }

    }
}
