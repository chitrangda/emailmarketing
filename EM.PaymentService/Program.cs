using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.PaymentService
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                PaymentTransaction trans = new PaymentTransaction();
                trans.GetBillingUsers().Wait();
            }
            catch (Exception ex)
            {
                LogService.WriteToLog(ex.Message, DateTime.Now.ToString("MMDDYY_hhmmss"));
            }
        }
    }
}
